package eqtrade.trading.engine;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import quickfix.FieldNotFound;
import quickfix.Message;
import quickfix.StringField;
import quickfix.field.BeginString;
import quickfix.field.ClOrdID;
import quickfix.field.ClientID;
import quickfix.field.ComplianceID;
import quickfix.field.ContraBroker;
import quickfix.field.ContraTrader;
import quickfix.field.CumQty;
import quickfix.field.ExecBroker;
import quickfix.field.HandlInst;
import quickfix.field.IOIid;
import quickfix.field.LeavesQty;
import quickfix.field.MsgType;
import quickfix.field.NoContraBrokers;
import quickfix.field.OrdStatus;
import quickfix.field.OrdType;
import quickfix.field.OrderID;
import quickfix.field.OrderQty;
import quickfix.field.OrigClOrdID;
import quickfix.field.Price;
import quickfix.field.SecondaryOrderID;
import quickfix.field.SecurityID;
import quickfix.field.Side;
import quickfix.field.Symbol;
import quickfix.field.SymbolSfx;
import quickfix.field.Text;
import quickfix.field.TimeInForce;
import quickfix.field.TradeDate;
import quickfix.field.TransactTime;
import quickfix.field.UserId;




import com.vollux.idata.GridModel;

import eqtrade.trading.core.Event;
import eqtrade.trading.core.EventHandler;
import eqtrade.trading.core.ITradingEngine;
import eqtrade.trading.core.ITradingListener;
import eqtrade.trading.core.OrderMatrixData;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.socket.TradingSocketConnection;
import eqtrade.trading.model.AccStatus;
import eqtrade.trading.model.AccStatusDef;
import eqtrade.trading.model.AccStock;
import eqtrade.trading.model.AccStockDef;
import eqtrade.trading.model.AccType;
import eqtrade.trading.model.AccTypeDef;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.AccountDef;
import eqtrade.trading.model.AccountRender;
import eqtrade.trading.model.Announcement;
import eqtrade.trading.model.AnnouncementDef;
import eqtrade.trading.model.Board;
import eqtrade.trading.model.BoardDef;
import eqtrade.trading.model.Broker;
import eqtrade.trading.model.BrokerDef;
import eqtrade.trading.model.BrowseOrder;
import eqtrade.trading.model.BrowseOrderDef;
import eqtrade.trading.model.BuySellDetail;
import eqtrade.trading.model.BuySellDetailDef;
import eqtrade.trading.model.CashColl;
import eqtrade.trading.model.CashCollDef;

import eqtrade.trading.model.CustType;
import eqtrade.trading.model.CustTypeDef;
import eqtrade.trading.model.DueDate;
import eqtrade.trading.model.DueDateDef;
import eqtrade.trading.model.Exchange;
import eqtrade.trading.model.ExchangeDef;
import eqtrade.trading.model.GTCDef;
import eqtrade.trading.model.GTCRender;
import eqtrade.trading.model.InvType;
import eqtrade.trading.model.InvTypeDef;
import eqtrade.trading.model.Margin;
import eqtrade.trading.model.MarginDef;
import eqtrade.trading.model.Match;
import eqtrade.trading.model.NegDealList;
import eqtrade.trading.model.NegDealListDef;
import eqtrade.trading.model.Notif;
import eqtrade.trading.model.NotifDef;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.OrderField;
import eqtrade.trading.model.OrderRender;
import eqtrade.trading.model.OrderStatus;
import eqtrade.trading.model.OrderStatusDef;
import eqtrade.trading.model.Portfolio;
import eqtrade.trading.model.PortfolioDef;
import eqtrade.trading.model.Schedule;
import eqtrade.trading.model.ScheduleDef;
import eqtrade.trading.model.Stock;
import eqtrade.trading.model.StockDef;
import eqtrade.trading.model.UserProfile;
import eqtrade.trading.model.UserProfileDef;
import eqtrade.trading.model.UserType;
import eqtrade.trading.model.UserTypeDef;
import eqtrade.trading.model.OrderMatrix;
import eqtrade.trading.model.OrderMatrixDef;

public class TradingEventDispatcher {
	protected final Log log = LogFactory.getLog(getClass());
	public static final int STATE_READY = 0;
	public static final int STATE_NOTREADY = 1;
	public static final int STATE_DISCONNECT = 2;

	// administratif event
	public static final String EVENT_LOGIN = "login";
	public static final String EVENT_LOGINOK = "login.ok";
	public static final String EVENT_LOGINSTATUS = "login.status";
	public static final String EVENT_LOGINFAILED = "login.failed";
	public static final String EVENT_RECONNECT = "reconnect";
	public static final String EVENT_RECONNECTOK = "reconnect.ok";
	public static final String EVENT_RECONNECTSTATUS = "reconnect.status";
	public static final String EVENT_RECONNECTFAILED = "reconect.failed";
	public static final String EVENT_CHGPASSWD = "change.password";
	public static final String EVENT_CHGPASSWDOK = "change.password.ok";
	public static final String EVENT_CHGPASSWDFAILED = "change.password.failed";
	public static final String EVENT_DISCONNECT = "disconnect";
	public static final String EVENT_LOGOUT = "logout";

	public static final String EVENT_CHECKPIN = "check.pin";
	public static final String EVENT_CHECKPINSLS = "check.pinsls";
	public static final String EVENT_CHECKPINOK = "check.pin.ok";
	public static final String EVENT_CHECKPINFAILED = "check.pin.failed";
	public static final String EVENT_CHGPIN = "change.pin";

	public static final String EVENT_CHECKPIN_NEGO = "check.pin.nego";
	public static final String EVENT_CHECKPIN_NEGO_OK = "check.pin.nego.ok";
	public static final String EVENT_CHECKPIN_NEGO_FAILED = "check.pin.nego.failed";

	public static final String EVENT_CHGPINOK = "change.pin.ok";
	public static final String EVENT_CHGPINFAILED = "change.pin.failed";

	public static final String EVENT_NEWSESSIONID = "new.session.id";
	public final static String EVENT_KILL = "event.kill";

	public static final String EVENT_REFRESH = "refresh";
	public static final String EVENT_REFRESHOK = "refresh.ok";
	public static final String EVENT_REFRESHSTATUS = "refresh.status";
	public static final String EVENT_REFRESHFAILED = "refresh.failed";
	public static final String EVENT_REFRESHPORTFOLIO = "refresh.portfolio.ok";

	public static final String EVENT_REFRESHACCOUNTID = "refresh.account.ok";

	public static final String EVENT_REFRESH_BYID = "refresh.by.id";

	public static final String EVENT_REFRESH_ORDER = "refresh.order";
	public static final String EVENT_REFRESH_TRADE = "refresh.order.trade";
	public static final String EVENT_REFRESH_PORTFOLIO = "refresh.portfolio";
	public static final String EVENT_REFRESH_ACCOUNT = "refresh.account";
	public static final String EVENT_REFRESH_MARGIN = "refresh.margin";// Margin
	public static final String EVENT_REFRESH_CASHCOLL = "refresh.cashcoll";
	public static final String EVENT_REFRESH_BUYSELLDETAIL = "refresh.buyselldetail";
	public static final String EVENT_REFRESH_DUEDATE = "refresh.duedate";
	public static final String EVENT_REFRESH_BACKNOTIF = "refresh.backnotif";
	public static final String EVENT_REFRESH_LOADORDERMATRIX = "refresh.loadordermatrix";
	public static final String EVENT_REFRESH_LOADORDERMATRIXLIST = "refresh.loadordermatrixlist";
	public static final String EVENT_REFRESH_ANNOUNCEMENT = "refresh.announcement";
	public static final String EVENT_REFRESH_ATS = "refresh.ats";
	public static final String EVENT_REFRESH_BROWSEATS = "refresh.browseats";
	public static final String EVENT_REFRESH_GTC = "refresh.gtc";
	public static final String PARAM_USERID = new String("userid");
	public static final String PARAM_PIN = new String("pin");
	public static final String PARAM_OLDPIN = new String("oldpin");
	public static final String PARAM_PASSWORD = new String("password");
	public static final String PARAM_OLDPASSWORD = new String("oldpassword");
	public static final String PARAM_CODE = new String("code");
	public static final String PARAM_MESSAGE = new String("message");
	public static final String PARAM_CLEAR = new String("clear");
	public static final String PARAM_COUNTER = new String("counter");
	public static final String PARAM_FIX = new String("fix");
	public static final String PARAM_ORDER = new String("order");
	public static final String PARAM_TRADE = new String("trade");
	public static final String PARAM_OLDSTATUS = new String("status");
	public static final String PARAM_OLDORDER = new String("old.order");
	public static final String PARAM_NEWORDER = new String("new.order");
	public static final String PARAM_RESULT = new String("result");
	public static final String PARAM_SESSIONID = new String("sessionid");
	public static final String PARAM_DATA = new String("data");
	public static final String PARAM_TIMESERVER = new String("timeserver");
	public static final String PARAM_ORDERID = new String("orderid");
	public static final String PARAM_ACCOUNTID = new String("accountid");
	public static final String PARAM_TRADINGID = new String("tradingid");
	public static final String PARAM_STOCK = new String("stock");
	public static final String PARAM_SCHEDULE = new String("schedule");// param ats
	public static final String PARAM_OLDSCHEDULE = new String("old.schedule");// old
	
	public static final String PARAM_ORDERMATRIX = new String("ordermatrix");
	public static final String PARAM_ORDERMATRIXLABEL = new String("ordermatrixlabel") ;
	public static final String PARAM_BASKETSENDTIME = new String("basketsendtime");
	
	public static final String PARAM_BUYORSELL = new String("buyorsell");
	public static final String PARAM_NOTIFICAION = new String("notification");
	public static final String PARAM_BACKNOTIF = new String("backnotif");
	public static final String PARAM_FORM = new String("form");


	public static final String PARAM_GTC = new String("GTC");
	public static final int PARAM_success = 7000;
	public static final int PARAM_desc = 7001;
	public static final int PARAM_curtl = 7002;
	public static final int PARAM_pavport = 7003;
	public static final int PARAM_ptlbefore = 7004;
	public static final int PARAM_pavportbefore = 7005;
	public static final int PARAM_plqvaluebefore = 7006;
	public static final int PARAM_pratiobefore = 7007;
	public static final int PARAM_pisoverlimit = 7008;
	public static final int PARAM_pisshortsell = 7009;
	public static final int PARAM_pcurratio = 7010;
	public static final int PARAM_pbuy = 7011;
	public static final int PARAM_psell = 7012;
	public static final int PARAM_pnetac = 7013;
	public static final int PARAM_pbid = 7014;
	public static final int PARAM_poffer = 7015;
	public static final int PARAM_pstockval = 7016;
	public static final int PARAM_pmarketval = 7017;
	public static final int PARAM_plqvalue = 7018;
	public static final int PARAM_ptopup = 7019;
	public static final int PARAM_pforcesell = 7020;
	public static final int PARAM_pcreditlimit = 7021;
	public static final int PARAM_pdeposit = 7022;
	public static final int PARAM_pcurvol = 7023;
	public static final int PARAM_pavgprice = 7024;
	public static final int PARAM_curtlold = 7025;
	public static final int PARAM_statusold = 7026;
	public static final int PARAM_pavportold = 7027;
	public static final int PARAM_pbuyold = 7028;
	public static final int PARAM_psellold = 7029;
	public static final int PARAM_pnetacold = 7030;
	public static final int PARAM_pbidold = 7031;
	public static final int PARAM_pofferold = 7032;
	public static final int PARAM_pstockvalold = 7033;
	public static final int PARAM_pmarketvalold = 7034;
	public static final int PARAM_plqvalueold = 7035;
	public static final int PARAM_ptopupold = 7036;
	public static final int PARAM_pforcesellold = 7037;
	public static final int PARAM_pcreditlimitold = 7038;
	public static final int PARAM_pdepositold = 7039;
	public static final int PARAM_pcurvolold = 7040;
	public static final int PARAM_pavgpriceold = 7041;
	public static final int PARAM_pcurratioold = 7042;
	public static final int PARAM_descold = 7053;
	
	// nego
	public static final int PARAM_plqvaluebeforeold = 7054;
	public static final int PARAM_pratiobeforeold = 7055;
	public static final int PARAM_isoverlimitold = 7056;
	public static final int PARAM_isshortsellold = 7057;
	public static final int PARAM_ptlbeforeold = 7058;
	public static final int PARAM_counteruserid = 7059;
	public static final int PARAM_pavportbeforeold = 7060;

	public static final int PARAM_pwithdraw = 7061;
	public static final int PARAM_pwithdrawold = 7062;
	public static final int PARAM_pwithdrawnew = 7063;
	public static final int atsbdfilter = 999113;
	public static final int atsid = 999100;
	public static final int PARAM_ATS_STATUS = 999126;
	public static final int PARAM_atsOrderStatus = 999105;
	public static final int PARAM_atsEntrytime = 666671;
	public static final int PARAM_atsValidUntil = 999104;
	public static final int PARAM_atsExecby = 999119;
	public static final int PARAM_atsExectime = 999120;
	public static final int PARAM_atsWithdrawtime = 999122;
	public static final int PARAM_atsWithdrawBy = 999121;
	public static final int PARAM_atsWithdrawTerminal = 999123;
	public static final int PARAM_atsDescription = 999118;
	

	// data event
	public static final String EVENT_DATAACCOUNTTYPE = "data.account.type";
	public static final String EVENT_DATAINVTYPE = "data.inv.type";
	public static final String EVENT_DATAUSERTYPE = "data.user.type";
	public static final String EVENT_DATACUSTTYPE = "data.cust.type";
	public static final String EVENT_DATAEXCHANGE = "data.exchange";
	public static final String EVENT_DATABOARD = "data.board";
	public static final String EVENT_DATABROKER = "data.broker";
	public static final String EVENT_DATASTOCK = "data.stock";
	public static final String EVENT_DATAACCSTATUS = "data.account.status";
	public static final String EVENT_DATAORDERSTATUS = "data.order.status";
	public static final String EVENT_DATAACCSTOCK = "data.account.stock";
	public static final String EVENT_DATAUSERPROFILE = "data.user.profile";
	public static final String EVENT_DATAORDER = "data.order";
	public static final String EVENT_DATATRADE = "data.trade";
	public static final String EVENT_DATAPORTFOLIO = "data.portfolio";
	public static final String EVENT_DATAACCOUNT = "data.account";
	public static final String EVENT_DATAMARGIN = "data.margin";// margin
	public static final String EVENT_DATANEGDEALLIST = "data.negdeallist";
	public static final String EVENT_TIMESERVER = "time.server";
	public static final String EVENT_DATACASHCOLL = "data.cash.coll";
	public static final String EVENT_DATABUYSELLDETAIL = "data.buyselldetail";
	public static final String EVENT_DATADUEDATE = "data.duedate";
	public static final String EVENT_DATASCHEDULE = "data.schedule";// schedule
	public static final String EVENT_DATABROWSEORDER = "data.browseorder";// browseorder
	public static final String EVENT_DATAANNOUNCEMENT = "data.announcement";
	public static final String EVENT_DATABACKNOTIF = "data.backnotif";
	
	public static final String EVENT_ORDERMATRIX = "data.ordermatrix";
	public static final String EVENT_ORDERMATRIXLIST = "data.ordermatrixlist";

	public static final String EVENT_DATAGTC = "data.gtc";
	// trading event
	public static final String EVENT_REQUESTAMEND = "request.amend";
	public static final String EVENT_REQUESTAMEND_ACK = "request.amend.ack";
	public static final String EVENT_REQUESTAMEND_NACK = "request.amend.nack";
	public static final String EVENT_REQUESTAMEND_OK = "request.amend.ok";
	public static final String EVENT_REQUESTAMEND_FAILED = "request.amend.failed";

	public static final String EVENT_REQUESTWITHDRAW_MULTI = "request.withdraw.multi";
	public static final String EVENT_REQUESTWITHDRAW = "request.withdraw";
	public static final String EVENT_REQUESTWITHDRAW_ACK = "request.withdraw.ack";
	public static final String EVENT_REQUESTWITHDRAW_NACK = "request.withdraw.nack";
	public static final String EVENT_REQUESTWITHDRAW_OK = "request.withdraw.ok";
	public static final String EVENT_REQUESTWITHDRAW_FAILED = "request.withdraw.failed";
	public static final String EVENT_WITHDRAWATS = "withdraw.ats";

	//*gtc awal
	public static final String EVENT_REQUESTWITHDRAW_GTC = "request.withdraw.gtc";
	public static final String EVENT_REFRESHAMEND_GTC = "refresh.amend.gtc";
	public static final String EVENT_REQUESTWITHDRAWGTC_OK = "request.withdrawgtc.ok";
	public static final String EVENT_CREATEGTCMULTI = "create.gtc.multi";
	public static final String EVENT_CREATEGTC = "create.gtc";// create gtc
	public static final String EVENT_CREATEGTC_OK = "create.gtc.ok";
	public static final String EVENT_WITHDRAWGTC = "withdraw.gtc";// withdraw
	public static final String EVENT_REQUESTAMENDGTC_OK = "request.amendgtc.ok";
																	
	public static final String EVENT_AMMENDGTC = "ammend.gtc";
	public static final String EVENT_ACK_GTC = "ack.gtc";
//*gtc akhir
	public static final String EVENT_CREATEORDER = "create.order";
	public static final String EVENT_CREATEORDER_ACK = "create.order.ack";
	public static final String EVENT_CREATEORDER_NACK = "create.order.nack";
	public static final String EVENT_CREATEORDER_OK = "create.order.ok";
	public static final String EVENT_UPDATE_STATUS = "update.status";
	public static final String EVENT_CREATEORDER_FAILED = "create.order.failed";
	public static final String EVENT_CREATEORDERMULTI = "create.order.order";

	public static final String EVENT_CREATEORDERMULTISPLIT = "create.order.order.multi";
	public static final String EVENT_RECEIVE_RISKMANAGEMENT = "risk.management";

	public static final String EVENT_CREATESCHEDULE = "create.schedule"; // create ATS																		
	public static final String EVENT_ENTRYSUCCESS = "ats.entry.success"; // entry ats success																			
	public static final String EVENT_WITHDRAW_SUCCESS = "ats.withdraw.success"; // withdraw ats success
	public static final String EVENT_EXECUTED = "ats.execute"; // execute success
	public static final String EVENT_WITHDRAW_FAILED = "ats.withdraw.failed";// withdraw ats failed
	public static final String EVENT_ENTRY_FAILED = "ats.entry.failed";// entry ats failed	

	public static final String EVENT_NOTIFICATION = "create.nitification";

	public static final String EVENT_SENDORDER = "send.order";
	public static final String EVENT_SENDORDER_ACK = "send.order.ack";
	public static final String EVENT_SENDORDER_NACK = "send.order.nack";

	public static final String EVENT_DELETEORDER = "delete.order";
	public static final String EVENT_DELETEORDER_ACK = "delete.order.ack";
	public static final String EVENT_DELETEORDER_NACK = "delete.order.nack";
	public static final String EVENT_DELETEORDER_OK = "delete.order.ok";
	public static final String EVENT_DELETEORDER_FAILED = "delete.order.failed";

	public static final String EVENT_RECEIVEORDER = "receive.order";
	public static final String EVENT_RECEIVETRADE = "receive.trade";
	public static final String EVENT_RECEIVENEWORDERREQ = "D";
	public static final String EVENT_RECEIVENEWORDERSPLIT_REPLY = "SPLIT-REPLY";
	public static final String EVENT_RECEIVENEWORDERSPLIT_ENTRY = "SPLIT-ENTRY";
	public static final String EVENT_RECEIVEAMENDREQ = "G";
	public static final String EVENT_RECEIVEWITHDRAWREQ = "F";
	public static final String EVENT_EXECUTION_REPORT = MsgType.EXECUTION_REPORT;
	public static final String EVENT_ORDERMATRIX_REPORT = "ROM"; // report ordermatrix
	public static final String EVENT_CANCEL_REJECT = MsgType.ORDER_CANCEL_REJECT;
	public static final String EVENT_RECEIVEMATCH = "receive.match";
	public static final String EVENT_5A = "5A";
	public static final String EVENT_RECEIVESPLIT = "USO";
	public static final String EVENT_BROADCAST = "BC";
	public static final String EVENT_MARGINCALL = "MC";
	public static final String EVENT_BCMARGINCALL = "BMC";

	public static final String EVENT_RM_ORDER = "rm.order";
	public static final String EVENT_RM_MATCH = "rm.match";

	public static final String EVENT_ACK = "ack";
	public static final String BEGIN_STRING = "FIX.4.0-SINARMAS";
	public static final String EVENT_ADVANDNEGDEALLIST = "advertising.and.negdeallist";
	
	public static final String EVENT_CREATEORDERMATRIX = "create.ordermatrix"; // order matrix
	public static final String EVENT_DELETEORDERMATRIX = "delete.ordermatrix";
	public static final String EVENT_DELETEORDERMATRIXDETIL = "delete.ordermatrix.detil";	
	public static final String EVENT_DELETEORDERMATRIXBYLABEL = "delete.ordermatrix.bylabel";

	private Date sameTime ;
	int a = 0;

	public Date oldsendOrder = new Date();
	private ITradingEngine engine;
	private HashMap map = new HashMap(20, 2);
	private String sessionid = "";


	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
	private static SimpleDateFormat dateFormatAll = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
	private static SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyyMMdd");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	public final static SimpleDateFormat fridayFormat = new SimpleDateFormat("E");
	private static NumberFormat formatter = new DecimalFormat("#,##0 ");
	private HashMap mapAmendOrder = new HashMap();// yosep 18032016

	private Clip sound = null;
	protected Order morder2;
	public TradingEventDispatcher(ITradingEngine engine) {
		this.engine = engine;
		init();
	}

	private String convertStr(Object obj) {
		return obj == null ? "" : (String) obj;
	}

	private Double convertDouble(Object obj) {
		Double result = null;
		if (obj != null) {
			try {
				result = (Double) obj;
			} catch (Exception e) {
				try {
					Long l = (Long) obj;
					result = new Double(l.doubleValue());
				} catch (Exception d) {
					String s = (String) obj;
					result = new Double(Double.parseDouble(s));
				}
			}
		}
		return result;
	}

	private Date convertDate(Object obj) {
		try {
			return obj == null ? null : dateFormat.parse(obj.toString());
		} catch (Exception ex) {
			return null;
		}
	}

	private StringField getTime(int tag, Date data) {
		return new StringField(tag, dateFormat.format(data));
	}

	public void registerEvent(String name, EventHandler handler) {
		map.put(name, handler);
	}


	private void playSound(String file) {
		try {

			// if (sound == null)
			// sound = AudioSystem.getClip();
			
			Clip sound = AudioSystem.getClip();

			sound.open(AudioSystem.getAudioInputStream(getClass().getResource(
					file)));

			// if (!sound.isRunning())
			// sound.start();
			//wait(100);
			sound.addLineListener(new LineListener() {
				
				@Override
				public void update(LineEvent evt) {
					
					//System.out.println(evt.getType()+" "+evt.getSource().getClass());
					
					if(evt.getType() == LineEvent.Type.STOP){
						Clip cl =(Clip) evt.getSource();
						cl.close();
					}
				}
			});
			sound.start();

		} catch (UnsupportedAudioFileException e1) {

			e1.printStackTrace();
		} catch (IOException e2) {

			e2.printStackTrace();
		} catch (LineUnavailableException e3) {

			e3.printStackTrace();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doSplit(JSONObject result) {
		if (result.get("psukses").toString().equals("1")) {
			engine.getEventDispatcher().pushEvent(
					new Event(TradingEventDispatcher.EVENT_DATAACCOUNT, false)
							.setField(TradingEventDispatcher.PARAM_CODE,
									TradingEventDispatcher.EVENT_REFRESHSTATUS)
							.setField(TradingEventDispatcher.PARAM_MESSAGE, "")
							.setField(TradingEventDispatcher.PARAM_CLEAR,
									new Boolean(false))
							.setField(
									TradingEventDispatcher.PARAM_DATA,
									Utils.compress(result.get("pacc")
											.toString().getBytes())));
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

			engine.getEventDispatcher().pushEvent(
					new Event(TradingEventDispatcher.EVENT_DATAMARGIN, false)
							.setField(TradingEventDispatcher.PARAM_CODE,
									TradingEventDispatcher.EVENT_REFRESHSTATUS)
							.setField(TradingEventDispatcher.PARAM_MESSAGE, "")
							.setField(TradingEventDispatcher.PARAM_CLEAR,
									new Boolean(false))
							.setField(
									TradingEventDispatcher.PARAM_DATA,
									Utils.compress(result.get("pacc")
											.toString().getBytes())));
			engine.getEventDispatcher().pushEvent(
					new Event(TradingEventDispatcher.EVENT_DATASCHEDULE, false)
							.setField(TradingEventDispatcher.PARAM_CODE,
									TradingEventDispatcher.EVENT_REFRESHSTATUS)
							.setField(TradingEventDispatcher.PARAM_MESSAGE, "")
							.setField(TradingEventDispatcher.PARAM_CLEAR,
									new Boolean(false))
							.setField(
									TradingEventDispatcher.PARAM_DATA,
									Utils.compress(result.get("pacc")
											.toString().getBytes())));
			engine.getEventDispatcher().pushEvent(
					new Event(TradingEventDispatcher.EVENT_DATABROWSEORDER,
							false)
							.setField(TradingEventDispatcher.PARAM_CODE,
									TradingEventDispatcher.EVENT_REFRESHSTATUS)
							.setField(TradingEventDispatcher.PARAM_MESSAGE, "")
							.setField(TradingEventDispatcher.PARAM_CLEAR,
									new Boolean(false))
							.setField(
									TradingEventDispatcher.PARAM_DATA,
									Utils.compress(result.get("pacc")
											.toString().getBytes())));
			// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

			engine.getEventDispatcher().pushEvent(
					new Event(TradingEventDispatcher.EVENT_DATAORDER, false)
							.setField(TradingEventDispatcher.PARAM_CODE,
									TradingEventDispatcher.EVENT_REFRESHSTATUS)
							.setField(TradingEventDispatcher.PARAM_MESSAGE, "")
							.setField(TradingEventDispatcher.PARAM_CLEAR,
									new Boolean(false))
							.setField(
									TradingEventDispatcher.PARAM_DATA,
									Utils.compress(result.get("porder")
											.toString().getBytes())));
			engine.getEventDispatcher().pushEvent(
					new Event(TradingEventDispatcher.EVENT_DATATRADE, false)
							.setField(TradingEventDispatcher.PARAM_CODE,
									TradingEventDispatcher.EVENT_REFRESHSTATUS)
							.setField(TradingEventDispatcher.PARAM_MESSAGE, "")
							.setField(TradingEventDispatcher.PARAM_CLEAR,
									new Boolean(false))
							.setField(
									TradingEventDispatcher.PARAM_DATA,
									Utils.compress(result.get("pmatch")
											.toString().getBytes())));
		}
	}

	private void init() {
		
		final SimpleDateFormat dt = new SimpleDateFormat("HH:mm:ss");

		map.put(EVENT_NEWSESSIONID, new EventHandler() {
			public void execute(Event event) {
				//log.info("execute EVENT_NEWSESSIONID");
				sessionid = ""
						+ ((Long) event.getParam().get(
								TradingEventDispatcher.PARAM_SESSIONID))
								.longValue();
				OrderIDGenerator.setSessionid(sessionid);
				log.info("new SESSIONID after connect/reconnect: " + sessionid);
			}
		});

		map.put(EVENT_TIMESERVER, new EventHandler() {
			public void execute(Event event) {
				log.info("execute EVENT_TIMESERVER");
				OrderIDGenerator.setTime(new Date(((Long) event.getParam().get(
						PARAM_TIMESERVER)).longValue()));
			}
		});
		
		map.put(EVENT_5A, new EventHandler() {
			public void execute(Event event) {
				Message msg = (Message) event.getParam().get(PARAM_FIX);
				try {
					if (msg.getDouble(999927) == engine.getConnection()
							.getSessionid()
							&& msg.getString(58).startsWith("killed")) {
						Event e = new Event(EVENT_KILL);
						pushEvent(e);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});

		map.put(EVENT_KILL, new EventHandler() {
			public void execute(Event event) {
				log.info("execute EVENT_KILL");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.killed((String) event.getField(PARAM_MESSAGE));
				}
			}
		});

		map.put(EVENT_LOGIN, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_LOGIN");
				engine.getConnection().logon(
						(String) param.getParam().get(PARAM_USERID),
						(String) param.getParam().get(PARAM_PASSWORD));
			}
		});
		map.put(EVENT_LOGINOK, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_LOGINOK");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.loginOK();
				}
			}
		});
		map.put(EVENT_LOGINFAILED, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_LOGINFAILED "+ param.getParam().get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.loginFailed((String) param.getParam().get(
							PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_LOGINSTATUS, new EventHandler() {
			public void execute(Event param) {
			log.info("execute EVENT_LOGINSTATUS "+ param.getParam().get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.loginStatus((String) param.getParam().get(
							PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_RECONNECT, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_RECONNECT");
				engine.getConnection().reconnect(
						((Integer) param.getParam().get(PARAM_COUNTER))
								.intValue());
			}
		});
		map.put(EVENT_RECONNECTSTATUS, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_RECONNECTSTATUS");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.reconnectStatus((String) param.getParam().get(
							PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_RECONNECTOK, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_RECONNECTOK");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.reconnectOK();
				}
			}
		});
		map.put(EVENT_RECONNECTFAILED, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_RECONNECTFAILED "
						+ param.getParam().get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.reconnectFailed((String) param.getParam().get(
							PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_CHGPASSWD, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_CHGPASSWD ");
				engine.getConnection().changePassword(
						(String) param.getParam().get(PARAM_OLDPASSWORD),
						(String) param.getParam().get(PARAM_PASSWORD));
			}
		});
		map.put(EVENT_CHGPASSWDOK, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_CHGPASSWDOK");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.changePwdOK();
				}
			}
		});
		map.put(EVENT_CHGPASSWDFAILED, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_CHGPASSWDFAILED "+ param.getParam().get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.changePwdFailed((String) param.getParam().get(
							PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_CHGPIN, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_CHGPIN ");
				engine.getConnection().changePIN(
						(String) param.getParam().get(PARAM_OLDPIN),
						(String) param.getParam().get(PARAM_PIN));
			}
		});

		map.put(EVENT_CHGPINOK, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_CHGPINOK");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.changePINOK();
				}
			}
		});
		map.put(EVENT_CHGPINFAILED, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_CHGPINFAILED "
						+ param.getParam().get(PARAM_MESSAGE));*/
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.changePINFailed((String) param.getParam().get(
							PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_CHECKPIN, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_CHECKPIN");
				engine.getConnection().checkPIN(
						(String) param.getParam().get(PARAM_USERID),
						(String) param.getParam().get(PARAM_PIN));
			}
		});
		map.put(EVENT_CHECKPINSLS, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_CHECKPINSLS");
				engine.getConnection().checkPINSLS(
						(String) param.getParam().get(PARAM_USERID),
						(String) param.getParam().get(PARAM_PIN));
			}
		});

		map.put(EVENT_CHECKPINOK, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_CHECKPINOK");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.checkPINOK();
				}
			}
		});
		map.put(EVENT_CHECKPINFAILED, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_CHECKPINFAILED"
						+ param.getParam().get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.checkPINFailed((String) param.getParam().get(
							PARAM_MESSAGE));
				}
			}
		});
		
		map.put(EVENT_CHECKPIN_NEGO, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_CHECKPIN_NEGO");
				engine.getConnection().checkPIN_NEGO(
						(String) param.getParam().get(PARAM_PIN),
						(String) param.getParam().get(PARAM_FORM));
			}
		});

		map.put(EVENT_CHECKPIN_NEGO_OK, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_CHECKPIN_NEGO_OK");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.checkPIN_NEGO_OK((String) param.getParam().get(PARAM_FORM));
				}
			}
		});
		map.put(EVENT_CHECKPIN_NEGO_FAILED, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_CHECKPIN_NEGO_FAILED"
						+ param.getParam().get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.checkPIN_NEGO_FAILED((String) param.getParam().get(PARAM_MESSAGE));
				}
			}
		});
		
		//*gtc awal
		map.put(EVENT_REFRESH_GTC, new EventHandler() { 
			public void execute(Event param) {
				log.info("execute EVENT_REFRESHGTC");
				/*//log.info("DATA = "+param.getParam().get(PARAM_DATA).toString());
				//log.info("DATA2 = "+param.getParam().get(PARAM_CODE).toString());*/
				engine.getConnection().refreshGTC(
						param.getParam().get(PARAM_ACCOUNTID).toString(),
						param.getParam().get(PARAM_ORDERID).toString());
				
			}
		});

		map.put(EVENT_REFRESHAMEND_GTC, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_REFRESHAMEND_GTC");
				engine.getConnection().refreshGTC(
						param.getParam().get(PARAM_ACCOUNTID).toString(),
						param.getParam().get(PARAM_ORDERID).toString());
			}
		});
		//*gtc akhir
		map.put(EVENT_DISCONNECT, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DISCONNECT ");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.disconnected();
				}
			}
		});
		map.put(EVENT_REFRESH, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESH");
				engine.getConnection().refreshData(
						((Boolean) param.getParam().get("MASTER"))
								.booleanValue(),
						((Boolean) param.getParam().get("ACCOUNT"))
								.booleanValue(),
						((Boolean) param.getParam().get("MARGIN"))
								.booleanValue(),
						((Boolean) param.getParam().get("PF")).booleanValue(),
						((Boolean) param.getParam().get("ORDER"))
								.booleanValue());
			}
		});
		map.put(EVENT_REFRESH_BYID, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESH BY ID");
				engine.getConnection().refreshData(
						param.getParam().get("TRADINGID").toString());
			}
		});
		map.put(EVENT_REFRESH_ORDER, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_REFRESHORDER");
				engine.getConnection().refreshOrder(
						param.getParam().get(PARAM_ACCOUNTID).toString(),
						param.getParam().get(PARAM_ORDERID).toString(),
						param.getParam().get(PARAM_STOCK).toString());
			}
		});
		map.put(EVENT_REFRESH_TRADE, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHTRADE");
				engine.getConnection().refreshTrade(
						param.getParam().get(PARAM_ORDERID).toString());
			}
		});
		map.put(EVENT_REFRESH_PORTFOLIO, new EventHandler() {
			public void execute(Event param) {
			//	log.info("execute EVENT_REFRESHPORTFOLIO");
				engine.getConnection().refreshPortfolio(
						param.getParam().get(PARAM_ACCOUNTID).toString(),
						param.getParam().get(PARAM_STOCK).toString());
			}
		});
		map.put(EVENT_REFRESH_ACCOUNT, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHACCOUNT");
				engine.getConnection().refreshAccount(
						param.getParam().get(PARAM_ACCOUNTID).toString());
			}
		});
		map.put(EVENT_REFRESH_ATS, new EventHandler() {


			@Override
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHATS");
				engine.getConnection().refreshAts(
						param.getParam().get(PARAM_ACCOUNTID).toString());
			}
		});
		map.put(EVENT_REFRESH_BROWSEATS, new EventHandler() {


			@Override
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHBROWESATS");
				engine.getConnection().refreshBrowseAts(
						param.getParam().get(PARAM_ACCOUNTID).toString());
			}
		});
		map.put(EVENT_REFRESH_CASHCOLL, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHCASHCOLL");
				engine.getConnection().refreshCashColl(
						param.getParam().get(PARAM_TRADINGID).toString());
			}
		});
		map.put(EVENT_REFRESH_BUYSELLDETAIL, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHBUYSELLDETAIL");
				engine.getConnection().refreshBuySellDetail(
						param.getParam().get(PARAM_TRADINGID).toString(),
						param.getParam().get(PARAM_ACCOUNTID).toString());
				// ((Integer)param.getParam().get(PARAM_BUYORSELL)).intValue());
			}
		});
		
		map.put(EVENT_REFRESH_DUEDATE, new EventHandler() {


			@Override
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHDUEDATE");
				engine.getConnection().refreshDueDate(
						param.getParam().get(PARAM_TRADINGID).toString());
				
			}
		});
		map.put(EVENT_REFRESH_LOADORDERMATRIX, new EventHandler() {
			
			@Override
			public void execute(Event param) {
				a++;
				//System.out.println(a+" - ");
				log.info("execute EVENT_REFRESHORDERMATRIX ");
				engine.getConnection().refreshOrderMatrix(param.getParam().get(PARAM_TRADINGID).toString());
				
			}
		});
		map.put(EVENT_REFRESH_LOADORDERMATRIXLIST, new EventHandler() {

			
			@Override
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHORDERMATRIXLIST ");
				engine.getConnection().refreshOrderMatrixList(
						param.getParam().get(PARAM_TRADINGID).toString(),
						param.getParam().get(PARAM_USERID).toString());

				
			}
		});
		map.put(EVENT_REFRESH_BACKNOTIF, new EventHandler() {


			@Override
			public void execute(Event param) {
				//log.info("execute EVENT_NOTIF");
				engine.getConnection().refreshNotif(
						param.getParam().get(PARAM_TRADINGID).toString());


			}
		});
		map.put(EVENT_REFRESHOK, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHOK");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.refreshOK();
				}
			}
		});

		map.put(EVENT_REFRESHPORTFOLIO, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHPORTFOLIO");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.refreshOKportfolio();
				}
			}
		});
		map.put(EVENT_REFRESHACCOUNTID, new EventHandler() {
			
			@Override
			public void execute(Event param) {
				//log.info("exectute EVENT_REFRESHACCOUNTID");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.refreshOKmargin();
				}
			}
		});
		map.put(EVENT_REFRESHFAILED, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHFAILED "+ param.getParam().get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.refreshFailed((String) param.getParam().get(
							PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_REFRESHSTATUS, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_REFRESHSTATUS "+ param.getParam().get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					ITradingListener listener = (ITradingListener) engine
							.getEventListener().elementAt(i);
					listener.refreshStatus((String) param.getParam().get(
							PARAM_MESSAGE));
				}
			}
		});

		map.put(EVENT_LOGOUT, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_LOGOUT");
				engine.getConnection().logout();
				engine.getDataStore().clear();
			}
		});

		map.put(EVENT_DATAACCOUNTTYPE, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAACCOUNTTYPE");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] accType = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (accType != null) {
					engine.getDataStore().get(TradingStore.DATA_ACCOUNTTYPE)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_ACCOUNTTYPE)
							.refresh();
					String s = new String(Utils.decompress(accType));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						AccType acc = new AccType();
						acc.setId(s1.get("acctype").toString());
						acc.setName(s1.get("acctypename").toString());
						Vector vorder = new Vector(1);
						vorder.addElement(AccTypeDef.createTableRow(acc));
						engine.getDataStore()
								.get(TradingStore.DATA_ACCOUNTTYPE)
								.addRow(vorder, false, false);
						AccountRender.hashAccType.put(acc.getId(),
								acc.getName());
					}
				}
			}
		});
		map.put(EVENT_DATAINVTYPE, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAINVTYPE");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_INVTYPE)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_INVTYPE)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						InvType inv = new InvType();
						inv.setId(s1.get("invtype").toString());
						inv.setName(s1.get("invtypename").toString());
						Vector vorder = new Vector(1);
						vorder.addElement(InvTypeDef.createTableRow(inv));
						engine.getDataStore().get(TradingStore.DATA_INVTYPE)
								.addRow(vorder, false, false);
						AccountRender.hashInvType.put(inv.getId(),
								inv.getName());
					}
				}
			}
		});
		map.put(EVENT_DATAUSERTYPE, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAUSERTYPE");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_USERTYPE)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_USERTYPE)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						UserType usr = new UserType();
						usr.setId(s1.get("usertype").toString());
						usr.setName(s1.get("usertypename").toString());
						Vector vorder = new Vector(1);
						vorder.addElement(UserTypeDef.createTableRow(usr));
						engine.getDataStore().get(TradingStore.DATA_USERTYPE)
								.addRow(vorder, false, false);
					}
				}
			}
		});
		map.put(EVENT_DATACUSTTYPE, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATACUSTYPE");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_CUSTTYPE)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_CUSTTYPE)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						CustType usr = new CustType();
						usr.setId(s1.get("custtype").toString());
						usr.setName(s1.get("custtypename").toString());
						Vector vorder = new Vector(1);
						vorder.addElement(CustTypeDef.createTableRow(usr));
						engine.getDataStore().get(TradingStore.DATA_CUSTTYPE)
								.addRow(vorder, false, false);
						AccountRender.hashCustType.put(usr.getId(),
								usr.getName());
					}
				}
			}
		});
				
		map.put(EVENT_DATAEXCHANGE, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAEXCHANGE");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_EXCHANGE)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_EXCHANGE)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						Exchange exch = new Exchange();
						exch.setId(s1.get("xchid").toString());
						exch.setName(s1.get("xchname").toString());
						try {
							exch.setPreopTime(dt.parse(s1.get("preopeningtime")
									.toString()));
							exch.setSess1Time(dt.parse(s1.get("session1time")
									.toString()));
							exch.setSess2Time(dt.parse(s1.get("session2time")
									.toString()));
							exch.setSess2FridayTime(dt.parse(s1.get(
									"session2timefriday").toString()));
						} catch (Exception ex) {
						}
						Vector vorder = new Vector(1);
						vorder.addElement(ExchangeDef.createTableRow(exch));
						engine.getDataStore().get(TradingStore.DATA_EXCHANGE)
								.addRow(vorder, false, false);
					}
				}
			}
		});
		map.put(EVENT_DATABOARD, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATABOARD");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_BOARD)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_BOARD)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						Board brd = new Board();
						brd.setId(s1.get("brdid").toString());
						brd.setName(s1.get("brdname").toString());
						Vector vorder = new Vector(1);
						vorder.addElement(BoardDef.createTableRow(brd));
						engine.getDataStore().get(TradingStore.DATA_BOARD)
								.addRow(vorder, false, false);
					}
				}
			}
		});
		map.put(EVENT_DATABROKER, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATABROKER");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_BROKER)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_BROKER)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						Broker brd = new Broker();
						brd.setId(s1.get("brokerid").toString());
						brd.setName(s1.get("brokername").toString());
						Vector vorder = new Vector(1);
						vorder.addElement(BrokerDef.createTableRow(brd));
						engine.getDataStore().get(TradingStore.DATA_BROKER)
								.addRow(vorder, false, false);
					}
				}
			}
		});
		map.put(EVENT_DATACASHCOLL, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_CASHCOLL");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_CASHCOLL)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_CASHCOLL)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						CashColl cash = new CashColl();
						cash.setAccID(s1.get("accid").toString());
						cash.setTradingID(s1.get("tradingid").toString());
						cash.setCashID(s1.get("cashid").toString());
						cash.setCashName(s1.get("cashname").toString());
						cash.setBegValue(convertDouble(s1.get("begvalue")));
						cash.setValue(convertDouble(s1.get("value")));
						Vector vorder = new Vector(1);
						vorder.addElement(CashCollDef.createTableRow(cash));
						engine.getDataStore().get(TradingStore.DATA_CASHCOLL)
								.addRow(vorder, false, false);
					}
				}
			}
		});		
		map.put(EVENT_DATABUYSELLDETAIL, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_BUYSELLDETAIL");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_BUYSELLDETAIL)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_BUYSELLDETAIL)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						// log.info(s1.toString());
						BuySellDetail bs = new BuySellDetail();
						bs.setTradingID(s1.get("custid").toString());
						bs.setAccID(s1.get("accid").toString());
						bs.setBuyOrSell(s1.get("buyorsell").toString());
						bs.setTSpan(s1.get("valtype").toString());
						bs.setSpanDate(s1.get("valdate").toString());
						bs.setValue(convertDouble(s1.get("curvalue")));
						Vector vorder = new Vector(1);
						vorder.addElement(BuySellDetailDef.createTableRow(bs));
						engine.getDataStore()
								.get(TradingStore.DATA_BUYSELLDETAIL)
								.addRow(vorder, false, false);
					}
				}
			}
		});		
		map.put(EVENT_DATADUEDATE, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATADUEDATE......++++");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_DUEDATE)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_DUEDATE)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					//System.out.println("-------");
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						//log.info(s1.toString());
						double net;
						DueDate dt = new DueDate();


						dt.setTradingID(convertStr(s1.get("custid")));
						dt.setAccID(convertStr(s1.get("accid")));
						dt.setTSpan(convertStr(s1.get("valtype")));
						dt.setBuy(convertDouble(s1.get("buy")));// //
						dt.setSell(convertDouble(s1.get("sell")));


						// dt.setBuyOrSell(convertStr(s1.get("buyorsell")));
						// dt.setValue(convertDouble(s1.get("curvalue")));
						// dt.setSpanDate(convertStr(s1.get("valdate")));
						net = (new Double(convertDouble(s1.get("sell"))
								- convertDouble(s1.get("buy"))));
						dt.setNet(convertDouble(net));
						Vector vorder = new Vector(1);
						vorder.addElement(DueDateDef.createTableRow(dt));
						engine.getDataStore().get(TradingStore.DATA_DUEDATE)
								.addRow(vorder, false, false);
						//System.out.println("Data Tampil +++++" + vorder);
					}
				}
			}
		});

		map.put(EVENT_DATASTOCK, new EventHandler() {
			public void execute(Event param) {
			//	log.info("execute EVENT_DATASTOCK");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_STOCK)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_STOCK)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						Stock stk = new Stock();
						stk.setSecurities(s1.get("secid").toString());
						stk.setName(s1.get("secname").toString());
						stk.setLotSize(convertDouble(s1.get("seclotsize")));
						stk.setClosingPrice(convertDouble(s1
								.get("closingprice")));
						stk.setLastPrice(convertDouble(s1.get("marketprice")));
						stk.setPreopening(s1.get("ispreopening").toString()
								.equals("1"));
						Vector vorder = new Vector(1);
						vorder.addElement(StockDef.createTableRow(stk));
						engine.getDataStore().get(TradingStore.DATA_STOCK)
								.addRow(vorder, false, false);
					}
				}
			}
		});

		map.put(EVENT_DATAACCSTATUS, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAACCSTATUS");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_ACCSTATUS)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_ACCSTATUS)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						AccStatus stk = new AccStatus();
						stk.setId(s1.get("accstatus").toString());
						stk.setName(s1.get("accstatusname").toString());
						Vector vorder = new Vector(1);
						vorder.addElement(AccStatusDef.createTableRow(stk));
						engine.getDataStore().get(TradingStore.DATA_ACCSTATUS)
								.addRow(vorder, false, false);
						AccountRender.hashStatus.put(stk.getId(), stk.getName());
					}
				}
			}
		});
		map.put(EVENT_DATAORDERSTATUS, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAORDERSTATUS");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						OrderStatus stk = new OrderStatus();
						stk.setId(s1.get("statusid").toString());
						stk.setName(s1.get("statusname").toString());
						Vector vorder = new Vector(1);
						vorder.addElement(OrderStatusDef.createTableRow(stk));
						engine.getDataStore()
								.get(TradingStore.DATA_ORDERSTATUS)
								.addRow(vorder, false, false);
						OrderRender.hashStatus.put(stk.getId(), stk.getName());
						GTCRender.hashStatus.put(stk.getId(), stk.getName());//*gtc
					}
				}
			}
		});
		map.put(EVENT_DATAGTC, new EventHandler() {//*gtc
			public void execute(Event param) {
				log.info("execute EVENT_DATAGTC");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] announcement = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (announcement != null) {
					engine.getDataStore().get(TradingStore.DATA_GTC)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_GTC).refresh();
					String s = new String(Utils.decompress(announcement));
					//log.info("GTC" + s);
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					final Vector vorder = new Vector(100, 10);
					for (int j = 0; j < i; j++) {
//						try{
						JSONObject s1 = (JSONObject) array.get(j);
						Order gtc = new Order();
						gtc.setEntryTime(convertDate(s1.get("entryrttime")));
						gtc.setId(convertStr(s1.get("orderid")));
						gtc.setTradingId(convertStr(s1.get("tradingid")));
						gtc.setBOS(convertStr(s1.get("buyorsell")));
						gtc.setStock(convertStr(s1.get("secid")));
						gtc.setLot(convertDouble(s1.get("lot")));
						gtc.setPrice(convertDouble(s1.get("price")));
						gtc.setVolume(convertDouble(s1.get("volume")));
						gtc.setValiduntil(convertDate(s1.get("validuntil")));
						gtc.setEntryBy(convertStr(s1.get("entryrtby")));
						gtc.setStatus(convertStr(s1.get("statusid")));
						gtc.setBoard(convertStr(s1.get("brdid")));
						gtc.setLastUpdate(convertDate(s1.get("last_update")));
						gtc.setWithdrawBy(convertStr(s1.get("withdrawby")));
						gtc.setWithdrawTime(convertDate(s1.get("withdrawtime")));
						gtc.setWithdrawTerminal(convertStr(s1.get("withdrawterminal")));
						gtc.setEntryTerminal(convertStr(s1.get("entryrtterminal")));
						gtc.setDoneVol(convertDouble(s1.get("donevolume")));
						gtc.calculate();
						 
						Vector vorder1 = new Vector(1);
						vorder1.addElement(GTCDef.createTableRow(gtc));
						engine.getDataStore().get(TradingStore.DATA_GTC)
								.addRow(vorder1, false, false);
//						}catch (Exception ex) {
//								ex.printStackTrace();
//								Utils.logException(ex);
//							}
					}
				}
				
			}
		});
		map.put(EVENT_DATAACCSTOCK, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAACCSTOCK");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_ACCSTOCK)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_ACCSTOCK)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						AccStock stk = new AccStock();
						stk.setAccType(s1.get("acctype").toString());
						stk.setStock(s1.get("secid").toString());
						stk.setHaircut(s1.get("haircut").toString());
						Vector vorder = new Vector(1);
						vorder.addElement(AccStockDef.createTableRow(stk));
						engine.getDataStore().get(TradingStore.DATA_ACCSTOCK)
								.addRow(vorder, false, false);
					}
				}
			}
		});
		map.put(EVENT_DATAUSERPROFILE, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAUSERPROFILE");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					engine.getDataStore().get(TradingStore.DATA_USERPROFILE)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_USERPROFILE)
							.refresh();
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
//					System.out.println("profilenaaaaaaaaaaa: \n"+ array.toString());
					//log.info("profile:" + array.toString());

					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						UserProfile stk = new UserProfile();
						stk.setUserId(s1.get("userid").toString());
						stk.setProfileId(s1.get("profileid").toString());
						stk.setMenuId(s1.get("menuid").toString());
						stk.setIsAllowed(s1.get("isallowed").toString());
						stk.setUsertype(s1.get("usertype").toString());
						Vector vorder = new Vector(1);
						vorder.addElement(UserProfileDef.createTableRow(stk));
						engine.getDataStore()
								.get(TradingStore.DATA_USERPROFILE)
								.addRow(vorder, false, false);

					}
					UserProfile sales = (UserProfile) engine
							.getDataStore()
							.get(TradingStore.DATA_USERPROFILE)
							.getDataByField(new Object[] { "40" },
									new int[] { UserProfile.C_MENUID });
					
					if(sales
					.getProfileId().toUpperCase().equals("SUPERUSER")){
						engine.getConnection().setFilter(true);
					}
					
				}
			}
		});
		map.put(EVENT_DATAORDER, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAORDER");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				boolean clear = ((Boolean) param
						.getField(TradingEventDispatcher.PARAM_CLEAR))
						.booleanValue();
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				
				if (data != null) {
					if (clear) {
						engine.getDataStore().get(TradingStore.DATA_ORDER).getDataVector().clear();
						engine.getDataStore().get(TradingStore.DATA_ORDER).refresh();
						/*log.info("DATA ORDER "
								+ engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.getRowCount());*/
					}
					String s = new String(Utils.decompress(data));
//					log.info("data order:"+s);					
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					final Vector vorder = new Vector(100, 10);
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						Order ord = new Order();
						
						ord.setId(s1.get("orderid").toString());
						ord.setOrderDate(convertDate(s1.get("orderdate")));
						ord.setExchange(convertStr(s1.get("xchid")));
						ord.setBoard(convertStr(s1.get("brdid")));
						ord.setBroker(convertStr(s1.get("brokerid")));
						ord.setBOS(s1.get("buyorsell").toString());
						ord.setOrdType(convertStr(s1.get("ordtype")));
						ord.setOrderType(convertStr(s1.get("ordertype")));
						ord.setStock(s1.get("secid").toString());
						ord.setIsAdvertising(s1.get("isadvertising").toString());
						ord.setTradingId(convertStr(s1.get("tradingid")));
						ord.setInvType(s1.get("invtype").toString());
						ord.setCurrency(convertStr(s1.get("curid")));
						ord.setContraBroker(convertStr(s1.get("contrabrokerid")));
						ord.setContraUser(convertStr(s1.get("contrauserid")));
						ord.setPrice(convertDouble(s1.get("price")));
						ord.setLot(convertDouble(s1.get("lot")));
						ord.setVolume(convertDouble(s1.get("volume")));
						if (s1.get("isbasketorder") != null) {
							ord.setIsBasketOrder(s1.get("isbasketorder").toString());
						}
						ord.setBasketTime(convertDate(s1.get("basketsendtime")));
						ord.setStatus(s1.get("statusid").toString());
						ord.setTLBefore(convertDouble(s1.get("tlbefore")));
						ord.setPortBefore(convertDouble(s1.get("avportbefore")));
						ord.setRatioBefore(convertDouble(s1.get("ratiobefore")));
						ord.setIsOverLimit(convertStr(s1.get("isoverlimit")));
						ord.setIsShortSell(convertStr(s1.get("isshortsell")));
						ord.setMarketOrderId(convertStr(s1.get("marketorderid")));
						ord.setDoneVol(convertDouble(s1.get("donevolume")));
						ord.setPrevDoneVolume(convertDouble(s1.get("prevdonevolume")));
						ord.setPrevOrderId(convertStr(s1.get("prevorderid")));
						ord.setNextOrderId(convertStr(s1.get("nextoderid")));
						ord.setPrevMarketOrderId(convertStr(s1.get("prevmarketorderid")));
						ord.setNextMarketOrderId(convertStr(s1.get("nextmarketorderid")));
						ord.setEntryTime(convertDate(s1.get("entryrttime")));
						ord.setEntryBy(convertStr(s1.get("entryrtby")));
						ord.setEntryTerminal(convertStr(s1.get("entryrtterminal")));
						ord.setRejectTime(convertDate(s1.get("rejecttime")));
						ord.setOpenTime(convertDate(s1.get("opentime")));
						ord.setOpenSendTime(convertDate(s1.get("opensendtime")));
						ord.setAmendTime(convertDate(s1.get("amendtime")));
						ord.setAmendBy(convertStr(s1.get("amendby")));
						ord.setAmendTerminal(convertStr(s1.get("amendterminal")));
						ord.setAmendSendTime(convertDate(s1.get("amendsendtime")));
						ord.setWithdrawTime(convertDate(s1.get("withdrawtime")));
						ord.setWithdrawBy(convertStr(s1.get("withdrawby")));
						ord.setWithdrawTerminal(convertStr(s1.get("withdrawterminal")));
						ord.setWithdrawSendTime(convertDate(s1.get("withdrawsendtime")));
						ord.setSplitTo(convertDouble(s1.get("splitto")));
						ord.setSplitBy(convertStr(s1.get("splitby")));
						ord.setSplitTerminal(convertStr(s1.get("splitterminal")));
						ord.setMatchCounter(convertDouble(s1.get("matchcounter")));
						ord.setMatchTime(convertDate(s1.get("matchtime")));
						ord.setLQValueBefore(convertDouble(s1.get("lqvaluebefore")));
						ord.setCounter(convertDouble(s1.get("counter")));
						ord.setTotalcounterSplit(convertDouble(s1.get("totalcountersplit")));
						ord.setNote(convertStr(s1.get("note")));
						ord.setComplianceId(convertStr(s1.get("complianceid")));
						ord.setUpdateDate(convertDate(s1.get("updatedate")));
						ord.setUpdateBy(convertStr(s1.get("updateby")));
						ord.setTerminalId(convertStr(s1.get("terminalid")));
						ord.setIsFloor(s1.get("isfloor").toString());
						ord.setMyAccount(s1.get("myacc").toString().equals("1"));
						ord.setAccId(s1.get("accid").toString());
						ord.setTradeNo("0");
						ord.setNegDealRef(convertStr(s1.get("marketordidnegdealconfirm")));
						ord.setOrderIDContra(convertStr(s1.get("orderidcontra")));
						ord.setAccIDContra(convertStr(s1.get("accidcontra")));
						ord.setInvTypeContra(convertStr(s1.get("invtypecontra")));
						ord.calculate();
						if (!clear) {
							final Vector vorder2 = new Vector(1);
							vorder2.addElement(OrderDef.createTableRow(ord));
							engine.getDataStore().get(TradingStore.DATA_ORDER).addRow(vorder2, false, false);							

						} else {
							vorder.addElement(OrderDef.createTableRow(ord));
						}
					}
					if (clear && vorder.size() > 0) {
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								engine.getDataStore().get(TradingStore.DATA_ORDER).addRow(vorder, false, false);
								engine.getDataStore().get(TradingStore.DATA_ORDER).refresh();
							}
						});							
					}
				}
			}
		});
		
		map.put(EVENT_ORDERMATRIX,new EventHandler() {
			
			@Override
			public void execute(Event param) {
				log.info("execute EVENT_ORDERMATRIX");
				String eventName = (String)param.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String)param.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[])param.getField(TradingEventDispatcher.PARAM_DATA);
				boolean clear = ((Boolean) param
						.getField(TradingEventDispatcher.PARAM_CLEAR))
						.booleanValue();
				
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				if(data!=null){					
					String s = new String(Utils.decompress(data));
					//log.info("data s trading eventdispatcher : "+s);
					JSONArray array = (JSONArray)JSONValue.parse(s);
					int i = array.size();
					Vector vorder = new Vector(100,10);
					for (int j = 0; j <i; j++){
						JSONObject s1 = (JSONObject)array.get(j);
						OrderMatrix oMatrix = new OrderMatrix();
						oMatrix.setLabel(convertStr(s1.get("label")));
						oMatrix.setLastedit(convertDate(s1.get("lastedit")));
						oMatrix.setTbuyamount(convertDouble(s1.get("TAmountBuy")));
						oMatrix.setTsellamount(convertDouble(s1.get("TAmountSell")));
						oMatrix.setIsBasket(convertStr(s1.get("isbasketorder")));
						oMatrix.setBasketTime(convertStr(s1.get("basketsendtime")));
						if (!clear) {
							vorder = new Vector(1);
							vorder.addElement(OrderMatrixDef.createTableRow(oMatrix));
							engine.getDataStore().get(TradingStore.DATA_ORDERMATRIXLIST).addRow(vorder, false, true);
						} else {
							vorder.addElement(OrderMatrixDef.createTableRow(oMatrix));
						}
					}
					if (clear && vorder.size() > 0) {
						engine.getDataStore().get(TradingStore.DATA_ORDERMATRIXLIST).addRow(vorder, false, false);
						engine.getDataStore().get(TradingStore.DATA_ORDERMATRIXLIST).refresh();
					}
				}
			}
		});
		
		map.put(EVENT_ORDERMATRIXLIST,new EventHandler() {			
			@Override
			public void execute(Event param) {
				log.info("execute EVENT_ORDERMATRIXLIST");
				String eventName = (String)param.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String)param.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[])param.getField(TradingEventDispatcher.PARAM_DATA);
				boolean clear = ((Boolean) param.getField(TradingEventDispatcher.PARAM_CLEAR)).booleanValue();
				
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				if(data!=null){
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray)JSONValue.parse(s);
					int i = array.size();
					Vector vorder = new Vector(100,10);
					for (int j = 0; j <i; j++){
						JSONObject s1 = (JSONObject)array.get(j);
						OrderMatrix oMatrix = new OrderMatrix();						
						oMatrix.setLabel(convertStr(s1.get("label")));
						oMatrix.setBuyorsell(convertStr(s1.get("buyorsell")));
						oMatrix.setSecid(convertStr(s1.get("secid") ));
						oMatrix.setPrice(convertDouble(s1.get("price")));
						oMatrix.setLot(convertDouble(s1.get("lot")));
						oMatrix.setVolume(convertDouble(s1.get("volume")));
						oMatrix.setClient(convertStr(s1.get("clientid")));
						oMatrix.setLabel(convertStr(s1.get("label")));
						oMatrix.setOrdermatrixid(convertStr(s1.get("ordermatrixid")));
						oMatrix.setIsBasket(convertStr(s1.get("isbasket")));
						oMatrix.setBasketTime(convertStr(s1.get("basketsendtime")));						
						
						oMatrix.buildAmount();
						if (!clear) {
							vorder = new Vector(1);
							vorder.addElement(OrderMatrixDef.createTableRow(oMatrix));
							engine.getDataStore().get(TradingStore.DATA_ORDERMATRIX).addRow(vorder, false, true);
						} else {
							vorder.addElement(OrderMatrixDef.createTableRow(oMatrix));
						}
					}
					if (clear && vorder.size() > 0) {
						engine.getDataStore().get(TradingStore.DATA_ORDERMATRIX).addRow(vorder, false, false);
						engine.getDataStore().get(TradingStore.DATA_ORDERMATRIX).refresh();
					}
				}
			}
		});
		
		map.put(EVENT_DATATRADE, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATATRADE");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				boolean clear = ((Boolean) param
						.getField(TradingEventDispatcher.PARAM_CLEAR))
						.booleanValue();
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {


					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					final Vector vtrade = new Vector(100, 10);

					//log.info("data trade " + clear + " " + s);

					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						final Order trade = new Order();
						trade.setExchange(convertStr(s1.get("xchid")));
						trade.setTradeNo(convertStr(s1.get("markettradeid")));
						// System.out.println(convertStr(s1.get("markettradeid"))
						// + " ==");
						trade.setMarketOrderId(convertStr(s1
								.get("marketorderid")));
						trade.setMarketRef(convertStr(s1.get("marketref")));
						trade.setMatchTime(convertDate(s1.get("tradedate")));
						trade.setEntryTime(convertDate(s1.get("ordertime")));
						trade.setStatus(convertStr(s1.get("statusid")));
						trade.setEntryBy(convertStr(s1.get("entryorderby")));
						trade.setTradingId(convertStr(s1.get("tradingid")));
						trade.setId(convertStr(s1.get("orderid")));
						trade.setInvType(convertStr(s1.get("invtype")));
						trade.setBoard(convertStr(s1.get("brdid")));
						trade.setBroker(convertStr(s1.get("brokerid")));
						trade.setContraBroker(convertStr(s1.get("contrabrokerid")));
						trade.setContraUser(convertStr(s1.get("contramarketuserid")));
						trade.setStock(convertStr(s1.get("secid")));
						trade.setBOS(convertStr(s1.get("buyorsell")));
						trade.setPrice(convertDouble(s1.get("price")));
						trade.setLot(convertDouble(s1.get("lot")));
						trade.setVolume(convertDouble(s1.get("volume")));
						trade.setComplianceId(convertStr(s1.get("complianceid")));
						trade.setUpdateDate(convertDate(s1.get("updatedate")));
						trade.setUpdateBy(convertStr(s1.get("updateby")));
						trade.setTerminalId(convertStr(s1.get("entryrtterminal")));
						trade.setMyAccount(s1.get("myacc").toString().equals("1"));
						trade.setIsFloor(convertStr(s1.get("isfloor")));
						if (trade.getIsFloor().equals(""))
							trade.setIsFloor("0");
						trade.calculate();
						// vorder.addElement(OrderDef.createTableRow(trade));

						if (!clear) {
							try {
								SwingUtilities.invokeAndWait(new Runnable() {

									@Override
									public void run() {
										

										Vector vord = new Vector(1);
										vord.addElement(OrderDef
												.createTableRow(trade));

										engine.getDataStore()
												.get(TradingStore.DATA_ORDER)
												.addRow(vord, false, false);
									}
								});
							} catch (Exception ex) {
								ex.printStackTrace();
								Utils.logException(ex);
							}

						} else {
							// vtrade.addElement(OrderDef.createTableRow(trade));
							vtrade.addElement(trade);
						}
					}
					if (clear && vtrade.size() > 0) {

						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {

								
							//	log.info("execute DATA_TRADE "+vtrade.size());

								for (Object obj : vtrade) {
									Order tr = (Order) obj;

									Vector vord = new Vector(1);
									vord.addElement(OrderDef.createTableRow(tr));

									engine.getDataStore()
											.get(TradingStore.DATA_ORDER)
											.addRow(vord, false, false);
								}
								engine.getDataStore()
										.get(TradingStore.DATA_ORDER).refresh();


							}

						});

					}
				}
			}
		});
				
		map.put(EVENT_DATAPORTFOLIO, new EventHandler() {
			public void execute(Event param) {
			//	log.info("execute EVENT_DATAPORTFOLIO");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				boolean clear = ((Boolean) param
						.getField(TradingEventDispatcher.PARAM_CLEAR))
						.booleanValue();

				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					//System.out.println(clear + " portfolio ");
					if (clear) {
						engine.getDataStore().get(TradingStore.DATA_PORTFOLIO)
								.getDataVector().clear();
						engine.getDataStore().get(TradingStore.DATA_PORTFOLIO)
								.refresh();
					}
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					Vector vorder = new Vector(100, 10);
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						Portfolio pf = new Portfolio();
						pf.setTradingId(s1.get("tradingid").toString());
						pf.setStock(s1.get("secid").toString());
						pf.setBegVolume(convertDouble(s1.get("begvolume")));
						pf.setAvailableVolume(convertDouble(s1
								.get("availablevolume")));
						pf.setStockVal(convertDouble(s1.get("stockval")));
						pf.setAvgPrice(convertDouble(s1.get("avgprice")));
						pf.setClosingPrice(convertDouble(s1.get("closingprice")));
						pf.setLastPrice(convertDouble(s1.get("marketprice")));
						pf.setMyAccount(s1.get("myacc").toString().equals("1"));
						pf.setAccountId(s1.get("accid").toString());
						pf.setCurrVolume(convertDouble(s1.get("curvolume")));
						Stock stock = (Stock) engine.getDataStore()
								.get(TradingStore.DATA_STOCK)
								.getDataByKey(new String[] { pf.getStock() });
						pf.setLotSize(new Double(stock != null ? stock
								.getLotSize().doubleValue() : Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE))));

						pf.calculate();


						if (!clear) {
							vorder = new Vector(1);
							vorder.addElement(PortfolioDef.createTableRow(pf));
							engine.getDataStore()
									.get(TradingStore.DATA_PORTFOLIO)
									.addRow(vorder, false, false);
						} else {
							vorder.addElement(PortfolioDef.createTableRow(pf));
						}
					}
					if (clear && vorder.size() > 0) {
						engine.getDataStore().get(TradingStore.DATA_PORTFOLIO)
								.addRow(vorder, true, false);
						engine.getDataStore().get(TradingStore.DATA_PORTFOLIO)
								.refresh();
					}
				}
			}
		});
		
		map.put(EVENT_DATAACCOUNT, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAACCOUNT");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				boolean clear = ((Boolean) param
						.getField(TradingEventDispatcher.PARAM_CLEAR))
						.booleanValue();
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));

				engine.getEventDispatcher()
						.pushEvent(
								new Event(
										TradingEventDispatcher.EVENT_DATAMARGIN,
										true)
										.setField(
												TradingEventDispatcher.PARAM_CODE,
												eventName)
										.setField(
												TradingEventDispatcher.PARAM_MESSAGE,
												"loading margin...")
										.setField(
												TradingEventDispatcher.PARAM_CLEAR,
												new Boolean(clear))
										.setField(
												TradingEventDispatcher.PARAM_DATA,
												data));













				if (data != null) {
					if (clear) {
						engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
								.getDataVector().clear();
						engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
								.refresh();
					}
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					Vector vorder = new Vector(100, 10);
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						double up = 0.5;
						double cell = 0.6;
						double netAc = 0;
						double hasil;
						double hasilF;
						double hasilForcel = 0;



						if (convertStr(s1.get("acctype")).equals("MR")) {
							if ((convertDouble(s1.get("currentratio"))) >= 64.5) {
								if (((convertDouble(s1.get("netac"))) < 0)) {

									netAc = (convertDouble(s1.get("netac"))
											.doubleValue() * -1);

								} else {

									netAc = (convertDouble(s1.get("netac"))
											.doubleValue());
								}
								hasil = (new Double((convertDouble(
										s1.get("marketval")).doubleValue() * up)
										- netAc));
								hasilForcel = (new Double(convertDouble(
										s1.get("marketval")).doubleValue()
										* cell) - netAc);
							} else {
								hasil = 0;
							}
							if (convertDouble(s1.get("currentratio")) >= 64.5) {
								hasilF = (new Double(
										convertDouble(hasilForcel) * 100) / (100 - (convertDouble(s1
										.get("currentratio"))).doubleValue()));
							} else {
								hasilF = 0;
							}
						} else {
							hasil = 0;
							hasilF = 0;
						}
						Account acc = new Account();
						acc.setTradingId(s1.get("tradingid").toString());
						acc.setBranchId(convertStr(s1.get("branchid")));
						acc.setSalesId(convertStr(s1.get("salesid")));
						acc.setCustType(convertStr(s1.get("custtype")));
						acc.setCreditLimit(convertDouble(s1.get("creditlimit")));
						acc.setCurrentRatio(convertDouble(s1
								.get("currentratio")));
						acc.setCurrTL(convertDouble(s1.get("curtl")));
						acc.setDeposit(convertDouble(s1.get("deposit")));
						acc.setBuy(convertDouble(s1.get("buy")));
						acc.setSell(convertDouble(s1.get("sell")));
						acc.setNetAC(convertDouble(s1.get("netac")));
						acc.setBid(convertDouble(s1.get("bid")));
						acc.setOffer(convertDouble(s1.get("offer")));
						acc.setStockVal(convertDouble(s1.get("stockval")));
						acc.setMarketVal(convertDouble(s1.get("marketval")));
						acc.setLQValue(convertDouble(s1.get("lqvalue")));
						acc.setTopup(hasil);
						acc.setForceSell(hasilF);
						acc.setMyAccount(s1.get("myacc").toString().equals("1"));
						acc.setAccId(convertStr(s1.get("accid")));
						acc.setName(convertStr(s1.get("accname")));
						acc.setCustId(convertStr(s1.get("custid")));
						acc.setStatus(convertStr(s1.get("accstatus")));
						acc.setIsCorporate(convertStr(s1.get("iscorporate")));
						acc.setInvType(convertStr(s1.get("invtype")));
						acc.setPhone(convertStr(s1.get("phone")));
						acc.setHandphone(convertStr(s1.get("handphone")));
						acc.setEmail(convertStr(s1.get("email")));
						acc.setSingleID(convertStr(s1.get("singleid")));
						acc.setVirtualAccount(convertStr(s1
								.get("virtualaccount")));
						acc.setInvestorAccount(convertStr(s1
								.get("investoraccount")));


						acc.setSubAccount(convertStr(s1.get("subaccount")));
						acc.setComplianceId(convertStr(s1.get("complianceid")));
						acc.setAddress(convertStr(s1.get("address")));
						acc.setAccType(convertStr(s1.get("acctype")));
						acc.setWithdraw(convertDouble(s1.get("withdraw")));
						acc.setAts(convertStr(s1.get("ats")));
						acc.setExpktp(convertStr(s1.get("expktp")));//yosep expktp
						if (!clear) {
							vorder = new Vector(1);
							vorder.addElement(AccountDef.createTableRow(acc));
							engine.getDataStore()
									.get(TradingStore.DATA_ACCOUNT)
									.addRow(vorder, false, false);
						} else {
							vorder.addElement(AccountDef.createTableRow(acc));
						}
					}
					if (clear && vorder.size() > 0) {
						engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
								.addRow(vorder, true, false);
						engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
								.refresh();
					}
				}
			}
		});
				
		map.put(EVENT_DATAMARGIN, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAMARGIN");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				boolean clear = ((Boolean) param
						.getField(TradingEventDispatcher.PARAM_CLEAR))
						.booleanValue();
				byte[] data = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (data != null) {
					if (clear) {
						engine.getDataStore().get(TradingStore.DATA_MARGIN)
								.getDataVector().clear();
						engine.getDataStore().get(TradingStore.DATA_MARGIN)
								.refresh();
					}
					String s = new String(Utils.decompress(data));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					Vector vorder = new Vector(100, 10);
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						if (convertStr(s1.get("acctype")).equals("MR")) {
							double up = 0.5;
							double cell = 0.6;
							double netAc = 0;
							double hasil;
							double hasilF;
							double hasilForcel = 0;






							if ((convertDouble(s1.get("currentratio"))) >= 64.5) {
								if (((convertDouble(s1.get("netac"))) < 0)) {
									netAc = (convertDouble(s1.get("netac"))
											.doubleValue() * -1);
								} else {
									netAc = (convertDouble(s1.get("netac"))
											.doubleValue());
								}
								/*hasil = (new Double((convertDouble(
										s1.get("lqvalue")).doubleValue() * up)
										- netAc));
								hasilForcel = (new Double(convertDouble(
										s1.get("lqvalue")).doubleValue()
										* cell) - netAc);*/
								hasil = (new Double((convertDouble(
										s1.get("marketval")).doubleValue() * up)
										- netAc));
								hasilForcel = (new Double(convertDouble(
										s1.get("marketval")).doubleValue()
										* cell) - netAc);
							} else {
								hasil = 0;
							}
							// double forcel =(new )

							if (convertDouble(s1.get("currentratio")) >= 64.5) {
								hasilF = (new Double(
										convertDouble(hasilForcel) * 100) / (100 - (convertDouble(s1
										.get("currentratio"))).doubleValue()));
							} else {
								hasilF = 0;
							}

							Margin MM = new Margin();
							// if(convertStr(s1.get("acctype")).equals("MR")){

							MM.setAccId(convertStr(s1.get("accid")));
							MM.setName(convertStr(s1.get("accname")));
							MM.setCurrentRatio(convertDouble(s1
									.get("currentratio")));
							MM.setCreditLimit(convertDouble(s1
									.get("creditlimit")));
							MM.setNetAC(convertDouble(s1.get("netac")));
							MM.setLQValue(convertDouble(s1.get("lqvalue")));
							MM.setMarketVal(convertDouble(s1.get("marketval")));
							MM.setCurrTL(convertDouble(s1.get("curtl")));
							MM.setTopup(convertDouble(hasil));

							MM.setForceSell(convertDouble(hasilF));

							// ////////////////////////////////////////////////
							MM.setTradingId(s1.get("tradingid").toString());
							MM.setBranchId(convertStr(s1.get("branchid")));
							MM.setSalesId(convertStr(s1.get("salesid")));
							MM.setCustType(convertStr(s1.get("custtype")));
							MM.setDeposit(convertDouble(s1.get("deposit")));
							MM.setBuy(convertDouble(s1.get("buy")));
							MM.setSell(convertDouble(s1.get("sell")));
							MM.setBid(convertDouble(s1.get("bid")));
							MM.setOffer(convertDouble(s1.get("offer")));
							MM.setStockVal(convertDouble(s1.get("stockval")));
							MM.setMyAccount(s1.get("myacc").toString()
									.equals("1"));
							MM.setCustId(convertStr(s1.get("custid")));
							MM.setStatus(convertStr(s1.get("accstatus")));
							MM.setIsCorporate(convertStr(s1.get("iscorporate")));
							MM.setInvType(convertStr(s1.get("invtype")));
							MM.setPhone(convertStr(s1.get("phone")));
							MM.setHandphone(convertStr(s1.get("handphone")));
							MM.setEmail(convertStr(s1.get("email")));
							MM.setSubAccount(convertStr(s1.get("subaccount")));
							MM.setComplianceId(convertStr(s1
									.get("complianceid")));
							MM.setAddress(convertStr(s1.get("address")));
							MM.setAccType(convertStr(s1.get("acctype")));
							MM.setWithdraw(convertDouble(s1.get("withdraw")));
							if (convertStr(s1.get("accid")).equalsIgnoreCase(
									"HOOS001Z")) {
								//System.out.println("daycount "+ convertDouble(s1.get("daycount")));
							}
							MM.setDayCount(convertDouble(s1.get("daycount")));
							if (convertDouble(s1.get("daycount")) > 2
									|| (convertDouble(s1.get("currentratio"))) >= 74.5) {
								MM.setNote("Force Sell");
							} else if (convertDouble(s1.get("daycount")) > 0) {
								MM.setNote("Margin Call");
							} else {
								MM.setNote("-");
							}
							if (!clear) {
								vorder = new Vector(1);
								vorder.addElement(MarginDef.createTableRow(MM));
								engine.getDataStore()
										.get(TradingStore.DATA_MARGIN)
										.addRow(vorder, false, false);
							} else {
								vorder.addElement(MarginDef.createTableRow(MM));
							}
						}
					}
					if (clear && vorder.size() > 0) {
						engine.getDataStore().get(TradingStore.DATA_MARGIN)
								.addRow(vorder, true, false);
						engine.getDataStore().get(TradingStore.DATA_MARGIN)
								.refresh();
					}
				}
			}
		});
		map.put(EVENT_DATASCHEDULE, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATASCHEDULE");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessege = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				boolean clear = ((Boolean) param
						.getField(TradingEventDispatcher.PARAM_CLEAR))
						.booleanValue();
				byte[] schedule = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				if (schedule != null) {
					if (clear) {
						engine.getDataStore()
								.get(TradingStore.DATA_SCHEDULELIST)
								.getDataVector().clear();
						engine.getDataStore()
								.get(TradingStore.DATA_SCHEDULELIST).refresh();

					}
					String s = new String(Utils.decompress(schedule));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					Vector vorder = new Vector(100, 10);
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						Schedule sc = new Schedule();

						sc.setcClientid(convertStr(s1.get("clientid")));
						sc.setcAtsid(convertStr(s1.get("atsid")));
						sc.setcAtsdate(convertDate(s1.get("atsdate")));
						sc.setcBuysell(convertStr(s1.get("buyorsell")));
						sc.setcSecid(convertStr(s1.get("secid")));
						sc.setcPirce(convertDouble(s1.get("price")));
						sc.setcLot(convertDouble(s1.get("lot")));
						sc.setcVolume(convertDouble(s1.get("volume")));
						sc.setcLabel(convertStr(s1.get("label")));
						sc.setcValidity(convertStr(s1.get("validity")));
						sc.setcValiduntil(convertDate(s1.get("validitytime")));
						sc.setcAtsstatus(convertStr(s1.get("atsstatus")));
						sc.setcOrderid(convertStr(s1.get("orderid")));
						sc.setcOrderstatus(convertStr(s1.get("orderstatus")));
						sc.setcConditiontype(convertStr(s1.get("conditiontype")));
						sc.setcConditionmethod(convertStr(s1
								.get("conditionmethod")));
						sc.setcComparelastprice(convertDouble(s1
								.get("comparelastprice")));
						sc.setcRemainbidlot(convertDouble(s1
								.get("remainbidlot")));
						sc.setcCumulativvedone(convertDouble(s1
								.get("cumulativedone")));
						sc.setcCumulativecalc(convertDouble(s1
								.get("cumulativecalc")));
						sc.setcReversallimit(convertDouble(s1
								.get("reversallimit")));






						sc.setcBdFilter(convertStr(s1.get("bdfilter")));
						sc.setcBdRefcode(convertStr(s1.get("bdrefcode")));
						sc.setcBdRefopt(convertStr(s1.get("bdrefopt")));
						sc.setcBdDonetype(convertStr(s1.get("bddonetype")));
						sc.setcBdDonespec(convertDouble(s1
								.get("bddonespecific")));

						sc.setcDescription(convertStr(s1.get("description")));
						sc.setcEntrybuy(convertStr(s1.get("entryby")));
						sc.setcEntrytime(convertDate(s1.get("entrytime")));
						sc.setcEntryterminal(convertStr(s1.get("entryterminal")));
						sc.setcExecby(convertStr(s1.get("execby")));
						sc.setcExectime(convertDate(s1.get("exectime")));
						sc.setcWithdrawby(convertStr(s1.get("withdrawby")));
						sc.setcWithdrawtime(convertDate(s1.get("withdrawtime")));
						sc.setcWithdrawterminal(convertStr(s1
								.get("withdrawterminal")));

						sc.setcFlag1(convertDouble(s1.get("flag1")));
						sc.setcInvtype(convertStr(s1.get("invtype")));


						if (!clear) {
							vorder = new Vector(1);
							vorder.addElement(ScheduleDef.createTableRow(sc));
							engine.getDataStore()
									.get(TradingStore.DATA_SCHEDULELIST)
									.addRow(vorder, false, false);
						} else {
							vorder.addElement(ScheduleDef.createTableRow(sc));

						}
					}
					if (clear && vorder.size() > 0) {
						engine.getDataStore()
								.get(TradingStore.DATA_SCHEDULELIST)
								.addRow(vorder, true, false);
						engine.getDataStore()
								.get(TradingStore.DATA_SCHEDULELIST).refresh();
					}


				}


			}
		});
		map.put(EVENT_DATABROWSEORDER, new EventHandler() {
			
			@Override
			public void execute(Event param) {
				//log.info("execute EVENT_BROWSEORDER");
				String eventName = (String)param
									.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] browseorder = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				boolean clear = ((Boolean) param
						.getField(TradingEventDispatcher.PARAM_CLEAR))
						.booleanValue();
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if(browseorder!= null){
					engine.getDataStore().get(TradingStore.DATA_BROWSEORDER)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_BROWSEORDER)
							.refresh();
					String s = new String(Utils.decompress(browseorder));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					Vector vorder = new Vector(100, 10);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject)array.get(j);
						BrowseOrder bo= new BrowseOrder();
						bo.setOrderid(convertStr(s1.get("orderid")));
						bo.setBuyorsell(convertStr(s1.get("buyorsell")));
						bo.setSecid(convertStr(s1.get("secid")));
						bo.setPrice(convertDouble(s1.get("price")));
						bo.setVolume(convertDouble(s1.get("volume")));
						bo.setAtsstatus(convertStr(s1.get("statusid")));
						bo.setLabel(convertStr(s1.get("label")));
						bo.setClientid(convertStr(s1.get("custid")));
						if (!clear) {
						vorder = new Vector(1);
						vorder.addElement(BrowseOrderDef.createTableRow(bo));
						engine.getDataStore()
								.get(TradingStore.DATA_BROWSEORDER)
								.addRow(vorder, false, false);} 
						else {
									vorder.addElement(BrowseOrderDef.createTableRow(bo));
								}
					}
					if (clear && vorder.size() > 0) {
						engine.getDataStore().get(TradingStore.DATA_BROWSEORDER)
						.addRow(vorder, true, false);
						engine.getDataStore().get(TradingStore.DATA_BROWSEORDER)
						.refresh();	}
				}
			}
		});	
		map.put(EVENT_DATABACKNOTIF, new EventHandler() {


			@Override
			public void execute(Event param) {
				//log.info("execute EVENT_DATABACKNOTIF");
				String evenName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String evenMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] backnotif = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap();
				p.put(TradingEventDispatcher.PARAM_MESSAGE, evenMessage);
				engine.getEventDispatcher().pushEvent(new Event(evenName, p));
				if (backnotif != null) {
					engine.getDataStore().get(TradingStore.DATA_NOTIFICATION)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_NOTIFICATION)
							.refresh();
					String s = new String(Utils.decompress(backnotif));
					//System.out.println("%%%" + s);
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						Notif nt = new Notif();
						nt.setcUserid(convertStr(s1.get("userid")));
						nt.setcNotif(convertStr(s1.get("notif")));
						nt.setcOnselldone(convertStr(s1.get("onselldone")));
						nt.setcOnbuydone(convertStr(s1.get("onbuydone")));
						nt.setcSendsms(convertStr(s1.get("sendsms")));
						nt.setcMobile1(convertStr(s1.get("mobile1")));
						nt.setcMobile2(convertStr(s1.get("mobile2")));
						nt.setcMobile3(convertStr(s1.get("mobile3")));
						nt.setcSendmail(convertStr(s1.get("sendemail")));
						nt.setcEmail1(convertStr(s1.get("email1")));
						nt.setcEmail2(convertStr(s1.get("email2")));
						nt.setcEmail3(convertStr(s1.get("email3")));
						Vector vnotif = new Vector();
						vnotif.addElement(NotifDef.createTableRow(nt));
						engine.getDataStore()
								.get(TradingStore.DATA_NOTIFICATION)
								.addRow(vnotif, true, false);
					}
				}
			}

		});
		map.put(EVENT_DATAANNOUNCEMENT, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATAANNOUNCEMENT");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] announcement = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (announcement != null) {
					engine.getDataStore().get(TradingStore.DATA_ANNOUNCEMENT)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_ANNOUNCEMENT)
							.refresh();
					String s = new String(Utils.decompress(announcement));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						System.out.println("announcement" + s1.toString());
						Announcement an = new Announcement();
						an.setcId(convertStr(s1.get("AUTOID")));
						an.setcTitle(convertStr(s1.get("TITLE")));
						an.setcContent(convertStr(s1.get("CONTENT")));
						an.setcEntrytime(convertStr(s1.get("ENTRYTIME")));
						an.setcEntryby(convertStr(s1.get("ENTRYBY")));
						an.setcStatus(convertStr(s1.get("STATUS")));
						an.setcStatustime(convertStr(s1.get("STATUSTIME")));
						an.setcStatusby(convertStr(s1.get("STATUSBY")));
						an.setcTime(convertStr(s1.get("WAKTU")));
						an.setcDate(convertStr(s1.get("TANGGAL")));
						Vector vorder = new Vector(1);
						vorder.addElement(AnnouncementDef.createTableRow(an));
						engine.getDataStore()
								.get(TradingStore.DATA_ANNOUNCEMENT)
								.addRow(vorder, true, false);
					}
				}
			}
		});
		
		map.put(EVENT_DATANEGDEALLIST, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_DATANEGDEALLIST");
				String eventName = (String) param
						.getField(TradingEventDispatcher.PARAM_CODE);
				String eventMessage = (String) param
						.getField(TradingEventDispatcher.PARAM_MESSAGE);
				byte[] negdeal = (byte[]) param
						.getField(TradingEventDispatcher.PARAM_DATA);
				HashMap p = new HashMap(1);
				p.put(TradingEventDispatcher.PARAM_MESSAGE, eventMessage);
				engine.getEventDispatcher().pushEvent(new Event(eventName, p));
				if (negdeal != null) {
					engine.getDataStore().get(TradingStore.DATA_NEGDEALLIST)
							.getDataVector().clear();
					engine.getDataStore().get(TradingStore.DATA_NEGDEALLIST)
							.refresh();
					String s = new String(Utils.decompress(negdeal));
					JSONArray array = (JSONArray) JSONValue.parse(s);
					int i = array.size();
					for (int j = 0; j < i; j++) {
						JSONObject s1 = (JSONObject) array.get(j);
						//System.out.println(s1.toString());
						NegDealList ng = new NegDealList();
						ng.setMarketOrderID(s1.get("marketorderid").toString());

						Object marketref = s1.get("marketref");
						if (marketref == null) {
							marketref = "";
						}
						ng.setMarketRef(marketref.toString());
						ng.setIsAdvertising(convertStr(s1.get("isadvertising")));
						ng.setOriginalOrderID(convertStr(s1
								.get("originalorderid")));
						ng.setUserID(convertStr(s1.get("userid")));
						ng.setBrokerID(convertStr(s1.get("brokerid")));
						ng.setOrderDate(convertDate(s1.get("orderdate")));
						ng.setStatusID(convertStr(s1.get("statusid")));
						ng.setInvType(convertStr(s1.get("invtype")));
						ng.setSecID(convertStr(s1.get("secid")));
						ng.setBrdID(convertStr(s1.get("brdid")));
						ng.setSecurityID(convertStr(s1.get("securityid")));
						ng.setBuySell(convertStr(s1.get("buyorsell")));
						ng.setVolume(convertDouble(s1.get("volume")));
						ng.setPrice(convertDouble(s1.get("price")));
						ng.setBalance(convertDouble(s1.get("balance")));
						ng.setOrdType(convertStr(s1.get("ordtype")));
						ng.setOrderType(convertStr(s1.get("ordertype")));
						ng.setTradeDate(convertDate(s1.get("tradedate")));
						ng.setNote(convertStr(s1.get("note")));
						ng.setContraBrokerID(convertStr(s1
								.get("contrabrokerid")));
						ng.setContraUserID(convertStr(s1.get("contrauserid")));
						ng.setComplianceID(convertStr(s1.get("complianceid")));

						Vector vorder = new Vector(1);
						vorder.addElement(NegDealListDef.createTableRow(ng));
						engine.getDataStore()
								.get(TradingStore.DATA_NEGDEALLIST)
								.addRow(vorder, false, false);
					}

				}
			}
		});
		
		map.put(EVENT_RECEIVESPLIT, new EventHandler() {
			public void execute(Event param) {
				//log.info("execute EVENT_RECEIVESPLITREQ "+ param.getParam().get(PARAM_FIX).toString());
				try {
					Message fix = (Message) param.getParam().get(PARAM_FIX);
					JSONObject result = (JSONObject) JSONValue.parse(fix
							.getString(58));
					doSplit((JSONObject) result.get("reply"));
				} catch (Exception ex) {
					log.warn(Utils.logException(ex));
				}
			}
		});
		map.put(EVENT_RECEIVENEWORDERREQ, new EventHandler() {
			public void execute(Event param) {
			//	log.info("execute EVENT_RECEIVENEWORDERREQ "+ param.getParam().get(PARAM_FIX).toString());
				try {
					Message message = (Message) param.getField(PARAM_FIX);
					double lotsize = Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));

					Stock sec = (Stock) engine
							.getDataStore()
							.get(TradingStore.DATA_STOCK)
							.getDataByKey(
									new String[] { message
											.getString(Symbol.FIELD) });
					if (sec != null) {
						lotsize = sec.getLotSize().doubleValue();
					}
					Order order = new Order();
					order.setId(message.getString(ClOrdID.FIELD));
					order.setOrderDate(new Date());
					order.setEntryTime(message.getUtcTimeStamp(666671));
					order.setCounter(new Double(message.getDouble(666672)));
					order.setSplitTo(new Double(message.getDouble(666673)));
					order.setExchange(message.getString(666675));
					order.setAccId(message.getString(666676));
					order.setEntryBy(message.getString(666677));
					order.setMarketRef(message.getString(666678));
					order.setEntryTerminal(message.getString(666679));
					order.setInvType(message
							.getString(quickfix.field.Account.FIELD));
					order.setStock(message.getString(Symbol.FIELD));
					order.setBoard(message.getString(SymbolSfx.FIELD));
					order.setBOS(message.getString(Side.FIELD));
					order.setPrice(new Double(message.getDouble(Price.FIELD)));
					order.setVolume(new Double(message
							.getDouble(OrderQty.FIELD)));

					if(order.getBoard().equalsIgnoreCase("RG")){
						order.setLot(new Double(order.getVolume().doubleValue()
								/ lotsize));}
						else{

							order.setLot(new Double(order.getVolume().doubleValue()) );
						}


					order.setBalVol(new Double(order.getVolume().doubleValue()));
					order.setBalLot(new Double(order.getLot().doubleValue()));
					order.setDoneVol(new Double(0));
					order.setDoneLot(new Double(0));
					order.setStatus(Order.C_SENDING_ENTRYSERVER);
					order.setExchange(message.getString(666675));
					order.setOrdType(message.getString(OrdType.FIELD));
					order.setOrderType(message.getString(TimeInForce.FIELD));
					order.setEntryTime(message
							.getUtcTimeStamp(TransactTime.FIELD));
					order.setMarketOrderId("");
					try {
						String isfloor = message.getString(666674);
						order.setIsFloor(isfloor == null ? "0" : isfloor);
					} catch (Exception ex) {
						order.setIsFloor("0");
					}

					try {
						String basket = message.getString(8007);
						order.setIsBasketOrder(basket == null ? "0" : basket);
					} catch (Exception ex) {
						order.setIsBasketOrder("0");
					}
					try {
						Date baskettime = message.getUtcTimeStamp(8008);
						order.setBasketTime(baskettime);
					} catch (Exception ex) {
					}
					String handinst = message.getString(HandlInst.FIELD);
					order.setIsAdvertising(handinst.equals("2") ? "1" : "0");
					order.setTradeNo("0");
					order.setUpdateBy(order.getEntryBy());
					order.setUpdateDate(order.getEntryTime());
					order.setTerminalId(order.getEntryTerminal());
					order.setComplianceId(convertStr(message.getString(376)));
					order.setEntryTime(convertDate(message.getString(666671)));
					order.setCounter(convertDouble(message.getString(666672)));
					order.setSplitTo(convertDouble(message.getString(666673)));
					order.setExchange(convertStr(message.getString(666675)));
					order.setAccId(convertStr(message.getString(666676)));
					order.setEntryBy(convertStr(message.getString(666677)));
					order.setMarketRef(convertStr(message.getString(666678)));
					order.setEntryTerminal(convertStr(message.getString(666679)));
					order.setBroker(convertStr(message.getString(7043)));
					order.setCurrency(convertStr(message.getString(7044)));
					try {
						order.setNegDealRef(convertStr(message.getString(7050)));
					} catch (Exception ex) {
					}
					try {
						order.setContraBroker(convertStr(message
								.getString(7048)));
					} catch (Exception ex) {
					}
					try {
						order.setContraUser(convertStr(message.getString(7049)));
					} catch (Exception ex) {
					}
					
					order.calculate();
					try {
						if (message.getString(7045).equals("")) {
							Order buy = new Order();
							buy.fromVector(order.getData());

							order.setOrderIDContra(message.getString(7045));
							order.setAccIDContra(message.getString(7046));
							order.setInvTypeContra(message.getString(7047));

							buy.setId(order.getOrderIDContra());
							buy.setAccId(order.getAccId());
							buy.setInvType(order.getInvTypeContra());
							buy.setBOS(Order.C_BUY);
							buy.setOrderIDContra(order.getId());
							buy.setAccIDContra(order.getAccId());
							buy.setInvTypeContra(order.getInvType());
							buy.setMarketRef(buy.getId());
							Account acc = (Account) engine
									.getDataStore()
									.get(TradingStore.DATA_ACCOUNT)
									.getDataByField(
											new String[] { order.getAccId() },
											new int[] { Order.C_ACCID });
							if (acc != null) {
								buy.setTradingId(acc.getTradingId());
							}
							
							buy.calculate();
							
							/*log.info("execute ordertaker rule (crossing): "
									+ param.getName() + " order id: "
									+ order.getId());*/
							final Vector vorder = new Vector(1);
							vorder.addElement(OrderDef.createTableRow(buy));
							
							
							SwingUtilities.invokeLater(new Runnable() {
								
								@Override
								public void run() {
							engine.getDataStore().get(TradingStore.DATA_ORDER)
									.addRow(vorder, false, false);
								}
							});
							
						}
					} catch (Exception ex) {
					}

					Account acc = (Account) engine
							.getDataStore()
							.get(TradingStore.DATA_ACCOUNT)
							.getDataByField(new String[] { order.getAccId() },
									new int[] { Account.C_ACCID });
					if (acc != null) {
						order.setTradingId(acc.getTradingId());
						/*log.info("execute ordertaker rule: " + param.getName()
							+ " order id: " + order.getId());*/
						final Vector vorder = new Vector(1);
						vorder.addElement(OrderDef.createTableRow(order));
						
						SwingUtilities.invokeLater(new Runnable() {
							
							@Override
							public void run() {
								engine.getDataStore().get(TradingStore.DATA_ORDER)
									.addRow(vorder, false, false);

							}
						});

					}
				} catch (Exception ex) {
					log.warn(Utils.logException(ex));
				}
			}
		});
		map.put(EVENT_RECEIVEWITHDRAWREQ, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_RECEIVEWITHDRAWREQ "
						+ param.getParam().get(PARAM_FIX).toString());*/
				try {
					Message message = (Message) param.getField(PARAM_FIX);
					Order order = (Order) engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getDataByKey(
									new Object[] {
											message.getField(new ClOrdID())
													.getValue(), "0" });
					if (order != null) {
						order.setStatus(Order.C_REQUEST_WITHDRAW);
						order.setWithdrawTime(message.getUtcTimeStamp(666671));
						order.setWithdrawBy(message.getString(666677));
						order.setWithdrawTerminal(message.getString(666679));
						order.setUpdateBy(order.getWithdrawBy());
						order.setUpdateDate(order.getWithdrawTime());
						order.setTerminalId(order.getWithdrawTerminal());
						int idx = engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getIndexByKey(
										new Object[] { order.getId(),
												order.getTradeNo() });
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.refresh(idx);
					} else {
						Event e = new Event(EVENT_REFRESH_ORDER);
						e.setField("accountid", "%");
						e.setField("orderid", message.getField(new ClOrdID())
								.getValue());
						pushEvent(e);
					}
				} catch (Exception ex) {
					log.warn(Utils.logException(ex));
				}
			}
		});
		map.put(EVENT_RECEIVEAMENDREQ, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_RECEIVEAMENDREQ "
						+ param.getParam().get(PARAM_FIX).toString());*/
				try {
					Message message = (Message) param.getField(PARAM_FIX);
					final Order order = (Order) engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getDataByKey(
									new Object[] {
											message.getField(new OrigClOrdID())
													.getValue(), "0" });
					if (order != null) {
						order.setAmendTime(message.getUtcTimeStamp(666671));
						order.setAmendBy(message.getString(666677));
						order.setAmendTerminal(message.getString(666679));
						order.setStatus(Order.C_REQUEST_AMEND);
						order.setUpdateBy(order.getAmendBy());
						order.setUpdateDate(order.getAmendTime());
						order.setTerminalId(order.getAmendTerminal());
						Order neworder = new Order();
						neworder.fromVector((Vector) order.getData().clone());
						neworder.setMarketRef(message.getString(666678));
						neworder.setPrice(new Double(message.getField(
								new Price()).getValue()));
						neworder.setId(new String(message.getField(
								new ClOrdID()).getValue()));
						neworder.updateVolume(message.getField(new OrderQty())
								.getValue());
						neworder.setMarketOrderId("");
						neworder.setStatus(Order.C_ENTRY);
						neworder.setEntryTime(order.getAmendTime());
						neworder.setEntryBy(order.getAmendBy());
						neworder.setEntryTerminal(order.getAmendTerminal());
						neworder.setUpdateBy(neworder.getEntryBy());
						neworder.setUpdateDate(neworder.getEntryTime());
						neworder.setTerminalId(neworder.getEntryTerminal());

						order.setNextOrderId(neworder.getId());
						neworder.setPrevOrderId(order.getId());
						neworder.setPrevMarketOrderId(order.getMarketOrderId());

						final Vector vo2 = new Vector(1);
						vo2.addElement(OrderDef.createTableRow(neworder));
						final String id = order.getId();
						final String tradeno = order.getTradeNo();
						
						SwingUtilities.invokeLater(new Runnable() {
							
							@Override
							public void run() {
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.addRow(vo2, false, false);

						int idx = engine
								.getDataStore()

								.get(TradingStore.DATA_ORDER)
								.getIndexByKey(
										new Object[] { order.getId(),
												order.getTradeNo() });



						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.refresh(idx);

							}
						});
						
						
					} else {
						Event e = new Event(EVENT_REFRESH_ORDER);
						e.setField("accountid", "%");
						e.setField("orderid",
								message.getField(new OrigClOrdID()).getValue());
						pushEvent(e);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					log.warn(Utils.logException(ex));
				}
			}
		});

		map.put(EVENT_BROADCAST, new EventHandler() {
			@Override
			public void execute(Event param) {
				try {
					Message message = (Message) param.getField(PARAM_FIX);
					/*log.info("execute EVENT_BROADCAST "
							+ param.getField(PARAM_FIX).toString());*/


					StringBuffer strDisclaimer = new StringBuffer();
					strDisclaimer.append("Zhtml><boddy>");
					strDisclaimer.append("<div align='justify' style='font-family: tahoma; font-size:10px'>");

					strDisclaimer.append(message.getString(999128));
					strDisclaimer.append("</div");
					strDisclaimer.append("</body></html>");



					if (message.getString(999128).startsWith("ATS")) {
						engine.getConnection().refreshAts("%");
					} else {

						Utils.showMessage(message.getString(999128), null);
					}

				} catch (Exception e2) {
					//System.out.println("ES --123 " + e2);
				}
			}
		});
		map.put(EVENT_BCMARGINCALL, new EventHandler() {
			@Override
			public void execute(Event param) {
				try {
					Message message = (Message) param.getField(PARAM_FIX);
					log.info("execute EVENT_BCMARGINCALL "
							+ param.getField(PARAM_FIX).toString());


					StringBuffer strDisclaimer = new StringBuffer();
					strDisclaimer.append("Zhtml><boddy>");
					strDisclaimer.append("<div align='justify' style='font-family: tahoma; font-size:10px'>");

					strDisclaimer.append("</div");
					strDisclaimer.append("</body></html>");


					if(TradingSetting.getMarginCall()){
						UserProfile sales = (UserProfile) engine
								.getDataStore().get(TradingStore.DATA_USERPROFILE)
								.getDataByField(new Object[] { "40" },
										new int[] { UserProfile.C_MENUID });

						if (sales.getProfileId().toUpperCase()
								.equals("SALES")||sales.getProfileId().toUpperCase()
								.equals("DEALER") || sales.getProfileId().toUpperCase()
								.equals("CONTROLLER")) {

							GridModel model =  new GridModel(MarginDef.getHeader(), false);
							String[] list = message.getString(58).split(",");

							Vector vRow = new Vector(100, 5);
							for (int i = 0; i < list.length; i++) {
								String accid = list[i].split("#")[0];
								String ratio = list[i].split("#")[1];
								log.info(accid +" EVENT_BCMARGINCALL");
								int a = engine.getDataStore().get(TradingStore.DATA_MARGIN).getIndexByKey(new Object[] {accid.trim()});
								if(a>0) {
								Margin mg = 
										(Margin) engine.getDataStore().get(TradingStore.DATA_MARGIN).getDataByKey(new Object[] { accid.trim() });
								mg.setCurrentRatio(Double.parseDouble(ratio)*100);
								vRow.addElement(MarginDef
										.createTableRow(mg));
								}
							}
							model.addRow(vRow, false, false);
							model.refresh();
							
							for (int i = 0; i < engine.getEventListener().size(); i++) {
								ITradingListener listener = (ITradingListener) engine
										.getEventListener().elementAt(i);
								listener.addMarginCall(model);
							}
						} else {
							Utils.showMessage(message.getString(58), null);
//							Utils.showMessage(message.getString(999128), null);
						}
					}
					
					

				} catch (Exception e2) {
					System.out.println("ES --123 " + e2);
					e2.printStackTrace();
				}
			}
		});
		
		map.put(EVENT_MARGINCALL, new EventHandler() {
			@Override
			public void execute(Event param) {
				try {
					Message message = (Message) param.getField(PARAM_FIX);
					log.info("execute EVENT_MARGINCALL "
							+ param.getField(PARAM_FIX).toString());


					/*StringBuffer strDisclaimer = new StringBuffer();
					strDisclaimer.append("Zhtml><boddy>");
					strDisclaimer.append("<div align='justify' style='font-family: tahoma; font-size:10px'>");

					strDisclaimer.append(message.getString(999128));
					strDisclaimer.append("</div");
					strDisclaimer.append("</body></html>");



					if (message.getString(999128).startsWith("ATS")) {
						engine.getConnection().refreshAts("%");
					} else {

						Utils.showMessage(message.getString(999128), null);
					}*/
					Utils.showMessage("test MARGIN CALL", null);




				} catch (Exception e2) {
					//System.out.println("ES --123 " + e2);
				}
			}
		});
		map.put(EVENT_RECEIVENEWORDERSPLIT_REPLY, new EventHandler() {

			@Override
			public void execute(Event param) {
				try {
					Message message = (Message) param.getParam().get(PARAM_FIX);

					/*log.info("execute EVENT_NEWORDERSPLIT_REPLY  "
							+ (message.isSetField(ClOrdID.FIELD) == true ? message
									.getString(ClOrdID.FIELD)
									: "ClordID not found" + ":"))*/
;
					engine.getConnection().deleteFilterByOrder(
							message.getString(ClOrdID.FIELD));
					String[] sclords = message.getString(ClOrdID.FIELD).split(
							"!");
					String[] ordstatuses = message.getString(OrdStatus.FIELD)
							.split("!");
					String[] ptlbefores = message.getString(7004).split("!");
					String[] pavportbefores = message.getString(7005)
							.split("!");
					String[] plqlbefores = message.getString(7006).split("!");
					String[] pratiobefores = message.getString(7007).split("!");
					String[] isoverlimit = message.getString(7008).split("!");
					String[] isshortsell = message.getString(7009).split("!");
					String[] curtls = message.getString(7002).split("!");
					String[] pavport = message.getString(7003).split("!");
					String[] curratio = message.getString(7010).split("!");
					String[] pbuy = message.getString(7011).split("!");
					String[] psell = message.getString(7012).split("!");

					String[] pnetacc = message.getString(7013).split("!");
					String[] pbid = message.getString(7014).split("!");
					String[] poffer = message.getString(7015).split("!");
					String[] pstockval = message.getString(7016).split("!");
					String[] pmarketval = message.getString(7017).split("!");
					String[] plqvalue = message.getString(7018).split("!");
					String[] ptopup = message.getString(7019).split("!");
					String[] pforcesell = message.getString(7020).split("!");
					String[] pcreditlimit = message.getString(7021).split("!");
					String[] pdeposit = message.getString(7022).split("!");
					String[] pcurrvol = message.getString(7023).split("!");
					String[] pavgprice = message.getString(7024).split("!");
					String[] pwithdraw = message.getString(7061).split("!");

					for (int i = 0; i < sclords.length; i++) {
						String sclordid = sclords[i];
						if (!sclordid.isEmpty()) {
							String ordstatus = ordstatuses[i];
							if (ordstatus.equals(Order.C_ENTRY)) {
								Event e = new Event(EVENT_ACK);
								e.setField("clordid", sclordid);
								e.setField("status", ordstatus);
								try {
									e.setField("description", "");
								} catch (Exception ex) {
									try {
										e.setField("description", "");
									} catch (Exception ep) {
										e.setField("description", "");
									}
								}
								pushEvent(e);
								try {
									if (!ordstatus
											.equals(Order.C_REQUEST_WITHDRAW)) {

										e = new Event(
												EVENT_RECEIVE_RISKMANAGEMENT);
										e.setField("clordid", sclordid);
										e.setField("" + PARAM_ptlbefore,
												new Double(ptlbefores[i]));
										e.setField("" + PARAM_pavportbefore,
												new Double(pavportbefores[i]));
										e.setField("" + PARAM_plqvaluebefore,
												new Double(plqlbefores[i]));
										e.setField("" + PARAM_pratiobefore,
												new Double(pratiobefores[i]));
										e.setField("" + PARAM_pisoverlimit,
												isoverlimit[i]);
										e.setField("" + PARAM_pisshortsell,
												isshortsell[i]);
										e.setField("" + PARAM_curtl,
												new Double(curtls[i]));
										e.setField("" + PARAM_pavport,
												new Double(pavport[i]));
										e.setField("" + PARAM_pcurratio,
												new Double(curratio[i]));
										e.setField("" + PARAM_pbuy, new Double(
												pbuy[i]));
										e.setField("" + PARAM_psell,
												new Double(psell[i]));
										e.setField("" + PARAM_pnetac,
												new Double(pnetacc[i]));
										e.setField("" + PARAM_pbid, new Double(
												pbid[i]));
										e.setField("" + PARAM_poffer,
												new Double(poffer[i]));
										e.setField("" + PARAM_pstockval,
												new Double(pstockval[i]));
										e.setField("" + PARAM_pmarketval,
												new Double(pmarketval[i]));
										e.setField("" + PARAM_plqvalue,
												new Double(plqvalue[i]));
										e.setField("" + PARAM_ptopup,
												new Double(ptopup[i]));
										e.setField("" + PARAM_pforcesell,
												new Double(pforcesell[i]));
										e.setField("" + PARAM_pcreditlimit,
												new Double(pcreditlimit[i]));
										e.setField("" + PARAM_pdeposit,
												new Double(pdeposit[i]));
										e.setField("" + PARAM_pcurvol,
												new Double(pcurrvol[i]));
										e.setField("" + PARAM_pavgprice,
												new Double(pavgprice[i]));

										if (pwithdraw != null) {
											e.setField("" + PARAM_pwithdraw,
													new Double(pwithdraw[i]));
										}
										pushEvent(e);

									}
								} catch (Exception ex) {
									ex.printStackTrace();
								}

							}

						

}
					}

				} catch (Exception e) {
					e.printStackTrace()

;

				}

			}

		});

		map.put(EVENT_RECEIVENEWORDERSPLIT_ENTRY, new EventHandler() {

			@Override
			public void execute(Event param) {
				Event e = null;

				try {

					Message message = (Message) param.getParam().get(PARAM_FIX);
					//log.info("EVENT_RECEIVE_SPLITENTRY " + message.toString());

					double lotsize = Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));

					Stock sec = (Stock) engine
							.getDataStore()
							.get(TradingStore.DATA_STOCK)
							.getDataByKey(
									new String[] { message
											.getString(Symbol.FIELD) });
					if (sec != null) {
						lotsize = sec.getLotSize().doubleValue();
					}

					String[] sclordids = message.getString(ClOrdID.FIELD)
							.split("!");
					String[] marketrefs = message.getString(666678).split("!");
					String[] orderqtys = message.getString(OrderQty.FIELD)
							.split("!");

					Account acc = (Account) engine
							.getDataStore()
							.get(TradingStore.DATA_ACCOUNT)
							.getDataByField(
									new String[] { message.getString(666676) },
									new int[] { Account.C_ACCID });

					final Vector vorder = new Vector(sclordids.length - 1);
					for (int i = 0; i < sclordids.length; i++) {
						if (!sclordids[i].isEmpty()) {
							Order order = new Order();
							order.setId(sclordids[i]);
							order.setOrderDate(new Date());
							order.setEntryTime(message.getUtcTimeStamp(666671));
							order.setCounter(new Double(message
									.getDouble(666672)));
							order.setSplitTo(new Double(message
									.getDouble(666673)));
							order.setExchange(message.getString(666675));
							order.setAccId(message.getString(666676));
							order.setEntryBy(message.getString(666677));
							order.setMarketRef(marketrefs[i]);
							order.setEntryTerminal(message.getString(666679));
							order.setInvType(message
									.getString(quickfix.field.Account.FIELD));
							order.setStock(message.getString(Symbol.FIELD));
							order.setBoard(message.getString(SymbolSfx.FIELD));
							order.setBOS(message.getString(Side.FIELD));
							order.setPrice(new Double(message
									.getDouble(Price.FIELD)));
							order.setVolume(new Double(orderqtys[i]));
							order.setLot(new Double(order.getVolume()
									.doubleValue() / lotsize));
							order.setBalVol(new Double(order.getVolume()
									.doubleValue()));
							order.setBalLot(new Double(order.getLot()
									.doubleValue()));
							order.setDoneVol(new Double(0));
							order.setDoneLot(new Double(0));
							order.setStatus(Order.C_SENDING_ENTRYSERVER);
							order.setExchange(message.getString(666675));
							order.setOrdType(message.getString(OrdType.FIELD));
							order.setOrderType(message
									.getString(TimeInForce.FIELD));
							order.setEntryTime(message
									.getUtcTimeStamp(TransactTime.FIELD));
							order.setMarketOrderId("");
							try {
								String isfloor = message.getString(666674);
								order.setIsFloor(isfloor == null ? "0"
										: isfloor);
							} catch (Exception ex) {
								order.setIsFloor("0");
							}

							try {
								String basket = message.getString(8007);
								order.setIsBasketOrder(basket == null ? "0"
										: basket);
							} catch (Exception ex) {
								order.setIsBasketOrder("0");
							}
							try {
								Date baskettime = message.getUtcTimeStamp(8008);
								order.setBasketTime(baskettime);
							} catch (Exception ex) {
							}
							String handinst = message
									.getString(HandlInst.FIELD);
							order.setIsAdvertising(handinst.equals("2") ? "1"
									: "0");
							order.setTradeNo("0");
							order.setUpdateBy(order.getEntryBy());
							order.setUpdateDate(order.getEntryTime());
							order.setTerminalId(order.getEntryTerminal());
							order.setComplianceId(convertStr(message
									.getString(376)));
							order.setEntryTime(convertDate(message
									.getString(666671)));
							order.setCounter(convertDouble(message
									.getString(666672)));
							order.setSplitTo(convertDouble(message
									.getString(666673)));
							order.setExchange(convertStr(message
									.getString(666675)));
							order.setAccId(convertStr(message.getString(666676)));
							order.setEntryBy(convertStr(message
									.getString(666677)));
							order.setMarketRef(convertStr(message
									.getString(666678)));
							order.setEntryTerminal(convertStr(message
									.getString(666679)));
							order.setBroker(convertStr(message.getString(7043)));
							order.setCurrency(convertStr(message
									.getString(7044)));
							try {
								order.setNegDealRef(convertStr(message
										.getString(7050)));
							} catch (Exception ex) {
							}
							try {
								order.setContraBroker(convertStr(message
										.getString(7048)));
							} catch (Exception ex) {
							}
							try {
								order.setContraUser(convertStr(message
										.getString(7049)));
							} catch (Exception ex) {
							}
							/*
							 * try { if (message.getString(7045).equals("")) {
							 * Order buy = new Order();
							 * buy.fromVector(order.getData());
							 * 
							 * order.setOrderIDContra(message.getString(7045));
							 * order.setAccIDContra(message.getString(7046));
							 * order.setInvTypeContra(message.getString(7047));
							 * 
							 * buy.setId(order.getOrderIDContra());
							 * buy.setAccId(order.getAccId());
							 * buy.setInvType(order.getInvTypeContra());
							 * buy.setBOS(Order.C_BUY);
							 * buy.setOrderIDContra(order.getId());
							 * buy.setAccIDContra(order.getAccId());
							 * buy.setInvTypeContra(order.getInvType());
							 * buy.setMarketRef(buy.getId()); Account acc =
							 * (Account) engine .getDataStore()
							 * .get(TradingStore.DATA_ACCOUNT) .getDataByField(
							 * new String[] { order.getAccId() }, new int[] {
							 * Order.C_ACCID }); if (acc != null) {
							 * buy.setTradingId(acc.getTradingId()); }
							 * log.info("execute ordertaker rule (crossing): " +
							 * param.getName() + " order id: " + order.getId());
							 * Vector vorder = new Vector(1);
							 * vorder.addElement(OrderDef.createTableRow(buy));
							 * engine.getDataStore()
							 * .get(TradingStore.DATA_ORDER) .addRow(vorder,
							 * false, false); } } catch (Exception ex) { }
							 */

							if (acc != null) {
								order.setTradingId(acc.getTradingId());
								/*log.info("execute ordertaker rule: "
										+ param.getName() + " order id: "
										+ order.getId());*/

								vorder.addElement(OrderDef
										.createTableRow(order));

							}


						}
					}

					try {
						SwingUtilities.invokeAndWait(new Runnable() {
							public void run() {

								try {
									/*log.info("execute split-entry order for taker "
											+ vorder.size());*/
									for (int i = 0; i < vorder.size(); i++) {
										OrderField of = (OrderField) ((Vector) vorder
												.get(i)).get(0);
										Order ord = (Order) of.getSource();
										Order ord2 = (Order) engine
												.getDataStore()
												.get(TradingStore.DATA_ORDER)
												.getDataByKey(
														new Object[] {
																ord.getId(),
																"0" });

										if (ord2 != null) {
											/*log.info("status before:"
													+ ord2.getStatus() + ":");*/
											ord.setStatus(ord2.getStatus());
										}

									}

									engine.getDataStore()
											.get(TradingStore.DATA_ORDER)
											.addRow(vorder, false, false);
								} catch (Exception ex) {
									ex.printStackTrace();
									log.warn(Utils.logException(ex));
								}
							}
						});
					} catch (Exception ex) {
						ex.printStackTrace();
						log.warn(Utils.logException(ex));
					}

				} catch (Exception ex) {
					log.warn(Utils.logException(ex));



				}

			}
		});		
		map.put(EVENT_ORDERMATRIX_REPORT, new EventHandler() {
			@Override
			public void execute(Event param) {
				Event e = null;
				Message message = (Message) param.getParam().get(PARAM_FIX);
				
				log.info("execute EVENT_ORDERMATRIX_REPORT");
				
				try {
					log.info("get string 7000 : "+message.getString(7000));
					e = new Event(EVENT_CREATEORDERMATRIX);
					e.setField(PARAM_FIX, message);
					pushEvent(e);
					return;
				} catch (Exception ex) {
					
				}
				
			}
			
		});
		map.put(EVENT_EXECUTION_REPORT, new EventHandler() {
			public void execute(Event param) {
				try {
					Event e = null;
					Message message = (Message) param.getParam().get(PARAM_FIX);

					log.info("execute EVENT_EXECUTEREPORT  "
							+ (message.isSetField(ClOrdID.FIELD) == true ? message
									.getString(ClOrdID.FIELD)
									: "ClordID not found" + ":")
							// + message.getString(OrdStatus.FIELD) + " "
							+ param.getParam().get(PARAM_FIX).toString());
					ExecBroker broker = new ExecBroker();
					// Replace 2011-02-28 untuk automatic withdrawal
					/*

					 * try { message.getField(broker); log.info(
					 * "found execution report for advertising or negdeal list"
					 * ); e = new Event(EVENT_ADVANDNEGDEALLIST);
					 * e.setField(PARAM_FIX, message); pushEvent(e); return; }


					 * catch (FieldNotFound ex) { // continue because this
					 * execute report for transaction }
					 */




					try {
						message.getField(broker);
						String handlins = "";
						//Valdhy20141217
						//System.err.println("YOSEP:"+message.getString(OrderID.FIELD)); 
						/*Order order = (Order) engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getDataByKey(
										new Object[] {
												message.getString(ClOrdID.FIELD),
												"0" });
						order.setMarketOrderId(message.getString(OrderID.FIELD));
						int idx = engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getIndexByKey(
										new String[] {
												order.getId(),
												order.getTradeNo() });
						engine.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.refresh(idx);*/
						if (message.isSetField(HandlInst.FIELD))
							handlins = message.getString(HandlInst.FIELD);
						if (!handlins.equals("1")) {
							//log.info("found execution report for advertising or negdeal list");
							e = new Event(EVENT_ADVANDNEGDEALLIST);
							e.setField(PARAM_FIX, message);
							pushEvent(e);
							return;
						}
					} catch (FieldNotFound ex) {
						// continue because this execute report for transaction

					}
					// End Penambahan 2011-02-28 --


					TransactTime time = new TransactTime(OrderIDGenerator
							.getTime());

					try {
						message.getField(time);
					} catch (FieldNotFound ex) {
						time.setValue(OrderIDGenerator.getTime());
					}

					String strStatus = "";
					String atsStatus = "";


					String gtcStatus = "";//*gtc
				//*gtc awal
					if (message.isSetField(999310) == true) {
						gtcStatus = message.getString(999310);
					} else {
						if (message.isSetField(atsbdfilter) == true) {
							atsStatus = message.getString(atsbdfilter);
						}

						else {
							strStatus = message.getString(OrdStatus.FIELD);
						}
						//System.out.println("===" + gtcStatus + strStatus);
					}
					//*gtc akhir


					if (strStatus.equals(Order.C_ENTRY)
							|| strStatus.equals(Order.C_REQUEST_WITHDRAW)) {
						e = new Event(EVENT_ACK);
						String orderid = message.getString(ClOrdID.FIELD);
						e.setField("clordid", orderid);
						e.setField("status", strStatus);
						try {
							e.setField("description", message.getString(7001));
						} catch (Exception ex) {
							try {
								e.setField("description",
										message.getString(Text.FIELD));
							} catch (Exception ep) {
								e.setField("description", "");
							}
						}
						pushEvent(e);
						try {
							if (!strStatus.equals(Order.C_REQUEST_WITHDRAW)) {
								e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
								e.setField("clordid",
										message.getString(ClOrdID.FIELD));
								e.setField("" + PARAM_ptlbefore, new Double(
										message.getDouble(PARAM_ptlbefore)));
								e.setField(
										"" + PARAM_pavportbefore,
										new Double(message
												.getDouble(PARAM_pavportbefore)));
								e.setField(
										"" + PARAM_plqvaluebefore,
										new Double(
												message.getDouble(PARAM_plqvaluebefore)));
								e.setField("" + PARAM_pratiobefore, new Double(
										message.getDouble(PARAM_pratiobefore)));
								e.setField("" + PARAM_pisoverlimit,
										message.getString(PARAM_pisoverlimit));
								e.setField("" + PARAM_pisshortsell,
										message.getString(PARAM_pisshortsell));
								e.setField(
										"" + PARAM_curtl,
										new Double(message
												.getDouble(PARAM_curtl)));
								e.setField("" + PARAM_pavport, new Double(
										message.getDouble(PARAM_pavport)));
								e.setField("" + PARAM_pcurratio, new Double(
										message.getDouble(PARAM_pcurratio)));
								e.setField(
										"" + PARAM_pbuy,
										new Double(message
												.getDouble(PARAM_pbuy)));
								e.setField(
										"" + PARAM_psell,
										new Double(message
												.getDouble(PARAM_psell)));
								e.setField("" + PARAM_pnetac, new Double(
										message.getDouble(PARAM_pnetac)));
								e.setField(
										"" + PARAM_pbid,
										new Double(message
												.getDouble(PARAM_pbid)));
								e.setField("" + PARAM_poffer, new Double(
										message.getDouble(PARAM_poffer)));
								e.setField("" + PARAM_pstockval, new Double(
										message.getDouble(PARAM_pstockval)));
								e.setField("" + PARAM_pmarketval, new Double(
										message.getDouble(PARAM_pmarketval)));
								e.setField("" + PARAM_plqvalue, new Double(
										message.getDouble(PARAM_plqvalue)));
								e.setField("" + PARAM_ptopup, new Double(
										message.getDouble(PARAM_ptopup)));
								e.setField("" + PARAM_pforcesell, new Double(
										message.getDouble(PARAM_pforcesell)));
								e.setField("" + PARAM_pcreditlimit, new Double(
										message.getDouble(PARAM_pcreditlimit)));
								e.setField("" + PARAM_pdeposit, new Double(
										message.getDouble(PARAM_pdeposit)));
								e.setField("" + PARAM_pcurvol, new Double(
										message.getDouble(PARAM_pcurvol)));
								e.setField("" + PARAM_pavgprice, new Double(
										message.getDouble(PARAM_pavgprice)));

								if (message.isSetField(PARAM_pwithdraw)) {
									e.setField(
											"" + PARAM_pwithdraw,
											new Double(message
													.getDouble(PARAM_pwithdraw)));
								}
								pushEvent(e);
							}
						} catch (Exception ex) {
							// ex.printStackTrace();
							log.warn("execution report without RM tag(s)");
						}
						try {
							Order order = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getDataByKey(new Object[] { orderid, "0" });
							if (order != null) {
								if (!order.getOrderIDContra().equals("")) {
									e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
									e.setField("clordid",
											order.getOrderIDContra());
									e.setField(
											"" + PARAM_ptlbefore,
											new Double(
													message.getDouble(PARAM_ptlbeforeold)));
									e.setField(
											"" + PARAM_pavportbefore,
											new Double(
													message.getDouble(PARAM_pavportbeforeold)));
									e.setField(
											"" + PARAM_plqvaluebefore,
											new Double(
													message.getDouble(PARAM_plqvaluebeforeold)));
									e.setField(
											"" + PARAM_pratiobefore,
											new Double(
													message.getDouble(PARAM_pratiobeforeold)));
									e.setField("" + PARAM_pisoverlimit, message
											.getString(PARAM_isoverlimitold));
									e.setField("" + PARAM_pisshortsell, message
											.getString(PARAM_isshortsellold));

									e.setField("" + PARAM_curtl, new Double(
											message.getDouble(PARAM_curtlold)));
									e.setField(
											"" + PARAM_pavport,
											new Double(
													message.getDouble(PARAM_pavportold)));
									e.setField(
											"" + PARAM_pcurratio,
											new Double(
													message.getDouble(PARAM_pcurratioold)));
									e.setField("" + PARAM_pbuy, new Double(
											message.getDouble(PARAM_pbuyold)));
									e.setField("" + PARAM_psell, new Double(
											message.getDouble(PARAM_psellold)));
									e.setField("" + PARAM_pnetac, new Double(
											message.getDouble(PARAM_pnetacold)));
									e.setField("" + PARAM_pbid, new Double(
											message.getDouble(PARAM_pbidold)));
									e.setField("" + PARAM_poffer, new Double(
											message.getDouble(PARAM_pofferold)));
									e.setField(
											"" + PARAM_pstockval,
											new Double(
													message.getDouble(PARAM_pstockvalold)));
									e.setField(
											"" + PARAM_pmarketval,
											new Double(
													message.getDouble(PARAM_pmarketvalold)));
									e.setField(
											"" + PARAM_plqvalue,
											new Double(
													message.getDouble(PARAM_plqvalueold)));
									e.setField("" + PARAM_ptopup, new Double(
											message.getDouble(PARAM_ptopupold)));
									e.setField(
											"" + PARAM_pforcesell,
											new Double(
													message.getDouble(PARAM_pforcesellold)));
									e.setField(
											"" + PARAM_pcreditlimit,
											new Double(
													message.getDouble(PARAM_pcreditlimitold)));
									e.setField(
											"" + PARAM_pdeposit,
											new Double(
													message.getDouble(PARAM_pdepositold)));
									e.setField(
											"" + PARAM_pcurvol,
											new Double(
													message.getDouble(PARAM_pcurvolold)));
									e.setField(
											"" + PARAM_pavgprice,
											new Double(
													message.getDouble(PARAM_pavgpriceold)));
									e.setField(
											"" + PARAM_pwithdraw,
											new Double(
													message.getDouble(PARAM_pwithdrawold)));
									pushEvent(e);
								}
							}
						} catch (Exception ex) {
							ex.printStackTrace();
							log.warn("execution report without RM tag(s)");
						}
//*gtc awal
					} else if (gtcStatus.equals(Order.C_ENTRY)) {
						e = new Event(EVENT_ACK_GTC);
						Order GTC2 = (Order) param.getParam().get(PARAM_GTC);
						GTC2.setStatus(Order.C_OPEN);
						String orderid = message.getString(ClOrdID.FIELD);
						e.setField("clordid", orderid);
						e.setField("status", gtcStatus);
						try {
							e.setField("description", message.getString(7001));
						} catch (Exception ex) {
							try {
								e.setField("description",
										message.getString(Text.FIELD));
							} catch (Exception ep) {
								e.setField("description", "");
							}
						}
						pushEvent(e);
						try {
							Order GTC = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_GTC)
									.getDataByKey(new Object[] { orderid, "0" });
							
						} catch (Exception ex) {
							ex.printStackTrace();
							log.warn("execution report without RM tag(s)");
						}
					} else if (gtcStatus.equals(Order.C_REQUEST_WITHDRAW)) {
						e = new Event(EVENT_ACK_GTC);
						Order GTC2 = (Order) param.getParam().get(PARAM_GTC);
						GTC2.setStatus(Order.C_WITHDRAW);
						String orderid = message.getString(ClOrdID.FIELD);
						e.setField("clordid", orderid);
						e.setField("status", gtcStatus);
						try {
							e.setField("description", message.getString(7001));
						} catch (Exception ex) {
							try {
								e.setField("description",
										message.getString(Text.FIELD));
							} catch (Exception ep) {
								e.setField("description", "");
							}
						}
						pushEvent(e);
						
						try {
							Order GTC = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_GTC)
									.getDataByKey(new Object[] { orderid, "0" });
						
						} catch (Exception ex) {
							ex.printStackTrace();
							log.warn("execution report without RM tag(s)");
						}
					} else if (strStatus.equals(Order.C_REQUEST_AMEND)) {
						e = new Event(EVENT_ACK);
						e.setField("clordid", message.getString(ClOrdID.FIELD));
						e.setField("status", strStatus);
						try {
							e.setField("description", message.getString(7001));
						} catch (Exception ex) {
							try {
								e.setField("description",
										message.getString(Text.FIELD));
							} catch (Exception ep) {
								e.setField("description", "");
							}
						}
						pushEvent(e);
						try {
							e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField(
									"" + PARAM_curtl,
									new Double(message
											.getDouble(PARAM_curtlold)));
							e.setField(
									"" + PARAM_pavport,
									new Double(message
											.getDouble(PARAM_pavportold)));
							e.setField(
									"" + PARAM_pcurratio,
									new Double(message
											.getDouble(PARAM_pcurratioold)));
							e.setField(
									"" + PARAM_pbuy,
									new Double(message.getDouble(PARAM_pbuyold)));
							e.setField(
									"" + PARAM_psell,
									new Double(message
											.getDouble(PARAM_psellold)));
							e.setField(
									"" + PARAM_pnetac,
									new Double(message
											.getDouble(PARAM_pnetacold)));
							e.setField(
									"" + PARAM_pbid,
									new Double(message.getDouble(PARAM_pbidold)));
							e.setField(
									"" + PARAM_poffer,
									new Double(message
											.getDouble(PARAM_pofferold)));
							e.setField(
									"" + PARAM_pstockval,
									new Double(message
											.getDouble(PARAM_pstockvalold)));
							e.setField("" + PARAM_pmarketval, new Double(
									message.getDouble(PARAM_pmarketvalold)));
							e.setField(
									"" + PARAM_plqvalue,
									new Double(message
											.getDouble(PARAM_plqvalueold)));
							e.setField(
									"" + PARAM_ptopup,
									new Double(message
											.getDouble(PARAM_ptopupold)));
							e.setField("" + PARAM_pforcesell, new Double(
									message.getDouble(PARAM_pforcesellold)));
							e.setField("" + PARAM_pcreditlimit, new Double(
									message.getDouble(PARAM_pcreditlimitold)));
							e.setField(
									"" + PARAM_pdeposit,
									new Double(message
											.getDouble(PARAM_pdepositold)));
							e.setField(
									"" + PARAM_pcurvol,
									new Double(message
											.getDouble(PARAM_pcurvolold)));
							e.setField(
									"" + PARAM_pavgprice,
									new Double(message
											.getDouble(PARAM_pavgpriceold)));
							e.setField(
									"" + PARAM_pwithdraw,
									new Double(message
											.getDouble(PARAM_pwithdrawold)));

							pushEvent(e);
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
							//log.error(Utils.logException(ex));
						}
						try {
							Order order = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getDataByKey(
											new Object[] {
													message.getString(ClOrdID.FIELD),
													"0" });
							if (order != null) {
								e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
								e.setField("clordid", order.getNextOrderId());
								e.setField("" + PARAM_ptlbefore, new Double(
										message.getDouble(PARAM_ptlbefore)));
								e.setField(
										"" + PARAM_pavportbefore,
										new Double(message
												.getDouble(PARAM_pavportbefore)));
								e.setField(
										"" + PARAM_plqvaluebefore,
										new Double(
												message.getDouble(PARAM_plqvaluebefore)));
								e.setField("" + PARAM_pratiobefore, new Double(
										message.getDouble(PARAM_pratiobefore)));
								e.setField("" + PARAM_pisoverlimit,
										message.getString(PARAM_pisoverlimit));
								e.setField("" + PARAM_pisshortsell,
										message.getString(PARAM_pisshortsell));
								e.setField(
										"" + PARAM_curtl,
										new Double(message
												.getDouble(PARAM_curtl)));
								e.setField("" + PARAM_pavport, new Double(
										message.getDouble(PARAM_pavport)));
								e.setField("" + PARAM_pcurratio, new Double(
										message.getDouble(PARAM_pcurratio)));
								e.setField(
										"" + PARAM_pbuy,
										new Double(message
												.getDouble(PARAM_pbuy)));
								e.setField(
										"" + PARAM_psell,
										new Double(message
												.getDouble(PARAM_psell)));
								e.setField("" + PARAM_pnetac, new Double(
										message.getDouble(PARAM_pnetac)));
								e.setField(
										"" + PARAM_pbid,
										new Double(message
												.getDouble(PARAM_pbid)));
								e.setField("" + PARAM_poffer, new Double(
										message.getDouble(PARAM_poffer)));
								e.setField("" + PARAM_pstockval, new Double(
										message.getDouble(PARAM_pstockval)));
								e.setField("" + PARAM_pmarketval, new Double(
										message.getDouble(PARAM_pmarketval)));
								e.setField("" + PARAM_plqvalue, new Double(
										message.getDouble(PARAM_plqvalue)));
								e.setField("" + PARAM_ptopup, new Double(
										message.getDouble(PARAM_ptopup)));
								e.setField("" + PARAM_pforcesell, new Double(
										message.getDouble(PARAM_pforcesell)));
								e.setField("" + PARAM_pcreditlimit, new Double(
										message.getDouble(PARAM_pcreditlimit)));
								e.setField("" + PARAM_pdeposit, new Double(
										message.getDouble(PARAM_pdeposit)));
								e.setField("" + PARAM_pcurvol, new Double(
										message.getDouble(PARAM_pcurvol)));
								e.setField("" + PARAM_pavgprice, new Double(
										message.getDouble(PARAM_pavgprice)));
								try {
									e.setField(
											"" + PARAM_pwithdraw,
											new Double(message
													.getDouble(PARAM_pwithdraw)));
								} catch (Exception ex) {
									e.setField(
											"" + PARAM_pwithdraw,
											new Double(
													message.getDouble(PARAM_pwithdrawnew)));
								}
								pushEvent(e);
							}
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
							//log.error(Utils.logException(ex));
						}
						//*gtc awal
					} else if (gtcStatus.equals(Order.C_REQUEST_AMEND)) {
						e = new Event(EVENT_ACK);
						e.setField("clordid", message.getString(ClOrdID.FIELD));
						e.setField("status", gtcStatus);
						try {
							e.setField("description", message.getString(7001));
						} catch (Exception ex) {
							try {
								e.setField("description",
										message.getString(Text.FIELD));
							} catch (Exception ep) {
								e.setField("description", "");
							}
						}
						pushEvent(e);
						
						try {
							Order GTC = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_GTC)
									.getDataByKey(
											new Object[] {
													message.getString(ClOrdID.FIELD),
													"0" });
							if (GTC != null) {
								e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
								e.setField("clordid", GTC.getNextOrderId());
								e.setField("" + PARAM_ptlbefore, new Double(
										message.getDouble(PARAM_ptlbefore)));
								e.setField(
										"" + PARAM_pavportbefore,
										new Double(message
												.getDouble(PARAM_pavportbefore)));
								e.setField(
										"" + PARAM_plqvaluebefore,
										new Double(
												message.getDouble(PARAM_plqvaluebefore)));
								e.setField("" + PARAM_pratiobefore, new Double(
										message.getDouble(PARAM_pratiobefore)));
								e.setField("" + PARAM_pisoverlimit,
										message.getString(PARAM_pisoverlimit));
								e.setField("" + PARAM_pisshortsell,
										message.getString(PARAM_pisshortsell));
								e.setField(
										"" + PARAM_curtl,
										new Double(message
												.getDouble(PARAM_curtl)));
								e.setField("" + PARAM_pavport, new Double(
										message.getDouble(PARAM_pavport)));
								e.setField("" + PARAM_pcurratio, new Double(
										message.getDouble(PARAM_pcurratio)));
								e.setField(
										"" + PARAM_pbuy,
										new Double(message
												.getDouble(PARAM_pbuy)));
								e.setField(
										"" + PARAM_psell,
										new Double(message
												.getDouble(PARAM_psell)));
								e.setField("" + PARAM_pnetac, new Double(
										message.getDouble(PARAM_pnetac)));
								e.setField(
										"" + PARAM_pbid,
										new Double(message
												.getDouble(PARAM_pbid)));
								e.setField("" + PARAM_poffer, new Double(
										message.getDouble(PARAM_poffer)));
								e.setField("" + PARAM_pstockval, new Double(
										message.getDouble(PARAM_pstockval)));
								e.setField("" + PARAM_pmarketval, new Double(
										message.getDouble(PARAM_pmarketval)));
								e.setField("" + PARAM_plqvalue, new Double(
										message.getDouble(PARAM_plqvalue)));
								e.setField("" + PARAM_ptopup, new Double(
										message.getDouble(PARAM_ptopup)));
								e.setField("" + PARAM_pforcesell, new Double(
										message.getDouble(PARAM_pforcesell)));
								e.setField("" + PARAM_pcreditlimit, new Double(
										message.getDouble(PARAM_pcreditlimit)));
								e.setField("" + PARAM_pdeposit, new Double(
										message.getDouble(PARAM_pdeposit)));
								e.setField("" + PARAM_pcurvol, new Double(
										message.getDouble(PARAM_pcurvol)));
								e.setField("" + PARAM_pavgprice, new Double(
										message.getDouble(PARAM_pavgprice)));
								try {
									e.setField(
											"" + PARAM_pwithdraw,
											new Double(message
													.getDouble(PARAM_pwithdraw)));
								} catch (Exception ex) {
									e.setField(
											"" + PARAM_pwithdraw,
											new Double(
													message.getDouble(PARAM_pwithdrawnew)));
								}
								pushEvent(e);
							}
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
							log.error(Utils.logException(ex));
						}
						//*gtc akhir
					} else if (strStatus.equals(Order.C_OPEN)
							|| strStatus.equals(Order.C_TEMPORARY)) {
						try {
							System.out.println("create event ok");
							e = new Event(EVENT_CREATEORDER_OK);
							e.setField("orderid",
									message.getString(OrderID.FIELD));
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							try {
								e.setField("opentime",
										message.getUtcTimeStamp(666680));
							} catch (Exception ex) {
								e.setField("opentime", time.getValue());
							}
							try {
								e.setField("opensendtime",
										message.getUtcTimeStamp(666681));
							} catch (Exception ex) {
								e.setField("opensendtime", time.getValue());
							}

							pushEvent(e);
							if (TradingSetting.getPopup()) {
								String myuserid = engine.getConnection()
										.getUserId();

								Order order = (Order) engine
										.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.getDataByKey(
												new Object[] {
														message.getString(ClOrdID.FIELD),
														"0" });

								if (order != null
										&& order.getEntryBy().equals(myuserid)) {
									/*
									 * Utils.showMessage( "order " + message
									 * .getString(ClOrdID.FIELD) +
									 * " accepted with market orderid: " +
									 * message .getString(OrderID.FIELD), null);
									 */
//									String bos = order.getBOS().equals("1") ? "beli"
//											: "jual";
									/*
									 * Utils.showMessage("Transaksi "+bos+" "+
									 * message
									 * .getString(ClOrdID.FIELD)+" "+message
									 * .getDouble
									 * (Price.FIELD)+" "+message.getDouble
									 * (OrderQty.FIELD)+" Accept", null);
									 */



									//:TODO order ammend Open 18032016
									if(mapAmendOrder.get(order.getId())== null)
									{
										Utils.showMessage(
											TradingEventDispatcher.this
													.genConfirm(order, message,
															"ACCEPT"), null);
									}


								}
							}
						} catch (Exception ex) {
							//log.error(Utils.logException(ex));
						}
						//*gtc awal
					} else if (gtcStatus.equals(Order.C_OPEN)
							|| gtcStatus.equals(Order.C_TEMPORARY)) {
						try {
							e = new Event(EVENT_CREATEGTC_OK);
							e.setField("orderid",
									message.getString(OrderID.FIELD));
							Order GTC2 = (Order) param.getParam().get(PARAM_GTC);// perubahan u/status O 25/11/13
							GTC2.setStatus(Order.C_OPEN);
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							try {
								e.setField("opentime",
										message.getUtcTimeStamp(666680));
							} catch (Exception ex) {
								e.setField("opentime", time.getValue());
							}
							try {
								e.setField("opensendtime",
										message.getUtcTimeStamp(666681));
							} catch (Exception ex) {
								e.setField("opensendtime", time.getValue());
							}

							pushEvent(e);
							if (TradingSetting.getPopup()) {
								String myuserid = engine.getConnection()
										.getUserId();

								Order GTC = (Order) engine
										.getDataStore()
										.get(TradingStore.DATA_GTC)
										.getDataByKey(
												new Object[] {
														message.getString(ClOrdID.FIELD),
														"2" });

								if (GTC != null
										&& GTC.getEntryBy().equals(myuserid)) {
									
									String bos = GTC.getBOS().equals("1") ? "beli"
											: "jual";
									Utils.showMessage(
											TradingEventDispatcher.this
													.genConfirm(GTC, message,
															"ACCEPT"), null);

								}
							}
						} catch (Exception ex) {
							log.error(Utils.logException(ex));
						}
						//*gtc akhir
					} else if (strStatus.equals(Order.C_PARTIAL_MATCH)
							|| strStatus.equals(Order.C_FULL_MATCH)) {
						try {
							System.out.println("message match "+message.toString());
							e = new Event(EVENT_RECEIVEMATCH);
							e.setField("orderid",
									message.getString(OrderID.FIELD));
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField("matchtime", time.getValue());
							e.setField("tradeno",
									message.getString(SecondaryOrderID.FIELD));
							e.setField("matchvolume",
									new Double(message.getDouble(CumQty.FIELD)));
							e.setField("matchprice",
									new Double(message.getDouble(Price.FIELD)));
							e.setField("contratrader",
									message.getString(ContraTrader.FIELD));
							e.setField("contrabroker",
									message.getString(ContraBroker.FIELD));
							Order order = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getDataByKey(
											new Object[] {
													message.getString(ClOrdID.FIELD),
													"0" });

							String userid = "";
							if (order != null)
								userid = order.getEntryBy();

							pushEvent(e);



							if (TradingSetting.getPlaySound()) {


								// Utils.showMessage("masuk sound", null);


								// Toolkit tk = Toolkit.getDefaultToolkit();
								// tk.beep();
								playSound("match.WAV");
							} // else {
								// Utils.showMessage("tidak masuk sound", null);
								// }



							if (TradingSetting.getPopup()) {
								String myuserid = engine.getConnection()
										.getUserId();

								if (myuserid.equals(userid)) {
									// Utils.showMessage(
									// "order "
									// + message
									// .getString(ClOrdID.FIELD)
									// + " match", null);
									String bos = order.getBOS().equals("1") ? "beli"
											: "jual";

									/*
									 * Utils.showMessage("Transaksi "+bos+" "+
									 * message
									 * .getString(ClOrdID.FIELD)+" "+message
									 * .getDouble
									 * (Price.FIELD)+" "+message.getDouble
									 * (OrderQty.FIELD)+" Match", null);
									 */

									// Toolkit tk = Toolkit.getDefaultToolkit();
									// tk.beep();
									Utils.showMessage(
											TradingEventDispatcher.this
													.genConfirm(order, message,
															"MATCH"), null);


								}
							}
						} catch (Exception ex) {
							ex.printStackTrace();
							try {
								e = new Event(EVENT_CREATEORDER_OK);
								e.setField("orderid",
										message.getString(OrderID.FIELD));
								e.setField("clordid",
										message.getString(ClOrdID.FIELD));
								log.warn("found match but for amend new order");
								try {
									e.setField("opentime",
											message.getUtcTimeStamp(666680));
								} catch (Exception exs) {
									e.setField("opentime", time.getValue());
								}
								try {
									e.setField("opensendtime",
											message.getUtcTimeStamp(666681));
								} catch (Exception exs) {
									e.setField("opensendtime", time.getValue());
								}
								pushEvent(e);
							} catch (Exception exs) {
								try {
									e = new Event(EVENT_CREATEORDER_OK);
									e.setField("clordid",
											message.getString(ClOrdID.FIELD));
									//log.info("found match but status only");
									pushEvent(e);
								} catch (Exception exz) {
									//log.error(Utils.logException(exz));
								}
							}
						}
						if (message.getDouble(CumQty.FIELD) > 0) {
							try {
								e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
								e.setField("clordid",
										message.getString(ClOrdID.FIELD));
								e.setField(
										"" + PARAM_curtl,
										new Double(message
												.getDouble(PARAM_curtl)));
								e.setField("" + PARAM_pavport, new Double(
										message.getDouble(PARAM_pavport)));
								e.setField("" + PARAM_pcurratio, new Double(
										message.getDouble(PARAM_pcurratio)));
								e.setField(
										"" + PARAM_pbuy,
										new Double(message
												.getDouble(PARAM_pbuy)));
								e.setField(
										"" + PARAM_psell,
										new Double(message
												.getDouble(PARAM_psell)));
								e.setField("" + PARAM_pnetac, new Double(
										message.getDouble(PARAM_pnetac)));
								e.setField(
										"" + PARAM_pbid,
										new Double(message
												.getDouble(PARAM_pbid)));
								e.setField("" + PARAM_poffer, new Double(
										message.getDouble(PARAM_poffer)));
								e.setField("" + PARAM_pstockval, new Double(
										message.getDouble(PARAM_pstockval)));
								e.setField("" + PARAM_pmarketval, new Double(
										message.getDouble(PARAM_pmarketval)));
								e.setField("" + PARAM_plqvalue, new Double(
										message.getDouble(PARAM_plqvalue)));
								e.setField("" + PARAM_ptopup, new Double(
										message.getDouble(PARAM_ptopup)));
								e.setField("" + PARAM_pforcesell, new Double(
										message.getDouble(PARAM_pforcesell)));
								e.setField("" + PARAM_pcreditlimit, new Double(
										message.getDouble(PARAM_pcreditlimit)));
								e.setField("" + PARAM_pdeposit, new Double(
										message.getDouble(PARAM_pdeposit)));
								e.setField("" + PARAM_pcurvol, new Double(
										message.getDouble(PARAM_pcurvol)));
								e.setField("" + PARAM_pavgprice, new Double(
										message.getDouble(PARAM_pavgprice)));
								e.setField("" + PARAM_pwithdraw, new Double(
										message.getDouble(PARAM_pwithdraw)));
								pushEvent(e);
							} catch (Exception ex) {
								log.warn("execution report without RM tag(s)");
								//log.error(Utils.logException(ex));
							}
						}
						//*gtc awal
					} else if (gtcStatus.equals(Order.C_PARTIAL_MATCH)
							|| gtcStatus.equals(Order.C_FULL_MATCH)) {
						try {
							e = new Event(EVENT_RECEIVEMATCH);
							e.setField("orderid",
									message.getString(OrderID.FIELD));
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField("matchtime", time.getValue());
							e.setField("tradeno",
									message.getString(SecondaryOrderID.FIELD));
							e.setField("matchvolume",
									new Double(message.getDouble(CumQty.FIELD)));
							e.setField("matchprice",
									new Double(message.getDouble(Price.FIELD)));
							e.setField("contratrader",
									message.getString(ContraTrader.FIELD));
							e.setField("contrabroker",
									message.getString(ContraBroker.FIELD));
							Order GTC = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_GTC)
									.getDataByKey(
											new Object[] {
													message.getString(ClOrdID.FIELD),
													"0" });

							String userid = "";
							if (GTC != null)
								userid = GTC.getEntryBy();

							pushEvent(e);

							if (TradingSetting.getPlaySound()) {

								// Utils.showMessage("masuk sound", null);

								// Toolkit tk = Toolkit.getDefaultToolkit();
								// tk.beep();
								playSound("match.WAV");
							} // else {
								// Utils.showMessage("tidak masuk sound", null);
								// }

							if (TradingSetting.getPopup()) {
								String myuserid = engine.getConnection()
										.getUserId();

								if (myuserid.equals(userid)) {
									// Utils.showMessage(
									// "order "
									// + message
									// .getString(ClOrdID.FIELD)
									// + " match", null);
									String bos = GTC.getBOS().equals("1") ? "beli"
											: "jual";

									/*
									 * Utils.showMessage("Transaksi "+bos+" "+
									 * message
									 * .getString(ClOrdID.FIELD)+" "+message
									 * .getDouble
									 * (Price.FIELD)+" "+message.getDouble
									 * (OrderQty.FIELD)+" Match", null);
									 */
									// Toolkit tk = Toolkit.getDefaultToolkit();
									// tk.beep();
									Utils.showMessage(
											TradingEventDispatcher.this
													.genConfirm(GTC, message,
															"MATCH"), null);
									
									if (message.getString(999128).startsWith("ATS")) {
										engine.getConnection().refreshAts("%");
									} else {
										Utils.showMessage(message.getString(999128), null);
									}

								}
							}
						} catch (Exception ex) {
							try {
								e = new Event(EVENT_CREATEORDER_OK);
								e.setField("orderid",
										message.getString(OrderID.FIELD));
								e.setField("clordid",
										message.getString(ClOrdID.FIELD));
								log.warn("found match but for amend new order");
								try {
									e.setField("opentime",
											message.getUtcTimeStamp(666680));
								} catch (Exception exs) {
									e.setField("opentime", time.getValue());
								}
								try {
									e.setField("opensendtime",
											message.getUtcTimeStamp(666681));
								} catch (Exception exs) {
									e.setField("opensendtime", time.getValue());
								}
								pushEvent(e);
							} catch (Exception exs) {
								try {
									e = new Event(EVENT_CREATEGTC);
									e.setField("clordid",
											message.getString(ClOrdID.FIELD));
									log.info("found match but status only");
									pushEvent(e);
								} catch (Exception exz) {
									log.error(Utils.logException(exz));
								}
							}
						}
						//*gtc akhir
					} else if (strStatus.equals(Order.C_WITHDRAW)) {
						try {
							log.info(message.toString());
							e = new Event(EVENT_REQUESTWITHDRAW_OK);
							e.setField("orderid",
									message.getString(OrderID.FIELD));
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField("withdrawtime", time.getValue());
							e.setField("withdrawsendtime",
									message.getUtcTimeStamp(666680));

							Order order = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getDataByKey(
											new Object[] {
													message.getString(ClOrdID.FIELD),
													"0" });

							String userid = "";
							if (order != null) {
								userid = order.getWithdrawBy();
							}

							pushEvent(e);
							if (TradingSetting.getPopup()) {
								String myuserid = engine.getConnection()
										.getUserId();

								if (myuserid.equals(userid)) {
									Utils.showMessage(
											"order "
													+ message
															.getString(ClOrdID.FIELD)
													+ " withdraw", null);
								}
							}
						} catch (Exception ex) {
							//log.error(Utils.logException(ex));
						}
						try {
							e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField("" + PARAM_curtl,
									new Double(message.getDouble(PARAM_curtl)));
							e.setField(
									"" + PARAM_pavport,
									new Double(message.getDouble(PARAM_pavport)));
							e.setField(
									"" + PARAM_pcurratio,
									new Double(message
											.getDouble(PARAM_pcurratio)));
							e.setField("" + PARAM_pbuy,
									new Double(message.getDouble(PARAM_pbuy)));
							e.setField("" + PARAM_psell,
									new Double(message.getDouble(PARAM_psell)));
							e.setField("" + PARAM_pnetac,
									new Double(message.getDouble(PARAM_pnetac)));
							e.setField("" + PARAM_pbid,
									new Double(message.getDouble(PARAM_pbid)));
							e.setField("" + PARAM_poffer,
									new Double(message.getDouble(PARAM_poffer)));
							e.setField(
									"" + PARAM_pstockval,
									new Double(message
											.getDouble(PARAM_pstockval)));
							e.setField("" + PARAM_pmarketval, new Double(
									message.getDouble(PARAM_pmarketval)));
							e.setField(
									"" + PARAM_plqvalue,
									new Double(message
											.getDouble(PARAM_plqvalue)));
							e.setField("" + PARAM_ptopup,
									new Double(message.getDouble(PARAM_ptopup)));
							e.setField("" + PARAM_pforcesell, new Double(
									message.getDouble(PARAM_pforcesell)));
							e.setField("" + PARAM_pcreditlimit, new Double(
									message.getDouble(PARAM_pcreditlimit)));
							e.setField(
									"" + PARAM_pdeposit,
									new Double(message
											.getDouble(PARAM_pdeposit)));
							e.setField(
									"" + PARAM_pcurvol,
									new Double(message.getDouble(PARAM_pcurvol)));
							e.setField(
									"" + PARAM_pavgprice,
									new Double(message
											.getDouble(PARAM_pavgprice)));
							e.setField(
									"" + PARAM_pwithdraw,
									new Double(message
											.getDouble(PARAM_pwithdraw)));
							pushEvent(e);
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
							//log.error(Utils.logException(ex));
						}
					} else if (strStatus.equals(Order.C_AMEND)) {
						e = new Event(EVENT_REQUESTAMEND_OK);
						e.setField("orderid", message.getString(OrderID.FIELD));
						e.setField("clordid", message.getString(ClOrdID.FIELD));
						e.setField("amendtime", time.getValue());
						log.error(message.toString());
						Order ordercek = (Order) engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getDataByKey(
										new Object[] {
												message.getString(ClOrdID.FIELD),
												"0" });

						String userid = "";
						if (ordercek != null) {
							userid = ordercek.getAmendBy();
						}

						pushEvent(e);
						if (TradingSetting.getPopup()) {
							String myuserid = engine.getConnection()
									.getUserId();

							if (myuserid.equals(userid)) {
								Utils.showMessage(

										"Order "
												+ message
														.getString(ClOrdID.FIELD)

												+ " Amend Success", null);
							}
						}
						try {
							e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField(
									"" + PARAM_curtl,
									new Double(message
											.getDouble(PARAM_curtlold)));
							e.setField(
									"" + PARAM_pavport,
									new Double(message
											.getDouble(PARAM_pavportold)));
							e.setField(
									"" + PARAM_pcurratio,
									new Double(message
											.getDouble(PARAM_pcurratioold)));
							e.setField(
									"" + PARAM_pbuy,
									new Double(message.getDouble(PARAM_pbuyold)));
							e.setField(
									"" + PARAM_psell,
									new Double(message
											.getDouble(PARAM_psellold)));
							e.setField(
									"" + PARAM_pnetac,
									new Double(message
											.getDouble(PARAM_pnetacold)));
							e.setField(
									"" + PARAM_pbid,
									new Double(message.getDouble(PARAM_pbidold)));
							e.setField(
									"" + PARAM_poffer,
									new Double(message
											.getDouble(PARAM_pofferold)));
							e.setField(
									"" + PARAM_pstockval,
									new Double(message
											.getDouble(PARAM_pstockvalold)));
							e.setField("" + PARAM_pmarketval, new Double(
									message.getDouble(PARAM_pmarketvalold)));
							e.setField(
									"" + PARAM_plqvalue,
									new Double(message
											.getDouble(PARAM_plqvalueold)));
							e.setField(
									"" + PARAM_ptopup,
									new Double(message
											.getDouble(PARAM_ptopupold)));
							e.setField("" + PARAM_pforcesell, new Double(
									message.getDouble(PARAM_pforcesellold)));
							e.setField("" + PARAM_pcreditlimit, new Double(
									message.getDouble(PARAM_pcreditlimitold)));
							e.setField(
									"" + PARAM_pdeposit,
									new Double(message
											.getDouble(PARAM_pdepositold)));
							e.setField(
									"" + PARAM_pcurvol,
									new Double(message
											.getDouble(PARAM_pcurvolold)));
							e.setField(
									"" + PARAM_pavgprice,
									new Double(message
											.getDouble(PARAM_pavgpriceold)));
							e.setField(
									"" + PARAM_pwithdraw,
									new Double(message
											.getDouble(PARAM_pwithdrawold)));
							pushEvent(e);
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
							//log.error(Utils.logException(ex));
						}
						try {
							Order order = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getDataByKey(
											new Object[] {
													message.getString(ClOrdID.FIELD),
													"0" });
							if (!order.getNextOrderId().equals("")) {
								e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
								e.setField("clordid", order.getNextOrderId());
								e.setField(
										"" + PARAM_curtl,
										new Double(message
												.getDouble(PARAM_curtl)));
								e.setField("" + PARAM_pavport, new Double(
										message.getDouble(PARAM_pavport)));
								e.setField("" + PARAM_pcurratio, new Double(
										message.getDouble(PARAM_pcurratio)));
								e.setField(
										"" + PARAM_pbuy,
										new Double(message
												.getDouble(PARAM_pbuy)));
								e.setField(
										"" + PARAM_psell,
										new Double(message
												.getDouble(PARAM_psell)));
								e.setField("" + PARAM_pnetac, new Double(
										message.getDouble(PARAM_pnetac)));
								e.setField(
										"" + PARAM_pbid,
										new Double(message
												.getDouble(PARAM_pbid)));
								e.setField("" + PARAM_poffer, new Double(
										message.getDouble(PARAM_poffer)));
								e.setField("" + PARAM_pstockval, new Double(
										message.getDouble(PARAM_pstockval)));
								e.setField("" + PARAM_pmarketval, new Double(
										message.getDouble(PARAM_pmarketval)));
								e.setField("" + PARAM_plqvalue, new Double(
										message.getDouble(PARAM_plqvalue)));
								e.setField("" + PARAM_ptopup, new Double(
										message.getDouble(PARAM_ptopup)));
								e.setField("" + PARAM_pforcesell, new Double(
										message.getDouble(PARAM_pforcesell)));
								e.setField("" + PARAM_pcreditlimit, new Double(
										message.getDouble(PARAM_pcreditlimit)));
								e.setField("" + PARAM_pdeposit, new Double(
										message.getDouble(PARAM_pdeposit)));
								e.setField("" + PARAM_pcurvol, new Double(
										message.getDouble(PARAM_pcurvol)));
								e.setField("" + PARAM_pavgprice, new Double(
										message.getDouble(PARAM_pavgprice)));
								try {
									e.setField(
											"" + PARAM_pwithdraw,
											new Double(message
													.getDouble(PARAM_pwithdraw)));
								} catch (Exception ex) {
									e.setField(
											"" + PARAM_pwithdraw,
											new Double(
													message.getDouble(PARAM_pwithdrawnew)));
								}
								pushEvent(e);
							}
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
							//log.error(Utils.logException(ex));
						}
//*gtc awal
					} else if (gtcStatus.equals(Order.C_WITHDRAW)) {
						try {
							e = new Event(EVENT_REQUESTWITHDRAW_GTC);
							e.setField("orderid",
									message.getString(OrderID.FIELD));
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField("withdrawtime", time.getValue());
							e.setField("withdrawsendtime",
									message.getUtcTimeStamp(666680));

							Order GTC = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_GTC)
									.getDataByKey(
											new Object[] {
													message.getString(ClOrdID.FIELD),
													"0" });

							String userid = "";
							if (GTC != null) {
								userid = GTC.getWithdrawBy();
							}

							pushEvent(e);
							if (TradingSetting.getPopup()) {
								String myuserid = engine.getConnection()
										.getUserId();

								if (myuserid.equals(userid)) {
									Utils.showMessage(
											"gtc "
													+ message
															.getString(ClOrdID.FIELD)
													+ " withdraw", null);
								}
							}
						} catch (Exception ex) {
							log.error(Utils.logException(ex));
						}
					
					} else if (gtcStatus.equals(Order.C_AMEND)) {
						try {
							e = new Event(EVENT_REQUESTAMENDGTC_OK);
							e.setField("orderid",
									message.getString(OrderID.FIELD));
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField("amendtime", time.getValue());
							e.setField("amendsendtime",
									message.getUtcTimeStamp(666680));

							Order GTC = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_GTC)
									.getDataByKey(
											new Object[] { message
													.getString(ClOrdID.FIELD) });

							String userid = "";
							if (GTC != null) {
								userid = GTC.getEntryBy();
							}

							pushEvent(e);
							if (TradingSetting.getPopup()) {
								String myuserid = engine.getConnection()
										.getUserId();

								if (myuserid.equals(userid)) {
									Utils.showMessage(
											"gtc "
													+ message
															.getString(OrderID.FIELD)
													+ " amend", null);
								} else
									;
							}
						} catch (Exception ex) {
							log.error(Utils.logException(ex));
						}
//*gtc akhir
					} else if (strStatus.equals(Order.C_REJECTED)
							|| strStatus.equals(Order.C_REJECTED2)) {
						e = new Event(EVENT_CREATEORDER_FAILED);
						e.setField("clordid", message.getString(ClOrdID.FIELD));
						try {
							e.setField("description", message.getString(7001));
						} catch (Exception ex) {
							try {
								e.setField("description",
										message.getString(Text.FIELD));
							} catch (Exception ep) {
								e.setField("description", "");
							}
						}
						e.setField("status", "R");
						e.setField("rejecttime", time.getValue());

						Order order = (Order) engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getDataByKey(
										new Object[] {
												message.getString(ClOrdID.FIELD),
												"0" });

						String userid = "";
						if (order != null) {
							userid = order.getWithdrawBy();
							if (userid.isEmpty()) {
								userid = order.getEntryBy();
							}
						}

						pushEvent(e);
						if (TradingSetting.getPopup()) {
							String myuserid = engine.getConnection()
									.getUserId();

							if (myuserid.equals(userid)) {
								Utils.showMessage(
										"order "
												+ message
														.getString(ClOrdID.FIELD)
												+ " rejected\n"
												+ e.getField("description"),
										null);
							}
						}

						try {
							e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField("" + PARAM_curtl,
									new Double(message.getDouble(PARAM_curtl)));
							e.setField(
									"" + PARAM_pavport,
									new Double(message.getDouble(PARAM_pavport)));
							e.setField(
									"" + PARAM_pcurratio,
									new Double(message
											.getDouble(PARAM_pcurratio)));
							e.setField("" + PARAM_pbuy,
									new Double(message.getDouble(PARAM_pbuy)));
							e.setField("" + PARAM_psell,
									new Double(message.getDouble(PARAM_psell)));
							e.setField("" + PARAM_pnetac,
									new Double(message.getDouble(PARAM_pnetac)));
							e.setField("" + PARAM_pbid,
									new Double(message.getDouble(PARAM_pbid)));
							e.setField("" + PARAM_poffer,
									new Double(message.getDouble(PARAM_poffer)));
							e.setField(
									"" + PARAM_pstockval,
									new Double(message
											.getDouble(PARAM_pstockval)));
							e.setField("" + PARAM_pmarketval, new Double(
									message.getDouble(PARAM_pmarketval)));
							e.setField(
									"" + PARAM_plqvalue,
									new Double(message
											.getDouble(PARAM_plqvalue)));
							e.setField("" + PARAM_ptopup,
									new Double(message.getDouble(PARAM_ptopup)));
							e.setField("" + PARAM_pforcesell, new Double(
									message.getDouble(PARAM_pforcesell)));
							e.setField("" + PARAM_pcreditlimit, new Double(
									message.getDouble(PARAM_pcreditlimit)));
							e.setField(
									"" + PARAM_pdeposit,
									new Double(message
											.getDouble(PARAM_pdeposit)));
							e.setField(
									"" + PARAM_pcurvol,
									new Double(message.getDouble(PARAM_pcurvol)));
							e.setField(
									"" + PARAM_pavgprice,
									new Double(message
											.getDouble(PARAM_pavgprice)));
							e.setField(
									"" + PARAM_pwithdraw,
									new Double(message
											.getDouble(PARAM_pwithdraw)));
							pushEvent(e);
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
							//log.error(Utils.logException(ex));
						}
					} else if (strStatus.equals(Order.C_DELETE)) {
						e = new Event(EVENT_DELETEORDER_OK);
						e.setField("clordid", message.getString(ClOrdID.FIELD));
						try {
							e.setField("description",
									message.getString(Text.FIELD));
						} catch (Exception ex) {
							e.setField("description", "");
						}
						e.setField("status", strStatus);
						pushEvent(e);
					} else if (atsStatus.equals(Schedule.C_ENTRY_SUCCESS)) {
						try {
							e = new Event(EVENT_ENTRYSUCCESS);
							e.setField("atsid", message.getString(atsid));
							e.setField("atsstatus",
									message.getString(PARAM_ATS_STATUS));
							e.setField("validuntil",
									message.getString(PARAM_atsValidUntil));
							e.setField("entrytime",
									message.getUtcTimeStamp(PARAM_atsEntrytime));
							// e.setField("description",
							// message.getString(Text.FIELD));


							Schedule sc = (Schedule) engine
									.getDataStore()
									.get(TradingStore.DATA_SCHEDULELIST)
									.getDataByKey(
											new String[] { message
													.getString(atsid) });

//							System.out.println("ats id "
//									+ message.getString(atsid) + " e.field "
//									+ e.getField("atsid") + "\n "
//									+ sc.getcAtsid() + " -- " + sc.getcSecid()
//									+ " -- " + sc.getcEntrybuy() + " -= "
//									+ sc.getcWithdrawby());
							String userid = "";
							String clientid = "";
							if (sc != null) {
								userid = sc.getcEntrybuy();
								clientid = sc.getcClientid();
							}
							//System.out.println("schedule id null");


							pushEvent(e);
							String myuserid = engine.getConnection()
									.getUserId();


							if (TradingSetting.getPopup()) {
								if (myuserid.equals(userid)) {
									Utils.showMessage(
											"Ats id "

													+ message.getString(atsid)
													+ " entry success\n", null);






								}
							} else if (myuserid.equalsIgnoreCase(clientid)) {
								Event e2 = new Event(EVENT_REFRESH_ATS);
								e2.setField("accountid", "%");
								pushEvent(e);
							}


						} catch (Exception e2) {
							//System.out.println("ES --123 " + e2);
						}
					} else if (atsStatus.equals(Schedule.C_EXECUTED)) {
						try {
							e = new Event(EVENT_EXECUTED);
							e.setField("atsid", message.getString(atsid));
							e.setField("atsstatus",
									message.getString(PARAM_ATS_STATUS));
							e.setField("orderid",
									message.getString(ClOrdID.FIELD));
							e.setField("orderstatus",
									message.getString(PARAM_atsOrderStatus));
							e.setField("execby",
									message.getString(PARAM_atsExecby));
							e.setField("exectime",
									message.getUtcTimeStamp(PARAM_atsExectime));
							// e.setField("description",
							// message.getString(Text.FIELD));


							Schedule sc = (Schedule) engine
									.getDataStore()
									.get(TradingStore.DATA_SCHEDULELIST)
									.getDataByKey(
											new String[] { message
													.getString(atsid) });

							//System.out.println("schedule id null");



							pushEvent(e);
							//System.out.println("entry by " + sc.getcEntrybuy());


							String userid = "";
							String clientid = "";
							if (sc != null) {
								userid = sc.getcEntrybuy();
								clientid = sc.getcClientid();
							}
							String myuserid = engine.getConnection()
									.getUserId();


							if (TradingSetting.getPopup()) {
								if (myuserid.equals(userid)) {
									Utils.showMessage(
											"Ats id "

													+ message.getString(atsid)
													+ " executed \n", null);

								}
							} else if (myuserid.equalsIgnoreCase(clientid)) {
								Event e2 = new Event(EVENT_REFRESH_ATS);
								e2.setField("accountid", "%");
								pushEvent(e);
							}
						} catch (Exception e2) {
							//log.info(e2 + " error execute");
						}
					} else if (atsStatus.equals(Schedule.C_WITHDRAW_SUCCESS)) {
						try {
							e = new Event(EVENT_WITHDRAW_SUCCESS);
							e.setField("atsid", message.getString(atsid));
							e.setField("atsstatus",
									message.getString(PARAM_ATS_STATUS));
							e.setField("withdrawtime",
									message.getString(PARAM_atsWithdrawtime));
							// e.setField("description",
							// message.getString(Text.FIELD));
							e.setField("withdrawby",
									message.getString(PARAM_atsWithdrawBy));
							e.setField("withdrawbyTerminal", message
									.getString(PARAM_atsWithdrawTerminal));

							Schedule sc = (Schedule) engine
									.getDataStore()
									.get(TradingStore.DATA_SCHEDULELIST)
									.getDataByKey(
											new String[] { message
													.getString(atsid) });


							String userid = "";
							String clientid = "";
							if (sc != null) {
								userid = sc.getcEntrybuy();
								clientid = sc.getcClientid();
							}
							pushEvent(e);
							String myuserid = engine.getConnection()
									.getUserId();
							if (TradingSetting.getPopup()) {


								if (myuserid.equals(userid)) {
									Utils.showMessage(
											"Ats id "

													+ message.getString(atsid)
													+ " withdraw success\n",
											null);
								}
							} else if (myuserid.equalsIgnoreCase(clientid)) {
								Event e2 = new Event(EVENT_REFRESH_ATS);
								e2.setField("accountid", "%");
								pushEvent(e);
							}
						} catch (Exception e2) {
							//System.out.println("es " + e2);
						}
					} else if (atsStatus.equals(Schedule.C_WITHDRAW_FAILED)) {
						try {
							e = new Event(EVENT_WITHDRAW_FAILED);
							e.setField("atsid", message.getString(atsid));
							e.setField("atsstatus",
									message.getString(PARAM_ATS_STATUS));
							// e.setField("description",
							// message.getString(Text.FIELD));


							Schedule sc = (Schedule) engine
									.getDataStore()
									.get(TradingStore.DATA_SCHEDULELIST)
									.getDataByKey(
											new Object[] { message
													.getString(atsid) });


							String userid = "";
							if (sc != null) {
								userid = sc.getcWithdrawby();
								if (userid.isEmpty()) {
									userid = sc.getcEntrybuy();
								}
							}
							pushEvent(e);
							if (TradingSetting.getPopup()) {
								String myuserid = engine.getConnection()
										.getUserId();

								if (myuserid.equals(userid)) {
									Utils.showMessage(
											"Ats id "

													+ message.getString(atsid)
													+ " withdraw failed \n",
											null);
								}
							}
						} catch (Exception e2) {
							// TODO: handle exception
						}
					} else if (atsStatus.equals(Schedule.C_ENTRY_FAILED)) {
						try {
							e = new Event(EVENT_ENTRY_FAILED);
							e.setField("atsid", message.getString(atsid));
							e.setField("atsstatus",
									message.getString(PARAM_ATS_STATUS));
							e.setField("description",
									message.getString(PARAM_atsDescription));


							Schedule sc = (Schedule) engine
									.getDataStore()
									.get(TradingStore.DATA_SCHEDULELIST)
									.getDataByKey(
											new Object[] { message
													.getString(atsid) });



							String userid = "";
							String clientid = "";
							if (sc != null) {
								userid = sc.getcEntrybuy();
								clientid = sc.getcClientid();
							}
							pushEvent(e);
							if (TradingSetting.getPopup()) {
								String myuserid = engine.getConnection()
										.getUserId();

								if (myuserid.equals(userid)) {
									Utils.showMessage(
											"Ats id "

													+ message.getString(atsid)
													+ " entry reject \n", null);

								}
							}
						} catch (Exception e2) {
							// TODO: handle exception
						}
					} else {
						log.warn("RECEIVE EXECUTEREPORT with other orderstatus: "
								+ strStatus);
						try {
							e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField("" + PARAM_curtl,
									new Double(message.getDouble(PARAM_curtl)));
							e.setField(
									"" + PARAM_pavport,
									new Double(message.getDouble(PARAM_pavport)));
							e.setField(
									"" + PARAM_pcurratio,
									new Double(message
											.getDouble(PARAM_pcurratio)));
							e.setField("" + PARAM_pbuy,
									new Double(message.getDouble(PARAM_pbuy)));
							e.setField("" + PARAM_psell,
									new Double(message.getDouble(PARAM_psell)));
							e.setField("" + PARAM_pnetac,
									new Double(message.getDouble(PARAM_pnetac)));
							e.setField("" + PARAM_pbid,
									new Double(message.getDouble(PARAM_pbid)));
							e.setField("" + PARAM_poffer,
									new Double(message.getDouble(PARAM_poffer)));
							e.setField(
									"" + PARAM_pstockval,
									new Double(message
											.getDouble(PARAM_pstockval)));
							e.setField("" + PARAM_pmarketval, new Double(
									message.getDouble(PARAM_pmarketval)));
							e.setField(
									"" + PARAM_plqvalue,
									new Double(message
											.getDouble(PARAM_plqvalue)));
							e.setField("" + PARAM_ptopup,
									new Double(message.getDouble(PARAM_ptopup)));
							e.setField("" + PARAM_pforcesell, new Double(
									message.getDouble(PARAM_pforcesell)));
							e.setField("" + PARAM_pcreditlimit, new Double(
									message.getDouble(PARAM_pcreditlimit)));
							e.setField(
									"" + PARAM_pdeposit,
									new Double(message
											.getDouble(PARAM_pdeposit)));
							e.setField(
									"" + PARAM_pcurvol,
									new Double(message.getDouble(PARAM_pcurvol)));
							e.setField(
									"" + PARAM_pavgprice,
									new Double(message
											.getDouble(PARAM_pavgprice)));
							e.setField(
									"" + PARAM_pwithdraw,
									new Double(message
											.getDouble(PARAM_pwithdraw)));
							pushEvent(e);
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
						//	log.error(Utils.logException(ex));
						}
						try {
							if (message.getString(OrigClOrdID.FIELD) != null) {
								e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
								e.setField("clordid",
										message.getString(OrigClOrdID.FIELD));
								e.setField(
										"" + PARAM_curtl,
										new Double(message
												.getDouble(PARAM_curtlold)));
								e.setField("" + PARAM_pavport, new Double(
										message.getDouble(PARAM_pavportold)));
								e.setField("" + PARAM_pcurratio, new Double(
										message.getDouble(PARAM_pcurratioold)));
								e.setField(
										"" + PARAM_pbuy,
										new Double(message
												.getDouble(PARAM_pbuyold)));
								e.setField(
										"" + PARAM_psell,
										new Double(message
												.getDouble(PARAM_psellold)));
								e.setField("" + PARAM_pnetac, new Double(
										message.getDouble(PARAM_pnetacold)));
								e.setField(
										"" + PARAM_pbid,
										new Double(message
												.getDouble(PARAM_pbidold)));
								e.setField("" + PARAM_poffer, new Double(
										message.getDouble(PARAM_pofferold)));
								e.setField("" + PARAM_pstockval, new Double(
										message.getDouble(PARAM_pstockvalold)));
								e.setField("" + PARAM_pmarketval, new Double(
										message.getDouble(PARAM_pmarketvalold)));
								e.setField("" + PARAM_plqvalue, new Double(
										message.getDouble(PARAM_plqvalueold)));
								e.setField("" + PARAM_ptopup, new Double(
										message.getDouble(PARAM_ptopupold)));
								e.setField("" + PARAM_pforcesell, new Double(
										message.getDouble(PARAM_pforcesellold)));
								e.setField(
										"" + PARAM_pcreditlimit,
										new Double(
												message.getDouble(PARAM_pcreditlimitold)));
								e.setField("" + PARAM_pdeposit, new Double(
										message.getDouble(PARAM_pdepositold)));
								e.setField("" + PARAM_pcurvol, new Double(
										message.getDouble(PARAM_pcurvolold)));
								e.setField("" + PARAM_pavgprice, new Double(
										message.getDouble(PARAM_pavgpriceold)));
								e.setField("" + PARAM_pwithdraw, new Double(
										message.getDouble(PARAM_pwithdrawold)));
								pushEvent(e);
							}
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
							//log.error(Utils.logException(ex));
						}
					}
				} catch (Exception ex) {
				}
			}
		});
		map.put(EVENT_CANCEL_REJECT, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_CANCELREPLACE_REJECT "
						+ param.getParam().get(PARAM_FIX).toString());
				try {
					Message message = (Message) param.getField(PARAM_FIX);
					Event e = null;
					if ((message.getString(ClOrdID.FIELD).equals(message
							.getString(OrigClOrdID.FIELD)))
							|| message.getString(OrigClOrdID.FIELD).equals("")) {
						e = new Event(EVENT_REQUESTWITHDRAW_FAILED);
						e.setField("clordid", message.getString(ClOrdID.FIELD));
						e.setField("status", message.getString(OrdStatus.FIELD));

						Order order = (Order) engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getDataByKey(
										new Object[] {
												message.getString(ClOrdID.FIELD),
												"0" });

						String userid = "";
						if (order != null)
							userid = order.getWithdrawBy();

						pushEvent(e);
						if (TradingSetting.getPopup()) {
							String myuserid = engine.getConnection()
									.getUserId();

							if (myuserid.equals(userid)) {
								Utils.showMessage(
										"order "
												+ message
														.getString(ClOrdID.FIELD)
												+ " request withdraw rejected",
										null);
							}
						}

						try {
							e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField("" + PARAM_curtl,
									new Double(message.getDouble(PARAM_curtl)));
							e.setField(
									"" + PARAM_pavport,
									new Double(message.getDouble(PARAM_pavport)));
							e.setField(
									"" + PARAM_pcurratio,
									new Double(message
											.getDouble(PARAM_pcurratio)));
							e.setField("" + PARAM_pbuy,
									new Double(message.getDouble(PARAM_pbuy)));
							e.setField("" + PARAM_psell,
									new Double(message.getDouble(PARAM_psell)));
							e.setField("" + PARAM_pnetac,
									new Double(message.getDouble(PARAM_pnetac)));
							e.setField("" + PARAM_pbid,
									new Double(message.getDouble(PARAM_pbid)));
							e.setField("" + PARAM_poffer,
									new Double(message.getDouble(PARAM_poffer)));
							e.setField(
									"" + PARAM_pstockval,
									new Double(message
											.getDouble(PARAM_pstockval)));
							e.setField("" + PARAM_pmarketval, new Double(
									message.getDouble(PARAM_pmarketval)));
							e.setField(
									"" + PARAM_plqvalue,
									new Double(message
											.getDouble(PARAM_plqvalue)));
							e.setField("" + PARAM_ptopup,
									new Double(message.getDouble(PARAM_ptopup)));
							e.setField("" + PARAM_pforcesell, new Double(
									message.getDouble(PARAM_pforcesell)));
							e.setField("" + PARAM_pcreditlimit, new Double(
									message.getDouble(PARAM_pcreditlimit)));
							e.setField(
									"" + PARAM_pdeposit,
									new Double(message
											.getDouble(PARAM_pdeposit)));
							e.setField(
									"" + PARAM_pcurvol,
									new Double(message.getDouble(PARAM_pcurvol)));
							e.setField(
									"" + PARAM_pavgprice,
									new Double(message
											.getDouble(PARAM_pavgprice)));
							e.setField(
									"" + PARAM_pwithdraw,
									new Double(message
											.getDouble(PARAM_pwithdraw)));
							pushEvent(e);
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
							//log.error(Utils.logException(ex));
						}
					} else {
						e = new Event(EVENT_REQUESTAMEND_FAILED);
						e.setField("clordid", message.getString(ClOrdID.FIELD));
						e.setField("oldclordid",
								message.getString(OrigClOrdID.FIELD));
						e.setField("status", message.getString(OrdStatus.FIELD));
						try {
							e.setField("description", message.getString(7001));
						} catch (Exception ex) {
							try {
								e.setField("description",
										message.getString(Text.FIELD));
							} catch (Exception ep) {
								e.setField("description", "");
							}
						}

						Order order = (Order) engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getDataByKey(
										new Object[] {
												message.getString(OrigClOrdID.FIELD),//message.getString(ClOrdID.FIELD), yosep 18032016												
												"0" });

						String userid = "";
						if (order != null)
							userid = order.getAmendBy();

						pushEvent(e);
						if (TradingSetting.getPopup()) {

							String myuserid = engine.getConnection()
									.getUserId();

							
							System.out.println(myuserid+" -- "+userid);
							if (myuserid.equals(userid)) {
								Utils.showMessage(
										"order "
												+ message
														.getString(ClOrdID.FIELD)
												+ " request amend rejected\n"
												+ e.getField("description"),
										null);
							}
						}
						try {
							e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
							e.setField("clordid",
									message.getString(ClOrdID.FIELD));
							e.setField("" + PARAM_curtl,
									new Double(message.getDouble(PARAM_curtl)));
							e.setField(
									"" + PARAM_pavport,
									new Double(message.getDouble(PARAM_pavport)));
							e.setField(
									"" + PARAM_pcurratio,
									new Double(message
											.getDouble(PARAM_pcurratio)));
							e.setField("" + PARAM_pbuy,
									new Double(message.getDouble(PARAM_pbuy)));
							e.setField("" + PARAM_psell,
									new Double(message.getDouble(PARAM_psell)));
							e.setField("" + PARAM_pnetac,
									new Double(message.getDouble(PARAM_pnetac)));
							e.setField("" + PARAM_pbid,
									new Double(message.getDouble(PARAM_pbid)));
							e.setField("" + PARAM_poffer,
									new Double(message.getDouble(PARAM_poffer)));
							e.setField(
									"" + PARAM_pstockval,
									new Double(message
											.getDouble(PARAM_pstockval)));
							e.setField("" + PARAM_pmarketval, new Double(
									message.getDouble(PARAM_pmarketval)));
							e.setField(
									"" + PARAM_plqvalue,
									new Double(message
											.getDouble(PARAM_plqvalue)));
							e.setField("" + PARAM_ptopup,
									new Double(message.getDouble(PARAM_ptopup)));
							e.setField("" + PARAM_pforcesell, new Double(
									message.getDouble(PARAM_pforcesell)));
							e.setField("" + PARAM_pcreditlimit, new Double(
									message.getDouble(PARAM_pcreditlimit)));
							e.setField(
									"" + PARAM_pdeposit,
									new Double(message
											.getDouble(PARAM_pdeposit)));
							e.setField(
									"" + PARAM_pcurvol,
									new Double(message.getDouble(PARAM_pcurvol)));
							e.setField(
									"" + PARAM_pavgprice,
									new Double(message
											.getDouble(PARAM_pavgprice)));
							try {
								e.setField("" + PARAM_pwithdraw, new Double(
										message.getDouble(PARAM_pwithdraw)));
							} catch (Exception ex) {
								e.setField("" + PARAM_pwithdraw, new Double(
										message.getDouble(PARAM_pwithdrawnew)));
							}
							pushEvent(e);
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
							//log.error(Utils.logException(ex));
						}
						try {
							e = new Event(EVENT_RECEIVE_RISKMANAGEMENT);
							e.setField("clordid",
									message.getString(OrigClOrdID.FIELD));
							e.setField(
									"" + PARAM_curtl,
									new Double(message
											.getDouble(PARAM_curtlold)));
							e.setField(
									"" + PARAM_pavport,
									new Double(message
											.getDouble(PARAM_pavportold)));
							e.setField(
									"" + PARAM_pcurratio,
									new Double(message
											.getDouble(PARAM_pcurratioold)));
							e.setField(
									"" + PARAM_pbuy,
									new Double(message.getDouble(PARAM_pbuyold)));
							e.setField(
									"" + PARAM_psell,
									new Double(message
											.getDouble(PARAM_psellold)));
							e.setField(
									"" + PARAM_pnetac,
									new Double(message
											.getDouble(PARAM_pnetacold)));
							e.setField(
									"" + PARAM_pbid,
									new Double(message.getDouble(PARAM_pbidold)));
							e.setField(
									"" + PARAM_poffer,
									new Double(message
											.getDouble(PARAM_pofferold)));
							e.setField(
									"" + PARAM_pstockval,
									new Double(message
											.getDouble(PARAM_pstockvalold)));
							e.setField("" + PARAM_pmarketval, new Double(
									message.getDouble(PARAM_pmarketvalold)));
							e.setField(
									"" + PARAM_plqvalue,
									new Double(message
											.getDouble(PARAM_plqvalueold)));
							e.setField(
									"" + PARAM_ptopup,
									new Double(message
											.getDouble(PARAM_ptopupold)));
							e.setField("" + PARAM_pforcesell, new Double(
									message.getDouble(PARAM_pforcesellold)));
							e.setField("" + PARAM_pcreditlimit, new Double(
									message.getDouble(PARAM_pcreditlimitold)));
							e.setField(
									"" + PARAM_pdeposit,
									new Double(message
											.getDouble(PARAM_pdepositold)));
							e.setField(
									"" + PARAM_pcurvol,
									new Double(message
											.getDouble(PARAM_pcurvolold)));
							e.setField(
									"" + PARAM_pavgprice,
									new Double(message
											.getDouble(PARAM_pavgpriceold)));
							e.setField(
									"" + PARAM_pwithdraw,
									new Double(message
											.getDouble(PARAM_pwithdrawold)));
							pushEvent(e);
						} catch (Exception ex) {
							log.warn("execution report without RM tag(s)");
						//	log.error(Utils.logException(ex));
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					log.warn(Utils.logException(ex));
				}
			}
		});
		map.put(EVENT_RECEIVE_RISKMANAGEMENT, new EventHandler() {
			public void execute(Event param) {
				try {
					String orderid = param.getField("clordid").toString();
					/*log.info("execute EVENT_RECEIVERISKMANAGEMENT for order"
							+ orderid);*/
					int idx = engine.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(new String[] { orderid, "0" });
					if (idx > -1) {
						Order order = (Order) engine.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getDataByIndex(idx);
						if (order != null) {
							Account account = (Account) engine
									.getDataStore()
									.get(TradingStore.DATA_ACCOUNT)
									.getDataByField(
											new Object[] { order.getTradingId() },
											new int[] { Account.C_TRADINGID });
							if (account != null) {
								account.setCurrTL((Double) param.getField(""
										+ PARAM_curtl));
								account.setCurrentRatio((Double) param
										.getField("" + PARAM_pcurratio));
								account.setBuy((Double) param.getField(""
										+ PARAM_pbuy));
								account.setSell((Double) param.getField(""
										+ PARAM_psell));
								account.setNetAC((Double) param.getField(""
										+ PARAM_pnetac));
								account.setBid((Double) param.getField(""
										+ PARAM_pbid));
								account.setOffer((Double) param.getField(""
										+ PARAM_poffer));
								account.setStockVal((Double) param.getField(""
										+ PARAM_pstockval));
								account.setMarketVal((Double) param.getField(""
										+ PARAM_pmarketval));
								account.setLQValue((Double) param.getField(""
										+ PARAM_plqvalue));
								account.setTopup((Double) param.getField(""
										+ PARAM_ptopup));
								account.setForceSell((Double) param.getField(""
										+ PARAM_pforcesell));
								account.setCreditLimit((Double) param
										.getField("" + PARAM_pcreditlimit));
								account.setWithdraw((Double) param.getField(""
										+ PARAM_pwithdraw));
								engine.getDataStore()
										.get(TradingStore.DATA_ACCOUNT)
										.refresh();
							}
							Portfolio pf = (Portfolio) engine
									.getDataStore()
									.get(TradingStore.DATA_PORTFOLIO)
									.getDataByKey(
											new Object[] {
													order.getTradingId(),
													order.getStock() });

							if (pf != null) {
								log.info("EVENT_RECEIVERISKMANAGEMENT_stock : "+pf.getStock()+" tradingid : "+pf.getTradingId()+
										" rm avalaibleVolume : "+(Double) param.getField("" + PARAM_pavport)+
										" avalaibleVolume : "+pf.getAvailableVolume() );
								
								pf.setAvailableVolume((Double) param
										.getField("" + PARAM_pavport));

								pf.setCurrVolume((Double) param.getField(""
										+ PARAM_pcurvol));
								pf.setAvgPrice((Double) param.getField(""
										+ PARAM_pavgprice));

								engine.getDataStore()
										.get(TradingStore.DATA_PORTFOLIO)
										.refresh();
								System.out.println("av lot "+pf.getAvailbaleLot());
							} else if (pf == null
									&& (order.getStatus().equals(
											Order.C_FULL_MATCH) || order
											.getStatus().equals(
													Order.C_PARTIAL_MATCH))) {

								engine.getEventDispatcher()
										.pushEvent(
												new Event(
														TradingEventDispatcher.EVENT_REFRESH_PORTFOLIO)
														.setField(
																TradingEventDispatcher.PARAM_ACCOUNTID,
																order.getTradingId())
														.setField(
																TradingEventDispatcher.PARAM_STOCK,
																order.getStock()));
							}

							if (param.getField("" + PARAM_ptlbefore) != null) {
								order.setTLBefore((Double) param.getField(""
										+ PARAM_ptlbefore));
								order.setPortBefore((Double) param.getField(""
										+ PARAM_pavportbefore));
								order.setLQValueBefore((Double) param
										.getField("" + PARAM_plqvaluebefore));
								order.setRatioBefore((Double) param.getField(""
										+ PARAM_pratiobefore));
								order.setIsOverLimit((String) param.getField(""
										+ PARAM_pisoverlimit));
								order.setIsShortSell((String) param.getField(""
										+ PARAM_pisshortsell));
								engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.refresh(idx);
							}
						}
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.refresh(idx);
					}
				} catch (Exception ex) {
					//log.error(Utils.logException(ex));
				}
			}
		});

		map.put(EVENT_CREATEORDER, new EventHandler() {
			public void execute(Event param) {
				final Order buy = new Order();
				log.info("execute EVENT_CREATEORDER "
						+ param.getParam().get(PARAM_ORDER).toString());
				final Order order = (Order) param.getParam().get(PARAM_ORDER);
				order.setStatus(order.getStatus().equals(Order.C_ENTRY) ? Order.C_SENDING_ENTRY
						: Order.C_SENDING_TEMPORARY);
				order.setEntryTime(OrderIDGenerator.getTime());
				// if(order.getEntryBy()==null){
				// System.out.println(engine.getConnection().getUserId()+" user_id");
				order.setEntryBy(engine.getConnection().getUserId()
						.toUpperCase());// }
				order.setEntryTerminal(OrderIDGenerator.getTerminal());
				// if(order.getUpdateBy()== null){
				order.setUpdateBy(engine.getConnection().getUserId()
						.toUpperCase());// }
				order.setUpdateDate(OrderIDGenerator.getTime());
				order.setTerminalId(OrderIDGenerator.getTerminal());


				// validasi apakah harus convert ke basket order atau tidak

				if (order.getIsBasketOrder().equals("0")) {
					try {
						

						String stock = order.getStock();
						Stock s = (Stock) engine.getDataStore()
								.get(TradingStore.DATA_STOCK)
								.getDataByKey(new String[] { stock });
						boolean stockpreopening = s.isPreopening();
						String dt = dateFormat2.format(OrderIDGenerator
								.getTime());
						boolean friday = fridayFormat
								.format(OrderIDGenerator.getTime())
								.toUpperCase().equals("FRI");


						String brdid = order.getBoard();








						String dtop = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_FIRST_PREOP);// 08\:45\:00

						String dtendpre = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_END_PREOP);// 08\:55\:00

						String dtses1 = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_FIRST_SES1);// 09\:00\:00
						String dtses2 = dt
								+ "-"
								+ (friday ? TradingSetting
										.getDateSession(TradingSetting.C_FIRST_FRIDAY_SES2)// 14\:00\:00
										: TradingSetting
												.getDateSession(TradingSetting.C_FIRST_SES2));// 13\:30\:00
						String dtendses1 = dt
								+ "-"
								+ (friday ? TradingSetting
										.getDateSession(TradingSetting.C_END_FRIDAY_SES1)// "11:30:00"
										: TradingSetting
												.getDateSession(TradingSetting.C_END_SES1));// "12:00:00"


						String dtendses2 = dt
								+ "-"

								+ TradingSetting
										.getDateSession(TradingSetting.C_END_SES2); // "11:30:00"
																					// :

																					// "12:00:00");
						String dtpost_first = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_FIRST_POSTTRADING);

						String dtpost_end = dt
								+ "-"

								+ TradingSetting
										.getDateSession(TradingSetting.C_END_POSTTRADING);

						String preclosing_end = dt
								+ "-"

								+ TradingSetting
										.getDateSession(TradingSetting.C_END_PRECLOSING);





						Date datepreopening = dateFormatAll.parse(dtop);
						Date dateses1 = dateFormatAll.parse(dtses1);
						Date dateses2 = dateFormatAll.parse(dtses2);
						Date enddateses1 = dateFormatAll.parse(dtendses1);
						Date endpre = dateFormatAll.parse(dtendpre);
						Date postfirst = dateFormatAll.parse(dtpost_first);
						Date postend = dateFormatAll.parse(dtpost_end);
						Date preclosingend = dateFormatAll
								.parse(preclosing_end);

						Date datecurrent = OrderIDGenerator.getTime();


						Date enddateses2 = dateFormatAll.parse(dtendses2);
						// log.info("boardid "+brdid);
						//System.out.println(dtpost_first + "-" + preclosing_end+ "-" + brdid.equals("RG") + " -- --");
						if (datecurrent.getTime() < datepreopening.getTime()) {
							order.setIsBasketOrder("1");
							order.setBasketTime(stockpreopening ? datepreopening
									: dateses1);
							Utils.showMessage("order will be send at "
									+ (stockpreopening ? datepreopening
											: dateses1), null);
						} else if ((datecurrent.getTime() < dateses1.getTime() && datecurrent
								.getTime() > datepreopening.getTime())
								&& brdid.equals("NG")) {
							order.setIsBasketOrder("1");
							order.setBasketTime(dateses1);
							Utils.showMessage("order will be send at "
									+ (dateses1), null);




						} else if (datecurrent.getTime() < endpre.getTime()) {
							if (!stockpreopening) {
								order.setIsBasketOrder("1");
								order.setBasketTime(dateses1);
								Utils.showMessage("order will be send at "
										+ (dateses1), null);
							}
						} else if (datecurrent.getTime() > endpre.getTime()
								&& datecurrent.getTime() < dateses1.getTime()) {
							order.setIsBasketOrder("1");
							order.setBasketTime(dateses1);
							Utils.showMessage("order will be send at "
									+ (dateses1), null);
						} else if (datecurrent.getTime() > enddateses1
								.getTime()
								&& datecurrent.getTime() < dateses2.getTime()) {
							order.setIsBasketOrder("1");
							order.setBasketTime(dateses2);
							Utils.showMessage("order will be send at "
									+ (dateses2), null);
						} else if ((datecurrent.getTime() > preclosingend

								.getTime() && datecurrent.getTime() < postfirst
								.getTime())
								&& brdid.equals("RG")) {
							order.setIsBasketOrder("1");
							order.setBasketTime(postfirst);// enddateses2
							Utils.showMessage("order will be send at "
									+ (postfirst), null);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}

				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Vector v = new Vector(1);
						v.addElement(OrderDef.createTableRow(order));
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.addRow(v, false, false);

					}
				});

				if (!order.getOrderIDContra().equals("")) {
					// crossing order
					buy.fromVector(order.getData());
					buy.setBOS(Order.C_BUY);
					buy.setId(order.getOrderIDContra());
					buy.setAccId(order.getAccIDContra());
					buy.setInvType(order.getInvTypeContra());
					buy.setTradingId(order.getTradingIDContra());
					buy.setMarketRef(buy.getId());
					buy.setOrderIDContra(order.getId());
					buy.setAccIDContra(order.getAccId());
					buy.setTradingIDContra(order.getTradingId());
					buy.setInvTypeContra(order.getInvType());
					Account account = (Account) engine.getDataStore()
							.get(TradingStore.DATA_ACCOUNT)
							.getDataByKey(new String[] { buy.getAccId() });
					if (account != null) {
						buy.setComplianceId(account.getComlianceId());
					}
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							Vector v = new Vector(1);
							v.addElement(OrderDef.createTableRow(buy));
							engine.getDataStore().get(TradingStore.DATA_ORDER)
									.addRow(v, false, false);

						}
					});
				}

				// TODO ganti ordertype  
				Order gtc = (Order) engine.getDataStore()
							.get(TradingStore.DATA_GTC)
							.getDataByKey(new String[] { order.getId() });
//				if(gtc != null){//gtc TN
				if(order.getOrderType().equals("G")){
					if(order.getBoard().equals("TN"))
					{
						order.setOrderType("S");
					}else {
						order.setOrderType("0");	
					}
				}
				Message newOrder = new Message();
				newOrder.getHeader().setField(new BeginString(BEGIN_STRING));
				newOrder.getHeader().setField(new MsgType("D"));
				newOrder.setField(new ClOrdID(order.getId()));
				newOrder.setField(new ClientID(order.getEntryBy()));
				newOrder.setField(new quickfix.field.Account(order.getInvType()));
				newOrder.setField(new HandlInst(order.getHandInst()));
				newOrder.setField(new Symbol(order.getStock()));
				newOrder.setField(new SymbolSfx(order.getBoard()));
				newOrder.setField(new Side(order.getBOS().charAt(0)));
				newOrder.setField(getTime(TransactTime.FIELD,
						order.getEntryTime()));
				newOrder.setField(new OrderQty(order.getVolume().doubleValue()));
				newOrder.setField(new OrdType(order.getOrdType().charAt(0)));
				newOrder.setField(new Price(order.getPrice().doubleValue()));
				newOrder.setField(new TimeInForce(order.getOrderType()
						.charAt(0)));
				newOrder.setString(376, order.getComplianceId());
				newOrder.setField(getTime(666671, order.getEntryTime()));
				newOrder.setDouble(666672, order.getCounter().doubleValue());
				newOrder.setDouble(666673, order.getSplitTo().doubleValue());
				newOrder.setString(666675, order.getExchange());
				newOrder.setString(666676, order.getAccId());
				newOrder.setString(666677, order.getEntryBy());
				newOrder.setString(666678, order.getMarketref());
				newOrder.setString(666679, order.getEntryTerminal());
				newOrder.setString(8007, order.getIsBasketOrder());
				newOrder.setString(7043, order.getBroker());
				newOrder.setString(7044, order.getCurrency());
				if (order.getBasketTime() != null)
					newOrder.setField(getTime(8008, order.getBasketTime()));
				if (order.getHandInst() == '3') {
					newOrder.setField(new IOIid(order.getNegDealRef()));
					newOrder.setField(new ContraBroker(order.getContraBroker()));
					newOrder.setField(new ContraTrader(order.getContraUser()));
					newOrder.setString(7045, order.getOrderIDContra());
					newOrder.setString(7046, order.getAccIDContra());
					newOrder.setString(7047, order.getInvTypeContra());
					newOrder.setString(7048, order.getContraBroker());
					newOrder.setString(7049, order.getContraUser());
					newOrder.setString(7050, order.getNegDealRef());
					newOrder.setString(7051, order.getContraBroker());
					newOrder.setString(7052, buy.getComplianceId());
				}
				newOrder.setString(440, " ");
				engine.getConnection().addFilterByOrder(order.getId());
				param.setField(PARAM_FIX, newOrder);
				engine.getConnection().sendEvent(param);





			}
		});
		map.put(EVENT_CREATEORDER_ACK, new EventHandler() {
			public void execute(Event param) {
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				log.info("execute EVENT_CREATEORDER_RECEIVEBYSERVER "
						+ order.getId());
				order.setStatus(Order.C_SENDING_ENTRYSERVER);
				// for testing only
				// if
				// (param.getParam().get(PARAM_OLDSTATUS).equals(Order.C_ENTRY)){
				// order.setStatus(Order.C_OPEN);
				// } else {
				// order.setStatus(Order.C_TEMPORARY);
				// }
				// end testing
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
				if (!order.getOrderIDContra().equals("")) {
					idx = engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(
									new String[] { order.getOrderIDContra(),
											order.getTradeNo() });
					if (idx > -1) {
						Order ord = (Order) engine.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getDataByIndex(idx);
						ord.setStatus(Order.C_SENDING_ENTRYSERVER);
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.refresh(idx);
					}
				}
			}
		});
		map.put(EVENT_CREATEORDER_NACK, new EventHandler() {
			public void execute(Event param) {
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				log.info("execute EVENT_CREATEORDER_SENDTOSERVERFAILED "
						+ order.getId());
				order.setStatus(Order.C_FAILED);
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
				if (!order.getOrderIDContra().equals("")) {
					idx = engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(
									new String[] { order.getOrderIDContra(),
											order.getTradeNo() });
					idx = engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(
									new String[] { order.getOrderIDContra(),
											order.getTradeNo() });
					if (idx > -1) {
						Order ord = (Order) engine.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getDataByIndex(idx);
						ord.setStatus(Order.C_FAILED);
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.refresh(idx);
					}
				}
			}
		});
		map.put(EVENT_UPDATE_STATUS, new EventHandler() {
			public void execute(Event param) {
				try {
					/*log.info("execute EVENT_UPDATE_STATUS "
							+ param.getParam().get("clordid").toString());*/
					Order order = (Order) engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getDataByKey(
									new Object[] { param.getField("clordid"),
											"0" });
					if (order != null) {
						order.setStatus(order.getVolume().doubleValue() == order
								.getBalVol().doubleValue() ? Order.C_OPEN
								: (order.getBalVol().doubleValue() <= 0) ? Order.C_FULL_MATCH
										: Order.C_PARTIAL_MATCH);
						int idx = engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getIndexByKey(
										new String[] { order.getId(),
												order.getTradeNo() });
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.refresh(idx);
						if (!order.getOrderIDContra().equals("")) {
							idx = engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getIndexByKey(
											new String[] {
													order.getOrderIDContra(),
													order.getTradeNo() });
							if (idx > -1) {
								Order ord = (Order) engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.getDataByIndex(idx);
								ord.setStatus(order.getVolume().doubleValue() == order
										.getBalVol().doubleValue() ? Order.C_OPEN
										: (order.getBalVol().doubleValue() <= 0) ? Order.C_FULL_MATCH
												: Order.C_PARTIAL_MATCH);
								engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.refresh(idx);
							}
						}
					} else {
						Event e = new Event(EVENT_REFRESH_ORDER);
						e.setField("accountid", "%");
						e.setField("orderid", param.getField("clordid"));
						pushEvent(e);
					}
				} catch (Exception ex) {
					//log.error(Utils.logException(ex));
				}
			}
		});
		map.put(EVENT_CREATEORDER_OK, new EventHandler() {
			public void execute(Event param) {
				try {
					log.info("execute EVENT_CREATEORDER_OK "
							+ param.getParam().get("clordid").toString());
					Order order = (Order) engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getDataByKey(
									new Object[] { param.getField("clordid"),
											"0" });
					if (order != null) {
						//#Valdhy20141215
						order.setMarketOrderId((String) param
								.getField("orderid"));
						order.setOpenTime((Date) param.getField("opentime"));
						order.setOpenSendTime((Date) param
								.getField("opensendtime"));
						order.setStatus(order.getVolume().doubleValue() == order
								.getBalVol().doubleValue() ? Order.C_OPEN
								: (order.getBalVol().doubleValue() <= 0) ? Order.C_FULL_MATCH
										: Order.C_PARTIAL_MATCH);
						order.buildAmount();
						int idx = engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getIndexByKey(
										new String[] { order.getId(),
												order.getTradeNo() });
						final int idx2 = idx;
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.refresh(idx2);

							}
						});
						if (!order.getOrderIDContra().equals("")) {
							idx = engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getIndexByKey(
											new String[] {
													order.getOrderIDContra(),
													order.getTradeNo() });
							if (idx > -1) {
								Order ord = (Order) engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.getDataByIndex(idx);
								//ord.setMarketOrderId((String) param
								//		.getField("orderid"));
								Integer MarketOrderIdContra = 1;
								ord.setMarketOrderId(Integer.toString(Integer.parseInt((String) param.getField("orderid"))+MarketOrderIdContra));
								
								ord.setOpenTime((Date) param
										.getField("opentime"));
								ord.setOpenSendTime((Date) param
										.getField("opensendtime"));
								ord.setStatus(order.getVolume().doubleValue() == order
										.getBalVol().doubleValue() ? Order.C_OPEN
										: (order.getBalVol().doubleValue() <= 0) ? Order.C_FULL_MATCH
												: Order.C_PARTIAL_MATCH);
								engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.refresh(idx);
							}
						}
					} else {
						Event e = new Event(EVENT_REFRESH_ORDER);
						e.setField("accountid", "%");
						e.setField("orderid", param.getField("clordid"));
						pushEvent(e);
					}
				} catch (Exception ex) {
					//log.error(Utils.logException(ex));
				}
			}
		});
//*gtc awal
		map.put(EVENT_CREATEGTC_OK, new EventHandler() {
			public void execute(Event param) {
				try {
					log.info("execute EVENT_CREATEGTC_OK "
							+ param.getParam().get("clordid").toString());
					Order GTC = (Order) engine
							.getDataStore()
							.get(TradingStore.DATA_GTC)
							.getDataByKey(
									new Object[] { param.getField("clordid"),
											"0" });
					if (GTC != null) {
						GTC.setMarketOrderId((String) param.getField("orderid"));
						GTC.setOpenTime((Date) param.getField("opentime"));
						GTC.setOpenSendTime((Date) param
								.getField("opensendtime"));
						//GTC.setField(getTime(666671, GTC.getEntryTime()));
						GTC.setStatus(GTC.getVolume().doubleValue() == GTC
								.getBalVol().doubleValue() ? Order.C_OPEN
								: (GTC.getBalVol().doubleValue() <= 0) ? Order.C_FULL_MATCH
										: Order.C_PARTIAL_MATCH);
						int idx = engine.getDataStore()
								.get(TradingStore.DATA_GTC)
								.getIndexByKey(new String[] { GTC.getId(), });
						final int idx2 = idx;
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								engine.getDataStore()
										.get(TradingStore.DATA_GTC)
										.refresh(idx2);

							}
						});
						if (!GTC.getOrderIDContra().equals("")) {
							idx = engine
									.getDataStore()
									.get(TradingStore.DATA_GTC)
									.getIndexByKey(
											new String[] { GTC
													.getOrderIDContra(), });
							if (idx > -1) {
								Order ord = (Order) engine.getDataStore()
										.get(TradingStore.DATA_GTC)
										.getDataByIndex(idx);
								ord.setMarketOrderId((String) param
										.getField("orderid"));
								ord.setOpenTime((Date) param
										.getField("opentime"));
								ord.setOpenSendTime((Date) param
										.getField("opensendtime"));
								ord.setStatus(GTC.getVolume().doubleValue() == GTC
										.getBalVol().doubleValue() ? Order.C_OPEN
										: (GTC.getBalVol().doubleValue() <= 0) ? Order.C_FULL_MATCH
												: Order.C_PARTIAL_MATCH);
								engine.getDataStore()
										.get(TradingStore.DATA_GTC)
										.refresh(idx);
							}
						}
					} else {
						Event e = new Event(EVENT_REFRESH_GTC);
						e.setField("accountid", "%");
						e.setField("orderid", param.getField("clordid"));
						pushEvent(e);
					}
				} catch (Exception ex) {
					log.error(Utils.logException(ex));
				}
				
				Order GTC = (Order) engine
				.getDataStore()
				.get(TradingStore.DATA_GTC)
				.getDataByKey(
						new Object[] { param.getField("clordid") });
		
		//log.info("***"+order+" "+ param.getField("clordid"));
		if (GTC != null) {
		
			GTC.setMarketOrderId((String) param.getField("orderid"));
			GTC.setOpenTime((Date) param.getField("opentime"));
			GTC.setOpenSendTime((Date) param.getField("opensendtime"));
			GTC.setEntryTime((Date) param.getField("entrytime"));
			GTC.setStatus(GTC.getVolume().doubleValue() == GTC
					.getBalVol().doubleValue() ? Order.C_OPEN
					: (GTC.getBalVol().doubleValue() <= 0) ? Order.C_FULL_MATCH
							: Order.C_PARTIAL_MATCH);
			
			
			int idx = engine
			.getDataStore()
			.get(TradingStore.DATA_GTC)
			.getIndexByKey(
					new String[] { GTC.getId()
							 });
	final int idx2 = idx;
	SwingUtilities.invokeLater(new Runnable() {
		public void run() {
			engine.getDataStore()
					.get(TradingStore.DATA_GTC)
					.refresh(idx2);

		
		}}
		);
			}
			}});
		//*gtc akhir
		map.put(EVENT_CREATEORDER_FAILED, new EventHandler() {
			public void execute(Event param) {
				try {
					log.info("execute EVENT_CREATEORDER_FAILED "
							+ param.getParam().get("clordid").toString());
					Order order = (Order) engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getDataByKey(
									new Object[] { param.getField("clordid"),
											"0" });
					if (order != null) {
						order.setStatus((String) param.getField("status"));
						order.setNote((String) param.getField("description"));
						order.setRejectTime((Date) param.getField("rejecttime"));
						int idx = engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getIndexByKey(
										new String[] { order.getId(),
												order.getTradeNo() });
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.refresh(idx);
						if (!order.getOrderIDContra().equals("")) {
							idx = engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getIndexByKey(
											new String[] {
													order.getOrderIDContra(),
													order.getTradeNo() });
							if (idx > -1) {
								Order ord = (Order) engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.getDataByIndex(idx);
								ord.setStatus((String) param.getField("status"));
								ord.setNote((String) param
										.getField("description"));
								ord.setRejectTime((Date) param
										.getField("rejecttime"));
								engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.refresh(idx);
							}
						}
						engine.getConnection().deleteFilterByOrder(
								param.getParam().get("clordid").toString());
					} else {
						Event e = new Event(EVENT_REFRESH_ORDER);
						e.setField("accountid", "%");
						e.setField("orderid", param.getField("clordid"));
						pushEvent(e);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		map.put(EVENT_ENTRYSUCCESS, new EventHandler() {			
			@Override
			public void execute(Event param) {
				try {

					/*log.info("execute EVENT_ENTRYSUCCESS " +
							param.getParam().get("atsid").toString());*/
					Schedule sc = (Schedule) engine
										.getDataStore()
										.get(TradingStore.DATA_SCHEDULELIST)
										.getDataByKey(
												new String[] {
														param.getField("atsid").toString()



														});
					if(sc != null){
						if(sc.getcExectime()==null){
						sc.setcAtsstatus((String) param.getField("atsstatus"));
						sc.setcEntrytime((Date) param.getField("entrytime"));



						Date date = dateFormat.parse((String) param.getField("validuntil"));

						sc.setcValiduntil((Date) date);

						
							int idx = engine
									 .getDataStore()
									 .get(TradingStore.DATA_SCHEDULELIST)
									 .getIndexByKey(
											 new String[] {sc.getcAtsid()});

							
							engine.getDataStore().get(TradingStore.DATA_SCHEDULELIST).refresh(idx);


							
						}else{
							//System.out.println("udah execute");
						}
					}else {
						//System.out.println("ga ketemu atsidnya"+param.getField("atsid"));
					}					
				} catch (Exception e) {
					//System.out.println("EVENT_ENTRYSUCCESS "+ e);
				}
			}
		});
		map.put(EVENT_EXECUTED, new EventHandler() {
			public void execute(Event param) {
				try {
					/*log.info("execute EVENT_EXECUTED "
							+ param.getParam().get("atsid").toString());*/


					Schedule sc = (Schedule) engine
							.getDataStore()

							.get(TradingStore.DATA_SCHEDULELIST)
							.getDataByKey(

									new String[] { (String) param
											.getField("atsid") });

					if (sc != null) {
						sc.setcAtsstatus((String) param.getField("atsstatus"));
						sc.setcOrderid((String) param.getField("orderid"));
						sc.setcOrderstatus((String) param
								.getField("orderstatus"));
						sc.setcExecby((String) param.getField("execby"));
						sc.setcExectime((Date) param.getField("exectime"));

						int idx = engine.getDataStore()
								.get(TradingStore.DATA_SCHEDULELIST)

								.getIndexByKey(new String[] { sc.getcAtsid() });
						engine.getDataStore()
								.get(TradingStore.DATA_SCHEDULELIST)
								.refresh(idx);
						//engine.getConnection().refreshOrder("%", "%");
					}
				} catch (Exception e) {
					//System.out.println(e + " error event_executed");
				}
			}
		});

		map.put(EVENT_WITHDRAW_SUCCESS, new EventHandler() {


			@Override
			public void execute(Event param) {
				try {
					/*log.info("execute EVENT_WITHDRAW_SUCCESS "
							+ param.getParam().get("atsid").toString());*/
					Schedule sc = (Schedule) engine
							.getDataStore()
							.get(TradingStore.DATA_SCHEDULELIST)
							.getDataByKey(

									new String[] { param.getField("atsid")
											.toString() });

					if (sc != null) {
						sc.setcAtsstatus((String) param.getField("atsstatus"));
						sc.setcWithdrawtime((Date) param
								.getField("withdrawtime"));

						sc.setcWithdrawby((String) param.getField("withdrawby"));
						sc.setcWithdrawterminal((String) param
								.getField("withdrawbyTerminal"));


						int idx = engine.getDataStore()
								.get(TradingStore.DATA_SCHEDULELIST)

								.getIndexByKey(new String[] { sc.getcAtsid() });
						engine.getDataStore()
								.get(TradingStore.DATA_SCHEDULELIST)
								.refresh(idx);

					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		});

		map.put(EVENT_WITHDRAW_FAILED, new EventHandler() {
			@Override
			public void execute(Event param) {
				// TODO Auto-generated method stub
				try {
					/*log.info("execute EVENT_WITHDRAW_FAILED "
							+ param.getParam().get("atsid").toString());*/
					Schedule sc = (Schedule) engine
							.getDataStore()
							.get(TradingStore.DATA_SCHEDULELIST)
							.getDataByKey(
									new Object[] { param.getField("atsid"), "0" });

					if (sc != null) {
						sc.setcAtsstatus((String) param.getField("atsstatus"));
						String desc = (String) param.getField("description");
						if (desc != null && !desc.equals("")) {
							int idx = engine
									.getDataStore()
									.get(TradingStore.DATA_SCHEDULELIST)
									.getIndexByKey(
											new String[] { sc.getcAtsid() });
							engine.getDataStore()
									.get(TradingStore.DATA_SCHEDULELIST)
									.refresh(idx);
						}
					}
				} catch (Exception e) {
					
				}
			}
		});

		map.put(EVENT_ENTRY_FAILED, new EventHandler() {


			@Override
			public void execute(Event param) {
				try {
					/*log.info("execute EVENT_ENTRY_FAILED "
							+ param.getParam().get("atsid").toString());*/
					Schedule sc = (Schedule) engine
							.getDataStore()
							.get(TradingStore.DATA_SCHEDULELIST)
							.getDataByKey(
									new Object[] { param.getField("atsid"), "0" });

					if (sc != null) {
						sc.setcAtsstatus((String) param.getField("atsstatus"));
						String desc = (String) param.getField("description");
						sc.setcDescription(desc);
						if (desc != null && !desc.equals("")) {
							int idx = engine
									.getDataStore()
									.get(TradingStore.DATA_SCHEDULELIST)
									.getIndexByKey(
											new String[] { sc.getcAtsid() });
							engine.getDataStore()
									.get(TradingStore.DATA_SCHEDULELIST)
									.refresh(idx);
						}
					}
				} catch (Exception e) {
					
				}
			}
		});
		map.put(EVENT_ACK, new EventHandler() {
			public void execute(Event param) {
				try {
					/*log.info("execute EVENT_ACK "
							+ param.getParam().get("clordid").toString());*/
					Order order = (Order) engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getDataByKey(
									new Object[] { param.getField("clordid"),
											"0" });
					if (order != null) {
						//log.info("status order " + order.getStatus());
						if (order.getStatus().equals("SE")
								|| order.getStatus().equals("SES")
								|| order.getStatus().equals("SA")
								|| order.getStatus().equals("SW"))
							order.setStatus((String) param.getField("status"));


						Order gtc = (Order) engine.getDataStore()
								.get(TradingStore.DATA_GTC)
								.getDataByKey(new String[] { order.getId() ,"0"});
						if(gtc != null){
							order.setOrderType("G");
						}
						String desc = (String) param.getField("description");
						if (desc != null && !desc.equals(""))
							order.setNote(desc);
						int idx = engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getIndexByKey(
										new String[] { order.getId(),
												order.getTradeNo() });
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.refresh(idx);
						if (!order.getOrderIDContra().equals("")) {
							idx = engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getIndexByKey(
											new String[] {
													order.getOrderIDContra(),
													order.getTradeNo() });
							if (idx > -1) {
								Order ord = (Order) engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.getDataByIndex(idx);
								ord.setStatus((String) param.getField("status"));
								if (desc != null && !desc.equals(""))
									ord.setNote(desc);
								engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.refresh(idx);
							}
						}
					} else {
						Event e = new Event(EVENT_REFRESH_ORDER);
						e.setField("accountid", "%");
						e.setField("orderid", param.getField("clordid"));
						pushEvent(e);
					}
				} catch (Exception ex) {
					//log.error(Utils.logException(ex));
				}
			}
		});
//*gtc awal
		map.put(EVENT_ACK_GTC, new EventHandler() {
			public void execute(Event param) {
				try {
					log.info("execute EVENT_ACK_GTC "
							+ param.getParam().get("clordid").toString());
					Order GTC = (Order) engine
							.getDataStore()
							.get(TradingStore.DATA_GTC)
							.getDataByKey(
									new Object[] { param.getField("clordid"),
											"0" });
					if (GTC != null) {
						log.info("status order " + GTC.getStatus());
						if (GTC.getStatus().equals("SE")
								|| GTC.getStatus().equals("SES")
								|| GTC.getStatus().equals("SA")
								|| GTC.getStatus().equals("SW"))
							GTC.setStatus((String) param.getField("status"));

						String desc = (String) param.getField("description");
						if (desc != null && !desc.equals(""))
							GTC.setNote(desc);
						int idx = engine
								.getDataStore()
								.get(TradingStore.DATA_GTC)
								.getIndexByKey(
										new String[] { GTC.getId(),
												GTC.getTradeNo() });
						engine.getDataStore().get(TradingStore.DATA_GTC)
								.refresh(idx);
						if (!GTC.getOrderIDContra().equals("")) {
							idx = engine
									.getDataStore()
									.get(TradingStore.DATA_GTC)
									.getIndexByKey(
											new String[] {
													GTC.getOrderIDContra(),
													GTC.getTradeNo() });
							if (idx > -1) {
								Order ord = (Order) engine.getDataStore()
										.get(TradingStore.DATA_GTC)
										.getDataByIndex(idx);
								ord.setStatus((String) param.getField("status"));
								if (desc != null && !desc.equals(""))
									ord.setNote(desc);
								engine.getDataStore()
										.get(TradingStore.DATA_GTC)
										.refresh(idx);
							}
						}
					} else {
						Event e = new Event(EVENT_REFRESH_GTC);
						e.setField("accountid", "%");
						e.setField("orderid", param.getField("clordid"));
						pushEvent(e);
					}
				} catch (Exception ex) {
					log.error(Utils.logException(ex));
				}
			}
		});
//*gtc akhir
		map.put(EVENT_ADVANDNEGDEALLIST, new EventHandler() {
			public void execute(Event param) {
				try {
					/*log.info("execute EVENT_ADVANDNEGDEALLIST "
							+ param.getParam().get(PARAM_FIX).toString());*/
					Message msg = (Message) param.getParam().get(PARAM_FIX);
					NegDealList ng = new NegDealList();
					try {
						msg.getField(new NoContraBrokers());
						// negdeallist
						ng.setContraBrokerID(msg.getString(ContraBroker.FIELD));
						ng.setContraUserID(msg.getString(ContraTrader.FIELD));
						if (msg.isSetField(ComplianceID.FIELD)) {
							ng.setComplianceID(msg
									.getString(ComplianceID.FIELD));
						}
						ng.setIsAdvertising("0");
					} catch (Exception ex) {
						// advertisinglist
						// /ex.printStackTrace();
						ng.setIsAdvertising("1");
						ng.setOrdType(msg.getString(OrdType.FIELD));
						try {
							ng.setTradeDate(convertDate(msg
									.getString(TradeDate.FIELD)));
						} catch (Exception exd) {
						}
						try {
							ng.setOrderDate(convertDate(msg
									.getString(TransactTime.FIELD)));
						} catch (Exception exd) {
						}
					}
					try {
						ng.setBalance(new Double(msg.getDouble(LeavesQty.FIELD)));
					} catch (Exception ex2) {
					}
					ng.setMarketOrderID(msg.getString(OrderID.FIELD));

					if (msg.isSetField(ClOrdID.FIELD))
						ng.setMarketRef(msg.getString(ClOrdID.FIELD));

					try {
						ng.setOriginalOrderID(convertStr(msg
								.getString(OrigClOrdID.FIELD)));
						if (!ng.getOriginalOrderID().equals("")) {
							// set old order to withdrawn
							int x = engine
									.getDataStore()
									.get(TradingStore.DATA_NEGDEALLIST)
									.getIndexByKey(
											new String[] { ng
													.getOriginalOrderID() });
							if (x > -1) {
								NegDealList old = (NegDealList) engine
										.getDataStore()
										.get(TradingStore.DATA_NEGDEALLIST)
										.getDataByKey(
												new String[] { ng
														.getOriginalOrderID() });
								if (old != null) {
									old.setStatusID(Order.C_WITHDRAW);


									engine.getDataStore()
											.get(TradingStore.DATA_NEGDEALLIST)
											.refresh(x);
								}
							}
						}
					} catch (Exception ex) {
						ng.setOriginalOrderID("");
					}
					ng.setUserID(msg.getString(ClientID.FIELD));
					ng.setBrokerID(msg.getString(ExecBroker.FIELD));
					ng.setStatusID(msg.getString(OrdStatus.FIELD));

					if (msg.isSetField(quickfix.field.Account.FIELD))
						ng.setInvType(msg
								.getString(quickfix.field.Account.FIELD));

					ng.setSecID(msg.getString(Symbol.FIELD));
					ng.setBrdID(msg.getString(SymbolSfx.FIELD));
					ng.setSecurityID(msg.getString(SecurityID.FIELD));
					ng.setBuySell(msg.getString(Side.FIELD));
					ng.setVolume(new Double(msg.getDouble(OrderQty.FIELD)));
					ng.setPrice(new Double(msg.getDouble(Price.FIELD)));

					if (msg.isSetField(Text.FIELD))
						ng.setNote(msg.getString(Text.FIELD));

					Vector v = new Vector(1);
					v.addElement(NegDealListDef.createTableRow(ng));
					engine.getDataStore().get(TradingStore.DATA_NEGDEALLIST)
							.addRow(v, false, false);
				} catch (Exception ex) {
					//log.error(Utils.logException(ex));
				}
			}
		});
		map.put(EVENT_WITHDRAWATS, new EventHandler() {


			@Override
			public void execute(Event param) {
				/*log.info("execute EVENT_WITHDRAWATS "
						+ param.getParam().get(PARAM_SCHEDULE).toString());*/
				Schedule sch = (Schedule) param.getParam().get(PARAM_SCHEDULE);
				//System.out.println(" schedule == " + sch.getcAtsid());
				sch.setcAtsstatus("w");
				sch.setcWithdrawby(engine.getConnection().getUserId());
				sch.setcWithdrawtime(OrderIDGenerator.getTime());
				sch.setcWithdrawterminal(OrderIDGenerator.getTerminal());


				Message withSch = new Message();
				withSch.getHeader().setField(new BeginString(BEGIN_STRING));
				withSch.getHeader().setField(new MsgType("AW"));


				withSch.setString(999100, sch.getcAtsid());
				withSch.setString(999126, Schedule.C_WITHDRAW);
				withSch.setString(999113, Schedule.C_REQUEST_WITHDRAW);
				withSch.setString(999121, engine.getConnection().getUserId()
						.toUpperCase());
				withSch.setField(getTime(999122, sch.getcWithdrawtime()));
				withSch.setString(999123, OrderIDGenerator.getTerminal());
				// ------------

				withSch.setString(ClientID.FIELD, sch.getcClientid());
				withSch.setField(getTime(666671, sch.getcWithdrawtime()));
				withSch.setString(666677, sch.getcWithdrawby());
				withSch.setString(666679, sch.getcWithdrawterminal());


				param.setField(PARAM_FIX, withSch);
				engine.getConnection().sendEvent(param);
			}
		});
		map.put(EVENT_REQUESTWITHDRAW, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_REQUESTWITHDRAW "
						+ param.getParam().get(PARAM_ORDER).toString());*/
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				order.setStatus(Order.C_SENDING_WITHDRAW);
				order.setWithdrawTime(OrderIDGenerator.getTime());
				order.setWithdrawBy(engine.getConnection().getUserId()
						.toUpperCase());
				order.setWithdrawTerminal(OrderIDGenerator.getTerminal());
				order.setUpdateBy(engine.getConnection().getUserId()
						.toUpperCase());
				order.setUpdateDate(OrderIDGenerator.getTime());
				order.setTerminalId(OrderIDGenerator.getTerminal());
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
				Message wOrder = new Message();
				wOrder.getHeader().setField(new BeginString(BEGIN_STRING));
				wOrder.getHeader().setField(new MsgType("F"));
				wOrder.setField(new OrigClOrdID(order.getId()));
				wOrder.setField(new OrderID(order.getMarketOrderId()));
				wOrder.setField(new ClOrdID(order.getId()));
				wOrder.setField(new quickfix.field.Account(order.getInvType()));
				wOrder.setField(new ClientID(order.getWithdrawBy()));
				wOrder.setField(new Symbol(order.getStock()));
				wOrder.setField(new SymbolSfx(order.getBoard()));
				wOrder.setField(new Side(order.getBOS().charAt(0)));
				wOrder.setField(new TransactTime(order.getWithdrawTime()));
				wOrder.setField(new OrderQty(order.getVolume().doubleValue()));
				wOrder.setField(getTime(666671, order.getWithdrawTime()));
				wOrder.setString(666674, order.getIsFloor());
				wOrder.setString(666677, order.getWithdrawBy());
				wOrder.setString(666679, order.getWithdrawTerminal());
				wOrder.setField(new HandlInst(order.getHandInst()));
				wOrder.setString(8007, order.getIsBasketOrder());
				if (order.getHandInst() == '3') {
					wOrder.setField(new OrderQty(2));
				} else {
					wOrder.setField(new OrderQty(1));
				}
				param.setField(PARAM_FIX, wOrder);
				engine.getConnection().addFilterByOrder(order.getId());
				engine.getConnection().sendEvent(param);
			}
		});
		map.put(EVENT_REQUESTWITHDRAW_ACK, new EventHandler() {
			public void execute(Event param) {
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				/*log.info("execute EVENT_REQUESTWITHDRAW_RECEIVEBYSERVER "
						+ order.getId());*/
				order.setStatus(Order.C_SENDING_WITHDRAWSERVER);
				// for testing only
				// order.setStatus(Order.C_WITHDRAW);
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
			}
		});
		map.put(EVENT_REQUESTWITHDRAW_NACK, new EventHandler() {
			public void execute(Event param) {
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				/*log.info("execute EVENT_REQUESTWITHDRAW_SENTTOSERVERFAILED "
						+ order.getId());*/
				order.setStatus((String) param.getParam().get(PARAM_OLDSTATUS));
				order.setWithdrawTime(null);
				order.setWithdrawBy("");
				order.setWithdrawTerminal("");
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
			}
		});
		map.put(EVENT_REQUESTWITHDRAW_OK, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_WITHDRAWORDER_OK "
						+ param.getParam().get("clordid").toString());*/
				Order order = (Order) engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getDataByKey(
								new Object[] { param.getField("clordid"), "0" });
				if (order != null) {
					order.setStatus(Order.C_WITHDRAW);
					// order.setWithdrawSendTime((Date)param.getField("withdrawtime"));
					order.setWithdrawSendTime((Date) param
							.getField("withdrawsendtime"));
					int idx = engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(
									new String[] { order.getId(),
											order.getTradeNo() });
					engine.getDataStore().get(TradingStore.DATA_ORDER)
							.refresh(idx);
					if (!order.getOrderIDContra().equals("")) {
						idx = engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getIndexByKey(
										new String[] {
												order.getOrderIDContra(),
												order.getTradeNo() });
						if (idx > -1) {
							Order ord = (Order) engine.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getDataByIndex(idx);
							ord.setStatus(Order.C_WITHDRAW);
							ord.setWithdrawSendTime((Date) param
									.getField("withdrawsendtime"));
							engine.getDataStore().get(TradingStore.DATA_ORDER)
									.refresh(idx);
						}
					}
				} else {
					Event e = new Event(EVENT_REFRESH_ORDER);
					e.setField("accountid", "%");
					e.setField("orderid", param.getField("clordid"));
					pushEvent(e);
				}
			}
		});
//*gtc awal
		map.put(EVENT_REQUESTWITHDRAWGTC_OK, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_REQUESTWITHDRAWGTC_OK "
						+ param.getParam().get("clordid").toString());
				Order GTC = (Order) engine
						.getDataStore()
						.get(TradingStore.DATA_GTC)
						.getDataByKey(
								new Object[] { param.getField("clordid"), "0" });
				if (GTC != null) {
					GTC.setStatus(Order.C_WITHDRAW);
					// GTC.setWithdrawSendTime((Date)param.getField("withdrawtime"));
					GTC.setWithdrawSendTime((Date) param
							.getField("withdrawsendtime"));
					int idx = engine.getDataStore().get(TradingStore.DATA_GTC)
							.getIndexByKey(new String[] { GTC.getId() });
					engine.getDataStore().get(TradingStore.DATA_GTC)
							.refresh(idx);
					if (!GTC.getOrderIDContra().equals("")) {
						idx = engine
								.getDataStore()
								.get(TradingStore.DATA_GTC)
								.getIndexByKey(
										new String[] { GTC.getOrderIDContra() });
						if (idx > -1) {
							Order GTC1 = (Order) engine.getDataStore()
									.get(TradingStore.DATA_GTC)
									.getDataByIndex(idx);
							GTC1.setStatus(Order.C_WITHDRAW);
							GTC1.setWithdrawSendTime((Date) param
									.getField("withdrawsendtime"));
							engine.getDataStore().get(TradingStore.DATA_GTC)
									.refresh(idx);
						}
					}
				} else {
					//System.out.println("ELSE");
					Event e = new Event(EVENT_REFRESH_GTC);
					e.setField("accountid", "%");
					e.setField("orderid", param.getField("clordid"));
					pushEvent(e);
				}
			}
		});
//*gtc akhir
		map.put(EVENT_REQUESTWITHDRAW_FAILED, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_WITHDRAWORDER_FAILED "
						+ param.getParam().get("clordid").toString());*/
				Order order = (Order) engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getDataByKey(
								new Object[] { param.getField("clordid"), "0" });
				if (order != null) {
					if (order.getStatus().equals(Order.C_REJECTED)
							|| order.getStatus().equals(Order.C_REJECTED2)
							|| order.getStatus().equals(Order.C_DELETE_REQUEST)) {
						//log.info("ignored because order has beed rejected");
					} else {
						order.setStatus(param.getParam().get("status")
								.toString());
						order.setWithdrawTime(null);
						order.setWithdrawBy("");
						order.setWithdrawTerminal("");
						int idx = engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getIndexByKey(
										new String[] { order.getId(),
												order.getTradeNo() });
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.refresh(idx);
					}
				} else {
					Event e = new Event(EVENT_REFRESH_ORDER);
					e.setField("accountid", "%");
					e.setField("orderid", param.getField("clordid"));
					pushEvent(e);
				}
			}
		});
		map.put(EVENT_REQUESTAMEND, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_REQUESTAMEND "
						+ param.getParam().get(PARAM_OLDORDER).toString()
						+ " amend to "
						+ param.getParam().get(PARAM_NEWORDER).toString());*/
				Order oldorder = (Order) param.getParam().get(PARAM_OLDORDER);
				oldorder.setAmendBy(engine.getConnection().getUserId()
						.toUpperCase());
				oldorder.setAmendTerminal(OrderIDGenerator.getTerminal());
				oldorder.setAmendTime(OrderIDGenerator.getTime());
				oldorder.setStatus(Order.C_SENDING_AMEND);
				oldorder.setUpdateBy(engine.getConnection().getUserId()
						.toUpperCase());
				oldorder.setUpdateDate(OrderIDGenerator.getTime());
				oldorder.setTerminalId(OrderIDGenerator.getTerminal());
				Order neworder = (Order) param.getParam().get(PARAM_NEWORDER);
				neworder.setStatus(Order.C_SENDING_ENTRY);
				neworder.setEntryBy(oldorder.getAmendBy());
				neworder.setEntryTerminal(oldorder.getAmendTerminal());
				neworder.setEntryTime(oldorder.getAmendTime());
				neworder.setMarketRef(neworder.getId());
				neworder.setPrevOrderId(oldorder.getId());
				neworder.setPrevMarketOrderId(oldorder.getMarketOrderId());
				neworder.setPrevDoneVolume(oldorder.getDoneVol());
				oldorder.setNextOrderId(neworder.getId());
				oldorder.setNextMarketOrderId(neworder.getMarketOrderId());
				neworder.setUpdateBy(engine.getConnection().getUserId()
						.toUpperCase());
				neworder.setUpdateDate(OrderIDGenerator.getTime());
				neworder.setTerminalId(OrderIDGenerator.getTerminal());

				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { oldorder.getId(),
										oldorder.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
				Vector v = new Vector(1);
				v.addElement(OrderDef.createTableRow(neworder));
				engine.getDataStore().get(TradingStore.DATA_ORDER)
						.addRow(v, false, false);
				Message aOrder = new Message();
				aOrder.getHeader().setField(new BeginString(BEGIN_STRING));
				aOrder.getHeader().setField(new MsgType("G"));
				aOrder.setField(new OrderID(oldorder.getMarketOrderId()));
				aOrder.setField(new ClientID(oldorder.getAmendBy()));
				aOrder.setField(new OrigClOrdID(oldorder.getId()));
				aOrder.setField(new ClOrdID(neworder.getId()));
				aOrder.setField(new quickfix.field.Account(neworder
						.getInvType()));
				aOrder.setField(new HandlInst(neworder.getHandInst()));
				aOrder.setField(new Symbol(neworder.getStock()));
				aOrder.setField(new Side(neworder.getBOS().charAt(0)));
				aOrder.setField(getTime(TransactTime.FIELD,
						oldorder.getAmendTime()));
				aOrder.setField(new OrderQty(neworder.getVolume().doubleValue()));
				aOrder.setField(new OrdType(neworder.getOrdType().charAt(0)));
				aOrder.setField(new Price(neworder.getPrice().doubleValue()));
				aOrder.setField(new TimeInForce(neworder.getOrderType().charAt(
						0)));
				aOrder.setString(376, neworder.getComplianceId());
				aOrder.setField(getTime(666671, oldorder.getAmendTime()));
				aOrder.setString(666674, oldorder.getIsFloor());
				aOrder.setString(666677, oldorder.getAmendBy());
				aOrder.setString(666678, neworder.getMarketref());
				aOrder.setString(666679, oldorder.getAmendTerminal());
				aOrder.setField(new HandlInst(neworder.getHandInst()));
				aOrder.setField(new SymbolSfx(neworder.getBoard()));
				aOrder.setString(666676, neworder.getAccId());
				if (neworder.getHandInst() == '3') {
					aOrder.setString(7048, neworder.getContraBroker());
					aOrder.setString(7049, neworder.getContraUser());
				}
				param.setField(PARAM_FIX, aOrder);
				// yosep 18032016
				mapAmendOrder.put(neworder.getId(), oldorder.getId());

				engine.getConnection().addFilterByOrder(oldorder.getId());
				engine.getConnection().addFilterByOrder(neworder.getId());

				engine.getConnection().sendEvent(param);
			}
		});
		map.put(EVENT_REQUESTAMEND_ACK, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_REQUESTAMEND_RECEIVEBYSERVER "
						+ param.getParam().get(PARAM_OLDORDER).toString()
						+ " to "
						+ param.getParam().get(PARAM_NEWORDER).toString());*/
				Order oldorder = (Order) param.getParam().get(PARAM_OLDORDER);
				oldorder.setStatus(Order.C_SENDING_AMENDSERVER);
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { oldorder.getId(),
										oldorder.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
				Order neworder = (Order) param.getParam().get(PARAM_NEWORDER);
				neworder.setStatus(Order.C_SENDING_ENTRYSERVER);
				// for testing only
				// oldorder.setStatus(Order.C_AMEND);
				// neworder.setStatus(Order.C_OPEN);
				// end testing
				idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { neworder.getId(),
										neworder.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
			}
		});
//*gtc awal
		map.put(EVENT_REQUESTAMENDGTC_OK, new EventHandler() {
			public void execute(Event param) {
				log.info("execute EVENT_REQUESTAMENDGTC_OK "
						+ param.getParam().get("clordid").toString());
				Order GTC = (Order) engine
						.getDataStore()
						.get(TradingStore.DATA_GTC)
						.getDataByKey(
								new Object[] { param.getField("clordid") });
				if (GTC != null) {
					GTC.setStatus(Order.C_AMEND);
					GTC.setAmendSendTime((Date) param.getField("amendtime"));
					int idx = engine.getDataStore().get(TradingStore.DATA_GTC)
							.getIndexByKey(new String[] { GTC.getId(),
							// order.getTradeNo()
									});
					engine.getDataStore().get(TradingStore.DATA_GTC)
							.refresh(idx);
				} else {
					Event e = new Event(EVENT_REFRESH_GTC);
					e.setField("accountid", "%");
					e.setField("orderid", param.getField("clordid"));
					pushEvent(e);
				}
			}
		});
//*gtc akhir
		map.put(EVENT_REQUESTAMEND_NACK, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_REQUESTAMEND_SENTTOSERVERFAILED "
						+ param.getParam().get(PARAM_OLDORDER).toString()
						+ " amend to "
						+ param.getParam().get(PARAM_NEWORDER).toString());*/
				Order oldorder = (Order) param.getParam().get(PARAM_OLDORDER);
				oldorder.setAmendBy("");
				oldorder.setAmendTime(null);
				oldorder.setAmendTerminal("");
				oldorder.setNextOrderId("");
				oldorder.setNextMarketOrderId("");
				oldorder.setStatus((String) param.getParam().get(
						PARAM_OLDSTATUS));
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { oldorder.getId(),
										oldorder.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
				Order neworder = (Order) param.getParam().get(PARAM_NEWORDER);
				neworder.setStatus(Order.C_FAILED);
				neworder.setPrevMarketOrderId("");
				neworder.setPrevOrderId("");
				idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { neworder.getId(),
										neworder.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
			}
		});
		map.put(EVENT_REQUESTAMEND_OK, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_REQUESTAMEND_OK "
						+ param.getParam().get("clordid").toString());*/
				Order order = (Order) engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getDataByKey(
								new Object[] { param.getField("clordid"), "0" });
				if (order != null) {
					order.setStatus(Order.C_AMEND);
					order.setAmendSendTime((Date) param.getField("amendtime"));
					int idx = engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(
									new String[] { order.getId(),
											order.getTradeNo() });
					engine.getDataStore().get(TradingStore.DATA_ORDER)
							.refresh(idx);
				} else {
					Event e = new Event(EVENT_REFRESH_ORDER);
					e.setField("accountid", "%");
					e.setField("orderid", param.getField("clordid"));
					pushEvent(e);
				}
			}
		});
		map.put(EVENT_REQUESTAMEND_FAILED, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_REQUESTAMEND_FAILED "
						+ param.getParam().get("clordid").toString());*/
				Order order = (Order) engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getDataByKey(
								new Object[] { param.getField("clordid"), "0" });
				if (order != null) {
					order.setStatus(Order.C_REJECTED2);
					order.setPrevMarketOrderId("");
					order.setPrevOrderId("");
					order.setNote((String) param.getParam().get("description"));
					int idx = engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(
									new String[] { order.getId(),
											order.getTradeNo() });
					engine.getDataStore().get(TradingStore.DATA_ORDER)
							.refresh(idx);
				} else {
					Event e = new Event(EVENT_REFRESH_ORDER);
					e.setField("accountid", "%");
					e.setField("orderid", param.getField("clordid"));
					pushEvent(e);
				}
				Order oldorder = (Order) engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getDataByKey(
								new Object[] { param.getField("oldclordid"),
										"0" });
				if (oldorder != null) {
					// if (oldorder.getLot().doubleValue() ==
					// oldorder.getBalLot().doubleValue()){
					// oldorder.setStatus(Order.C_OPEN);
					// } else if (oldorder.getBalLot().doubleValue() <= 0){
					// oldorder.setStatus(Order.C_FULL_MATCH);
					// } else {
					// oldorder.setStatus(Order.C_PARTIAL_MATCH);
					// }
					oldorder.setStatus(param.getParam().get("status")
							.toString());
					oldorder.setAmendBy("");
					oldorder.setAmendTerminal("");
					oldorder.setAmendTime(null);
					oldorder.setNextMarketOrderId("");
					oldorder.setNextOrderId("");
					int idx = engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(
									new Object[] { oldorder.getId(),
											oldorder.getTradeNo() });
					engine.getDataStore().get(TradingStore.DATA_ORDER)
							.refresh(idx);
				} else {
					Event e = new Event(EVENT_REFRESH_ORDER);
					e.setField("accountid", "%");
					e.setField("orderid", param.getField("oldclordid"));
					pushEvent(e);
				}
			}
		});
		map.put(EVENT_RECEIVEMATCH, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_RECEIVEMATCH "
						+ param.getParam().get("clordid").toString());*/
				Order order = (Order) engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getDataByKey(
								new Object[] { param.getField("clordid"), "0" });
				if (order != null) {
					Stock stock = (Stock) engine.getDataStore()
							.get(TradingStore.DATA_STOCK)
							.getDataByKey(new Object[] { order.getStock() });
					double matchvol = ((Double) param.getField("matchvolume"))
							.doubleValue();
					double matchlot = matchvol
							/ stock.getLotSize().doubleValue();
					double matchprice = ((Double) param.getField("matchprice"))
							.doubleValue();
					String trxseqno = (String) param.getField("tradeno");
					//#Valdhy20141215
					//String MarketOrderIdContra = order.getMarketOrderId();
					order.setMarketOrderId((String) param.getField("orderid"));

					// get record match order jika sama abaikan
					boolean next = true;
					int idxmatch = engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(
									new Object[] { order.getId(), trxseqno });
					if (idxmatch >= 0) {
						Order omatch = (Order) engine
								.getDataStore()
								.get(TradingStore.DATA_ORDER)
								.getDataByKey(
										new Object[] { order.getId(), trxseqno });
						if (omatch.getDoneLot().doubleValue() == matchlot
								&& omatch.getPrice().doubleValue() == matchprice) {
							next = false;
							omatch.setMarketOrderId((String) param
									.getField("orderid"));

							Vector vo = new Vector(2);
							vo.addElement(OrderDef.createTableRow(omatch));

							engine.getDataStore().get(TradingStore.DATA_ORDER)
									.addRow(vo, false, false);
							log.warn("received duplicate match order, this message ignored");
						}
					}
					if (next) {
						if (order.getOpenTime() == null
								|| order.getOpenTime().equals("")) {
							order.setOpenTime(new Timestamp(((Date) param
									.getField("matchtime")).getTime()));
						}
						order.setMatchTime(new Timestamp(((Date) param
								.getField("matchtime")).getTime()));

						Order morder = new Order();
						morder.fromVector(order.getData());
						morder.setVolume(((Double) param
								.getField("matchvolume")));
						morder.setLot(new Double(morder.getVolume()
								.doubleValue()
								/ stock.getLotSize().doubleValue()));
						morder.setPrice((Double) param.getField("matchprice"));
						morder.setBalVol(matchvol);
						morder.setBalLot(matchlot);
						morder.setDoneVol(new Double(morder.getVolume()
								.doubleValue()));
						morder.setDoneLot(new Double(morder.getLot()
								.doubleValue()));
						morder.setStatus(Order.C_FULL_MATCH);
						morder.setTradeNo((String) param.getField("tradeno"));
						morder.setMarketOrderId((String) param
								.getField("orderid"));
						//#Valdhy20141215
						//morder2.setMarketOrderId(MarketOrderIdContra);
						morder.setContraUser((String) param
								.getField("contratrader"));
						morder.setContraBroker((String) param
								.getField("contrabroker"));


						if (idxmatch == -1 && matchvol > 0) { // process jika
																// data match
																// emang belum
																// ada diclient


							order.setBalVol(new Double(order.getBalVol()
									.doubleValue() - matchvol));
							order.setBalLot(new Double(order.getBalLot()
									.doubleValue() - matchlot));
							order.setDoneVol(new Double(order.getDoneVol()
									.doubleValue() + matchvol));
							order.setDoneLot(new Double(order.getDoneLot()
									.doubleValue() + matchlot));
							Double matchcounter = (Double) order
									.getMatchCounter();
							if (matchcounter == null) {
								matchcounter = new Double(0);
							}
							order.setMatchCounter(new Double(matchcounter
									.doubleValue() + 1));
							//log.info("match not found " + order.getBalVol());
							if (order.getBalVol().doubleValue() < 0) {
								order.setBalVol(new Double(0));
								order.setBalLot(new Double(0));
								order.setDoneVol(new Double(order.getVolume()
										.doubleValue()));
								order.setDoneLot(new Double(order.getLot()
										.doubleValue()));
							}

							// jika order ini done tapi udah terlanjur diamend
							// kurangin balance sesuai match utk order barunya
							// dengan ambil data ordernya dari server
							Order neworder = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getDataByField(
											new String[] { order.getId(), "0",
													"O" },
											new int[] { Order.C_PREVORDERID,
													Order.C_TRADENO,
													Order.C_STATUSID });

							if (neworder == null) {
								neworder = (Order) engine
										.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.getDataByField(
												new String[] { order.getId(),
														"0", "1" },
												new int[] {
														Order.C_PREVORDERID,
														Order.C_TRADENO,
														Order.C_STATUSID });
							}

							if (neworder == null) {
								neworder = (Order) engine
										.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.getDataByField(
												new String[] { order.getId(),
														"0", "EA" },
												new int[] {
														Order.C_PREVORDERID,
														Order.C_TRADENO,
														Order.C_STATUSID });
							}

							if (neworder != null) {
								/*log.info("FOUND match for order who has been ammendment. please subtract balance new order with "
										+ morder.getLot().doubleValue()
										+ " lot");*/
								neworder.setBalLot(new Double(neworder
										.getBalLot().doubleValue()
										- morder.getLot().doubleValue()));
								neworder.setBalVol(new Double(neworder
										.getBalVol().doubleValue()
										- morder.getVolume().doubleValue()));
								neworder.setDoneLot(new Double(neworder
										.getDoneLot().doubleValue()
										+ morder.getLot().doubleValue()));
								neworder.setDoneVol(new Double(neworder
										.getDoneVol().doubleValue()
										+ morder.getVolume().doubleValue()));
								neworder.setStatus(neworder.getBalVol()
										.doubleValue() <= 0 ? Order.C_FULL_MATCH
										: Order.C_PARTIAL_MATCH);
								int temp = engine
										.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.getIndexByKey(
												new Object[] {
														neworder.getId(), "0" });
								if (temp != -1) {
									engine.getDataStore()
											.get(TradingStore.DATA_ORDER)
											.refresh(temp);
								}
							}

							if (!order.getStatus().equals(Order.C_WITHDRAW)
									&& !order.getStatus().equals(Order.C_AMEND)) {
								order.setStatus(order.getBalVol().doubleValue() <= 0 ? Order.C_FULL_MATCH
										: Order.C_PARTIAL_MATCH);
							}
							order.setMarketOrderId((String) param
									.getField("orderid"));
							int idx = engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getIndexByKey(
											new Object[] { order.getId(),
													order.getTradeNo() });
							engine.getDataStore().get(TradingStore.DATA_ORDER)
									.refresh(idx);
						}

						Vector vo = new Vector(2);
						vo.addElement(OrderDef.createTableRow(morder));
						engine.getDataStore().get(TradingStore.DATA_ORDER)
								.addRow(vo, false, false);

						if (!order.getOrderIDContra().equals("")) {
							Order order2 = (Order) engine
									.getDataStore()
									.get(TradingStore.DATA_ORDER)
									.getDataByKey(
											new Object[] {
													order.getOrderIDContra(),
													"0" });
							if (order2 == null)
								return;
							if (order2.getOpenTime() == null
									|| order2.getOpenTime().equals("")) {
								order2.setOpenTime(new Timestamp(((Date) param
										.getField("matchtime")).getTime()));
							}
							order2.setMatchTime(new Timestamp(((Date) param
									.getField("matchtime")).getTime()));
							Order morder2 = new Order();
							morder2.fromVector(order2.getData());
							//System.out.println("marketordid "+ order2.getMarketOrderId() +" orderidcontra "+order.getOrderIDContra() +" orderid "+morder2.getId() );
							
							morder2.setVolume(((Double) param
									.getField("matchvolume")));
							morder2.setLot(new Double(morder2.getVolume()
									.doubleValue()
									/ stock.getLotSize().doubleValue()));
							morder2.setPrice((Double) param
									.getField("matchprice"));
							morder2.setBalVol(new Double(0));
							morder2.setBalLot(new Double(0));
							morder2.setDoneVol(new Double(morder.getVolume()
									.doubleValue()));
							morder2.setDoneLot(new Double(morder.getLot()
									.doubleValue()));
							morder2.setStatus(Order.C_FULL_MATCH);
							morder2.setTradeNo((String) param
									.getField("tradeno"));
							//#Valdhy20141110
							//morder2.setMarketOrderId((String) param
							//		.getField("orderid"));
							//morder2.setMarketOrderId((String) order2.getMarketOrderId());
							//#Valdhy20141215
							//morder2.setMarketOrderId(MarketOrderIdContra);
							morder2.setContraUser((String) param
									.getField("contratrader"));
							morder2.setContraBroker((String) param
									.getField("contrabroker"));

							if (idxmatch == -1 && matchvol > 0) { // process
																	// jika data
																	// match
																	// emang
																	// belum ada
																	// diclient
								order2.setBalVol(new Double(order2.getBalVol()
										.doubleValue() - matchvol));
								order2.setBalLot(new Double(order2.getBalLot()
										.doubleValue() - matchlot));
								order2.setDoneVol(new Double(order2
										.getDoneVol().doubleValue() + matchvol));
								order2.setDoneLot(new Double(order2
										.getDoneLot().doubleValue() + matchlot));
								Double matchcounter = (Double) order2
										.getMatchCounter();
								if (matchcounter == null) {
									matchcounter = new Double(0);
								}
								order2.setMatchCounter(new Double(matchcounter
										.doubleValue() + 1));
								if (order2.getBalVol().doubleValue() < 0) {
									order2.setBalVol(new Double(0));
									order2.setBalLot(new Double(0));
									order2.setDoneVol(new Double(order2
											.getVolume().doubleValue()));
									order2.setDoneLot(new Double(order2
											.getLot().doubleValue()));
								}
								if (!order2.getStatus()
										.equals(Order.C_WITHDRAW)
										&& !order2.getStatus().equals(
												Order.C_AMEND)) {
									order2.setStatus(order2.getBalVol()
											.doubleValue() <= 0 ? Order.C_FULL_MATCH
											: Order.C_PARTIAL_MATCH);
								}
								//order2.setMarketOrderId((String) param
								//		.getField("orderid"));
								int idx = engine
										.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.getIndexByKey(
												new Object[] { order2.getId(),
														order2.getTradeNo() });
								engine.getDataStore()
										.get(TradingStore.DATA_ORDER)
										.refresh(idx);
							}

							Vector vo2 = new Vector(2);
							vo2.addElement(OrderDef.createTableRow(morder2));
							engine.getDataStore().get(TradingStore.DATA_ORDER)
									.addRow(vo2, false, false);
						}
					}
				} else {
					Event e = new Event(EVENT_REFRESH_ORDER);
					e.setField("accountid", "%");
					e.setField("orderid", param.getField("clordid"));
					pushEvent(e);
				}
			}
		});
		map.put(EVENT_RECEIVEORDER, new EventHandler() {
			public void execute(Event param) {
			//	log.info("execute EVENT_RECEIVEORDER "+ param.getParam().get(PARAM_ORDER).toString());
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				if (idx != -1) {
					Order currorder = (Order) engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getDataByKey(
									new String[] { order.getId(),
											order.getTradeNo() });
					currorder.fromVector(order.getData());
					engine.getDataStore().get(TradingStore.DATA_ORDER)
							.refresh(idx);
				} else {
					Vector v = new Vector(1);
					v.addElement(OrderDef.createTableRow(order));
					engine.getDataStore().get(TradingStore.DATA_ORDER)
							.addRow(v, false, false);
				}
				Account acc = (Account) engine.getDataStore()
						.get(TradingStore.DATA_ACCOUNT)
						.getDataByKey(new String[] { order.getTradingId() });
				idx = engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
						.getIndexByKey(new String[] { order.getTradingId() });
				if (acc != null) {
					acc.setCurrTL(order.getTLBefore());
					engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
							.refresh(idx);
				}
				Portfolio pf = (Portfolio) engine
						.getDataStore()
						.get(TradingStore.DATA_PORTFOLIO)
						.getDataByKey(
								new String[] { order.getTradingId(),
										order.getStock() });
				idx = engine
						.getDataStore()
						.get(TradingStore.DATA_PORTFOLIO)
						.getIndexByKey(
								new String[] { order.getTradingId(),
										order.getStock() });
				if (pf != null) {
					pf.setAvailableVolume(order.getPortBefore());
					pf.setCurrLot(new Double(order.getPortBefore()
							.doubleValue() / Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE))));

					pf.calculate();
					engine.getDataStore().get(TradingStore.DATA_PORTFOLIO)
							.refresh(idx);
				}
				pushEvent(new Event(TradingEventDispatcher.EVENT_RM_ORDER)
						.setField(TradingEventDispatcher.PARAM_ORDER, order));
			}
		});
		map.put(EVENT_RECEIVETRADE, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_RECEIVETRADE "
						+ param.getParam().get(PARAM_TRADE).toString());*/
				Match trade = (Match) param.getParam().get(PARAM_TRADE);
				Order order = (Order) engine.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getDataByKey(new String[] { trade.getId(), "0" });
				if (order != null) {
					Order neword = new Order();
					neword.fromVector(order.getData());
					neword.setTradeNo(trade.getTradeNo());
					neword.setMatchTime(trade.getMatchDate());
					neword.setContraBroker(trade.getContraBroker());
					neword.setContraUser(trade.getContraTrader());
					neword.setPrice(trade.getPrice());
					neword.setLot(trade.getLot());
					neword.setVolume(trade.getVolume());
					Vector vorder = new Vector(1);
					vorder.addElement(OrderDef.createTableRow(neword));
					engine.getDataStore().get(TradingStore.DATA_ORDER)
							.addRow(vorder, false, false);
					Account acc = (Account) engine
							.getDataStore()
							.get(TradingStore.DATA_ACCOUNT)
							.getDataByKey(new String[] { order.getTradingId() });
					int idx = engine
							.getDataStore()
							.get(TradingStore.DATA_ACCOUNT)
							.getIndexByKey(
									new String[] { order.getTradingId() });
					if (acc != null) {
						acc.setCurrTL(order.getTLBefore());
						engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
								.refresh(idx);
					}
					Portfolio pf = (Portfolio) engine
							.getDataStore()
							.get(TradingStore.DATA_PORTFOLIO)
							.getDataByKey(
									new String[] { order.getTradingId(),
											order.getStock() });
					idx = engine
							.getDataStore()
							.get(TradingStore.DATA_PORTFOLIO)
							.getIndexByKey(
									new String[] { order.getTradingId(),
											order.getStock() });
					if (pf != null) {
						pf.setAvailableVolume(order.getPortBefore());
						pf.setCurrLot(new Double(order.getPortBefore()
								.doubleValue() / Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE))));

						pf.calculate();
						engine.getDataStore().get(TradingStore.DATA_PORTFOLIO)
								.refresh(idx);
					}
					pushEvent(new Event(TradingEventDispatcher.EVENT_RM_MATCH)
							.setField(TradingEventDispatcher.PARAM_ORDER,
									neword));
				}
			}
		});
		map.put(EVENT_RM_ORDER, new EventHandler() {
			public void execute(Event param) {
				Order o = (Order) param.getParam().get(PARAM_ORDER);
				int idx = engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
						.getIndexByKey(new String[] { o.getTradingId() });
				if (idx != -1) {
					GridModel m = engine.getDataStore().get(
							TradingStore.DATA_ORDER);
					double openbuy = 0, opensell = 0, stockopenbuy = 0, stockopensell = 0;
					synchronized (m) {
						int total = m.getRowCount();
						for (int i = 0; i < total; i++) {
							Order ord = (Order) m.getDataByIndex(i);
							if (ord.getTradingId().equals(o.getTradingId())
									&& ord.getTradeNo().equals("0")) {
								boolean valid = ord.getStatus().equals(
										Order.C_OPEN)
										|| ord.getStatus().equals(
												Order.C_PARTIAL_MATCH)
										|| ord.getStatus().equals(
												Order.C_REQUEST_WITHDRAW)
										|| ord.getStatus().equals(
												Order.C_REQUEST_AMEND)
										|| ord.getStatus().equals(
												Order.C_SENDING_WITHDRAW)
										|| ord.getStatus().equals(
												Order.C_SENDING_AMEND);
								if (valid) {
									openbuy = openbuy
											+ (ord.getBOS().equals(Order.C_BUY) ? ord
													.getBalLot().doubleValue()
													: 0);
									opensell = opensell
											+ (ord.getBOS()
													.equals(Order.C_SELL) ? ord
													.getBalLot().doubleValue()
													: 0);
								}
								if (valid
										&& ord.getStock().equals(o.getStock())) {
									stockopenbuy = stockopenbuy
											+ (ord.getBOS().equals(Order.C_BUY) ? ord
													.getBalLot().doubleValue()
													: 0);
									stockopensell = stockopensell
											+ (ord.getBOS()
													.equals(Order.C_SELL) ? ord
													.getBalLot().doubleValue()
													: 0);

								}
							}
						}
					}
					Account acc = (Account) engine.getDataStore()
							.get(TradingStore.DATA_ACCOUNT)
							.getDataByKey(new String[] { o.getTradingId() });
					acc.setBid(new Double(openbuy));
					acc.setOffer(new Double(opensell));
					engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
							.refresh(idx);
					idx = engine
							.getDataStore()
							.get(TradingStore.DATA_PORTFOLIO)
							.getIndexByKey(
									new String[] { o.getTradingId(),
											o.getStock() });
					if (idx != -1) {
						Portfolio pf = (Portfolio) engine
								.getDataStore()
								.get(TradingStore.DATA_PORTFOLIO)
								.getDataByKey(
										new String[] { o.getTradingId(),
												o.getStock() });
						pf.setClosingPrice(new Double(stockopenbuy * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE))));
						// pf.setOpenSellVolume(new Double(stockopensell*500));
						pf.calculate();
					} else {
						pushEvent(new Event(EVENT_REFRESH_PORTFOLIO).setField(
								PARAM_ACCOUNTID, o.getTradingId()).setField(
								PARAM_STOCK, o.getStock()));
					}
				}
			}
		});
		map.put(EVENT_RM_MATCH, new EventHandler() {
			public void execute(Event param) {
				Order o = (Order) param.getParam().get(PARAM_ORDER);
				int idx = engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
						.getIndexByKey(new String[] { o.getTradingId() });
				if (idx != -1) {
					GridModel m = engine.getDataStore().get(
							TradingStore.DATA_ORDER);
					double tradebuy = 0, tradesell = 0, stocktradebuy = 0, stocktradesell = 0;
					synchronized (m) {
						int total = m.getRowCount();
						for (int i = 0; i < total; i++) {
							Order ord = (Order) m.getDataByIndex(i);
							if (ord.getTradingId().equals(o.getTradingId())
									&& ord.getTradeNo().equals("0")) {
								boolean valid = ord.getStatus().equals(
										Order.C_PARTIAL_MATCH)
										|| ord.getStatus().equals(
												Order.C_FULL_MATCH)
										|| ord.getStatus().equals(
												Order.C_REQUEST_WITHDRAW)
										|| ord.getStatus().equals(
												Order.C_REQUEST_AMEND)
										|| ord.getStatus().equals(
												Order.C_SENDING_WITHDRAW)
										|| ord.getStatus().equals(
												Order.C_SENDING_AMEND)
										|| ord.getStatus().equals(
												Order.C_WITHDRAW);
								if (valid) {
									tradebuy = tradebuy
											+ (ord.getBOS().equals(Order.C_BUY) ? ord
													.getDoneLot().doubleValue()
													: 0);
									tradesell = tradesell
											+ (ord.getBOS()
													.equals(Order.C_SELL) ? ord
													.getDoneLot().doubleValue()
													: 0);
								}
								if (valid
										&& ord.getStock().equals(o.getStock())) {
									stocktradebuy = stocktradebuy
											+ (ord.getBOS().equals(Order.C_BUY) ? ord
													.getDoneLot().doubleValue()
													: 0);
									stocktradesell = stocktradesell
											+ (ord.getBOS()
													.equals(Order.C_SELL) ? ord
													.getDoneLot().doubleValue()
													: 0);
								}
							}
						}
					}
					Account acc = (Account) engine.getDataStore()
							.get(TradingStore.DATA_ACCOUNT)
							.getDataByKey(new String[] { o.getTradingId() });
					acc.setBuy(new Double(tradebuy));
					acc.setSell(new Double(tradesell));
					engine.getDataStore().get(TradingStore.DATA_ACCOUNT)
							.refresh(idx);
					idx = engine
							.getDataStore()
							.get(TradingStore.DATA_PORTFOLIO)
							.getIndexByKey(
									new String[] { o.getTradingId(),
											o.getStock() });
					if (idx != -1) {
						Portfolio pf = (Portfolio) engine
								.getDataStore()
								.get(TradingStore.DATA_PORTFOLIO)
								.getDataByKey(
										new String[] { o.getTradingId(),
												o.getStock() });
						pf.setClosingPrice(new Double(stocktradebuy * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE))));
						// pf.setOpenSellVolume(new Double(stocktradesell*500));
						pf.calculate();
					} else {
						pushEvent(new Event(EVENT_REFRESH_PORTFOLIO).setField(
								PARAM_ACCOUNTID, o.getTradingId()).setField(
								PARAM_STOCK, o.getStock()));
					}
				}
			}
		});
		map.put(EVENT_SENDORDER, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_SENDORDER "
						+ param.getParam().get(PARAM_ORDER).toString());*/
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				if (order.getStatus().equals(Order.C_TEMPORARY)) {
					order.setStatus(Order.C_SENDING_ENTRY);
					int idx = engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(
									new String[] { order.getId(),
											order.getTradeNo() });
					engine.getDataStore().get(TradingStore.DATA_ORDER)
							.refresh(idx);
					engine.getConnection().sendEvent(param);
				}
			}
		});
		map.put(EVENT_SENDORDER_ACK, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_SENDORDER_ACK "
						+ param.getParam().get(PARAM_ORDER).toString());*/
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				order.setStatus(Order.C_ENTRY);
				// for testing only
				order.setStatus(Order.C_OPEN);
				// end testing
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
			}
		});
		map.put(EVENT_SENDORDER_NACK, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_SENDORDER_NACK "
						+ param.getParam().get(PARAM_ORDER).toString());*/
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				order.setStatus(Order.C_TEMPORARY);
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
			}
		});
		map.put(EVENT_DELETEORDER, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_DELETEORDER "
						+ param.getParam().get(PARAM_ORDER).toString());*/
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				order.setStatus(Order.C_SENDING_DELETE);
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
				engine.getConnection().sendEvent(param);
			}
		});
		map.put(EVENT_DELETEORDER_ACK, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_DELETEORDER_ACK "
						+ param.getParam().get(PARAM_ORDER).toString());*/
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				order.setStatus(Order.C_DELETE_REQUEST);
				// for testing only
				order.setStatus(Order.C_DELETE);
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
			}
		});
		map.put(EVENT_DELETEORDER_NACK, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_DELETEORDER_NACK "
						+ param.getParam().get(PARAM_ORDER).toString());*/
				Order order = (Order) param.getParam().get(PARAM_ORDER);
				order.setStatus(Order.C_TEMPORARY);
				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_ORDER)
						.getIndexByKey(
								new String[] { order.getId(),
										order.getTradeNo() });
				engine.getDataStore().get(TradingStore.DATA_ORDER).refresh(idx);
			}
		});

		map.put(EVENT_REQUESTWITHDRAW_MULTI, new EventHandler() {

			@Override
			public void execute(Event param) {
				Vector vorder = (Vector) param.getParam().get(PARAM_ORDER);
				//log.info("execute EVENT_REQUESTWITHDRAW MULTI");
				for (int i = 0; i < vorder.size(); i++) {
					final Order order = (Order) vorder.elementAt(i);
					order.setStatus(Order.C_SENDING_WITHDRAW);
					order.setWithdrawTime(OrderIDGenerator.getTime());
					order.setWithdrawBy(engine.getConnection().getUserId()
							.toUpperCase());
					order.setWithdrawTerminal(OrderIDGenerator.getTerminal());
					order.setUpdateBy(engine.getConnection().getUserId()
							.toUpperCase());
					order.setUpdateDate(OrderIDGenerator.getTime());
					order.setTerminalId(OrderIDGenerator.getTerminal());
					int idx = engine
							.getDataStore()
							.get(TradingStore.DATA_ORDER)
							.getIndexByKey(
									new String[] { order.getId(),
											order.getTradeNo() });
					engine.getDataStore().get(TradingStore.DATA_ORDER)
							.refresh(idx);
					Message wOrder = new Message();
					wOrder.getHeader().setField(new BeginString(BEGIN_STRING));
					wOrder.getHeader().setField(new MsgType("F"));
					wOrder.setField(new OrigClOrdID(order.getId()));
					wOrder.setField(new OrderID(order.getMarketOrderId()));
					wOrder.setField(new ClOrdID(order.getId()));
					wOrder.setField(new quickfix.field.Account(order
							.getInvType()));
					wOrder.setField(new ClientID(order.getWithdrawBy()));
					wOrder.setField(new Symbol(order.getStock()));
					wOrder.setField(new SymbolSfx(order.getBoard()));
					wOrder.setField(new Side(order.getBOS().charAt(0)));
					wOrder.setField(new TransactTime(order.getWithdrawTime()));
					wOrder.setField(new OrderQty(order.getVolume()
							.doubleValue()));
					wOrder.setField(getTime(666671, order.getWithdrawTime()));
					wOrder.setString(666674, order.getIsFloor());
					wOrder.setString(666677, order.getWithdrawBy());
					wOrder.setString(666679, order.getWithdrawTerminal());
					wOrder.setField(new HandlInst(order.getHandInst()));
					wOrder.setString(8007, order.getIsBasketOrder());
					if (order.getHandInst() == '3') {
						wOrder.setField(new OrderQty(2));
					} else {
						wOrder.setField(new OrderQty(1));
					}
					// param.setField(PARAM_FIX, wOrder);
					Event e = new Event(
							TradingEventDispatcher.EVENT_REQUESTWITHDRAW)
							.setField(TradingEventDispatcher.PARAM_OLDSTATUS,
									order.getStatus()).setField(PARAM_FIX,
									wOrder);
					engine.getConnection().addFilterByOrder(order.getId());
					engine.getConnection().sendEvent(e);
				}
			}

		});
//*gtc awal
		map.put(EVENT_CREATEGTCMULTI, new EventHandler() {
			public void execute(Event param) {
				Vector vorder = (Vector) param.getParam().get(PARAM_GTC);
				log.info("execute EVENT_CREATEGTC MULTI ");
				Order buy = new Order();
				for (int i = 0; i < vorder.size(); i++) {
					final Order order = (Order) vorder.elementAt(i);
					order.setStatus(order.getStatus().equals(Order.C_ENTRY) ? Order.C_SENDING_ENTRY
							: Order.C_SENDING_TEMPORARY);
					order.setEntryTime(OrderIDGenerator.getTime());
					order.setEntryBy(engine.getConnection().getUserId()
							.toUpperCase());
					order.setEntryTerminal(OrderIDGenerator.getTerminal());
					order.setUpdateBy(engine.getConnection().getUserId()
							.toUpperCase());
					order.setUpdateDate(OrderIDGenerator.getTime());
					order.setTerminalId(OrderIDGenerator.getTerminal());
					if (order.getIsBasketOrder().equals("0")) {
						try {
							String stock = order.getStock();
							Stock s = (Stock) engine.getDataStore()
									.get(TradingStore.DATA_STOCK)
									.getDataByKey(new String[] { stock });
							boolean stockpreopening = s.isPreopening();
							String dt = dateFormat2.format(OrderIDGenerator
									.getTime());
							boolean friday = fridayFormat
									.format(OrderIDGenerator.getTime())
									.toUpperCase().equals("FRI");
							String dtop = dt + "-" + "08:45:00";// "09:10:00";
							String dtses1 = dt + "-" + "09:00:00";// "09:30:00";
							String dtses2 = dt + "-"
									+ (friday ? "14:00:00" : "13:30:00");
							String dtendses1 = dt + "-"
									+ (friday ? "11:30:00" : "12:00:00");
							String dtendpre = dt + "-" + "08:55:00";// "09:25:00";
							Date datepreopening = dateFormatAll.parse(dtop);
							Date dateses1 = dateFormatAll.parse(dtses1);
							Date dateses2 = dateFormatAll.parse(dtses2);
							Date enddateses1 = dateFormatAll.parse(dtendses1);
							Date endpre = dateFormatAll.parse(dtendpre);
							Date datecurrent = OrderIDGenerator.getTime();
							if (datecurrent.getTime() < datepreopening
									.getTime()) {
								order.setIsBasketOrder("1");
								order.setBasketTime(stockpreopening ? datepreopening
										: dateses1);
								Utils.showMessage("order will be send at "
										+ (stockpreopening ? datepreopening
												: dateses1), null);
							} else if (datecurrent.getTime() < endpre.getTime()) {
								if (!stockpreopening) {
									order.setIsBasketOrder("1");
									order.setBasketTime(dateses1);
									Utils.showMessage("order will be send at "
											+ (dateses1), null);
								}
							} else if (datecurrent.getTime() > endpre.getTime()
									&& datecurrent.getTime() < dateses1
											.getTime()) {
								order.setIsBasketOrder("1");
								order.setBasketTime(dateses1);
								Utils.showMessage("order will be send at "
										+ (dateses1), null);
							} else if (datecurrent.getTime() > enddateses1
									.getTime()
									&& datecurrent.getTime() < dateses2
											.getTime()) {
								order.setIsBasketOrder("1");
								order.setBasketTime(dateses2);
								Utils.showMessage("order will be send at "
										+ (dateses2), null);
							}
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}

					Message messageGTC = new Message();
					messageGTC.getHeader().setField(
							new BeginString(BEGIN_STRING));
					messageGTC.getHeader().setField(new MsgType("GTC"));
					messageGTC.setField(getTime(TransactTime.FIELD,
							order.getEntryTime()));
					messageGTC.setField(new ClOrdID(order.getId()));
					messageGTC.setField(new ClientID(order.getEntryBy()));
					messageGTC.setField(new quickfix.field.Account(order
							.getInvType()));
					messageGTC.setField(new HandlInst(order.getHandInst()));
					messageGTC.setField(new Symbol(order.getStock()));
					messageGTC.setField(new SymbolSfx(order.getBoard()));
					messageGTC.setField(new Side(order.getBOS().charAt(0)));
					messageGTC
							.setField(getTime(TransactTime.FIELD, new Date()));
					messageGTC.setField(new SymbolSfx(order.getBoard()));
					messageGTC.setField(getTime(999104, order.getcValiduntil()));
					messageGTC.setField(new OrderQty(order.getVolume()
							.doubleValue()));
					// messageGTC.setField(new
					// (order.getOrderAmount().doubleValue()));
					messageGTC.setField(new OrdType(order.getOrdType()
							.charAt(0)));
					messageGTC.setField(new Price(order.getPrice()
							.doubleValue()));
					messageGTC.setField(new TimeInForce(order.getOrderType()
							.charAt(0)));
					messageGTC.setString(376, order.getComplianceId());
					messageGTC.setField(getTime(666671, new Date()));
					messageGTC.setDouble(666672, order.getCounter()
							.doubleValue());
					messageGTC.setDouble(666673, order.getSplitTo()
							.doubleValue());
					messageGTC.setString(666675, order.getExchange());
					messageGTC.setString(666676, order.getAccId());
					messageGTC.setString(666677, order.getEntryBy());
					messageGTC.setString(666678, order.getMarketref());
					messageGTC.setString(666679, order.getEntryTerminal());
					messageGTC.setString(8007, order.getIsBasketOrder());
					messageGTC.setString(7043, order.getBroker());
					messageGTC.setString(7044, order.getCurrency());
					if (order.getBasketTime() != null)
						messageGTC.setField(getTime(8008, order.getBasketTime()));
					if (order.getHandInst() == '3') {
						messageGTC.setField(new IOIid(order.getNegDealRef()));
						messageGTC.setField(new ContraBroker(order
								.getContraBroker()));
						messageGTC.setField(new ContraTrader(order
								.getContraUser()));
						messageGTC.setString(7045, order.getOrderIDContra());
						messageGTC.setString(7046, order.getAccIDContra());
						messageGTC.setString(7047, order.getInvTypeContra());
						messageGTC.setString(7048, order.getContraBroker());
						messageGTC.setString(7049, order.getContraUser());
						messageGTC.setString(7050, order.getNegDealRef());
						messageGTC.setString(7051, order.getContraBroker());
						messageGTC.setString(7052, order.getComplianceId());
					}

					Event t = new Event(TradingEventDispatcher.EVENT_CREATEGTC)
							.setField(TradingEventDispatcher.PARAM_GTC,
									messageGTC)
							.setField(TradingEventDispatcher.PARAM_OLDSTATUS,
									order.getStatus())
							.setField(PARAM_FIX, messageGTC);
					engine.getConnection().sendEvent(t);
				}
			}
		});
//*gtc awal
		map.put(EVENT_CREATEORDERMULTI, new EventHandler() {
			public void execute(Event param) {
				Vector vorder = (Vector) param.getParam().get(PARAM_ORDER);
				//log.info("execute EVENT_CREATEORDER MULTI ");
				for (int i = 0; i < vorder.size(); i++) {
					Order buy = new Order();
					final Order order = (Order) vorder.elementAt(i);
					order.setStatus(order.getStatus().equals(Order.C_ENTRY) ? Order.C_SENDING_ENTRY: Order.C_SENDING_TEMPORARY);
					order.setEntryTime(OrderIDGenerator.getTime());
					order.setEntryBy(engine.getConnection().getUserId().toUpperCase());
					order.setEntryTerminal(OrderIDGenerator.getTerminal());
					order.setUpdateBy(engine.getConnection().getUserId().toUpperCase());
					order.setUpdateDate(OrderIDGenerator.getTime());
					order.setTerminalId(OrderIDGenerator.getTerminal());
					if (order.getIsBasketOrder().equals("0")) {
						try {
							String stock = order.getStock();
							Stock s = (Stock) engine.getDataStore().get(TradingStore.DATA_STOCK).getDataByKey(new String[] { stock });
							boolean stockpreopening = s.isPreopening();
							String dt = dateFormat2.format(OrderIDGenerator.getTime());
							boolean friday = fridayFormat.format(OrderIDGenerator.getTime()).toUpperCase().equals("FRI");
							String dtop = dt + "-" + "08:45:00";// "09:10:00";
							String dtses1 = dt + "-" + "09:00:00";// "09:30:00";
							String dtses2 = dt + "-"+ (friday ? "14:00:00" : "13:30:00");
							String dtendses1 = dt + "-"+ (friday ? "11:30:00" : "12:00:00");
							String dtendpre = dt + "-" + "08:55:00";// "09:25:00";
							Date datepreopening = dateFormatAll.parse(dtop);
							Date dateses1 = dateFormatAll.parse(dtses1);
							Date dateses2 = dateFormatAll.parse(dtses2);
							Date enddateses1 = dateFormatAll.parse(dtendses1);
							Date endpre = dateFormatAll.parse(dtendpre);
							Date datecurrent = OrderIDGenerator.getTime();
							if (datecurrent.getTime() < datepreopening	.getTime()) {
								order.setIsBasketOrder("1");
								order.setBasketTime(stockpreopening ? datepreopening: dateses1);
								Utils.showMessage("order will be send at "	+ (stockpreopening ? datepreopening	: dateses1), null);
							} else if (datecurrent.getTime() < endpre.getTime()) {
								if (!stockpreopening) {
									order.setIsBasketOrder("1");
									order.setBasketTime(dateses1);
									Utils.showMessage("order will be send at "+ (dateses1), null);
								}
							} else if (datecurrent.getTime() > endpre.getTime()&& datecurrent.getTime() < dateses1.getTime()) {
								order.setIsBasketOrder("1");
								order.setBasketTime(dateses1);
								Utils.showMessage("order will be send at "+ (dateses1), null);
							} else if (datecurrent.getTime() > enddateses1
									.getTime()	&& datecurrent.getTime() < dateses2	.getTime()) {
								order.setIsBasketOrder("1");
								order.setBasketTime(dateses2);
								Utils.showMessage("order will be send" +" at "+ (dateses2), null);
							}
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}

					// TODO ganti ordertype  
					Order gtc = (Order) engine.getDataStore()
								.get(TradingStore.DATA_GTC)
								.getDataByKey(new String[] { order.getId() });
//					if(gtc != null){
					if(order.getOrderType().equals("G")){
						if(order.getBoard().equals("TN"))
						{
							order.setOrderType("S");
						}else {
							order.setOrderType("0");	
						}
					}
					Message newOrder = new Message();
					newOrder.getHeader().setField(new BeginString(BEGIN_STRING));
					newOrder.getHeader().setField(new MsgType("D"));
					newOrder.setField(new ClOrdID(order.getId()));
					newOrder.setField(new ClientID(order.getEntryBy()));
					newOrder.setField(new quickfix.field.Account(order.getInvType()));
					newOrder.setField(new HandlInst(order.getHandInst()));
					newOrder.setField(new Symbol(order.getStock()));
					newOrder.setField(new SymbolSfx(order.getBoard()));
					newOrder.setField(new Side(order.getBOS().charAt(0)));
					newOrder.setField(getTime(TransactTime.FIELD,order.getEntryTime()));
					newOrder.setField(new OrderQty(order.getVolume().doubleValue()));
					newOrder.setField(new OrdType(order.getOrdType().charAt(0)));
					newOrder.setField(new Price(order.getPrice().doubleValue()));
					newOrder.setField(new TimeInForce(order.getOrderType().charAt(0)));
					newOrder.setString(376, order.getComplianceId());
					newOrder.setField(getTime(666671, order.getEntryTime()));
					newOrder.setDouble(666672, order.getCounter().doubleValue());
					newOrder.setDouble(666673, order.getSplitTo().doubleValue());
					newOrder.setString(666675, order.getExchange());
					newOrder.setString(666676, order.getAccId());
					newOrder.setString(666677, order.getEntryBy());
					newOrder.setString(666678, order.getMarketref());
					newOrder.setString(666679, order.getEntryTerminal());
					newOrder.setString(8007, order.getIsBasketOrder());
					newOrder.setString(7043, order.getBroker());
					newOrder.setString(7044, order.getCurrency());
					if (order.getBasketTime() != null)
						newOrder.setField(getTime(8008, order.getBasketTime()));
					if (order.getHandInst() == '3') {
						newOrder.setField(new IOIid(order.getNegDealRef()));
						newOrder.setField(new ContraBroker(order.getContraBroker()));
						newOrder.setField(new ContraTrader(order.getContraUser()));
						newOrder.setString(7045, order.getOrderIDContra());
						newOrder.setString(7046, order.getAccIDContra());
						newOrder.setString(7047, order.getInvTypeContra());
						newOrder.setString(7048, order.getContraBroker());
						newOrder.setString(7049, order.getContraUser());
						newOrder.setString(7050, order.getNegDealRef());
						newOrder.setString(7051, order.getContraBroker());
						newOrder.setString(7052, buy.getComplianceId());
					}
					newOrder.setString(440, " ");
					Event e = new Event(TradingEventDispatcher.EVENT_CREATEORDER).setField(TradingEventDispatcher.PARAM_ORDER, order).setField(TradingEventDispatcher.PARAM_OLDSTATUS,order.getStatus()).setField(PARAM_FIX, newOrder);
					engine.getConnection().addFilterByOrder(order.getId());
					engine.getConnection().sendEvent(e);
				}
			}
		});
		map.put(EVENT_CREATEORDERMULTISPLIT, new EventHandler() {
			public void execute(Event param) {
				Vector vorder = (Vector) param.getParam().get(PARAM_ORDER);
				/*log.info("execute EVENT_CREATEORDER MULTI " + vorder.size());*/

				String sclordid = "", orderqty = "", marketref = "";

				for (int i = 0; i < vorder.size(); i++) {
					Order buy = new Order();
					final Order order = (Order) vorder.elementAt(i);
					order.setStatus(order.getStatus().equals(Order.C_ENTRY) ? Order.C_SENDING_ENTRY: Order.C_SENDING_TEMPORARY);
					order.setEntryTime(OrderIDGenerator.getTime());
					order.setEntryBy(engine.getConnection().getUserId().toUpperCase());
					order.setEntryTerminal(OrderIDGenerator.getTerminal());
					order.setUpdateBy(engine.getConnection().getUserId().toUpperCase());
					order.setUpdateDate(OrderIDGenerator.getTime());
					order.setTerminalId(OrderIDGenerator.getTerminal());
					if (order.getIsBasketOrder().equals("0")) {
						try {
							String stock = order.getStock();
							Stock s = (Stock) engine.getDataStore().get(TradingStore.DATA_STOCK).getDataByKey(new String[] { stock });
							boolean stockpreopening = s.isPreopening();
							String dt = dateFormat2.format(OrderIDGenerator.getTime());
							boolean friday = fridayFormat.format(OrderIDGenerator.getTime()).toUpperCase().equals("FRI");
							String dtop = dt + "-" + "08:45:00";// "09:10:00";
							String dtses1 = dt + "-" + "09:00:00";// "09:30:00";
							String dtses2 = dt + "-"	+ (friday ? "14:00:00" : "13:30:00");
							String dtendses1 = dt + "-"	+ (friday ? "11:30:00" : "12:00:00");
							String dtendpre = dt + "-" + "08:55:00";// "09:25:00";
							Date datepreopening = dateFormatAll.parse(dtop);
							Date dateses1 = dateFormatAll.parse(dtses1);
							Date dateses2 = dateFormatAll.parse(dtses2);
							Date enddateses1 = dateFormatAll.parse(dtendses1);
							Date endpre = dateFormatAll.parse(dtendpre);
							Date datecurrent = OrderIDGenerator.getTime();
							if (datecurrent.getTime() < datepreopening.getTime()) {
								order.setIsBasketOrder("1");
								order.setBasketTime(stockpreopening ? datepreopening	: dateses1);
								Utils.showMessage("order will be send at "	+ (stockpreopening ? datepreopening	: dateses1), null);
							} else if (datecurrent.getTime() < endpre.getTime()) {
								if (!stockpreopening) {
									order.setIsBasketOrder("1");
									order.setBasketTime(dateses1);
									Utils.showMessage("order will be send at "	+ (dateses1), null);
								}
							} else if (datecurrent.getTime() > endpre.getTime()&& datecurrent.getTime() < dateses1.getTime()) {
								order.setIsBasketOrder("1");
								order.setBasketTime(dateses1);
								Utils.showMessage("order will be send at "+ (dateses1), null);
							} else if (datecurrent.getTime() > enddateses1.getTime()&& datecurrent.getTime() < dateses2.getTime()) {
								order.setIsBasketOrder("1");
								order.setBasketTime(dateses2);
								Utils.showMessage("order will be send at "+ (dateses2), null);
							}
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
					sclordid = sclordid + "!" + order.getId();
					orderqty = orderqty + "!" + order.getVolume();
					marketref = marketref + "!" + order.getMarketref();
					engine.getConnection().addFilterByOrder(order.getId());
				}

				Order order = (Order) vorder.elementAt(0);

				Message newOrder = new Message();
				newOrder.getHeader().setField(new BeginString(BEGIN_STRING));
				newOrder.getHeader().setField(new MsgType("SPLIT-ENTRY"));
				newOrder.setField(new ClOrdID(sclordid));
				newOrder.setField(new ClientID(order.getEntryBy()));
				newOrder.setField(new quickfix.field.Account(order.getInvType()));
				newOrder.setField(new HandlInst(order.getHandInst()));
				newOrder.setField(new Symbol(order.getStock()));
				newOrder.setField(new SymbolSfx(order.getBoard()));
				newOrder.setField(new Side(order.getBOS().charAt(0)));
				newOrder.setField(getTime(TransactTime.FIELD,order.getEntryTime()));
				// newOrder.setField(new OrderQty(orderqty));
				newOrder.setString(38, orderqty);
				newOrder.setField(new OrdType(order.getOrdType().charAt(0)));
				newOrder.setField(new Price(order.getPrice().doubleValue()));
				newOrder.setField(new TimeInForce(order.getOrderType().charAt(0)));
				newOrder.setString(376, order.getComplianceId());
				newOrder.setField(getTime(666671, order.getEntryTime()));
				newOrder.setDouble(666672, order.getCounter().doubleValue());
				newOrder.setDouble(666673, order.getSplitTo().doubleValue());
				newOrder.setString(666675, order.getExchange());
				newOrder.setString(666676, order.getAccId());
				newOrder.setString(666677, order.getEntryBy());
				newOrder.setString(666678, marketref);
				newOrder.setString(666679, order.getEntryTerminal());
				newOrder.setString(8007, order.getIsBasketOrder());
				newOrder.setString(7043, order.getBroker());
				newOrder.setString(7044, order.getCurrency());
				
				if (order.getBasketTime() != null)
					newOrder.setField(getTime(8008, order.getBasketTime()));
				
				newOrder.setString(440, " ");
				Event e = new Event(TradingEventDispatcher.EVENT_CREATEORDER).setField(TradingEventDispatcher.PARAM_ORDER, order).setField(TradingEventDispatcher.PARAM_OLDSTATUS,order.getStatus()).setField(PARAM_FIX, newOrder);
				engine.getConnection().sendEvent(e);
				engine.getConnection().addFilterByOrder(sclordid);

			}
		});
		
		map.put(EVENT_CREATESCHEDULE, new EventHandler() {
			public void execute(Event param) {
				/*log.info("execute EVENT_CREATESCHEDULE "+ param.getParam().get(PARAM_SCHEDULE).toString());*/
				final Schedule schedule = (Schedule) param.getParam().get(PARAM_SCHEDULE);
				/*

				 * order.setStatus(order.getStatus().equals(Order.C_ENTRY) ?
				 * Order.C_SENDING_ENTRY : Order.C_SENDING_TEMPORARY);
				 * order.setEntryTime(OrderIDGenerator.getTime());
				 */

				final Schedule sc = new Schedule();
				sc.setcAtsid(schedule.getcAtsid());
				sc.setcAtsdate(OrderIDGenerator.getTime());
				sc.setcClientid(schedule.getcClientid());
				sc.setcBuysell(schedule.getcBuysell());
				sc.setcSecid(schedule.getcSecid());
				sc.setcPirce(schedule.getcPirce());
				sc.setcLot(schedule.getcLot());
				sc.setcVolume(schedule.getcVolume());
				//System.out.println(schedule.getcLabel().toString());
				sc.setcLabel(schedule.getcLabel());
				sc.setcValidity(schedule.getcValidity());
				//System.out.println("valid "+ schedule.getcValidity().toString());
				try {
					String dt = dateFormat2.format(OrderIDGenerator.getTime());
					Calendar cal = Calendar.getInstance();
					if (schedule.getcValidity().equals("D")) {
						String dtClose = dt + "-" + "16:00:00";
						Date untildate = dateFormatAll.parse(dtClose);
						sc.setcValiduntil(untildate);
						//System.out.println("until date .... " + untildate+ "--" + dtClose);
						// gtc 7+ (tidak termasuk hari libur kerja dan nasional)
					} else if (schedule.getcValidity().equals("GTC")) {
						String dtClose = dt + "-" + "12:00:00";
						//System.out.println("GTC --- ");
						Date untildate = dateFormatAll.parse(dtClose);
						sc.setcValiduntil(untildate);
					} else if (schedule.getcValidity().equals("S")) {
						String dtClose1 = dt + "-" + "12:00:00";
						String dtClose2 = dt + "-" + "16:20:00";
						Date untildate1 = dateFormatAll.parse(dtClose1);
						Date untildate2 = dateFormatAll.parse(dtClose2);
						Date dtnow = OrderIDGenerator.getTime();
						int result = dtnow.compareTo(untildate1);
						if (result > 0) {
							sc.setcValiduntil(untildate2);
						} else
							sc.setcValiduntil(untildate1);
						// System.out.println("until date .... "+untildate+"--"+dtClose);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				sc.setcAtsstatus(schedule.C_ENTRY);
				sc.setcOrderid(schedule.getcOrderid());
				sc.setcOrderstatus(schedule.getcOrderstatus());
				sc.setcConditiontype(schedule.getcConditiontype());
				sc.setcConditionmethod(schedule.getcConditionmethod());
				sc.setcComparelastprice(schedule.getcComparelastprice());
				sc.setcRemainbidlot(schedule.getcRemainbidlot());
				sc.setcCumulativvedone(schedule.getcCumulativvedone());
				sc.setcCumulativecalc(schedule.getcCumulativecalc());
				sc.setcReversallimit(schedule.getcReversallimit());
				sc.setcBdFilter(schedule.C_REQUEST_ENTRY);// System.out.println("--"+schedule.getcBdFilter()+"--");
				sc.setcBdRefcode(schedule.getcBdRefcode());
				sc.setcBdRefopt(schedule.getcBdRefopt());
				sc.setcBdDonetype(schedule.getcBdDonetype());
				sc.setcBdDonespec(schedule.getcBdDonespec());
				//System.out.println(schedule.getcDescription());
				sc.setcDescription(schedule.getcDescription());
				sc.setcEntrybuy(engine.getConnection().getUserId().toUpperCase());
				//System.out.println("entry time "+ convertDate(schedule.getcEntrytime()));
				sc.setcEntrybuy(engine.getConnection().getUserId().toUpperCase());
				sc.setcEntrytime(schedule.getcEntrytime());
				sc.setcEntryterminal(OrderIDGenerator.getTerminal());
				sc.setcInvtype(schedule.getcInvtype());
				sc.setComplianceId(schedule.getComplianceId());
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {						
						Vector v = new Vector(1);
						v.addElement(ScheduleDef.createTableRow(sc));
						engine.getDataStore().get(TradingStore.DATA_SCHEDULELIST).addRow(v, false, false);

					}
				});
				// buat ngirim. pake message tp liat alamat na ada di quickfix..
				//System.out.println(" " + sc.getcValidity().toString());
				Message newSchedule = new Message();
				newSchedule.getHeader().setField(new BeginString(BEGIN_STRING));
				newSchedule.getHeader().setField(new MsgType("ATS"));
				// newSchedule.setField(getTime(TransactTime.FIELD,
				// schedule.getcEntrytime()));
				// newSchedule.setField(new ClOrdID(sc.getcOrderid()));
				newSchedule.setField(new OrderQty(sc.getcVolume().doubleValue()));

				newSchedule.setField(new Price(sc.getcPirce().doubleValue()));
				newSchedule.setField(new Symbol(sc.getcSecid()));
				newSchedule.setField(new Side(sc.getcBuysell().charAt(0)));
				newSchedule.setField(new ClientID(sc.getcEntrybuy()));
				newSchedule.setString(666676, sc.getcClientid());
				newSchedule.setString(666677, sc.getcEntrybuy());
				newSchedule.setString(666679, sc.getcEntryterminal());
				newSchedule.setField(getTime(6666671, sc.getcEntrytime()));


				newSchedule.setDouble(999125, sc.getcLot().doubleValue());
				newSchedule.setString(999126, sc.getcAtsstatus());
				newSchedule.setString(999100, sc.getcAtsid());
				newSchedule.setField(getTime(999101, sc.getcAtsdate()));
				newSchedule.setString(999102, sc.getcLabel());
				newSchedule.setString(999103, sc.getcValidity());
				newSchedule.setField(getTime(999104, sc.getcValiduntil()));
				newSchedule.setString(999126, sc.getcAtsstatus());
				newSchedule.setString(999105, sc.getcOrderstatus());
				newSchedule.setString(999106, sc.getcConditiontype());
				newSchedule.setString(999107, sc.getcConditionmethod());
				newSchedule.setString(999113, sc.getcBdFilter());
				// if(sc.getcConditiontype().equalsIgnoreCase("P")){
				newSchedule.setDouble(999108, sc.getcComparelastprice().doubleValue());
				newSchedule.setDouble(999109, sc.getcRemainbidlot().doubleValue());
				newSchedule.setDouble(999110, sc.getcCumulativvedone().doubleValue());
				newSchedule.setDouble(999111, sc.getcCumulativecalc()	.doubleValue());
				// } else if(sc.getcConditiontype().equalsIgnoreCase("T")){
				newSchedule.setDouble(999112, sc.getcReversallimit().doubleValue());
				// } else if(sc.getcConditiontype().equalsIgnoreCase("D")){
				newSchedule.setString(999114, sc.getcBdRefcode());
				newSchedule.setString(999115, sc.getcBdRefopt());
				newSchedule.setString(999116, sc.getcBdDonetype());
				newSchedule.setDouble(999117, sc.getcBdDonespec());
				// }

				newSchedule.setString(999118, sc.getcDescription());
				newSchedule.setString(999127, sc.getcInvtype());
				newSchedule.setString(ComplianceID.FIELD, sc.getComplianceId());
				// newSchedule.setDouble(999124, sc.getcFlag1().doubleValue());
				// /dicoba
				/*
				 * newSchedule.setString(999119,schedule.getcExecby());
				 * newSchedule.setField(getTime(999120,
				 * schedule.getcExectime()));
				 * newSchedule.setField(getTime(666671, sc.getcEntrytime() ));
				 * newSchedule.setString(666677, sc.getcEntrybuy());
				 * newSchedule.setString(666679, sc.getcEntryterminal());
				 */
				param.setField(PARAM_FIX, newSchedule);
				engine.getConnection().sendEvent(param);
			}
		});
		
		map.put(EVENT_NOTIFICATION, new EventHandler() {// Notification

					@Override
					public void execute(Event param) {
						final Notif notification = (Notif) param.getParam().get(PARAM_NOTIFICAION);

						final Notif notif = new Notif();
						notif.setcUserid(notification.getcUserid().trim());
						notif.setcNotif(notification.getcNotif().trim());
						notif.setcOnselldone(notification.getcOnselldone().trim());
						notif.setcOnbuydone(notification.getcOnbuydone().trim());
						notif.setcSendsms(notification.getcSendsms().trim());
						notif.setcMobile1(notification.getcMobile1().trim());
						notif.setcMobile2(notification.getcMobile2().trim());
						notif.setcMobile3(notification.getcMobile3().trim());
						notif.setcSendmail(notification.getcSendmail().trim());
						notif.setcEmail1(notification.getcEmail1().trim());
						notif.setcEmail2(notification.getcEmail2().trim());
						notif.setcEmail3(notification.getcEmail3().trim());

						Message messageNotification = new Message();
						String myuserid = engine.getConnection().getUserId();
						//System.out.println("UserID--" + notif.getcMobile1());
						messageNotification.getHeader().setField(new BeginString(BEGIN_STRING));
						messageNotification.getHeader().setField(new MsgType("NOTIF"));
						messageNotification.setString(999220, myuserid);
						messageNotification.setString(999221, notif.getcNotif());
						messageNotification.setString(999222,notif.getcOnselldone());
						messageNotification.setString(999223,notif.getcOnbuydone());
						messageNotification.setString(999224,notif.getcSendsms());
						messageNotification.setString(999225,notif.getcMobile1());
						messageNotification.setString(999226,notif.getcMobile2());
						messageNotification.setString(999227,notif.getcMobile3());
						messageNotification.setString(999228,notif.getcSendmail());
						messageNotification.setString(999229,notif.getcEmail1());
						messageNotification.setString(999230,notif.getcEmail2());
						messageNotification.setString(999231,notif.getcEmail3());

						param.setField(PARAM_FIX, messageNotification);
						engine.getConnection().sendEvent(param);
					}
				});
		
		//*gtc awal
		map.put(EVENT_CREATEGTC, new EventHandler() {// Notification

			@Override
			public void execute(Event param) {
				log.info("execute EVENT_CREATEGTC"
						+ param.getParam().get(PARAM_GTC));
				final Order GTC = (Order) param.getParam().get(
						PARAM_GTC);
				Order GTC2 = (Order) param.getParam().get(PARAM_GTC);
				GTC2.setStatus(Order.C_OPEN);
				/*
				 * GTC.setStatus(GTC.getStatus().equals(Order.C_ENTRY) ?
				 * Order.C_SENDING_ENTRY : Order.C_SENDING_TEMPORARY);
				 */
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Vector v = new Vector(1);
						v.addElement(GTCDef.createTableRow(GTC));
						engine.getDataStore()
								.get(TradingStore.DATA_GTC)
								.addRow(v, false, false);

					}
				});
				GTC.setEntryBy(engine.getConnection().getUserId()
						.toUpperCase());
				GTC.setEntryTerminal(OrderIDGenerator.getTerminal());
				Message messageGTC = new Message();
				String myuserid = engine.getConnection().getUserId();
				messageGTC.getHeader().setField(
						new BeginString(BEGIN_STRING));
				messageGTC.getHeader().setField(new MsgType("GTC"));
				Message newOrder = new Message();
				messageGTC.setField(getTime(TransactTime.FIELD,
						GTC.getEntryTime()));
				messageGTC.setField(new ClOrdID(GTC.getId()));
				messageGTC.setField(new ClientID(GTC.getEntryBy()));
//				messageGTC.setField(new quickfix.field.Account(GTC
//						.getInvType()));
//				messageGTC.setField(new HandlInst(GTC.getHandInst()));
				messageGTC.setField(new Symbol(GTC.getStock()));
				messageGTC.setField(new SymbolSfx(GTC.getBoard()));
				messageGTC.setField(new Side(GTC.getBOS().charAt(0)));
				messageGTC.setField(getTime(TransactTime.FIELD,
						new Date()));
				messageGTC.setField(getTime(666671, GTC.getEntryTime()));
				messageGTC.setField(new SymbolSfx(GTC.getBoard()));
				messageGTC.setField(getTime(999104,
						GTC.getcValiduntil()));
				messageGTC.setField(new OrderQty(GTC.getVolume()
						.doubleValue()));
				// messageGTC.setField(new
				// (GTC.getOrderAmount().doubleValue()));
//				messageGTC.setField(new OrdType(GTC.getOrdType()
//						.charAt(0)));
				messageGTC.setField(new Price(GTC.getPrice()
						.doubleValue()));
//				messageGTC.setField(new TimeInForce(GTC.getOrderType()
//						.charAt(0)));
//				messageGTC.setString(376, GTC.getComplianceId());
				messageGTC.setField(getTime(666671, new Date()));
//				messageGTC.setDouble(666672, GTC.getCounter()
//						.doubleValue());
//				messageGTC.setDouble(666673, GTC.getSplitTo()
//						.doubleValue());
//				messageGTC.setString(666675, GTC.getExchange());
				messageGTC.setString(666676, GTC.getAccId());
				messageGTC.setString(666677, GTC.getEntryBy());
//				messageGTC.setString(666678, GTC.getMarketref());
				messageGTC.setString(666679, GTC.getEntryTerminal());
//				messageGTC.setString(8007, GTC.getIsBasketOrder());
//				messageGTC.setString(7043, GTC.getBroker());
//				messageGTC.setString(7044, GTC.getCurrency());
//				if (GTC.getBasketTime() != null)
//					messageGTC.setField(getTime(8008,
//							GTC.getBasketTime()));
//				if (GTC.getHandInst() == '3') {
//					messageGTC.setField(new IOIid(GTC.getNegDealRef()));
//					messageGTC.setField(new ContraBroker(GTC
//							.getContraBroker()));
//					messageGTC.setField(new ContraTrader(GTC
//							.getContraUser()));
//					messageGTC.setString(7045, GTC.getOrderIDContra());
//					messageGTC.setString(7046, GTC.getAccIDContra());
//					messageGTC.setString(7047, GTC.getInvTypeContra());
//					messageGTC.setString(7048, GTC.getContraBroker());
//					messageGTC.setString(7049, GTC.getContraUser());
//					messageGTC.setString(7050, GTC.getNegDealRef());
//					messageGTC.setString(7051, GTC.getContraBroker());
//					messageGTC.setString(7052, GTC.getComplianceId());
//				}
				messageGTC.setString(440, " ");
				param.setField(PARAM_FIX, messageGTC);

				param.setField(PARAM_GTC, messageGTC);
				engine.getConnection().sendEvent(param);
				Event e = new Event(
						TradingEventDispatcher.EVENT_CREATEGTC)
						.setField(TradingEventDispatcher.PARAM_GTC, GTC)
						.setField(
								TradingEventDispatcher.PARAM_OLDSTATUS,
								GTC.getStatus())
						.setField(PARAM_FIX, newOrder);
			}
		});

map.put(EVENT_WITHDRAWGTC, new EventHandler() {// Notification
			@Override
			public void execute(Event param) {
				log.info("execute EVENT_WITHDRAWGTC " + param.getName());
				// Vector vorder = (Vector)
				// param.getParam().get(PARAM_GTC);
				{
					// log.info("******"+vorder);
					Order GTC = (Order) param.getParam().get(PARAM_GTC);
					log.info("****" + PARAM_GTC);
					GTC.setStatus(Order.C_WITHDRAW);
					GTC.setWithdrawTime(OrderIDGenerator.getTime());
					GTC.setWithdrawBy(engine.getConnection()
							.getUserId().toUpperCase());
					GTC.setWithdrawTerminal(OrderIDGenerator
							.getTerminal());
					GTC.setUpdateBy(engine.getConnection().getUserId()
							.toUpperCase());
					GTC.setUpdateDate(OrderIDGenerator.getTime());
					GTC.setTerminalId(OrderIDGenerator.getTerminal());
					int idx = engine
							.getDataStore()
							.get(TradingStore.DATA_GTC)
							.getIndexByKey(new String[] { GTC.getId() });
					log.info("GTC_INDEX_" + idx);
					engine.getDataStore().get(TradingStore.DATA_GTC)
							.refresh(idx);
					
	 Message GTC1 = new Message();
		GTC1.getHeader().setField( new BeginString(BEGIN_STRING));
		GTC1.getHeader().setField(new MsgType("WG"));
		GTC1.setField(new OrigClOrdID(GTC.getId()));
		GTC1.setField(new ClOrdID(GTC.getId()));
		GTC1.setField(new ClientID(GTC.getWithdrawBy()));
		GTC1.setField(new OrderID(GTC.getMarketOrderId()));
		GTC1.setField(new quickfix.field.Account(GTC.getInvType())); GTC1.setField(new Symbol(GTC.getStock())); GTC1.setField(new
		SymbolSfx(GTC.getBoard())); GTC1.setField(new Side(GTC.getBOS().charAt(0))); GTC1.setField(new
		TransactTime(GTC .getWithdrawTime()));
		GTC1.setField(new OrderQty(GTC.getVolume().doubleValue())); GTC1.setField(getTime(666671,
		GTC.getWithdrawTime())); 
		// GTC.setString(666674, order.getIsFloor()); 
		GTC1.setString(666677,GTC.getWithdrawBy()); 
		GTC1.setString(666679,GTC.getWithdrawTerminal()); 
		// GTC.setField(new HandlInst(order.getHandInst())); //
		GTC1.setString(8007, GTC.getIsBasketOrder());
			 if
			(GTC.getHandInst() == '3') { GTC1.setField(new
				OrderQty(2)); } else { GTC1.setField(new
				OrderQty(1)); } param.setField(PARAM_FIX, GTC1);
				engine.getConnection()
				 .addFilterByOrder(GTC.getId());
				engine.getConnection().sendEvent(param); Event e= new Event
				(TradingEventDispatcher.EVENT_REQUESTWITHDRAWGTC_OK).setField(TradingEventDispatcher.PARAM_GTC,GTC1)
				.setField(TradingEventDispatcher.PARAM_OLDSTATUS,GTC.getStatus()) .setField(PARAM_FIX, GTC1);

				}
			}
		});

map.put(EVENT_REQUESTWITHDRAW_GTC, new EventHandler() {

	@Override
	public void execute(Event param) {
		Vector vorder = (Vector) param.getParam().get(PARAM_GTC);
		log.info("execute EVENT_REQUESTWITHDRAW_GTC");
		for (int i = 0; i < vorder.size(); i++) {
			final Order order = (Order) vorder.elementAt(i);
			order.setStatus(Order.C_SENDING_WITHDRAW);
			order.setWithdrawTime(OrderIDGenerator.getTime());
			order.setWithdrawBy(engine.getConnection().getUserId()
					.toUpperCase());
			order.setWithdrawTerminal(OrderIDGenerator.getTerminal());
			order.setUpdateBy(engine.getConnection().getUserId()
					.toUpperCase());
			order.setUpdateDate(OrderIDGenerator.getTime());
			order.setTerminalId(OrderIDGenerator.getTerminal());
			int idx = engine.getDataStore().get(TradingStore.DATA_GTC)
					.getIndexByKey(new String[] { order.getId() });
			engine.getDataStore().get(TradingStore.DATA_GTC)
					.refresh(idx);
			Message wOrder = new Message();
			wOrder.getHeader().setField(new BeginString(BEGIN_STRING));
			wOrder.getHeader().setField(new MsgType("WG"));
			wOrder.setField(new OrigClOrdID(order.getId()));
			wOrder.setField(new OrderID(order.getMarketOrderId()));
			wOrder.setField(new ClOrdID(order.getId()));
			wOrder.setField(new quickfix.field.Account(order
					.getInvType()));
			wOrder.setField(new ClientID(order.getWithdrawBy()));
			wOrder.setField(new Symbol(order.getStock()));
			wOrder.setField(new SymbolSfx(order.getBoard()));
			wOrder.setField(new Side(order.getBOS().charAt(0)));
			wOrder.setField(new TransactTime(order.getWithdrawTime()));
			wOrder.setField(new OrderQty(order.getVolume()
					.doubleValue()));
			wOrder.setField(getTime(666671, order.getWithdrawTime()));
			wOrder.setString(666674, order.getIsFloor());
			wOrder.setString(666677, order.getWithdrawBy());
			wOrder.setString(666679, order.getWithdrawTerminal());
			wOrder.setField(new HandlInst(order.getHandInst()));
			wOrder.setString(8007, order.getIsBasketOrder());
			if (order.getHandInst() == '3') {
				wOrder.setField(new OrderQty(2));
			} else {
				wOrder.setField(new OrderQty(1));
			}
			// param.setField(PARAM_FIX, wOrder);
			/*
			 * Event e = new Event(
			 * TradingEventDispatcher.EVENT_WITHDRAWGTC)
			 * .setField(TradingEventDispatcher.PARAM_GTC,
			 * order.getStatus()).setField(PARAM_FIX, wOrder);
			 */
			engine.getConnection().addFilterByOrder(order.getId());
			engine.getConnection().sendEvent(param);
		}
	}

});

map.put(EVENT_AMMENDGTC, new EventHandler() {// Notification
			@Override
			public void execute(Event param) {
				log.info("execute EVENT_AMMENDGTC "
						+ param.getParam().get(PARAM_OLDORDER)
						+ " amend to "
						+ param.getParam().get(PARAM_NEWORDER));
				Order oldorder = (Order) param.getParam().get(
						PARAM_OLDORDER);

				oldorder.setAmendBy(engine.getConnection().getUserId()
						.toUpperCase());
				oldorder.setAmendTerminal(OrderIDGenerator
						.getTerminal());
				oldorder.setAmendTime(OrderIDGenerator.getTime());
				oldorder.setStatus(Order.C_AMEND);
				oldorder.setUpdateBy(engine.getConnection().getUserId()
						.toUpperCase());
				oldorder.setUpdateDate(OrderIDGenerator.getTime());
				oldorder.setTerminalId(OrderIDGenerator.getTerminal());
				Order neworder = (Order) param.getParam().get(
						PARAM_NEWORDER);
				neworder.setStatus(Order.C_OPEN);
				neworder.setEntryBy(oldorder.getAmendBy());
				neworder.setEntryTerminal(oldorder.getAmendTerminal());
				neworder.setEntryTime(oldorder.getAmendTime());
				oldorder.setMarketRef(neworder.getId());
				neworder.setPrevOrderId(oldorder.getId());
				neworder.setPrevMarketOrderId(oldorder
						.getMarketOrderId());
				neworder.setPrevDoneVolume(oldorder.getDoneVol());
				oldorder.setValiduntil(neworder.getcValiduntil());
				oldorder.setNextOrderId(neworder.getId());
				//neworder.setNextOrderId(oldorder.getId());
				oldorder.setNextMarketOrderId(neworder
						.getMarketOrderId());
				neworder.setUpdateBy(engine.getConnection().getUserId()
						.toUpperCase());
				neworder.setUpdateDate(OrderIDGenerator.getTime());
				neworder.setTerminalId(OrderIDGenerator.getTerminal());

				int idx = engine
						.getDataStore()
						.get(TradingStore.DATA_GTC)
						.getIndexByKey(
								new String[] { neworder.getAccId() });//perubahan 6 des 2013
				//engine.getDataStore().get(TradingStore.DATA_GTC)
				//		.refresh(idx);
				Vector v = new Vector(1);
				v.addElement(GTCDef.createTableRow(neworder));
				engine.getDataStore().get(TradingStore.DATA_GTC)
						.addRow(v, false, false);
				Message aOrder = new Message();
				aOrder.getHeader().setField(
						new BeginString(BEGIN_STRING));
				aOrder.getHeader().setField(new MsgType("AG"));
				aOrder.setField(new OrderID(oldorder.getMarketOrderId()));
				aOrder.setField(new ClientID(oldorder.getAmendBy()));
				aOrder.setField(new OrigClOrdID(oldorder.getId()));
				//aOrder.setField(new NextOrderId(neworder.getId()));
				aOrder.setField(new ClOrdID(neworder.getId()));
				aOrder.setField(new quickfix.field.Account(neworder
						.getInvType()));
				aOrder.setField(new HandlInst(neworder.getHandInst()));
				aOrder.setField(new Symbol(neworder.getStock()));
				aOrder.setField(new Side(neworder.getBOS().charAt(0)));
				aOrder.setField(getTime(TransactTime.FIELD,
						oldorder.getAmendTime()));
				aOrder.setField(new OrderQty(neworder.getVolume()
						.doubleValue()));
				aOrder.setField(new Price(neworder.getPrice()
						.doubleValue()));
				aOrder.setField(new TimeInForce(neworder.getOrderType()
						.charAt(0)));
				aOrder.setField(getTime(999104,neworder.getcValiduntil()));
				aOrder.setString(376, neworder.getComplianceId());
				aOrder.setField(getTime(666671, oldorder.getAmendTime()));
				aOrder.setString(666674, oldorder.getIsFloor());
				aOrder.setString(666677, oldorder.getAmendBy());
				aOrder.setString(666678, neworder.getMarketref());
				aOrder.setString(666679, oldorder.getAmendTerminal());
				aOrder.setField(new HandlInst(neworder.getHandInst()));
				aOrder.setField(new SymbolSfx(neworder.getBoard()));
				aOrder.setString(666676, neworder.getAccId());
				if (neworder.getHandInst() == '3') {
					aOrder.setString(7048, neworder.getContraBroker());
					aOrder.setString(7049, neworder.getContraUser());
				}
				param.setField(PARAM_FIX, aOrder);

				Event e = new Event(
						TradingEventDispatcher.EVENT_AMMENDGTC)
						.setField(TradingEventDispatcher.PARAM_GTC,
								neworder.getStatus()).setField(
								PARAM_FIX, neworder);

				engine.getConnection().addFilterByOrder(
						oldorder.getId());
				engine.getConnection().addFilterByOrder(
						neworder.getId());

				engine.getConnection().sendEvent(param);
				/*
				 * try { engine.getConnection().loadGTC("", "", "",
				 * false); } catch (Exception e) { // TODO
				 * Auto-generated catch block e.printStackTrace(); }
				 */
				/*
				 * Event e = new Event(
				 * TradingEventDispatcher.EVENT_WITHDRAWGTC)
				 * .setField(TradingEventDispatcher.PARAM_GTC,
				 * oldorder.getStatus()).setField(PARAM_FIX, aOrder);
				 * engine
				 * .getConnection().addFilterByOrder(neworder.getId());
				 * engine.getConnection().sendEvent(e);
				 */
			}
		});

map.put(EVENT_REFRESHAMEND_GTC, new EventHandler() {

	@Override
	public void execute(Event param) {
		Vector vorder = (Vector) param.getParam().get(PARAM_GTC);
		log.info("execute EVENT_REFRESHAMEND_GTC");
		for (int i = 0; i < vorder.size(); i++) {
			final Order order = (Order) vorder.elementAt(i);
			order.setStatus(Order.C_OPEN);
			order.setWithdrawTime(OrderIDGenerator.getTime());
			order.setWithdrawBy(engine.getConnection().getUserId()
					.toUpperCase());
			order.setWithdrawTerminal(OrderIDGenerator.getTerminal());
			order.setUpdateBy(engine.getConnection().getUserId()
					.toUpperCase());
			order.setUpdateDate(OrderIDGenerator.getTime());
			order.setTerminalId(OrderIDGenerator.getTerminal());
			int idx = engine.getDataStore().get(TradingStore.DATA_GTC)
					.getIndexByKey(new String[] { order.getId() });
			engine.getDataStore().get(TradingStore.DATA_GTC)
					.refresh(idx);
			Message wOrder = new Message();
			wOrder.getHeader().setField(new BeginString(BEGIN_STRING));
			wOrder.getHeader().setField(new MsgType("AG"));
			wOrder.setField(new OrigClOrdID(order.getId()));
			wOrder.setField(new OrderID(order.getMarketOrderId()));
			wOrder.setField(new ClOrdID(order.getId()));
			wOrder.setField(new quickfix.field.Account(order
					.getInvType()));
			wOrder.setField(new ClientID(order.getAmendBy()));
			wOrder.setField(new Symbol(order.getStock()));
			wOrder.setField(new SymbolSfx(order.getBoard()));
			wOrder.setField(new Side(order.getBOS().charAt(0)));
			wOrder.setField(new TransactTime(order.getAmendTime()));
			wOrder.setField(new OrderQty(order.getVolume()
					.doubleValue()));
			wOrder.setField(getTime(666671, order.getAmendTime()));
			wOrder.setString(666674, order.getIsFloor());
			wOrder.setString(666677, order.getAmendBy());
			wOrder.setString(666679, order.getAmendTerminal());
			wOrder.setField(new HandlInst(order.getHandInst()));
			wOrder.setString(8007, order.getIsBasketOrder());
			if (order.getHandInst() == '3') {
				wOrder.setField(new OrderQty(2));
			} else {
				wOrder.setField(new OrderQty(1));
			}
			// param.setField(PARAM_FIX, wOrder);
			Event e = new Event(TradingEventDispatcher.EVENT_REFRESHAMEND_GTC)
					.setField(TradingEventDispatcher.PARAM_GTC,
							order.getStatus()).setField(PARAM_FIX,
							wOrder);
			engine.getConnection().addFilterByOrder(order.getId());
			engine.getConnection().sendEvent(e);
		}
	}

});
//*gtc akhir
		map.put(EVENT_CREATEORDERMATRIX, new EventHandler() {
			@Override
			public void execute(Event param) {					
				System.out.println("execute EVENT_CREATEORDERMATRIX");
				
				final OrderMatrixData[] data = (OrderMatrixData[]) param.getParam().get(PARAM_DATA);	
				final boolean isbasket = ((Boolean) param.getParam().get("ISBASKET")).booleanValue();
				
				String basketsendtime = isbasket ? (String) param.getParam().get(PARAM_BASKETSENDTIME) : "";
				
				for(int i = 0; i < data.length; i++) {
					if(!data[i].getStock().isEmpty() && !data[i].getLot().equals(new Double(0)) && !data[i].getPrice().equals(new Double(0)) && !data[i].getClientID().isEmpty()) {
						Message ordermatrix = new Message();
						
						ordermatrix.getHeader().setField(new BeginString(BEGIN_STRING));
						ordermatrix.getHeader().setField(new MsgType("OM"));		
						
						//System.err.println("id : "+ data[i].getStock());
						ordermatrix.setInt(999302, data[i].getNoUrut()); // Order Matrix ID
						ordermatrix.setString(999300, data[i].getOrderMatrixID()); // Order Matrix ID
						ordermatrix.setString(666677, data[i].getUserID()); // USerID
						ordermatrix.setString(109, data[i].getClientID()); // Client ID					
						ordermatrix.setString(999102, data[i].getLabel()); // Label						
						ordermatrix.setString(54, Integer.toString(data[i].getBos())); // Buy or Sell						
						ordermatrix.setString(55, data[i].getStock()); // Stock
						ordermatrix.setDouble(3578, data[i].getLot()); // Lot
						ordermatrix.setDouble(44, data[i].getPrice()); // Price
						ordermatrix.setDouble(489231, data[i].getAmount()); // Amount
						ordermatrix.setDouble(38, data[i].getVolume()); // Volume							
						ordermatrix.setString(8007, isbasket ? "1" : "0"); // Is Basket Order
						ordermatrix.setString(999301, isbasket ? basketsendtime : "");
						
						ordermatrix.setString(440, " ");
						Event e = new Event(TradingEventDispatcher.EVENT_CREATEORDERMATRIX).setField(PARAM_FIX, ordermatrix);
						engine.getConnection().sendEvent(e);	
					}	
				}
			}
		});
		
		map.put(EVENT_DELETEORDERMATRIX, new EventHandler() {			
			@Override
			public void execute(Event param) {					
				log.info("EXECUTE EVENT_DELETEORDERMATRIX");				
				
				Vector vOm = (Vector) param.getParam().get(PARAM_DATA);					
				
				for(int i = 0; i < vOm.size(); i++) {
					if(!vOm.get(i).equals("0")) {	
						Message msgOrderMatrix = new Message();
						
						msgOrderMatrix.getHeader().setField(new BeginString(BEGIN_STRING));
						msgOrderMatrix.getHeader().setField(new MsgType("DOM"));
						
						msgOrderMatrix.setString(999300, (String)vOm.get(i)); 
						
						msgOrderMatrix.setString(440, " ");
						Event e = new Event(TradingEventDispatcher.EVENT_DELETEORDERMATRIX).setField(PARAM_FIX, msgOrderMatrix);
						engine.getConnection().sendEvent(e);	
						
//						msgOrderMatrix.setString(440, " ");
//						param.setField(PARAM_FIX, msgOrderMatrix);
//						engine.getConnection().sendEvent(param);	
					}
				}
			}
		});
		
		map.put(EVENT_DELETEORDERMATRIXDETIL, new EventHandler() {			
			@Override
			public void execute(Event param) {
				
				log.info("execute EVENT_DELETEORDERMATRIXDETIL");
				
				Vector vOm = (Vector) param.getParam().get(PARAM_DATA);
									
				if(!vOm.get(0).toString().isEmpty() || vOm.get(1) != null) {
					char ch = '2';
					Message msgOrderMatrix = new Message();
					
					msgOrderMatrix.getHeader().setField(new BeginString(BEGIN_STRING));
					msgOrderMatrix.getHeader().setField(new MsgType("DOM"));
					msgOrderMatrix.setString(999102, (String)vOm.get(9));
					msgOrderMatrix.setString(UserId.FIELD, (String)vOm.get(4));
					msgOrderMatrix.setString(999310, "2");
					msgOrderMatrix.setString(999300, vOm.get(7).toString().replace(".0", ""));
					msgOrderMatrix.setField(new HandlInst(ch));
					msgOrderMatrix.setString(440, " ");
					param.setField(PARAM_FIX, msgOrderMatrix);
					engine.getConnection().sendEvent(param);					
				}
			}
		});
	
		map.put(EVENT_DELETEORDERMATRIXBYLABEL, new EventHandler() {
			@Override
			public void execute(Event param) {
				log.info("Execute EVENT_DELETEORDERMATRIXBYLABEL");
				
				String vorder = (String) param.getParam().get(PARAM_ORDERMATRIXLABEL);
				
				Message msgOrderMatrix = new Message();
				
				msgOrderMatrix.getHeader().setField(new BeginString(BEGIN_STRING));
				msgOrderMatrix.getHeader().setField(new MsgType("DOMBL"));
				
				msgOrderMatrix.setString(666677, (String) param.getParam().get(PARAM_USERID)); 
				msgOrderMatrix.setString(999102, (String) param.getParam().get(PARAM_ORDERMATRIXLABEL)); 
				
				msgOrderMatrix.setString(440, " ");
				
				Event e = new Event(TradingEventDispatcher.EVENT_DELETEORDERMATRIXDETIL).setField(PARAM_FIX, msgOrderMatrix);
				engine.getConnection().sendEvent(e);					
			}
			
		});
	}

	Calendar calBefore = Calendar.getInstance();
	Calendar calAfter;
	public boolean wait2sec(){
		calBefore.setTime(new Date());
		if (calAfter == null) {
			calAfter = Calendar.getInstance();
		} 
		//System.out.println(calAfter.getTime()+" after \n"+calBefore.getTime()+" before "+OrderIDGenerator.getTime());
		if(calBefore.compareTo(calAfter)>0 ) {
			calAfter.setTime(new Date());
			calAfter.add(Calendar.SECOND, 2);	
			return true;
		}else if( calBefore.compareTo(calAfter)==0) {
			calAfter.setTime(new Date());
			calAfter.add(Calendar.SECOND, 2);	
			return true;
		}
		return false;
	}

	public EventHandler getEventHandler(String key) {
		return (EventHandler) map.get(key);
	}

	public void pushEvent(final Event evt) {
		if (evt.isImmediately()) {
			try {
				((EventHandler) map.get(evt.getName())).execute(evt);
			} catch (Exception ex) {
				/*log.error("cannot found event handller: " + evt.getName(), ex);
				log.error("error while processing data: "+ evt.getParam().toString());*/
			}
		} else {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					try {
						//log.info("push event" + evt.getName());
						((EventHandler) map.get(evt.getName())).execute(evt);
					} catch (Exception ex) {
						/*log.error("cannot found event handller: " + evt.getName(),ex);
						log.error("error while processing data: "+ evt.getParam().toString());*/
					}
				}
			});
		}
	}

	public String genConfirm(Order order, Message message, String status)
			throws FieldNotFound {
		String result = "";
		String type = "";
		// log.info("BOS : " + order.getBOS());
		if (order.getBOS().equals("M"))
			type = "BELI";
		else
			type = order.getBOS().equals("1") ? "BELI" : "SELL";


		result = result.concat("Status	       : " + status).concat("\n");
		result = result.concat("Transaksi  : " + type).concat("\n");
		result = result.concat("Saham      :  " + order.getStock()).concat("\n");
		result = result.concat("Price :  "+ formatter.format(message.getDouble(Price.FIELD)).concat("\n"));

		if(order.getBoard().equalsIgnoreCase("NG")) {
			result = result.concat("Share : "+ formatter.format(message.getDouble(OrderQty.FIELD))).concat("\n");

		} else {
			result = result.concat("Lot : "+ formatter.format(message.getDouble(OrderQty.FIELD))).concat("\n");
		}
		return result;
	}

}