package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class OrderDef {
	public static Hashtable getTableDef() {
		Hashtable htable = new Hashtable();
		htable.put("header", dataHeader.clone());
		htable.put("alignment", defaultHeaderAlignment.clone());
		htable.put("width", defaultColumnWidth.clone());
		htable.put("order", defaultColumnOrder.clone());
		htable.put("sorted", columnsort.clone());
		htable.put("sortcolumn", new Integer(33));
		htable.put("sortascending", new Boolean(true));
		htable.put("hide", columnhide.clone());
		return htable;
	}
	
	public static Hashtable getTableOrderReportDef() {
		Hashtable htable = new Hashtable();
		htable.put("header", dataHeader.clone());
		htable.put("alignment", defaultHeaderAlignment.clone());
		htable.put("width", defaultColumnWidthReport.clone());
		htable.put("order", defaultColumnOrder2.clone());
		htable.put("sorted", columnsort.clone());
		htable.put("sortcolumn", new Integer(33));
		htable.put("sortascending", new Boolean(true));
		htable.put("hide", columnhide.clone());
		return htable;
	}

	public static Hashtable getTableTradeDef() {
		Hashtable htable = new Hashtable();
		htable.put("header", dataHeader.clone());
		htable.put("alignment", defaultHeaderAlignment.clone());
		htable.put("width", defaultColumnWidth2.clone());
		htable.put("order", defaultColumnOrder2.clone());
		htable.put("sorted", columnsort.clone());
		htable.put("sortcolumn", new Integer(0));
		htable.put("sortascending", new Boolean(true));
		htable.put("hide", columnhide2.clone());
		return htable;
	}

	public static Hashtable getTableSplitDef() {
		Hashtable htable = new Hashtable();
		htable.put("header", dataHeader.clone());
		htable.put("alignment", defaultHeaderAlignment.clone());
		htable.put("width", defaultColumnWidth3.clone());
		htable.put("order", defaultColumnOrder3.clone());
		htable.put("sorted", columnsort.clone());
		htable.put("sortcolumn", new Integer(Order.C_TRADINGID));
		htable.put("sortascending", new Boolean(true));
		htable.put("hide", columnhide3.clone());
		return htable;
	}

	public static int[] defaultColumnWidth3 = new int[] { 100, 100, 100, 100,
			00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
			00, 00, 00, 00,
			00,
			// hidden fields
			00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
			00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
			00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 // ,00
	};

	public static int[] defaultColumnOrder3 = new int[] { 10, 15, 16, 0, 1, 2,
			3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 17, 18, 19, 20, 21, 22, 23,
			24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
			41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
			58, 59, 60, 61, 62, 63, 64, 64, 66, 67, 68, 69, 70, 71, 72, 73 // ,74
	};

	public static int[] columnhide3 = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
			11, 12, 13, 14, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
			30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46,
			47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62/*
																		 * ,63,64
																		 */,
			64, 66, 67, 68, 69, 70, 71, 72, 73 // ,74
	};

	/*
	 * public static int[] columnhide = new int[]{ Order.C_MYACC,
	 * Order.C_BALANCEVOLUME, Order.C_BALANCELOT, Order.C_DONELOT,
	 * Order.C_TRADENO, Order.C_MARKETREF, Order.C_ACCID , Order.C_ACCIDCONTRA
	 * };
	 */
	public static int[] columnhide = new int[] { Order.C_MYACC, /*
																 * Order.C_DONELOT
																 * ,
																 */
	Order.C_TRADENO, Order.C_MARKETREF, Order.C_ACCID, Order.C_ACCIDCONTRA };

	public static int[] columnhide2 = new int[] { Order.C_ORDERDATE,
			Order.C_ORDTYPE, Order.C_ORDERTYPE, Order.C_ISADVERTISING,
			Order.C_CURID, Order.C_ISBASKETORDER, Order.C_BASKETSENDTIME,
			Order.C_TLBEFORE, Order.C_AVPORTBEFORE, Order.C_RATIOBEFORE,
			Order.C_ISOVERLIMIT, Order.C_ISSHORTSELL, Order.C_DONEVOLUME,
			Order.C_PREVDONEVOLUME, Order.C_PREVORDERID, Order.C_NEXTORDERID,
			Order.C_PREVMARKETORDERID, Order.C_NEXTMARKETORDERID,
			Order.C_ENTRYRTTERMINAL, Order.C_REJECTTIME, Order.C_OPENTIME,
			Order.C_OPENSENDTIME, Order.C_AMENDTIME, Order.C_AMENDBY,
			Order.C_AMENDTERMINAL, Order.C_AMENDSENDTIME, Order.C_WITHDRAWTIME,
			Order.C_WITHDRAWBY, Order.C_WITHDRAWTERMINAL,
			Order.C_WITHDRAWSENDTIME, Order.C_SPLITTO, Order.C_SPLITBY,
			Order.C_SPLITTERMINAL, Order.C_MATCHCOUNTER, Order.C_LQVALUEBEFORE,
			Order.C_COUNTER, Order.C_TOTALCOUNTERSPLIT, Order.C_NOTE,
			Order.C_ISFLOOR, Order.C_MYACC,
			/* Order.C_BALANCEVOLUME ,Order.C_BALANCELOT , */Order.C_DONELOT,
			Order.C_NEGDEALREF, Order.C_ACCID, Order.C_ACCIDCONTRA };

	
	
	public static int[] defaultColumnOrder2 = new int[] { 
		Order.C_TRADINGID,
		Order.C_MATCHTIME,
		Order.C_ORDERID,
		Order.C_MARKETORDERID,
		Order.C_BRDID,
		Order.C_CONTRAUSERID,
		Order.C_BUYORSELL,
		Order.C_SECID,
		Order.C_PRICE,
		Order.C_LOT,
		Order.C_ORDERAMOUNT,
		// hide
		Order.C_XCHID,
		Order.C_TRADENO,		
		Order.C_MARKETREF,
		Order.C_BROKERID,		
		Order.C_ENTRYRTTIME,
		Order.C_STATUSID,
		Order.C_ENTRYRTBY,		
		Order.C_INVTYPE,		
		Order.C_CONTRABROKERID,		
		Order.C_VOLUME,
		Order.C_COMPLAINCEID,
		Order.C_UPDATEDATE,
		Order.C_USERID,
		Order.C_UPDATEBY,
		Order.C_TERMINALID,		
		Order.C_ORDERDATE, Order.C_ORDTYPE, Order.C_ORDERTYPE,
		Order.C_ISADVERTISING, Order.C_CURID, Order.C_ISBASKETORDER,
		Order.C_BASKETSENDTIME, Order.C_TLBEFORE, Order.C_AVPORTBEFORE,
		Order.C_RATIOBEFORE, Order.C_ISOVERLIMIT, Order.C_ISSHORTSELL,
		Order.C_DONEVOLUME, Order.C_PREVDONEVOLUME, Order.C_PREVORDERID,
		Order.C_NEXTORDERID, Order.C_PREVMARKETORDERID,
		Order.C_NEXTMARKETORDERID, Order.C_ENTRYRTTERMINAL,
		Order.C_REJECTTIME, Order.C_OPENTIME, Order.C_OPENSENDTIME,
		Order.C_AMENDTIME, Order.C_AMENDBY, Order.C_AMENDTERMINAL,
		Order.C_AMENDSENDTIME, Order.C_WITHDRAWTIME, Order.C_WITHDRAWBY,
		Order.C_WITHDRAWTERMINAL, Order.C_WITHDRAWSENDTIME,
		Order.C_SPLITTO, Order.C_SPLITBY, Order.C_SPLITTERMINAL,
		Order.C_MATCHCOUNTER, Order.C_LQVALUEBEFORE, Order.C_COUNTER,
		Order.C_TOTALCOUNTERSPLIT, Order.C_NOTE, Order.C_ISFLOOR,
		Order.C_MYACC, Order.C_BALANCEVOLUME, Order.C_BALANCELOT,
		Order.C_DONELOT, Order.C_NEGDEALREF, Order.C_ACCID,
		Order.C_ORDERIDCONTRA, Order.C_ACCIDCONTRA, Order.C_INVTYPECONTRA,
		Order.C_TRADINGIDCONTRA};

	/*public static int[] defaultColumnOrder2 = new int[] { Order.C_XCHID,
			Order.C_TRADENO,
			Order.C_MARKETORDERID,
			Order.C_MARKETREF,
			Order.C_BROKERID,
			Order.C_MATCHTIME,
			Order.C_ENTRYRTTIME,
			Order.C_STATUSID,
			Order.C_ENTRYRTBY,
			Order.C_TRADINGID,
			Order.C_ORDERID,
			Order.C_INVTYPE,
			Order.C_BRDID,
			Order.C_CONTRABROKERID,
			Order.C_CONTRAUSERID,
			Order.C_SECID,
			Order.C_BUYORSELL,
			Order.C_PRICE,
			Order.C_LOT,
			Order.C_VOLUME,
			Order.C_COMPLAINCEID,
			Order.C_UPDATEDATE,
			Order.C_USERID,
			Order.C_UPDATEBY,
			Order.C_TERMINALID,
			// hide
			Order.C_ORDERDATE, Order.C_ORDTYPE, Order.C_ORDERTYPE,
			Order.C_ISADVERTISING, Order.C_CURID, Order.C_ISBASKETORDER,
			Order.C_BASKETSENDTIME, Order.C_TLBEFORE, Order.C_AVPORTBEFORE,
			Order.C_RATIOBEFORE, Order.C_ISOVERLIMIT, Order.C_ISSHORTSELL,
			Order.C_DONEVOLUME, Order.C_PREVDONEVOLUME, Order.C_PREVORDERID,
			Order.C_NEXTORDERID, Order.C_PREVMARKETORDERID,
			Order.C_NEXTMARKETORDERID, Order.C_ENTRYRTTERMINAL,
			Order.C_REJECTTIME, Order.C_OPENTIME, Order.C_OPENSENDTIME,
			Order.C_AMENDTIME, Order.C_AMENDBY, Order.C_AMENDTERMINAL,
			Order.C_AMENDSENDTIME, Order.C_WITHDRAWTIME, Order.C_WITHDRAWBY,
			Order.C_WITHDRAWTERMINAL, Order.C_WITHDRAWSENDTIME,
			Order.C_SPLITTO, Order.C_SPLITBY, Order.C_SPLITTERMINAL,
			Order.C_MATCHCOUNTER, Order.C_LQVALUEBEFORE, Order.C_COUNTER,
			Order.C_TOTALCOUNTERSPLIT, Order.C_NOTE, Order.C_ISFLOOR,
			Order.C_MYACC, Order.C_BALANCEVOLUME, Order.C_BALANCELOT,
			Order.C_DONELOT, Order.C_NEGDEALREF, Order.C_ACCID,
			Order.C_ORDERIDCONTRA, Order.C_ACCIDCONTRA, Order.C_INVTYPECONTRA,
			Order.C_TRADINGIDCONTRA, Order.C_ORDERAMOUNT };*/

	public static int[] defaultColumnWidth2 = new int[] { 100, 100, 100, 100, 100,
			100, 100, 100, 100, 100, 100, 
			// hidden fields
			00, 00, 00, 00, 00, 00, 00, 00,	00, 00, 00, 00, 00, 00,	00,
			00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
			00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
			00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 };
	
	public static String[] dataHeader = new String[] { "OrderID", "OrderDate",
		"MarketID", "BoardID", "BrokerID", "BuySell", "OrdType",
		"OrderExpire", "StockID", "Advertisement", "ClientID",
		"InvestorType", "CurrencyID", "CounterPartBrokerID",
		"CounterparUserID", "Price", "Lot", "Volume", "BasketOrder",
		"BasketSendTime", "OrderStatus", "TradeLimit", "StockBalance",
		"CurrentRatio", "OverLimit", "Short", "MarketOrderID",
		"DoneVolume", "PrevDoneVolume", "PrevOrderID", "NextOrderID",
		"PrevMarketOrderID", "NextMarketOrderID", "EntryTime", "EntryBy",
		"EntryTerminal", "RejectTime", "OpenTime", "OpenSendTime",
		"AmendTime", "AmendBy", "AmendTerminal", "AmendSendTime",
		"WithdrawTime", "WithdrawBy", "WithdrawTerminal",
		"WithdrawSendTime", "SplitTo", "SplitBy", "SplitTerminal",
		"MatchCounter", "MatchTime", "Liquidity", "Counter",
		"TotalCounterSplit", "Note", "ComplainceID", "LastUpdate",
		"UserID", "SysUserID", "TerminalID", "Floor", "MyACC", "BalVolume",
		"BalLot", "DoneLot", "MarketTradeId", "MarketRef", "NegDealRef",
		"AccID", "OrderIDContra", "AccIDContra", "InvTypeContra",
		"ClientIDContra", "Amount" };
	
	
	
	public static int[] defaultColumnWidth = new int[] { 100, 100, 100, 100,
		100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
		// hide
		00,  00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,			
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00

};
	
	public static int[] defaultColumnWidthReport = new int[] { 100, 100, 100, 100, 100,
		100, 100, 100, 100, 100, 100, 
		// hidden fields
		00, 00, 00, 00, 00, 00, 00, 00,	00, 00, 00, 00, 00, 00,	00,
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
		00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00 };
	
	public static int[] defaultColumnOrder = new int[] { Order.C_ENTRYRTTIME,
		Order.C_TRADINGID, Order.C_ORDERID, Order.C_MARKETORDERID,
		Order.C_BRDID, Order.C_BUYORSELL, Order.C_SECID, Order.C_PRICE,
		Order.C_LOT, Order.C_ORDERAMOUNT, Order.C_STATUSID, Order.C_NOTE,
		Order.C_MATCHTIME, Order.C_DONELOT, Order.C_BALANCELOT,
		Order.C_AMENDTIME, Order.C_WITHDRAWTIME,Order.C_ORDTYPE, Order.C_TLBEFORE,
		Order.C_ORDERDATE, Order.C_XCHID, Order.C_BROKERID,
		 Order.C_ORDERTYPE, Order.C_ISADVERTISING,
		Order.C_INVTYPE, Order.C_CURID, Order.C_CONTRABROKERID,
		Order.C_CONTRAUSERID, Order.C_VOLUME, Order.C_ISBASKETORDER,
		Order.C_BASKETSENDTIME, Order.C_AVPORTBEFORE, Order.C_RATIOBEFORE,
		Order.C_ISOVERLIMIT, Order.C_ISSHORTSELL, Order.C_DONEVOLUME,
		Order.C_PREVDONEVOLUME, Order.C_PREVORDERID, Order.C_NEXTORDERID,
		Order.C_PREVMARKETORDERID, Order.C_NEXTMARKETORDERID,
		Order.C_ENTRYRTBY, Order.C_ENTRYRTTERMINAL, Order.C_REJECTTIME,
		Order.C_OPENTIME, Order.C_OPENSENDTIME, Order.C_AMENDBY,
		Order.C_AMENDTERMINAL, Order.C_AMENDSENDTIME, Order.C_WITHDRAWBY,
		Order.C_WITHDRAWTERMINAL, Order.C_WITHDRAWSENDTIME,
		Order.C_SPLITTO, Order.C_SPLITBY, Order.C_SPLITTERMINAL,
		Order.C_MATCHCOUNTER, Order.C_LQVALUEBEFORE, Order.C_COUNTER,
		Order.C_TOTALCOUNTERSPLIT, Order.C_COMPLAINCEID,
		Order.C_UPDATEDATE, Order.C_USERID, Order.C_UPDATEBY,
		Order.C_TERMINALID, Order.C_ISFLOOR, Order.C_MYACC,
		Order.C_BALANCEVOLUME, Order.C_TRADENO, Order.C_MARKETREF,
		Order.C_NEGDEALREF, Order.C_ACCID, Order.C_ORDERIDCONTRA,
		Order.C_ACCIDCONTRA, Order.C_INVTYPECONTRA, Order.C_TRADINGIDCONTRA };

	

	/*
	 * public static int[] defaultColumnOrder = new int[]{ Order.C_SECID,
	 * Order.C_PRICE, Order.C_LOT, Order.C_TLBEFORE, Order.C_STATUSID ,
	 * Order.C_NOTE ,Order.C_ORDERID, Order.C_ORDERDATE, Order.C_XCHID ,
	 * Order.C_BRDID , Order.C_BROKERID , Order.C_BUYORSELL , Order.C_ORDTYPE ,
	 * Order.C_ORDERTYPE , Order.C_ISADVERTISING , Order.C_TRADINGID ,
	 * Order.C_INVTYPE , Order.C_CURID , Order.C_CONTRABROKERID ,
	 * Order.C_CONTRAUSERID , Order.C_VOLUME , Order.C_ISBASKETORDER ,
	 * Order.C_BASKETSENDTIME, Order.C_AVPORTBEFORE , Order.C_RATIOBEFORE ,
	 * Order.C_ISOVERLIMIT , Order.C_ISSHORTSELL , Order.C_MARKETORDERID ,
	 * Order.C_DONEVOLUME , Order.C_PREVDONEVOLUME , Order.C_PREVORDERID ,
	 * Order.C_NEXTORDERID , Order.C_PREVMARKETORDERID ,
	 * Order.C_NEXTMARKETORDERID , Order.C_ENTRYRTTIME , Order.C_ENTRYRTBY ,
	 * Order.C_ENTRYRTTERMINAL , Order.C_REJECTTIME , Order.C_OPENTIME ,
	 * Order.C_OPENSENDTIME , Order.C_AMENDTIME , Order.C_AMENDBY ,
	 * Order.C_AMENDTERMINAL , Order.C_AMENDSENDTIME , Order.C_WITHDRAWTIME ,
	 * Order.C_WITHDRAWBY , Order.C_WITHDRAWTERMINAL , Order.C_WITHDRAWSENDTIME
	 * , Order.C_SPLITTO , Order.C_SPLITBY , Order.C_SPLITTERMINAL ,
	 * Order.C_MATCHCOUNTER , Order.C_MATCHTIME , Order.C_LQVALUEBEFORE ,
	 * Order.C_COUNTER , Order.C_TOTALCOUNTERSPLIT , Order.C_COMPLAINCEID ,
	 * Order.C_UPDATEDATE , Order.C_USERID , Order.C_UPDATEBY ,
	 * Order.C_TERMINALID , Order.C_ISFLOOR , Order.C_MYACC ,
	 * Order.C_BALANCEVOLUME , Order.C_BALANCELOT , Order.C_DONELOT ,
	 * Order.C_TRADENO , Order.C_MARKETREF , Order.C_NEGDEALREF , Order.C_ACCID
	 * , Order.C_ORDERIDCONTRA, Order.C_ACCIDCONTRA, Order.C_INVTYPECONTRA,
	 * Order.C_TRADINGIDCONTRA };
	 */

	

	public static int[] defaultHeaderAlignment = new int[] {
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER };

	public static boolean[] columnsort = new boolean[] { true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true };

	public static Vector getHeader() {
		Vector header = new Vector(Order.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}

	public static List createTableRow(Row pf) {
		List vec = new Vector(Order.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Order.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new OrderField(pf, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}

	public final static OrdRender render = new OrdRender();

	static class OrdRender extends MutableIDisplayAdapter {
		DefaultTableCellRenderer renderer = new OrderRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			renderer.getTableCellRendererComponent(table, value, isSelected,
					hasFocus, row, column);
			((OrderRender) renderer).setValue(value);
			return renderer;
		}
	}
}
