package eqtrade.trading.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import eqtrade.trading.core.Utils;


public final class Match extends Model {
    private static final long serialVersionUID = 309177931626178221L; 
    public static final int	C_ID = 0;
    public static final int	C_ORIGINALID = 1;
    public static final int	C_USER = 2;
    public static final int	C_ACCOUNT = 3;
    public static final int	C_STOCK = 4;
    public static final int	C_BOARD= 5;
    public static final int	C_STATUS = 6;
    public static final int	C_EXCHANGE = 7;
    public static final int	C_EXCHANGEORDID = 8;
    public static final int	C_PRICE = 9;
    public static final int	C_BUYORSELL = 10;
    public static final int	C_VOLUME = 11;
    public static final int	C_LOT = 12;
    public static final int	C_BALVOLUME = 13;
    public static final int	C_BALLOT = 14;
    public static final int	C_TEMPBY = 15;
    public static final int	C_TEMPDATE = 16;
    public static final int	C_ENTRYBY = 17;
    public static final int	C_ENTRYDATE = 18;
    public static final int	C_OPENDATE = 19;
    public static final int	C_WITHDRAWBY = 20;
    public static final int	C_WITHDRAWDATE = 21;
    public static final int	C_AMENDBY = 22;
    public static final int	C_AMENDDATE = 23;
    public static final int	C_REJECTDATE = 24;
    public static final int	C_DESCRIPTION = 25;
    public static final int	C_MATCHDATE = 26;
    public static final int	C_IDNEWAMEND = 27;
    public static final int	C_NEGDEALREF = 28;
    public static final int	C_TLBEFORE = 29;
    public static final int	C_VOLPFBEFORE = 30;
    public static final int	C_SERVEREXECTIME = 31;
    public static final int	C_MYACC = 32;
    public static final int	C_INVTYPE = 33;
    public static final int	C_ORDERTYPE = 34;
    public static final int	C_TRADENO = 35;
    public static final int	C_CONTRABROKER = 36;
    public static final int	C_CONTRATRADER = 37;
    public static final int    C_VALUE = 38;
    public static final int    C_DONEVOL = 39;
    public static final int    C_DONELOT = 40;
    public static final int	C_TRADINGID = 41;
 	public static final int 	CIDN_NUMBEROFFIELDS = 42;
 	
 	public static final String C_BUY = "1";
    public static final String C_SELL = "2";
    
    public static final String C_DAYORDER = "0";
    public static final String C_SESSIONORDER = "7";
    
    
    public static final String C_ENTRY_TEMPORARY = "ET";
    public static final String C_TEMPORARY = "T";
    public static final String C_ENTRY = "E";
    public static final String C_OPEN = "O";
    public static final String C_PARTIAL_MATCH = "P";
    public static final String C_FULL_MATCH = "F";
    public static final String C_DELETE_REQUEST = "RD";
    public static final String C_DELETE = "D";
    public static final String C_WITHDRAW = "W";
    public static final String C_AMEND = "A";            
    public static final String C_REQUEST_WITHDRAW = "EW";
    public static final String C_REJECTED = "R";
    public static final String C_REQUEST_AMEND = "EA";
    public static final String C_SENDING_ENTRY = "SE";
    public static final String C_SENDING_TEMPORARY = "ST";
    public static final String C_SENDING_DELETE = "SD";
    public static final String C_SENDING_WITHDRAW = "SW";
    public static final String C_SENDING_AMEND = "SA";
    public static final String C_FAILED = "F";
    
    public final static SimpleDateFormat formatdate = new SimpleDateFormat("HHmmss");


	public Match(){
		super(CIDN_NUMBEROFFIELDS);
	}
	public Match(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
		 	case 9: case 11: case 12: case 13: case 14: case 29: case 30: case 38: case 39: case 40:
			vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
		 	case 16: case 18: case 19: case 21: case 23: case 24: case 26: case 31:
		 		if (!vdata.elementAt(i).equals(""))
                    try {
                        vdata.setElementAt(formatdate.parse((String)vdata.elementAt(i)), i);
                    } catch (Exception ex){vdata.setElementAt(null,i);}
                else 
                    vdata.setElementAt(null, i);
				break;
			default:
				break;
			}
		}
	}
	
    public void updateVolume(double newVolume){
        if (getData(C_VOLUME) != null){
            double lotsize = ((Double)getData(C_VOLUME)).doubleValue() / ((Double)getData(C_LOT)).doubleValue(); 
            double diff = newVolume - ((Double)getData(C_VOLUME)).doubleValue();
            double newbalance = ((Double)getData(C_BALVOLUME)).doubleValue() + diff;
            setData(new Double(newbalance), C_BALVOLUME);
            setData(new Double(newVolume / lotsize), C_LOT);
            setData(new Double(newVolume), C_VOLUME);
            setData(new Double(newVolume * ((Double)getData(C_PRICE)).doubleValue()), C_VALUE);
            setData(new Double(newbalance / lotsize), C_BALLOT);
        } else {
            setData(new Double(newVolume), C_VOLUME);
        }
    }  
    
    public String getId(){
		return (String) getData(C_ID);
	}
	public void setId(String sinput){
		setData(sinput, C_ID);
	}
	public String getTradingId(){
		return (String) getData(C_TRADINGID);
	}
	public void setTradingId(String sinput){
		setData(sinput, C_TRADINGID);
	}
    public String getOriginalId(){
		return (String) getData(C_ORIGINALID);
	}
	public void setOriginalId(String sinput){
		setData(sinput, C_ORIGINALID);
	}
    public String getUser(){
		return (String) getData(C_USER);
	}
	public void setUser(String sinput){
		setData(sinput, C_USER);
	}
    public String getAccount(){
		return (String) getData(C_ACCOUNT);
	}
	public void setAccount(String sinput){
		setData(sinput, C_ACCOUNT);
	}
    public String getStock(){
		return (String) getData(C_STOCK);
	}
	public void setStock(String sinput){
		setData(sinput, C_STOCK);
	}
    public String getBoard(){
		return (String) getData(C_BOARD);
	}
	public void setBoard(String sinput){
		setData(sinput, C_BOARD);
	}
    public String getStatus(){
		return (String) getData(C_STATUS);
	}
	public void setStatus(String sinput){
		setData(sinput, C_STATUS);
	}
    public String getExchange(){
		return (String) getData(C_EXCHANGE);
	}
	public void setExchange(String sinput){
		setData(sinput, C_EXCHANGE);
	}
    public String getExchOrderId(){
		return (String) getData(C_EXCHANGEORDID);
	}
	public void setExchOrderId(String sinput){
		setData(sinput, C_EXCHANGEORDID);
	}
    public Double getPrice(){
		return (Double) getData(C_PRICE);
	}
	public void setPrice(Double sinput){
		setData(sinput, C_PRICE);
	}
    public String getBOS(){
		return (String) getData(C_BUYORSELL);
	}
	public void setBOS(String sinput){
		setData(sinput, C_BUYORSELL);
	}
    public Double getVolume(){
		return (Double) getData(C_VOLUME);
	}
	public void setVolume(Double sinput){
		setData(sinput, C_VOLUME);
	}
    public Double getLot(){
		return (Double) getData(C_LOT);
	}
	public void setLot(Double sinput){
		setData(sinput, C_LOT);
	}
    public Double getBalVol(){
		return (Double) getData(C_BALVOLUME);
	}
	public void setBalVol(Double sinput){
		setData(sinput, C_BALVOLUME);
	}
    public Double getBalLot(){
		return (Double) getData(C_BALLOT);
	}
	public void setBalLot(Double sinput){
		setData(sinput, C_BALLOT);
	}
    public String getTempBy(){
		return (String) getData(C_TEMPBY);
	}
	public void setTempBy(String sinput){
		setData(sinput, C_TEMPBY);
	}
    public Date getTempDate(){
		return (Date) getData(C_TEMPDATE);
	}
	public void setTempDate(Date sinput){
		setData(sinput, C_TEMPDATE);
	}
    public String getEntryBy(){
		return (String) getData(C_ENTRYBY);
	}
	public void setEntryBy(String sinput){
		setData(sinput, C_ENTRYBY);
	}
    public Date getEntryDate(){
		return (Date) getData(C_ENTRYDATE);
	}
	public void setEntryDate(Date sinput){
		setData(sinput, C_ENTRYDATE);
	}
    public Date getOpenDate(){
		return (Date) getData(C_OPENDATE);
	}
	public void setOpenDate(Date sinput){
		setData(sinput, C_OPENDATE);
	}
    public String getWithdrawBy(){
		return (String) getData(C_WITHDRAWBY);
	}
	public void setWithdrawBy(String sinput){
		setData(sinput, C_WITHDRAWBY);
	}
    public Date getWithdrawDate(){
		return (Date) getData(C_WITHDRAWDATE);
	}
	public void setWithdrawDate(Date sinput){
		setData(sinput, C_WITHDRAWDATE);
	}
    public String getAmendBy(){
		return (String) getData(C_AMENDBY);
	}
	public void setAmendBy(String sinput){
		setData(sinput, C_AMENDBY);
	}
    public Date getAmendDate(){
		return (Date) getData(C_AMENDDATE);
	}
	public void setAmendDate(Date sinput){
		setData(sinput, C_AMENDDATE);
	}
    public Date getRejectDate(){
		return (Date) getData(C_REJECTDATE);
	}
	public void setRejectDate(Date sinput){
		setData(sinput, C_REJECTDATE);
	}
    public String getDescription(){
		return (String) getData(C_DESCRIPTION);
	}
	public void setDescription(String sinput){
		setData(sinput, C_DESCRIPTION);
	}
    public Date getMatchDate(){
		return (Date) getData(C_MATCHDATE);
	}
	public void setMatchDate(Date sinput){
		setData(sinput, C_MATCHDATE);
	}
    public String getIdNewAmend(){
		return (String) getData(C_IDNEWAMEND);
	}
	public void setIdNewAmend(String sinput){
		setData(sinput, C_IDNEWAMEND);
	}
    public String getNegDealRef(){
		return (String) getData(C_NEGDEALREF);
	}
	public void setNegDealRef(String sinput){
		setData(sinput, C_NEGDEALREF);
	}
    public Double getTLBefore(){
		return (Double) getData(C_TLBEFORE);
	}
	public void setTLBefore(Double sinput){
		setData(sinput, C_TLBEFORE);
	}
    public Double getVolPFBefore(){
		return (Double) getData(C_VOLPFBEFORE);
	}
	public void setVolPFBefore(Double sinput){
		setData(sinput, C_VOLPFBEFORE);
	}	
    public String getExecTime(){
		return (String) getData(C_SERVEREXECTIME);
	}
	public void setExecTime(String sinput){
		setData(sinput, C_SERVEREXECTIME);
	}	
    public String getInvType(){
		return (String) getData(C_INVTYPE);
	}
	public void setInvType(String sinput){
		setData(sinput, C_INVTYPE);
	}	
    public String getOrderType(){
		return (String) getData(C_ORDERTYPE);
	}
	public void setOrderType(String sinput){
		setData(sinput, C_ORDERTYPE);
	}	
    public String getTradeNo(){
		return (String) getData(C_TRADENO);
	}
	public void setTradeNo(String sinput){
		setData(sinput, C_TRADENO);
	}	
    public String getContraBroker(){
		return (String) getData(C_CONTRABROKER);
	}
	public void setContraBroker(String sinput){
		setData(sinput, C_CONTRABROKER);
	}	
    public String getContraTrader(){
		return (String) getData(C_CONTRATRADER);
	}
	public void setContraTrader(String sinput){
		setData(sinput, C_CONTRATRADER);
	}	
    public Double getValue(){
		return (Double) getData(C_VALUE);
	}
	public void setValue(Double sinput){
		setData(sinput, C_VALUE);
	}	
	
    public Double getDoneLot(){
		return (Double) getData(C_DONELOT);
	}
	public void setDoneLot(Double sinput){
		setData(sinput, C_DONELOT);
	}		
    public Double getDoneVol(){
		return (Double) getData(C_DONEVOL);
	}
	public void setDoneVol(Double sinput){
		setData(sinput, C_DONEVOL);
	}		
    public boolean isMyAccount(){
        return ((Double)getData(C_MYACC)).doubleValue() == 1;
    }
    public void setMyAccount(boolean sinput){
        setData(new Double(sinput?"1":"0"), C_MYACC);
    }    
    
    @Override
	public String toString(){
    	return "";    	
    }
    
    @Override
	public void fromString(String str){
    	
    }

}