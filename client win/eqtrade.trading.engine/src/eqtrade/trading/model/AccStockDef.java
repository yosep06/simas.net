package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class AccStockDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader);
        htable.put("alignment", defaultHeaderAlignment);
        htable.put("width", defaultColumnWidth);
        htable.put("order", defaultColumnOrder);
        htable.put("sorted", columnsort);        
        htable.put("sortcolumn",new Integer(0));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}	
    public static String[] dataHeader = new String[]{
    	"Acc. Type", "Stock", "Haircut"
    };

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			100,100,100
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2
	};	

	public static boolean[] columnsort = new boolean[]{
			true, true, true
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(AccStock.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row row) {
		List vec = new Vector(AccStock.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < AccStock.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new AccStockField(row, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static SecAccRender render = new SecAccRender();
	
    static class SecAccRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new AccStockRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((AccStockRender)renderer).setValue(value);
			return renderer;
		}
	}
}
