package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class MarginDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(Margin.C_CURRENTRATIO));
        htable.put("sortascending", new Boolean(false));
        htable.put("hide", columhide.clone());
        return htable;
	}	
	
	public static int[] columhide = new int[]{
		Margin.C_MYACC
	};

	public static String[] dataHeader = new String[]{
	    	 "Acc Id","Name","Current Ratio","Credit Limit","NetAC","LQ Value","Market Value","Current TL","TopUp", "Force Sell",
	    	 "DayCount","Note","Trading Id", "Branch", "Sales", "Cust Type",    "Deposit", "Buy", "Sell",
	    	 "Bid", "Offer", "Stock Value","MyACC",
	    	 "Cust Id", "Status", "Is Corp", "Inv Type", "Phone", "Handphone", "Email", "Sub Account", 
	    	"ComplainceId", "Address", "Acc Type", "Cash Withdraw"
	    };

	    public static int[] defaultHeaderAlignment = new int[]{
			SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
	        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
	        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
	        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
	        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
	        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
	        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,  SwingConstants.CENTER
	    };

		public  static int[] defaultColumnWidth = new int[]{
				100,100,100,100,100,100,100,100,100,100,
	            100,100,100,100,100,100,100,100,100,100,
	            100,100,100,100,100,100,100,100,100,100,100,
	            100 ,100,100 ,100
		};

		public  static int[] defaultColumnOrder = new int[]{
				0,1,2,3,4,5,6,7,8,9,10,
	            11,12,13,14,15,16,17,18,19,20,
	            21,22,23,24,25,26,27,28,29,30,31,32,33,34
		};	

		public static boolean[] columnsort = new boolean[]{
				true, true, true, true, true,true, true,true,true,true,
	            true, true, true, true, true,true, true,true,true,true,
	            true, true, true, true, true,true, true,true,true,true,
	            true, true, true, true, true
		};
	
    public static Vector getHeader() {
		Vector header = new Vector(Margin.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row acc) {
		List vec = new Vector(Margin.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Margin.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new MarginField(acc, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static AccRender render = new AccRender();
	
    static class AccRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new MarginRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((MarginRender)renderer).setValue(value);
			return renderer;
		}
	}
}
