package eqtrade.trading.model;

public class Notif extends Model{

	private static final long serialVersionUID = 6184221489919298608L;
	public static final int C_USERID=0;
	public static final int C_NOTIF=1;
	public static final int C_ONSELLDONE=2;
	public static final int C_ONBUYDONE=3;
	public static final int C_SENDSMS=4;
	public static final int C_MOBILE1=5;
	public static final int C_MOBILE2=6;
	public static final int C_MOBILE3=7;
	public static final int C_SENDMAIL=8;
	public static final int C_EMAIL1=9;
	public static final int C_EMAIL2=10;
	public static final int C_EMAIL3=11;
	public static final int 	CIDN_NUMBEROFFIELDS = 12;
	
	public Notif() {
		super(CIDN_NUMBEROFFIELDS);
		// TODO Auto-generated constructor stub
	}
	public Notif(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	@Override
	protected void convertType() {
		// TODO Auto-generated method stub
		
	}

	public String getcUserid() {
		return (String)getData(C_USERID);
	}

	public void setcUserid(String cUserid) {
		setData(cUserid,C_USERID );
		System.out.println("userid---"+cUserid);
	}

	public String getcNotif() {
		return (String)getData(C_NOTIF);
	}

	public void setcNotif(String cNotif) {
		System.out.println("Notif,,,,,---"+cNotif);
		setData(cNotif,C_NOTIF);
	}

	public String getcOnselldone() {
		return (String) getData(C_ONSELLDONE);
	}

	public void setcOnselldone(String cOnselldone) {
		setData(cOnselldone,C_ONSELLDONE);
	}

	public String getcOnbuydone() {
		return (String) getData(C_ONBUYDONE);
	}

	public void setcOnbuydone(String cOnbuydone) {
		setData(cOnbuydone,C_ONBUYDONE );
	}

	public String getcSendsms() {
		return (String) getData(C_SENDSMS);
	}

	public void setcSendsms(String cSendsms) {
		setData(cSendsms,C_SENDSMS);
	}

	public String getcMobile1() {
		return (String) getData(C_MOBILE1);
	}

	public void setcMobile1(String cMobile1) {
		setData(cMobile1,C_MOBILE1);
	}

	public String getcMobile2() {
		return (String) getData(C_MOBILE2);
	}

	public void setcMobile2(String cMobile2) {
		setData(cMobile2,C_MOBILE2);
	}

	public String getcMobile3() {
		return (String) getData(C_MOBILE3);
	}

	public void setcMobile3(String cMobile3) {
		setData(cMobile3,C_MOBILE3);
	}

	public String getcSendmail() {
		return (String) getData(C_SENDMAIL);
	}

	public void setcSendmail(String cSendmail) {
		setData(cSendmail,C_SENDMAIL);
	}

	public String getcEmail1() {
		return (String) getData(C_EMAIL1);
	}

	public void setcEmail1(String cEmail1) {
		setData(cEmail1,C_EMAIL1);
	}

	public String getcEmail2() {
		return (String) getData(C_EMAIL2);
	}

	public void setcEmail2(String cEmail2) {
		setData(cEmail2,C_EMAIL2);
	}

	public String getcEmail3() {
		return (String) getData(C_EMAIL3);
	}

	public void setcEmail3(String cEmail3) {
		setData(cEmail3,C_EMAIL3);
	}
	

}
