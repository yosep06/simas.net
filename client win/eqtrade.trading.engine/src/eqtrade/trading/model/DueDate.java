package eqtrade.trading.model;

public final class DueDate extends Model {
	 /**
	 * 
	 */
	private static final long serialVersionUID = -1651311826016595130L;
	public static final int	C_TRADINGID = 0;
    public static final int C_ACCID = 1;
	//public static final int	C_BUYORSELL = 2;
	public static final int C_TSPAN = 2;
	public static final int C_BUY=3;
	public static final int C_SELL=4;
	//public static final int C_SPANDATE = 6;
	//public static final int C_VALUE = 7;
	public static final int C_NET=5;
	public static final int CIDN_NUMBEROFFIELDS = 6;
	public DueDate() {
		super(CIDN_NUMBEROFFIELDS);
		// TODO Auto-generated constructor stub
	}
	public DueDate(String smsg) {
		super(CIDN_NUMBEROFFIELDS,smsg);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void convertType() {
		// TODO Auto-generated method stub
		
	}
	    	
		public String getTradingID(){
			return (String) getData(C_TRADINGID);
		}
		public void setTradingID(String sinput){
			setData(sinput, C_TRADINGID);
		}
		public String getAccID(){
			return (String) getData(C_ACCID);
		}
		public void setAccID(String sinput){
			setData(sinput, C_ACCID);
		}	
		    
		public String getTSpan(){
			return (String) getData(C_TSPAN);
		}
		public void setTSpan(String sinput){
			setData(sinput, C_TSPAN);
		}
		public Double getBuy(){
			return (Double) getData(C_BUY);
		}
		public void setBuy(Double sinput){
			setData(sinput, C_BUY);
		}
		public Double getSell(){
			return (Double) getData(C_SELL);
		}
		public void setSell(Double sinput){
			setData(sinput, C_SELL);
		}		
	
		public Double getNet() {
			return (Double) getData(C_NET);
		}
		public void setNet(Double sinput) {
			setData(sinput, C_NET);
		}
		
	
}
