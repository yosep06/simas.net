package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.colorchooser.DefaultColorSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

import eqtrade.trading.model.DueDateDef.BSDRender;
import eqtrade.trading.model.ScheduleDef.AccRender;

public final class OrderMatrixDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(Schedule.C_ENTRYTIME));
        htable.put("sortascending", new Boolean(true));
        htable.put("hide", columhide.clone());
        return htable;
	}	
	
	public static int[] columhide = new int[]{
		OrderMatrix.C_BUYORSELL,OrderMatrix.C_SECID,OrderMatrix.C_PRICE,OrderMatrix.C_LOT,OrderMatrix.C_AMOUNT
		,OrderMatrix.C_CLIENT, OrderMatrix.C_VOLUME,OrderMatrix.C_USERID, OrderMatrix.C_ORDERMATRIXID, OrderMatrix.C_ISBASKET,
		OrderMatrix.C_BASKETTIME
	};
	
    public static String[] dataHeader = new String[]{
		"Label","Buy/Sell","Stock","Price","Lot","Amount","Client",
		"Total Buy Amount","Total Sell Amount","Last Edit","Volume","User id","Autonum","isBasket","BasketTime"
    };
        
    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER
       
    };

	public  static int[] defaultColumnWidth = new int[]{
			100,100,100,100,100,
			100,100,120,120,150,
			100,100,100,100,100

	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6,7,8,9,10,11,12,13,14
	};	

	public static boolean[] columnsort = new boolean[]{
		true, true, true, true, true, true, true, true, true ,true ,true ,true, true ,true, true
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(OrderMatrix.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row acc) {
		List vec = new Vector(OrderMatrix.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < OrderMatrix.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new OrderMatrixField(acc, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
public final static BSDRender render = new BSDRender();
	
    static class BSDRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new OrderMatrixRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((OrderMatrixRender)renderer).setValue(value);
			return renderer;
		}
	}
}
