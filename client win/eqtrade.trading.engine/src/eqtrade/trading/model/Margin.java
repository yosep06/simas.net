package eqtrade.trading.model;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.TradingStore;

public final class Margin extends Model {
   
	private static final long serialVersionUID = -2254611977196184441L;
	//private static final long serialVersionUID = -7326173869763611211L;

	public static final int  C_ACCID = 0;
	    public static final int  C_NAME = 1;
	    public static final int  C_CURRENTRATIO = 2;
	    public static final int  C_CREDITLIMIT = 3;
	    public static final int  C_NETAC = 4;
	    public static final int  C_LQVALUE = 5;
	    public static final int  C_MARKETVAL = 6;
	    public static final int  C_CURRTL = 7;
	    public static final int  C_TOPUP = 8;
	    public static final int  C_FORCESELL = 9;
	    public static final int  C_DAYCOUNT=10;
	    public static final int  C_NOTE=11;
	    public static final int  C_TRADINGID = 12;
	    public static final int  C_BRANCHID = 13;
	    public static final int  C_SALESID = 14;
	    public static final int  C_CUSTTYPE = 15;
	    public static final int  C_DEPOSIT = 16;
	    public static final int  C_BUY = 17;
	    public static final int  C_SELL = 18;   
	    public static final int  C_BID = 19;
	    public static final int  C_OFFER = 20;
	    public static final int  C_STOCKVAL = 21;
	    public static final int  C_MYACC = 22;
	    public static final int  C_CUSTID = 23;
	    public static final int  C_STATUS = 24;
	    public static final int	C_ISCORPORATE = 25;
	    public static final int  C_INVTYPE = 26;
	    public static final int  C_PHONE = 27;
	    public static final int  C_HANDPHONE = 28;
	    public static final int  C_EMAIL = 29;
	    public static final int  C_SUBACCOUNT =30;
	    public static final int	C_COMPLIANCEID = 31;
	    public static final int  C_ADDRESS = 32;    
	    public static final int  C_ACCTYPE = 33;
	    public static final int  C_WITHDRAW = 34;
	    public static final int  C_STOCK=35;
		public static final int 	CIDN_NUMBEROFFIELDS = 36;

	public Margin(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Margin(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 0:case 1:case 2:case 3:case 4:case 5: case 6: case 7: case 8: case 9:case 10: case 11: case 12: case 13: case 14: case 15: case 16: case 17: case 32:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}
	
	private void calculate(){
		if(getCurrentRatio()>64){
			double netAc = 0;System.out.println("(0.5*"+getLQValue().doubleValue()+")-"+netAc);
			if(getNetAC()<0){netAc = getNetAC().doubleValue()*-1;}
			setTopup( (0.5*getLQValue().doubleValue() )-netAc );
		}
	}
	/*
	private Date today = new Date();
	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	protected final Log logCek = LogFactory.getLog("UImarginCek");
	private HashMap<String, String> copyLog =new HashMap<String,String>();
	private HashMap<String, Integer> idRatio = new HashMap<String, Integer>();
	String accid = new String();
	private void log() {
		if(readFile("data/logs/Monitor.dat") != null//&& ((HashMap) readFile("data/logs/tglMonitor.dat")).get(getAccId()) == null
				){
			try{
				HashMap tgl = (HashMap) readFile("data/logs/tglMonitor.dat");
				System.out.println("tanggal log "+tgl.get(getAccId())+" getaccid "+getAccId());
				Object obj = readFile("data/logs/Monitor.dat");
				idRatio = (HashMap) obj ;
				if(tgl.get(getAccId()) != null ){
					if(!tgl.get(getAccId()).equals(formatter.format(today))  ){						
							if(getCurrentRatio().doubleValue() >= 64.5){
								if(idRatio.get(getAccId()) != null)
								{	idRatio.put(getAccId(), idRatio.get(getAccId())+1);}
								else{idRatio.put(getAccId(), 1);}
							}else {idRatio.put(getAccId(), 0);}
							daynot();
							tgl.put(getAccId(), formatter.format(today));System.out.println("get .. "+idRatio.get(getAccId())+" get curr ratio "+getCurrentRatio() );
							writeFile("data/logs/Monitor.dat",idRatio);
							writeFile("data/logs/tglMonitor.dat", tgl);
					}else{System.out.println(idRatio.get("YULI006Z"));
							if(getCurrentRatio().doubleValue() >= 64.5){
								if(idRatio.get(getAccId()) != null)
								{	if(idRatio.get(getAccId())<1){idRatio.put(getAccId(), idRatio.get(getAccId())+1);}
									System.out.println("idratio "+getAccId()+","+idRatio.get(getAccId())+", ratio "+getCurrentRatio());
								}
								else{idRatio.put(getAccId(), 1);}
							}else {idRatio.put(getAccId(), 0);}
							daynot();
							writeFile("data/logs/Monitor.dat",idRatio);
					}
				}else{writeAcc();}
				
			}catch (Exception e) {
				System.out.println("log error "+e);				
			}
		}else {writeLog();}
			
	}
	
	void writeLoging(){
		logCek.info(" Acc Id : "+getAccId()+" | " +
				" Name : "+getName()+" | " +
						"Current Ratio : "+getCurrentRatio().doubleValue()+" | " +
								"Day Count : "+idRatio.get(getAccId())+" | " +
										"Note : "+ getNote()+"\r\n");
	}
	private void daynot(){
		int dc = idRatio.get(getAccId());
		if(dc>2){setNote("Force Sell");}
		else if(dc>0 && dc<3 )
			if(getCurrentRatio()>=64.5 && getCurrentRatio()<=75)
			{setNote("Margin Call");}
			else {setNote("Force Sell");}
		else {setNote("-");}
		writeLoging();System.out.println("hahhaha");
		setDayCount(idRatio.get(getAccId()) );
	}
	private void writeAcc(){
		copyLog = (HashMap) readFile("data/logs/tglMonitor.dat");
		System.out.println(getAccId());
			String date_4 = new String();	
			Calendar cal = Calendar.getInstance();
			cal.setTime(today);	cal.add(Calendar.DAY_OF_MONTH, -1);	date_4 = formatter.format(cal.getTime());
			System.out.println(date_4);
			copyLog.put(getAccId(), date_4);
			writeFile("data/logs/Monitor.dat",idRatio);
			writeFile("data/logs/tglMonitor.dat", copyLog);
			System.out.println("tgl write -- "+copyLog.get(getAccId()));
	log();
	}
	private void writeLog(){
		try{		
		System.out.println(getAccId());int c =0;
		idRatio.put(getAccId(), c);
		String date_4 = new String();	
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);	cal.add(Calendar.DAY_OF_MONTH, -1);	date_4 = formatter.format(cal.getTime());
		System.out.println(date_4);
		copyLog.put(getAccId(), date_4);
		writeFile("data/logs/Monitor.dat",idRatio);
		writeFile("data/logs/tglMonitor.dat", copyLog);
		System.out.println("tgl write -- "+copyLog.get(getAccId()));
		log();
		}catch (Exception e) {
			System.out.println(e+"---"+getAccId());
		}
	}
	public static Object readFile(String pname) {
		Object objresult = null;

		try {
			File file = new File(pname);
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fis);
			objresult = in.readObject();
			in.close();
		} catch (Exception ex) {
            objresult = null;
		}

		return objresult;
	}

	public static boolean writeFile(String pname, Object objin) {
		boolean bresult = false;

		try {
			if (objin != null) {
				FileOutputStream fos = new FileOutputStream(pname);
				ObjectOutput out = new ObjectOutputStream(fos);
				out.writeObject(objin);
				out.close();
				bresult = true;
			}
		} catch (Exception ex) {
			//ex.printStackTrace();
		}

		return bresult;
	}*/
	
	public String getAccId(){
		return (String) getData(C_ACCID);
	}
	public void setAccId(String sinput){
		setData(sinput, C_ACCID);
		//log();
	}
	public String getBranchId(){
		return (String) getData(C_BRANCHID);
	}
	public void setBranchId(String sinput){
		setData(sinput, C_BRANCHID);
	}
	public String getComlianceId(){
		return (String) getData(C_COMPLIANCEID);
	}
	public void setComplianceId(String sinput){
		setData(sinput, C_COMPLIANCEID);
	}
	public String getSalesId(){
		return (String) getData(C_SALESID);
	}
	public void setSalesId(String sinput){
		setData(sinput, C_SALESID);
	}
	
    public String getAddress(){
        return (String) getData(C_ADDRESS);
    }
    public void setAddress(String sinput){
        setData(sinput, C_ADDRESS);
    }
    
    public String getAccType(){
        return (String) getData(C_ACCTYPE);
    }
    public void setAccType(String sinput){
        setData(sinput, C_ACCTYPE);
    }
    public String getCustId(){
        return (String) getData(C_CUSTID);
    }
    public void setCustId(String sinput){
        setData(sinput, C_CUSTID);
    }
    public String getIsCorporate(){
        return (String) getData(C_ISCORPORATE);
    }
    public void setIsCorporate(String sinput){
        setData(sinput, C_ISCORPORATE);
    }    
    public String getName(){
        return (String) getData(C_NAME);
    }
    public void setName(String sinput){
        setData(sinput, C_NAME);
    }
    public Double getCreditLimit(){
        return (Double) getData(C_CREDITLIMIT);
    }
    public void setCreditLimit(Double sinput){
        setData(sinput, C_CREDITLIMIT);
    }
    public Double getCurrentRatio(){
        return (Double) getData(C_CURRENTRATIO);
    }
    public void setCurrentRatio(Double sinput){calculate();
        setData(sinput, C_CURRENTRATIO);
       // log();
    }
    public Double getDayCount(){
    	return (Double) getData(C_DAYCOUNT);
    }
    public void setDayCount(Double double1){
    	setData(double1, C_DAYCOUNT);
    }
    public String getNote(){
    	return (String) getData(C_NOTE);
    }
    public void setNote(String sinput){
    	setData(sinput, C_NOTE);
    }
    public Double getLQValue(){
        return (Double) getData(C_LQVALUE);
    }
    public void setLQValue(Double sinput){
        setData(sinput, C_LQVALUE);
    }
    public Double getDeposit(){
        return (Double) getData(C_DEPOSIT);
    }
    public void setDeposit(Double sinput){
        setData(sinput, C_DEPOSIT);
    }
    public Double getBuy(){
        return (Double) getData(C_BUY);
    }
    public void setBuy(Double sinput){
        setData(sinput, C_BUY);
    }
    public Double getSell(){
        return (Double) getData(C_SELL);
    }
    public void setSell(Double sinput){
        setData(sinput, C_SELL);
    }
    public Double getNetAC(){
        return (Double) getData(C_NETAC);
    }
    public void setNetAC(Double sinput){
        setData(sinput, C_NETAC);
    }
    public Double getBid(){
        return (Double) getData(C_BID);
    }
    public void setBid(Double sinput){
        setData(sinput, C_BID);
    }
    public Double getOffer(){
        return (Double) getData(C_OFFER);
    }
    public void setOffer(Double sinput){
        setData(sinput, C_OFFER);
    }
    public Double getStockVal(){
        return (Double) getData(C_STOCKVAL);
    }
    public void setStockVal(Double sinput){
        setData(sinput, C_STOCKVAL);
    }
    public Double getMarketVal(){
        return (Double) getData(C_MARKETVAL);
    }
    public void setMarketVal(Double sinput){
        setData(sinput, C_MARKETVAL);
    }
    public Double getCurrTL(){
        return (Double) getData(C_CURRTL);
    }
    public void setCurrTL(Double sinput){
        setData(sinput, C_CURRTL);
    }
    public String getStatus(){
        return (String) getData(C_STATUS);
    }
    public void setStatus(String sinput){
        setData(sinput, C_STATUS);
    }
    public String getInvType(){
        return (String) getData(C_INVTYPE);
    }
    public void setInvType(String sinput){
        setData(sinput, C_INVTYPE);
    }
    public String getCustType(){
        return (String) getData(C_CUSTTYPE);
    }
    public void setCustType(String sinput){
        setData(sinput, C_CUSTTYPE);
    }
    public String getTradingId(){
        return (String) getData(C_TRADINGID);
    }
    public void setTradingId(String  sinput){
        setData(sinput, C_TRADINGID);
    }
    public String getSubAccount(){
        return (String) getData(C_SUBACCOUNT);
    }
    public void setSubAccount(String sinput){
        setData(sinput, C_SUBACCOUNT);
    }
    public String getPhone(){
        return (String) getData(C_PHONE);
    }
    public void setPhone(String sinput){
        setData(sinput, C_PHONE);
    }    
    public String getEmail(){
        return (String) getData(C_EMAIL);
    }
    public void setEmail(String sinput){
        setData(sinput, C_EMAIL);
    }    
    public String getHandphone(){
        return (String) getData(C_HANDPHONE);
    }
    public void setHandphone(String sinput){
        setData(sinput, C_HANDPHONE);
    }    
    public Double getTopup(){
        return (Double) getData(C_TOPUP);
    }
    public void setTopup(Double sinput){
        setData(sinput, C_TOPUP);
    }
    public Double getForceSell(){
        return (Double) getData(C_FORCESELL);
    }
    public void setForceSell(Double sinput){
        setData(sinput, C_FORCESELL);
    }
    public boolean isMyAccount(){
        return ((Double)getData(C_MYACC)).doubleValue() == 1;
    }
    public void setMyAccount(boolean sinput){
        setData(new Double(sinput?"1":"0"), C_MYACC);
    }    
    public Double getWithdraw(){
        return (Double) getData(C_WITHDRAW);
    }
    public void setWithdraw(Double sinput){
        setData(sinput, C_WITHDRAW);
    }
}