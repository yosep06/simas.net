package eqtrade.trading.model;

public final class Announcement extends Model {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int C_ID =0;
	public static final int C_Date=1;
	public static final int C_TIME=2;
	public static final int C_TITLE=3;
	public static final int C_CONTENT=4;
	public static final int C_ENTRYTIME=5;
	public static final int C_ENTRYBY=6;
	public static final int C_STATUS=7;
	public static final int C_STATUSTIME=8;
	public static final int C_STATUSBY=9;
	public static final int CIDN_NUMBEROFFIELDS=10;
		
	public Announcement() {
		super(CIDN_NUMBEROFFIELDS);
	}
	public Announcement(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		
	}
	public String getcId() {
		return (String) getData(C_ID);
	}
	public  void setcId(String cId) {
		setData(cId,C_ID );
	}
	public Double getcDate() {
		return (Double) getData(C_Date);
	}
	public void setcDate(String cDate) {
		setData(cDate, C_Date);
	}
	public String getcTime() {
		return (String) getData(C_TIME);
	}
	public void setcTime(String cTime) {
		setData(cTime,C_TIME);
	}
	public String getcTitle() {
		return (String) getData(C_TITLE);
	}
	public void setcTitle(String cTitle) {
	setData(cTitle,C_TITLE);
	}
	public String getcContent() {
		return (String) getData(C_CONTENT);
	}
	public void setcContent(String cContent) {
		setData(cContent,C_CONTENT);
	}
	public String getcEntrytime() {
		return (String) getData(C_ENTRYTIME);
	}
	public  void setcEntrytime(String cEntrytime) {
		setData(cEntrytime,C_ENTRYTIME);
	}
	public String getcEntryby() {
		return (String) getData(C_ENTRYBY);
	}
	public  void setcEntryby(String cEntryby) {
		setData(cEntryby,C_ENTRYBY);
	}
	public String getcStatus() {
		return (String) getData (C_STATUS);
	}
	public  void setcStatus(String cStatus) {
		setData(cStatus,C_STATUS);
	}
	public String getcStatustime() {
		return (String) getData (C_STATUSTIME);
	}
	public  void setcStatustime(String cStatustime) {
		setData(cStatustime,C_STATUSTIME);
	}
	public String getcStatusby() {
		return (String) getData (C_STATUSBY);
	}
	public  void setcStatusby(String cStatusby) {
		setData(cStatusby,C_STATUSBY);
	}
	

}
