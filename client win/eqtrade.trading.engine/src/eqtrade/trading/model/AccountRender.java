package eqtrade.trading.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.trading.core.SelectedBorder;
import eqtrade.trading.core.TradingSetting;

public class AccountRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     private String strFieldName = new String("");
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
    private static Color newBack;
    private static Color newFore;
    private static SelectedBorder border = new SelectedBorder();
     public static HashMap hashStatus = new HashMap();
     public static HashMap hashAccType = new HashMap();
     public static HashMap hashInvType = new HashMap();
     public static HashMap hashCustType = new HashMap();

	 @Override
	public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        strFieldName = table.getColumnName(column);
		newBack =  (column % 2) == 0 ? TradingSetting.getColor(TradingSetting.C_BACKGROUND) : TradingSetting.getColor(TradingSetting.C_BACKGROUNDEVEN);
        newFore = TradingSetting.getColor(TradingSetting.C_FOREGROUND);   
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);  
	 	return component;
    }
	
	 @Override
	public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 String strTemp;
                 setHorizontalAlignment((dat instanceof Double)? SwingConstants.RIGHT : SwingConstants.LEFT);
	             if (dat instanceof Double) {
	            		 if (strFieldName.equals(AccountDef.dataHeader[Account.C_MYACC])){
	            			 setText(((Double)dat).doubleValue()==1? "Yes":"No");
	            		 } else {
	            			 setText(formatter.format(dat));
	            		 }
                 } else if (strFieldName.equals(AccountDef.dataHeader[Account.C_ACCTYPE])){
                     strTemp = (String)hashAccType.get(dat);
                     if (strTemp == null) strTemp = " "+dat+"-Unknown";
                     setText(strTemp);
                 } else if (strFieldName.equals(AccountDef.dataHeader[Account.C_CUSTTYPE])){
                     strTemp = (String)hashCustType.get(dat);
                     if (strTemp == null) strTemp = " "+dat+"-Unknown";
                     setText(strTemp);
                 } else if (strFieldName.equals(AccountDef.dataHeader[Account.C_INVTYPE])){
                     strTemp = (String)hashInvType.get(dat);
                     if (strTemp == null) strTemp = " "+dat+"-Unknown";
                     setText(strTemp);
	    		} else if (strFieldName.equals(AccountDef.dataHeader[Account.C_STATUS])){
	    			strTemp = (String)hashStatus.get(dat);
	    			if (strTemp == null) strTemp = " Unknown";
	    			setText(strTemp);                     
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
