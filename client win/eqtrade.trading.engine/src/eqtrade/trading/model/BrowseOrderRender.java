package eqtrade.trading.model;

import java.awt.Color;
import java.awt.Component;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.trading.core.SelectedBorder;
import eqtrade.trading.core.TradingSetting;

public class BrowseOrderRender extends DefaultTableCellRenderer{

	private static final long serialVersionUID = 3370246029931093639L;
    private String strFieldName = new String("");
    private static Color newBack;
    private static Color newFore;
    private static SelectedBorder border = new SelectedBorder();
	
	 @Override
		public Component getTableCellRendererComponent(JTable table, Object value,
		 	boolean isSelected, boolean hasFocus, int row, int column){
		 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
	        strFieldName = table.getColumnName(column); //System.out.println("cek----------cek-------------cek");
			newBack =  (column % 2) == 0 ? TradingSetting.getColor(TradingSetting.C_BACKGROUND) : TradingSetting.getColor(TradingSetting.C_BACKGROUNDEVEN);
	        newFore = TradingSetting.getColor(TradingSetting.C_FOREGROUND);   
	        if (isSelected) {
	            ((JLabel)component).setBorder(border);
	        }
	        component.setBackground(newBack);
	        component.setForeground(newFore);  
		 	return component;
	    }
		
		 @Override
		public void setValue(Object value){
		     try {
		         if (value instanceof MutableIData) {
		             MutableIData args = (MutableIData)value;
		             Object dat = args.getData();
	                 String strTemp = null;
	                 setHorizontalAlignment((dat instanceof Double)? SwingConstants.RIGHT : SwingConstants.LEFT);
		             if (strFieldName.equals(BrowseOrderDef.dataHeader[BrowseOrder.C_BUYORSELL])){
	                     String buysell = (String)dat;
	                     if (buysell.equalsIgnoreCase("1")) strTemp = "BUY";
	                     else strTemp = "SELL";
	                     setText(strTemp);
	                 } else if (dat == null){
		                 setText("");
		             } else {
		            	 setText(" "+dat.toString());
		             }
		         }
		     } catch (Exception e){
		     }
		 }
}
