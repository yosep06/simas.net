package eqtrade.trading.model;

import eqtrade.trading.core.Utils;

public final class BuySellDetail extends Model {
    private static final long serialVersionUID = 309177931626178221L;
    public static final int	C_TRADINGID = 0;
    public static final int C_ACCID = 1;
	public static final int	C_BUYORSELL = 2;
	public static final int C_TSPAN = 3;
	public static final int C_SPANDATE = 4;
	public static final int C_VALUE = 5;
	public static final int CIDN_NUMBEROFFIELDS = 6;

	public BuySellDetail(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public BuySellDetail(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 5:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}	

    public Double  getValue(){
        return ((Double)getData(C_VALUE));
    }
    public void setValue(Double sinput){
        setData(sinput, C_VALUE);
    }    
	
	
	public String getTradingID(){
		return (String) getData(C_TRADINGID);
	}
	public void setTradingID(String sinput){
		setData(sinput, C_TRADINGID);
	}
	public String getAccID(){
		return (String) getData(C_ACCID);
	}
	public void setAccID(String sinput){
		setData(sinput, C_ACCID);
	}	
	public String getBuyOrSell(){
		return (String) getData(C_BUYORSELL);
	}
	public void setBuyOrSell(String sinput){
		setData(sinput, C_BUYORSELL);
	}    
	public String getTSpan(){
		return (String) getData(C_TSPAN);
	}
	public void setTSpan(String sinput){
		setData(sinput, C_TSPAN);
	}
	public String getSpanDate() {
		return (String) getData(C_SPANDATE);
	}
	public void setSpanDate(String sinput) {
		setData(sinput, C_SPANDATE);
	}
}