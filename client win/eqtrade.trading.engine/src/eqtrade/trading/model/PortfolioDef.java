package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class PortfolioDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(0));
        htable.put("sortascending", new Boolean(true));
        htable.put("hide", columnhidedetail.clone());
        htable.put("hideentry", columnhide2.clone());
        return htable;
	}	
	
	public static Hashtable getTableDetailDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthDet.clone());
        htable.put("order", defaultColumnOrderDet.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(Portfolio.C_STOCK));
        htable.put("sortascending", new Boolean(true));
        htable.put("hide", columnhidedetail.clone());
        htable.put("hideentry", columnhide2.clone());
        return htable;
	}	
	
	public static int[] columnhide2 = new int[]{
		Portfolio.C_ACCOUNTID, Portfolio.C_AVAILABLEVOLUME, Portfolio.C_BEGLOT, Portfolio.C_BEGVOLUME, Portfolio.C_CLOSINGPRICE,
		Portfolio.C_CURRLOT, Portfolio.C_CURRVOLUME, Portfolio.C_LASTPRICE, Portfolio.C_LOTSIZE, Portfolio.C_MYACC, Portfolio.C_PL,
		Portfolio.C_PLPERCENT, Portfolio.C_STOCKVAL, Portfolio.C_TRADINGID, Portfolio.C_VALUELASTPRICE
	};

	public static int[] columnhide3 = new int[]{
		Portfolio.C_ACCOUNTID, Portfolio.C_AVAILABLEVOLUME, Portfolio.C_BEGLOT, Portfolio.C_BEGVOLUME, Portfolio.C_CLOSINGPRICE,
		Portfolio.C_CURRLOT, Portfolio.C_CURRVOLUME, Portfolio.C_LASTPRICE, Portfolio.C_LOTSIZE, Portfolio.C_MYACC, Portfolio.C_PL,
		Portfolio.C_PLPERCENT, Portfolio.C_STOCKVAL, Portfolio.C_VALUELASTPRICE
	};

	public static int[] columnhidedetail = new int[]{
		Portfolio.C_TRADINGID,  Portfolio.C_BEGVOLUME, Portfolio.C_AVAILABLEVOLUME,  Portfolio.C_MYACC, Portfolio.C_ACCOUNTID,
		Portfolio.C_CURRVOLUME, Portfolio.C_LOTSIZE, Portfolio.C_CURRLOT//, Portfolio.C_PLPERCENT
	};
	
	public static String[] dataHeader = new String[]{
		"Client ID", "Stock ID", "Beg Vol", "Available Vol", "Stock Val", "AVG Price", "Close Price",
		"MyACC", "Account Id", "Current Volume", "Market Price", "Lot Size", "M. Val", "Balance",
		"Net Balance", "Curr Balance", "Unrealized", "Unrealized(%)"
	};

	public  static int[] defaultColumnWidthDet = new int[]{
		100,100,100,100,100,100,100,100,100,100,
        100,100,100,100,100,100,100,100
	};
	
	public  static int[] defaultColumnOrderDet = new int[]{
		0,1,13,14,2,3,4,12,5,6,7,8,9,10,11,15,16,17
	};	

public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER,SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, 
		SwingConstants.CENTER,SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER,SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			100,100,100,100,100,100,100,100,100,100,
            100,100,100,100,100,100,100,100
	};

	public  static int[] defaultColumnOrder = new int[]{
		0,1,14,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17
	};	

	public static boolean[] columnsort = new boolean[]{
			true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(Portfolio.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row pf) {
		List vec = new Vector(Portfolio.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Portfolio.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new PortfolioField(pf, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static PFRender render = new PFRender();
	
    static class PFRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new PortfolioRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((PortfolioRender)renderer).setValue(value);
			return renderer;
		}
	}
}
