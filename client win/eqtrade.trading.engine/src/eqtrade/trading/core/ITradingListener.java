package eqtrade.trading.core;

import com.vollux.idata.GridModel;


public interface ITradingListener {
	void loginOK();
	void loginFailed(String msg);
	void loginStatus(String msg);
	void changePwdOK();
	void changePwdFailed(String msg);
	void disconnected();
	void reconnectStatus(String msg);
	void reconnectFailed(String msg);
	void reconnectOK();
	void refreshOK();
	void refreshFailed(String msg);
	void refreshStatus(String msg);
	void killed(String msg);
	void checkPINOK();
	void checkPINFailed(String msg);
	void changePINOK();
	void changePINFailed(String msg);
	void checkPIN_NEGO_OK(String form);
	void checkPIN_NEGO_FAILED(String msg);
	//refresh portfolio
	void refreshOKportfolio();
	void refreshOKmargin();
	void addMarginCall(GridModel model);
}
