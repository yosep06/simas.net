package eqtrade.trading.core;

public class OrderMatrixData {
	private String _ordermatrixid;
	private String _userid;
	private int _bos; // buy or sell
	private String _stock;
	private Double _price;
	private Double _lot;
	private Double _volume;
	private Double _amount;
	private String _client_id;
	private String _client_name;
	private String _label;
	private int _nourut;
	
	public OrderMatrixData() {
		
	}
	
	public String getOrderMatrixID() {
		return this._ordermatrixid;
	}
	
	public void setOrderMatrixID(String ordermatrixid) {
		this._ordermatrixid = ordermatrixid;
	}
	
	public String getUserID() {
		return this._userid;
	}
	
	public void setUserID(String userid) {
		this._userid = userid;
	}
	
	public int getBos() {
		return this._bos;
	}
	
	public void setBos(int bos) {
		this._bos = bos;
	}
	
	public String getStock() {
		return this._stock;
	}
	
	public void setStock(String stock) {
		this._stock = stock;
	}
	
	public Double getPrice(){
		return this._price;
	}
	
	public void setPrice(Double price) {
		this._price = price;
	}
	
	public Double getLot() {
		return this._lot;
	}
	
	public void setLot(Double lot){
		this._lot = lot;
	}
	
	public Double getAmount(){
		return this._amount;
	}
	
	public void setAmount(Double amount) {
		this._amount = amount;
	}
	
	public Double getVolume(){
		return this._volume;
	}
	
	public void setVolume(Double volume){
		this._volume = volume;
	}		
		
	public String getClientID() {
		return this._client_id;
	}
	
	public void setClientID(String clientid) {
		this._client_id = clientid;
	}
	
	public String getClientName() {
		return this._client_name;
	}
	
	public void setClientName(String clientname) {
		this._client_name = clientname;
	}
	
	public String getLabel() {
		return this._label;
	}
	
	public void setLabel(String label) {
		this._label = label;
	}
	
	public int getNoUrut() {
		return this._nourut;
	}
	
	public void setNoUrut(int nourut) {
		this._nourut = nourut;
	}
}
