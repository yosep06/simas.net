package eqtrade.trading.core;

import java.rmi.RemoteException;
import java.util.Vector;

import eqtrade.trading.model.Order;//*gtc
import eqtrade.trading.model.OrderMatrix;

public interface ITradingConnection {
	String getPassword();

	String getUserId();

	String getPIN();

	String getIPServer();

	long getSessionid();

	void loadMaster(String eventStatus) throws Exception;

	void loadOrder(String eventStatus, String param1, String param2, String param3,
			boolean clear) throws Exception;

	void loadTrade(String eventStatus, String param1) throws Exception;

	void loadAccount(String eventStatus, String param1, boolean clear)
			throws Exception;

	void loadPortfolio(String eventStatus, String param1, String param2,
			boolean clear) throws Exception;

	void loadCashColl(String eventStatus, String param1, boolean clear)
			throws Exception;

	void refreshData(final boolean master, final boolean account,
			final boolean pf, final boolean order);

	void refreshData(final boolean master, final boolean account,
			final boolean margin, final boolean pf, final boolean order);
	
	void loadSchedule(String eventStatus,String param1,String param2,
			  boolean clear) throws Exception;
	
	void loadOrderMatrix(String eventStatus,String param1, boolean clear) throws Exception;
	
	void loadOrderMatrixList(String eventStatus,String param1,String param2, boolean clear) throws Exception;
	
	void loadBrowseOrder(String eventStatus, String param1, boolean clear)throws Exception;
	 
	//*gtc awal
	void loadGTC(String eventStatus, String param1, String param2,
			boolean clear) throws Exception;
	
	void refreshGTC (final String param, final String param2);
	
	boolean createGtc(Object data) throws Exception;
	
	public void gtc (Order param)throws Exception;
	//*gtc akhir
	void refreshAts(final String param);

	void refreshNotif(final String param);

	void refreshBrowseAts(final String param);

	void refreshBuySellDetail(final String param1, final String param2);

	void refreshDueDate(final String param1);

	// void refreshData(final String tradingid);

	// void refreshData(final String tradingid, final boolean isclear);

	void refreshOrder(final String param1, final String param2, final String param3);

	void refreshTrade(final String param1);

	void refreshPortfolio(final String param1, final String param2);

	void refreshAccount(final String param);

	void refreshCashColl(final String param);

	String splitOrder(String param);

	// TradingLimit getTradingLimit();

	void getServertime() throws Exception;

	void logon(final String suid, final String spwd);

	void changePassword(final String soldpwd, final String snewpwd);

	void changePIN(final String soldpin, final String snewpin);

	void checkPIN(final String puserid, final String pPIN);
	
	void checkPIN_NEGO (final String pPIN, final String form);

	void checkPINSLS(final String puserid, final String pPIN);

	boolean logout();

	public boolean isConnected();

	boolean sendEvent(Event event);

	void refreshData(final String tradingid);

	void reconnect(final int count);

	void reconnect();

	boolean entryWithdraw(Object data) throws Exception;

	boolean entryAmend(Object data) throws Exception;

	boolean createOrder(Object data) throws Exception;

	boolean createAts(Object data) throws Exception;

	boolean atsWithdraw(Object data) throws Exception;

	boolean notification(Object data) throws Exception;

	void addFilterByClient(String filter);

	void deleteFilterByClient(String filter);

	void addFilterByOrder(String filter);

	void deleteFilterByOrder(String filter);
	
	void addFilterByStock(String filter);

	void deleteFilterByStock(String filter);
	
	public void setFilter(boolean isFilter);
	
	public void saveFilterByClient(String filter);
	
	public String loadFilterByClient();
	

	boolean createOrderMatrix(Object data)throws Exception;
	
	boolean deleteOrderMatrix(Object data)throws Exception;
	
	boolean deleteOrderMatrixDetil(Object data)throws Exception;

	void refreshOrderMatrix(String param);
	
	void refreshOrderMatrixList(String param,String param2);

}
