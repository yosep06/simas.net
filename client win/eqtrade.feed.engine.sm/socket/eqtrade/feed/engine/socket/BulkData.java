package eqtrade.feed.engine.socket;

import java.util.Vector;

public class BulkData {
	private String header;
	private Vector vmsg = new Vector();

	public BulkData(String header, Vector vmsg) {
		super();
		this.header = header;
		this.vmsg = vmsg;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public Vector getVmsg() {
		return vmsg;
	}

	public void setVmsg(Vector vmsg) {
		this.vmsg = vmsg;
	}

}
