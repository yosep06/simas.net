package eqtrade.feed.engine.socket;

import java.util.HashMap;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.engine.socket.blockrequest.BaseBlock;
import eqtrade.feed.engine.socket.blockrequest.CHIComposite;
import eqtrade.feed.engine.socket.blockrequest.LoginRequest;
import eqtrade.feed.engine.socket.blockrequest.LoginRequestV;

public class BlockRequestSynchronized {
	public final static String REQUEST_TIME = new String("time");
	public final static String REQUEST_LOGIN = new String("login");
	public final static String REQUEST_LOGINV = new String("vlogin");
	public final static String REQUEST_DATE = new String("date");
	public final static String REQUEST_HISTORY = new String("history");
	public final static String REQUEST_HISTORY_FILE = new String("history.file");

	public final static String REQUEST_BROKERINFO = new String("brokerinfo");
	private HashMap<String, BaseBlock> map = new HashMap<String, BaseBlock>();
	private IFeedEngine engine;
	private Logger log = LoggerFactory.getLogger(getClass());

	public BlockRequestSynchronized(IFeedEngine engine) {
		this.engine = engine;
		init();
	}

	private void init() {
		map.put(REQUEST_TIME, new BaseBlock(REQUEST_TIME,engine));
		map.put(REQUEST_HISTORY_FILE, new BaseBlock(REQUEST_HISTORY_FILE,engine));
		map.put(REQUEST_LOGIN, new LoginRequest(REQUEST_LOGIN,engine));
		map.put(REQUEST_LOGINV, new LoginRequestV(REQUEST_LOGINV,engine));
		map.put(REQUEST_DATE, new BaseBlock(REQUEST_DATE,engine));
		map.put(REQUEST_HISTORY, new CHIComposite(REQUEST_HISTORY,engine));
		map.put(REQUEST_BROKERINFO, new BaseBlock(REQUEST_BROKERINFO, engine));
	}

	public BaseBlock get(String type) {
		return map.get(type);
	}

	public void receive(String msg) {
		String header = msg.split("\\|")[0];
		if (map.containsKey(header)) {
			msg = msg.substring(msg.indexOf("|") + 1, msg.length());
			map.get(header).addReceive(msg);
		} else {
			String[] dt = msg.split("\\|");
			if (dt.length >= 2) {
				map.get(dt[0] + "|" + dt[1]).addReceive(
						msg.replace(REQUEST_HISTORY + "|", ""));
			}
		}
	}

	public boolean isBlockingRequest(String msg) {
		try {
			String header = msg.split("\\|")[0];
			if (map.containsKey(header)) {
				return true;
			} else {
				String[] dt = msg.split("\\|");
				
				if (dt.length >= 2) {
					header = dt[0] + "|" + dt[1];
					//log.info("isblocking "+header+" "+map.containsKey(header));
					return map.containsKey(header);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}
	
	public void clearWaitingStateClient() {
		Iterator<String> iters = map.keySet().iterator();
		while (iters.hasNext()) {
			String next = iters.next();
			BaseBlock bb = map.get(next);
			bb.clear();
		}
	}
}