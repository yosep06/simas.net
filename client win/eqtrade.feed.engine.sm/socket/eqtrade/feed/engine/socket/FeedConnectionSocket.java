package eqtrade.feed.engine.socket;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.Timer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.PropertyConfigurator;
import org.jboss.netty.bootstrap.ClientBootstrap;

import com.eqtrade.ClientSocket;
import com.eqtrade.Receiver;
import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedEventDispatcher;

public class FeedConnectionSocket implements eqtrade.feed.core.IFeedConnection, Receiver {
	private Log log = LogFactory.getLog(getClass());
	private SocketInterface socketConnector;
	private Thread inQueue;
	private boolean bstop = false, blogout = false;
	private Vector vin = new Vector();
	private Long sesid = -1l;
	private long oldsesid = -1l; // yosep other device// yosep other device
	private String suid, spwd, oldsuid;
	private String uid = "", pwd, svr;
	private String ssvr;
	private Timer timerRefresh = null;
	private Thread outQueue;
	private int ninterval, totalServer;
	private volatile Vector vou = new Vector(10, 5);
	private final static String C_GETNEW = "GET";
	private final static String C_SUBSCRIBE = "SUB";
	private final static String CMD_UNSUBSCRIBE = "UNSUB";
	private final static String CMD_LOGOUT = "LOGOUT";
	private final static String CMD_LOGIN = "LOGIN";
	private eqtrade.feed.engine.FeedEngine engine;
	private HashMap hashIP = new HashMap();
	private Vector<String> vrequestcallback = new Vector<String>();
	// private Queue<String> tempreq = new ConcurrentLinkedQueue<String>();
	private BlockRequestSynchronized blockRequest;
	private ClientBootstrap bootstrap;
	private long diff = 0;
	// yosep other device
	private boolean isFeedLogin = false;
	private int countFeedLogin= 0;
	Timer timerServer =null;//timer sync yosep
//	RequestTime reqTimeServer = new RequestTime(this);//timer sync yosep
	ReqTime reqTime = null;
	
	public FeedConnectionSocket(eqtrade.feed.engine.FeedEngine engine) {
		this.engine = engine;
		init();
	}

	public void init() {
		try {
			loadSettings();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		inQueue = new Thread(createInQueue());

		outQueue = new Thread(createOutQueue());
		timerRefresh = new Timer(1 * 60 * 1000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				(new Thread(new Runnable() {
					public void run() {
						addOut(C_GETNEW);
					}
				})).start();
			}
		});
		timerRefresh.setInitialDelay(1 * 60 * 1000);
		timerRefresh.stop();
		
		if(reqTime != null) {
			reqTime.stop();
			reqTime = null;
		}
		reqTime = new ReqTime(this);
		reqTime.start();
		
		blockRequest = new BlockRequestSynchronized(engine);
	}

	private Runnable createOutQueue() {
		return new Runnable() {
			public void run() {
				Vector vtemp;
				while (!bstop) {
					try {
						if (vou.size() > 0) {
							vtemp = (Vector) vou.clone();
							vou.removeAll(vtemp);
							processOut(vtemp);
						} else {
							synchronized (vou) {
								vou.wait();
							}
						}
					} catch (Exception ex) {
					}
				}
				System.gc();
			}
		};
	}

	protected void processOut(Vector vdatamsg) {
		byte[] bpar = null;
		try {
			String scmd, stemp;
			Vector vtempdata = new Vector(10, 5);
			for (int i = 0; i < vdatamsg.size(); i++) {
				stemp = vdatamsg.elementAt(i).toString();
				if (!vtempdata.contains(stemp)) {
					vtempdata.addElement(stemp);
				}
			}

			for (int i = 0; i < vtempdata.size(); i++) {
				scmd = vtempdata.elementAt(i).toString();
				log.info("processOut:" + scmd + ":" + sesid);
				if (sesid > -1) {

					if (scmd.equals(C_GETNEW)) {
						//channel.write("heartbeat|" + sesid);
						socketConnector.sendMessage(Utils.compress(("heartbeat*" + sesid).getBytes()));
						log.info("send heartbeat : "+sesid);
					} else if (scmd.startsWith(C_SUBSCRIBE)) {
						String[] msg = scmd.split("\\*");
						// channel.write("subscribe|" + sesid + "|" + msg[1]);
						log.info("subscribe*" + sesid + "*" + msg[1]);

						socketConnector.sendMessage(Utils
								.compress(("subscribe*" + sesid + "*" + msg[1])
										.getBytes()));

					} else if (scmd.startsWith(CMD_UNSUBSCRIBE)) {
						String[] msg = scmd.split("\\*");
						// channel.write("unsubscribe|" + sesid + "|" + msg[1]);
						socketConnector
								.sendMessage(Utils.compress(("unsubscribe*"
										+ sesid + "*" + msg[1]).getBytes()));

					} else if (scmd.startsWith(CMD_LOGOUT)) {
						// channel.write("logout|" + sesid);
						socketConnector.sendMessage(Utils
								.compress(("logout*" + sesid).getBytes()));
						socketConnector.closeSocket();

					}
				}

			}
		} /*
		 * catch (RemoteException ex) { log.error(Utils.logException(ex));
		 * ex.printStackTrace(); if (!ex.getMessage().endsWith("killed")) {
		 * reconnect(); } else { if (timerRefresh.isRunning()) killed(); } }
		 */catch (Exception ex) {
			ex.printStackTrace();
			//log.error(Utils.logException(ex));
		}
	}

	private void startServices() {

		if (!this.inQueue.isAlive())
			inQueue.start();

		if (!this.outQueue.isAlive())
			outQueue.start();
		timerRefresh.start();
		log.info("service started");
	}

	private Runnable createInQueue() {
		return new Runnable() {
			public void run() {
				Vector vtemp;
				while (!bstop) {
					try {
						if (vin.size() > 0) {
							vtemp = (Vector) vin.clone();
							vin.removeAll(vtemp);
							for (int i = 0; i < vtemp.size(); i++) {
								// HashMap param = new HashMap(1);
								// param.put(FeedEventDispatcher.PARAM_MESSAGE,
								// vtemp.elementAt(i));
								// engine.getEventDispatcher().pushEvent(FeedEventDispatcher.EVENT_ONDATA,
								// param);
								// engine.getParser().parse(
								// (String) vtemp.elementAt(i));
								// log.info("get message " +
								// vtemp.elementAt(i));

								engine.getParser().parse(
										(String) vtemp.elementAt(i));
							}
						} else {
							synchronized (vin) {
								vin.wait();
							}
						}
					} catch (Exception ex) {
					}
				}
				System.gc();
			}
		};
	}

	public void loadSettings() throws Exception {

		blogout = false;
		socketConnector = SocketFactory.createSocket("data/config/socket.ini", this);

		Object o = Utils.readFile("data/config/feed.dat");
		Vector v = null;
		if (o == null) {
			v = new Vector(3);
			//v.addElement("172.21.1.101:5555");
			//v.addElement("172.21.1.101:5555");
			// v.addElement("127.0.0.1:5555/feed");
			// v.addElement("127.0.0.1:5555/feed");
			Utils.writeFile("data/config/feed.dat", v);
		} else {
			v = (Vector) o;
		}
		// ninterval = ((Integer) v.elementAt(2)).intValue();
		int x = v.size();
		//ninterval = ((Integer) v.elementAt(x - 1)).intValue();
		totalServer = x ;
		hashIP.clear();
		for (int i = 1; i <= totalServer; i++) {
			ssvr = (String) v.elementAt(i - 1);
			hashIP.put(new String(i + ""), new String(ssvr));
		}
		// try {
		// sip = InetAddress.getLocalHost().getHostAddress();
		// } catch (Exception ex){}
		log.info("available server test: " + totalServer);
		log.info("available server test: " + hashIP.toString());

	}

	Thread threadLogin = null;

	@Override
	public void logon(final String suid, final String spwd) {
		this.suid = suid;
		this.spwd = spwd;
		/*
		 * channel.write("login|" + suid + "|" + spwd); // addOut(CMD_LOGIN +
		 * "*" + "|" + suid + "|" + spwd); // channel.write("login|" + this.suid
		 * + "|" + this.spwd); // addOut(CMD_LOGIN + "*login|" + this.suid + "|"
		 * + this.spwd);
		 */
		if (threadLogin != null) {
			threadLogin.stop();
			threadLogin = null;
		}
		threadLogin = new Thread(new Runnable() {
			public void run() {
				// doLogin(suid, spwd);
				try {
					blogout = false;
					loadSettings();

					searchingServer();
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		});
		threadLogin.start();

	}

	public boolean searchingServer() {
		boolean bresult = false;
		int i = 1, curr = 1, next = 2, loginattempt = 10, logcoba = 1;

		String url;
		while (logcoba <= loginattempt) {
			for (; i <= (totalServer * 1); i++) { 
				if (i == next) {
					curr++;
					next = next + 1;
				}

				url = (String) hashIP.get(new String(curr + ""));
				log.info("login to : " + url + " user name: " + suid);
				String[] urlport = url.split("\\:");

				bresult = connect(urlport[0], new Integer(urlport[1]));

				log.info("isconnected " + bresult);

				if (bresult) {
					svr = url;
					logcoba = logcoba + loginattempt + 1;
					break;
				} else {
					log.warn("trying to reconnect (" + i + ")");
				}

			}
			logcoba = logcoba + 1;

		}

		if (!bresult && i > totalServer * 1) {
			HashMap param = new HashMap(1);
			param.put(FeedEventDispatcher.PARAM_MESSAGE,
					"Connection Failed, Please Try Again ..");
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_LOGINFAILED, param);
			socketConnector.closeSocket();
		}

		return bresult;
	}
	//yosep enforcementpass
	public void doLoginV(String suid, String spwd) {
			boolean bresult = false;
			int i = 1, curr = 1, next = 2, loginattempt = 10, logcoba = 1;

			int retrying = 3;
			String url;
//			if (isFeedLogin && uid.equalsIgnoreCase(suid)){
//				engine.getEventDispatcher().pushEvent(
//						FeedEventDispatcher.EVENT_LOGINOK, null);
//				isFeedLogin = true;//yosep other device
//				System.out.println("other device ");
//			}
//			else{
				String message = new String();	String next_action = new String();
				String sukses = new String();
				for (; i <= retrying; i++) {
					Object obj = blockRequest.get(
							BlockRequestSynchronized.REQUEST_LOGINV).sendAndReceive(
							"vlogin*" + suid + "*" + spwd);
					log.info("login receive " + obj.toString()+ "=");
		//			sesid = (Long) obj; 
					String[] reply = ((String)obj).split("\\|");
					sesid = Long.parseLong(reply[0]);
					bresult = sesid > 0;
					message = reply[2];
					next_action = reply[3];
					if (bresult) {
						try {
							String session = getDate();
							if (session != null) {
								if (next_action.equalsIgnoreCase("CHANGE_PWD")) {
									HashMap param = new HashMap(1);
									param.put(FeedEventDispatcher.PARAM_MESSAGE,
											message);
//									engine.getEventDispatcher().pushEvent(
//											FeedEventDispatcher.EVENT_CHGPWD, param);
								}
								logcoba = loginattempt + 1;
								FeedSetting.session = new String(session);
								diff = new Date().getTime() - getServerTime();
								startServices();
								uid = suid;
								pwd = spwd;
								log.info("login OK ");
								engine.getEventDispatcher().pushEvent(
										FeedEventDispatcher.EVENT_LOGINOK, null);
								isFeedLogin = true;//yosep other device
								oldsesid = sesid; // yosep other device
								oldsuid	 = suid; // yosep other device
								break;
							} else {
								log.warn("Login Failed, retrying(" + i + ")....");
								HashMap param = new HashMap(1);
								param.put(FeedEventDispatcher.PARAM_MESSAGE,
										"Register Failed, retrying(" + i + ")....");
								engine.getEventDispatcher().pushEvent(
										FeedEventDispatcher.EVENT_LOGINSTATUS, param);
								isFeedLogin = false;//yosep other device
									countFeedLogin= 0 ;
							}
						} catch (Exception exp) {
							log.warn("Failed, " + exp.getMessage());
							HashMap param = new HashMap(1);
							// param.put(FeedEventDispatcher.PARAM_MESSAGE,
							// "Failed, retrying(" + i + ")....");
							param.put(FeedEventDispatcher.PARAM_MESSAGE,
									"connecting.....");
							engine.getEventDispatcher().pushEvent(
									FeedEventDispatcher.EVENT_LOGINSTATUS, param);
							try {
								Thread.sleep(500);
							} catch (Exception d) {
							}
							;
							bresult = false;
							//log.error(Utils.logException(exp));
							exp.printStackTrace();
						}
		
					} else if (next_action.equalsIgnoreCase("RESET_PWD")) {
//						engine.getEventDispatcher().pushEvent(
//								FeedEventDispatcher.EVENT_FORGOTPWD, null);
						HashMap param = new HashMap(1);
						param.put(FeedEventDispatcher.PARAM_MESSAGE,
								message);
						engine.getEventDispatcher().pushEvent(
								FeedEventDispatcher.EVENT_LOGINFAILED, param);
						socketConnector.closeSocket();
						break;
					} else if(isFeedLogin) {//yosep other device
							log.warn("Invalid userid/password....");
							HashMap param = new HashMap(1);
							param.put(FeedEventDispatcher.PARAM_MESSAGE,
									"Invalid userid/password");
							engine.getEventDispatcher().pushEvent(
									FeedEventDispatcher.EVENT_FALSEPWD, param);
							sesid = oldsesid;
							this.suid = oldsuid;
							break;
					} else {
						log.warn("Invalid userid/password....");
						HashMap param = new HashMap(1);
						param.put(FeedEventDispatcher.PARAM_MESSAGE,
								message);
						engine.getEventDispatcher().pushEvent(
								FeedEventDispatcher.EVENT_LOGINFAILED, param);
						socketConnector.closeSocket();
						isFeedLogin = false;//yosep other device
						countFeedLogin= 0 ;
						break;
					}
		
				}
//			}
			if (i > retrying) {
				HashMap param = new HashMap(1);
				param.put(FeedEventDispatcher.PARAM_MESSAGE,
						"Connection Failed, Please Try Again ..");
				engine.getEventDispatcher().pushEvent(
						FeedEventDispatcher.EVENT_LOGINFAILED, param);
			}
			
		}
	public void doLogin(String suid, String spwd) {
		boolean bresult = false;
		int i = 1, curr = 1, next = 2, loginattempt = 10, logcoba = 1;

		int retrying = 3;
		String url;
		if (isFeedLogin && uid.equalsIgnoreCase(suid)){
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_LOGINOK, null);
			isFeedLogin = true;//yosep other device
		}
		else{
			for (; i <= retrying; i++) {
				Object obj = blockRequest.get(
						BlockRequestSynchronized.REQUEST_LOGIN).sendAndReceive(
						"login*" + suid + "*" + spwd);
				log.info("login receive " + obj.toString()+ "=");
				sesid = (Long) obj;
				bresult = sesid > 0;
				if (bresult) {
					try {
						String session = getDate();
						if (session != null) {
							logcoba = loginattempt + 1;
							FeedSetting.session = new String(session);
							diff = new Date().getTime() - getServerTime();
							startServices();
							uid = suid;
							pwd = spwd;
							log.info("login OK "+diff+" "+getServerTime()+" "+new Date());
							engine.getEventDispatcher().pushEvent(
									FeedEventDispatcher.EVENT_LOGINOK, null);
							isFeedLogin = true;//yosep other device
							oldsesid = sesid; // yosep other device
							oldsuid	 = suid; // yosep other device
							reqTime.setReq(true);//timer sync yosep
							break;
						} else {
							log.warn("Login Failed, retrying(" + i + ")....");
							HashMap param = new HashMap(1);
							param.put(FeedEventDispatcher.PARAM_MESSAGE,
									"Register Failed, retrying(" + i + ")....");
							engine.getEventDispatcher().pushEvent(
									FeedEventDispatcher.EVENT_LOGINSTATUS, param);
							isFeedLogin = false;//yosep other device
							countFeedLogin= 0 ;
						}
					} catch (Exception exp) {
						log.warn("Failed, " + exp.getMessage());
						HashMap param = new HashMap(1);
						// param.put(FeedEventDispatcher.PARAM_MESSAGE,
						// "Failed, retrying(" + i + ")....");
						param.put(FeedEventDispatcher.PARAM_MESSAGE,
								"connecting.....");
						engine.getEventDispatcher().pushEvent(
								FeedEventDispatcher.EVENT_LOGINSTATUS, param);
						try {
							Thread.sleep(500);
						} catch (Exception d) {
						}
						;
						bresult = false;
						//log.error(Utils.logException(exp));
						exp.printStackTrace();
					}
	
				} else if(isFeedLogin) {//yosep other device
					log.warn("Invalid userid/password....");
					HashMap param = new HashMap(1);
					param.put(FeedEventDispatcher.PARAM_MESSAGE,
							"Invalid userid/password");
					engine.getEventDispatcher().pushEvent(
							FeedEventDispatcher.EVENT_FALSEPWD, param);
					sesid = oldsesid;
					this.suid = oldsuid;
					break;
				} else {
					log.warn("Invalid userid/password....");
					HashMap param = new HashMap(1);
					param.put(FeedEventDispatcher.PARAM_MESSAGE,
							"Invalid userid/password");
					engine.getEventDispatcher().pushEvent(
							FeedEventDispatcher.EVENT_LOGINFAILED, param);
					socketConnector.closeSocket();
					isFeedLogin = false;//yosep other device
					countFeedLogin= 0 ;
					break;
				}
			}	
		}

		if (i > retrying) {
			HashMap param = new HashMap(1);
			param.put(FeedEventDispatcher.PARAM_MESSAGE,
					"Connection Failed, Please Try Again ..");
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_LOGINFAILED, param);
		}
	}

	public boolean connect(String host, int port) {
		boolean isstart = socketConnector.start(host, port);

		return isstart;
 
	}

	@Override
	public boolean logout() {
		// channel.write("logout|" + sesid);

		timerRefresh.stop();
		boolean bresult = true;
		blogout = true;
		if (this.outQueue.isAlive())
			addOut(CMD_LOGOUT);
		isReconnect = false;// yosep other device
		log.info("logout " + this.outQueue.isAlive()+" d9 "+countFeedLogin+" "+isReconnect);
		
		if (countFeedLogin>0 ) {
			try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			logon(suid, spwd);
		}else {
			isFeedLogin = false;//yosep other device
			countFeedLogin= 0 ;
			suid = "";
			spwd = "";
			reqTime.setReq(false);//timer sync yosep
		}
		
		return false;
	}

	@Override
	public void subscribe(String sheader) {
		// channel.write(sheader);
		addOut(C_SUBSCRIBE + "*" + sheader);
	}

	@Override
	public void unsubscribe(String msgtype) {
		addOut(CMD_UNSUBSCRIBE + "*" + msgtype);
	}

	private void addIn(String smsg) {
		vin.addElement(smsg);
		synchronized (vin) {
			vin.notify();
		}
	}

	private void addOut(String smsg) {
		vou.addElement(smsg);
		synchronized (vou) {
			vou.notify();
		}
	}

	public static void main(String[] args) {
		PropertyConfigurator.configure("log.properties");
		// FeedConnectionSocket conn = new FeedConnectionSocket();
		// conn.loadSettings();
		// conn.logon("edosls", Utils.getMD5("password"));
		/*
		 * try { ClientBootstrap bootstrap = new ClientBootstrap( new
		 * NioClientSocketChannelFactory( Executors.newCachedThreadPool(),
		 * Executors.newCachedThreadPool())); bootstrap.setPipelineFactory(new
		 * ChannelPipelineFactory() {
		 * 
		 * @Override public ChannelPipeline getPipeline() throws Exception {
		 * OrderedMemoryAwareThreadPoolExecutor ordered = new
		 * OrderedMemoryAwareThreadPoolExecutor( 3, 100, 100, 10,
		 * TimeUnit.SECONDS); ChannelPipeline pipeline = pipeline();
		 * pipeline.addLast("executor", new ExecutionHandler(ordered));
		 * pipeline.addLast("decoder", new StringFrameDecoder());
		 * pipeline.addLast("encoder", new StringEncoder()); //
		 * pipeline.addLast("handler", FeedConnectionSocket.this); return
		 * pipeline; } }); bootstrap.connect(new
		 * InetSocketAddress("192.168.0.56", 1878)); } catch (Exception e) {
		 * e.printStackTrace(); }
		 */
	}

	@Override
	public String getHistory(String param) throws Exception {
		String[] sp = param.split("\\|");
		log.info("getHistory " + param + " " + sp.length);

		if (sp.length >= 2) {
			String result = (String) blockRequest.get(
					BlockRequestSynchronized.REQUEST_HISTORY).sendAndReceive(
					"history*" + sesid + "*" + (String) param);
//			 log.info("result history " + result);
			return result;
		} else if (param.equals("BROKER")) {
			// log.info("can't parsing getHistory");
			String result = (String) blockRequest.get(
					BlockRequestSynchronized.REQUEST_BROKERINFO)
					.sendAndReceive("brokerinfo" + "*" + sesid + "*" + param);
			return result;
		}
		return null;
		// return null;
	}

	@Override
	public byte[] getFile(String param) throws Exception {
		String sresult = (String) blockRequest.get(
				BlockRequestSynchronized.REQUEST_HISTORY_FILE)
				.sendAndReceiveWithParam(param);
		return sresult.getBytes();
	}

	@Override
	public void reconnect(final int count) {
		new Thread(new Runnable() {
			public void run() {
				doReconnect(count);
			}
		}).start();
	}

	private int flip = 1, nextflip = 1, cnt = 0;
	private boolean isReconnect = false;

	protected void doReconnect(int count) {
		boolean bresult = false;
		try {
			cnt++;
			HashMap param = new HashMap(1);
			param.put(FeedEventDispatcher.PARAM_MESSAGE,
					"trying reconnecting...");
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_RECONNECTSTATUS, param);
			if (cnt == nextflip) {
				flip++;
				nextflip = nextflip + 1;
			}
			if (cnt >= totalServer * 1) {
				flip = 1;
				nextflip = 1;
				cnt = 0;
			}
			String url = (String) hashIP.get(new String(flip + ""));
			log.info("reconnect to: " + url + " userid: " + uid
					+ " flip number: " + flip + " " + totalServer);
			String[] urlport = url.split("\\:");
			// for (; !bresult;) {
			bresult = connect(urlport[0], new Integer(urlport[1]));
			log.info("doReconnect:" + url + " " + bresult);

			if (bresult) {
				svr = url;
				isReconnect = true;
				log.info("reconnect socket connected "+count);
			} else {
				log.warn("reconnecting failed");
				HashMap param2 = new HashMap(1);
				param2.put(FeedEventDispatcher.PARAM_MESSAGE,
						"reconnecting failed or server not ready");
				engine.getEventDispatcher().pushEvent(
						FeedEventDispatcher.EVENT_RECONNECTSTATUS, param2);
			} /*
			 * else { log.warn("reconnecting failed"); HashMap param2 = new
			 * HashMap(1); param2.put(FeedEventDispatcher.PARAM_MESSAGE,
			 * "reconnecting failed or server not ready");
			 * engine.getEventDispatcher().pushEvent(
			 * FeedEventDispatcher.EVENT_RECONNECTFAILED, param2); }
			 */
		} catch (Exception ex) {
			log.warn("reconnecting failed");
			HashMap param2 = new HashMap(1);
			param2.put(FeedEventDispatcher.PARAM_MESSAGE,
					"reconnecting failed or server not ready");
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_RECONNECTFAILED, param2);
			//log.error(Utils.logException(ex));
			ex.printStackTrace();
		}
	}

	@Override
	public void changePassword(String oldpassword, String newpwd) {
	}

	@Override
	public String getUserId() {
		return this.suid;
	}

	@Override
	public long getTime() {
		return new Date().getTime() - diff;
	}

	public long getServerTime() {
		Long data = Long.parseLong((String) blockRequest.get(
				BlockRequestSynchronized.REQUEST_TIME).sendAndReceive(
				"time*" + sesid));
		System.out.println(data+" oo ");
		return data;

	}

	@Override
	public String getPassword() {
		return spwd;
	}

	@Override
	public void connected(ClientSocket sock) {

		new Thread() {

			@Override
			public void run() {
				log.info("connected to server socket " + svr + " "
						+ isReconnect);

				if (!isReconnect)
					doLogin(suid, spwd);
//					doLoginV(suid, spwd);
				else
					processReconnect();
			}

		}.start();

	}

	private void processReconnect() {
		int retrying = 5;
		boolean bresult = false;
		String message = new String();	String next_action = new String();
		String sukses = new String();
		
		for (int i = 0; i < retrying && !bresult; i++) {
			try {
				log.info("Reconnect send userid and password "+uid+" "+pwd);
				Object obj = blockRequest.get(
						BlockRequestSynchronized.REQUEST_LOGIN).sendAndReceive(
								"login*" + suid + "*" + spwd);
	//								"vlogin*" + uid + "*" + pwd);// yosep other device
				sesid = (Long) obj;
				bresult = sesid > 0;
				/*//yosep enforcementpass
				log.info("Reconnect login receive " + obj.toString()+ "=");
	//			sesid = (Long) obj; 
				String[] reply = ((String)obj).split("\\|");
				sesid = Long.parseLong(reply[0]);
				bresult = sesid > 0;
				message = reply[2];
				next_action = reply[3];*/
				if (bresult) {
					String session = getDate();
					if (session != null) {
						FeedSetting.session = new String(session);
						startServices();
						log.info("ok, connection ready");
						engine.getEventDispatcher().pushEvent(
								FeedEventDispatcher.EVENT_RECONNECTOK, null);
						isFeedLogin = true; // yosep other device 
						isReconnect = false; // yosep other device 
					} else {
						log.warn("error while get session from server");
						HashMap param2 = new HashMap(1);
						param2.put(FeedEventDispatcher.PARAM_MESSAGE,
								"Failed on register to server, try again");
						engine.getEventDispatcher().pushEvent(
								FeedEventDispatcher.EVENT_RECONNECTSTATUS,
								param2);
						isFeedLogin = false; // yosep other device 
						countFeedLogin= 0 ;// yosep other device 
					}
				} else {
					log.warn("reconnecting failed");
					HashMap param2 = new HashMap(1);
					param2.put(FeedEventDispatcher.PARAM_MESSAGE,
							"reconnecting failed or server not ready");
					engine.getEventDispatcher().pushEvent(
							FeedEventDispatcher.EVENT_RECONNECTSTATUS, param2);
					isFeedLogin = false; // yosep other device 
					countFeedLogin= 0 ; // yosep other device 
				}
	
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

	@Override
	public void disconnect(ClientSocket arg0) {
		log.info("disconnected client " + blogout);
		blockRequest.clearWaitingStateClient();
		if (!blogout ){
			isFeedLogin = false;//yosep other device
			countFeedLogin= 0 ;
			reconnect();
		}
	}

	@Override
	public void receive(ClientSocket arg0, byte[] bt) {
		String msg = new String(Utils.decompress(bt));
		// log.info("receive message " + msg);
		String[] data = msg.split("\\|");

		/*
		 * if (data[0].equals("login")) { sesid = new Integer(data[2]); if
		 * (sesid == -1) { log.info("login failed " + data[1]); this.suid = "";
		 * this.spwd = ""; } else { startServices();
		 * engine.getParser().subscribe(FeedParser.PARSER_STOCK,
		 * FeedParser.PARSER_STOCK);
		 * engine.getParser().subscribe(FeedParser.PARSER_BROKER,
		 * FeedParser.PARSER_BROKER);
		 * engine.getParser().subscribe(FeedParser.PARSER_INDICES,
		 * FeedParser.PARSER_INDICES);
		 * engine.getParser().subscribe(FeedParser.PARSER_STOCKSUMMARY,
		 * FeedParser.PARSER_STOCKSUMMARY);
		 * engine.getParser().subscribe(FeedParser.PARSER_RUNNINGTRADE,
		 * FeedParser.PARSER_RUNNINGTRADE);
		 * engine.getParser().subscribe(FeedParser.PARSER_TRADESUMMINV,
		 * FeedParser.PARSER_TRADESUMMINV); } } else
		 */if (data[0].equals("logout")) {

			sesid = -1l;
			this.suid = "";
			this.spwd = "";
		} else if (data[0].equals("kill")) {
			sesid = -1l;
			this.suid = "";
			this.spwd = "";
			log.info("session killed " + data[1]);
			if (data[1].contains("killed")) {
				if (timerRefresh.isRunning()) {
					socketConnector.closeSocket();
					killed();
				}
			}
		} else if (data[0].equals("heartbeat")) {
			log.info("heartbeat exception " + data[1]);
			if (data[1].contains("expired")) {
				/*if (timerRefresh.isRunning())
					killed();*/
				isFeedLogin = false;//yosep other device
				countFeedLogin= 0 ;//yosep other device
				reconnect();
			}
		} else if (blockRequest.isBlockingRequest(msg)) {
			// log.info("blocking response " + msg);
			blockRequest.receive(msg);
		} else {

			String[] stemp = msg.split("\n", -2);

			for (int i = 0; i < stemp.length; i++) {
				if (stemp[i] != null && !stemp[i].isEmpty())
					addIn(stemp[i]);
			}
		}
	}

	private void killed() {
		engine.getEventDispatcher().pushEvent(FeedEventDispatcher.EVENT_KILLED,
				null);
	}

	@Override
	public String getDate() throws Exception {
		// TODO Auto-generated method stub
		return (String) blockRequest.get(BlockRequestSynchronized.REQUEST_DATE)
				.sendAndReceive();
	}

	@Override
	public Long getSessionid() {
		// TODO Auto-generated method stub
		return sesid;
	}

	@Override
	public void send(String msg) {
		socketConnector.sendMessage(Utils.compress(msg.getBytes()));
	}

	private void reconnect() {
		timerRefresh.stop();
		if (sesid != -1)
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_DISCONNECT, null);
	}

	@Override
	public void receive(ClientSocket arg0, Object arg1) {
		
	}
	// yosep other device #start
		@Override
		public int getCountFeedLogin() {
			// TODO Auto-generated method stub
			return countFeedLogin;
		}
		@Override
		public void setCountFeedLogin(int a) {
			// TODO Auto-generated method stub
			countFeedLogin =+a;
			System.out.println(countFeedLogin+" lld");
		}

		@Override
		public void setuserpass(String username, String password) {
			suid = username;
			spwd = password;
		}
		// yosep other device #end

		@Override
		public boolean getFeedLogin() {
			// TODO Auto-generated method stub
			return isFeedLogin;
		}
		class ReqTime extends Thread{
			FeedConnectionSocket socketConnection;
			boolean req = false;
			ReqTime(FeedConnectionSocket connectionSocket){
				socketConnection = connectionSocket;
			}
			
			public void setReq(boolean req){
				this.req = req;
			}

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					while (true){
						if (req) {
							socketConnection.diff = new Date().getTime() - socketConnection.getServerTime();System.out.println("reqtime thread");
							Thread.sleep(1* 60 * 1000);				
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
}


//package eqtrade.feed.engine.socket;
//
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Vector;
//
//import javax.swing.Timer;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//import org.apache.log4j.PropertyConfigurator;
//import org.jboss.netty.bootstrap.ClientBootstrap;
//
//import com.eqtrade.ClientSocket;
//import com.eqtrade.Receiver;
//import com.eqtrade.SocketFactory;
//import com.eqtrade.SocketInterface;
//
//import eqtrade.feed.core.FeedSetting;
//import eqtrade.feed.core.Utils;
//import eqtrade.feed.engine.FeedEventDispatcher;
//
//public class FeedConnectionSocket implements eqtrade.feed.core.IFeedConnection,
//		Receiver {
//	private Log log = LogFactory.getLog(getClass());
//	private SocketInterface socketConnector;
//	private Thread inQueue;
//	private boolean bstop = false, blogout = false;
//	private Vector vin = new Vector();
//	private Long sesid = -1l;
//	private String suid, spwd;
//	private String uid = "", pwd, svr;
//	private String ssvr;
//	private Timer timerRefresh = null;
//	private Thread outQueue;
//	private int ninterval, totalServer;
//	private volatile Vector vou = new Vector(10, 5);
//	//private final static String C_GETNEW = "GET";
//	private final static String C_GETNEWMSG = "getMsg";
//	private final static String C_SUBSCRIBE = "SUB";
//	private final static String CMD_UNSUBSCRIBE = "UNSUB";
//	private final static String CMD_LOGOUT = "LOGOUT";
//	private final static String CMD_LOGIN = "LOGIN";
//	private eqtrade.feed.engine.FeedEngine engine;
//	private HashMap hashIP = new HashMap();
//	private Vector<String> vrequestcallback = new Vector<String>();
//	// private Queue<String> tempreq = new ConcurrentLinkedQueue<String>();
//	private BlockRequestSynchronized blockRequest;
//	private ClientBootstrap bootstrap;
//	private long diff = 0;
//	
//	private byte[] btmsgGet;
//	private final static String C_GETNEW = "GET",C_GETHEARTBEAT = "GETHEARTBEAT", C_GETMSGNEW = "REFRESH",
//			C_GETMSGTRADE = "getMessageTrade";
//
//	public FeedConnectionSocket(eqtrade.feed.engine.FeedEngine engine) {
//		this.engine = engine;
//		init();
//	}
//
//	public void init() {
//		try {
//			loadSettings();
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		}
//		inQueue = new Thread(createInQueue());
//
//		outQueue = new Thread(createOutQueue());
//		int timer = 50;
//		//int timer =1 * 60 * 1000;
//		
//		timerRefresh = new Timer(timer, new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				(new Thread(new Runnable() {
//					public void run() {
//						// log.info("send heartbeat feed.socket");
//
//						//addOut(C_GETMSGTRADE);
//						addOut(C_GETMSGTRADE);
//						//addOut(C_GETNEW);
//					}
//				})).start();
//				// diff = new Date().getTime() - getServerTime();
//			}
//		});
//		
////		timerRefresh = new Timer(timer, new ActionListener() { //timerRefresh = new Timer(1 * 60 * 1000, new ActionListener()
////			public void actionPerformed(ActionEvent e) {
////				(new Thread(new Runnable() {
////					public void run() {
////						//addOut(C_GETNEW);
////						addOut(C_GETNEWMSG);
////
////					}
////				})).start();
////				// diff = new Date().getTime() - getServerTime();
////			}
////		});
////		timerRefresh.setInitialDelay(1 * 5 * 1000); //timerRefresh.setInitialDelay(1 * 60 * 1000);
////		timerRefresh.stop();
//		
//		timerRefresh.setInitialDelay(timer);
//		timerRefresh.stop();
//
//		blockRequest = new BlockRequestSynchronized(engine);
//	}
//
//	private Runnable createOutQueue() {
//		return new Runnable() {
//			public void run() {
//				Vector vtemp;
//				while (!bstop) {
//					try {
//						if (vou.size() > 0) {
//							vtemp = (Vector) vou.clone();
//							vou.removeAll(vtemp);
//							processOut(vtemp);
//						} else {
//							synchronized (vou) {
//								vou.wait();
//							}
//						}
//					} catch (Exception ex) {
//					}
//				}
//				System.gc();
//			}
//		};
//	}
//
//	protected void processOut(Vector vdatamsg) {
//		byte[] bpar = null;
//		try {
//			String scmd, stemp;
//			Vector vtempdata = new Vector(10, 5);
//			for (int i = 0; i < vdatamsg.size(); i++) {
//				stemp = vdatamsg.elementAt(i).toString();
//				if (!vtempdata.contains(stemp)) {
//					vtempdata.addElement(stemp);
//				}
//			}
//
//			for (int i = 0; i < vtempdata.size(); i++) {
//				scmd = vtempdata.elementAt(i).toString();
//				//log.info("processOut:" + scmd + ":" + sesid);
//				if (sesid > -1) {
//
//					if (scmd.equals(C_GETNEW)) {
//						// channel.write("heartbeat|" + sesid);
//						socketConnector.sendMessage(Utils
//								.compress(("heartbeat*" + sesid).getBytes()));
//					}else if (scmd.equals(C_GETNEWMSG)) {
//						// channel.write("heartbeat|" + sesid);
//						socketConnector.sendMessage(Utils
//								.compress((C_GETNEWMSG+"*" + sesid).getBytes()));
//					} else if (scmd.startsWith(C_SUBSCRIBE)) {
//						String[] msg = scmd.split("\\*");
//						// channel.write("subscribe|" + sesid + "|" + msg[1]);
//
//						socketConnector.sendMessage(Utils
//								.compress(("subscribe*" + sesid + "*" + msg[1])
//										.getBytes()));
//
//					} else if (scmd.startsWith(CMD_UNSUBSCRIBE)) {
//						String[] msg = scmd.split("\\*");
//						// channel.write("unsubscribe|" + sesid + "|" + msg[1]);
//						socketConnector
//								.sendMessage(Utils.compress(("unsubscribe*"
//										+ sesid + "*" + msg[1]).getBytes()));
//
//					} else if (scmd.startsWith(CMD_LOGOUT)) {
//						// channel.write("logout|" + sesid);
//						socketConnector.sendMessage(Utils
//								.compress(("logout*" + sesid).getBytes()));
//						socketConnector.closeSocket();
//
//					} else if (scmd.equals(C_GETMSGTRADE) && btmsgGet!= null) {
//						// channel.write("heartbeat|" + sesid);
//						socketConnector.sendMessage(btmsgGet);
//					}
//				}
//
//			}
//		} /*
//		 * catch (RemoteException ex) { log.error(Utils.logException(ex));
//		 * ex.printStackTrace(); if (!ex.getMessage().endsWith("killed")) {
//		 * reconnect(); } else { if (timerRefresh.isRunning()) killed(); } }
//		 */catch (Exception ex) {
//			ex.printStackTrace();
//			//log.error(Utils.logException(ex));
//		}
//	}
//
//	private void startServices() {
//
//		if (!this.inQueue.isAlive())
//			inQueue.start();
//
//		if (!this.outQueue.isAlive())
//			outQueue.start();
//		timerRefresh.start();
//		log.info("service started");
//	}
//
//	private Runnable createInQueue() {
//		return new Runnable() {
//			public void run() {
//				Vector vtemp;
//				while (!bstop) {
//					try {
//						if (vin.size() > 0) {
//							vtemp = (Vector) vin.clone();
//							vin.removeAll(vtemp);
//							for (int i = 0; i < vtemp.size(); i++) {
//								// HashMap param = new HashMap(1);
//								// param.put(FeedEventDispatcher.PARAM_MESSAGE,
//								// vtemp.elementAt(i));
//								// engine.getEventDispatcher().pushEvent(FeedEventDispatcher.EVENT_ONDATA,
//								// param);
//								// engine.getParser().parse(
//								// (String) vtemp.elementAt(i));
//								// log.info("get message " +
//								// vtemp.elementAt(i));
//								// log.info("parser "+vtemp.elementAt(i));
//
//								engine.getParser().parse(
//										(String) vtemp.elementAt(i));
//							}
//						} else {
//							synchronized (vin) {
//								vin.wait();
//							}
//						}
//					} catch (Exception ex) {
//					}
//				}
//				System.gc();
//			}
//		};
//	}
//
//	public void loadSettings() throws Exception {
//
//		blogout = false;
//		socketConnector = SocketFactory.createSocket(
//				"data/config/socket.ini", this);
//
//		Object o = Utils.readFile("data/config/feed.dat");
//		Vector v = null;
//		if (o == null) {
//			v = new Vector(3);
//			v.addElement("172.21.1.101:5555");
//			v.addElement("172.21.1.101:5555");
//			// v.addElement("127.0.0.1:5555/feed");
//			// v.addElement("127.0.0.1:5555/feed");
//			Utils.writeFile("data/config/feed.dat", v);
//		} else {
//			v = (Vector) o;
//		}
//		// ninterval = ((Integer) v.elementAt(2)).intValue();
//		int x = v.size();
//		//ninterval = ((Integer) v.elementAt(x - 1)).intValue();
//		totalServer = x ;
//		hashIP.clear();
//		for (int i = 1; i <= totalServer; i++) {
//			ssvr = (String) v.elementAt(i - 1);
//			hashIP.put(new String(i + ""), new String(ssvr));
//		}
//		// try {
//		// sip = InetAddress.getLocalHost().getHostAddress();
//		// } catch (Exception ex){}
//		log.info("available server test: " + totalServer);
//		log.info("available server test: " + hashIP.toString());
//
//	}
//
//	Thread threadLogin = null;
//
//	@Override
//	public void logon(final String suid, final String spwd) {
//		this.suid = suid;
//		this.spwd = spwd;
//		/*
//		 * channel.write("login|" + suid + "|" + spwd); // addOut(CMD_LOGIN +
//		 * "*" + "|" + suid + "|" + spwd); // channel.write("login|" + this.suid
//		 * + "|" + this.spwd); // addOut(CMD_LOGIN + "*login|" + this.suid + "|"
//		 * + this.spwd);
//		 */
//		if (threadLogin != null) {
//			threadLogin.stop();
//			threadLogin = null;
//		}
//		threadLogin = new Thread(new Runnable() {
//			public void run() {
//				// doLogin(suid, spwd);
//				try {
//					blogout = false;
//					loadSettings();
//
//					searchingServer();
//				} catch (Exception ex) {
//					ex.printStackTrace();
//				}
//
//			}
//		});
//		threadLogin.start();
//
//	}
//
//	public boolean searchingServer() {
//		boolean bresult = false;
//		int i = 1, curr = 1, next = 2, loginattempt = 10, logcoba = 1;
//
//		String url;
//		while (logcoba <= loginattempt) {
//			for (; i <= (totalServer * 1); i++) { 
//				if (i == next) {
//					curr++;
//					next = next + 1;
//				}
//
//				url = (String) hashIP.get(new String(curr + ""));
//				log.info("login to : " + url + " user name: " + suid);
//				String[] urlport = url.split("\\:");
//
//				bresult = connect(urlport[0], new Integer(urlport[1]));
//
//				log.info("isconnected " + bresult);
//
//				if (bresult) {
//					svr = url;
//					logcoba = logcoba + loginattempt + 1;
//					break;
//				} else {
//					log.warn("trying to reconnect (" + i + ")");
//				}
//
//			}
//			logcoba = logcoba + 1;
//
//		}
//
//		if (!bresult && i > totalServer * 1) {
//			HashMap param = new HashMap(1);
//			param.put(FeedEventDispatcher.PARAM_MESSAGE,
//					"Connection Failed, Please Try Again ..");
//			engine.getEventDispatcher().pushEvent(
//					FeedEventDispatcher.EVENT_LOGINFAILED, param);
//			socketConnector.closeSocket();
//		}
//
//		return bresult;
//	}
//
//	public void doLogin(String suid, String spwd) {
//		boolean bresult = false;
//		int i = 1, curr = 1, next = 2, loginattempt = 10, logcoba = 1;
//
//		int retrying = 3;
//		String url;
//
//		for (; i <= retrying; i++) {
//			Object obj = blockRequest.get(
//					BlockRequestSynchronized.REQUEST_LOGIN).sendAndReceive(
//					"login*" + suid + "*" + spwd);
//			log.info("login receive " + obj.toString()+ "=");
//			sesid = (Long) obj;
//			bresult = sesid > 0;
//			if (bresult) {
//				try {
//					String session = getDate();
//					if (session != null) {
//						logcoba = loginattempt + 1;
//						FeedSetting.session = new String(session);
//						diff = new Date().getTime() - getServerTime();
//						startServices();
//						uid = suid;
//						pwd = spwd;
//						log.info("login OK");
//						
//						btmsgGet = Utils.compress(("getMsg*" + sesid)
//								.getBytes());
//						
//						engine.getEventDispatcher().pushEvent(
//								FeedEventDispatcher.EVENT_LOGINOK, null);
//						break;
//					} else {
//						log.warn("Login Failed, retrying(" + i + ")....");
//						HashMap param = new HashMap(1);
//						param.put(FeedEventDispatcher.PARAM_MESSAGE,
//								"Register Failed, retrying(" + i + ")....");
//						engine.getEventDispatcher().pushEvent(
//								FeedEventDispatcher.EVENT_LOGINSTATUS, param);
//					}
//				} catch (Exception exp) {
//					log.warn("Failed, " + exp.getMessage());
//					HashMap param = new HashMap(1);
//					// param.put(FeedEventDispatcher.PARAM_MESSAGE,
//					// "Failed, retrying(" + i + ")....");
//					param.put(FeedEventDispatcher.PARAM_MESSAGE,
//							"connecting.....");
//					engine.getEventDispatcher().pushEvent(
//							FeedEventDispatcher.EVENT_LOGINSTATUS, param);
//					try {
//						Thread.sleep(500);
//					} catch (Exception d) {
//					}
//					;
//					bresult = false;
//					//log.error(Utils.logException(exp));
//					exp.printStackTrace();
//				}
//
//			} else {
//				log.warn("Invalid userid/password....");
//				HashMap param = new HashMap(1);
//				param.put(FeedEventDispatcher.PARAM_MESSAGE,
//						"Invalid userid/password");
//				engine.getEventDispatcher().pushEvent(
//						FeedEventDispatcher.EVENT_LOGINFAILED, param);
//				socketConnector.closeSocket();
//				break;
//			}
//
//		}
//
//		if (i > retrying) {
//			HashMap param = new HashMap(1);
//			param.put(FeedEventDispatcher.PARAM_MESSAGE,
//					"Connection Failed, Please Try Again ..");
//			engine.getEventDispatcher().pushEvent(
//					FeedEventDispatcher.EVENT_LOGINFAILED, param);
//		}
//	}
//
//	public boolean connect(String host, int port) {
//		boolean isstart = socketConnector.start(host, port);
//
//		return isstart;
//
//	}
//
//	@Override
//	public boolean logout() {
//		// channel.write("logout|" + sesid);
//
//		timerRefresh.stop();
//		boolean bresult = true;
//		blogout = true;
//		if (this.outQueue.isAlive())
//			addOut(CMD_LOGOUT);
//
//		log.info("logout " + this.outQueue.isAlive());
//
//		return false;
//	}
//
//	@Override
//	public void subscribe(String sheader) {
//		// channel.write(sheader);
//		addOut(C_SUBSCRIBE + "*" + sheader);
//	}
//
//	@Override
//	public void unsubscribe(String msgtype) {
//		addOut(CMD_UNSUBSCRIBE + "*" + msgtype);
//	}
//
//	private void addIn(String smsg) {
//		vin.addElement(smsg);
//		synchronized (vin) {
//			vin.notify();
//		}
//	}
//
//	private void addOut(String smsg) {
//		vou.addElement(smsg);
//		synchronized (vou) {
//			vou.notify();
//		}
//	}
//
//	public static void main(String[] args) {
//		PropertyConfigurator.configure("log.properties");
//		// FeedConnectionSocket conn = new FeedConnectionSocket();
//		// conn.loadSettings();
//		// conn.logon("edosls", Utils.getMD5("password"));
//		/*
//		 * try { ClientBootstrap bootstrap = new ClientBootstrap( new
//		 * NioClientSocketChannelFactory( Executors.newCachedThreadPool(),
//		 * Executors.newCachedThreadPool())); bootstrap.setPipelineFactory(new
//		 * ChannelPipelineFactory() {
//		 * 
//		 * @Override public ChannelPipeline getPipeline() throws Exception {
//		 * OrderedMemoryAwareThreadPoolExecutor ordered = new
//		 * OrderedMemoryAwareThreadPoolExecutor( 3, 100, 100, 10,
//		 * TimeUnit.SECONDS); ChannelPipeline pipeline = pipeline();
//		 * pipeline.addLast("executor", new ExecutionHandler(ordered));
//		 * pipeline.addLast("decoder", new StringFrameDecoder());
//		 * pipeline.addLast("encoder", new StringEncoder()); //
//		 * pipeline.addLast("handler", FeedConnectionSocket.this); return
//		 * pipeline; } }); bootstrap.connect(new
//		 * InetSocketAddress("192.168.0.56", 1878)); } catch (Exception e) {
//		 * e.printStackTrace(); }
//		 */
//	}
//
//	@Override
//	public String getHistory(String param) throws Exception {
//		String[] sp = param.split("\\|");
//		log.info("getHistory " + param + " " + sp.length);
//
//		if (sp.length >= 2) {
//			String result = (String) blockRequest.get(
//					BlockRequestSynchronized.REQUEST_HISTORY).sendAndReceive(
//					"history*" + sesid + "*" + (String) param);
//			// log.info("result history " + result);
//			return result;
//		} else if (param.equals("BROKER")) {
//			// log.info("can't parsing getHistory");
//			String result = (String) blockRequest.get(
//					BlockRequestSynchronized.REQUEST_BROKERINFO)
//					.sendAndReceive("brokerinfo" + "*" + sesid + "*" + param);
//			return result;
//		}
//		return null;
//		// return null;
//	}
//
//	@Override
//	public byte[] getFile(String param) throws Exception {
//		String sresult = (String) blockRequest.get(
//				BlockRequestSynchronized.REQUEST_HISTORY_FILE)
//				.sendAndReceiveWithParam(param);
//		return sresult.getBytes();
//	}
//
//	@Override
//	public void reconnect(final int count) {
//		new Thread(new Runnable() {
//			public void run() {
//				doReconnect(count);
//			}
//		}).start();
//	}
//
//	private int flip = 1, nextflip = 1, cnt = 0;
//	private boolean isReconnect = false;
//
//	protected void doReconnect(int count) {
//		boolean bresult = false;
//		isReconnect = true;
//		try {
//			cnt++;
//			HashMap param = new HashMap(1);
//			param.put(FeedEventDispatcher.PARAM_MESSAGE,
//					"trying reconnecting...");
//			engine.getEventDispatcher().pushEvent(
//					FeedEventDispatcher.EVENT_RECONNECTSTATUS, param);
//			if (cnt == nextflip) {
//				flip++;
//				nextflip = nextflip + 1;
//			}
//			if (cnt >= totalServer * 1) {
//				flip = 1;
//				nextflip = 1;
//				cnt = 0;
//			}
//			String url = (String) hashIP.get(new String(flip + ""));
//			log.info("reconnect to: " + url + " userid: " + uid
//					+ " flip number: " + flip + " " + totalServer);
//			String[] urlport = url.split("\\:");
//			// for (; !bresult;) {
//			bresult = connect(urlport[0], new Integer(urlport[1]));
//			log.info("doReconnect:" + url + " " + bresult);
//
//			if (bresult) {
//				svr = url;
//				log.info("reconnect socket connected");
//			} else {
//				log.warn("reconnecting failed");
//				HashMap param2 = new HashMap(1);
//				param2.put(FeedEventDispatcher.PARAM_MESSAGE,
//						"reconnecting failed or server not ready");
//				engine.getEventDispatcher().pushEvent(
//						FeedEventDispatcher.EVENT_RECONNECTSTATUS, param2);
//			} /*
//			 * else { log.warn("reconnecting failed"); HashMap param2 = new
//			 * HashMap(1); param2.put(FeedEventDispatcher.PARAM_MESSAGE,
//			 * "reconnecting failed or server not ready");
//			 * engine.getEventDispatcher().pushEvent(
//			 * FeedEventDispatcher.EVENT_RECONNECTFAILED, param2); }
//			 */
//		} catch (Exception ex) {
//			log.warn("reconnecting failed");
//			HashMap param2 = new HashMap(1);
//			param2.put(FeedEventDispatcher.PARAM_MESSAGE,
//					"reconnecting failed or server not ready");
//			engine.getEventDispatcher().pushEvent(
//					FeedEventDispatcher.EVENT_RECONNECTFAILED, param2);
//			//log.error(Utils.logException(ex));
//			ex.printStackTrace();
//		}
//	}
//
//	@Override
//	public void changePassword(String oldpassword, String newpwd) {
//	}
//
//	@Override
//	public String getUserId() {
//		return this.suid;
//	}
//
//	@Override
//	public long getTime() {
//		return new Date().getTime() - diff;
//	}
//
//	public long getServerTime() {
//		Long data = Long.parseLong((String) blockRequest.get(
//				BlockRequestSynchronized.REQUEST_TIME).sendAndReceive(
//				"time*" + sesid));
//		return data;
//
//	}
//
//	@Override
//	public String getPassword() {
//		return spwd;
//	}
//
//	@Override
//	public void connected(ClientSocket sock) {
//
//		new Thread() {
//
//			@Override
//			public void run() {
//				log.info("connected to server socket " + svr + " "
//						+ isReconnect);
//
//				if (!isReconnect)
//					doLogin(suid, spwd);
//				else
//					processReconnect();
//			}
//
//		}.start();
//
//	}
//
//	private void processReconnect() {
//		int retrying = 5;
//		boolean bresult = false;
//
//		for (int i = 0; i < retrying && !bresult; i++) {
//			try {
//				log.info("send userid and password "+uid+" "+pwd);
//				Object obj = blockRequest.get(
//						BlockRequestSynchronized.REQUEST_LOGIN).sendAndReceive(
//								"login*" + suid + "*" + spwd);
//				sesid = (Long) obj;
//				bresult = sesid > 0;
//				if (bresult) {
//					String session = getDate();
//					if (session != null) {
//						FeedSetting.session = new String(session);
//						startServices();
//						btmsgGet = Utils.compress(("getMsg*" + sesid)
//								.getBytes());
//						log.info("ok, connection ready");
//						engine.getEventDispatcher().pushEvent(
//								FeedEventDispatcher.EVENT_RECONNECTOK, null);
//					} else {
//						log.warn("error while get session from server");
//						HashMap param2 = new HashMap(1);
//						param2.put(FeedEventDispatcher.PARAM_MESSAGE,
//								"Failed on register to server, try again");
//						engine.getEventDispatcher().pushEvent(
//								FeedEventDispatcher.EVENT_RECONNECTSTATUS,
//								param2);
//					}
//				} else {
//					log.warn("reconnecting failed");
//					HashMap param2 = new HashMap(1);
//					param2.put(FeedEventDispatcher.PARAM_MESSAGE,
//							"reconnecting failed or server not ready");
//					engine.getEventDispatcher().pushEvent(
//							FeedEventDispatcher.EVENT_RECONNECTSTATUS, param2);
//				}
//
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			}
//		}
//
//	}
//
//	@Override
//	public void disconnect(ClientSocket arg0) {
//		log.info("disconnected client " + blogout);
//		blockRequest.clearWaitingStateClient();
//		if (!blogout)
//			reconnect();
//	}
//
//	@Override
//	public void receive(ClientSocket arg0, byte[] bt) {
//		String msg = new String(Utils.decompress(bt));
//		// log.info("receive message " + msg);
//		String[] data = msg.split("\\|");
//
//		/*
//		 * if (data[0].equals("login")) { sesid = new Integer(data[2]); if
//		 * (sesid == -1) { log.info("login failed " + data[1]); this.suid = "";
//		 * this.spwd = ""; } else { startServices();
//		 * engine.getParser().subscribe(FeedParser.PARSER_STOCK,
//		 * FeedParser.PARSER_STOCK);
//		 * engine.getParser().subscribe(FeedParser.PARSER_BROKER,
//		 * FeedParser.PARSER_BROKER);
//		 * engine.getParser().subscribe(FeedParser.PARSER_INDICES,
//		 * FeedParser.PARSER_INDICES);
//		 * engine.getParser().subscribe(FeedParser.PARSER_STOCKSUMMARY,
//		 * FeedParser.PARSER_STOCKSUMMARY);
//		 * engine.getParser().subscribe(FeedParser.PARSER_RUNNINGTRADE,
//		 * FeedParser.PARSER_RUNNINGTRADE);
//		 * engine.getParser().subscribe(FeedParser.PARSER_TRADESUMMINV,
//		 * FeedParser.PARSER_TRADESUMMINV); } } else
//		 */if (data[0].equals("logout")) {
//
//			sesid = -1l;
//			this.suid = "";
//			this.spwd = "";
//		} else if (data[0].equals("kill")) {
//			sesid = -1l;
//			this.suid = "";
//			this.spwd = "";
//			log.info("session killed " + data[1]);
//			if (data[1].contains("killed")) {
//				if (timerRefresh.isRunning()) {
//					socketConnector.closeSocket();
//					killed();
//				}
//			}
//		} else if (data[0].equals("heartbeat")) {
//			log.info("heartbeat exception " + data[1]);
//			if (data[1].contains("expired")) {
//				/*if (timerRefresh.isRunning())
//					killed();*/
//				reconnect();
//			}
//		} else if (blockRequest.isBlockingRequest(msg)) {
//			// log.info("blocking response " + msg);
//			blockRequest.receive(msg);
//		} else {
//
//			String[] stemp = msg.split("\n", -2);
//
//			for (int i = 0; i < stemp.length; i++) {
//				if (stemp[i] != null && !stemp[i].isEmpty())
//					addIn(stemp[i]);
//			}
//		}
//	}
//
//	private void killed() {
//		engine.getEventDispatcher().pushEvent(FeedEventDispatcher.EVENT_KILLED,
//				null);
//	}
//
//	@Override
//	public String getDate() throws Exception {
//		// TODO Auto-generated method stub
//		return (String) blockRequest.get(BlockRequestSynchronized.REQUEST_DATE)
//				.sendAndReceive();
//	}
//
//	@Override
//	public Long getSessionid() {
//		// TODO Auto-generated method stub
//		return sesid;
//	}
//
//	@Override
//	public void send(String msg) {
//		socketConnector.sendMessage(Utils.compress(msg.getBytes()));
//	}
//
//	private void reconnect() {
//		timerRefresh.stop();
//		if (sesid != -1)
//			engine.getEventDispatcher().pushEvent(
//					FeedEventDispatcher.EVENT_DISCONNECT, null);
//	}
//
//	@Override
//	public void receive(ClientSocket arg0, Object arg1) {
//		
//	}
//
//}
