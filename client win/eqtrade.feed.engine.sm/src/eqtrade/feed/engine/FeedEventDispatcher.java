package eqtrade.feed.engine;

import java.util.HashMap;

import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eqtrade.feed.core.Event;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.IFeedListener;
import eqtrade.feed.core.Utils;
import eqtrade.feed.model.BrokerRender;
import eqtrade.feed.model.YearRender;

public class FeedEventDispatcher {
	protected final Log log = LogFactory.getLog(getClass());
	public static final int STATE_READY = 0;
	public static final int STATE_NOTREADY = 1;
	public static final int STATE_DISCONNECT = 2;

	public static final Integer EVENT_LOGIN = new Integer(0);
	public static final Integer EVENT_LOGINOK = new Integer(1);
	public static final Integer EVENT_LOGINSTATUS = new Integer(2);
	public static final Integer EVENT_LOGINFAILED = new Integer(3);
	public static final Integer EVENT_RECONNECT = new Integer(4);
	public static final Integer EVENT_RECONNECTOK = new Integer(5);
	public static final Integer EVENT_RECONNECTSTATUS = new Integer(6);
	public static final Integer EVENT_RECONNECTFAILED = new Integer(7);
	public static final Integer EVENT_CHGPASSWD = new Integer(8);
	public static final Integer EVENT_CHGPASSWDOK = new Integer(9);
	public static final Integer EVENT_CHGPASSWDFAILED = new Integer(10);
	public static final Integer EVENT_SUBSCRIBE = new Integer(11);
	public static final Integer EVENT_UNSUBSCRIBE = new Integer(12);
	public static final Integer EVENT_DISCONNECT = new Integer(13);
	public static final Integer EVENT_ONDATA = new Integer(14);
	public static final Integer EVENT_LOGOUT = new Integer(15);
	public static final Integer EVENT_KILLED = new Integer(16);
//	public static final Integer EVENT_FORGOTPWD = new Integer(17);// yosep enforcementpass
//	public static final Integer EVENT_CHGPWD = new Integer(18);// yosep enforcementpass
	public static final Integer EVENT_FALSEPWD = new Integer(19); //yosep other device
	public static final Integer EVENT_SETTIMETRADE = new Integer(20);// yosep fixingTimetrade

	public static final String PARAM_USERID = new String("userid");
	public static final String PARAM_PASSWORD = new String("password");
	public static final String PARAM_OLDPASSWORD = new String("oldpassword");
	public static final String PARAM_CODE = new String("code");
	public static final String PARAM_MESSAGE = new String("message");
	public static final String PARAM_COUNTER = new String("counter");
	public static final String PARAM_HISTORY = new String("history"); // yosep fixingTimetrade

	private IFeedEngine engine;
	private HashMap map = new HashMap(20, 2);

	public FeedEventDispatcher(IFeedEngine engine) {
		this.engine = engine;
		init();
	}

	private void init() {
		map.put(EVENT_LOGIN, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_LOGIN");
				engine.getConnection().logon((String) param.get(PARAM_USERID),
						(String) param.get(PARAM_PASSWORD));
				engine.getConnection().setCountFeedLogin((Integer)param.get(PARAM_COUNTER));// yosep other device
			}
		});
		map.put(EVENT_LOGINOK, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_LOGINOK");
				log.info("delete old data session before: "
						+ FeedSetting.session);
				Utils.deleteFile("data/session", new String[] {
						FeedSetting.session, "3", "4" });
				try {
					String brokerinfo = engine.getConnection().getHistory(
							"BROKER");
					BrokerRender.setBroker(brokerinfo);
				} catch (Exception ex) {
				}
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						String result;
						try {
//							result = engine.getConnection().getHistory("GYPMI" + "|" + null);
//							YearRender.setYear(result);
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						engine.getParser().subscribe(FeedParser.PARSER_SECTOR,
								FeedParser.PARSER_SECTOR);
						engine.getParser().subscribe(FeedParser.PARSER_STOCK,
								FeedParser.PARSER_STOCK);
						engine.getParser().subscribe(FeedParser.PARSER_BROKER,
								FeedParser.PARSER_BROKER);
						engine.getParser().subscribe(FeedParser.PARSER_INDICES,
								FeedParser.PARSER_INDICES);
						engine.getParser().subscribe(
								FeedParser.PARSER_STOCKSUMMARY,
								FeedParser.PARSER_STOCKSUMMARY);
						engine.getParser().subscribe(
								FeedParser.PARSER_RUNNINGTRADE,
								FeedParser.PARSER_RUNNINGTRADE);
						engine.getParser().subscribe(
								FeedParser.PARSER_TRADESUMMINV,
								FeedParser.PARSER_TRADESUMMINV);
					}
				});
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.loginOK();
				}

				try {
					engine.getConnectionQueue().start();
				} catch (Exception ex) {
					ex.printStackTrace();

				}
			}
		});
		map.put(EVENT_LOGINFAILED, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_LOGINFAILED "
						+ param.get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.loginFailed((String) param.get(PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_LOGINSTATUS, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_LOGINSTATUS "
						+ param.get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.loginStatus((String) param.get(PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_RECONNECT, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_RECONNECT");
				engine.getConnection().reconnect(
						((Integer) param.get(PARAM_COUNTER)).intValue());
			}
		});
		map.put(EVENT_RECONNECTOK, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_RECONNECTOK");
				engine.getParser().resubscribe();
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.reconnectOK();
				}
			}
		});
		map.put(EVENT_RECONNECTFAILED, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_RECONNECTFAILED "
						+ param.get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.reconnectFailed((String) param.get(PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_CHGPASSWD, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_CHGPASSWD ");
				engine.getConnection().changePassword(
						(String) param.get(PARAM_OLDPASSWORD),
						(String) param.get(PARAM_PASSWORD));
			}
		});
		map.put(EVENT_CHGPASSWDOK, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_CHGPASSWDOK");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.changePwdOK();
				}
			}
		});
		map.put(EVENT_CHGPASSWDFAILED, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_CHGPASSWDFAILED "
						+ param.get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.changePwdFailed((String) param.get(PARAM_MESSAGE));
				}
			}
		});
		map.put(EVENT_SUBSCRIBE, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_SUBSCRIBE " + param.get(PARAM_CODE));
				engine.getParser().subscribe((String) param.get(PARAM_CODE),
						(String) param.get(PARAM_MESSAGE));
			}
		});
		map.put(EVENT_UNSUBSCRIBE, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_UNSUBSCRIBE " + param.get(PARAM_CODE));
				engine.getParser().unsubscribe((String) param.get(PARAM_CODE),
						(String) param.get(PARAM_MESSAGE));
			}
		});
		map.put(EVENT_DISCONNECT, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_DISCONNECT ");
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.disconnected();
				}
			}
		});
		map.put(EVENT_ONDATA, new Event() {
			public void execute(HashMap param) {
				try {
					engine.getParser().parse((String) param.get(PARAM_MESSAGE));
				} catch (Exception ex) {
					// ex.printStackTrace();
					log.error("error processing event EVENT_ONDATA");
					log.error(Utils.logException(ex));
				}
			}
		});
		map.put(EVENT_LOGOUT, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_LOGOUT");
				engine.getParser().unsubscribe(FeedParser.PARSER_STOCK,
						FeedParser.PARSER_STOCK);
				engine.getParser().unsubscribe(FeedParser.PARSER_BROKER,
						FeedParser.PARSER_BROKER);
				engine.getParser().unsubscribe(FeedParser.PARSER_RUNNINGTRADE,
						FeedParser.PARSER_RUNNINGTRADE);
				engine.getParser().unsubscribe(FeedParser.PARSER_INDICES,
						FeedParser.PARSER_INDICES);
				engine.getParser().unsubscribe(FeedParser.PARSER_STOCKSUMMARY,
						FeedParser.PARSER_STOCKSUMMARY);
				engine.getParser().unsubscribe(FeedParser.PARSER_TRADESUMMINV,
						FeedParser.PARSER_TRADESUMMINV);
				if (param.get(PARAM_COUNTER) != null) {// yosep other device
					engine.getConnection().setCountFeedLogin((Integer)param.get(PARAM_COUNTER));
					engine.getConnection().setuserpass((String)param.get(PARAM_USERID), 
							(String)param.get(PARAM_PASSWORD));
				}
				engine.getConnection().logout();
				try {
					engine.getConnectionQueue().stop();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.info(e.getMessage());
				}
			}
		});
		map.put(EVENT_KILLED, new Event() {
			public void execute(HashMap param) {
				log.info("execute EVENT_KILLED");
				engine.getParser().unsubscribe(FeedParser.PARSER_STOCK,
						FeedParser.PARSER_STOCK);
				engine.getParser().unsubscribe(FeedParser.PARSER_BROKER,
						FeedParser.PARSER_BROKER);
				engine.getParser().unsubscribe(FeedParser.PARSER_RUNNINGTRADE,
						FeedParser.PARSER_RUNNINGTRADE);
				engine.getParser().unsubscribe(FeedParser.PARSER_INDICES,
						FeedParser.PARSER_INDICES);
				engine.getParser().unsubscribe(FeedParser.PARSER_STOCKSUMMARY,
						FeedParser.PARSER_STOCKSUMMARY);
				engine.getConnection().logout();
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.killed();
				}
			}
		});
		// yosep fixingTimetrade
		map.put(EVENT_SETTIMETRADE, new Event() {
			@Override
			public void execute(HashMap param) {
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.finisTimeTrade((Boolean)param.get(PARAM_HISTORY));
				}
			}
		});
		// yosep enforcementpass START
		/*map.put(EVENT_FORGOTPWD, new Event() {
			
			@Override
			public void execute(HashMap param) {
				// TODO Auto-generated method stub
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.forgotPwd();
				}
			}
		});
		map.put(EVENT_CHGPWD, new Event() {
			
			@Override
			public void execute(HashMap param) { 
				log.info("execute EVENT_CHGPWD "+ param.get(PARAM_MESSAGE)+" "+engine.getEventListener().size());
				// TODO Auto-generated method stub
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.changePwd((String)param.get(FeedEventDispatcher.PARAM_MESSAGE));
				}
			}
		});*/
		// yosep enforcementpass END
		// yosep other device
		map.put(EVENT_FALSEPWD, new Event() {
			
			@Override
			public void execute(HashMap param) {
				log.info("execute EVENT_FALSEPWD "
						+ param.get(PARAM_MESSAGE));
				for (int i = 0; i < engine.getEventListener().size(); i++) {
					IFeedListener listener = (IFeedListener) engine
							.getEventListener().elementAt(i);
					listener.falseloginStatus((String) param.get(PARAM_MESSAGE));
				}				
			}
		});

	}

	public void pushEvent(final Integer id, final HashMap param) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				((Event) map.get(id)).execute(param);
			}
		});
	}
}
