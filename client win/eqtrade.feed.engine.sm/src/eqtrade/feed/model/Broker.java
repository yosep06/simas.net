package eqtrade.feed.model;

public final class Broker extends Model  {
	private static final long serialVersionUID = 5417016306669964378L;
	public static final int		CIDX_CODE = 3;
	public static final int		CIDX_NAME = 4;
	public static final int		CIDX_STATUS = 5;
	public static final int 	CIDN_NUMBEROFFIELDS = 6;

	public Broker (){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Broker (String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	protected void convertType() {
	}

	public String getCode(){
		return (String) getData(CIDX_CODE);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_CODE);
	}

	public String getName(){
		return (String) getData(CIDX_NAME);
	}
	public void setName(String sinput){
		setData(sinput, CIDX_NAME);
	}

	public String getStatus(){
		return (String) getData(CIDX_STATUS);
	}
	public void setStatus(String sinput){
		setData(sinput, CIDX_STATUS);
	}
}