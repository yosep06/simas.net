package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public class Statistic extends Model {

	public Statistic(int nsizepar) {
		super(CIDN_NUMBEROFFIELDS);
	}
	
	public Statistic(String smsg) {
		super(CIDN_NUMBEROFFIELDS, smsg);		
	}
	
	private static final long serialVersionUID = 1L;
	public static final int	CIDX_DATE = 0;
    public static final int CIDX_OPEN = 1;
    public static final int	CIDX_HIGH = 2;
    public static final int	CIDX_LOW = 3;
    public static final int CIDX_LASTHIGH = 4;
    public static final int CIDX_LASTLOW = 5;
    public static final int CIDX_CLOSE = 6;   
    public static final int CIDX_PREV = 7; 
    public static final int CIDN_NUMBEROFFIELDS = 8;

	@Override
	protected void convertType() {
		/*for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 1: case 2: case 3:case 4: case 5: case 6: case 7:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;			
			default:
				break;
			}			
		}*/
	}
	
	public String getDate() {
		return (String) getData(CIDX_DATE);		
	}
	public void setDate(String sinput){
		setData(sinput, CIDX_DATE);
	}
	
	public String getOpen() {
		return (String) getData(CIDX_OPEN);
	}
	public void setOpen(String sinput) {
		setData(sinput, CIDX_OPEN);
	}
	
	public String getHigh() {
		return (String) getData(CIDX_HIGH);
	}
	public void setHigh(String sinput) {
		setData(sinput, CIDX_HIGH);
	}
	
	public String getLow() {
		return (String) getData(CIDX_LOW);
	}
	public void setLow(String sinput) {
		setData(sinput, CIDX_LOW);
	}
	
	public String getLastHigh() {
		return (String) getData(CIDX_LASTHIGH);
	}
	public void SetLastHigh(String sinput){
		setData(sinput,CIDX_LASTHIGH);
	}
	
	public String getLastLow() {
		return (String) getData(CIDX_LASTHIGH);
	}
	public void SetLLastLow(String sinput){
		setData(sinput,CIDX_LASTHIGH);
	}
	
	public String getClose() {
		return (String) getData(CIDX_CLOSE);
	}
	public void setClose(String sinput) {
		setData(sinput, CIDX_CLOSE);
	}
	
	public String getPrev() {
		return (String) getData(CIDX_PREV);
	}
	public void setPrev(String sinput) {
		setData(sinput, CIDX_PREV);
	}
}
