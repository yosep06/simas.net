package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class RupsRender extends DefaultTableCellRenderer{
	private static final long serialVersionUID = 213203526059607433L;
    //private String strFieldName = new String("");
    //private int row;
    private static NumberFormat formatter = new DecimalFormat("#,##0  ");
    private static Color newBack;
    private static Color newFore;
    private static SelectedBorder border = new SelectedBorder();
    private String strFieldName = new String("");

	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
        Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        //strFieldName = table.getColumnName(column);
        //this.row = row+1;
        strFieldName = table.getColumnName(column);
        if (value instanceof MutableIData) {	
        
		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
		if (strFieldName.equals(RupsDef.dataHeader[Rups.CIDX_STOCK])){
			newFore = StockRender.getSectorColor(((MutableIData)value).getData().toString()); 
		 } else {
			 newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);           	
		 }
        }
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);  
	 	return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
                 Calendar cal = Calendar.getInstance();
                 
                 String DATE_FORMAT_NOW = "yyyy-MM-dd";
                 Log log = LogFactory.getLog(getClass());
                 
                 Calendar current = Calendar.getInstance();
                 SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                 sdf.format(current.getTime());
                 if (dat instanceof Double) {
	            	 setText( (((Double)dat).doubleValue() == 0) ? "" : formatter.format(dat));
	             }  else {
	            	 if(strFieldName.equals(RupsDef.dataHeader[Rups.CIDX_STOCK])){
	            		 Rups rups=(Rups)args.getSource();
		            	 String tgl=rups.getDate();
		            	// log.info("tgl "+tgl+ " length "+tgl.length());
		            	 Calendar now = Calendar.getInstance();
			           //   log.info("Current date : " + (now.get(Calendar.MONTH) + 1) + "-"
			             //        + now.get(Calendar.DATE) + "-" + now.get(Calendar.YEAR));
			              now.set(Calendar.MONTH, now.get(Calendar.MONTH) + 1);
		            	 if(!tgl.equals("null")) {
		            	//	 log.info("masuk");
		            		 Date dt = new Date();
		            //		 log.info("isi di dalam "+tgl.substring(0,4)+"-"+tgl.substring(5,7)+"-"+tgl.substring(8,10));
		            				            //  log.info("tgl "+Integer.parseInt(tgl.substring(0,4))+"-"+Integer.parseInt(tgl.substring(6,8))+"-"+Integer.parseInt(tgl.substring(8,10)));
			            	 cal.set(Calendar.YEAR, Integer.parseInt(tgl.substring(0,4)));
			            	 cal.set(Calendar.MONTH, Integer.parseInt(tgl.substring(5,7)));
			            	 cal.set(Calendar.DATE,Integer.parseInt(tgl.substring(8,10)));
			           // 	 log.info("cal "+cal);
			           // 	 log.info("Is old before now ? : " + cal.before(now));
			            	 if(cal.before(now)){
			            		// setHorizontalAlignment(SwingConstants.CENTER);
		            			  setText(" "+dat.toString()+"*");
		            		  } else {
		            			  setText(" "+dat.toString());
		            		  }
		            	 }
		                 
	            		 
		                 String val = ((String)dat).toString();
	            		// log.info("isi tgl"+currentcal);
	            		 //log.info("cal "+cal.get(Calendar.DATE)+"-"+cal.get(Calendar.MONTH)+"-"+cal.get(Calendar.YEAR));
	            		 //log.info("current cal "+currentcal.get(Calendar.DATE)+"-"+currentcal.get(Calendar.MONTH)+"-"+currentcal.get(Calendar.YEAR));
	            	//	 log.info("current cal "+currentcal);
	            	//	 log.info("cal "+cal);
	            		 
	            		
	            			
	            			
	            	 } else {
	            	setText(" "+dat.toString());
	            	 }
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
