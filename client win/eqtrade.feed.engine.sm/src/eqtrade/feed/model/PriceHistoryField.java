package eqtrade.feed.model;

import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class PriceHistoryField extends Column implements MutableIDisplayIData{

	public PriceHistoryField(Row source, int idx) throws Exception {
		super(source, idx);		
	}

	@Override
	public ImmutableIDisplay getImmutableIDisplay() {
		return PriceHistoryDef.render;
	}

	@Override
	public MutableIDisplay getMutableIDisplay() {
		 return PriceHistoryDef.render;
	}

}
