package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public final class Market extends Model implements Comparable {
	private static final long serialVersionUID = 7891111031929287100L;
    public static final int		CIDX_BOARD = 0;
    public static final int		CIDX_VALUE = 1;
    public static final int 	CIDX_VOLUME = 2;
    public static final int		CIDX_FREQ = 3;
    public static final int		CIDN_NUMBEROFFIELDS = 4;

	public Market(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Market(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 1: case 2: case 3:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}
    
	public String getBoard(){
		return (String) getData(CIDX_BOARD);
	}
	public void setBoard(String sinput){
		setData(sinput, CIDX_BOARD);
	}

	public Double getValue(){
		return (Double) getData(CIDX_VALUE);
	}
	public void setValue(Double sinput){
		setData(sinput, CIDX_VALUE);
	}

	public Double getVolume(){
		return (Double) getData(CIDX_VOLUME);
	}
	public void setVolume(Double sinput){
		setData(sinput, CIDX_VOLUME);
	}

	public Double getFreq(){
		return (Double) getData(CIDX_FREQ);
	}
	public void setFreq(Double sinput){
		setData(sinput, CIDX_FREQ);
	}

	public int compareTo(Object otemp){
		Market dmtemp = (Market) otemp;
		String stemp = dmtemp.getBoard().toUpperCase();
		String ssrc = getBoard().toUpperCase();
		if (stemp.startsWith("RG")) stemp = "0" + stemp;
        if (stemp.startsWith("TS")) stemp = "1" + stemp;
        if (stemp.startsWith("NG")) stemp = "2" + stemp;
        if (stemp.startsWith("TN")) stemp = "3" + stemp;
        if (stemp.startsWith("TOTAL")) stemp = "4" + stemp;
        if (stemp.startsWith("SPACE")) stemp = "5" + stemp;
        if (stemp.startsWith("FOREIGNBUY")) stemp = "6" + stemp;
        if (stemp.startsWith("FOREIGNSELL")) stemp = "7" + stemp;
        if (stemp.startsWith("FOREIGNNET")) stemp = "8" + stemp;
        
        if (ssrc.startsWith("RG")) ssrc = "0" + ssrc;
        if (ssrc.startsWith("TS")) ssrc = "1" + ssrc;
        if (ssrc.startsWith("NG")) ssrc = "2" + ssrc;
        if (ssrc.startsWith("TN")) ssrc = "3" + ssrc;
        if (ssrc.startsWith("TOTAL")) ssrc = "4" + ssrc;
        if (ssrc.startsWith("SPACE")) ssrc = "5" + ssrc;
        if (ssrc.startsWith("FOREIGNBUY")) ssrc = "6" + ssrc;
        if (ssrc.startsWith("FOREIGNSELL")) ssrc = "7" + ssrc;
        if (ssrc.startsWith("FOREIGNNET")) ssrc = "8" + ssrc;
		return ssrc.compareTo(stemp);
	}

}
