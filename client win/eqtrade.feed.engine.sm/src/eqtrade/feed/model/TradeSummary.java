package eqtrade.feed.model;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;

public final class TradeSummary extends Model{
	private static final long serialVersionUID = 7891111031929287100L;
	//stock|board|broker|investor|buyavg|sellavg|buyvol|buyval|buyfreq|sellvol|sellval|sellfreq
    public static final int		CIDX_STOCK = 3;
    public static final int  	CIDX_BOARD = 4;
    public static final int		CIDX_BROKER = 5;
    public static final int		CIDX_INVESTOR = 6;
    public static final int 	CIDX_BUYAVG = 7;
    public static final int		CIDX_SELLAVG = 8;
    public static final int		CIDX_BUYVOL = 9;
    public static final int		CIDX_BUYVAL = 10;
    public static final int		CIDX_BUYFREQ = 11;
    public static final int		CIDX_SELLVOL = 12;
    public static final int		CIDX_SELLVAL = 13;
    public static final int		CIDX_SELLFREQ = 14;
    
    public static final int		CIDX_BUYLOT = 15;
    public static final int		CIDX_SELLLOT = 16;
    
    public static final int		CIDX_VOLTOT = 17;
    public static final int		CIDX_LOTTOT = 18;
    public static final int		CIDX_VALTOT = 19;
    public static final int		CIDX_FREQTOT = 20;
    public static final int		CIDX_VOLNET = 21;
    public static final int		CIDX_LOTNET = 22;
    public static final int		CIDX_VALNET = 23;
    public static final int		CIDX_STOCKNAME = 24;
    public static final int		CIDX_BROKERNAME = 25;
    public static final int		CIDN_NUMBEROFFIELDS = 26;
    //private static final int	lotsize = 500;
    
    public void calculate(){
    	//System.out.println("LOT : "+FeedSetting.getLot());
    	setBuylot(new Double(getBuyvol().doubleValue() / Double.parseDouble(FeedSetting.getLot())));
    	setSelllot(new Double(getSellvol().doubleValue() / Double.parseDouble(FeedSetting.getLot())));
    	setVoltot(new Double(getBuyvol().doubleValue() + getSellvol().doubleValue()));
    	setLottot(new Double(getBuylot().doubleValue() + getSelllot().doubleValue()));
    	setValtot(new Double(getBuyval().doubleValue() + getSellval().doubleValue()));
    	setFreqtot(new Double(getBuyfreq().doubleValue() + getSellfreq().doubleValue()));
    	setVolnet(new Double(getBuyvol().doubleValue() - getSellvol().doubleValue()));
    	setLotnet(new Double(getBuylot().doubleValue() - getSelllot().doubleValue()));
    	setValnet(new Double(getBuyval().doubleValue() - getSellval().doubleValue()));
    }

	public Double getBuylot(){
		return (Double) getData(CIDX_BUYLOT);
	}
	public void setBuylot(Double sinput){
		setData(sinput, CIDX_BUYLOT);
	}
	public Double getSelllot(){
		return (Double) getData(CIDX_SELLLOT);
	}
	public void setSelllot(Double sinput){
		setData(sinput, CIDX_SELLLOT);
	}
	public Double getVoltot(){
		return (Double) getData(CIDX_VOLTOT);
	}
	public void setVoltot(Double sinput){
		setData(sinput, CIDX_VOLTOT);
	}
	public Double getLottot(){
		return (Double) getData(CIDX_LOTTOT);
	}
	public void setLottot(Double sinput){
		setData(sinput, CIDX_LOTTOT);
	}
	public Double getValtot(){
		return (Double) getData(CIDX_VALTOT);
	}
	public void setValtot(Double sinput){
		setData(sinput, CIDX_VALTOT);
	}
	public Double getFreqtot(){
		return (Double) getData(CIDX_FREQTOT);
	}
	public void setFreqtot(Double sinput){
		setData(sinput, CIDX_FREQTOT);
	}
	public Double getVolnet(){
		return (Double) getData(CIDX_VOLNET);
	}
	public void setVolnet(Double sinput){
		setData(sinput, CIDX_VOLNET);
	}
	public Double getLotnet(){
		return (Double) getData(CIDX_LOTNET);
	}
	public void setLotnet(Double sinput){
		setData(sinput, CIDX_LOTNET);
	}
	public Double getValnet(){
		return (Double) getData(CIDX_VALNET);
	}
	public void setValnet(Double sinput){
		setData(sinput, CIDX_VALNET);
	}

    
	public TradeSummary(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public TradeSummary(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 7: case 8:case 9:case 10: case 11: case 12: case 13: case 14:
			case 15: case 16: case 17: case 18: case 19: case 20: case 21: case 22: case 23:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
		calculate();
	}
    
	public String getStock(){
		return (String) getData(CIDX_STOCK);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_STOCK);
	}
	public String getBrokername(){
		return (String) getData(CIDX_BROKERNAME);
	}
	public void setBrokername(String sinput){
		setData(sinput, CIDX_BROKERNAME);
	}
	public String getStockname(){
		return (String) getData(CIDX_STOCKNAME);
	}
	public void setStockname(String sinput){
		setData(sinput, CIDX_STOCKNAME);
	}
	
	public String getBoard(){
		return (String) getData(CIDX_BOARD);
	}
	public void setBoard(String sinput){
		setData(sinput, CIDX_BOARD);
	}

	public String getBroker(){
		return (String) getData(CIDX_BROKER);
	}
	public void setBroker(String sinput){
		setData(sinput, CIDX_BROKER);
	}

	public String getInvestor(){
		return (String) getData(CIDX_INVESTOR);
	}
	public void setInvestor(String sinput){
		setData(sinput, CIDX_INVESTOR);
	}

	public Double getBuyavg(){
		return (Double) getData(CIDX_BUYAVG);
	}
	public void setBuyavg(Double sinput){
		setData(sinput, CIDX_BUYAVG);
	}

	public Double getSellavg(){
		return (Double) getData(CIDX_SELLAVG);
	}
	
	public void setSellavg(Double sinput){
		setData(sinput, CIDX_SELLAVG);
	}
	
	public Double getBuyvol(){
		return (Double) getData(CIDX_BUYVOL);
	}
	
	public void setBuyvol(Double sinput){
		setData(sinput, CIDX_BUYVOL);
	}

	public Double getBuyval(){
		return (Double) getData(CIDX_BUYVAL);
	}
	
	public void setBuyval(Double sinput){
		setData(sinput, CIDX_BUYVAL);
	}
	public Double getBuyfreq(){
		return (Double) getData(CIDX_BUYFREQ);
	}
	
	public void setBuyfreq(Double sinput){
		setData(sinput, CIDX_BUYFREQ);
	}
	public Double getSellvol(){
		return (Double) getData(CIDX_SELLVOL);
	}
	
	public void setSellvol(Double sinput){
		setData(sinput, CIDX_SELLVOL);
	}

	public Double getSellval(){
		return (Double) getData(CIDX_SELLVAL);
	}
	
	public void setSellval(Double sinput){
		setData(sinput, CIDX_SELLVAL);
	}
	public Double getSellfreq(){
		return (Double) getData(CIDX_SELLFREQ);
	}
	
	public void setSellfreq(Double sinput){
		setData(sinput, CIDX_SELLFREQ);
	}
}
