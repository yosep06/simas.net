package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import sun.util.logging.resources.logging;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class StockSummaryDef {
	public static final Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(StockSummary.CIDX_PERCENT));
        htable.put("sortascending", new Boolean(false));
        return htable;
	}
	public static final Hashtable getTableNGDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrderNG.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(StockSummary.CIDX_CODE));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}
    public static final String[] dataHeader = new String[]{
		"header", "type", "Rank", "Stock", "Board", "Remarks", "Close", "Hi", "Lo", "Last", "Chg",
		"T.Vol", "T.Val", "T.Freq", "Index", "Foreigner", "Open",
		"Bid", "B.Lot", "Offer", "O.Lot", "% Chg", "Avg", "Name", "T.Lot", "Sector"};

    public static final int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER
    };

	public  static final int[] defaultColumnWidth = new int[]{
			80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80
	};
	
	public  static final int[] defaultColumnOrder = new int[]{
		2,3, 23, 16, 7,8,9,10,21,11,12,13, 0, 1,4,5,6,14,15,17,18,19,20,22,24,25
	};	
	
	public static final boolean[] columnsort2 = new boolean[]{
		false, false, false, false, false, false, false, false, false, false, true,
		true, true, true ,false, false, false, 
		false, false, false, false, true, false, false, true,true
	};

	public  static final int[] defaultColumnOrderNG = new int[]{
		2,3, 23, 16, 7,8,9,10,21,11,12,13, 0, 1,4,5,6,14,15,18,17, 19,20,22,24,25
	};	

	public static final boolean[] columnsort = new boolean[]{
			false, true, true,true, true, true,true, true, true,true, true, true,true, true, true, true,true, true, true, true,true,true,true,true,true,true
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(StockSummary.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row ss) {
		List vec = new Vector(StockSummary.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < StockSummary.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new StockSummaryField(ss, i));
				
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static SSRender render = new SSRender();
	
    static class SSRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new StockSummaryRender();

		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((StockSummaryRender)renderer).setValue(value);
			return renderer;
		}
	}
}