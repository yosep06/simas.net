package eqtrade.feed.model;

import java.util.ArrayList;
import java.util.Vector;

public class Queue extends Model {

	
	private static final long serialVersionUID = 1L;
	/*public static final int CIDX_STOCK = 0;
	public static final int CIDX_BOARD = 1;
	public static final int CIDX_TYPE = 2;
	public static final int CIDX_PRICE = 3;
	public static final int CIDX_LOT = 4;
	public static final int CIDX_ORDERID = 5;
	public static final int CIDX_ID = 6;
	public static final int CIDX_STATE=7;
	public static final int CIDN_NUMBEROFFIELDS = 8; */
	 public static final int CIDX_NO = 0;
	  public static final int CIDX_STOCK = 1;
	  public static final int CIDX_BOARD = 2;
	  public static final int CIDX_TYPE = 3;
	  public static final int CIDX_PRICE = 4;
	  public static final int CIDX_LOT = 5;
	  public static final int CIDX_ORDERID = 6;
	  public static final int CIDX_ID = 7;
	  public static final int CIDX_STATE = 8;
	  public static final int CIDN_NUMBEROFFIELDS = 9;
	  
// Store and Retrieve Market Order ID List
	public static ArrayList marketOrderIDList;

	public static ArrayList getMarketOrderIDList() {
		return marketOrderIDList;
	}

	public static void setMarketOrderIDList(ArrayList marketOrderIDList) {
		Queue.marketOrderIDList = marketOrderIDList;
	}
///////////////

	public Queue() {
		super(CIDN_NUMBEROFFIELDS);
	}

	protected void convertType() {
	} 

	public String getCode() {
		return (String) getData(CIDX_STOCK);
	}

	public void setActiontype(String sinput) {
		setData(sinput, CIDX_STOCK);
	}

	public String getBoard() {
		return (String) getData(CIDX_BOARD);
	}

	public void setBoard(String sinput) {
		setData(sinput, CIDX_BOARD);
	}	

	public Double getPrice() {
		return (Double) getData(CIDX_PRICE);
	}

	public void setPrice(Double sinput) {
		setData(sinput, CIDX_PRICE);
	}

	public Double getLot() {
		return (Double) getData(CIDX_LOT);
	}

	public void setLot(Double sinput) {
		setData(sinput, CIDX_LOT);
	}

	public String getOrderid() {
		return (String) getData(CIDX_ORDERID);
	}

	public void setOrderid(String sinput) {
		setData(sinput, CIDX_ORDERID);
	}	
	
	public Integer getId() {
		return (Integer) getData(CIDX_ID);
	}

	public void setId(Integer sinput) {
		setData(sinput, CIDX_ID);
	}	
	
	public void setTypeData(Integer type){
		setData(type, CIDX_TYPE);
	}
	
	public Integer getTypeData() {
		return (Integer) getData(CIDX_TYPE);
	}
	public void setState(String sinput){
		setData(sinput, CIDX_STATE);
	}
	public String getState(){
		return (getData(CIDX_STATE).toString());
	}
	 public Integer getNo() {
		return (Integer)getData(CIDX_NO);
	 }

	public void setNo(Integer sinput) {
		setData(sinput, CIDX_NO);
	}

}
