package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class QuoteRender extends DefaultTableCellRenderer{
	private static final long serialVersionUID = 213203526059607433L;
    private static NumberFormat formatter = new DecimalFormat("#,##0  ");
    private String strFieldName = new String("");

    private static Color newBack;
    private static Color newFore;
    private boolean highlight = false;
    private static SelectedBorder border = new SelectedBorder();

	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
        Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
	 	strFieldName = table.getColumnName(column);
//		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
//        newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
        
	 	if (value instanceof MutableIData){
	 	    MutableIData arg = (MutableIData)value;
	 	    Quote oq = (Quote)arg.getSource();
	 	    double cp = oq.getPrev().doubleValue();	 	    
	  		 if (strFieldName.equals(QuoteDef.dataHeaderBid[Quote.CIDX_FREQ]) || strFieldName.equals(QuoteDef.dataHeaderBid[Quote.CIDX_LOT]) ||
	  				strFieldName.equals(QuoteDef.dataHeaderOff[Quote.CIDX_FREQ]) || strFieldName.equals(QuoteDef.dataHeaderOff[Quote.CIDX_LOT]) || 
	  				strFieldName.equals(QuoteDef.dataHeaderVer[Quote.CIDX_FREQ]) || strFieldName.equals(QuoteDef.dataHeaderVer[Quote.CIDX_LOT])){
		 	        newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
	  		 } else {
			 	    if(oq.getPrice().doubleValue()==cp)
			 	        {newFore = FeedSetting.getColor(FeedSetting.C_ZERO); }
			 	    else if((oq.getPrice().doubleValue())>cp)
			 	        {newFore = FeedSetting.getColor(FeedSetting.C_PLUS);  }
			 	    else if(oq.getPrice().doubleValue()<cp)
			 	        {newFore =  FeedSetting.getColor(FeedSetting.C_MINUS); }
	  		 }	 	    
	 	   if (arg.getData() instanceof String){
			            if (column == 0){
			                newBack = table.getTableHeader().getBackground();
			                newFore = Color.black;
			            } else {
			                newBack = FeedSetting.getColor(FeedSetting.C_BACKGROUND);   
			            }
	 	   } else {
	 		   newBack = FeedSetting.getColor(FeedSetting.C_BACKGROUND);   
	 	   }
            
	 	   if (oq.getNumber().doubleValue() == 0 || oq.getNumber().doubleValue() == 22 || oq.getType().equals("L")){
                newBack = FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);   
                newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
                highlight = true;
            } else {
            	highlight = false;
            }
	 	}
        if (highlight) {
            ((JLabel)component).setBorder(border);
        } else {
            ((JLabel)component).setBorder(null);        	
        }
        component.setBackground(newBack);
        component.setForeground(newFore);  
	 	return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
	             if (dat instanceof Double) {
	            	 setText( (((Double)dat).doubleValue() == 0) ? "" : formatter.format(dat));
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}

//package eqtrade.feed.model;
//
//import java.awt.Color;
//import java.awt.Component;
//import java.text.DecimalFormat;
//import java.text.NumberFormat;
//
//import javax.swing.JLabel;
//import javax.swing.JTable;
//import javax.swing.table.DefaultTableCellRenderer;
//
//import com.vollux.idata.indirection.MutableIData;
//
//import eqtrade.feed.core.FeedSetting;
//import eqtrade.feed.core.SelectedBorder;
//
//public class QuoteRender extends DefaultTableCellRenderer{
//	private static final long serialVersionUID = 213203526059607433L;
//    private static NumberFormat formatter = new DecimalFormat("#,##0  ");
//    private String strFieldName = new String("");
//
//    private static Color newBack;
//    private static Color newFore;
//    private boolean highlight = false;
//    private static SelectedBorder border = new SelectedBorder();
//
//	 public Component getTableCellRendererComponent(JTable table, Object value,
//	 	boolean isSelected, boolean hasFocus, int row, int column){
//        Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
//	 	strFieldName = table.getColumnName(column);
////		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
////        newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
//        
//	 	if (value instanceof MutableIData){
//	 	    MutableIData arg = (MutableIData)value;
//	 	    Quote oq = (Quote)arg.getSource();
//	 	    double cp = oq.getPrev().doubleValue();	 	    
//	  		 if (strFieldName.equals(QuoteDef.dataHeaderBid[Quote.CIDX_FREQ]) || strFieldName.equals(QuoteDef.dataHeaderBid[Quote.CIDX_LOT]) ||
//	  				strFieldName.equals(QuoteDef.dataHeaderOff[Quote.CIDX_FREQ]) || strFieldName.equals(QuoteDef.dataHeaderOff[Quote.CIDX_LOT]) || 
//	  				strFieldName.equals(QuoteDef.dataHeaderVer[Quote.CIDX_FREQ]) || strFieldName.equals(QuoteDef.dataHeaderVer[Quote.CIDX_LOT])){
//		 	        newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
//	  		 } else {
//			 	    if(oq.getPrice().doubleValue()==cp)
//			 	        {newFore = FeedSetting.getColor(FeedSetting.C_ZERO); }
//			 	    else if((oq.getPrice().doubleValue())>cp)
//			 	        {newFore = FeedSetting.getColor(FeedSetting.C_PLUS);  }
//			 	    else if(oq.getPrice().doubleValue()<cp)
//			 	        {newFore =  FeedSetting.getColor(FeedSetting.C_MINUS); }
//	  		 }	 	    
//	 	   if (arg.getData() instanceof String){
//			            if (column == 0){
//			                newBack = table.getTableHeader().getBackground();
//			                newFore = Color.black;
//			            } else {
//			                newBack = FeedSetting.getColor(FeedSetting.C_BACKGROUND);   
//			            }
//	 	   } else {
//	 		   newBack = FeedSetting.getColor(FeedSetting.C_BACKGROUND);   
//	 	   }
//            
//	 	   if (oq.getNumber().doubleValue() == 0 || oq.getNumber().doubleValue() == 22 || oq.getType().equals("L")){
//                newBack = FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);   
//                newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
//                highlight = oq.getLot() == 0 ? false : true;
//                if(!highlight) {
//                	newBack = FeedSetting.getColor(FeedSetting.C_BACKGROUND); 
//                }
//            } else {
//            	highlight = false;
//            }
//	 	}
//        if (highlight) {
//            ((JLabel)component).setBorder(border);
//        } else {
//            ((JLabel)component).setBorder(null);        	
//        }
//        component.setBackground(newBack);
//        component.setForeground(newFore);  
//	 	return component;
//    }
//	
//	 public void setValue(Object value){
//	     try {
//	         if (value instanceof MutableIData) {
//	             MutableIData args = (MutableIData)value;
//	             Object dat = args.getData();
//                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
//	             if (dat instanceof Double) {
//	            	 setText( (((Double)dat).doubleValue() == 0) ? "" : formatter.format(dat));
//	             } else if (dat == null){
//	                 setText("");
//	             } else {
//	            	 setText(" "+dat.toString());
//	             }
//	         }
//	     } catch (Exception e){
//	     }
//	 }
//}
