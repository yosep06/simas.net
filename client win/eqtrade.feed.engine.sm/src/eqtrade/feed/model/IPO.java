package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public final class IPO extends Model  {
	private static final long serialVersionUID = 5417016306669964378L;
	public static final int		CIDX_NAME = 3;
	public static final int		CIDX_STOCK = 4;
	public static final int		CIDX_NOMINAL = 5;
	public static final int		CIDX_TOTALSHARE = 6;
	public static final int		CIDX_PERCENTSHARE = 7;
	public static final int		CIDX_EFFDATE = 8;
	public static final int		CIDX_OFFERSTART = 9;
	public static final int		CIDX_OFFEREND = 10;
	public static final int		CIDX_ALLOTMENT = 11;
	public static final int 	CIDX_REFUNDDATE = 12;
	public static final int 	CIDX_LISTINGDATE = 13;
	public static final int 	CIDN_NUMBEROFFIELDS = 14;
	
	
	

	public IPO (){
		super(CIDN_NUMBEROFFIELDS);
	}

	public IPO (String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 5: case 6: 
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}	}
	
	public String getEffdate(){
		return (String) getData(CIDX_EFFDATE);
	}
	public void setEffdate(String sinput){
		setData(sinput, CIDX_EFFDATE);
	}
	public String getOfferstart(){
		return (String) getData(CIDX_OFFERSTART);
	}
	public void setOfferstart(String sinput){
		setData(sinput, CIDX_OFFERSTART);
	}
	public String getOfferend(){
		return (String) getData(CIDX_OFFEREND);
	}
	public void setOfferend(String sinput){
		setData(sinput, CIDX_OFFEREND);
	}
	public String getAllotment(){
		return (String) getData(CIDX_ALLOTMENT);
	}
	public void setAllotment(String sinput){
		setData(sinput, CIDX_ALLOTMENT);
	}
	public String getRefunddate(){
		return (String) getData(CIDX_REFUNDDATE);
	}
	public void setRefunddate(String sinput){
		setData(sinput, CIDX_REFUNDDATE);
	}

	public String getListingdate(){
		return (String) getData(CIDX_LISTINGDATE);
	}
	public void setListingdate(String sinput){
		setData(sinput, CIDX_LISTINGDATE);
	}

	public String getName(){
		return (String) getData(CIDX_NAME);
	}
	public void setName(String sinput){
		setData(sinput, CIDX_NAME);
	}

	public String getStock(){
		return (String) getData(CIDX_STOCK);
	}
	
	public void setStock(String sinput){
		setData(sinput, CIDX_STOCK);
	}

	public Double getNominal(){
		return (Double) getData(CIDX_NOMINAL);
	}
	public void setNominal(Double sinput){
		setData(sinput, CIDX_NOMINAL);
	}
	public Double getTotalshare(){
		return (Double) getData(CIDX_TOTALSHARE);
	}
	public void setTotalshare(Double sinput){
		setData(sinput, CIDX_TOTALSHARE);
	}
	public Double getPercentshare(){
		return (Double) getData(CIDX_PERCENTSHARE);
	}
	public void setPercentshare(Double sinput){
		setData(sinput, CIDX_PERCENTSHARE);
	}
	
}