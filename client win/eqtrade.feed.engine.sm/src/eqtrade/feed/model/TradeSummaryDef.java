package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class TradeSummaryDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(TradeSummary.CIDX_STOCK));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}
	
	public static Hashtable getTableHisDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthHis.clone());
        htable.put("order", defaultColumnOrderHis.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(TradeSummary.CIDX_STOCK));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}	
	
	public static Hashtable getTableStockAcHeaderDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthHis.clone());
        htable.put("order", defaultColumnOrderStockAcHeader.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(TradeSummary.CIDX_STOCK));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}	
	
	public static Hashtable getTableStockAcDetailDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthHis.clone());
        htable.put("order", defaultColumnOrderStockAcDetail.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(TradeSummary.CIDX_STOCK));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}	
	
	public static Hashtable getTableBrokerAcDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthHis.clone());
        htable.put("order", defaultColumnOrderBrokerAc.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(TradeSummary.CIDX_VALNET));
        htable.put("sortascending", new Boolean(false));
        return htable;
	}
	
	public static Hashtable getTableBrokerAcDefKanan()
	{
		Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthHis.clone());
        htable.put("order", defaultColumnOrderBrokerAcKanan.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(TradeSummary.CIDX_VALNET));
        htable.put("sortascending", new Boolean(true));
        return htable;
		
	}
	

	public static Hashtable getTableTopBrokerDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthHis.clone());
        htable.put("order", defaultColumnOrderTopBroker.clone());
        htable.put("sorted", columnsort2.clone());        
        htable.put("sortcolumn",new Integer(TradeSummary.CIDX_VALTOT));
        htable.put("sortascending", new Boolean(false));
        return htable;
	}	
	
	public static Hashtable getTableBrokerSummDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthHis.clone());
        htable.put("order", defaultColumnOrderBrokerSumm.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(TradeSummary.CIDX_BROKER));
        htable.put("sortascending", new Boolean(false));
        return htable;
	}	
	public static Hashtable getTableInvDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthHis.clone());
        htable.put("order", defaultColumnOrderInv.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(TradeSummary.CIDX_INVESTOR));
        htable.put("sortascending", new Boolean(false));
        return htable;
	}	
	
	public static Hashtable getTableMarketInvDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthHis.clone());
        htable.put("order", defaultColumnOrderMarketInv.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(TradeSummary.CIDX_INVESTOR));
        htable.put("sortascending", new Boolean(false));
        return htable;
	}	

	public static String[] dataHeader = new String[]{
		"header", "type", "Rank", "Stock", "Board", "Broker ID","Investor",  "Buy Avg", "Sell Avg", "Buy Vol", "Buy Val", 
		"Buy Freq", "Sell Vol", "Sell Val", "Sell Freq", "Buy Lot", "Sell Lot", "Total Vol", "Total Lot", "Total Val", 
		"Total Freq", "Net Vol", "Net Lot", "Net Val", "StockName", "BrokerName"};

	public  static int[] defaultColumnOrderInv = new int[]{
		6,23,10,13,19,21,9,12,18,11,14,
		0,1,2,3,4,5,7,8,15,16,18,20,22,24,25
	};	
	public  static int[] defaultColumnOrderMarketInv = new int[]{
		3,6,23,10,13,19,21,9,12,18,11,14,
		0,1,2,4,5,7,8,15,16,18,20,22,24,25
	};	
	public  static int[] defaultColumnOrderBrokerAc = new int[]{
//		5,25,10,15,11,7,13,17,14,8,23,22,
//		0,1,2,3,4,6,9,12,16,18,19,20,21,24
		0,1,2,3,4,5,25,10,7,11,15,6,8,9,12,13,14,16,17,18,19,20,21,23,22,24
	};
	public  static int[] defaultColumnOrderBrokerAcKanan = new int[]{
		0,1,2,3,4,5,25,10,7,11,15,13,8,14,16,6,9,12,17,18,19,20,21,23,22,24
	};
	public  static int[] defaultColumnOrderBrokerSumm = new int[]{
		5,25,15,10,7,16,13,8,22,23,
		0,1,2,3,4,6,9,11,12,14,17,18,19,20,21,24
	};	
	public  static int[] defaultColumnOrderTopBroker = new int[]{
		2,5,25,10,13,23,19,17,20,0,1,3,4,6,7,8,9,11,12,14,15,16,18,21,22,24
	};	

	public static boolean[] columnsort2 = new boolean[]{
		false, false, false, false, false, false,false,true,true,true,true,
		true,true,true,true,true,true,true,true,true,
		true,true,true,true,false,false
	};
	public  static int[] defaultColumnOrderStockAcHeader = new int[]{
		5, 25,10,13,19,15,16,18,11,14,20,
		0,1,2,3,4,6,7,8,9,12,17,21,22,23,24
	};	
	public  static int[] defaultColumnOrderStockAcDetail = new int[]{
		3,24,10,15,7,11,13,16,8,14,23,22,
		0,1,2,4,5,6,9,12,17,18,19,20,21,25
	};	
	public  static int[] defaultColumnOrderHis = new int[]{
		3,24,15,10,7,16,13,8, 22,23,
		0,1,2,4,5,6,9,11,12,14,17,18,19,20,21,25
	};	
	public  static int[] defaultColumnWidthHis = new int[]{
		100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100
	};
	
    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER,SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			0,0,0,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,24,4,25,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23
	};	

	public static boolean[] columnsort = new boolean[]{
			true,true,true,true,true,true,true,true, true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true
	};
	
	public static Vector getHeader() {
		Vector header = new Vector(TradeSummary.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row tradersumm) {
		List vec = new Vector(TradeSummary.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < TradeSummary.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new TradeSummaryField(tradersumm, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static TSUMMRender render = new TSUMMRender();
	
    static class TSUMMRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new TradeSummaryRender();

		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((TradeSummaryRender)renderer).setValue(value);
			return renderer;
		}
	}
}
