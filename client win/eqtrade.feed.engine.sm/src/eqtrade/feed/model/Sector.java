package eqtrade.feed.model;


public final class Sector extends Model {

	private static final long serialVersionUID = 447317705639645109L;
	public static final int CIDX_SECTORCODE=3;
	public static final int CIDX_SECTORNAME=4;
	public static final int CIDN_NUMBEROFFIELDS= 5 ;
	
	public Sector(){
		super(CIDN_NUMBEROFFIELDS);
	}
	public Sector(String smsg) {
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	@Override
	protected void convertType() {
		
	}
	public Double getCIDX_SECTORCODE() {
		return  Double.parseDouble((String) getData(CIDX_SECTORCODE));
	}
	public void setCIDX_SECTORCODE(String cIDX_SECTORCODE) {
		setData(cIDX_SECTORCODE,CIDX_SECTORCODE);
	}
	public String getCIDX_SECTORNAME() {
		return (String)getData(CIDX_SECTORNAME);
	}
	public void setCIDX_SECTORNAME(String cIDX_SECTORNAME) {
		setData(cIDX_SECTORNAME,CIDX_SECTORNAME );
	}
}