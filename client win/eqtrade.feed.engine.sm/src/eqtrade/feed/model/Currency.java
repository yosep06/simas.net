package eqtrade.feed.model;

import eqtrade.feed.core.Utils;


public final class Currency extends Model {
	private static final long serialVersionUID = 1681132210428041847L;
	public static final int CIDX_CODE = 3;
	public static final int	CIDX_TIME = 4;
	public static final int	CIDX_VALUE = 5; 
    public static final int	CIDX_CHANGE = 6;
    public static final int	CIDX_HIGH = 7; 
    public static final int	CIDX_LOW = 8;
    public static final int	CIDX_TIPE = 9;
    public static final int CIDN_NUMBEROFFIELDS = 10;
    
	public Currency(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Currency(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 5: case 6: case 7: case 8:  
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;	
			
			default:
				break;
			}
		}
	}
    
    public Double getValue(){
        return (Double) getData(CIDX_VALUE);
    }
    public void setValue(Double sinput){
        setData(sinput, CIDX_VALUE);
    }
    
    public Double getChange(){
		return (Double) getData(CIDX_CHANGE);
	}
	public void setChange(String sinput){
		setData(sinput, CIDX_CHANGE);
	}
	
	public String getCode(){
		return (String) getData(CIDX_CODE);
	}
	public void setCode(String sinput){
		setData(sinput, CIDX_CODE);
	}
	 public Double getHigh(){
			return (Double) getData(CIDX_HIGH);
	}
	public void setHigh(String sinput){
			setData(sinput, CIDX_HIGH);
	}
	public Double getLow(){
		return (Double) getData(CIDX_LOW);
	}
	public void setLow(String sinput){
		setData(sinput, CIDX_LOW);
	}
	public String getTime1(){
		return (String) getData(CIDX_TIME);
	}
	public void setTime1(String sinput){
		setData(sinput, CIDX_TIME);
	}
	public String getTipe(){
		return (String) getData(CIDX_TIPE);
	}
	public void setTipe(String sinput){
		setData(sinput, CIDX_TIPE);
	}
}