package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class IndicesRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     private String strFieldName = new String("");
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	 private static NumberFormat formatter2 = new DecimalFormat("#,##0.000  ");
     private static Color newBack;
     private static Color newFore;
     private static SelectedBorder border = new SelectedBorder();
     


	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){

	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        strFieldName = table.getColumnName(column);
        if (value instanceof MutableIData) {
			newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
            newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
            Indices in = (Indices)((MutableIData)value).getSource();
	       	 if (
	        		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_LAST]) ||
	        		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_CHANGE]) ||
	        		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_OPEN]) ||
	        		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_HIGH]) ||
	        		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_LOW]) ||
	        		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_PERCENT])
	        		 ){
		            if (in.getChange().doubleValue()>0) {
		                newFore = FeedSetting.getColor(FeedSetting.C_PLUS);   
		            } else if (in.getChange().doubleValue()<0) {
		                newFore = FeedSetting.getColor(FeedSetting.C_MINUS);   
		            } else {
		                newFore = FeedSetting.getColor(FeedSetting.C_ZERO);                    
		            }
	       	 } else if (strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_CODE])){
	     	 	if (in.getCode().startsWith("AGRI")){
	     	 		newFore  = FeedSetting.getColor(FeedSetting.C_AGRICULTURE);      
	    	 	} else if (in.getCode().startsWith("MINING")){
	    	 		newFore  = FeedSetting.getColor(FeedSetting.C_MINING);      
	    	 	} else if (in.getCode().startsWith("BASIC")){
	    	 		newFore  = FeedSetting.getColor(FeedSetting.C_BASICINDUSTRY);      
	    	 	} else if (in.getCode().startsWith("MISC")){
	    	 		newFore  = FeedSetting.getColor(FeedSetting.C_MISCELLANEOUS);      
	    	 	} else if (in.getCode().startsWith("CONSUMER")){
	    	 		newFore  = FeedSetting.getColor(FeedSetting.C_CONSUMER);      
	    	 	} else if (in.getCode().startsWith("PROPERTY")){
	    	 		newFore  = FeedSetting.getColor(FeedSetting.C_PROPERTY);      
	    	 	} else if (in.getCode().startsWith("INFRA")){
	    	 		newFore  = FeedSetting.getColor(FeedSetting.C_INFRASTRUCTURE);      
	    	 	} else if (in.getCode().startsWith("FINANCE")){
	    	 		newFore  = FeedSetting.getColor(FeedSetting.C_FINANCE);      
	    	 	} else if (in.getCode().startsWith("TRADE")){
	    	 		newFore  = FeedSetting.getColor(FeedSetting.C_TRADE);      
	    	 	}  else {
		       		 newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   	    	 		
	    	 	}
	       	 } else {
	       		 newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
	       	 }

        }
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);           
        
	 	return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
	             if (dat instanceof Double) {
	            	 if (strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_LAST]) ||
	            		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_PREV]) ||
	            		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_CHANGE]) ||
	            		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_OPEN]) ||
	            		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_HIGH]) ||
	            		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_LOW]) ||
	            		 strFieldName.equals(IndicesDef.dataHeader[Indices.CIDX_PERCENT])
	            		 ){
	            		 setText(formatter2.format(dat));
	            	 } else {
	            		 setText(formatter.format(dat));
	            	 }
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
