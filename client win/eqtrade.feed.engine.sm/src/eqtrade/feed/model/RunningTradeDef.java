package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;
public final class RunningTradeDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(-1));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}
//	time|stock|board|price|lot|buyer|buyertype|seller|sellertype|
//	bestbid|bestbidlot|bestoffer|bestofferlot|tradeno|previous|chg|%

    public static String[] dataHeader = new String[]{
    	"header", "type", "seqno", "Time", "Stock", "Board", "Price", "Lot", "Buyer", "Buy", "Seller", "Sell",
    	"Best.Bid", "Bid.Lot", "Best.Off", "Off.Lot", "Market Order ID", "Prev", "Chg", "%"};

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER,SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
		//0,70,100,70,100,50,100,75,75,100,50,50,50,50,0,0,0,0,100
	 // 0, 0, 100,70,100,50,100,75,75,100,50,50,50,50,0,0,0,0,100
		0,70,100,70,100,50,100,75,75,100,50,50,50,50,0,0,0,0,100,0
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6,18,19,7,9,8,10,11,12,13,14,15,16,17
	};	

	public static boolean[] columnsort = new boolean[]{
			false,false,false,false,false,false,false,false,false,false,false,false,false,false,
			false,false,false,false,false,false
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(RunningTrade.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row trade) {
		List vec = new Vector(RunningTrade.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < RunningTrade.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new RunningTradeField(trade, i));
				//System.out.println("Board"+vec);
			} catch (Exception e) {
			}
		}
		return vec;
	}
    
    public static boolean simpleRunningTrade = false;
    public static void setSimpleRunningTrade(boolean iya) {
    	simpleRunningTrade = iya;
    }
	
	public final static RTRender render = new RTRender();
	
    static class RTRender extends MutableIDisplayAdapter{
    	RunningTradeRender renderer = new RunningTradeRender();

		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			if (simpleRunningTrade) {
				renderer.getTableCellRendererComponentSimple(table, value,isSelected, hasFocus, row, column);
			} else {
				renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			}
			//((RunningTradeRender)renderer).setValue(value);
			renderer.setValue(value);
			return (DefaultTableCellRenderer)renderer;
		}
	}
}
