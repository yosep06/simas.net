package eqtrade.feed.model;

import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class CorpActionField extends Column  implements MutableIDisplayIData {
    public CorpActionField(Row source, int idx) throws Exception{
        super(source, idx);
    }
    
    public MutableIDisplay getMutableIDisplay() {
		return CorpActionDef.render;
	}

	public ImmutableIDisplay getImmutableIDisplay() {
		return CorpActionDef.render;
	}
}
