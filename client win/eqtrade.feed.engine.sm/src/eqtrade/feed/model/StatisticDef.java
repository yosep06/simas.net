package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;



public class StatisticDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(100));
        htable.put("sortascending", new Boolean(false));
        return htable;
	}
	public static String[] dataHeader = new String[]{
		"Date","Open","High","Low","Last High","Last Low","Close","Prev"};

   public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER
   };

	public  static int[] defaultColumnWidth = new int[]{
			50,50,50,50,50,50,50,0
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6,7
	};	

	public static boolean[] columnsort = new boolean[]{
			false, false, false,false, false, false,false,false
	};
	
   public static Vector getHeader() {
		Vector header = new Vector(Statistic.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
   public static List createTableRow(Row statistic) {
		List vec = new Vector(Statistic.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Statistic.CIDN_NUMBEROFFIELDS; i++) {
			
			try {
				vec.add(i, new StatisticField(statistic, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static TRRender render = new TRRender();
	
   static class TRRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new StatisticRender();

		public TableCellRenderer getTableCellRenderer(JTable tabelStatistic, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(tabelStatistic, value,isSelected, hasFocus, row, column);
			((StatisticRender)renderer).setValue(value);
			return renderer;
		}
	}
}
