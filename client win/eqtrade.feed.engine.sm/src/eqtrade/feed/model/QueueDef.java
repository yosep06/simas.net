package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public class QueueDef {
	public static Hashtable getTableDef() {
		Hashtable htable = new Hashtable();
		htable.put("header", dataHeaderBid);
		htable.put("alignment", defaultHeaderAlignment);
		htable.put("width", defaultColumnWidthBid);
		htable.put("order", defaultColumnOrderBid);
		htable.put("sorted", columnsort);
		htable.put("sortcolumn", new Integer(Queue.CIDX_NO));
		htable.put("sortascending", new Boolean(true));
		return htable;
	}

	// stock|board|side|number|price|lot|freq|prev
	public static String[] dataHeaderBid = new String[] { "No", "Stock",
			"Board", "Type", "Price", "Lot", "MarketOrderID", "ID", "State" };

	public static int[] defaultHeaderAlignment = new int[] {
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER };

	public static int[] defaultColumnWidthBid = new int[] { 100, 100, 100, 100,
			100, 100, 100, 0, 0 };
	public static int[] defaultColumnOrderBid = new int[] { 0, 1, 2, 3, 4, 5,
			6, 7, 8 };

	public static boolean[] columnsort = new boolean[] { false, false, false,
			false, false, false, false, false, false };

	public static Vector getHeader() {
		Vector header = new Vector(Queue.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeaderBid.length; i++) {
			header.addElement(dataHeaderBid[i]);
		}
		return header;
	}

	public static List createTableRow(Row queue) {
		List vec = new Vector(Queue.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Queue.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new QueueField(queue, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}

	public final static OQRender render = new OQRender();

	static class OQRender extends MutableIDisplayAdapter {
		DefaultTableCellRenderer renderer = new QueueRender();

		public TableCellRenderer getTableCellRenderer(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			renderer.getTableCellRendererComponent(table, value, isSelected,
					hasFocus, row, column);
			((QueueRender) renderer).setValue(value);
			return renderer;
		}
	}
}
