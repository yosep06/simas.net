package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Sector;
import eqtrade.feed.model.SectorDef;
import eqtrade.feed.model.Subscriber;

public class SectorParser extends Parser{

	public SectorParser(IFeedEngine engine) {
		super(engine);
		setName(this.getClass().getName());
	}

	@Override
	public void process(Vector vdata) {
		
		if(vdata.size()>0){
			Sector sc;
			Vector vRow;
			for(int i=0; i<vdata.size();i++){
				String source = (String)vdata.elementAt(i);
				sc= new Sector(source);
//				System.out.println("parser sector "+source);
				vRow = new Vector();
				vRow.addElement(SectorDef.createTableRow(sc));
				engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
				engine.getDataStore().get(getModelID2()).addRow(vRow, false, false);
				((Subscriber) hashSubscriber.get(sc.getType())).setSeqno((int)sc.getSeqno());///////.....
				refreshListener(sc);
			}
				
		}
		
	}
	protected Integer getModelID() {
		return FeedStore.DATA_SECTOR;
	}
	
	protected Integer getModelID2() {
		return FeedStore.DATA_SECTORNAME;
	}
	
	protected List createTableRow(Row dat) {
		return SectorDef.createTableRow(dat);
	}
	
}
