package eqtrade.feed.parser;

import java.awt.Robot;
import java.util.Vector;

import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.eqtrade.ClientSocket;
import com.eqtrade.Receiver;
import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;
import com.vollux.idata.Column;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Queue;
import eqtrade.feed.model.QueueDef;
import eqtrade.feed.model.Subscriber;

public class OrderParser extends Parser {
	private Log log = LogFactory.getLog(getClass());
	private String activeState = "kuning";
	private int num = 0;
	private String bs = "";

	// private SocketInterface socketQueue;

	public OrderParser(IFeedEngine engine) throws Exception {
		super(engine);
	}

	@Override
	public void process(Vector vdata) {

		log.info("data queue " + vdata.size() + ":" + vdata.toString());

		for (int i = 0; i < vdata.size(); i++) {
			String data = (String) vdata.elementAt(i);
			// log.info("data "+data);

			String[] header = data.split("\\!");
			String[] sp = header[1].split("\\|");

			if (sp.length > 0) {

				int defno = engine.getDataStore().get(getModelID())
						.getRowCount();

				int nourut = Integer.parseInt(sp[3]);
				Double price = Double.parseDouble(sp[5]);
				Double lot = Double.parseDouble(sp[6]);
				String action = sp[2].trim();

				Queue q = new Queue();
				q.setActiontype(sp[0].trim());
				q.setBoard(sp[1].trim());

				// Queue qb = (Queue) engine.getDataStore().get(getModelID())
				// .getDataByKey(new String[] { q.getOrderid() });

				// if (qb == null) {

				// q.setNo(engine.getDataStore().get(getModelID())
				// .getRowCount());
				// }

				q.setOrderid(sp[4].trim());
				q.setPrice(price);
				q.setLot(lot);

				if (action.equals("0") || action.equals("1")
						|| action.equals("4") || action.equals("5")) {

					if (action.equals("0") || action.equals("1")) {
						q.setNo(engine.getDataStore().get(getModelID())
								.getRowCount() + 1);
					} else {
						Queue q2 = (Queue) engine.getDataStore()
								.get(getModelID())
								.getDataByKey(new String[] { q.getOrderid() });
						if (q2 != null)
							q.setNo(q2.getNo());
					}

					Vector v = new Vector();
					v.add(QueueDef.createTableRow(q));
					engine.getDataStore().get(getModelID())
							.addRow(v, false, false);

				} else if (action.equals("2") || action.equals("3")) {
					int idx = engine.getDataStore().get(getModelID())
							.getIndexByKey(new String[] { q.getOrderid() });
					if (idx >= 0) {
						engine.getDataStore().get(getModelID()).deleteRow(idx);
						changeNoQueue();
					}
				}

				log.info("action " + action + "|" + q.getCode() + "|"
						+ q.getBoard() + "|" + q.getPrice() + "|"
						+ q.getOrderid() + "|" + q.getLot() + "|" + q.getNo());

				engine.getDataStore().get(getModelID()).refresh();
				log.info("size of data "
						+ engine.getDataStore().get(getModelID()).getRowCount());
			}

		}

		/*
		 * for (int i = 0; i < vdata.size(); i++) {
		 * 
		 * String data = (String) vdata.elementAt(i); //
		 * log.info("data order "+data); String[] sp = data.split("\\|"); Queue
		 * q = new Queue(); q.setActiontype(sp[1]); q.setBoard(sp[2]);
		 * q.setTypeData(Integer.parseInt(sp[3])); q.setOrderid(sp[4]);
		 * q.setPrice(Double.parseDouble(sp[5]));
		 * q.setLot(Double.parseDouble(sp[6]));
		 * q.setId(Integer.parseInt(sp[7])); q.setState(activeState);
		 * 
		 * //log.info("Queue Order"+q.get); if (q.getTypeData() == 0 ||
		 * q.getTypeData() == 1) { if(!bs.equalsIgnoreCase(data)){ num+=1;
		 * bs=data; } q.setNo((num)); Vector v = new Vector();
		 * v.add(QueueDef.createTableRow(q));
		 * engine.getDataStore().get(getModelID()).addRow(v, false, false); }
		 * else if (q.getTypeData() == 5 || q.getTypeData() == 6) { // delete
		 * all order by price deleteAllorderByPrice(q); } else {
		 * log.info("delete order " + q.getTypeData() + " " + q.getOrderid() +
		 * " " + q.getLot() + " " + q.getPrice() + " " + q.getId());
		 * deleteOrder(q); }
		 * 
		 * }
		 */
	}

	private void changeNoQueue() {
		int count = engine.getDataStore().get(getModelID()).getRowCount();
		for (int i = 0; i < count; i++) {
			Queue q = (Queue) engine.getDataStore().get(getModelID())
					.getDataByIndex(i);
			q.setNo(new Integer(i + 1));
		}
		engine.getDataStore().get(getModelID()).refresh();
	}

	private void deleteAllorderByPrice(Queue q) {
		Vector vAll = engine.getDataStore().get(getModelID()).getDataVector();
		Queue order;
		// int idx = -1;
		Vector<Integer> vdel = new Vector<Integer>();
		for (int i = 0; i < vAll.size(); i++) {
			order = (Queue) ((Column) ((Vector) vAll.elementAt(i)).elementAt(0))
					.getSource();
			if (q.getCode().equals(order.getCode())
					&& q.getBoard().equals(order.getBoard())
					&& q.getPrice() == order.getPrice()) {
				// engine.getDataStore().get(getModelID()).de
				// idx = i;
				vdel.add(i);
				break;
			}
		}

		log.info("idx delete " + vdel.size());
		if (vdel.isEmpty()) {
			for (int idx : vdel)
				engine.getDataStore().get(getModelID()).deleteRow(idx);
		}
	}

	public void deleteOrder(Queue q) {
		Vector vAll = engine.getDataStore().get(getModelID()).getDataVector();
		Queue order;
		int idx = -1;
		for (int i = 0; i < vAll.size(); i++) {
			order = (Queue) ((Column) ((Vector) vAll.elementAt(i)).elementAt(0))
					.getSource();
			if (q.getOrderid().equals(order.getOrderid())) {
				// engine.getDataStore().get(getModelID()).de
				idx = i;
				break;
			}
		}

		log.info("idx delete " + idx);
		if (idx != -1)
			engine.getDataStore().get(getModelID()).deleteRow(idx);
	}

	// queue-receiver|sub|type|stock|board|price
	public void subscribe(String msg) {
		StringBuffer sb = new StringBuffer();
		sb.append("queue-receiver|sub|").append(msg);
		log.info("sub " + sb.toString());

		// socketQueue.sendMessage(sb.toString());
		engine.getConnectionQueue().subscribe(sb.toString());

	}

	/*
	 * @Override public void subscribe(String msg) { System.out.println(" -- " +
	 * msg.length()); System.out.println(" MSG OREDER " + msg); // String sms =
	 * msg.split("\\|")[1]; log.info("subscribe to : " + msg);
	 * System.out.println(); String[] states = msg.split("#"); try { activeState
	 * = states[4]; System.out.println("Aktif=" + activeState); } catch
	 * (Exception e) {
	 * 
	 * } String msg2 = ""; StringBuilder sb = new StringBuilder(); int i; for (i
	 * = 0; i < states.length - 2; i++) sb.append(states[i] + "#"); msg2 =
	 * sb.toString() + states[i]; bs = states[2]; num = 0;
	 * 
	 * Subscriber r = (Subscriber) hashSubscriber.get(msg2); if (r == null) { r
	 * = genRequest(msg2); r.addSubscriber(); hashSubscriber.put(msg2, r);
	 * sendRequest(r); } else { r.addSubscriber(); if (r.getSubscriber() == 1) {
	 * if (removeAfterUnsubscribe()) r.setSeqno(loadModel(msg2));
	 * sendRequest(r); } } }
	 */

	public void unsubscribe(String msg) {
		/*
		 * String[] states = msg.split("#"); try { activeState = states[4]; }
		 * catch (Exception localException) { } String msg2 = ""; StringBuilder
		 * sb = new StringBuilder(); int i; for (i = 0; i < states.length - 2;
		 * i++) sb.append(states[i] + "#"); msg2 = sb.toString() + states[i];
		 * System.out.println("unsubscribe order " + msg2);
		 * super.unsubscribe(msg2);
		 */
		StringBuffer sb = new StringBuffer();
		sb.append("queue-receiver|unsub|").append(msg);
		log.info("unsub " + sb.toString());

		// socketQueue.sendMessage(sb.toString());
		engine.getConnectionQueue().unsubscribe(sb.toString());

	}

	@Override
	protected Integer getModelID() {
		return FeedStore.DATA_QUOTE_QUEUE;
	}

	public void receive(String msg) {
		String[] sp = msg.split(">");
		final Vector v = new Vector();
		for (String ss : sp) {
			if (!ss.isEmpty())
				v.add(ss);
		}
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				process(v);

			}
		});

	}

}
