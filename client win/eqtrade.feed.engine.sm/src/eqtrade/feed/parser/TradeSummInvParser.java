package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vollux.idata.Row;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Market;
import eqtrade.feed.model.MarketDef;
import eqtrade.feed.model.Subscriber;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;


public final class TradeSummInvParser  extends Parser {

    public TradeSummInvParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}
    
	public void process(Vector vdata){
        if (vdata.size() > 0){
    		TradeSummary trade;
			Vector vRow;
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			trade = new TradeSummary(source);
    			vRow = new Vector(1);
                vRow.addElement(createTableRow(trade));
                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                ((Subscriber)hashSubscriber.get(trade.getType())).setSeqno((int)trade.getSeqno());
                genInv();
                refreshListener(trade);
    		}
        }		
	}
	
    protected void genInv(){
    	TradeSummary F = (TradeSummary)engine.getDataStore().get(getModelID()).getDataByKey(new Object[]{"F"});
    	TradeSummary D = (TradeSummary)engine.getDataStore().get(getModelID()).getDataByKey(new Object[]{"D"});
    	if (F!=null && D!=null){
            Market marketBuy = new Market();
            marketBuy.setBoard("FOREIGNBUY");
            marketBuy.setValue(new Double(F.getBuyval().doubleValue()));
            marketBuy.setVolume(new Double(F.getBuyvol().doubleValue()));
            marketBuy.setFreq(new Double(F.getBuyfreq().doubleValue()));
           Vector vRow = new Vector(1);
            vRow.addElement(MarketDef.createTableRow(marketBuy));
            engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vRow, false, false);
        	
            Market marketSell = new Market();
            marketSell.setBoard("FOREIGNSELL");
            marketSell.setValue(new Double(F.getSellval().doubleValue()));
            marketSell.setVolume(new Double(F.getSellvol().doubleValue()));
            marketSell.setFreq(new Double(F.getSellfreq().doubleValue()));
           vRow = new Vector(1);
            vRow.addElement(MarketDef.createTableRow(marketSell));
            engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vRow, false, false);
            
            Market marketNet = new Market();
            marketNet.setBoard("FOREIGNNET");
            marketNet.setValue(new Double(marketBuy.getValue().doubleValue() - marketSell.getValue().doubleValue()));
            marketNet.setVolume(new Double(marketBuy.getVolume().doubleValue() - marketSell.getVolume().doubleValue()));
            marketNet.setFreq(new Double(marketBuy.getFreq().doubleValue() - marketSell.getFreq().doubleValue()));
            vRow = new Vector(1);
            vRow.addElement(MarketDef.createTableRow(marketNet));
            engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vRow, false, false);
            
            Market marketFTot = new Market();
            marketFTot.setBoard("FOREIGN%");
            
            marketFTot.setValue(new Double(F.getValtot().doubleValue() * Double.parseDouble(FeedSetting.getLot()) / (F.getValtot().doubleValue() +D.getValtot().doubleValue())));
            marketFTot.setVolume(new Double(F.getVoltot().doubleValue() * Double.parseDouble(FeedSetting.getLot()) / (F.getVoltot().doubleValue()+D.getVoltot().doubleValue())));
            marketFTot.setFreq(new Double(F.getFreqtot().doubleValue() * Double.parseDouble(FeedSetting.getLot()) / (F.getFreqtot().doubleValue()+D.getFreqtot().doubleValue())));
            vRow = new Vector(1);
            vRow.addElement(MarketDef.createTableRow(marketFTot));
            engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vRow, false, false);            
    	}
    }
	
    
    protected Integer getModelID(){
        return FeedStore.DATA_TRADESUMINV;
    }
    
    protected List createTableRow(Row dat){
    	return TradeSummaryDef.createTableRow(dat);
    }
    
   
}