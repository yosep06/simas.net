package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.StockComparisonBySector;
import eqtrade.feed.model.StockComparisonBySectorDef;
import eqtrade.feed.model.Subscriber;

public class StockComparisonBySectorParser extends Parser {

	public StockComparisonBySectorParser(IFeedEngine engine) {
		super(engine);
		setName(this.getClass().getName());
	}

	@Override
	public void process(Vector vdata) {
		if(vdata.size()>0){
			StockComparisonBySector cbs;
			Vector vRow;
			for(int i = 0; i<vdata.size(); i++){
				String source = (String) vdata.elementAt(i);
				cbs = new StockComparisonBySector(source);
				vRow = new Vector(i);
				vRow.addElement(StockComparisonBySectorDef.createTableRow(cbs));
				engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
				((Subscriber)hashSubscriber.get(cbs.getType())).setSeqno((int)cbs.getSeqno());
				refreshListener(cbs);
				System.out.println("Comparison___"+cbs.getPer());
			}
		}
		
	}
	protected Integer getModelID(){
		return FeedStore.DATA_STOCKCOMPARISONBYSECTOR;
	}
	protected List createTableRow(Row dat) {
		return StockComparisonBySectorDef.createTableRow(dat);
		
	}

}
