package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.GlobalIndices;
import eqtrade.feed.model.GlobalIndicesDef;
import eqtrade.feed.model.Subscriber;


public final class GlobalIdxParser  extends Parser {

    public GlobalIdxParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}
    
    //code|value|change
	public void process(Vector vdata){
        if (vdata.size() > 0){
    		GlobalIndices			idx;
			Vector vRow;
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			idx = new GlobalIndices(source);
    			vRow = new Vector(1);
                vRow.addElement(createTableRow(idx));
                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                ((Subscriber)hashSubscriber.get(idx.getType())).setSeqno((int)idx.getSeqno());
                refreshListener(idx);
    		}
        }		
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_GLOBALINDICES;
    }
    
    protected List createTableRow(Row dat){
    	return GlobalIndicesDef.createTableRow(dat);
    }
}