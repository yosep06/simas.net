package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Column;
import com.vollux.idata.Row;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Quote;
import eqtrade.feed.model.QuoteDef;
import eqtrade.feed.model.Subscriber;
import eqtrade.feed.model.TradePrice;
import eqtrade.feed.model.TradePriceDef;


public final class TradePriceParser  extends Parser {

    public TradePriceParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}
    
    protected String genFilename(String msg){
    	String[] s1 = msg.split("\\|");
    	String[] s2 = s1[1].split("\\#");
    	return "data/session/"+FeedSetting.session+"-"+s1[0]+s2[0]+s2[1]+".cache";
    }

	public void process(Vector vdata){
		double totallot=0, tfreq=0;
		//System.out.println("--tradePriceParser totlot="+ TradePrice.CIDX_LOT + " --freq="+TradePrice.CIDN_NUMBEROFFIELDS + "--"+ TradePrice.CIDX_CODE);
				
        if (vdata.size() > 0){
//        	System.out.println("--tradePricePaarser --vdata=" + vdata);
    		TradePrice price;
			Vector vRow;
			
			String board="",code="";
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			price = new TradePrice(source);
    			
    			//System.out.println("--TradePriceParser--source="+ source +"--size=");
    			//tambahan
    			board = price.getBoard();
    			code = price.getStock();
    			//end tambahan
    			vRow = new Vector(1);
                vRow.addElement(TradePriceDef.createTableRow(price));
                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                ((Subscriber)hashSubscriber.get(price.getType()+"|"+price.getStock()+"#"+price.getBoard())).setSeqno((int)price.getSeqno());
                //System.out.println("--TradePriceParser--process---getfreq="+ i +"-"+price.getFreq()+ " --getlot="+ price.getLot());
                refreshListener(price);
                //System.out.println("--TradePriceParser--process---getlot="+ price.getLot());                                  
    		}
    		
    		//System.out.println("-->nama engine model: board= "+ board + "--stock="+ code + "--"+ engine.getDataStore().get(getModelID()));
    		//System.out.println("---UIBEstQuote ---Get Table -- "+ TradePrice.getTable().hashCode());
    		Vector vAll = engine.getDataStore().get(getModelID()).getDataVector();
    		totallot =0; tfreq=0;
    		//System.out.println("nilai awal = "+ price.getPrice()+"--freq:"+ price.getFreq());
    		for (int i = 0; i < vAll.size();i++){
	    			
    	            TradePrice price2 = (TradePrice)((Column)((Vector)vAll.elementAt(i)).elementAt(0)).getSource();
    	            //System.out.println("freq awal di harga= "+ price2.getStock() + "--"+ price2.getPrice()+"--freq:"+ price2.getFreq());
    	            //if (i == 0) {
		            	//tfreq = tfreq + price.getFreq().doubleValue();
		            	//totallot =0; tfreq=0;        	
		            	/*price2.setPrice(new Double(0));	
		            	price2.setLot(new Double(0));
		            	price2.setFreq(new Double(0));*/
		            //}
    	            if (price2.getPrice() != 0 && price2.getBoard().equals(board) && price2.getStock().equals(code)){
    	            
    	            
    	            totallot = totallot + price2.getLot().doubleValue();
    	            tfreq = tfreq + price2.getFreq().doubleValue();
    	            }
    	            //System.out.println("freq akhir di price:"+price2.getPrice()+":"+tfreq);
    	            //System.out.println("calculate price akhir: "+ price.getPrice()+"--freq:"+ price.getFreq());
    	            
    		 }
    		//System.out.println("calculate total freq ke-1="+tfreq+ "--asal freq="+ TradePrice.CIDX_FREQ+"--price:"+TradePrice.CIDX_PRICE);
    		 TradePrice pp = new TradePrice();
    		 //System.out.println("calculate total freq ke-2="+tfreq+ "--asal freq="+ TradePrice.CIDX_FREQ+"--price:"+TradePrice.CIDX_PRICE);
    		 pp.setBoard(board);
    		 pp.setStock(code);
    		 pp.setLot(totallot);
    		 pp.setFreq(tfreq);
    		 pp.setType("L");
    		 pp.setPrice(new Double(0));
    		 //pp.setFreq(new Double(0));
    		//pp.setPrice(new Double.);
    		 
    		 vRow = new Vector(1);
             vRow.addElement(TradePriceDef.createTableRow(pp));
             engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
             //System.out.println("calculate total freq ke-3="+tfreq+ "--asal freq="+ TradePrice.CIDX_FREQ+"--price:"+TradePrice.CIDX_PRICE);
             //pp.setPrice(new Double(0));
             //pp.setLot(new Double(0));
             //pp.setFreq(new Double(0));
        }		
        
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_TRADEPRICE;
    }
    
    protected List createTableRow(Row dat){
    	return TradePriceDef.createTableRow(dat);
    }
    
    protected boolean removeAfterUnsubscribe(){
    	return true;
    }
    
    protected Vector getDataVector(Subscriber r){
    	Vector vtemp = new Vector(10);
        TradePrice price;
        
        Vector vAll = engine.getDataStore().get(getModelID()).getDataVector();
       //System.out.println("--TradePriceParser--getdataVector --"+ vAll.hashCode());
        for (int i = 0; i < vAll.size();i++){
            price = (TradePrice)((Column)((Vector)vAll.elementAt(i)).elementAt(0)).getSource();
            if (r.getKey().endsWith(price.getStock()+"#"+price.getBoard())){
            	vtemp.addElement(vAll.elementAt(i));
            } 
        }        
        return vtemp;
    }
    
    public void removeModel(Subscriber r){
        int rowCount = engine.getDataStore().get(getModelID()).getRowCount();
        TradePrice price;
        int counter =0;
        //System.out.println("--removemodel---"+ getModelID());
        for (int i = 0; i < rowCount;i++){
            price = (TradePrice)engine.getDataStore().get(getModelID()).getDataByIndex(counter);
            if (r.getKey().endsWith(price.getStock()+"#"+price.getBoard())){
            	engine.getDataStore().get(getModelID()).deleteRow(counter);
            } else counter++;
        }
    }  
}