package eqtrade.feed.parser;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.swing.SwingUtilities;

import com.vollux.idata.Column;
import com.vollux.idata.Row;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Broker;
import eqtrade.feed.model.Market;
import eqtrade.feed.model.MarketDef;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.Subscriber;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;


public final class TradeSummParser  extends Parser {

	public TradeSummParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}
    
    protected String getStockName(String code){
        Stock s = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new Object[]{code}));
        return (s!=null) ? s.getName() : "";
    }
    
    protected String getBrokerName(String code){
        Broker s = ((Broker)engine.getDataStore().get(FeedStore.DATA_BROKER).getDataByKey(new Object[]{code}));
        return (s!=null) ? s.getName() : "";
    }

	public void process(Vector vdata){
        if (vdata.size() > 0){
    		TradeSummary trade;
			Vector vRow;
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			trade = new TradeSummary(source);
    			if (!trade.getBoard().equals("RG")) continue;
    			trade.calculate();
                int idx = engine.getDataStore().get(getModelID()).getIndexByKey(new Object[]{trade.getBroker(),trade.getStock(), trade.getBoard(), trade.getInvestor(), });
                if (idx>-1){
	                    TradeSummary old = (TradeSummary)engine.getDataStore().get(getModelID()).getDataByIndex(idx);
	                    genSummary(FeedStore.DATA_TRADESUMBROKER, new Object[]{trade.getBroker(), "ALL", "ALL", "ALL"}, old, trade);
	                    genSummary(FeedStore.DATA_TRADESUMBS, new Object[]{trade.getBroker(), trade.getStock(), "ALL", "ALL"}, old, trade);
	                    genSummary(FeedStore.DATA_TRADESUMSTOCKINV, new Object[]{"ALL", trade.getStock(), "ALL", trade.getInvestor()}, old, trade);
	                    genSummary(FeedStore.DATA_TRADESUMINV, new Object[]{"ALL", "ALL", "ALL", trade.getInvestor()}, old, trade);
	                    old.fromVector(trade.getData());
	    	            engine.getDataStore().get(getModelID()).refresh(idx);
                } else {
	        			trade.setBrokername(getBrokerName(trade.getBroker()));
	        			trade.setStockname(getStockName(trade.getStock()));
	                    genSummary(FeedStore.DATA_TRADESUMBROKER, new Object[]{trade.getBroker(), "ALL", "ALL", "ALL"}, null, trade);
	                    genSummary(FeedStore.DATA_TRADESUMBS, new Object[]{trade.getBroker(), trade.getStock(), "ALL", "ALL"}, null, trade);
	                    genSummary(FeedStore.DATA_TRADESUMSTOCKINV, new Object[]{"ALL",trade.getStock(), "ALL", trade.getInvestor()}, null, trade);
	                    genSummary(FeedStore.DATA_TRADESUMINV, new Object[]{"ALL","ALL", "ALL", trade.getInvestor()}, null, trade);
	                    vRow = new Vector(1);
	                    vRow.addElement(TradeSummaryDef.createTableRow(trade));
	                    engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                }
                refreshListener(trade);
                ((Subscriber)hashSubscriber.get(trade.getType())).setSeqno((int)trade.getSeqno());
    		}
    		SwingUtilities.invokeLater(new Runnable(){
    			public void run() {
    	            genInv();
    			}
    		});
        }		
	}
	
    protected void genSummary(Integer model, Object[] keys, TradeSummary oldTrade, TradeSummary newTrade){
        TradeSummary summ;        
        int idx = engine.getDataStore().get(model).getIndexByKey(keys);
        if (idx > -1){
            summ = (TradeSummary)engine.getDataStore().get(model).getDataByIndex(idx);
            if (oldTrade == null){
                summ.setBuyvol(new Double(summ.getBuyvol().doubleValue() + newTrade.getBuyvol().doubleValue()));
                summ.setBuyval(new Double(summ.getBuyval().doubleValue() + newTrade.getBuyval().doubleValue()));
                summ.setBuyfreq(new Double(summ.getBuyfreq().doubleValue() + newTrade.getBuyfreq().doubleValue()));                
                summ.setSellvol(new Double(summ.getSellvol().doubleValue() + newTrade.getSellvol().doubleValue()));
                summ.setSellval(new Double(summ.getSellval().doubleValue() + newTrade.getSellval().doubleValue()));
                summ.setSellfreq(new Double(summ.getSellfreq().doubleValue() + newTrade.getSellfreq().doubleValue()));    
                summ.calculate();
            } else {
                summ.setBuyvol(new Double(summ.getBuyvol().doubleValue() - oldTrade.getBuyvol().doubleValue() + newTrade.getBuyvol().doubleValue()));
                summ.setBuyval(new Double(summ.getBuyval().doubleValue() - oldTrade.getBuyval().doubleValue() + newTrade.getBuyval().doubleValue()));
                summ.setBuyfreq(new Double(summ.getBuyfreq().doubleValue() - oldTrade.getBuyfreq().doubleValue() + newTrade.getBuyfreq().doubleValue()));                
                summ.setSellvol(new Double(summ.getSellvol().doubleValue() - oldTrade.getSellvol().doubleValue() + newTrade.getSellvol().doubleValue()));
                summ.setSellval(new Double(summ.getSellval().doubleValue() - oldTrade.getSellval().doubleValue() + newTrade.getSellval().doubleValue()));
                summ.setSellfreq(new Double(summ.getSellfreq().doubleValue() - oldTrade.getSellfreq().doubleValue() + newTrade.getSellfreq().doubleValue()));       
                summ.calculate();
            }
            try {
            	summ.setBuyavg(new Double(summ.getBuyval().doubleValue() / summ.getBuyvol().doubleValue()));
            } catch (Exception ex){
            	summ.setBuyavg(new Double(0));
            }
            try {
            	summ.setSellavg(new Double(summ.getSellval().doubleValue() / summ.getSellvol().doubleValue()));
            } catch (Exception ex){
            	summ.setSellavg(new Double(0));
            }
            engine.getDataStore().get(model).refresh(idx);
        } else {
            summ = new TradeSummary();
            summ.fromVector(newTrade.getData());
            summ.setBroker(keys[0].toString());
            summ.setActiontype(keys[1].toString());
            summ.setBoard(keys[2].toString());
            summ.setInvestor(keys[3].toString());
            summ.calculate();
			summ.setBrokername(getBrokerName(summ.getBroker()));
			summ.setStockname(getStockName(summ.getStock()));
           Vector vRow = new Vector(1);
            vRow.addElement(TradeSummaryDef.createTableRow(summ));
            engine.getDataStore().get(model).addRow(vRow, false, false);
        }
    }

    protected Integer getModelID(){
        return FeedStore.DATA_TRADESUMMARY;
    }
   
    protected List createTableRow(Row dat){
    	return TradeSummaryDef.createTableRow(dat);
    }
    
    protected void genInv(){
    	TradeSummary F = (TradeSummary)engine.getDataStore().get(FeedStore.DATA_TRADESUMINV).getDataByKey(new Object[]{"ALL", "ALL", "ALL", "F", "T"});
    	TradeSummary D = (TradeSummary)engine.getDataStore().get(FeedStore.DATA_TRADESUMINV).getDataByKey(new Object[]{"ALL", "ALL", "ALL", "D", "T"});
    	if (F!=null && D!=null){
            Market marketBuy = new Market();
            marketBuy.setBoard("FOREIGNBUY");
            marketBuy.setValue(new Double(F.getBuyval().doubleValue()));
            marketBuy.setVolume(new Double(F.getBuyvol().doubleValue()));
            marketBuy.setFreq(new Double(F.getBuyfreq().doubleValue()));
           Vector vRow = new Vector(1);
            vRow.addElement(MarketDef.createTableRow(marketBuy));
            engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vRow, false, false);
        	
            Market marketSell = new Market();
            marketSell.setBoard("FOREIGNSELL");
            marketSell.setValue(new Double(F.getSellval().doubleValue()));
            marketSell.setVolume(new Double(F.getSellvol().doubleValue()));
            marketSell.setFreq(new Double(F.getSellfreq().doubleValue()));
           vRow = new Vector(1);
            vRow.addElement(MarketDef.createTableRow(marketSell));
            engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vRow, false, false);
            
            Market marketNet = new Market();
            marketNet.setBoard("FOREIGNNET");
            marketNet.setValue(new Double(marketBuy.getValue().doubleValue() - marketSell.getValue().doubleValue()));
            marketNet.setVolume(new Double(marketBuy.getVolume().doubleValue() - marketSell.getVolume().doubleValue()));
            marketNet.setFreq(new Double(marketBuy.getFreq().doubleValue() - marketSell.getFreq().doubleValue()));
            vRow = new Vector(1);
            vRow.addElement(MarketDef.createTableRow(marketNet));
            engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vRow, false, false);
            
            Market marketFTot = new Market();
            marketFTot.setBoard("FOREIGN%");
            
            marketFTot.setValue(new Double(F.getValtot().doubleValue() * Double.parseDouble(FeedSetting.getLot()) / (F.getValtot().doubleValue() +D.getValtot().doubleValue())));
            marketFTot.setVolume(new Double(F.getVoltot().doubleValue() * Double.parseDouble(FeedSetting.getLot()) / (F.getVoltot().doubleValue()+D.getVoltot().doubleValue())));
            marketFTot.setFreq(new Double(F.getFreqtot().doubleValue() * Double.parseDouble(FeedSetting.getLot()) / (F.getFreqtot().doubleValue()+D.getFreqtot().doubleValue())));
            vRow = new Vector(1);
            vRow.addElement(MarketDef.createTableRow(marketFTot));
            engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vRow, false, false);            
    	}
    }
    
    
    private void saveDetail(String code, Integer data, Subscriber r){
        String filename = "data/session/"+FeedSetting.session+"-"+code+".cache";
        Vector vdata = new Vector(50,10);
        if (getModelID() != null){
            Vector vtemp = engine.getDataStore().get(data).getDataVector();
            for (int i=0; i<vtemp.size();i++){
                vdata.addElement(((Column)((Vector)vtemp.elementAt(i)).elementAt(0)).getSource());
            }
            HashMap map = new HashMap(2);
            map.put("seq", new Integer(r.getSeqno()));
            map.put("msg", code);
            log.info("save model: "+map+ " to: "+filename);
            map.put("data", vdata);
            Utils.writeFile(filename, Utils.objectToCompressedByte(map));
        }    	
    }
    
    protected void saveModel(Subscriber r){
    	saveDetail("TSB", FeedStore.DATA_TRADESUMBROKER, r);
    	saveDetail("TSBS", FeedStore.DATA_TRADESUMBS, r);
    	saveDetail("TSSI", FeedStore.DATA_TRADESUMSTOCKINV, r);
    	saveDetail("TSI", FeedStore.DATA_TRADESUMINV, r);    	
        super.saveModel(r);
    }

    private void loadDetail(String msg, Integer data){
        try {
            String  filename = "data/session/"+FeedSetting.session+"-"+msg+".cache";
            Object o = Utils.readFile(filename);
            if (o != null){
                byte[] byt = (byte[])o;            	
                HashMap map =  (HashMap)Utils.compressedByteToObject(byt);
                Vector vdata = (Vector)map.get("data");
                Vector vrow = new Vector(100,5);
                for (int i=0; i<vdata.size();i++){
                    vrow.addElement(TradeSummaryDef.createTableRow((Row)vdata.elementAt(i)));
                }
                engine.getDataStore().get(data).addRow(vrow, false, false);
                engine.getDataStore().get(data).refresh();
            } 
        } catch (Exception ex){
        }

    }
    
    protected int loadModel(String msg){
    	loadDetail("TSB", FeedStore.DATA_TRADESUMBROKER);
    	loadDetail("TSBS", FeedStore.DATA_TRADESUMBS);
    	loadDetail("TSSI", FeedStore.DATA_TRADESUMSTOCKINV);
    	loadDetail("TSI", FeedStore.DATA_TRADESUMINV);
        int seq = super.loadModel(msg);
        return seq;
    }
    
}