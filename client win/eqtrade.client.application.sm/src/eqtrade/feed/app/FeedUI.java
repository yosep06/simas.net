package eqtrade.feed.app;

import java.awt.Rectangle;
import java.util.Iterator;
import java.util.Vector;

import com.vollux.framework.IApp;
import com.vollux.framework.UI;
import com.vollux.framework.VolluxUI;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.ui.broker.UIBrokerActivity;
import eqtrade.feed.ui.broker.UIBrokerList;
import eqtrade.feed.ui.broker.UIBrokerSummary;
import eqtrade.feed.ui.broker.UITopBroker;
import eqtrade.feed.ui.chart.UIChart;
import eqtrade.feed.ui.chart.UIChartCompare;
import eqtrade.feed.ui.chart.UIIntraday;
import eqtrade.feed.ui.core.UIChgPwd;
import eqtrade.feed.ui.core.UIColour;
import eqtrade.feed.ui.core.UIConnection;
import eqtrade.feed.ui.core.UIDlgBroker;
import eqtrade.feed.ui.core.UIDlgBrokerNews;
import eqtrade.feed.ui.core.UIDlgNews;
import eqtrade.feed.ui.core.UIDlgStock;
import eqtrade.feed.ui.core.UIDlgStockNews;
import eqtrade.feed.ui.core.UIForgotPassword;
import eqtrade.feed.ui.core.UILogon;
import eqtrade.feed.ui.core.UIReconnect;
import eqtrade.feed.ui.core.UIShortcut;
import eqtrade.feed.ui.investor.UIInvByMarket;
import eqtrade.feed.ui.investor.UIInvByStock;
import eqtrade.feed.ui.market.UICorpAction;
import eqtrade.feed.ui.market.UIIPO;
import eqtrade.feed.ui.market.UIIndices;
import eqtrade.feed.ui.market.UIMarketSummary;
import eqtrade.feed.ui.market.UINews;
import eqtrade.feed.ui.market.UIRups;
import eqtrade.feed.ui.market.UITicker;
import eqtrade.feed.ui.news.UIDownloadMarketInfo;
import eqtrade.feed.ui.news.UIGlobalNews;
import eqtrade.feed.ui.news.UIIndicesCurrency;
import eqtrade.feed.ui.quote.UIMultiBestQuote;
import eqtrade.feed.ui.quote.UINGQuote;
import eqtrade.feed.ui.quote.UIQuoteQueue;
import eqtrade.feed.ui.quote.UIQuoteSummary;
import eqtrade.feed.ui.quote.UISelectedQuote;
import eqtrade.feed.ui.reksadana.UILinkAccount;
import eqtrade.feed.ui.reksadana.UILoginSimasROL;
import eqtrade.feed.ui.stock.UICashManagement;
import eqtrade.feed.ui.stock.UICompanyProfile;
import eqtrade.feed.ui.stock.UIDailyNews;
import eqtrade.feed.ui.stock.UIEmailHelpDesk;
import eqtrade.feed.ui.stock.UIHistorical;
import eqtrade.feed.ui.stock.UIOurOffice;
import eqtrade.feed.ui.stock.UIPickOfTheDay;
import eqtrade.feed.ui.stock.UIReporting;
import eqtrade.feed.ui.stock.UIStockActivity;
import eqtrade.feed.ui.stock.UIStockComparison2;
import eqtrade.feed.ui.stock.UIStockComparisonBySector;
import eqtrade.feed.ui.stock.UIStockDetail;
import eqtrade.feed.ui.stock.UIStockList;
import eqtrade.feed.ui.stock.UIStockSummary;
import eqtrade.feed.ui.stock.UITopStock;
import eqtrade.feed.ui.trade.UILiveTrade;
import eqtrade.feed.ui.trade.UILiveTradeBroker;
import eqtrade.feed.ui.trade.UILiveTradeStock;
import eqtrade.feed.ui.trade.UITimeTrade;
import eqtrade.feed.ui.trade.UITimeTradePrice;
import eqtrade.trading.ui.UIAnnouncement;
public class FeedUI extends VolluxUI {
	// public final static String UI_MAIN = "ui.main";
	public final static String UI_LOGON = "ui.logon";
	public final static String UI_CHGPWD = "ui.chgpwd";
	public final static String UI_RECONNECT = "ui.reconnect";
	public final static String UI_CONNECTION = "ui.connection";
	public final static String UI_COLOUR = "ui.colour";
	public final static String UI_DLGSTOCK = "ui.stock.entry";
	public final static String UI_DLGBROKER = "ui.broker.entry";
	public final static String UI_DLGNEWS = "ui.dialog.news";
	public final static String UI_DLGSTOCKNEWS="ui.dialog.stocknews";
	public final static String UI_DLGBROKERNEWS="ui.dialog.brokernews";
	public final static String UI_DLGSHORTCUT = "ui.dialog.shortcut";

	public final static String UI_BESTQUOTE = "ui.best.quote";
	public final static String UI_NGQUOTE = "ui.negotation.quote";
	public final static String UI_QUOTESUMMARY = "ui.quote.summary";
	public final static String UI_SELECTEDQUOTE = "ui.selected.quote";
	public final static String	UI_QUEUE="ui.queue.quote";
	public final static String UI_LIVETRADE = "ui.livetrade";
	public final static String UI_LIVETRADESTOCK = "ui.livetrade.stock";
	public final static String UI_LIVETRADEBROKER = "ui.livetrade.broker";
	public final static String UI_TIMETRADE = "ui.time.trade";
	public final static String UI_TIMETRADEPRICE = "ui.time.trade.price";
	public final static String UI_QUOTEQUEUE = "ui.qoute.queue";//Order Traking

	public final static String UI_TOPSTOCK = "ui.top.stock";
	public final static String UI_STOCKSUMMARY = "ui.stock.summary";
	public final static String UI_STOCKACTIVITY = "ui.stock.activity";
	public final static String UI_STOCKLIST = "ui.stocklist";

	public final static String UI_TOPBROKER = "top.broker";
	public final static String UI_BROKERSUMMARY = "broker.summary";
	public final static String UI_BROKERACTIVITY = "broker.activity";
	public final static String UI_BROKERLIST = "broker.list";

	public final static String UI_GLOBALNEWS = "global.news";
	public final static String UI_WORLDIDXCURR = "global.indices.currency";

	public final static String UI_INDICES = "ui.indices";
	public final static String UI_MARKETSUMMARY = "ui.marketsummary";
	public final static String UI_CHART = "ui.chart";
	public final static String UI_NEWS = "ui.news";
	public final static String UI_CORPACTION = "ui.corpaction";
	public final static String UI_ANNOUNCEMENT ="ui.announcement";
	public final static String UI_TICKER = "ui.ticker";
	public final static String UI_INTRADAY = "ui.intraday";
	public final static String UI_STOCKCOMPARISON2 ="ui.stock.comparisonto";//Stock Comparison
	public final static String UI_STOCKCOMPARISONBYSECTOR ="ui.stock.comparisonbysector";//Stock Comparison
	public final static String UI_CHARTCOMPARE = "ui.chart.compare";
	public final static String UI_CASHMANAGEMENT = "ui.cash.management";
	public final static String UI_CASHWITHDRAW = "ui.cash.withdraw";
	public final static String UI_CASHDEPOSIT = "ui.cash.deposit";
	public final static String UI_INVMARKET = "ui.investor.market";
	public final static String UI_INVSTOCK = "ui.investor.stock";
	public final static String UI_COMPANYPROFILE = "ui.company.profile";
	public final static String UI_RUPS = "ui.rups";
	public final static String UI_IPO = "ui.ipo";
	public final static String UI_REPORTING = "ui.reporting";
	public final static String UI_HISTORICAL = "ui.historical";
	public final static String UI_DAILYNEWS = "ui.dailynews";
	public final static String UI_PICKOFTHEDAY = "ui.pickoftheday";
	public final static String UI_EMAILHELPDESK = "ui.emailhelpdesk";
	public final static String UI_OUROFFICE = "ui.ouroffice";
	public final static String UI_FORGOTPASSWORD = "ui.forgotpassword";	
	public final static String UI_STOCKDETAIL = "ui.stockdetail";
	public final static String UI_DOWNLOADMARKETINFO = "ui.downloadmerketinfo";

	public final static String UI_LINKACCOUNT = "ui.linkaccount";
	public final static String UI_LOGINSIMASROLL = "ui.loginsimasroll";
	
	//untuk queue order
	//public final static String UI_QUEUEORDER = "ui.queueorder";
	// end of remark
	public FeedUI(IApp apps) {
		super(apps);
	}

	@Override
	protected void buildUI() {
		formList.put(UI_LOGON, new UILogon(FeedApplication.CONS_FEED));
		formList.put(UI_FORGOTPASSWORD, new UIForgotPassword(FeedApplication.CONS_FEED));
		formList.put(UI_CHGPWD, new UIChgPwd(FeedApplication.CONS_FEED));
		formList.put(UI_RECONNECT, new UIReconnect(FeedApplication.CONS_FEED));
		formList.put(UI_DLGSTOCK, new UIDlgStock(FeedApplication.CONS_FEED));
		formList.put(UI_DLGBROKER, new UIDlgBroker(FeedApplication.CONS_FEED));
		formList.put(UI_DLGNEWS, new UIDlgNews(FeedApplication.CONS_FEED));
		formList.put(UI_DLGSTOCKNEWS, new UIDlgStockNews(FeedApplication.CONS_FEED));
		formList.put(UI_DLGBROKERNEWS, new UIDlgBrokerNews(FeedApplication.CONS_FEED));
		formList.put(UI_DLGSHORTCUT, new UIShortcut(FeedApplication.CONS_FEED));

		UI uiQuote = new UIMultiBestQuote(apps, FeedApplication.CONS_FEED);
		addMulti(UI_BESTQUOTE, uiQuote);
		formList.put(UI_BESTQUOTE, uiQuote);
		formList.put(UI_NGQUOTE, new UINGQuote(FeedApplication.CONS_FEED));
		formList.put(UI_SELECTEDQUOTE, new UISelectedQuote(
				FeedApplication.CONS_FEED));
		formList.put(UI_QUEUE, new UIQuoteQueue(FeedApplication.CONS_FEED));
		formList.put(UI_QUOTESUMMARY, new UIQuoteSummary(
				FeedApplication.CONS_FEED));

		formList.put(UI_LIVETRADE, new UILiveTrade(FeedApplication.CONS_FEED));
		formList.put(UI_LIVETRADESTOCK, new UILiveTradeStock(
				FeedApplication.CONS_FEED));
		formList.put(UI_LIVETRADEBROKER, new UILiveTradeBroker(
				FeedApplication.CONS_FEED));
		formList.put(UI_TIMETRADE, new UITimeTrade(FeedApplication.CONS_FEED));
		formList.put(UI_TIMETRADEPRICE, new UITimeTradePrice(
				FeedApplication.CONS_FEED));

		formList.put(UI_TOPSTOCK, new UITopStock(FeedApplication.CONS_FEED));
		formList.put(UI_STOCKSUMMARY, new UIStockSummary(
				FeedApplication.CONS_FEED));
		formList.put(UI_STOCKACTIVITY, new UIStockActivity(
				FeedApplication.CONS_FEED));
		formList.put(UI_STOCKLIST, new UIStockList(FeedApplication.CONS_FEED));

		formList.put(UI_TOPBROKER, new UITopBroker(FeedApplication.CONS_FEED));
		formList.put(UI_BROKERSUMMARY, new UIBrokerSummary(
				FeedApplication.CONS_FEED));
		formList.put(UI_BROKERACTIVITY, new UIBrokerActivity(
				FeedApplication.CONS_FEED));
		formList.put(UI_BROKERLIST, new UIBrokerList(FeedApplication.CONS_FEED));

		formList.put(UI_INVMARKET, new UIInvByMarket(FeedApplication.CONS_FEED));
		formList.put(UI_INVSTOCK, new UIInvByStock(FeedApplication.CONS_FEED));

		formList.put(UI_GLOBALNEWS, new UIGlobalNews(FeedApplication.CONS_FEED));
		formList.put(UI_WORLDIDXCURR, new UIIndicesCurrency(
				FeedApplication.CONS_FEED));

		formList.put(UI_INDICES, new UIIndices(FeedApplication.CONS_FEED));
		formList.put(UI_MARKETSUMMARY, new UIMarketSummary(
				FeedApplication.CONS_FEED));
		formList.put(UI_CHART, new UIChart(FeedApplication.CONS_FEED));
		formList.put(UI_NEWS, new UINews(FeedApplication.CONS_FEED));
		formList.put(UI_CORPACTION, new UICorpAction(FeedApplication.CONS_FEED));
		formList.put(UI_ANNOUNCEMENT, new UIAnnouncement(FeedApplication.CONS_FEED));
		formList.put(UI_CONNECTION, new UIConnection(FeedApplication.CONS_FEED));
		formList.put(UI_COLOUR, new UIColour(FeedApplication.CONS_FEED));
		formList.put(UI_TICKER, new UITicker(FeedApplication.CONS_FEED));
		formList.put(UI_INTRADAY, new UIIntraday(FeedApplication.CONS_FEED));
		formList.put(UI_CHARTCOMPARE, new UIChartCompare(
				FeedApplication.CONS_FEED));
		formList.put(UI_COMPANYPROFILE, new UICompanyProfile(
				FeedApplication.CONS_FEED));
		formList.put(UI_CASHMANAGEMENT, new UICashManagement(
				FeedApplication.CONS_FEED));
		//formList.put(UI_CASHDEPOSIT, new UICashDeposit(
		//		FeedApplication.CONS_FEED));
		//formList.put(UI_CASHWITHDRAW, new UICashWithdraw(
		//		FeedApplication.CONS_FEED));
		formList.put(UI_RUPS, new UIRups(FeedApplication.CONS_FEED));
		formList.put(UI_IPO, new UIIPO(FeedApplication.CONS_FEED));
		formList.put(UI_REPORTING, new UIReporting(FeedApplication.CONS_FEED));
		formList.put(UI_HISTORICAL, new UIHistorical(FeedApplication.CONS_FEED));
		formList.put(UI_DAILYNEWS, new UIDailyNews(FeedApplication.CONS_FEED));
		formList.put(UI_PICKOFTHEDAY, new UIPickOfTheDay(FeedApplication.CONS_FEED));
		formList.put(UI_EMAILHELPDESK, new UIEmailHelpDesk(FeedApplication.CONS_FEED));
		formList.put(UI_OUROFFICE, new UIOurOffice(FeedApplication.CONS_FEED));
		//formList.put(UI_FORGOTPASSWORD, new UIForgotPassword(FeedApplication.CONS_FEED));
		//formList.put(UI_QUEUEORDER, new UIQueueOrder(FeedApplication.CONS_FEED));	
		formList.put(UI_STOCKCOMPARISON2, new UIStockComparison2(FeedApplication.CONS_FEED));
		formList.put(UI_STOCKCOMPARISONBYSECTOR, new UIStockComparisonBySector(FeedApplication.CONS_FEED));
		formList.put(UI_QUOTEQUEUE, new UIQuoteQueue(FeedApplication.CONS_FEED));
		formList.put(UI_ANNOUNCEMENT, new UIAnnouncement(FeedApplication.CONS_FEED));
		formList.put(UI_STOCKDETAIL, new UIStockDetail(FeedApplication.CONS_FEED));
		formList.put(UI_DOWNLOADMARKETINFO, new UIDownloadMarketInfo(FeedApplication.CONS_FEED));
		formList.put(UI_LINKACCOUNT, new UILinkAccount(FeedApplication.CONS_FEED));
		formList.put(UI_LOGINSIMASROLL, new UILoginSimasROL(FeedApplication.CONS_FEED));
	}

	@Override
	protected void saveLayout(Vector currView) {
		FeedSetting.putLayout("opened", currView);
		FeedSetting.putSetting("main", apps.getUIMain().getProperty("size"));
	}

	@Override
	protected Vector loadLayout() {
		Rectangle r = (Rectangle) FeedSetting.getSetting("main");
		if (r != null) {
			apps.getUIMain().setProperty("size", r);
		}
		return (Vector) FeedSetting.getLayout("opened");
	}
	
	@Override
	public void load(){
        try {
            Vector currView = loadLayout();
            if (currView!=null){
                    for (int i=0;i<currView.size();i++){
                        UI ui = (UI)formList.get((String)currView.elementAt(i));
                        if (!((Boolean)ui.getProperty(UI.C_PROPERTY_MULTI)).booleanValue()) {
                            String name = (String)currView.elementAt(i);
                            if (!name.equals(UI_LOGON)) showUI(name);
                        }
                    }
            }

            for(Iterator i = formMultiList.keySet().iterator();i.hasNext();){
                UI v = (UI)formMultiList.get(i.next());
                v.init(apps);
            }        
        } catch (Exception ex){
            ex.printStackTrace();
        }        
    }

}






/*package eqtrade.feed.app;

import java.awt.Rectangle;
import java.util.Iterator;
import java.util.Vector;

import com.vollux.framework.IApp;
import com.vollux.framework.UI;
import com.vollux.framework.VolluxUI;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.ui.broker.UIBrokerActivity;
import eqtrade.feed.ui.broker.UIBrokerList;
import eqtrade.feed.ui.broker.UIBrokerSummary;
import eqtrade.feed.ui.broker.UITopBroker;
import eqtrade.feed.ui.chart.UIChart;
import eqtrade.feed.ui.chart.UIChartCompare;
import eqtrade.feed.ui.chart.UIIntraday;
import eqtrade.feed.ui.core.UIChgPwd;
import eqtrade.feed.ui.core.UIColour;
import eqtrade.feed.ui.core.UIConnection;
import eqtrade.feed.ui.core.UIDlgBroker;
import eqtrade.feed.ui.core.UIDlgBrokerNews;
import eqtrade.feed.ui.core.UIDlgNews;
import eqtrade.feed.ui.core.UIDlgStock;
import eqtrade.feed.ui.core.UIDlgStockNews;
import eqtrade.feed.ui.core.UIForgotPassword;
import eqtrade.feed.ui.core.UILogon;
import eqtrade.feed.ui.core.UIReconnect;
import eqtrade.feed.ui.core.UIShortcut;
import eqtrade.feed.ui.investor.UIInvByMarket;
import eqtrade.feed.ui.investor.UIInvByStock;
import eqtrade.feed.ui.market.UICorpAction;
import eqtrade.feed.ui.market.UIIPO;
import eqtrade.feed.ui.market.UIIndices;
import eqtrade.feed.ui.market.UIMarketSummary;
import eqtrade.feed.ui.market.UINews;
import eqtrade.feed.ui.market.UIRups;
import eqtrade.feed.ui.market.UITicker;
import eqtrade.feed.ui.news.UIDownloadMarketInfo;
import eqtrade.feed.ui.news.UIGlobalNews;
import eqtrade.feed.ui.news.UIIndicesCurrency;
import eqtrade.feed.ui.quote.UIMultiBestQuote;
import eqtrade.feed.ui.quote.UINGQuote;
import eqtrade.feed.ui.quote.UIQuoteQueue;
import eqtrade.feed.ui.quote.UIQuoteSummary;
import eqtrade.feed.ui.quote.UISelectedQuote;
import eqtrade.feed.ui.reksadana.UILinkAccount;
import eqtrade.feed.ui.reksadana.UILoginSimasROL;
import eqtrade.feed.ui.stock.UICashManagement;
import eqtrade.feed.ui.stock.UICompanyProfile;
import eqtrade.feed.ui.stock.UIDailyNews;
import eqtrade.feed.ui.stock.UIEmailHelpDesk;
import eqtrade.feed.ui.stock.UIHistorical;
import eqtrade.feed.ui.stock.UIOurOffice;
import eqtrade.feed.ui.stock.UIPickOfTheDay;
import eqtrade.feed.ui.stock.UIReporting;
import eqtrade.feed.ui.stock.UIStockActivity;
import eqtrade.feed.ui.stock.UIStockComparison2;
import eqtrade.feed.ui.stock.UIStockComparisonBySector;
import eqtrade.feed.ui.stock.UIStockDetail;
import eqtrade.feed.ui.stock.UIStockList;
import eqtrade.feed.ui.stock.UIStockSummary;
import eqtrade.feed.ui.stock.UITopStock;
import eqtrade.feed.ui.trade.UILiveTrade;
import eqtrade.feed.ui.trade.UILiveTradeBroker;
import eqtrade.feed.ui.trade.UILiveTradeStock;
import eqtrade.feed.ui.trade.UITimeTrade;
import eqtrade.feed.ui.trade.UITimeTradePrice;
import eqtrade.trading.ui.UIAnnouncement;
public class FeedUI extends VolluxUI {
	// public final static String UI_MAIN = "ui.main";
	public final static String UI_LOGON = "ui.logon";
	public final static String UI_CHGPWD = "ui.chgpwd";
	public final static String UI_RECONNECT = "ui.reconnect";
	public final static String UI_CONNECTION = "ui.connection";
	public final static String UI_COLOUR = "ui.colour";
	public final static String UI_DLGSTOCK = "ui.stock.entry";
	public final static String UI_DLGBROKER = "ui.broker.entry";
	public final static String UI_DLGNEWS = "ui.dialog.news";
	public final static String UI_DLGSTOCKNEWS="ui.dialog.stocknews";
	public final static String UI_DLGBROKERNEWS="ui.dialog.brokernews";
	public final static String UI_DLGSHORTCUT = "ui.dialog.shortcut";

	public final static String UI_BESTQUOTE = "ui.best.quote";
	public final static String UI_NGQUOTE = "ui.negotation.quote";
	public final static String UI_QUOTESUMMARY = "ui.quote.summary";
	public final static String UI_SELECTEDQUOTE = "ui.selected.quote";
	public final static String	UI_QUEUE="ui.queue.quote";
	public final static String UI_LIVETRADE = "ui.livetrade";
	public final static String UI_LIVETRADESTOCK = "ui.livetrade.stock";
	public final static String UI_LIVETRADEBROKER = "ui.livetrade.broker";
	public final static String UI_TIMETRADE = "ui.time.trade";
	public final static String UI_TIMETRADEPRICE = "ui.time.trade.price";
	public final static String UI_QUOTEQUEUE = "ui.qoute.queue";//Order Traking

	public final static String UI_TOPSTOCK = "ui.top.stock";
	public final static String UI_STOCKSUMMARY = "ui.stock.summary";
	public final static String UI_STOCKACTIVITY = "ui.stock.activity";
	public final static String UI_STOCKLIST = "ui.stocklist";

	public final static String UI_TOPBROKER = "top.broker";
	public final static String UI_BROKERSUMMARY = "broker.summary";
	public final static String UI_BROKERACTIVITY = "broker.activity";
	public final static String UI_BROKERLIST = "broker.list";

	public final static String UI_GLOBALNEWS = "global.news";
	public final static String UI_WORLDIDXCURR = "global.indices.currency";

	public final static String UI_INDICES = "ui.indices";
	public final static String UI_MARKETSUMMARY = "ui.marketsummary";
	public final static String UI_CHART = "ui.chart";
	public final static String UI_NEWS = "ui.news";
	public final static String UI_CORPACTION = "ui.corpaction";
	public final static String UI_ANNOUNCEMENT ="ui.announcement";
	public final static String UI_TICKER = "ui.ticker";
	public final static String UI_INTRADAY = "ui.intraday";
	public final static String UI_STOCKCOMPARISON2 ="ui.stock.comparisonto";//Stock Comparison
	public final static String UI_STOCKCOMPARISONBYSECTOR ="ui.stock.comparisonbysector";//Stock Comparison
	public final static String UI_CHARTCOMPARE = "ui.chart.compare";
	public final static String UI_CASHMANAGEMENT = "ui.cash.management";
	public final static String UI_CASHWITHDRAW = "ui.cash.withdraw";
	public final static String UI_CASHDEPOSIT = "ui.cash.deposit";
	public final static String UI_INVMARKET = "ui.investor.market";
	public final static String UI_INVSTOCK = "ui.investor.stock";
	public final static String UI_COMPANYPROFILE = "ui.company.profile";
	public final static String UI_RUPS = "ui.rups";
	public final static String UI_IPO = "ui.ipo";
	public final static String UI_REPORTING = "ui.reporting";
	public final static String UI_HISTORICAL = "ui.historical";
	public final static String UI_DAILYNEWS = "ui.dailynews";
	public final static String UI_PICKOFTHEDAY = "ui.pickoftheday";
	public final static String UI_EMAILHELPDESK = "ui.emailhelpdesk";
	public final static String UI_OUROFFICE = "ui.ouroffice";
	public final static String UI_FORGOTPASSWORD = "ui.forgotpassword";	
	public final static String UI_STOCKDETAIL = "ui.stockdetail";
	public final static String UI_DOWNLOADMARKETINFO = "ui.downloadmerketinfo";

	public final static String UI_LINKACCOUNT = "ui.linkaccount";
	public final static String UI_LOGINSIMASROLL = "ui.loginsimasroll";
	
	//untuk queue order
	//public final static String UI_QUEUEORDER = "ui.queueorder";
	// end of remark
	public FeedUI(IApp apps) {
		super(apps);
	}

	@Override
	protected void buildUI() {
		formList.put(UI_LOGON, new UILogon(FeedApplication.CONS_FEED));
		formList.put(UI_FORGOTPASSWORD, new UIForgotPassword(FeedApplication.CONS_FEED));
		formList.put(UI_CHGPWD, new UIChgPwd(FeedApplication.CONS_FEED));
		formList.put(UI_RECONNECT, new UIReconnect(FeedApplication.CONS_FEED));
		formList.put(UI_DLGSTOCK, new UIDlgStock(FeedApplication.CONS_FEED));
		formList.put(UI_DLGBROKER, new UIDlgBroker(FeedApplication.CONS_FEED));
		formList.put(UI_DLGNEWS, new UIDlgNews(FeedApplication.CONS_FEED));
		formList.put(UI_DLGSTOCKNEWS, new UIDlgBrokerNews(FeedApplication.CONS_FEED));
		formList.put(UI_DLGBROKERNEWS, new UIDlgBrokerNews(FeedApplication.CONS_FEED));
		formList.put(UI_DLGSHORTCUT, new UIShortcut(FeedApplication.CONS_FEED));

		UI uiQuote = new UIMultiBestQuote(apps, FeedApplication.CONS_FEED);
		addMulti(UI_BESTQUOTE, uiQuote);
		formList.put(UI_BESTQUOTE, uiQuote);
		formList.put(UI_NGQUOTE, new UINGQuote(FeedApplication.CONS_FEED));
		formList.put(UI_SELECTEDQUOTE, new UISelectedQuote(
				FeedApplication.CONS_FEED));
		formList.put(UI_QUEUE, new UIQuoteQueue(FeedApplication.CONS_FEED));
		formList.put(UI_QUOTESUMMARY, new UIQuoteSummary(
				FeedApplication.CONS_FEED));

		formList.put(UI_LIVETRADE, new UILiveTrade(FeedApplication.CONS_FEED));
		formList.put(UI_LIVETRADESTOCK, new UILiveTradeStock(
				FeedApplication.CONS_FEED));
		formList.put(UI_LIVETRADEBROKER, new UILiveTradeBroker(
				FeedApplication.CONS_FEED));
		formList.put(UI_TIMETRADE, new UITimeTrade(FeedApplication.CONS_FEED));
		formList.put(UI_TIMETRADEPRICE, new UITimeTradePrice(
				FeedApplication.CONS_FEED));

		formList.put(UI_TOPSTOCK, new UITopStock(FeedApplication.CONS_FEED));
		formList.put(UI_STOCKSUMMARY, new UIStockSummary(
				FeedApplication.CONS_FEED));
		formList.put(UI_STOCKACTIVITY, new UIStockActivity(
				FeedApplication.CONS_FEED));
		formList.put(UI_STOCKLIST, new UIStockList(FeedApplication.CONS_FEED));

		formList.put(UI_TOPBROKER, new UITopBroker(FeedApplication.CONS_FEED));
		formList.put(UI_BROKERSUMMARY, new UIBrokerSummary(
				FeedApplication.CONS_FEED));
		formList.put(UI_BROKERACTIVITY, new UIBrokerActivity(
				FeedApplication.CONS_FEED));
		formList.put(UI_BROKERLIST, new UIBrokerList(FeedApplication.CONS_FEED));

		formList.put(UI_INVMARKET, new UIInvByMarket(FeedApplication.CONS_FEED));
		formList.put(UI_INVSTOCK, new UIInvByStock(FeedApplication.CONS_FEED));

		formList.put(UI_GLOBALNEWS, new UIGlobalNews(FeedApplication.CONS_FEED));
		formList.put(UI_WORLDIDXCURR, new UIIndicesCurrency(
				FeedApplication.CONS_FEED));

		formList.put(UI_INDICES, new UIIndices(FeedApplication.CONS_FEED));
		formList.put(UI_MARKETSUMMARY, new UIMarketSummary(
				FeedApplication.CONS_FEED));
		formList.put(UI_CHART, new UIChart(FeedApplication.CONS_FEED));
		formList.put(UI_NEWS, new UINews(FeedApplication.CONS_FEED));
		formList.put(UI_CORPACTION, new UICorpAction(FeedApplication.CONS_FEED));
		formList.put(UI_ANNOUNCEMENT, new UIAnnouncement(FeedApplication.CONS_FEED));
		formList.put(UI_CONNECTION, new UIConnection(FeedApplication.CONS_FEED));
		formList.put(UI_COLOUR, new UIColour(FeedApplication.CONS_FEED));
		formList.put(UI_TICKER, new UITicker(FeedApplication.CONS_FEED));
		formList.put(UI_INTRADAY, new UIIntraday(FeedApplication.CONS_FEED));
		formList.put(UI_CHARTCOMPARE, new UIChartCompare(
				FeedApplication.CONS_FEED));
		formList.put(UI_COMPANYPROFILE, new UICompanyProfile(
				FeedApplication.CONS_FEED));
		formList.put(UI_CASHMANAGEMENT, new UICashManagement(
				FeedApplication.CONS_FEED));
		//formList.put(UI_CASHDEPOSIT, new UICashDeposit(
		//		FeedApplication.CONS_FEED));
		//formList.put(UI_CASHWITHDRAW, new UICashWithdraw(
		//		FeedApplication.CONS_FEED));
		formList.put(UI_RUPS, new UIRups(FeedApplication.CONS_FEED));
		formList.put(UI_IPO, new UIIPO(FeedApplication.CONS_FEED));
		formList.put(UI_REPORTING, new UIReporting(FeedApplication.CONS_FEED));
		formList.put(UI_HISTORICAL, new UIHistorical(FeedApplication.CONS_FEED));
		formList.put(UI_DAILYNEWS, new UIDailyNews(FeedApplication.CONS_FEED));
		formList.put(UI_PICKOFTHEDAY, new UIPickOfTheDay(FeedApplication.CONS_FEED));
		formList.put(UI_EMAILHELPDESK, new UIEmailHelpDesk(FeedApplication.CONS_FEED));
		formList.put(UI_OUROFFICE, new UIOurOffice(FeedApplication.CONS_FEED));
		//formList.put(UI_FORGOTPASSWORD, new UIForgotPassword(FeedApplication.CONS_FEED));
		//formList.put(UI_QUEUEORDER, new UIQueueOrder(FeedApplication.CONS_FEED));	
		formList.put(UI_STOCKCOMPARISON2, new UIStockComparison2(FeedApplication.CONS_FEED));
		formList.put(UI_STOCKCOMPARISONBYSECTOR, new UIStockComparisonBySector(FeedApplication.CONS_FEED));
		formList.put(UI_QUOTEQUEUE, new UIQuoteQueue(FeedApplication.CONS_FEED));
		formList.put(UI_ANNOUNCEMENT, new UIAnnouncement(FeedApplication.CONS_FEED));
		formList.put(UI_STOCKDETAIL, new UIStockDetail(FeedApplication.CONS_FEED));
		formList.put(UI_DOWNLOADMARKETINFO, new UIDownloadMarketInfo(FeedApplication.CONS_FEED));
		formList.put(UI_LINKACCOUNT, new UILinkAccount(FeedApplication.CONS_FEED));
		formList.put(UI_LOGINSIMASROLL, new UILoginSimasROL(FeedApplication.CONS_FEED));
	}

	@Override
	protected void saveLayout(Vector currView) {
		FeedSetting.putLayout("opened", currView);
		FeedSetting.putSetting("main", apps.getUIMain().getProperty("size"));
	}

	@Override
	protected Vector loadLayout() {
		Rectangle r = (Rectangle) FeedSetting.getSetting("main");
		if (r != null) {
			apps.getUIMain().setProperty("size", r);
		}
		return (Vector) FeedSetting.getLayout("opened");
	}
	
	@Override
	public void load(){
        try {
            Vector currView = loadLayout();
            if (currView!=null){
                    for (int i=0;i<currView.size();i++){
                        UI ui = (UI)formList.get((String)currView.elementAt(i));
                        if (!((Boolean)ui.getProperty(UI.C_PROPERTY_MULTI)).booleanValue()) {
                            String name = (String)currView.elementAt(i);
                            if (!name.equals(UI_LOGON)) showUI(name);
                        }
                    }
            }

            for(Iterator i = formMultiList.keySet().iterator();i.hasNext();){
                UI v = (UI)formMultiList.get(i.next());
                v.init(apps);
            }        
        } catch (Exception ex){
            ex.printStackTrace();
        }        
    }

}
*/