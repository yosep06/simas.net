package eqtrade.feed.ui.trade;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.RunningTrade;
import eqtrade.feed.model.RunningTradeDef;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;
import eqtrade.feed.ui.stock.FilterTopStock;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
public class UITimeTradePrice extends UI {
	private JGrid table;
	private JGrid tableTrade;
	private JPanel pnlInfo;

	private FilterTopStock filter;
	private FilterTrade filterTrade;
	private JTextField fieldStock= new JTextField("");;
	private JNumber fieldPrice;
	private JLabel fieldName;
	private String oldStock = "";
	private Double oldPrice = new Double(0);

	private static NumberFormat formatter = new DecimalFormat("#,##0  ");

	public UITimeTradePrice(String app) {
		super("Time & Trade Summary", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_TIMETRADEPRICE);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem mnBrokerSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_BROKERSUMMARY, param);
				}
			}
		});
		mnBrokerSumm.setText("Buy/Sell Broker Summary");
		popupMenu.add(mnBrokerSumm);

		JMenuItem mnBrokerActivity = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_BROKERACTIVITY, param);
				}
			}
		});
		mnBrokerActivity.setText("Broker Activity by Stock");
		popupMenu.add(mnBrokerActivity);

		JMenuItem mnInvStock = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_INVSTOCK, param);
				}
			}
		});
		mnInvStock.setText("Foreign/Domestic by Stock");
		popupMenu.add(mnInvStock);

		JMenuItem mnCompany = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					int i = tableTrade.getMappedRow(table.getSelectedRow());
					if (i > -1) {
						RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_TRADEHISTORY)
								.getDataByIndex(i);
						HashMap param = new HashMap();
						param.put("STOCK", order.getStock());
						apps.getUI().showUI(FeedUI.UI_COMPANYPROFILE, param);
					}
				} catch (Exception ex) {
					Utils.showMessage("cannot open Browser", null);
				}
			}
		});
		mnCompany.setText("Company Profile");
		popupMenu.add(mnCompany);
		JMenuItem mnIntraday = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_INTRADAY, param);
				}
			}
		});
		mnIntraday.setText("Intraday Chart");
		popupMenu.add(mnIntraday);

		JMenuItem mnTechnical = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_CHART, param);
				}
			}
		});
		mnTechnical.setText("Technical Chart");
		popupMenu.add(mnTechnical);
		JMenuItem mnTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_LIVETRADESTOCK, param);
				}
			}
		});
		mnTrade.setText("Live Trade By Stock");
		popupMenu.add(mnTrade);

		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
	}

	@Override
	protected void build() {
		oldStock = "";
		oldPrice = new Double(0);
		createPopup();
		filter = new FilterTopStock(pnlContent);
		((FilterColumn) filter.getFilteredData("stock")).setField("");
		filterTrade = new FilterTrade(pnlContent);
	
		((FilterColumn) filter.getFilteredData("board")).setField(null);
		Vector  vboard = new  Vector(2);
		vboard.addElement("RG");
		vboard.addElement("TN");
		((FilterColumn) filter.getFilteredData("multiboard")).setField(vboard);
		table = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_STOCKSUMMARY), filter,
				(Hashtable) hSetting.get("table"));
		// table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO, StockSummary.CIDX_BESTBID,
				StockSummary.CIDX_BESTBIDVOLUME, StockSummary.CIDX_BESTOFFER,
				StockSummary.CIDX_BESTOFFERVOLUME, StockSummary.CIDX_BOARD,
				StockSummary.CIDX_CODE, StockSummary.CIDX_FOREIGNERS,
				StockSummary.CIDX_INDIVIDUALINDEX, StockSummary.CIDX_REMARKS,
				StockSummary.CIDX_TRADEDFREQUENCY,
				StockSummary.CIDX_TRADEDVALUE, StockSummary.CIDX_TRADEDVOLUME,
				StockSummary.CIDX_TRADEDLOT });

		tableTrade = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_TRADEHISTORY), filterTrade,
				(Hashtable) hSetting.get("tabletrade"));
		// tableTrade.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		tableTrade.setColumnHide(new int[] { RunningTrade.CIDX_STOCK,
				Model.CIDX_HEADER, Model.CIDX_TYPE, Model.CIDX_SEQNO,
				RunningTrade.CIDX_BOARD });
		tableTrade.setSortColumn(RunningTrade.CIDX_TXTIME);
		tableTrade.setSortAscending(false);
		tableTrade.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		pnlInfo = new JPanel();
		fieldStock = new JTextField("");
		fieldPrice = new JNumber(Double.class, 0, true);
		fieldName = new JLabel("please type a stock code & price");

		FormLayout l = new FormLayout("75px,2dlu,50px, 2dlu, pref:grow, pref",
				"pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(fieldStock, c.xy(1, 1));
		b.add(fieldPrice, c.xy(3, 1));
		b.add(fieldName, c.xy(5, 1));

		JSkinPnl header = new JSkinPnl();
		header.add(b.getPanel(), BorderLayout.NORTH);
		header.add(table, BorderLayout.CENTER);
		table.setPreferredSize(new Dimension(100, 60));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(header, BorderLayout.NORTH);
		pnlContent.add(tableTrade, BorderLayout.CENTER);

		fieldStock.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				fieldStockAction();
			}

			@Override
			public void focusGained(FocusEvent e) {
				fieldStock.selectAll();
			}
		});
		fieldStock.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fieldStockAction();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		fieldPrice.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				fieldStockAction();
			}

			@Override
			public void focusGained(FocusEvent e) {
				fieldStock.selectAll();
			}
		});
		fieldPrice.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fieldStockAction();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		refresh();
		if (hSetting.get("stock") != null) {
			fieldStock.setText((String) hSetting.get("stock"));
		}
		if (hSetting.get("price") != null) {
			fieldPrice.setValue(hSetting.get("price"));
		}
		fieldStockAction();
	}

	private void fieldStockAction() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String newStock = fieldStock.getText().trim().toUpperCase();
				Double newPrice = (Double) fieldPrice.getValue();
				if (newPrice == null)
					newPrice = new Double(0);
				if ((newStock.length() > 0 && !oldStock.equals(newStock) && newPrice
						.doubleValue() > 0)
						|| (newPrice.doubleValue() > 0 && newStock.length() > 0 && newPrice
								.doubleValue() != oldPrice.doubleValue())) {
					setFieldStock(false);// yosep fixing timetrade
					if (oldStock.length() > 0) {
						((IEQTradeApp) apps).getFeedEngine()
								.unsubscribe(
										FeedParser.PARSER_TRADEHISTORY,
										FeedParser.PARSER_TRADEHISTORY + "|"
												+ oldStock);
					}
					fieldStock.setText(newStock);
					oldStock = newStock;
					oldPrice = newPrice;
					((FilterColumn) filter.getFilteredData("stock"))
							.setField(newStock);
					filter.fireFilterChanged();
					((FilterColumn) filterTrade.getFilteredData("stock"))
							.setField(newStock);
					((FilterColumn) filterTrade.getFilteredData("price"))
							.setField(newPrice);
					filterTrade.fireFilterChanged();
					fieldName.setText(geStockName(newStock));
					form.setTitle(oldStock + " Time & Trade Summary at "
							+ formatter.format(oldPrice.doubleValue()));
					((IEQTradeApp) apps).getFeedEngine().subscribe(
							FeedParser.PARSER_TRADEHISTORY,
							FeedParser.PARSER_TRADEHISTORY + "|" + oldStock);
					//System.out.println("Old Stock...."+oldStock);
					setFieldStock(true);// yosep fixing timetrade
				}
				fieldStock.selectAll();
			}
		});
	}
	

	private String geStockName(String code) {
		String name = "";
		int i = ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK)
				.getIndexByKey(new Object[] { code });
		if (i > -1) {
			name = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_STOCK).getDataByIndex(i))
					.getName();
		}
		return name;
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", StockSummaryDef.getTableDef());
			hSetting.put("tabletrade", RunningTradeDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 450, 350));
		
		((Hashtable) hSetting.get("table")).put("header",StockSummaryDef.dataHeader.clone());
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		hSetting.put("tabletrade", tableTrade.getTableProperties());
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put("price", oldPrice);
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableTrade.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		fieldName.setForeground(FeedSetting.getColor(FeedSetting.C_FOREGROUND));
		pnlInfo.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
		tableTrade.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void show(Object param) {
		if (canClose) {
			if (param != null) {
				HashMap par = (HashMap) param;
				fieldStock.setText(par.get("STOCK").toString());
				fieldPrice.setValue(par.get("PRICE"));
				fieldStockAction();
			}
		}else {
			Utils.showMessage(String.format("%20s", "Please Wait"), null);
		}
		show();
	}

	@Override
	public void hide() {
		// if (!oldStock.equals("")){
		// ((IEQTradeApp)apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADEHISTORY,
		// FeedParser.PARSER_TRADEHISTORY+"|"+oldStock);
		// }
		// super.hide();
		close();
	}

	@Override
	public void close() {
		if (canClose) {// yosep fixingTimetrade
				if (!oldStock.equals("")) {
				((IEQTradeApp) apps).getFeedEngine().unsubscribe(
						FeedParser.PARSER_TRADEHISTORY,
						FeedParser.PARSER_TRADEHISTORY + "|" + oldStock);
			}
			super.close();
		}

	}
	
	public String getOldStock(){
		return oldStock;
	}
	boolean canClose=true;
	public void setFieldStock(boolean hist){
//		System.out.println("setfield UItimeTradeprice "+hist);
		try {
			fieldStock.setEnabled(hist);
			fieldPrice.setEnabled(hist);
			canClose = hist;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
