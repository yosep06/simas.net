package eqtrade.feed.ui.quote;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.Queue;
import eqtrade.feed.model.Quote;

public class FilterQueue extends FilterBase {
	private HashMap mapFilter = new HashMap();

	public FilterQueue(Component parent) {
		super(parent, "filter");
		mapFilter.put("stock", new FilterColumn("stock", String.class,
				new String(""), FilterColumn.C_EQUAL));
		mapFilter.put("price", new FilterColumn("price", String.class, null,
				FilterColumn.C_EQUAL));

	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			Queue dat = (Queue) val.getSource();
			/*System.out.println("dat:"
					+ ((FilterColumn) mapFilter.get("stock")).getField()
					+ ":"
					+ ((FilterColumn) mapFilter.get("price")).getField()
					+ ":"
					+ dat.getCode()
					+ ":"
					+ dat.getPrice()
					+ ":"
					+ ((FilterColumn) mapFilter.get("price")).compare(dat
							.getPrice()));*/
			if (((FilterColumn) mapFilter.get("stock")).compare(dat.getCode())
					&& ((FilterColumn) mapFilter.get("price")).compare(dat
							.getPrice())) {
				avail = true;
			}
			return avail;

		} catch (Exception e) {
		}
		return false;
	}

	@Override
	public FilterColumn getFilteredData(String key) {
		return (FilterColumn) mapFilter.get(key);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

}
