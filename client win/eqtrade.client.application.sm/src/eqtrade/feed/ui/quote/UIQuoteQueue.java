package eqtrade.feed.ui.quote;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.sun.org.apache.xml.internal.dtm.ref.DTMDefaultBaseIterators.ParentIterator;
import com.vollux.demo.Model;
import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Queue;
import eqtrade.feed.model.QueueDef;
import eqtrade.feed.model.Stock;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.model.Order;

public class UIQuoteQueue extends UI {
	// protected QueuePanel queuePanel;
	// private FilterQueue filter;
	// private FilterQueueOrder filter;
	private JGrid table;

	private FilterQueue filterStock;
	private JLabel fieldPrice;
	private JDropDown comboStock;
	private JNumber price;
	private JNumber nomor;
	JNumber tCount, tLot;
	private JButton btnView;
	private JLabel fieldStock;
	private JPanel pnlInfo;
	private JSplitPane splitter;
	private FilterQueue filter;
	protected GridModel modelQueue;
	private JNumber fieldvarbid;
	String keybid = "";
	String Ctype = "";
	String strState = "";
	String board = "";
	protected static NumberFormat format2 = new DecimalFormat("###0");

	public UIQuoteQueue(String app) {
		super("Quote Queue", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_QUEUE);
		setProperty(C_PROPERTY_NO, no);
	}

	@Override
	protected void build() {
		modelQueue = ((IEQTradeApp) apps).getFeedEngine().getStore(
				FeedStore.DATA_QUOTE_QUEUE);
		comboStock = new JDropDown(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK), Stock.CIDX_CODE, true);
		comboStock.setBorder(new EmptyBorder(0, 0, 0, 0));
		price = new JNumber(Double.class, 0, 0, false, true);
		fieldvarbid = new JNumber(Double.class, 0, 0, false, true);
		filter = new FilterQueue(pnlContent);

		tCount = new JNumber(Double.class, 0, 0, false, false);
		tLot = new JNumber(Double.class, 0, 0, false, false);

		table = createTable(modelQueue, null,
				(Hashtable) hSetting.get("tablequeue"));
		table.setColumnHide(new int[] { Queue.CIDX_STATE, Queue.CIDX_TYPE });
		table.getTable().getTableHeader().setReorderingAllowed(false);
		table.getTable().setAutoResizeMode(
				JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		fieldPrice = new JLabel("Price", JLabel.RIGHT);
		fieldStock = new JLabel("Stock", JLabel.RIGHT);
		btnView = new JButton("View");
		btnView.setMnemonic('V');
		JSkinPnl query = new JSkinPnl();
		FormLayout ll = new FormLayout(
				"40px,2dlu, 75px,2dlu,40px, 2dlu,60px, 2dlu, pref,pref:grow ",
				// "40px,2dlu,pref:grow,2dlu, 40px,2dlu, 40px,2dlu,pref:grow",
				"pref");
		CellConstraints cc = new CellConstraints();
		PanelBuilder bb = new PanelBuilder(ll, query);
		bb.add(fieldStock, cc.xy(1, 1));
		bb.add(comboStock, cc.xy(3, 1));
		bb.add(fieldPrice, cc.xy(5, 1));
		bb.add(price, cc.xy(7, 1));
		bb.add(btnView, cc.xy(9, 1));

		btnView.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fieldStockAction();
				// filterQueue.fireFilterChanged();
			}
		});

		JPanel infoPanel = new JPanel();
		FormLayout infLayout = new FormLayout(
				"pref,2dlu,150px,2dlu,pref,2dlu,150px, 2dlu, pref, 2dlu, 110px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 110px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 110px",
				"pref");
		PanelBuilder infBuilder = new PanelBuilder(infLayout, infoPanel);
		infBuilder.setBorder(new EmptyBorder(4, 4, 4, 4));

		infBuilder.add(new JLabel("Count"), cc.xy(1, 1));
		infBuilder.add(tCount, cc.xy(3, 1));
		infBuilder.add(new JLabel("Total Lot"), cc.xy(5, 1));
		infBuilder.add(tLot, cc.xy(7, 1));

		pnlInfo = new JPanel();
		FormLayout l = new FormLayout(
				"50px,2dlu,pref:grow, 2dlu,50px,2dlu,pref:grow",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(query, c.xyw(1, 1, 7));

		pnlInfo.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlInfo, BorderLayout.CENTER);

		splitter = new JSplitPane(JSplitPane.VERTICAL_SPLIT, bb.getPanel(),
				table);
		splitter.setBorder(new EmptyBorder(0, 0, 0, 0));
		Integer pos = (Integer) hSetting.get("splitter");
		if (pos != null)
			splitter.setDividerLocation(pos.intValue());
		pnlContent.add(splitter, BorderLayout.CENTER);
		// pnlContent.add(modelQueue, BorderLayout.CENTER);

		JScrollPane bd = new JScrollPane(infoPanel);
		bd.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		bd.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		pnlContent.add(bd, BorderLayout.PAGE_END);

		fieldPrice.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				fieldStockAction();
			}
		});
		fieldPrice.registerKeyboardAction(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				fieldStockAction();

			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		refresh();

		modelQueue.addTableModelListener(new RowChanged());

	}

	private void fieldStockAction() {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_QUOTE_QUEUE).realData.clear();
				((IEQTradeApp) apps).getFeedEngine()
						.getStore(FeedStore.DATA_QUOTE_QUEUE).getDataVector()
						.clear();

				((IEQTradeApp) apps).getFeedEngine()
						.getStore(FeedStore.DATA_QUOTE_QUEUE).refresh();

				if (!keybid.isEmpty()) {
					((IEQTradeApp) apps).getFeedEngine().unsubscribe(
							FeedParser.PARSER_QUEUE, keybid);
				}

				String newStock = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
						.getStore(FeedStore.DATA_STOCK)
						.getDataByIndex(comboStock.getSelectedIndex()))
						.getCode();

				// log.info("price "+price.getValue());

				Double dd = new Double(price.getValue().toString());

				String key = newStock + "|"+board+"|" + Ctype + "|"
						+ format2.format(dd);

				keybid = key;

				((IEQTradeApp) apps).getFeedEngine().subscribe(
						FeedParser.PARSER_QUEUE, key);

				// filter.getFilteredData("stock").setField(newStock);
				// filter.getFilteredData("price").setField(dd);
				// filter.fireFilterChanged();

				/*
				 * String newPrice = price.getText().trim().toUpperCase();
				 * String svarbid = fieldvarbid.getText().trim().toUpperCase();
				 * 
				 * if (svarbid.equals("10")){ form.setTitle("Queue Bid Order "+
				 * newStock + " on "+ newPrice); } else {
				 * form.setTitle("Queue Offer Order "+ newStock + " on "+
				 * newPrice); } System.out.println("Price--"+price.getValue());
				 * Double dd = new Double(price.getValue().toString());
				 * 
				 * String stock = comboStock.getText().toUpperCase(); String key
				 * = stock + "#RG" +"#"+Ctype + "#" +
				 * format2.format(dd)+"#"+strState; // log.info("subscribe:" +
				 * key);)0O System.out.println("subscribe:" + key); keybid =
				 * key;
				 * 
				 * ((IEQTradeApp) apps).getFeedEngine().subscribe(
				 * FeedParser.PARSER_QUEUE, FeedParser.PARSER_QUEUE + "|" +
				 * key);
				 * 
				 * filter.getFilteredData("stock").setField(stock);
				 * filter.getFilteredData("price").setField(dd);
				 * filter.fireFilterChanged();
				 */

			}
		});
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});

	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME)
						+ getProperty(C_PROPERTY_NO));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tablequeue", QueueDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 100, 600, 420));

	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("tablequeue", table.getTableProperties());

	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
			comboStock.setSelectedItem((String) par.get("stock").toString());
			price.setValue(par.get("price"));
			Ctype = par.get("type").toString();
			board = par.get("board").toString();
			if (!keybid.equals("")) {
				((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_QUEUE, keybid);
			}
			keybid = "";
			fieldStockAction();
		}
		show();
	}

	public void hide() {
		form.setVisible(false);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_QUEUE, keybid);
		super.hide();
	}

	@Override
	public void close() {

		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_QUEUE, keybid);
		super.close();
	}

	protected class RowChanged implements TableModelListener {

		public void tableChanged(final TableModelEvent e) {
			double count = 0, ocount = 0, ovalue = 0, dcount = 0, dvalue = 0, tlot = 0, tvalue = 0;
			int size = modelQueue.getRowCount();

			for (int i = 0; i < size; i++) {
				Queue order = (Queue) modelQueue.getDataByIndex(i);
				count++;
				tlot = tlot + order.getLot().doubleValue();

			}
			tCount.setValue(new Double(count));
			tLot.setValue(new Double(tlot));

		}

		/*
		 * public void tableChanged(final TableModelEvent e) { double count = 0,
		 * ocount = 0, ovalue = 0, dcount = 0, dvalue = 0, tlot = 0, tvalue = 0;
		 * int size = tableOrder.getTable().getRowCount(); for (int i = 0; i <
		 * size; i++) { Order order = (Order) tableOrder.getDataByIndex(i);
		 * count++; tlot = tlot + order.getLot().doubleValue(); tvalue = tvalue
		 * + (order.getVolume().doubleValue() * order.getPrice()
		 * .doubleValue()); if (order.getStatus().equals(Order.C_PARTIAL_MATCH)
		 * || order.getStatus().equals(Order.C_OPEN)) { ocount++; ovalue =
		 * ovalue + (order.getBalVol().doubleValue() * order
		 * .getPrice().doubleValue()); } if
		 * (order.getStatus().equals(Order.C_PARTIAL_MATCH) ||
		 * order.getStatus().equals(Order.C_FULL_MATCH)) { dcount++; dvalue =
		 * dvalue + (order.getDoneVol().doubleValue() * order
		 * .getPrice().doubleValue()); } tCount.setValue(new Double(count));
		 * oCount.setValue(new Double(ocount)); oValue.setValue(new
		 * Double(ovalue)); dCount.setValue(new Double(dcount));
		 * dValue.setValue(new Double(dvalue)); tLot.setValue(new Double(tlot));
		 * tValue.setValue(new Double(tvalue)); } }
		 */
	}

}
