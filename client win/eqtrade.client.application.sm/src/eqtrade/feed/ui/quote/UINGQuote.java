package eqtrade.feed.ui.quote;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;

public class UINGQuote extends UI {
	private JGrid table;
	private FilterQuoteSummary filter;

	public UINGQuote(String app) {
		super("Negotation Quote", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_NGQUOTE);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		filter = new FilterQuoteSummary(pnlContent);
		((FilterColumn) filter.getFilteredData("board")).setField("NG");
		((FilterColumn) filter.getFilteredData("freq")).setField(new Double(0));
		table = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY), filter,(Hashtable) hSetting.get("table"));
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				StockSummary.CIDX_REMARKS, StockSummary.CIDX_BOARD,
				StockSummary.CIDX_FOREIGNERS, StockSummary.CIDX_HIGHEST,
				StockSummary.CIDX_INDIVIDUALINDEX, StockSummary.CIDX_LOWEST,
				StockSummary.CIDX_OPENING, StockSummary.CIDX_PREVIOUS,
				StockSummary.CIDX_TRADEDFREQUENCY,
				StockSummary.CIDX_TRADEDVALUE, StockSummary.CIDX_TRADEDVOLUME,
				StockSummary.CIDX_AVG, StockSummary.CIDX_TRADEDLOT });
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		refresh();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", StockSummaryDef.getTableNGDef());
		}
		if (hSetting.get("size") == null) hSetting.put("size", new Rectangle(20, 20, 400, 300));
		
		((Hashtable) hSetting.get("table")).put("header",StockSummaryDef.dataHeader.clone());
	}

	@Override
	public void saveSetting() {
		hSetting.put("size",new Rectangle(form.getX(), form.getY(), form.getWidth(), form.getHeight()));
		hSetting.put("table", table.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
	}
}
