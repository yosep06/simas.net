package eqtrade.feed.ui.chart;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import net.sf.nachocalendar.CalendarFactory;
import net.sf.nachocalendar.components.DateField;

import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTick;
import org.jfree.chart.axis.SegmentedTimeline;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.TextAnchor;

//import vollux.chart.core.ChartContainerDaily;
import vollux.chart.core.CustomDateAxis;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Indices;
import eqtrade.feed.model.Stock;
import eqtrade.trading.market.chart.core.ChartContainerDaily;
import feed.admin.Utils;

public class UIChartCompare extends UI {
	private JPanel pnlInput;
	private ChartContainerDaily chart;
	private TimeSeriesCollection xydataset;
	private XYItemRenderer render;
	private CustomDateAxis axis;//update-280814
	private DateField dfFrom;
	private DateField dfTo;
	private JButton btnPreview;
	private JDropDown comboStock;
	private JButton btnStock;
	private JDropDown comboIndices;
	private JButton btnIndices;

	private JTextField fieldStock;
	private String oldStock = "";

	public UIChartCompare(String app) {
		super("Chart Comparison", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_CHARTCOMPARE);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	public static final SimpleDateFormat C_DATEFORMAT = new SimpleDateFormat(
			"yyyy-MM-dd");

	@Override
	protected void build() {
		createPopup();

		pnlInput = new JPanel(new BorderLayout());
		fieldStock = new JTextField("");
		dfFrom = CalendarFactory.createDateField();
		dfFrom.setDateFormat(C_DATEFORMAT);
		dfFrom.setValue(new Date());
		dfTo = CalendarFactory.createDateField();
		dfTo.setDateFormat(C_DATEFORMAT);
		dfTo.setValue(new Date());
		btnPreview = new JButton("Draw");
		comboStock = new JDropDown(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK), Stock.CIDX_CODE, true);
		comboStock.setBorder(new EmptyBorder(0, 0, 0, 0));
		comboStock.getTextEditor().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				comboStock.getTextEditor().setText(
						comboStock.getTextEditor().getText().toUpperCase());
			}

			@Override
			public void focusGained(FocusEvent e) {
				comboStock.getTextEditor().selectAll();
			}
		});
		btnStock = new JButton("Add");
		btnStock.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!comboStock.getText().equals("")) {
					if (fieldStock.getText().equals("")) {
						fieldStock.setText(comboStock.getText().toUpperCase());
					} else {
						fieldStock.setText(fieldStock.getText().trim()
								.toUpperCase()
								+ ";" + comboStock.getText().toUpperCase());
					}
					comboStock.getTextEditor().setText("");
				}
			}
		});
		comboIndices = new JDropDown(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_INDICES), Indices.CIDX_CODE, true);
		comboIndices.setBorder(new EmptyBorder(0, 0, 0, 0));
		comboIndices.getTextEditor().addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				comboIndices.getTextEditor().setText(
						comboIndices.getTextEditor().getText().toUpperCase());
			}

			@Override
			public void focusGained(FocusEvent e) {
				comboIndices.getTextEditor().selectAll();
			}
		});
		btnIndices = new JButton("Add");
		btnIndices.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!comboIndices.getText().equals("")) {
					if (fieldStock.getText().equals("")) {
						fieldStock
								.setText(comboIndices.getText().toUpperCase());
					} else {
						fieldStock.setText(fieldStock.getText().trim()
								.toUpperCase()
								+ ";" + comboIndices.getText().toUpperCase());
					}
					comboIndices.getTextEditor().setText("");
				}
			}
		});
		FormLayout ll = new FormLayout(
				"250px:grow,75px,pref, 2dlu, pref, 2dlu, pref, 2dlu, pref,2dlu,pref",
				"pref, 2dlu, pref");
		PanelBuilder bb = new PanelBuilder(ll, pnlInput);
		pnlInput.setBorder(new EmptyBorder(5, 5, 5, 5));
		CellConstraints c = new CellConstraints();
		JLabel temp = new JLabel("used ';' for delimiter, Stock ");
		temp.setHorizontalAlignment(SwingConstants.RIGHT);
		bb.add(fieldStock, c.xy(1, 1));
		bb.add(new JLabel("from"), c.xy(3, 1));
		bb.add(dfFrom, c.xy(5, 1));
		bb.add(new JLabel("to"), c.xy(7, 1));
		bb.add(dfTo, c.xy(9, 1));
		bb.add(btnPreview, c.xy(11, 1));
		bb.add(temp, c.xy(1, 3));
		bb.add(comboStock, c.xy(3, 3));
		bb.add(btnStock, c.xy(5, 3));
		bb.add(new JLabel("Sector"), c.xy(7, 3));
		bb.add(comboIndices, c.xy(9, 3));
		bb.add(btnIndices, c.xy(11, 3));

		btnPreview.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					
				fieldStockAction();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
		});

		fieldStock.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				fieldStockAction();
			}

			@Override
			public void focusGained(FocusEvent e) {
				fieldStock.selectAll();
			}
		});
		fieldStock.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fieldStockAction();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		refresh();

		if (hSetting.get("stock") != null) {
			fieldStock.setText((String) hSetting.get("stock"));
		} else {
			fieldStock.setText("COMPOSITE");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -3);
		dfFrom.setValue(cal.getTime());
		dfTo.setValue(new Date());
		fieldStockAction();

		chart = new ChartContainerDaily();
		chart.getNumberAxis().setNumberFormatOverride(
				new DecimalFormat("#,##0.00%"));
		chart.getPanel().setPreferredSize(new Dimension(200, 300));
		StandardChartTheme.createDarknessTheme().apply(
				chart.getPanel().getChart());
		render = new XYLineAndShapeRenderer(true, false);
		xydataset = new TimeSeriesCollection();
		chart.addPlot("", name, xydataset, null, render);
		render.setBaseToolTipGenerator(new StandardXYToolTipGenerator(
				"({1}, {2})", new SimpleDateFormat("dd-MM-yyyy"),
				new DecimalFormat("0.00")));
		// render.setSeriesStroke(0, new BasicStroke(1F));
		pnlContent = new JSkinPnl();
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(bb.getPanel(), BorderLayout.NORTH);
		pnlContent.add(chart.getPanel(), BorderLayout.CENTER);
		refresh();
	}

	private void setEnabled(boolean enabled) {
		fieldStock.setEnabled(enabled);
		dfFrom.setEnabled(enabled);
		dfTo.setEnabled(enabled);
		btnPreview.setEnabled(enabled);
	}

	private void fieldStockAction() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (((Date) dfTo.getValue()).getTime() < ((Date) dfFrom
						.getValue()).getTime()) {
					Utils.showMessage(
							"Aborted, please enter valid date parameter", null);
					return;
				}
				setEnabled(false);
				String newStock = fieldStock.getText().trim().toUpperCase();
				fieldStock.setText(newStock);
				oldStock = newStock;
				xydataset.removeAllSeries();
				queryChart(oldStock);
				chart.getDateAxis().setRangeDate((Date) dfFrom.getValue(),
						(Date) dfTo.getValue());//update-280814
				// String stock[] = oldStock.split("\\;");
				// for (int i=0; i<stock.length; i++){
				// if (!stock[i].equals("")){
				// queryChart(stock[i]);
				// }
				// }
				// setEnabled(true);
				// fieldStock.selectAll();
			}
		});
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 630, 360));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put("from", dfFrom.getValue());
		hSetting.put("to", dfTo.getValue());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		pnlInput.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
	}

	Thread threadLoad = null;

	private void queryChart(final String code) {
		if (threadLoad != null) {
			threadLoad.stop();
			threadLoad = null;
		}
		threadLoad = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					String stock[] = code.split("\\;");
					for (int i = 0; i < stock.length; i++) {
						if (!stock[i].equals("")) {
							TimeSeries timeseries = new TimeSeries(stock[i]);
							String from = C_DATEFORMAT.format(
									(Date) dfFrom.getValue()).trim();
							String to = C_DATEFORMAT.format(
									(Date) dfTo.getValue()).trim();

							String result = ((IEQTradeApp) apps)
									.getFeedEngine()
									.getEngine()
									.getConnection()
									.getHistory(
											"CHC" + "|"
													+ stock[i].toUpperCase()
													+ "|" + from + "|" + to);
							if (result != null) {
								String as[] = result.split("\n", -2);
								int aslength = as.length - 1;
								for (int j = 0; j < aslength; j++) {
									String s1 = as[j];
									if (!s1.trim().equals("")) {
										String detail[] = s1.split("\\|");
										Date t = timeFormat.parse(detail[0]
												+ " 000000");
										timeseries.addOrUpdate(Second
												.parseSecond(datetimeFormat
														.format(t)), Double
												.parseDouble(detail[2]) / 100);
									}
								}
								xydataset.addSeries(timeseries);
								chart.updateDataset("", name, xydataset);
								chart.getPanel().repaint();
							}
						}
					}
					setEnabled(true);
					fieldStock.selectAll();
				} catch (Exception ex) {
					xydataset.removeAllSeries();
					setEnabled(true);
					fieldStock.selectAll();
					oldStock = "";
					Utils.showMessage("failed, please try again",
							UIChartCompare.this.form);
					ex.printStackTrace();
				}
			}
		});
		threadLoad.start();
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
			fieldStock.setText(par.get("STOCK").toString());
			fieldStockAction();
		}
		show();
	}

	private static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat(
			"yyyy-MM-dd HHmmss");
	private static SimpleDateFormat datetimeFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	static class CustomDateAxis extends DateAxis {
		private static final long serialVersionUID = 4239630992518135526L;
		private Date base;

		public CustomDateAxis(String s, Date date, SegmentedTimeline timeline) {
			super(s);
			base = date;
			this.setTimeline(timeline);
			try {
				this.setAutoRange(true);
				this.setRange(
						timeFormat.parse(dateFormat.format(new Date())
								+ " 093000"),
						timeFormat.parse(dateFormat.format(new Date())
								+ " 160000"));
			} catch (Exception ex) {
			}
		}

		@Override
		public List refreshTicks(java.awt.Graphics2D graphics2d,
				AxisState axisstate, java.awt.geom.Rectangle2D rectangle2d,
				RectangleEdge rectangleedge) {
			ArrayList arraylist = new ArrayList();
			try {
				Date start = timeFormat.parse(dateFormat.format(base)
						+ " 093000");
				arraylist.add(new DateTick(start, "09:30", TextAnchor.TOP_LEFT,
						TextAnchor.CENTER_LEFT, 0.0D));
				Date middle = timeFormat.parse(dateFormat.format(base)
						+ " 133000");
				arraylist.add(new DateTick(middle, "13:30",
						TextAnchor.TOP_CENTER, TextAnchor.CENTER, 0.0D));
				Date end = timeFormat
						.parse(dateFormat.format(base) + " 160000");
				arraylist.add(new DateTick(end, "16:00", TextAnchor.TOP_RIGHT,
						TextAnchor.CENTER_RIGHT, 0.0D));
			} catch (Exception ex) {
			}
			return arraylist;
		}
	}

}
