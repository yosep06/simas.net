package eqtrade.feed.ui.chart;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import net.sf.nachocalendar.CalendarFactory;
import net.sf.nachocalendar.components.DateField;

import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.XYPlot;

import vollux.chart.core.ChartController;
import vollux.chart.core.CustomDateAxis;
import vollux.chart.ext.ICallback;
import vollux.chart.ext.IChartDatasource;
import vollux.chart.ext.IResult;
import vollux.chart.indicator.BBANDSIndicator;
import vollux.chart.indicator.CandleIndicator;
import vollux.chart.indicator.LineIndicator;
import vollux.chart.indicator.MACDIndicator;
import vollux.chart.indicator.MAIndicator;
import vollux.chart.indicator.STOCHIndicator;
import vollux.chart.model.Chart;
import vollux.chart.util.ChartUtil;
import vollux.chart.util.FormattedTextField;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IMessage;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Indices;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.StockSummary;
import feed.admin.Utils;

public class UIChart extends UI implements TableModelListener, ICallback,
		IChartDatasource , IMessage{
	private FormattedTextField fieldCode;
	private JComboBox cmbPeriod;
	private JComboBox cmbType;
	private DateField fieldFrom;
	private DateField fieldTo;
	private JButton btnDraw;

	private JLabel fieldLast;
	private JLabel fieldChange;
	private JLabel fieldOpen;
	private JLabel fieldHigh;
	private JLabel fieldLow;
	private JLabel fieldPrev;

	private JNumber macdFast;
	private JNumber macdSlow;
	private JNumber macdSignal;
	private JButton btnMACDhide;
	private JButton btnMACDdraw;

	private JNumber stochFastK;
	private JNumber stochSlowK;
	private JNumber stochSlowD;
	private JButton btnStochHide;
	private JButton btnStochDraw;

	private JNumber ma1;
	private JNumber ma2;
	private JNumber ma3;
	private JComboBox maType;
	private JButton btnMAhide;
	private JButton btnMAdraw;

	private JNumber bbPeriod;
	private JNumber bbUpper;
	private JNumber bbLower;
	private JButton btnBBhide;
	private JButton btnBBdraw;

	private ChartController controller;
	private CustomDateAxis axis;

	private MACDIndicator macd;
	private STOCHIndicator stoch;
	private CandleIndicator candle;
	private BBANDSIndicator bband;
	private LineIndicator line;
	private MAIndicator ma1ind;
	private MAIndicator ma2ind;
	private MAIndicator ma3ind;

	private static final SimpleDateFormat year = new SimpleDateFormat("yyyy");
	private Calendar cal = Calendar.getInstance();
	private String oldCode = "";

	IResult proses ;
	SimpleDateFormat frmt = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat day = new SimpleDateFormat("EEE");
	
	public UIChart(String app) {
		super("Technical Charting", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_CHART);
	}

	Thread threadLoad = null;
	ThreadUpdate threadupdate =null;
	public void getData(final String type, final String stock,
			final String from, final String to, boolean adjusted,
			final IResult process, final ICallback callback) {
		if (threadLoad != null) {
			threadLoad.stop();
			threadLoad = null;
		}
		setNav(false);
		final Vector row = new Vector(100, 5);
		threadLoad = new Thread(new Runnable() {
			public void run() {
				try {
					String result = ((IEQTradeApp) apps)
							.getFeedEngine()
							.getEngine()
							.getConnection()
							.getHistory(
									"CHD" + "|" + stock.toUpperCase().trim()
											+ "|" + from + "|" + to);
					//log.info("RESULT TC : "+result);
					//Vector row = new Vector(100, 5);
					if (result != null) {
						String as[] = result.split("\n", -2);
						int aslength = as.length - 1;
						for (int j = 0; j < aslength; j++) {
							String s1 = as[j];
							if (!s1.trim().equals("")) {
								String detail[] = s1.split("\\|");
								// 2010-03-09|JII|406.736|412.178|406.736|411.908|0.0
								if (!day.format(frmt.parse(detail[0])).equalsIgnoreCase("Sat")) {
									Chart c = new Chart();
									c.setStock(stock);
									c.setDate(detail[0]);
									c.setTime("");
									c.setOpen(detail[2]);
									c.setHigh(detail[3]);
									c.setLow(detail[4]);
									c.setClose(detail[5]);
									c.setVolume(detail[6]);
									row.add(0, c);									
								}
							}
						}
						if (frmt.format((Date) fieldTo.getValue()).equalsIgnoreCase(frmt.format(new Date()))) {
							int m = ((IEQTradeApp)apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).getIndexByKey(new String[]{fieldCode.getText(),"RG"});
							if (m>-1) {
								StockSummary stock = (StockSummary) ((IEQTradeApp)apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new String[]{fieldCode.getText(),"RG"});
								if (stock.getOpening() >0 ){
									Chart c = new Chart();
									c.setStock(stock.getCode());
									c.setDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
									c.setTime("");
									c.setOpen(stock.getOpening().toString());
									c.setHigh(stock.getHighest().toString());
									c.setLow(stock.getLowest().toString());
									c.setClose(stock.getClosing().toString());
									c.setVolume(stock.getTradedVolume().toString());
									row.set(row.size()-1, c);	
								}
							}
							
						}
						process.receiveData(row);
						callback.success();
					} else {
						process.receiveData(row);
						callback.failed("no data available");
					}
				} catch (Exception ex) {
					Utils.showMessage("failed, please try again", null);
					oldCode = "";
					callback.failed("failed, please try again");
					ex.printStackTrace();
				}
			}
		});
		threadLoad.start();
		if (threadupdate != null) {
			threadupdate.stop();
			threadupdate = null;
		}
		threadupdate = new ThreadUpdate(process, callback,row);
		threadupdate.start();
	}

	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				fieldCode.requestFocus();
			}
		});
	}

	protected void build() {
		// source = new YahooSource();
		controller = new ChartController(this);
		axis = new CustomDateAxis("", new Date(), new Date(), controller
				.getContainer().getTimeline());
		XYPlot xyplot = controller.getContainer().getPlot();
		xyplot.setDomainAxis(axis);
		StandardChartTheme.createDarknessTheme().apply(
				controller.getContainer().getPanel().getChart());

		macd = new MACDIndicator(controller, "macd", "");
		macd.show();
		stoch = new STOCHIndicator(controller, "stochastic", "");
		stoch.show();
		candle = new CandleIndicator(controller, "candle stick", "");
		bband = new BBANDSIndicator(controller, "BBands", "");
		line = new LineIndicator(controller, "line", "");
		ma1ind = new MAIndicator(controller, "MA1", "");
		ma2ind = new MAIndicator(controller, "MA2", "");
		ma2ind.setPeriod(20);
		ma3ind = new MAIndicator(controller, "MA3", "");
		ma3ind.setPeriod(50);
		ma1ind.setColor(Color.blue);
		ma2ind.setColor(Color.cyan);
		ma3ind.setColor(Color.magenta);
		ma1ind.show();
		ma2ind.show();
		ma3ind.show();
		candle.show();
		JComponent temp = buildPanel();
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.add(new JScrollPane(temp), BorderLayout.CENTER);
		((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCKSUMMARY)
				.addTableModelListener(this);
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_INDICES)
				.addTableModelListener(this);
		tableChanged(null);
	}

	private JComponent buildPanel() {
		initComponents();
		CellConstraints c = new CellConstraints();
		FormLayout paramLayout = new FormLayout("pref",
				"pref,pref,5dlu,pref,pref,5dlu,pref,pref,2dlu, pref, 2dlu, pref,2dlu,pref");
		PanelBuilder param = new PanelBuilder(paramLayout);
		param.setDefaultDialogBorder();

		FormLayout entryLayout = new FormLayout(
				"pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px",
				"pref, 2dlu, pref, 2dlu, pref"); // rows
		PanelBuilder entry = new PanelBuilder(entryLayout);

		entry.add(new JLabel("Code"), c.xy(1, 1));
		entry.add(fieldCode, c.xyw(3, 1, 3));
		entry.add(btnDraw, c.xy(7, 1));
		entry.add(new JLabel("Type"), c.xy(1, 3));
		entry.add(cmbType, c.xy(3, 3));
		entry.add(new JLabel("Period"), c.xy(5, 3));
		entry.add(cmbPeriod, c.xy(7, 3));
		entry.add(new JLabel("from"), c.xy(1, 5));
		entry.add(fieldFrom, c.xy(3, 5));
		entry.add(new JLabel("to"), c.xy(5, 5));
		entry.add(fieldTo, c.xy(7, 5));

		FormLayout todayLayout = new FormLayout(
				"pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px",
				"pref, 2dlu, pref, 2dlu, pref");
		PanelBuilder today = new PanelBuilder(todayLayout);
		today.add(new JLabel("last"), c.xy(1, 1));
		today.add(fieldLast, c.xy(3, 1));
		today.add(new JLabel("hi"), c.xy(5, 1));
		today.add(fieldHigh, c.xy(7, 1));
		today.add(new JLabel("chg"), c.xy(1, 3));
		today.add(fieldChange, c.xy(3, 3));
		today.add(new JLabel("low"), c.xy(5, 3));
		today.add(fieldLow, c.xy(7, 3));
		today.add(new JLabel("open"), c.xy(1, 5));
		today.add(fieldOpen, c.xy(3, 5));
		today.add(new JLabel("prev"), c.xy(5, 5));
		today.add(fieldPrev, c.xy(7, 5));
		today.getPanel().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		today.setDefaultDialogBorder();

		FormLayout macdLayout = new FormLayout(
				"40px:grow, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px",
				"pref, 2dlu, pref");
		PanelBuilder macd = new PanelBuilder(macdLayout);
		macd.addSeparator("MACD", c.xyw(1, 1, 3));
		macd.add(btnMACDhide, c.xyw(5, 1, 3));
		macd.add(btnMACDdraw, c.xyw(9, 1, 3));
		macd.add(new JLabel("fast"), c.xy(1, 3));
		macd.add(macdFast, c.xy(3, 3));
		macd.add(new JLabel("slow"), c.xy(5, 3));
		macd.add(macdSlow, c.xy(7, 3));
		macd.add(new JLabel("signal"), c.xy(9, 3));
		macd.add(macdSignal, c.xy(11, 3));
		macd.getPanel().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		macd.setDefaultDialogBorder();

		FormLayout stochLayout = new FormLayout(
				"40px:grow, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px",
				"pref, 2dlu, pref");
		PanelBuilder stoch = new PanelBuilder(stochLayout);
		stoch.addSeparator("Stochastic", c.xyw(1, 1, 3));
		stoch.add(btnStochHide, c.xyw(5, 1, 3));
		stoch.add(btnStochDraw, c.xyw(9, 1, 3));
		stoch.add(new JLabel("fast%k"), c.xy(1, 3));
		stoch.add(stochFastK, c.xy(3, 3));
		stoch.add(new JLabel("slow%k"), c.xy(5, 3));
		stoch.add(stochSlowK, c.xy(7, 3));
		stoch.add(new JLabel("slow%d"), c.xy(9, 3));
		stoch.add(stochSlowD, c.xy(11, 3));
		stoch.getPanel().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN));
		stoch.setDefaultDialogBorder();

		FormLayout maLayout = new FormLayout(
				"40px:grow, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px",
				"pref, 2dlu, pref, 2dlu, pref");
		PanelBuilder ma = new PanelBuilder(maLayout);
		ma.addSeparator("Moving Avg", c.xyw(1, 1, 3));
		ma.add(btnMAhide, c.xyw(5, 1, 3));
		ma.add(btnMAdraw, c.xyw(9, 1, 3));
		ma.add(new JLabel("ma1"), c.xy(1, 3));
		ma.add(ma1, c.xy(3, 3));
		ma.add(new JLabel("ma2"), c.xy(5, 3));
		ma.add(ma2, c.xy(7, 3));
		ma.add(new JLabel("ma3"), c.xy(9, 3));
		ma.add(ma3, c.xy(11, 3));
		ma.add(new JLabel("type"), c.xy(1, 5));
		ma.add(maType, c.xyw(3, 5, 9));
		ma.getPanel().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		ma.setDefaultDialogBorder();

		FormLayout bbLayout = new FormLayout(
				"40px:grow, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px",
				"pref, 2dlu, pref");
		PanelBuilder bb = new PanelBuilder(bbLayout);
		bb.addSeparator("BBands", c.xyw(1, 1, 3));
		bb.add(btnBBhide, c.xyw(5, 1, 3));
		bb.add(btnBBdraw, c.xyw(9, 1, 3));
		bb.add(new JLabel("period"), c.xy(1, 3));
		bb.add(bbPeriod, c.xy(3, 3));
		bb.add(new JLabel("upper"), c.xy(5, 3));
		bb.add(bbUpper, c.xy(7, 3));
		bb.add(new JLabel("lower"), c.xy(9, 3));
		bb.add(bbLower, c.xy(11, 3));
		bb.getPanel().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN));
		bb.setDefaultDialogBorder();

		param.addSeparator("Parameter", c.xy(1, 1));
		param.add(entry.getPanel(), c.xy(1, 2));
		param.addSeparator("Today", c.xy(1, 4));
		param.add(today.getPanel(), c.xy(1, 5));
		param.addSeparator("Technical Indicator", c.xy(1, 7));
		param.add(macd.getPanel(), c.xy(1, 8));
		param.add(stoch.getPanel(), c.xy(1, 10));
		param.add(ma.getPanel(), c.xy(1, 12));
		param.add(bb.getPanel(), c.xy(1, 14));

		JPanel content = new JPanel(new BorderLayout());
		content.add(param.getPanel(), BorderLayout.EAST);
		content.add(controller.getContainer().getPanel(), BorderLayout.CENTER);
		return content;
	}

	public static final SimpleDateFormat C_DATEFORMAT = new SimpleDateFormat(
			"yyyy-MM-dd");

	private void initComponents() {
		fieldCode = new FormattedTextField();
		cmbPeriod = new JComboBox(new Object[] { "1 Month", "3 Month",
				"6 Month", "Year to Date", "1 Year", "2 Year", "5 Year",
				"Custom" });
		cmbType = new JComboBox(new Object[] { "candlestick", "line" });
		fieldFrom = CalendarFactory.createDateField();
		fieldFrom.setDateFormat(C_DATEFORMAT);
		fieldTo = CalendarFactory.createDateField();
		fieldTo.setDateFormat(C_DATEFORMAT);
		btnDraw = new JButton("Draw");

		fieldLast = new JLabel(" ", JLabel.RIGHT);
		fieldChange = new JLabel(" ", JLabel.RIGHT);
		fieldOpen = new JLabel(" ", JLabel.RIGHT);
		fieldHigh = new JLabel(" ", JLabel.RIGHT);
		fieldLow = new JLabel(" ", JLabel.RIGHT);
		fieldPrev = new JLabel(" ", JLabel.RIGHT);

		macdFast = new JNumber(Double.class, 0, 0, true);
		macdSlow = new JNumber(Double.class, 0, 0, true);
		macdSignal = new JNumber(Double.class, 0, 0, true);
		btnMACDhide = new JButton("hide");
		btnMACDdraw = new JButton("draw");
		macdFast.setValue(new Double(macd.getFastPeriod()));
		macdSlow.setValue(new Double(macd.getSlowPeriod()));
		macdSignal.setValue(new Double(macd.getSignalPeriod()));
		btnMACDhide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				macd.hide();
			}
		});
		btnMACDdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				macd.hide();
				macd.setFastPeriod(((Double) macdFast.getValue()).intValue());
				macd.setSlowPeriod(((Double) macdSlow.getValue()).intValue());
				macd.setSignalPeriod(((Double) macdSignal.getValue())
						.intValue());
				macd.show();
			}
		});

		stochFastK = new JNumber(Double.class, 0, 0, true);
		stochSlowK = new JNumber(Double.class, 0, 0, true);
		stochSlowD = new JNumber(Double.class, 0, 0, true);
		btnStochHide = new JButton("hide");
		btnStochDraw = new JButton("draw");
		stochFastK.setValue(new Double(stoch.getFastKPeriod()));
		stochSlowK.setValue(new Double(stoch.getSlowKPeriod()));
		stochSlowD.setValue(new Double(stoch.getSlowDPeriod()));
		btnStochHide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stoch.hide();
			}
		});
		btnStochDraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stoch.hide();
				stoch.setFastKPeriod(((Double) stochFastK.getValue())
						.intValue());
				stoch.setSlowKPeriod(((Double) stochSlowK.getValue())
						.intValue());
				stoch.setSlowDPeriod(((Double) stochSlowD.getValue())
						.intValue());
				stoch.show();
			}
		});

		ma1 = new JNumber(Double.class, 0, 0, true);
		ma2 = new JNumber(Double.class, 0, 0, true);
		ma3 = new JNumber(Double.class, 0, 0, true);
		maType = ChartUtil.getComboMA();
		btnMAhide = new JButton("hide");
		btnMAdraw = new JButton("draw");
		ma1.setValue(new Double(ma1ind.getPeriod()));
		ma2.setValue(new Double(ma2ind.getPeriod()));
		ma3.setValue(new Double(ma3ind.getPeriod()));
		ma1ind.setColor(Color.blue);
		ma2ind.setColor(Color.cyan);
		ma3ind.setColor(Color.magenta);
		btnMAhide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ma1ind.hide();
				ma2ind.hide();
				ma3ind.hide();
			}
		});
		btnMAdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ma1ind.hide();
				ma2ind.hide();
				ma3ind.hide();
				ma1ind.setPeriod(((Double) ma1.getValue()).intValue());
				ma2ind.setPeriod(((Double) ma2.getValue()).intValue());
				ma3ind.setPeriod(((Double) ma3.getValue()).intValue());
				ma1ind.setType(maType.getSelectedIndex());
				ma2ind.setType(maType.getSelectedIndex());
				ma3ind.setType(maType.getSelectedIndex());
				ma1ind.show();
				ma2ind.show();
				ma3ind.show();
			}
		});

		bbPeriod = new JNumber(Double.class, 0, 0, true);
		bbUpper = new JNumber(Double.class, 1, 1, true);
		bbLower = new JNumber(Double.class, 1, 1, true);
		btnBBhide = new JButton("hide");
		btnBBdraw = new JButton("draw");
		bbPeriod.setValue(new Double(bband.getPeriod()));
		bbUpper.setValue(new Double(bband.getUpperDeviation()));
		bbLower.setValue(new Double(bband.getLowerDeviation()));
		btnBBhide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bband.hide();
			}
		});
		btnBBdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bband.hide();
				bband.setPeriod(((Double) bbPeriod.getValue()).intValue());
				bband.setUpperDeviation(((Double) bbUpper.getValue())
						.doubleValue());
				bband.setLowerDeviation(((Double) bbLower.getValue())
						.doubleValue());
				bband.show();
			}
		});
		cmbType.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				line.hide();
				candle.hide();
				if (cmbType.getSelectedIndex() == 0)
					candle.show();
				else
					line.show();
			}
		});

		cmbPeriod.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				fieldTo.setValue(new Date());
				cal.setTime(new Date());
				if (cmbPeriod.getSelectedItem().equals("1 Month")) {
					cal.add(Calendar.MONTH, -1);
				} else if (cmbPeriod.getSelectedItem().equals("3 Month")) {
					cal.add(Calendar.MONTH, -3);
				} else if (cmbPeriod.getSelectedItem().equals("6 Month")) {
					cal.add(Calendar.MONTH, -6);
				} else if (cmbPeriod.getSelectedItem().equals("Year to Date")) {
					try {
						cal.setTime(ChartUtil.C_FORMAT2.parse(year
								.format(new Date()) + "/01/01"));
					} catch (Exception ex) {
					}
				} else if (cmbPeriod.getSelectedItem().equals("1 Year")) {
					cal.add(Calendar.YEAR, -1);
				} else if (cmbPeriod.getSelectedItem().equals("2 Year")) {
					cal.add(Calendar.YEAR, -2);
				} else if (cmbPeriod.getSelectedItem().equals("5 Year")) {
					cal.add(Calendar.YEAR, -5);
				}
				fieldFrom.setValue(cal.getTime());
				fieldFrom.setEnabled(cmbPeriod.getSelectedItem().equals(
						"Custom"));
				fieldTo.setEnabled(cmbPeriod.getSelectedItem().equals("Custom"));

			}
		});
		cmbPeriod.setSelectedItem("6 Month");

		fieldCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				fieldCode.setText(fieldCode.getText().toUpperCase());
			}
		});
		fieldCode.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				fieldCode.selectAll();
				previewChart();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		btnDraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				previewChart();
			}
		});
	}

	public void previewChart() {
		if ("".equals(fieldCode.getText().trim())) {
			// JOptionPane.showMessageDialog(null,
			// "Please insert Securities ... !!!", "Error",
			// JOptionPane.ERROR_MESSAGE);
			fieldCode.requestFocus();
		} else if (((Date)fieldFrom.getValue() ).after((Date)fieldTo.getValue()) ){
			Utils.showMessage("Date from cannot be more than Date to ", null);
		}else if (((Date)fieldFrom.getValue() ).equals((Date)fieldTo.getValue()) ){
			Utils.showMessage("Date from cannot be equals Date to ", null);
		}else {
			axis.setRangeDate((Date) fieldFrom.getValue(),
					(Date) fieldTo.getValue());
			controller.getContainer().getPanel().restoreAutoBounds();
			oldCode = fieldCode.getText().toUpperCase();
			tableChanged(null);
			controller.getDataset().loadData(
					cmbPeriod.getSelectedItem().toString(),
					fieldCode.getText().trim().toUpperCase(),
					C_DATEFORMAT.format(fieldFrom.getValue()),
					C_DATEFORMAT.format(fieldTo.getValue()), false, this);
		}
	}

	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 1000, 560));
	}

	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	public void refresh() {
		SwingUtilities.updateComponentTreeUI(controller.getContainer()
				.getPanel());
		tableChanged(null);
	}

	private static NumberFormat format = new DecimalFormat("#,##0 ");
	private static NumberFormat format3 = new DecimalFormat("#,##0.00 ");

	public void tableChanged(TableModelEvent arg0) {
		StockSummary ss = (StockSummary) ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCKSUMMARY)
				.getDataByKey(new Object[] { oldCode, "RG" });
		Indices index = (Indices) ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_INDICES)
				.getDataByKey(new Object[] { oldCode });
		if (ss != null) {
			fieldLast.setText(format.format(ss.getClosing()));
			fieldChange.setText(format.format(ss.getChange()) + "("
					+ format.format(ss.getPercent()) + "%) ");
			fieldOpen.setText(format.format(ss.getOpening()));
			fieldHigh.setText(format.format(ss.getHighest()));
			fieldLow.setText(format.format(ss.getLowest()));
			fieldPrev.setText(format.format(ss.getPrevious()));
			fieldLast
					.setForeground(ss.getChange().doubleValue() > 0 ? FeedSetting
							.getColor(FeedSetting.C_PLUS) : (ss.getChange()
							.doubleValue() < 0) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldChange
					.setForeground(ss.getChange().doubleValue() > 0 ? FeedSetting
							.getColor(FeedSetting.C_PLUS) : (ss.getChange()
							.doubleValue() < 0) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldOpen.setForeground(ss.getOpening().doubleValue() > ss
					.getPrevious().doubleValue() ? FeedSetting
					.getColor(FeedSetting.C_PLUS)
					: (ss.getOpening().doubleValue() < ss.getPrevious()
							.doubleValue()) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldHigh.setForeground(ss.getHighest().doubleValue() > ss
					.getClosing().doubleValue() ? FeedSetting
					.getColor(FeedSetting.C_PLUS)
					: (ss.getHighest().doubleValue() < ss.getClosing()
							.doubleValue()) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldLow.setForeground(ss.getLowest().doubleValue() > ss
					.getClosing().doubleValue() ? FeedSetting
					.getColor(FeedSetting.C_PLUS)
					: (ss.getLowest().doubleValue() < ss.getClosing()
							.doubleValue()) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldPrev.setForeground(FeedSetting.getColor(FeedSetting.C_ZERO));
		} else if (index != null) {
			fieldLast.setText(format3.format(index.getLast()));
			fieldChange.setText(format3.format(index.getChange()) + "("
					+ format3.format(index.getPercent()) + "%) ");
			fieldOpen.setText(format3.format(index.getOpen()));
			fieldHigh.setText(format3.format(index.getHigh()));
			fieldLow.setText(format3.format(index.getLow()));
			fieldPrev.setText(format3.format(index.getPrev()));
			fieldLast
					.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
							.getColor(FeedSetting.C_PLUS) : (index.getChange()
							.doubleValue() < 0) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldChange
					.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
							.getColor(FeedSetting.C_PLUS) : (index.getChange()
							.doubleValue() < 0) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldOpen.setForeground(index.getOpen().doubleValue() > index
					.getPrev().doubleValue() ? FeedSetting
					.getColor(FeedSetting.C_PLUS)
					: (index.getOpen().doubleValue() < index.getPrev()
							.doubleValue()) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldHigh.setForeground(index.getHigh().doubleValue() > index
					.getLast().doubleValue() ? FeedSetting
					.getColor(FeedSetting.C_PLUS)
					: (index.getHigh().doubleValue() < index.getLast()
							.doubleValue()) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldLow.setForeground(index.getLow().doubleValue() > index
					.getLast().doubleValue() ? FeedSetting
					.getColor(FeedSetting.C_PLUS)
					: (index.getLow().doubleValue() < index.getLast()
							.doubleValue()) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldPrev.setForeground(FeedSetting.getColor(FeedSetting.C_ZERO));
		} else {
			fieldLast.setText("- ");
			fieldChange.setText("- ");
			fieldOpen.setText("- ");
			fieldHigh.setText("- ");
			fieldLow.setText("- ");
			fieldPrev.setText("- ");
		}
	}

	private void setNav(boolean state) {
		fieldCode.setEnabled(state);
		btnDraw.setEnabled(state);
		if (state) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					fieldCode.requestFocus();
					fieldCode.selectAll();
				}
			});
		}
	}

	public void failed(String reason) {
		setInfo(reason);
		setNav(true);
	}

	public void setStatus(String status) {
		setInfo(status);
	}

	public void setWait(boolean b) {
	}

	public void success() {
		setInfo("");
		setNav(true);
	}

	private void setInfo(final String state) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				form.setTitle("Technical Charting "
						+ (state.equals("") ? "" : "(" + state + ")"));
			}
		});
	}

	public void show() {
		((IEQTradeApp)apps).getFeedEngine().getEngine().getParser()
		.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
		setNav(true);
		super.show();
	}

	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
			fieldCode.setText(par.get("STOCK").toString());
			previewChart();
		}
		show();
	}

	@Override
	public void close() {
		((IEQTradeApp)apps).getFeedEngine().getEngine().getParser()
		.get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
		// TODO Auto-generated method stub
		super.close();
	}

	@Override
	public void hide() {
		((IEQTradeApp)apps).getFeedEngine().getEngine().getParser()
		.get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
		// TODO Auto-generated method stub
		super.hide();
	}

	@Override
	public void newMessage(Model model) {
		// TODO Auto-generated method stub
		StockSummary stock = (StockSummary) model;
		if (stock.getCode().equalsIgnoreCase(fieldCode.getText())
			  && threadupdate !=null && frmt.format((Date) fieldTo.getValue()).equalsIgnoreCase(frmt.format(new Date())) ) {
			Vector msg = new Vector();
			msg.add(stock);
			threadupdate.addmessage(msg);
		}
	}
	
	
	class ThreadUpdate extends Thread {
	IResult process;
	ICallback callback;
	Vector vmsg = new Vector();
	Vector row = new Vector(100, 5);
	
	public ThreadUpdate(final IResult process, final ICallback callback, Vector row){
		this.process  = process;
		this.callback = callback;
		this.row = row;
	}
	
	public void addmessage(Vector msg){
		try {
			synchronized (vmsg) {
				vmsg.addAll(msg);
				vmsg.notify();
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
		@Override
		public void run() {
			// TODO Auto-generated method stub
			while(true){
				if (vmsg.size() == 0) {
					synchronized (vmsg) {
						try {
							vmsg.wait();
						} catch (InterruptedException e) {
							// TODO: handle exception
						}	
					}
				} else if(row.size()>0) {
					// 2010-03-09|JII|406.736|412.178|406.736|411.908|0.0
					System.out.println(row.size()+" ");
					Chart c1 = (Chart)row.get(row.size()-1);
					System.out.println(c1.getDate()+" -date");
					StockSummary stock  = (StockSummary) vmsg.remove(0);
//					Vector row = new Vector(100, 5);
					Chart c = new Chart();
					c.setStock(stock.getCode());
					c.setDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
					c.setTime("");
					c.setOpen(stock.getOpening().toString());
					c.setHigh(stock.getHighest().toString());
					c.setLow(stock.getLowest().toString());
					c.setClose(stock.getClosing().toString());
					c.setVolume(stock.getTradedVolume().toString());
					if (c1.getDate().equalsIgnoreCase(new SimpleDateFormat("yyyy-MM-dd").format(new Date())+" 16:00")) {
						row.set(row.size()-1, c);
					} else {
						row.add(row.size(), c);
					}
						process.receiveData(row);
				}
			}

		}
		
		
	}
}
