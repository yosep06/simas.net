package eqtrade.feed.ui.broker;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.vollux.framework.UI;
import com.vollux.model.TableModelSorterEvent;
import com.vollux.model.TableModelSorterListener;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;

public class UITopBroker extends UI {
	private JGrid table;

	public UITopBroker(String app) {
		super("Top Broker", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_TOPBROKER);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.requestFocus();
			}
		});
	}

	private void changeTitle(int sort, boolean asc, String header) {
		String col = TradeSummaryDef.dataHeader[sort];
		if (asc) {
			if (form != null)
				form.setTitle("Top Losser Broker (" + col + ") " + header);
		} else {
			if (form != null)
				form.setTitle("Top Gainer Broker (" + col + ") " + header);
		}
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem mnStockSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMBROKER)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("BROKER", order.getBroker());
					apps.getUI().showUI(FeedUI.UI_STOCKSUMMARY, param);
				}
			}
		});
		mnStockSumm.setText("Buy/Sell Stock Summary");
		popupMenu.add(mnStockSumm);

		JMenuItem mnStockActivity = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMBROKER)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("BROKER", order.getBroker());
					apps.getUI().showUI(FeedUI.UI_STOCKACTIVITY, param);
				}
			}
		});
		mnStockActivity.setText("Broker Activity by Stock");
		popupMenu.add(mnStockActivity);

	}

	@Override
	protected void build() {
		createPopup();
		table = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_TRADESUMBROKER), null,
				(Hashtable) hSetting.get("table"));
		// table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		table.getDefaultSorter().addTableModelSorterListener(
				new TableModelSorterListener() {
					@Override
					public void sortOrderChanged(TableModelSorterEvent e) {
						int sort = e.getColumn();
						boolean asc = e.isAscending();
						changeTitle(sort, asc, "");
					}
				});
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				TradeSummary.CIDX_STOCK, TradeSummary.CIDX_STOCKNAME,
				TradeSummary.CIDX_BOARD, TradeSummary.CIDX_INVESTOR,
				TradeSummary.CIDX_BUYAVG, TradeSummary.CIDX_BUYVOL,
				TradeSummary.CIDX_BUYLOT, TradeSummary.CIDX_BUYFREQ,
				TradeSummary.CIDX_SELLAVG, TradeSummary.CIDX_SELLVOL,
				TradeSummary.CIDX_SELLLOT, TradeSummary.CIDX_SELLFREQ,
				TradeSummary.CIDX_VOLNET, TradeSummary.CIDX_LOTNET,
				TradeSummary.CIDX_LOTTOT

		});

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		refresh();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 600, 350));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void show() {
		if (!form.isVisible())
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_TRADESUMMBROKER,
					FeedParser.PARSER_TRADESUMMBROKER);
		int sort = table.getSortColumn();
		boolean asc = table.getSortAscending();
		changeTitle(sort, asc, "");
		super.show();
	}

	@Override
	public void hide() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_TRADESUMMBROKER,
				FeedParser.PARSER_TRADESUMMBROKER);
		super.hide();
	}

	@Override
	public void close() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_TRADESUMMBROKER,
				FeedParser.PARSER_TRADESUMMBROKER);
		super.close();
	}
}
