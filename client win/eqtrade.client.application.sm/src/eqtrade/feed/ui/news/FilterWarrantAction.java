package eqtrade.feed.ui.news;

import java.awt.Component;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.CorpAction;

public class FilterWarrantAction extends FilterBase{

	public FilterWarrantAction(Component parent) {
		super(parent, "filter");
		mapFilter.put("action3", new FilterColumn("action2", String.class, null, FilterColumn.C_EQUAL));
		mapFilter.put("stock", new FilterColumn("stock", String.class, null, FilterColumn.C_EQUAL));
	}
	
	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			CorpAction dat = (CorpAction) val.getSource();
			 if ((((FilterColumn) mapFilter.get("action3")).compare(dat.getActiontype())) && ((FilterColumn) mapFilter.get("stock")).compare(dat.getStock())) {
				 avail = true;
			 }
			return avail;
		} catch (Exception ec) {
			return false;
		}
	}
	
	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

}
