package eqtrade.feed.ui.news;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.vollux.idata.GridModel;
import com.vollux.model.TableModelFilter;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Article;
import eqtrade.feed.model.CommodityDef;
import eqtrade.feed.model.Currency;
import eqtrade.feed.model.CurrencyDef;
import eqtrade.feed.model.FutureDef;
import eqtrade.feed.model.GlobalIndices;
import eqtrade.feed.model.GlobalIndicesDef;
import eqtrade.feed.model.Model;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.ui.CustomSelectionModel;
import eqtrade.trading.ui.FilterOrder;

public class UIIndicesCurrency extends UI {
	private JGrid tableIndices;
	private JGrid tableSpotCurrency;
	private JGrid tableCrossCurrency;
	private JGrid tableCommodity;
	private JGrid tableFuture;
	private JTabbedPane tab;
	private JLabel updateIndices;
	private JLabel updateCommodity;
	private JLabel updateCurr;
	private JLabel updateFuture;
	private FilterCurrency filterSpotCurrency;
	private FilterCurrency filterCrossCurrency;
	private static SimpleDateFormat format = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	CustomSelectionModel customSelection;
	
	public UIIndicesCurrency(String app) {
		super("World Indices & Exchange Rate", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_WORLDIDXCURR);
		this.title = "World Indices & Exchange Rate";
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tableIndices.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		System.err.println("TES1");
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				tableIndices.showProperties();
				
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		Currency cur=new Currency();
		
		tab = new JTabbedPane();
		
		updateIndices = new JLabel("-");
		updateCurr = new JLabel("-");
		updateCommodity = new JLabel("-");
		updateFuture = new JLabel("-");
		filterSpotCurrency = new FilterCurrency(pnlContent);
		((FilterColumn) filterSpotCurrency.getFilteredData("tipe")).setField("USD");
		filterCrossCurrency = new FilterCurrency(pnlContent);
		((FilterColumn) filterCrossCurrency.getFilteredData("tipe")).setField("IDR");
		tableIndices = createTable(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_GLOBALINDICES), null,
				(Hashtable) hSetting.get("tableindices"));
		//tableIndices.getTable().setAutoResizeMode(
		//		JTable.AUTO_RESIZE_ALL_COLUMNS);
		

		tableIndices.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_SEQNO, Model.CIDX_TYPE,
				 GlobalIndices.CIDX_URUTAN });
		
		tableSpotCurrency = createTable(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_CURRENCY), filterSpotCurrency,
				(Hashtable) hSetting.get("tablespotcurrency"));
		
		tableSpotCurrency.getTable().setAutoResizeMode(
				JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableSpotCurrency.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_SEQNO, Model.CIDX_TYPE });
		
		tableCrossCurrency = createTable(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_CURRENCY), filterCrossCurrency,
				(Hashtable) hSetting.get("tablecrosscurrency"));
		
		tableCrossCurrency.getTable().setAutoResizeMode(
				JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableCrossCurrency.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_SEQNO, Model.CIDX_TYPE });
		
		tableCommodity = createTable(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_COMMODITY), null,
				(Hashtable) hSetting.get("tablecommodity"));
		
	//	tableCommodity.getTable().setAutoResizeMode(
	//			JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableCommodity.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_SEQNO, Model.CIDX_TYPE });
		
		tableFuture = createTable(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_FUTURE), null,
				(Hashtable) hSetting.get("tablefuture"));
		
	//	tableFuture.getTable().setAutoResizeMode(
	//			JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableFuture.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_SEQNO, Model.CIDX_TYPE });
		
		
		JPanel temp1 = new JPanel(new BorderLayout());
		temp1.add(updateIndices, BorderLayout.NORTH);
		temp1.add(tableIndices, BorderLayout.CENTER);

		JPanel temp2 = new JPanel(new BorderLayout());
	//	temp2.add(updateCurr, BorderLayout.NORTH);
		temp2.add(tableSpotCurrency, BorderLayout.CENTER);
		
		JPanel temp3 = new JPanel(new BorderLayout());
	//	temp3.add(updateCurr, BorderLayout.NORTH);
		temp3.add(tableCrossCurrency, BorderLayout.CENTER);
		
		JPanel temp4 = new JPanel(new BorderLayout());
		temp4.add(updateCommodity, BorderLayout.NORTH);
		temp4.add(tableCommodity, BorderLayout.CENTER);
		
		JPanel temp5 = new JPanel(new BorderLayout());
		temp5.add(updateFuture, BorderLayout.NORTH);
		temp5.add(tableFuture, BorderLayout.CENTER);
		
		tab.addTab("World Indices", temp1);
		tab.addTab("Spot Rate", temp2);
		tab.addTab("Cross Rate",temp3);
		tab.addTab("Commodity",temp4);
		tab.addTab("Futures Index",temp5);
		
		//customSelection = new CustomSelectionModel();
		//customSelection.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		//tableIndices.getTable().setSelectionModel(customSelection);
		//tableIndices.getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_GLOBALINDICES)
				.addTableModelListener(new TableModelListener() {
					@Override
					public void tableChanged(TableModelEvent e) {
						priceChanged(0);
					}
				});

		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CURRENCY)
				.addTableModelListener(new TableModelListener() {
					@Override
					public void tableChanged(TableModelEvent e) {
						priceChanged(1);
					}
				});
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_COMMODITY)
		.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				priceChanged(1);
			}
		});
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_FUTURE)
		.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				priceChanged(1);
			}
		});
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(tab, BorderLayout.CENTER);
		refresh();
		priceChanged(0);
		priceChanged(1);
		
		Timer timer = new Timer();

		timer.schedule( new TimerTask() {
		
		    public void run() {
//		    	unsubscribe();
				fungsi_refresh();
		    }
		 }, 0, 60*1000);
	}
	
	public void fungsi_refresh()
	{
	((IEQTradeApp) apps).getFeedEngine().subscribe(
			FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
	((IEQTradeApp) apps).getFeedEngine().subscribe(
			FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
	((IEQTradeApp) apps).getFeedEngine().subscribe(
			FeedParser.PARSER_COMMODITY, FeedParser.PARSER_COMMODITY);
	((IEQTradeApp) apps).getFeedEngine().subscribe(
			FeedParser.PARSER_FUTURE, FeedParser.PARSER_FUTURE);	
	((IEQTradeApp) apps).getFeedEngine()
		.getStore(FeedStore.DATA_GLOBALINDICES)
		.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				priceChanged(0);
			}
		});

	((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CURRENCY)
			.addTableModelListener(new TableModelListener() {
				@Override
				public void tableChanged(TableModelEvent e) {
					priceChanged(1);
				}
			});
	((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_COMMODITY)
	.addTableModelListener(new TableModelListener() {
		@Override
		public void tableChanged(TableModelEvent e) {
			priceChanged(1);
		}
	});
	((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_FUTURE)
	.addTableModelListener(new TableModelListener() {
		@Override
		public void tableChanged(TableModelEvent e) {
			priceChanged(1);
		}
	});
	refresh();
	priceChanged(0);
	priceChanged(1);
	}
	private void priceChanged(int type) {
		String dt = "last update: " + format.format(new Date());
		if (type == 0) {
			updateIndices.setText(dt);
		} else {
			updateCommodity.setText(dt);
			updateFuture.setText(dt);
		}
	}

	/*@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		hSetting.put("tableindices", GlobalIndicesDef.getTableDef());
		hSetting.put("tablespotcurrency", CurrencyDef.getTableDef());
		hSetting.put("tablecrosscurrency", CurrencyDef.getTableDef());
		hSetting.put("tablecommodity", CommodityDef.getTableDef());
		hSetting.put("tablefuture", FutureDef.getTableDef());
		((Hashtable) hSetting.get("tableindices")).put("header",
				GlobalIndicesDef.dataHeader.clone());
		((Hashtable) hSetting.get("tablespotcurrency")).put("header",
				CurrencyDef.dataHeader.clone());
		((Hashtable) hSetting.get("tablecrosscurrency")).put("header",
				CurrencyDef.dataHeader.clone());
		((Hashtable) hSetting.get("tablecommodity")).put("header",
				CommodityDef.dataHeader.clone());
		((Hashtable) hSetting.get("tablefuture")).put("header",
				FutureDef.dataHeader.clone());
		//log.info("isi code:"+Currency.CIDX_VALUE);
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 500, 300));

		((Hashtable) hSetting.get("tableindices")).put("sortcolumn",
				new Integer(GlobalIndices.CIDX_URUTAN));
		
		

	}*/
	
	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		
		
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tableindices", GlobalIndicesDef.getTableDef());
			hSetting.put("tablespotcurrency", CurrencyDef.getTableDef());
			hSetting.put("tablecrosscurrency", CurrencyDef.getTableDef());
			hSetting.put("tablecommodity", CommodityDef.getTableDef());
			hSetting.put("tablefuture", FutureDef.getTableDef());
		}
		String[] header = (String[])((Hashtable)hSetting.get("tableindices")).get("header");
		/*for (int i = 0; i < header.length; i++) {
					System.out.println( header[i] );
					
				}*/
		if (header.length !=GlobalIndicesDef.dataHeader.length) {
			hSetting.put("tableindices", GlobalIndicesDef.getTableDef());
			hSetting.put("tablespotcurrency", CurrencyDef.getTableDef());
			hSetting.put("tablecrosscurrency", CurrencyDef.getTableDef());
			hSetting.put("tablecommodity", CommodityDef.getTableDef());
			hSetting.put("tablefuture", FutureDef.getTableDef());
		}
		
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 500, 300));		

	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("tableindices", tableIndices.getTableProperties());
		hSetting.put("tablespotcurrency", tableSpotCurrency.getTableProperties());
		hSetting.put("tablecrosscurrency", tableCrossCurrency.getTableProperties());
		hSetting.put("tablecommodity", tableCommodity.getTableProperties());
		hSetting.put("tablefuture", tableFuture.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		
		tableIndices.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableIndices.setNewFont(FeedSetting.getFont());
		tableSpotCurrency.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableSpotCurrency.setNewFont(FeedSetting.getFont());
		tableCrossCurrency.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableCrossCurrency.setNewFont(FeedSetting.getFont());
		tableCommodity.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableCommodity.setNewFont(FeedSetting.getFont());
		tableFuture.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableFuture.setNewFont(FeedSetting.getFont());
		
	}

	@Override
	public void show() {
		if (!form.isVisible()) {
			
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_COMMODITY, FeedParser.PARSER_COMMODITY);
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_FUTURE, FeedParser.PARSER_FUTURE);
		}
		super.show();
	}

	@Override
	public void hide() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_COMMODITY, FeedParser.PARSER_COMMODITY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_FUTURE, FeedParser.PARSER_FUTURE);
		super.hide();
	}

	@Override
	public void close() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_COMMODITY, FeedParser.PARSER_COMMODITY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_FUTURE, FeedParser.PARSER_FUTURE);
		super.close();
	}
	public void unsubscribe(){
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_COMMODITY, FeedParser.PARSER_COMMODITY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_FUTURE, FeedParser.PARSER_FUTURE);
	}
}

/*package eqtrade.feed.ui.news;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Article;
import eqtrade.feed.model.CommodityDef;
import eqtrade.feed.model.Currency;
import eqtrade.feed.model.CurrencyDef;
import eqtrade.feed.model.FutureDef;
import eqtrade.feed.model.GlobalIndices;
import eqtrade.feed.model.GlobalIndicesDef;
import eqtrade.feed.model.Model;

public class UIIndicesCurrency extends UI {
	private JGrid tableIndices;
	private JGrid tableSpotCurrency;
	private JGrid tableCrossCurrency;
	private JGrid tableCommodity;
	private JGrid tableFuture;
	private JTabbedPane tab;
	private JLabel updateIndices;
	private JLabel updateCommodity;
	private JLabel updateCurr;
	private JLabel updateFuture;
	private FilterCurrency filterSpotCurrency;
	private FilterCurrency filterCrossCurrency;
	private static SimpleDateFormat format = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");

	public UIIndicesCurrency(String app) {
		super("World Indices & Exchange Rate", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_WORLDIDXCURR);
		this.title = "World Indices & Exchange Rate";
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tableIndices.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				tableIndices.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		Currency cur=new Currency();
		
		tab = new JTabbedPane();
		
		updateIndices = new JLabel("-");
		updateCurr = new JLabel("-");
		updateCommodity = new JLabel("-");
		updateFuture = new JLabel("-");
		filterSpotCurrency = new FilterCurrency(pnlContent);
		((FilterColumn) filterSpotCurrency.getFilteredData("tipe")).setField("USD");
		filterCrossCurrency = new FilterCurrency(pnlContent);
		((FilterColumn) filterCrossCurrency.getFilteredData("tipe")).setField("IDR");
		
		tableIndices = createTable(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_GLOBALINDICES), null,
				(Hashtable) hSetting.get("tableindices"));
		//tableIndices.getTable().setAutoResizeMode(
		//		JTable.AUTO_RESIZE_ALL_COLUMNS);
		

		tableIndices.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_SEQNO, Model.CIDX_TYPE,
				 GlobalIndices.CIDX_URUTAN });
		
		tableSpotCurrency = createTable(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_CURRENCY), filterSpotCurrency,
				(Hashtable) hSetting.get("tablespotcurrency"));
		
		tableSpotCurrency.getTable().setAutoResizeMode(
				JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableSpotCurrency.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_SEQNO, Model.CIDX_TYPE });
		
		tableCrossCurrency = createTable(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_CURRENCY), filterCrossCurrency,
				(Hashtable) hSetting.get("tablecrosscurrency"));
		
		tableCrossCurrency.getTable().setAutoResizeMode(
				JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableCrossCurrency.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_SEQNO, Model.CIDX_TYPE });
		
		tableCommodity = createTable(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_COMMODITY), null,
				(Hashtable) hSetting.get("tablecommodity"));
		
	//	tableCommodity.getTable().setAutoResizeMode(
	//			JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableCommodity.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_SEQNO, Model.CIDX_TYPE });
		
		tableFuture = createTable(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_FUTURE), null,
				(Hashtable) hSetting.get("tablefuture"));
		
	//	tableFuture.getTable().setAutoResizeMode(
	//			JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableFuture.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_SEQNO, Model.CIDX_TYPE });
		
		
		JPanel temp1 = new JPanel(new BorderLayout());
		temp1.add(updateIndices, BorderLayout.NORTH);
		temp1.add(tableIndices, BorderLayout.CENTER);

		JPanel temp2 = new JPanel(new BorderLayout());
	//	temp2.add(updateCurr, BorderLayout.NORTH);
		temp2.add(tableSpotCurrency, BorderLayout.CENTER);
		
		JPanel temp3 = new JPanel(new BorderLayout());
	//	temp3.add(updateCurr, BorderLayout.NORTH);
		temp3.add(tableCrossCurrency, BorderLayout.CENTER);
		
		JPanel temp4 = new JPanel(new BorderLayout());
		temp4.add(updateCommodity, BorderLayout.NORTH);
		temp4.add(tableCommodity, BorderLayout.CENTER);
		
		JPanel temp5 = new JPanel(new BorderLayout());
		temp5.add(updateFuture, BorderLayout.NORTH);
		temp5.add(tableFuture, BorderLayout.CENTER);
		
		tab.addTab("World Indices", temp1);
		tab.addTab("Spot Rate", temp2);
		tab.addTab("Cross Rate",temp3);
		tab.addTab("Commodity",temp4);
		tab.addTab("Futures Index",temp5);
		
		((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_GLOBALINDICES)
				.addTableModelListener(new TableModelListener() {
					@Override
					public void tableChanged(TableModelEvent e) {
						priceChanged(0);
					}
				});

		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CURRENCY)
				.addTableModelListener(new TableModelListener() {
					@Override
					public void tableChanged(TableModelEvent e) {
						priceChanged(1);
					}
				});
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_COMMODITY)
		.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				priceChanged(1);
			}
		});
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_FUTURE)
		.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				priceChanged(1);
			}
		});
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(tab, BorderLayout.CENTER);
		refresh();
		priceChanged(0);
		priceChanged(1);
		
		Timer timer = new Timer();

		timer.schedule( new TimerTask() {
		
		    public void run() {
//		    	unsubscribe();
				fungsi_refresh();
		    }
		 }, 0, 60*1000);
	}
	
	public void fungsi_refresh()
	{
	((IEQTradeApp) apps).getFeedEngine().subscribe(
			FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
	((IEQTradeApp) apps).getFeedEngine().subscribe(
			FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
	((IEQTradeApp) apps).getFeedEngine().subscribe(
			FeedParser.PARSER_COMMODITY, FeedParser.PARSER_COMMODITY);
	((IEQTradeApp) apps).getFeedEngine().subscribe(
			FeedParser.PARSER_FUTURE, FeedParser.PARSER_FUTURE);	
	((IEQTradeApp) apps).getFeedEngine()
		.getStore(FeedStore.DATA_GLOBALINDICES)
		.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				priceChanged(0);
			}
		});

	((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CURRENCY)
			.addTableModelListener(new TableModelListener() {
				@Override
				public void tableChanged(TableModelEvent e) {
					priceChanged(1);
				}
			});
	((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_COMMODITY)
	.addTableModelListener(new TableModelListener() {
		@Override
		public void tableChanged(TableModelEvent e) {
			priceChanged(1);
		}
	});
	((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_FUTURE)
	.addTableModelListener(new TableModelListener() {
		@Override
		public void tableChanged(TableModelEvent e) {
			priceChanged(1);
		}
	});
	refresh();
	priceChanged(0);
	priceChanged(1);
	}
	private void priceChanged(int type) {
		String dt = "last update: " + format.format(new Date());
		if (type == 0) {
			updateIndices.setText(dt);
		} else {
			updateCommodity.setText(dt);
			updateFuture.setText(dt);
		}
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		hSetting.put("tableindices", GlobalIndicesDef.getTableDef());
		hSetting.put("tablespotcurrency", CurrencyDef.getTableDef());
		hSetting.put("tablecrosscurrency", CurrencyDef.getTableDef());
		hSetting.put("tablecommodity", CommodityDef.getTableDef());
		hSetting.put("tablefuture", FutureDef.getTableDef());
		((Hashtable) hSetting.get("tableindices")).put("header",
				GlobalIndicesDef.dataHeader.clone());
		((Hashtable) hSetting.get("tablespotcurrency")).put("header",
				CurrencyDef.dataHeader.clone());
		((Hashtable) hSetting.get("tablecrosscurrency")).put("header",
				CurrencyDef.dataHeader.clone());
		((Hashtable) hSetting.get("tablecommodity")).put("header",
				CommodityDef.dataHeader.clone());
		((Hashtable) hSetting.get("tablefuture")).put("header",
				FutureDef.dataHeader.clone());
		//log.info("isi code:"+Currency.CIDX_VALUE);
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 500, 300));

		((Hashtable) hSetting.get("tableindices")).put("sortcolumn",
				new Integer(GlobalIndices.CIDX_URUTAN));
		
		

	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("tableindices", tableIndices.getTableProperties());
		hSetting.put("tablespotcurrency", tableSpotCurrency.getTableProperties());
		hSetting.put("tablecrosscurrency", tableCrossCurrency.getTableProperties());
		hSetting.put("tablecommodity", tableCommodity.getTableProperties());
		hSetting.put("tablefuture", tableFuture.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		
		tableIndices.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableIndices.setNewFont(FeedSetting.getFont());
		tableSpotCurrency.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableSpotCurrency.setNewFont(FeedSetting.getFont());
		tableCrossCurrency.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableCrossCurrency.setNewFont(FeedSetting.getFont());
		tableCommodity.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableCommodity.setNewFont(FeedSetting.getFont());
		tableFuture.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableFuture.setNewFont(FeedSetting.getFont());
		
	}

	@Override
	public void show() {
		if (!form.isVisible()) {
			
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_COMMODITY, FeedParser.PARSER_COMMODITY);
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_FUTURE, FeedParser.PARSER_FUTURE);
		}
		super.show();
	}

	@Override
	public void hide() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_COMMODITY, FeedParser.PARSER_COMMODITY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_FUTURE, FeedParser.PARSER_FUTURE);
		super.hide();
	}

	@Override
	public void close() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_COMMODITY, FeedParser.PARSER_COMMODITY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_FUTURE, FeedParser.PARSER_FUTURE);
		super.close();
	}
	public void unsubscribe(){
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_COMMODITY, FeedParser.PARSER_COMMODITY);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_FUTURE, FeedParser.PARSER_FUTURE);
	}
}
*/
