package eqtrade.feed.ui.reksadana;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.TradeSummaryDef;

public class UILinkAccount extends UI{

	private  JWebBrowser webBrowser;// = new JWebBrowser();
	private String locationUrl = "";
	
	public UILinkAccount( String app) {
		super("Link Account", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_LINKACCOUNT);
	}

	@Override
	protected void build() {		
		JPanel pnlInfo = new JPanel();
	    FormLayout l = new FormLayout("100px,2dlu,100px, 2dlu, pref:grow, pref",
			"pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlInfo, BorderLayout.NORTH);
		jalan();
		
	}

	private void jalan() {
		//	UIUtils.setPreferredLookAndFeel();
//		    NativeInterface.open();
		    webBrowser = new JWebBrowser();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
				    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
				    webBrowser.setBarsVisible(false);
				    webBrowser.setStatusBarVisible(false);
				    //locationUrl="simas-rol.dev/loginv2.asp?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    locationUrl="rf" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    webBrowser.navigate(locationUrl);
				    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
				    pnlContent.add(webBrowser, BorderLayout.CENTER);
					//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		      }
		    });
		}
	
	@Override
	public void focus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadSetting() {
		// TODO Auto-generated method stub
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 950, 750));
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
			
	}
	
	public void show(){
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				 NativeInterface.open();
				 locationUrl="rol.sinarmas-am.co.id/loginv2.asp?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				 webBrowser.navigate(locationUrl);	
			}
		});
	   	   
		super.show();		
	}

}
