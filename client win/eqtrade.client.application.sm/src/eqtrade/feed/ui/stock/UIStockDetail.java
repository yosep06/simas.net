package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Timer;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import net.sf.nachocalendar.CalendarFactory;
import net.sf.nachocalendar.components.DateField;

import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.plot.XYPlot;

import vollux.chart.core.ChartController;
import vollux.chart.core.CustomDateAxis;
import vollux.chart.ext.ICallback;
import vollux.chart.ext.IChartDatasource;
import vollux.chart.ext.IResult;
import vollux.chart.indicator.BBANDSIndicator;
import vollux.chart.indicator.CandleIndicator;
import vollux.chart.indicator.LineIndicator;
import vollux.chart.indicator.MACDIndicator;
import vollux.chart.indicator.MAIndicator;
import vollux.chart.indicator.STOCHIndicator;
import vollux.chart.model.Chart;
import vollux.chart.util.ChartUtil;
import vollux.chart.util.FormattedTextField;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.CorpAction;
import eqtrade.feed.model.CorpActionDef;
import eqtrade.feed.model.Indices;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.PriceHistoryDef;
import eqtrade.feed.model.RupsDef;
import eqtrade.feed.model.Statistic;
import eqtrade.feed.model.StatisticDef;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockNews;
import eqtrade.feed.model.StockNewsDef;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.PriceHistory;
import eqtrade.feed.model.TradeSummaryDef;
import eqtrade.feed.ui.news.FilterRightAction;
import eqtrade.feed.ui.news.FilterStockAction;
import eqtrade.feed.ui.news.FilterStockNews;
import eqtrade.feed.ui.news.FilterStockRup;
import eqtrade.feed.ui.news.FilterStockSplitAction;
import eqtrade.feed.ui.news.FilterWarrantAction;
import feed.admin.Utils;

public class UIStockDetail extends UI implements TableModelListener, ICallback,IChartDatasource {	
			
	private int i = 0;
	private int counterclick = 0;
	private int indexcmbperiodph = 0;
	
	String tempfrom = "";
	String tempto = "";
	
	private String FirstStock = "";
	private String oldCode = "";
	private String oldStock = "";
	private String TempStock = "";
	
	private static String locationUrl = "";
	
	private JButton btnDraw;	
	private JButton btnMACDhide;
	private JButton btnMACDdraw;
	private JButton btnStochHide;
	private JButton btnStochDraw;
	private JButton btnMAhide;
	private JButton btnMAdraw;
	private JButton btnBBhide;
	private JButton btnBBdraw;
	private JButton btnViewPH; // PH == Price History
	
	private JComboBox cmbPeriod;
	private JComboBox cmbType;
	private JComboBox maType;
	private JComboBox cmbPeriodPH; // PH == Price History
	
	private JGrid tableNews;	
	private JGrid tableDividen;
	private JGrid tableOther;
	private JGrid tableRups;
	private JGrid tabelPriceHistory;
	private JGrid tabelStatistic;
	
	private JLabel fieldName;	
	private JLabel labelNews;
	private JLabel labelCorp;	
	private JLabel fieldLast;
	private JLabel fieldChange;
	private JLabel fieldOpen;
	private JLabel fieldHigh;
	private JLabel fieldLow;
	private JLabel fieldPrev;	
	private JLabel labelPeriodPH; // PH == Price History
	private JLabel labelFromPH; // PH == Price History
	private JLabel labelToPH; // PH == Price History
	private JLabel labelStatistic;
	private JLabel labelPriceHistory;	
	
	private JNumber macdFast;
	private JNumber macdSlow;
	private JNumber macdSignal;	
	private JNumber bbPeriod;
	private JNumber bbUpper;
	private JNumber bbLower;
	private JNumber stochFastK;
	private JNumber stochSlowK;
	private JNumber stochSlowD;
	private JNumber ma1;
	private JNumber ma2;
	private JNumber ma3;
	
	private JPanel panelNewsAndPriceHistory;
	private JPanel panelPriceHistoryAndStatistic;
	private	JPanel panelStockNewsAndCorp;
	private	JPanel panelTechnical;
	private	JPanel panelStatistic;
	private	JPanel panelPriceHistory;	
	private JPanel panelSearch;	
	private JPanel panelInfoPriceHistory;	
	
	private JScrollPane scrollNewsAndPriceHistory;
	private JScrollPane scrollStatistic;
	
	private	JTabbedPane tabStockDetail;
	private JTabbedPane tabCorpAction;
	
	private JTextField textfieldStock;
	
	private  JWebBrowser webBrowser;
	
	private ChartController controller;
	private CustomDateAxis axis;	
	private Calendar cal = Calendar.getInstance();
	
	private DateField fieldFrom;
	private DateField fieldTo;	
	private DateField dateFieldFromPH; // PH == Price History
	private DateField dateFieldToPH; // PH == Price Historys
			
	private FilterStockNews filterStockNews;
	private FilterStockAction filterDividen;
	private FilterStockAction filterOrther;
	private FilterStockRup filterRups;
	
	private FormattedTextField fieldCode;
	
	private GridModel PriceHistoryModel;
	private GridModel StatisticModel;		
	
	private Thread threadLoad = null;
	private Thread threadLoadPH = null;
	
	private BBANDSIndicator bband;
	private CandleIndicator candle;	
	private LineIndicator line;
	private MACDIndicator macd;
	private MAIndicator ma1ind;
	private MAIndicator ma2ind;
	private MAIndicator ma3ind;	
	private STOCHIndicator stoch;
	
	private int counter = 0;
	private Timer refreshTimer = null;
	
	private JGrid tableRight;
	private JGrid tableStockSplit;
	private JGrid tableWarrant;
	
	private FilterRightAction filterRight;
	private FilterStockSplitAction filterStockSplit;
	private FilterWarrantAction filterWarrant;
	
	private static NumberFormat format = new DecimalFormat("#,##0 ");
	private static NumberFormat format3 = new DecimalFormat("#,##0.00 ");
	private static final SimpleDateFormat year = new SimpleDateFormat("yyyy");
	public static final SimpleDateFormat C_DATEFORMAT = new SimpleDateFormat("yyyy-MM-dd");
		
	/*private Dimension originalTabsDim;
	private static int maxW = 0;
    private static int maxH = 0;*/
	
	public UIStockDetail(String app) {
		super("Stock Detail", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_STOCKDETAIL);
		this.title = "Stock Detail";
	}			
	
	public void getData(final String type, final String stock,final String from, final String to, boolean adjusted,	final IResult process, final ICallback callback) {
		if (threadLoad != null) {
			threadLoad.stop();
			threadLoad = null;
		}
		setNav(false);	
		threadLoad = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					String result = ((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().getHistory("CHD" + "|" + stock.toUpperCase().trim()+ "|" + from + "|" + to);
					Vector row = new Vector(100, 5);
					if (result != null) {
						String as[] = result.split("\n", -2);
						int aslength = as.length - 1;
						for (int j = 0; j < aslength; j++) {
							String s1 = as[j];
							if (!s1.trim().equals("")) {
								String detail[] = s1.split("\\|");
								Chart c = new Chart();
								c.setStock(stock);
								c.setDate(detail[0]);
								c.setTime("");
								c.setOpen(detail[2]);
								c.setHigh(detail[3]);
								c.setLow(detail[4]);
								c.setClose(detail[5]);
								c.setVolume(detail[6]);
								row.add(0, c);
							}
						}
						process.receiveData(row);
						callback.success();
					} else {
						process.receiveData(row);
						callback.failed("no data available");
					}
				} catch (Exception ex) {
					//Utils.showMessage("failed, please try again", null);
					oldCode = "";
					callback.failed("failed, please try again");
					//ex.printStackTrace();
					return;
				}
			}
		});
		threadLoad.start();
	}
	
	private void initComponents() {
		fieldCode = new FormattedTextField();
		cmbPeriod = new JComboBox(new Object[] { "1 Month", "3 Month","6 Month", "Year to Date", "1 Year", "2 Year", "5 Year","Custom" });
		cmbType = new JComboBox(new Object[] { "candlestick", "line" });
		fieldFrom = CalendarFactory.createDateField();
		fieldFrom.setDateFormat(C_DATEFORMAT);
		fieldTo = CalendarFactory.createDateField();
		fieldTo.setDateFormat(C_DATEFORMAT);
		btnDraw = new JButton("Draw");

		fieldLast = new JLabel(" ", JLabel.RIGHT);
		fieldChange = new JLabel(" ", JLabel.RIGHT);
		fieldOpen = new JLabel(" ", JLabel.RIGHT);
		fieldHigh = new JLabel(" ", JLabel.RIGHT);
		fieldLow = new JLabel(" ", JLabel.RIGHT);
		fieldPrev = new JLabel(" ", JLabel.RIGHT);

		macdFast = new JNumber(Double.class, 0, 0, true);
		macdSlow = new JNumber(Double.class, 0, 0, true);
		macdSignal = new JNumber(Double.class, 0, 0, true);
		btnMACDhide = new JButton("hide");
		btnMACDdraw = new JButton("draw");
		macdFast.setValue(new Double(macd.getFastPeriod()));
		macdSlow.setValue(new Double(macd.getSlowPeriod()));
		macdSignal.setValue(new Double(macd.getSignalPeriod()));
		btnMACDhide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				macd.hide();
			}
		});
		btnMACDdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				macd.hide();
				macd.setFastPeriod(((Double) macdFast.getValue()).intValue());
				macd.setSlowPeriod(((Double) macdSlow.getValue()).intValue());
				macd.setSignalPeriod(((Double) macdSignal.getValue()).intValue());
				macd.show();
			}
		});

		stochFastK = new JNumber(Double.class, 0, 0, true);
		stochSlowK = new JNumber(Double.class, 0, 0, true);
		stochSlowD = new JNumber(Double.class, 0, 0, true);
		btnStochHide = new JButton("hide");
		btnStochDraw = new JButton("draw");
		stochFastK.setValue(new Double(stoch.getFastKPeriod()));
		stochSlowK.setValue(new Double(stoch.getSlowKPeriod()));
		stochSlowD.setValue(new Double(stoch.getSlowDPeriod()));
		btnStochHide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stoch.hide();
			}
		});
		btnStochDraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stoch.hide();
				stoch.setFastKPeriod(((Double) stochFastK.getValue())
						.intValue());
				stoch.setSlowKPeriod(((Double) stochSlowK.getValue())
						.intValue());
				stoch.setSlowDPeriod(((Double) stochSlowD.getValue())
						.intValue());
				stoch.show();
			}
		});

		ma1 = new JNumber(Double.class, 0, 0, true);
		ma2 = new JNumber(Double.class, 0, 0, true);
		ma3 = new JNumber(Double.class, 0, 0, true);
		maType = ChartUtil.getComboMA();
		btnMAhide = new JButton("hide");
		btnMAdraw = new JButton("draw");
		ma1.setValue(new Double(ma1ind.getPeriod()));
		ma2.setValue(new Double(ma2ind.getPeriod()));
		ma3.setValue(new Double(ma3ind.getPeriod()));
		ma1ind.setColor(Color.blue);
		ma2ind.setColor(Color.cyan);
		ma3ind.setColor(Color.magenta);
		btnMAhide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ma1ind.hide();
				ma2ind.hide();
				ma3ind.hide();
			}
		});
		btnMAdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ma1ind.hide();
				ma2ind.hide();
				ma3ind.hide();
				ma1ind.setPeriod(((Double) ma1.getValue()).intValue());
				ma2ind.setPeriod(((Double) ma2.getValue()).intValue());
				ma3ind.setPeriod(((Double) ma3.getValue()).intValue());
				ma1ind.setType(maType.getSelectedIndex());
				ma2ind.setType(maType.getSelectedIndex());
				ma3ind.setType(maType.getSelectedIndex());
				ma1ind.show();
				ma2ind.show();
				ma3ind.show();
			}
		});

		bbPeriod = new JNumber(Double.class, 0, 0, true);
		bbUpper = new JNumber(Double.class, 1, 1, true);
		bbLower = new JNumber(Double.class, 1, 1, true);
		btnBBhide = new JButton("hide");
		btnBBdraw = new JButton("draw");
		bbPeriod.setValue(new Double(bband.getPeriod()));
		bbUpper.setValue(new Double(bband.getUpperDeviation()));
		bbLower.setValue(new Double(bband.getLowerDeviation()));
		btnBBhide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bband.hide();
			}
		});
		btnBBdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bband.hide();
				bband.setPeriod(((Double) bbPeriod.getValue()).intValue());
				bband.setUpperDeviation(((Double) bbUpper.getValue()).doubleValue());
				bband.setLowerDeviation(((Double) bbLower.getValue()).doubleValue());
				bband.show();
			}
		});
		cmbType.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				line.hide();
				candle.hide();
				if (cmbType.getSelectedIndex() == 0)
					candle.show();
				else
					line.show();
			}
		});
		cmbPeriod.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {		    	
		    	fieldTo.setValue(new Date());
				cal.setTime(new Date());
				fieldFrom.setEnabled(false);
				fieldTo.setEnabled(false);				
				
				if (cmbPeriod.getSelectedItem().equals("1 Month")) {
					cal.add(Calendar.MONTH, -1);	
				} else if (cmbPeriod.getSelectedItem().equals("3 Month"))	{					
					cal.add(Calendar.MONTH, -3);		
				} else if (cmbPeriod.getSelectedItem().equals("6 Month"))	{					
					cal.add(Calendar.MONTH, -6);	
				} else if (cmbPeriod.getSelectedItem().equals("Year to Date")) {
					try	{
						cal.setTime(ChartUtil.C_FORMAT2.parse(year.format(new Date()) + "/01/01"));
					} catch (Exception ex) {
					}
				} else if (cmbPeriod.getSelectedItem().equals("1 Year")) {					
					cal.add(Calendar.YEAR, -1);				
				} else if (cmbPeriod.getSelectedItem().equals("2 Year")) {
					cal.add(Calendar.YEAR, -2);	
				} else if (cmbPeriod.getSelectedItem().equals("5 Year")) {				
					cal.add(Calendar.YEAR, -5);	
				} else if (cmbPeriod.getSelectedItem().equals("Custom")) {					
					fieldFrom.setEnabled(true);
					fieldTo.setEnabled(true);			
				}
				fieldFrom.setValue(cal.getTime());
		    }
		});
		cmbPeriod.setSelectedItem("6 Month");
		fieldCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				fieldCode.setText(fieldCode.getText().toUpperCase());
			}
		});
		fieldCode.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {fieldCode.setText(textfieldStock.getText());
				fieldCode.selectAll();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),JComponent.WHEN_FOCUSED);		
	
	}
	
	private JComponent buildPanel() {
		initComponents();
		CellConstraints c = new CellConstraints();
		FormLayout paramLayout = new FormLayout("pref","pref,pref,5dlu,pref,pref,5dlu,pref,pref,2dlu, pref, 2dlu, pref,2dlu,pref");
		PanelBuilder param = new PanelBuilder(paramLayout);
		param.setDefaultDialogBorder();

		FormLayout entryLayout = new FormLayout("pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px","pref, 2dlu, pref, 2dlu, pref"); // rows
		PanelBuilder entry = new PanelBuilder(entryLayout);		
		
		entry.add(fieldCode, c.xyw(3, 1, 3));
		entry.add(btnDraw, c.xy(7, 1));		
		entry.add(cmbType, c.xy(3, 3));		
		entry.add(cmbPeriod, c.xy(7, 3));		
		entry.add(fieldFrom, c.xy(3, 5));		
		entry.add(fieldTo, c.xy(7, 5));

		FormLayout todayLayout = new FormLayout("pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px","pref, 2dlu, pref, 2dlu, pref");
		PanelBuilder today = new PanelBuilder(todayLayout);
		today.add(new JLabel("last"), c.xy(1, 1));
		today.add(fieldLast, c.xy(3, 1));
		today.add(new JLabel("hi"), c.xy(5, 1));
		today.add(fieldHigh, c.xy(7, 1));
		today.add(new JLabel("chg"), c.xy(1, 3));
		today.add(fieldChange, c.xy(3, 3));
		today.add(new JLabel("low"), c.xy(5, 3));
		today.add(fieldLow, c.xy(7, 3));
		today.add(new JLabel("open"), c.xy(1, 5));
		today.add(fieldOpen, c.xy(3, 5));
		today.add(new JLabel("prev"), c.xy(5, 5));
		today.add(fieldPrev, c.xy(7, 5));
		today.getPanel().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		today.setDefaultDialogBorder();

		FormLayout macdLayout = new FormLayout("40px:grow, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px","pref, 2dlu, pref");
		PanelBuilder macd = new PanelBuilder(macdLayout);
		macd.addSeparator("MACD", c.xyw(1, 1, 3));
		macd.add(btnMACDhide, c.xyw(5, 1, 3));
		macd.add(btnMACDdraw, c.xyw(9, 1, 3));
		macd.add(new JLabel("fast"), c.xy(1, 3));
		macd.add(macdFast, c.xy(3, 3));
		macd.add(new JLabel("slow"), c.xy(5, 3));
		macd.add(macdSlow, c.xy(7, 3));
		macd.add(new JLabel("signal"), c.xy(9, 3));
		macd.add(macdSignal, c.xy(11, 3));
		macd.getPanel().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		macd.setDefaultDialogBorder();

		FormLayout stochLayout = new FormLayout("40px:grow, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px","pref, 2dlu, pref");
		PanelBuilder stoch = new PanelBuilder(stochLayout);
		stoch.addSeparator("Stochastic", c.xyw(1, 1, 3));
		stoch.add(btnStochHide, c.xyw(5, 1, 3));
		stoch.add(btnStochDraw, c.xyw(9, 1, 3));
		stoch.add(new JLabel("fast%k"), c.xy(1, 3));
		stoch.add(stochFastK, c.xy(3, 3));
		stoch.add(new JLabel("slow%k"), c.xy(5, 3));
		stoch.add(stochSlowK, c.xy(7, 3));
		stoch.add(new JLabel("slow%d"), c.xy(9, 3));
		stoch.add(stochSlowD, c.xy(11, 3));
		stoch.getPanel().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN));
		stoch.setDefaultDialogBorder();

		FormLayout maLayout = new FormLayout("40px:grow, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px","pref, 2dlu, pref, 2dlu, pref");
		PanelBuilder ma = new PanelBuilder(maLayout);
		ma.addSeparator("Moving Avg", c.xyw(1, 1, 3));
		ma.add(btnMAhide, c.xyw(5, 1, 3));
		ma.add(btnMAdraw, c.xyw(9, 1, 3));
		ma.add(new JLabel("ma1"), c.xy(1, 3));
		ma.add(ma1, c.xy(3, 3));
		ma.add(new JLabel("ma2"), c.xy(5, 3));
		ma.add(ma2, c.xy(7, 3));
		ma.add(new JLabel("ma3"), c.xy(9, 3));
		ma.add(ma3, c.xy(11, 3));
		ma.add(new JLabel("type"), c.xy(1, 5));
		ma.add(maType, c.xyw(3, 5, 9));
		ma.getPanel().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		ma.setDefaultDialogBorder();

		FormLayout bbLayout = new FormLayout("40px:grow, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px, 2dlu, 40px, 2dlu, 25px","pref, 2dlu, pref");
		PanelBuilder bb = new PanelBuilder(bbLayout);
		bb.addSeparator("BBands", c.xyw(1, 1, 3));
		bb.add(btnBBhide, c.xyw(5, 1, 3));
		bb.add(btnBBdraw, c.xyw(9, 1, 3));
		bb.add(new JLabel("period"), c.xy(1, 3));
		bb.add(bbPeriod, c.xy(3, 3));
		bb.add(new JLabel("upper"), c.xy(5, 3));
		bb.add(bbUpper, c.xy(7, 3));
		bb.add(new JLabel("lower"), c.xy(9, 3));
		bb.add(bbLower, c.xy(11, 3));
		bb.getPanel().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN));
		bb.setDefaultDialogBorder();

		param.add(entry.getPanel(), c.xy(1, 2));
		param.addSeparator("Today", c.xy(1, 4));
		param.add(today.getPanel(), c.xy(1, 5));
		param.addSeparator("Technical Indicator", c.xy(1, 7));
		param.add(macd.getPanel(), c.xy(1, 8));
		param.add(stoch.getPanel(), c.xy(1, 10));
		param.add(ma.getPanel(), c.xy(1, 12));
		param.add(bb.getPanel(), c.xy(1, 14));

		JPanel content = new JPanel(new BorderLayout());
		content.add(param.getPanel(), BorderLayout.EAST);
		content.add(controller.getContainer().getPanel(), BorderLayout.CENTER);
		return content;
	}
	
	public void tableChanged(TableModelEvent arg0) {
		StockSummary ss = (StockSummary) ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new Object[] { oldCode, "RG" });
		Indices index = (Indices) ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_INDICES).getDataByKey(new Object[] { oldCode });
		if (ss != null) {
			fieldLast.setText(format.format(ss.getClosing()));
			fieldChange.setText(format.format(ss.getChange()) + "("+ format.format(ss.getPercent()) + "%) ");
			fieldOpen.setText(format.format(ss.getOpening()));
			fieldHigh.setText(format.format(ss.getHighest()));
			fieldLow.setText(format.format(ss.getLowest()));
			fieldPrev.setText(format.format(ss.getPrevious()));
			fieldLast.setForeground(ss.getChange().doubleValue() > 0 ? FeedSetting.getColor(FeedSetting.C_PLUS) : (ss.getChange().doubleValue() < 0) ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
			fieldChange.setForeground(ss.getChange().doubleValue() > 0 ? FeedSetting.getColor(FeedSetting.C_PLUS) : (ss.getChange().doubleValue() < 0) ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
			fieldOpen.setForeground(ss.getOpening().doubleValue() > ss.getPrevious().doubleValue() ? FeedSetting.getColor(FeedSetting.C_PLUS):(ss.getOpening().doubleValue() < ss.getPrevious().doubleValue()) ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
			fieldHigh.setForeground(ss.getHighest().doubleValue() > ss.getClosing().doubleValue() ? FeedSetting.getColor(FeedSetting.C_PLUS) : (ss.getHighest().doubleValue() < ss.getClosing().doubleValue()) ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
			fieldLow.setForeground(ss.getLowest().doubleValue() > ss.getClosing().doubleValue() ? FeedSetting.getColor(FeedSetting.C_PLUS) : (ss.getLowest().doubleValue() < ss.getClosing().doubleValue()) ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
			fieldPrev.setForeground(FeedSetting.getColor(FeedSetting.C_ZERO));
		} else if (index != null) {
			fieldLast.setText(format3.format(index.getLast()));
			fieldChange.setText(format3.format(index.getChange()) + "("	+ format3.format(index.getPercent()) + "%) ");
			fieldOpen.setText(format3.format(index.getOpen()));
			fieldHigh.setText(format3.format(index.getHigh()));
			fieldLow.setText(format3.format(index.getLow()));
			fieldPrev.setText(format3.format(index.getPrev()));
			fieldLast.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
							.getColor(FeedSetting.C_PLUS) : (index.getChange()
							.doubleValue() < 0) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldChange
					.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
							.getColor(FeedSetting.C_PLUS) : (index.getChange()
							.doubleValue() < 0) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldOpen.setForeground(index.getOpen().doubleValue() > index
					.getPrev().doubleValue() ? FeedSetting
					.getColor(FeedSetting.C_PLUS)
					: (index.getOpen().doubleValue() < index.getPrev()
							.doubleValue()) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldHigh.setForeground(index.getHigh().doubleValue() > index
					.getLast().doubleValue() ? FeedSetting
					.getColor(FeedSetting.C_PLUS)
					: (index.getHigh().doubleValue() < index.getLast()
							.doubleValue()) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldLow.setForeground(index.getLow().doubleValue() > index
					.getLast().doubleValue() ? FeedSetting
					.getColor(FeedSetting.C_PLUS)
					: (index.getLow().doubleValue() < index.getLast()
							.doubleValue()) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
			fieldPrev.setForeground(FeedSetting.getColor(FeedSetting.C_ZERO));
		} else {
			fieldLast.setText("- ");
			fieldChange.setText("- ");
			fieldOpen.setText("- ");
			fieldHigh.setText("- ");
			fieldLow.setText("- ");
			fieldPrev.setText("- ");
		}
	}
	
	private void previewChart() {
		if ("".equals(fieldCode.getText().trim())) {
			fieldCode.requestFocus();			
		} else {
			axis.setRangeDate((Date) fieldFrom.getValue(),(Date) fieldTo.getValue());
			controller.getContainer().getPanel().restoreAutoBounds();
			oldCode = fieldCode.getText().toUpperCase();
			tableChanged(null);			
			controller.getDataset().loadData(cmbPeriod.getSelectedItem().toString(),fieldCode.getText().trim().toUpperCase(),C_DATEFORMAT.format(fieldFrom.getValue()),C_DATEFORMAT.format(fieldTo.getValue()), false, this);
		}
	}
		
	private String getStockName(String code) {
		String name = "";		
		 int i = ((IEQTradeApp) apps).getTradingEngine().getStore(FeedStore.DATA_STOCK).getIndexByKey(new Object[] { code });		
		  if (i > -1) {
			  name = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_STOCK).getDataByIndex(i))
					.getName();
			  }
		return name;
	}
	
	private String setStockName(String stock) {
		String stockName = "";
		int i = ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCK).getIndexByKey(new Object[] { stock });
		if (i > -1) {
			stockName = ((Stock) ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCK).getDataByIndex(i)).getName();
		} else {
			i = ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_INDICES).getIndexByKey(new Object[] { stock });
			if (i > 1) {
				stockName = ((Indices) ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_INDICES).getDataByIndex(i)).getCode();
			}
		}
		return stockName;
	}
		
	private void bindStockNews() {
		((FilterColumn) filterStockNews.getFilteredData("newsstock")).setField(textfieldStock.getText());
		filterStockNews.fireFilterChanged();
		((FilterColumn) filterDividen.getFilteredData("stock")).setField(textfieldStock.getText());
		filterDividen.fireFilterChanged();
		((FilterColumn) filterRight.getFilteredData("stock")).setField(textfieldStock.getText());
		filterRight.fireFilterChanged();
		((FilterColumn) filterStockSplit.getFilteredData("stock")).setField(textfieldStock.getText());
		filterStockSplit.fireFilterChanged();
		((FilterColumn) filterWarrant.getFilteredData("stock")).setField(textfieldStock.getText());
		filterWarrant.fireFilterChanged();
		((FilterColumn) filterRups.getFilteredData("stock")).setField(textfieldStock.getText());
		filterRups.fireFilterChanged();
	}
	
	private void bindTechnical() {
		fieldCode.setText(textfieldStock.getText());
		fieldCode.selectAll();			
		previewChart();
	}
	
	private void bindPriceHistory() {		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {		
				try {		
					String period = cmbPeriodPH.getSelectedItem().toString();
					String stock = textfieldStock.getText().toUpperCase().toString().trim();
					String from = C_DATEFORMAT.format((Date) dateFieldFromPH.getValue()).trim();
					String to = C_DATEFORMAT.format((Date) dateFieldToPH.getValue()).trim();
					
				
					
					DateFormat forrmatter;
					Date date_from;
					Date date_to;
					forrmatter = new SimpleDateFormat("yyyy-mm-dd");
					date_from = (Date)forrmatter.parse(from);
					date_to = (Date)forrmatter.parse(to);
				
					if(indexcmbperiodph != 3) {
						dateFieldFromPH.setEnabled(false);
						dateFieldToPH.setEnabled(false);
						btnViewPH.setEnabled(false);
						Date todayDate = new Date();
						dateFieldFromPH.setValue(todayDate); 
						dateFieldToPH.setValue(todayDate);
					} else {
						dateFieldFromPH.setEnabled(true);
						dateFieldToPH.setEnabled(true);
						btnViewPH.setEnabled(true);
						if (date_from.compareTo(date_to)>0) {
							Utils.showMessage("Aborted, please enter valid date parameter",	null);				
							Date todayDate = new Date();
							dateFieldFromPH.setValue(todayDate); 
							dateFieldToPH.setValue(todayDate);
							from = C_DATEFORMAT.format((Date) dateFieldFromPH.getValue()).trim();
							to = C_DATEFORMAT.format((Date) dateFieldToPH.getValue()).trim();
							PriceHistoryModel.getDataVector().clear();
							PriceHistoryModel.refresh();
						}
					}
					
					//textfieldStock.setEnabled(false);			
					String result = ((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().getHistory("PH" + "|" + stock + "|" + period + "|" + from + "|"  + to);
					
					PriceHistoryModel.getDataVector().clear();
					PriceHistoryModel.refresh();			
					
					if (result != "GAGAL") {
						String as[] = result.split("\n", -2);
						int aslength = as.length - 1;
						Vector vRow = new Vector(100, 5);
						for (int j = 0; j < aslength; j++) {
							String s1 = as[j];
							if (!s1.trim().equals("")) {
								PriceHistory phistory = new PriceHistory(s1);							
								vRow.addElement(PriceHistoryDef	.createTableRow(phistory));
							}
						}
						PriceHistoryModel.addRow(vRow, false, false);
						PriceHistoryModel.refresh();	
						cmbPeriodPH.setEnabled(true);
					} else {		
						PriceHistoryModel.refresh();
					}
				} catch (Exception ex) {	
					ex.printStackTrace();
					//Utils.showMessage("failed, please try again", form);
					return;
				}	
			}
		});
	}
		
	private void bindStatistic() {
		SwingUtilities.invokeLater(new Runnable() {		
			@Override
			public void run() {
				try {			
					String resultstatistic = ((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection()
					.getHistory("SSD" + "|" + textfieldStock.getText().toUpperCase().trim()); // SSD  = STATISTIC STOCK DETAIL
				
					StatisticModel.getDataVector().clear();
					StatisticModel.refresh();			
					
					if (resultstatistic != "GAGAL") {	
						String as[] = resultstatistic.split("\n", -2);
						int aslength = as.length - 1;
						Vector vRow = new Vector(100, 5);
						for (int j = 0; j < aslength; j++) {
							String s1 = as[j];					
							if (!s1.trim().equals("")) {
								Statistic statistic= new Statistic(s1);							
								vRow.addElement(StatisticDef.createTableRow(statistic));
							}					
						}				
						StatisticModel.addRow(vRow, false, false);
						StatisticModel.refresh();					
					} else {	
						StatisticModel.refresh();	
					}
					StatisticModel.refresh();	
				} catch (Exception ex) {	
					ex.printStackTrace();
					//Utils.showMessage("failed, please try again", form);
					return;
				}				
			}
		});
	}
	
	private void bindCompanyProfile () {
		 NativeInterface.open();
		    SwingUtilities.invokeLater(new Runnable() {
		    	public void run() {	
			    	  String fieldname;
			    	  String oldstock = "";
			    	  String newstock  = textfieldStock.getText().trim().toUpperCase();		          
			          
			    	  fieldname = getStockName(newstock);
			          
			          if (!oldstock.equals(newstock)) {
			        	  locationUrl = "file:///" + new File("data").getAbsolutePath()+ File.separator + "loading" + ".htm";
			        	  webBrowser.navigate(locationUrl);
			        	  if ((newstock.length() > 0)) {
			        		  textfieldStock.setText(newstock);
			        		  oldstock = newstock;
			        		  fieldname = getStockName(newstock);
			        		  if (fieldname.equals("")) {
			        			  File ftest = new File("data/temp/" + oldstock + ".zip");				        			  
			        			  if (ftest.exists()) {
			        				  locationUrl = "file:///" + new File("data/temp/" + oldstock.toLowerCase() + "/").getAbsolutePath() 
			        				  	+ File.separator + oldstock.toLowerCase() + ".htm";
			        				  } else {
			        					  try {
			        						//System.out.println("file ");
			  		  						byte[] result = ((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().getFile("FILE" + "|" + oldstock);								
			  		  						//System.out.println("file 2");
			  		  						FileOutputStream fos = new FileOutputStream("data/temp/" + oldstock + ".htm");
			  		  						//System.out.println("file 3");
			  		  						fos.write(result);
			  		  						//System.out.println("file 4");
			  		  						fos.close();
			  		  						//System.out.println("file 5");
			  		  						locationUrl = "file:///" + new File("data/temp/" + oldstock.toLowerCase() + ".htm").getAbsolutePath();	  						
			  		  						//System.out.println("file 6");
			  		  						webBrowser.navigate(locationUrl);  					
			  		  					} catch (Exception ex) {
			  		  						oldstock = "";
			  		  						fieldname = "failed, please try again";
			  		  						locationUrl = "file:///"+ new File("data").getAbsolutePath()+ File.separator + "blank" + ".htm";
			  		  						webBrowser.navigate(locationUrl);
			  		  						ex.printStackTrace();
			  		  					}
			        				  }
			        		  } else {
			        			  locationUrl = "file:///"+ new File("data").getAbsolutePath()+ File.separator + "blank" + ".htm";
			        			  webBrowser.navigate(locationUrl);
			        		  }
			        	  }		        	  
			          }
			      }
		    });
	}
		
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				textfieldStock.requestFocus();
			}
		});
	}
	
	@Override
	protected void build() {
		textfieldStock = new JTextField("");	
		fieldName = new JLabel("please type a stock code");
		
		tabStockDetail = new JTabbedPane();		
		
		panelSearch = new JSkinPnl();	
		panelSearch.setBorder(new EmptyBorder(0,0,0,0));
		FormLayout layoutSearch = new FormLayout("90px,2dlu,50px, 2dlu, pref:grow, pref","pref");
		PanelBuilder builderSearch = new PanelBuilder(layoutSearch,panelSearch);	
		CellConstraints cellSearch = new CellConstraints();
		builderSearch.add(textfieldStock, cellSearch.xy(1,1));
		builderSearch.add(fieldName, cellSearch.xyw(3, 1, 3));
		
		pnlContent = new JSkinPnl(new BorderLayout());
		
		panelNewsAndPriceHistory = new JSkinPnl();		
		panelTechnical = new JSkinPnl();
		panelPriceHistory = new JSkinPnl();
		panelStatistic = new JSkinPnl();		
		
		News();
		TechnicalChart();	
		CompanyProfile();
		StatisticAndPriceHistory();
		
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));		
		pnlContent.add(panelSearch, BorderLayout.NORTH);
		pnlContent.add(tabStockDetail, BorderLayout.CENTER);		
		
		tabStockDetail.addTab("News", panelStockNewsAndCorp);		
		tabStockDetail.addTab("Technical", panelTechnical);		
		tabStockDetail.addTab("Company Profile", webBrowser);
		tabStockDetail.addTab("Price History & Statistic", panelPriceHistoryAndStatistic);	
		
		textfieldStock.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				SwingUtilities.invokeLater(new Runnable() {					
					@Override
					public void run() {
						if(textfieldStock.getText().equals("")) {
							fieldName.setText("please type a stock code");				
						} else {					
							FirstStock = textfieldStock.getText().toUpperCase().trim();
							if(i == 0) {	
								TempStock = textfieldStock.getText().toUpperCase().trim();
								fieldName.setText(setStockName(textfieldStock.getText().toUpperCase().trim()));						
								
								bindStockNews();
								bindTechnical();
								bindPriceHistory();
								bindStatistic();
								bindCompanyProfile();
								
								i++;
							}
							else {
								if(!TempStock.equals(textfieldStock.getText().toUpperCase().trim())) {
									int j = 0;
									TempStock = textfieldStock.getText().toUpperCase().trim();
									fieldName.setText(setStockName(textfieldStock.getText().toUpperCase().trim()));
									cmbPeriodPH.setSelectedItem("Daily");
									
									bindStockNews();
									bindTechnical();
									bindPriceHistory();
									bindStatistic();
									bindCompanyProfile();
									
									j++;							
								}
								else {
									return;
								}
							}
						}	
					}
				});							
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),JComponent.WHEN_FOCUSED);
		
		refresh();
	}
		
	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
		
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tableNews", StockNewsDef.getTableDef());
			hSetting.put("tableDividen", CorpActionDef.getTableDef());
			hSetting.put("tableRight", CorpActionDef.getTableDef());
			hSetting.put("tableStockSplit", CorpActionDef.getTableDef());
			hSetting.put("tableWarrant", CorpActionDef.getTableDef());
			hSetting.put("tableRups", RupsDef.getTableDef());
			hSetting.put("tabelPriceHistory", PriceHistoryDef.getTableDef());
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
			hSetting.put("tabelStatistic", StatisticDef.getTableDef());
		}
		hSetting.put("tableNews", StockNewsDef.getTableDef());
		hSetting.put("tableDividen", CorpActionDef.getTableDef());
		hSetting.put("tableRight", CorpActionDef.getTableDef());
		hSetting.put("tableStockSplit", CorpActionDef.getTableDef());
		hSetting.put("tableWarrant", CorpActionDef.getTableDef());
		hSetting.put("tableRups", RupsDef.getTableDef());
		hSetting.put("tabelPriceHistory", PriceHistoryDef.getTableDef());
		hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		hSetting.put("tabelStatistic", StatisticDef.getTableDef());
		if (hSetting.get("size") == null) {
			hSetting.put("size", new Rectangle(20, 20, 800, 560));
		}	
	}
	
	@Override	
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put("size",new Rectangle(form.getX(), form.getY(), form.getWidth(), form.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
		
		hSetting.put("tableNews", StockNewsDef.getTableDef());
		hSetting.put("tableDividen", CorpActionDef.getTableDef());
		hSetting.put("tableRight", tableRight.getTableProperties());
		hSetting.put("tableStockSplit", tableStockSplit.getTableProperties());
		hSetting.put("tableWarrant", tableWarrant.getTableProperties());
		hSetting.put("tableRups", RupsDef.getTableDef());
		hSetting.put("tabelPriceHistory", PriceHistoryDef.getTableDef());
		hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		hSetting.put("tabelStatistic", StatisticDef.getTableDef());
	}
	
	@Override
	public void refresh() {			
		tableNews.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableNews.setNewFont(FeedSetting.getFont());	
		
		tabelPriceHistory.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tabelPriceHistory.setNewFont(FeedSetting.getFont());	
		
		tabelStatistic.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tabelStatistic.setNewFont(FeedSetting.getFont());
		
		tableRight.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableRight.setNewFont(FeedSetting.getFont());
		
		tableStockSplit.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableStockSplit.setNewFont(FeedSetting.getFont());
		
		tableWarrant.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableWarrant.setNewFont(FeedSetting.getFont());
	}
	
	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
			fieldCode.setText(par.get("STOCK").toString());
			textfieldStock.setText(par.get("STOCK").toString());
			System.out.println(par.get("STOCK").toString()+" ff");
			bindStockNews();
			bindTechnical();
			bindPriceHistory();
			bindStatistic();
			bindCompanyProfile();
		}
		show();
	}
	
	@Override
	public void show() {
		if(counter == 0) {	
			counter++;
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_STOCKNEWS, FeedParser.PARSER_STOCKNEWS);
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_CORPACTION, FeedParser.PARSER_CORPACTION);
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_IPO, FeedParser.PARSER_IPO);
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_RUPS, FeedParser.PARSER_RUPS);				
		}	
		
		setNav(true);		
		super.show();
	}
	
	@Override
	public void hide() {		
		super.hide();
	}
	
	protected void initUI() {			
		super.initUI();	
		form.setResizable(false);
		form.pack();
	}
	
	@Override
	public void close() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_STOCKNEWS, FeedParser.PARSER_STOCKNEWS);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_CORPACTIONSD, FeedParser.PARSER_CORPACTIONSD);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_IPO, FeedParser.PARSER_IPO);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_RUPS, FeedParser.PARSER_RUPS);
		
		super.close();
	}
	
	private void setNav(boolean state) {		
		btnDraw.setEnabled(state);
		if (state) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					textfieldStock.requestFocus();
					fieldCode.selectAll();
				}
			});
		}
	}
	
	public void failed(String reason) {
		setInfo(reason);
		setNav(true);		
	}
	
	public void setStatus(String status) {
		setInfo(status);		
	}
	
	public void setWait(boolean b) {				
	}
	
	public void success() {
		setInfo("");
		setNav(true);
	}
	
	private void setInfo(final String state) {
		
	}
	
	private void News() {
		tabCorpAction= new JTabbedPane();		
		
		labelNews = new JLabel("News");
		labelNews.setFont(new Font("Tahoma", Font.BOLD,12));		
		
		labelCorp = new JLabel("Corporate Action");
		labelCorp.setFont(new Font("Tahoma", Font.BOLD, 12));			
		
		panelStockNewsAndCorp = new JPanel();
		panelStockNewsAndCorp.setBorder(new EmptyBorder(0,0,0,0));	
		
		filterStockNews  = new  FilterStockNews(pnlContent);
		filterDividen = new FilterStockAction(pnlContent);
		filterRups = new FilterStockRup(pnlContent);
		
		filterRight = new FilterRightAction(pnlContent);
		filterStockSplit = new FilterStockSplitAction(pnlContent);
		filterWarrant = new FilterWarrantAction(pnlContent);
							
		((FilterColumn) filterDividen.getFilteredData("action")).setField("Deviden Tunai");
		((FilterColumn) filterDividen.getFilteredData("action2")).setField("Deviden Stock");
		((FilterColumn) filterStockSplit.getFilteredData("action2")).setField("Stock Split");
		((FilterColumn) filterRight.getFilteredData("action")).setField("Right Issue");
		((FilterColumn) filterWarrant.getFilteredData("action3")).setField("Warrant");
		
		tableNews = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKNEWS),filterStockNews,(Hashtable) hSetting.get("tableNews"));
		tableNews.setColumnHide(new int[]{StockNews.CIDX_NEWSID,StockNews.CIDX_NEWSSTOCK});
		tableNews.setPreferredSize(new Dimension(0, 525));
		tableNews.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableNews.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					int i = tableNews.getMappedRow(tableNews.getSelectedRow());
					if (i > -1) {
						StockNews order = (StockNews) ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKNEWS).getDataByIndex(i);
						HashMap map = new HashMap();
						map.put("content", order.getContent());
						apps.getUI().showUI(FeedUI.UI_DLGSTOCKNEWS, map);
					}
				}
			}
		});
		
		tableDividen = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterDividen,(Hashtable) hSetting.get("tableDividen"));
		tableDividen.setColumnHide(new int[] {CorpAction.CIDX_TRANSACTIONDATE, CorpAction.CIDX_LASTTRANSACTIONDATE});
		tableDividen.setPreferredSize(new Dimension(0, 468));
		tableDividen.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		/*tableOther = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterOrther,(Hashtable) hSetting.get("tableOther"));
		tableOther.setPreferredSize(new Dimension(0, 468));
		tableOther.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);*/
		
		tableRups = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_RUPS), filterRups,(Hashtable) hSetting.get("tableRups"));
		tableRups.setPreferredSize(new Dimension(0, 468));
		tableRups.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,Model.CIDX_SEQNO });
		tableRups.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);	
		
		tableRight = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterRight, (Hashtable) hSetting.get("tableRight"));
		tableRight.setColumnHide(new int[] { CorpAction.CIDX_EXPDATE });
		tableRight.setSortColumn(CorpAction.CIDX_CUMDATE);
		tableRight.setSortAscending(false);
		tableRight.setPreferredSize(new Dimension(0, 468));
		
		tableStockSplit = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterStockSplit, (Hashtable) hSetting.get("tableStockSplit"));
		tableStockSplit.setColumnHide(new int[] { CorpAction.CIDX_AMOUNT, CorpAction.CIDX_TRANSACTIONDATE, CorpAction.CIDX_LASTTRANSACTIONDATE, CorpAction.CIDX_EXPDATE });
		tableStockSplit.setSortColumn(CorpAction.CIDX_CUMDATE);
		tableStockSplit.setSortAscending(false);
		tableStockSplit.setPreferredSize(new Dimension(0, 468));
	
		tableWarrant = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterWarrant, (Hashtable) hSetting.get("tableWarrant"));
		tableWarrant.setColumnHide(new int[] { CorpAction.CIDX_RATIO1, CorpAction.CIDX_RATIO2,CorpAction.CIDX_CUMDATE, CorpAction.CIDX_RECORDDATE, CorpAction.CIDX_DISTDATE});
		tableWarrant.setSortColumn( CorpAction.CIDX_TRANSACTIONDATE);
		tableWarrant.setSortAscending(false);
		tableWarrant.setPreferredSize(new Dimension(0, 468));

		FormLayout layoutStockNewsAndCorp = new FormLayout("320dlu,1dlu,320dlu","pref,1dlu,pref");
		PanelBuilder builderSotckNews = new PanelBuilder(layoutStockNewsAndCorp, panelStockNewsAndCorp);		
		CellConstraints cellStockNewsAndCorp = new  CellConstraints();		
		builderSotckNews.add(labelNews, cellStockNewsAndCorp.xy(1,1));
		builderSotckNews.add(tableNews, cellStockNewsAndCorp.xy(1,3));	
		builderSotckNews.add(labelCorp, cellStockNewsAndCorp.xy(3,1));	
		builderSotckNews.add(tabCorpAction, cellStockNewsAndCorp.xy(3,3));	

		tabCorpAction.addTab("Deviden",tableDividen);
		tabCorpAction.addTab("RUPS & Public Expose",tableRups);
		tabCorpAction.addTab("Right Issue",tableRight);	
		tabCorpAction.addTab("Stock Split",tableStockSplit);	
		tabCorpAction.addTab("Warrant",tableWarrant);	
		
//		scrollNewsAndPriceHistory = new JScrollPane(panelStockNewsAndCorp);
	}
 	
	private void TechnicalChart() {			
		controller = new ChartController(this);
		axis = new CustomDateAxis("", new Date(), new Date(), controller.getContainer().getTimeline());
		XYPlot xyplot = controller.getContainer().getPlot();
		xyplot.setDomainAxis(axis);
		StandardChartTheme.createDarknessTheme().apply(controller.getContainer().getPanel().getChart());

		macd = new MACDIndicator(controller, "macd", "");
		macd.show();
		
		stoch = new STOCHIndicator(controller, "stochastic", "");
		stoch.show();
		
		candle = new CandleIndicator(controller, "candle stick", "");
		bband = new BBANDSIndicator(controller, "BBands", "");
		line = new LineIndicator(controller, "line", "");
		ma1ind = new MAIndicator(controller, "MA1", "");
		ma2ind = new MAIndicator(controller, "MA2", "");
		ma2ind.setPeriod(20);
		ma3ind = new MAIndicator(controller, "MA3", "");
		ma3ind.setPeriod(50);
		ma1ind.setColor(Color.blue);
		ma2ind.setColor(Color.cyan);
		ma3ind.setColor(Color.magenta);
		ma1ind.show();
		ma2ind.show();
		ma3ind.show();
		candle.show();
		
		JComponent temp = buildPanel();		
	
		fieldCode.hide();
		btnDraw.hide();
		cmbType.hide();
		cmbPeriod.hide();
		fieldFrom.hide();
		fieldTo.hide();		
		
		panelTechnical.add(new JScrollPane(temp), BorderLayout.CENTER);		
		
		textfieldStock.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				textfieldStock.selectAll();
			}
		});
		
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).addTableModelListener(this);
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_INDICES).addTableModelListener(this);
		tableChanged(null);
		
	}
 	
 	private void StatisticAndPriceHistory() {
 		
 		labelPriceHistory = new JLabel("Price History");
		labelPriceHistory.setFont(new Font("Tahoma", Font.BOLD, 12));
 		
 		labelStatistic = new JLabel("Statistic");
 		labelStatistic.setFont(new Font("Tahoma", Font.BOLD,12));
 		
 		panelInfoPriceHistory = new JPanel();
		panelInfoPriceHistory.setBorder(new EmptyBorder(0,0,0,0));		
		
		panelPriceHistory = new JSkinPnl();		
		panelPriceHistory.setBorder(new EmptyBorder(0,0,0,0));
 		
 		panelStatistic = new JSkinPnl();		
 		panelStatistic.setBorder(new EmptyBorder(0,0,0,0)); 		
 		
 		panelPriceHistoryAndStatistic = new JSkinPnl();		
 		panelPriceHistoryAndStatistic.setBorder(new EmptyBorder(2,2,2,2));	
 		
 		labelPeriodPH = new JLabel("Period");
		cmbPeriodPH = new JComboBox(new Object[] {"Daily", "Weekly", "Monthly", "Custom"});
		labelFromPH = new JLabel("From");
		dateFieldFromPH = CalendarFactory.createDateField();
		dateFieldFromPH.setDateFormat(C_DATEFORMAT);
		labelToPH = new JLabel("To");
		dateFieldToPH = CalendarFactory.createDateField();
		dateFieldToPH.setDateFormat(C_DATEFORMAT);
		btnViewPH = new JButton("View");	
		
		cmbPeriodPH.setEnabled(false);
		dateFieldFromPH.setEnabled(false);
		dateFieldToPH.setEnabled(false);
		btnViewPH.setEnabled(false);
 		
 		PriceHistoryModel = new GridModel(PriceHistoryDef.getHeader(), false);			
		tabelPriceHistory = createTable(PriceHistoryModel, null, (Hashtable) hSetting.get("tabelPriceHistory"));
		tabelPriceHistory.setColumnHide(new int[] {PriceHistory.CIDX_PREV});
		tabelPriceHistory.setPreferredSize(new Dimension(0, 500));
		tabelPriceHistory.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);		
		
		FormLayout layoutPriceHistoryInfo = new FormLayout("pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref","pref");
		PanelBuilder builderPriceHistoryInfo = new PanelBuilder(layoutPriceHistoryInfo,panelInfoPriceHistory);
		CellConstraints cellPriceHistoryInfo = new CellConstraints();
		
		builderPriceHistoryInfo.add(labelPeriodPH, cellPriceHistoryInfo.xy(1, 1));
		builderPriceHistoryInfo.add(cmbPeriodPH, cellPriceHistoryInfo.xy(3, 1));
		builderPriceHistoryInfo.add(labelFromPH, cellPriceHistoryInfo.xy(5, 1));
		builderPriceHistoryInfo.add(dateFieldFromPH, cellPriceHistoryInfo.xy(7, 1));
		builderPriceHistoryInfo.add(labelToPH, cellPriceHistoryInfo.xy(9, 1));
		builderPriceHistoryInfo.add(dateFieldToPH, cellPriceHistoryInfo.xy(11, 1));		
		builderPriceHistoryInfo.add(btnViewPH, cellPriceHistoryInfo.xy(13, 1));		
		
		FormLayout layoutPriceHistory = new FormLayout("320dlu,pref","pref,1dlu,pref,1dlu,pref,1dlu,pref,1dlu,pref,1dlu,pref,1dlu");
		PanelBuilder builderPriceHistory = new PanelBuilder(layoutPriceHistory,panelPriceHistory);		
		CellConstraints cellPriceHistory = new CellConstraints();	
		builderPriceHistory.add(labelPriceHistory, cellPriceHistory.xy(1,1));
		builderPriceHistory.add(panelInfoPriceHistory, cellPriceHistory.xy(1,3));
		builderPriceHistory.add(tabelPriceHistory, cellPriceHistory.xy(1,5));
 		
 		StatisticModel = new GridModel(StatisticDef.getHeader(), false);			
		tabelStatistic = createTable(StatisticModel, null, (Hashtable) hSetting.get("tabelStatistic"));
		tabelStatistic.setColumnHide(new int[] {Statistic.CIDX_PREV});
		tabelStatistic.setPreferredSize(new Dimension(0, 500));
		tabelStatistic.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);	
		
		FormLayout layoutStatistic = new FormLayout("320dlu,pref","pref,10dlu,pref");
		PanelBuilder builderStatistic = new PanelBuilder(layoutStatistic,panelStatistic);		
		CellConstraints cellStatistic = new CellConstraints();		
		builderStatistic.add(labelStatistic, cellStatistic.xy(1,1));
		//builderStatistic.add(new JLabel("adasdasd"), cellStatistic.xy(1,3));
		builderStatistic.add(tabelStatistic, cellStatistic.xy(1,3));
		
		FormLayout layoutPriceHistoryAndStatistic = new FormLayout("pref,1dlu,pref,1dlu","pref,1dlu,pref,1dlu");
		PanelBuilder builderNews = new PanelBuilder(layoutPriceHistoryAndStatistic,panelPriceHistoryAndStatistic);		
		CellConstraints cellNewsAndPriceHistory = new CellConstraints();		
		builderNews.add(panelPriceHistory, cellNewsAndPriceHistory.xy(1, 1));
		builderNews.add(panelStatistic,cellNewsAndPriceHistory.xy(3, 1));
		
//		scrollStatistic = new JScrollPane(panelPriceHistoryAndStatistic);
		
		btnViewPH.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {	
				if(!tempfrom.equals(C_DATEFORMAT.format((Date) dateFieldFromPH.getValue()).trim()) || !tempto.equals(C_DATEFORMAT.format((Date) dateFieldToPH.getValue()).trim())) {
//					tempfrom = C_DATEFORMAT.format((Date) dateFieldFromPH.getValue()).trim();
//					tempto = C_DATEFORMAT.format((Date) dateFieldToPH.getValue()).trim();
//					System.err.println("clikkkkkkkkkkkkkkkk");
					bindPriceHistory();
					
				}
			}
		});
		
		cmbPeriodPH.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				indexcmbperiodph = cmbPeriodPH.getSelectedIndex();
				
				bindPriceHistory();
				textfieldStock.setText(FirstStock);				
			}
			
		});
		
		refresh();
 	}

 	private void CompanyProfile() {
	    webBrowser = new JWebBrowser();
	    webBrowser.setBarsVisible(false);
	    webBrowser.setStatusBarVisible(true);
	    locationUrl = "file:///"+ new File("data").getAbsolutePath()+ File.separator + "blank" + ".htm";
	    webBrowser.navigate(locationUrl);	   
	}
}