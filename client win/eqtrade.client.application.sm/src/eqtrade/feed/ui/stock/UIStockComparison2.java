package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.TradeSummaryDef;

public class UIStockComparison2 extends UI {
	static Runnable pageloader;
	static Runnable pageloader1;
	static String pageThread;
	static String pageThread1;
	private static JEditorPane contentArea;
	private static JEditorPane contentArea1;
	private static String locationUrl="";
	private static String locationUrl1="";
	private JTextField fieldStock;
	private JTextField fieldStock1;
	private JLabel fieldName;
	private JLabel fieldName1;
	private String oldStock="";
	private String oldStock1="";
	private JPanel pnlInfo;
	private JButton btnGo;
	private JButton btnGo1;
	private  JScrollPane scroll;
	private  JScrollPane scroll1;
	private int oldVPos = 0;
	private int oldHPos = 0;
	private int oldVPos1 = 0;
	private int oldHPos1 = 0;
	public UIStockComparison2(String app) {
		super("Stock Comparison By Company Profile", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_STOCKCOMPARISON2);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldStock.requestFocus();
				fieldStock1.requestFocus();
						
			}
		});
	}
	private void getThePage(){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try{
					//log.info("Coba");
					//log.info("Hasil Location"+locationUrl);
					contentArea.setPage(locationUrl);
				}catch (Exception e) {
					e.printStackTrace();
					fieldName.setText("failed, Please try again");
					locationUrl="file:///"
						+ new File("data").getAbsolutePath()
						+File.separator+"blank"+".htm";
					getThePage();
				}
			}
		});
	}
	private void getThePage1(){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try{
					//log.info("Coba1");
					//log.info("Hasil Location1"+locationUrl1);
					contentArea1.setPage(locationUrl1);
				}catch (Exception e) {
					e.printStackTrace();
					fieldName.setText("failed, Please try again");
					locationUrl1="file:///"
						+ new File("data").getAbsolutePath()
						+File.separator+"blank"+".htm";
					getThePage1();
				}
			}
		});
	}
	/*AdjustmentListener adjustmentListener = new AdjustmentListener() {			
		@Override
		public void adjustmentValueChanged(AdjustmentEvent e) {
		int vPos = scroll.getVerticalScrollBar().getValue(),
			hPos = scroll.getHorizontalScrollBar().getValue();
		int vPos1 = scroll.getVerticalScrollBar().getValue(),
			hPos1 = scroll.getHorizontalScrollBar().getValue();
			
		if(e.getSource().equals(scroll.getVerticalScrollBar().getValue())&& vPos !=oldVPos){
			scroll1.getVerticalScrollBar().setValue(vPos);
			System.out.println("Scrool"+vPos);
			oldVPos=vPos;
		}else if(e.getSource().equals(scroll1).get)
		if(e.getSource().equals(scroll.getHorizontalScrollBar().getValue())&& hPos !=oldHPos){
			scroll1.getHorizontalScrollBar().setValue(hPos);
		}
			
			
		}
	};*/
	AdjustmentListener adjustmentListener = new AdjustmentListener() {

		@Override
		public void adjustmentValueChanged(AdjustmentEvent e) {
			int vPos = scroll.getVerticalScrollBar().getValue(), 
			    hPos = scroll.getHorizontalScrollBar().getValue(),
			    vPos1 = scroll1.getVerticalScrollBar().getValue(), 
			    hPos1 = scroll1.getHorizontalScrollBar().getValue();

			/*if (e.getSource().equals(scroll.getVerticalScrollBar()) 
					&& vPos != oldVPos) {
			
				scroll1.getVerticalScrollBar().setValue(vPos);
			
			}*/
			if (e.getSource().equals(scroll.getHorizontalScrollBar())
					&& hPos != oldHPos) {
			scroll1.getHorizontalScrollBar().setValue(hPos);
			
			}
			/*if (e.getSource().equals(scroll1.getVerticalScrollBar()) 
					&& vPos1 != oldVPos1) {
			
				scroll.getVerticalScrollBar().setValue(vPos1);
			
			}*/
			if (e.getSource().equals(scroll1.getHorizontalScrollBar())
					&& hPos1 != oldHPos1) {
				scroll.getHorizontalScrollBar().setValue(hPos1);
				
			}
		}
	};
	@Override
	protected void build() {
	
		contentArea  = new JEditorPane();
		contentArea1 = new JEditorPane();
		contentArea.setEditable(false);
		contentArea1.setEditable(false);
		
		scroll = new JScrollPane(contentArea);
		scroll1 = new JScrollPane(contentArea1);
		scroll.getVerticalScrollBar().addAdjustmentListener(adjustmentListener);
		scroll1.getVerticalScrollBar().addAdjustmentListener(adjustmentListener);
		scroll.getHorizontalScrollBar().addAdjustmentListener(adjustmentListener);
		scroll1.getHorizontalScrollBar().addAdjustmentListener(adjustmentListener);
		scroll.setPreferredSize(new Dimension(300,570));
		scroll1.setPreferredSize(new Dimension(300, 570));
		
		contentArea.addHyperlinkListener(new HyperlinkListener() {
			
			@Override
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if(e.getEventType()==HyperlinkEvent.EventType.ACTIVATED){
					locationUrl = e.getURL().toString();
					
					try{
						contentArea.setPage(locationUrl);
					}catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				
			}
		});
		contentArea1.addHyperlinkListener(new HyperlinkListener() {
			
			@Override
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if(e.getEventType()==HyperlinkEvent.EventType.ACTIVATED){
					locationUrl1 = e.getURL().toString();
					
					try{
						contentArea1.setPage(locationUrl1);
					}catch (Exception ex) {
						ex.printStackTrace();
					}
				}
				
			}
		});
		
		pnlInfo = new JPanel();
		
		fieldStock = new JTextField("");
		fieldName = new JLabel("please type a stock code");
		btnGo = new JButton("GO");
		btnGo.setFont(new Font("Tahoma", Font.BOLD,14));
		fieldStock1 = new JTextField("");
		fieldStock.setText("");
		fieldStock1.setText("");
		fieldName1 = new JLabel("please type a stock code");
		btnGo1 = new JButton("GO");
		btnGo1.setFont(new Font("Tahoma", Font.BOLD,14));
		
		FormLayout l = new FormLayout("pref,75px,pref,35dlu,265dlu,75px,pref,35dlu,265dlu,pref",
				"pref,pref,380dlu");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		 /*scroll = new JScrollPane(contentArea, 
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);*/
		/* scroll1 = new JScrollPane(contentArea1, 
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);*/

		
		
		b.add(fieldStock, c.xy(2, 1));
		b.add(btnGo,c.xy(4, 1));
		b.add(fieldName,c.xy(5, 1));
		b.add(scroll, c.xyw(2, 3, 4));
		
		b.add(fieldStock1, c.xy(6, 1));
		b.add(btnGo1,c.xy(8, 1));
		b.add(fieldName1,c.xy(9, 1));
		b.add(scroll1, c.xyw(6, 3, 4));
		
		//////////////
		
		
		
		//////////////
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlInfo);
		
		
		
		fieldStock.addFocusListener(new FocusAdapter() {
			
			public void focusGained(FocusEvent e) {
				fieldStock.selectAll();
			}
		});
		
		fieldStock.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fieldStockAction();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		fieldStock1.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fieldStockAction1();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		btnGo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				fieldStockAction();
			}
		});
		btnGo1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				fieldStockAction1();
				
			}
		});
		refresh();
		
		if (hSetting.get("stock") != null) {
			fieldStock.setText((String) hSetting.get("stock"));
			fieldStockAction();
			fieldStock1.setText((String) hSetting.get("stock"));
			fieldStockAction1();
		}
	}
	Thread threadLoad = null;
	private void fieldStockAction() {
		if (threadLoad != null) {
			threadLoad.stop();
			threadLoad = null;
		}
		threadLoad = new Thread(new Runnable() {
			@Override
			public void run() {
				String newStock = fieldStock.getText().trim().toUpperCase();
                fieldStock.setText(newStock);
                fieldStock.selectAll();
                if(!oldStock.equals(newStock)){


					//log.info("test2");
					locationUrl = "file:///" + new File("data").getAbsolutePath()
							+ File.separator + "loading" + ".htm";
					//log.info("location 1 "+locationUrl);
					getThePage();
					//log.info("test 3");
					//String newStock = fieldStock.getText().trim().toUpperCase();
					//log.info("test 4 "+newStock);
					//log.info("test 5 "+oldStock);
				
					if ((newStock.length() > 0 /*&& !oldStock.equals(newStock)*/)) {
						fieldStock.setText(newStock);
						oldStock = newStock;
						//log.info("isi stock "+newStock);
						//log.info("nama "+geStockName(newStock));
						//log.info("old stock "+oldStock);
						fieldName.setText(geStockName(newStock));
						
						//log.info("test 6"+fieldName.getText());
						if (fieldName.getText().equals("")) {
							File ftest = new File("data/temp/" + oldStock + ".zip");
							if (ftest.exists()) {
								//log.info("test 8 ");
								locationUrl = "file:///"
										+ new File("data/temp/"
												+ oldStock.toLowerCase() + "/")
												.getAbsolutePath() + File.separator
										+ oldStock.toLowerCase() + ".htm";
								//log.info("location 2 "+locationUrl);
								getThePage();
							} else {
								//log.info("test 7");
								try {
									byte[] result = ((IEQTradeApp) apps)
											.getFeedEngine().getEngine()
											.getConnection()
											.getFile("FILE" + "|" + oldStock);
									//log.info("isi"+result);
									//FileOutputStream fos = new FileOutputStream(
									//		"data/temp/" + oldStock + ".zip");
									
									FileOutputStream fos = new FileOutputStream("data/temp/" + oldStock + ".htm");
									fos.write(result);
									fos.close();
									//unzip(new ZipFile("data/temp/" + oldStock
								//			+ ".zip"), new File("data/temp"));
									locationUrl = "file:///"
											+ new File("data/temp/"
													+ oldStock.toLowerCase() + ".htm")
													.getAbsolutePath();
											
									//log.info("location 3  "+locationUrl);
									getThePage();
								} catch (Exception ex) {
									oldStock = "";
									fieldName.setText("failed, please try again");
									locationUrl = "file:///"
											+ new File("data").getAbsolutePath()
											+ File.separator + "blank" + ".htm";
									getThePage();
									// Utils.showMessage("failed, please try again",
									// UICompanyProfile.this.form);
									ex.printStackTrace();
								}
							}
						} else {
							locationUrl = "file:///"
									+ new File("data").getAbsolutePath()
									+ File.separator + "blank" + ".htm";
							getThePage();
						}
					}
					fieldStock.selectAll();
				}
			}   
		});
		threadLoad.start();
	}
	private void fieldStockAction1() {
		if (threadLoad != null) {
			threadLoad.stop();
			threadLoad = null;
		}
		threadLoad = new Thread(new Runnable() {
			@Override
			public void run() {
				String newStock1 = fieldStock1.getText().trim().toUpperCase();
                fieldStock1.setText(newStock1);
                fieldStock1.selectAll();
                if(!oldStock1.equals(newStock1)){

	
					//log.info("test2");
					locationUrl1 = "file:///" + new File("data").getAbsolutePath()
							+ File.separator + "loading" + ".htm";
					//log.info("location 1 "+locationUrl1);
					getThePage1();
					//log.info("test1 3");
					//String newStock1 = fieldStock1.getText().trim().toUpperCase();
					//log.info("test1 4 "+newStock1);
					//log.info("test1 5 "+oldStock1);
					
					if ((newStock1.length() > 0 /*&& !oldStock1.equals(newStock1)*/)) {
						fieldStock1.setText(newStock1);
						oldStock1 = newStock1;
						//log.info("isi stock "+newStock1);
						//log.info("nama "+geStockName(newStock1));
						//log.info("old stock "+oldStock1);
						fieldName1.setText(geStockName(newStock1));
						
						//log.info("test 6"+fieldName1.getText());
						if (fieldName1.getText().equals("")) {
							File ftest = new File("data/temp/" + oldStock1 + ".zip");
							if (ftest.exists()) {
								//log.info("test 8 ");
								locationUrl1 = "file:///"
										+ new File("data/temp/"
												+ oldStock1.toLowerCase() + "/")
												.getAbsolutePath() + File.separator
										+ oldStock1.toLowerCase() + ".htm";
								//log.info("location 2 "+locationUrl1);
								getThePage1();
							} else {
								//log.info("test 7");
								try {
									byte[] result = ((IEQTradeApp) apps)
											.getFeedEngine().getEngine()
											.getConnection()
											.getFile("FILE" + "|" + oldStock1);
									//log.info("isi"+result);
									//FileOutputStream fos = new FileOutputStream(
									//		"data/temp/" + oldStock + ".zip");
									
									FileOutputStream fos = new FileOutputStream("data/temp/" + oldStock1 + ".htm");
									fos.write(result);
									fos.close();
									//unzip(new ZipFile("data/temp/" + oldStock
								//			+ ".zip"), new File("data/temp"));
									locationUrl1 = "file:///"
											+ new File("data/temp/"
													+ oldStock1.toLowerCase() + ".htm")
													.getAbsolutePath();
											
									//log.info("location 3  "+locationUrl1);
									getThePage1();
								} catch (Exception ex) {
									oldStock1 = "";
									fieldName1.setText("failed, please try again");
									locationUrl1 = "file:///"
											+ new File("data").getAbsolutePath()
											+ File.separator + "blank" + ".htm";
									getThePage1();
									// Utils.showMessage("failed, please try again",
									// UICompanyProfile.this.form);
									ex.printStackTrace();
								}
							}
						} else {
							locationUrl1 = "file:///"
									+ new File("data").getAbsolutePath()
									+ File.separator + "blank" + ".htm";
							getThePage1();
						}
					}
					fieldStock1.selectAll();
				}
			}    
		});
		threadLoad.start();
	}
	private String geStockName(String code) {
		String name = "";
		//log.info("isi "+code);
		 int i = ((IEQTradeApp) apps).getTradingEngine()
		 .getStore(FeedStore.DATA_STOCK)
		 .getIndexByKey(new Object[] { code });
		// log.info("i :"+i);
		//log.info("i "+i);
		if (i > -1) {
			name = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_STOCK).getDataByIndex(i))
					.getName();
		}		
		return name;
	}
	
	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
		.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 750, 450));
		
	}
	protected void initUI() {
		super.initUI();
		form.setResizable(false);
		form.pack();
	}
	@Override
	public void refresh() {
		
		
	}

	@Override
	public void saveSetting() {
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
		
	}

}
