package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.StockComparisonBySector;
import eqtrade.feed.model.StockComparisonBySectorDef;
import eqtrade.feed.model.Sector;
public class UIStockComparisonBySector extends UI{
	private JGrid table;
	private JLabel lblSector;
	private JDropDown comboSector;
	private FilterStockComparisonBySector filterSector;
	private JButton btnViewAll;
	private JButton btnRefresh;
	private static SimpleDateFormat format = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
	private JLabel updateStockComparison;
	
	public UIStockComparisonBySector(String app) {
		super("Stock Comparison ", app);
		type=C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_STOCKCOMPARISONBYSECTOR);
		this.title="Sectoral Comparison By Financial Data";
		
	}
	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
			table.getTable().requestFocus();				
			}
		});
		
	}

	@Override
	protected void build() {
		btnViewAll = new JButton("View All");
		btnRefresh = new JButton("Refresh");
		//btnViewAll.setFont(new Font("Tahoma", Font.BOLD,12));
		filterSector = new FilterStockComparisonBySector(pnlContent);
		/*((FilterColumn)filterSector.getFilteredData("sector")).setField("");*/
		updateStockComparison = new JLabel("-");
		table = createTable(
				((IEQTradeApp)apps).getFeedEngine().getStore(
						FeedStore.DATA_STOCKCOMPARISONBYSECTOR), 
						filterSector, (Hashtable) hSetting.get("tableCom"));
		table.setColumnHide(new int[]{StockComparisonBySector.CIDX_SECTOR,StockComparisonBySector.CIDX_CLOSE});
		comboSector  = new  JDropDown(((IEQTradeApp)apps).getFeedEngine()
				.getStore(FeedStore.DATA_SECTOR),Sector.CIDX_SECTORNAME,true);
		comboSector.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(comboSector.getText().equals(""))
				{
					((FilterColumn)filterSector.getFilteredData("sector")).setField(null);
					filterSector.fireFilterChanged();
					//System.out.println("Null");
				}else{
					Sector sector = (Sector) ((IEQTradeApp) apps)
									.getFeedEngine()
									.getStore(FeedStore.DATA_SECTOR)
									.getDataByKey(new  String[]{comboSector.getText()});
					((FilterColumn)filterSector.getFilteredData("sector")).setField(sector.getCIDX_SECTORCODE().toString());
					filterSector.fireFilterChanged();
					//System.out.println("Isi");
				}
			}
		});
		btnViewAll.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				comboSector.setSelectedItem(null);
				((FilterColumn)filterSector.getFilteredData("sector")).setField(null);
				filterSector.fireFilterChanged();
				
			}
		});
		btnRefresh.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
					
					((IEQTradeApp)apps).getFeedEngine().unsubscribe(
							FeedParser.PARSER_COMPARISONBYSECTOR, 
							FeedParser.PARSER_COMPARISONBYSECTOR);
					((IEQTradeApp)apps).getFeedEngine().unsubscribe(
							FeedParser.PARSER_SECTOR, 
							FeedParser.PARSER_SECTOR);
					((IEQTradeApp)apps).getFeedEngine().subscribe(
							FeedParser.PARSER_COMPARISONBYSECTOR, 
							FeedParser.PARSER_COMPARISONBYSECTOR);
					((IEQTradeApp)apps).getFeedEngine().subscribe(
							FeedParser.PARSER_SECTOR, 
							FeedParser.PARSER_SECTOR);
				
				
			}
		});
		/*comboSector = new JDropDown(((IEQTradeApp)apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK),Stock.C_ID,true);*/
		lblSector = new JLabel("Sector");
		
		
		JPanel pnltop = new JPanel();
		FormLayout layout = new FormLayout("2dlu,35px,2dlu,230px,2dlu,80px,2dlu,80px","2dlu,pref,2dlu,pref,2dlu,pref");
		CellConstraints cell = new CellConstraints();
		PanelBuilder builder = new PanelBuilder(layout, pnltop);
		builder.add(lblSector, cell.xy(2, 2));
		builder.add(comboSector, cell.xy(4, 2));
		builder.add(btnViewAll, cell.xy(6, 2));
		builder.add(btnRefresh, cell.xy(8, 2));
		//builder.add(updateStockComparison,cell.xy(4, 4));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2,2,2,2));
		pnlContent.add(pnltop,BorderLayout.NORTH);
		pnlContent.add(table,BorderLayout.CENTER);
		((IEQTradeApp)apps).getFeedEngine()
			.getStore(FeedStore.DATA_STOCKCOMPARISONBYSECTOR)
			.addTableModelListener(new TableModelListener() {
				
				@Override
				public void tableChanged(TableModelEvent arg0) {
					// TODO Auto-generated method stub
					priceChanged(0);
				}
			});
		
		refresh();
		priceChanged(0);
	}

	private void priceChanged(int type) {
		String dt = "last update: " + format.format(new Date());
		if (type == 0) {
			updateStockComparison.setText(dt);
		} else{
			//System.out.println("lalalalalala");
		}
	}
	
	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
					.getLayout((String)getProperty(C_PROPERTY_NAME));
		if(hSetting==null){
			hSetting = new Hashtable();
		}
		hSetting.put("tableCom", StockComparisonBySectorDef.getTabledDef());
		((Hashtable) hSetting.get("tableCom")).put("header", 
				StockComparisonBySectorDef.dataHeader.clone());
		if(hSetting.get("size") ==null)
			hSetting.put("size", new Rectangle(20,20,500,300));
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void saveSetting() {
		hSetting.put("size", 
				new Rectangle(form.getX(), form.getY(), form.getWidth(), 
						form.getHeight()));
		hSetting.put("tableCom", table.getTableProperties());
		FeedSetting.putLayout((String)getProperty(C_PROPERTY_NAME), hSetting);
		
	}
	@Override
	public void show(){
		if(!form.isVisible()){
		((IEQTradeApp)apps).getFeedEngine().subscribe(
				FeedParser.PARSER_COMPARISONBYSECTOR, 
				FeedParser.PARSER_COMPARISONBYSECTOR);
		((IEQTradeApp)apps).getFeedEngine().subscribe(
				FeedParser.PARSER_SECTOR, 
				FeedParser.PARSER_SECTOR);
		}
		super.show();
	}
	@Override
	public void hide(){
		((IEQTradeApp)apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_COMPARISONBYSECTOR, 
				FeedParser.PARSER_COMPARISONBYSECTOR);
		((IEQTradeApp)apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_SECTOR, 
				FeedParser.PARSER_SECTOR);
		super.hide();

	}
		@Override
	public void close(){
		((IEQTradeApp)apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_COMPARISONBYSECTOR, 
				FeedParser.PARSER_COMPARISONBYSECTOR);
		((IEQTradeApp)apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_SECTOR, 
				FeedParser.PARSER_SECTOR);

		super.close();
	}
	

}
