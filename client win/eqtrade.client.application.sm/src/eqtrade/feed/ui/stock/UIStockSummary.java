package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import net.sf.nachocalendar.CalendarFactory;
import net.sf.nachocalendar.components.DateField;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Board;
import eqtrade.feed.model.Broker;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;
import feed.admin.Utils;

public class UIStockSummary extends UI {
	private JGrid table;
	private GridModel model;
	private JPanel pnlInfo;
	private DateField dfFrom;
	private DateField dfTo;
	private JDropDown comboInv;
	private JDropDown comboBoard;
	private JButton btnView;

	private JTextField fieldBroker;
	private JLabel fieldName;
	private String oldBroker = "";

	private JLabel fieldVal;
	private JLabel fieldLot;

	public UIStockSummary(String app) {
		super("Buy/Sell Stock Summary on Broker", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_STOCKSUMMARY);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldBroker.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem mnBrokerSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) model.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_BROKERSUMMARY, param);
				}
			}
		});
		mnBrokerSumm.setText("Buy/Sell Broker Summary");
		popupMenu.add(mnBrokerSumm);

		JMenuItem mnBrokerActivity = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) model.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_BROKERACTIVITY, param);
				}
			}
		});
		mnBrokerActivity.setText("Broker Activity by Stock");
		popupMenu.add(mnBrokerActivity);

		JMenuItem mnInvStock = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) model.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_INVSTOCK, param);
				}
			}
		});
		mnInvStock.setText("Foreign/Domestic by Stock");
		popupMenu.add(mnInvStock);

		JMenuItem mnTimeTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) model.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_TIMETRADE, param);
				}
			}
		});
		mnTimeTrade.setText("Time & Trade Summary");
		popupMenu.add(mnTimeTrade);

		JMenuItem mnCompany = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					int i = table.getMappedRow(table.getSelectedRow());
					if (i > -1) {
						TradeSummary order = (TradeSummary) model
								.getDataByIndex(i);
						HashMap param = new HashMap();
						param.put("STOCK", order.getStock());
						apps.getUI().showUI(FeedUI.UI_COMPANYPROFILE, param);
					}
				} catch (Exception ex) {
					Utils.showMessage("cannot open Browser", null);
				}
			}
		});
		mnCompany.setText("Company Profile");
		popupMenu.add(mnCompany);
		JMenuItem mnIntraday = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) model.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_INTRADAY, param);
				}
			}
		});
		mnIntraday.setText("Intraday Chart");
		popupMenu.add(mnIntraday);

		JMenuItem mnTechnical = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) model.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_CHART, param);
				}
			}
		});
		mnTechnical.setText("Technical Chart");
		popupMenu.add(mnTechnical);
		JMenuItem mnTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) model.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_LIVETRADESTOCK, param);
				}
			}
		});
		mnTrade.setText("Live Trade by Stock");
		popupMenu.add(mnTrade);

		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		propertiesMenu.setText("Properties");
	}

	public static final SimpleDateFormat C_DATEFORMAT = new SimpleDateFormat(
			" yyyy-MM-dd ");

	@Override
	protected void build() {
		createPopup();
		model = new GridModel(TradeSummaryDef.getHeader(), false);
		table = createTable(model, null, (Hashtable) hSetting.get("table"));
		// table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});

		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO, TradeSummary.CIDX_BOARD,
				TradeSummary.CIDX_BROKER, TradeSummary.CIDX_BROKERNAME,
				TradeSummary.CIDX_INVESTOR, TradeSummary.CIDX_BUYVOL,
				TradeSummary.CIDX_SELLVOL, TradeSummary.CIDX_VOLTOT,
				TradeSummary.CIDX_LOTTOT, TradeSummary.CIDX_VALTOT,
				TradeSummary.CIDX_BUYFREQ, TradeSummary.CIDX_SELLFREQ,
				TradeSummary.CIDX_FREQTOT, TradeSummary.CIDX_FREQTOT,
				TradeSummary.CIDX_VOLNET });

		pnlInfo = new JPanel();
		fieldBroker = new JTextField("");
		fieldName = new JLabel("please type a broker code");
		dfFrom = CalendarFactory.createDateField();
		dfFrom.setDateFormat(C_DATEFORMAT);
		dfTo = CalendarFactory.createDateField();
		dfTo.setDateFormat(C_DATEFORMAT);
		comboInv = new JDropDown(((IEQTradeApp) apps).getFeedEngine().getStore(
				FeedStore.DATA_INVESTOR), Board.CIDX_CODE);
		comboInv.setSelectedIndex(0);
		comboBoard = new JDropDown(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_BOARD), Board.CIDX_CODE);
		comboBoard.setSelectedIndex(0);
		btnView = new JButton("View");

		FormLayout l = new FormLayout(
				"pref,2dlu,50px,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref",
				"pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(new JLabel("Broker"), c.xy(1, 1));
		b.add(fieldBroker, c.xy(3, 1));
		b.add(new JLabel("from"), c.xy(5, 1));
		b.add(dfFrom, c.xy(7, 1));
		b.add(new JLabel("to"), c.xy(9, 1));
		b.add(dfTo, c.xy(11, 1));
		b.add(new JLabel("inv"), c.xy(13, 1));
		b.add(comboInv, c.xy(15, 1));
		b.add(new JLabel("Board"), c.xy(17, 1));
		b.add(comboBoard, c.xy(19, 1));
		b.add(btnView, c.xy(21, 1));

		fieldBroker.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fieldBrokerAction();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);

		btnView.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fieldBrokerAction();
			}
		});
		fieldVal = new JLabel(" ", SwingConstants.RIGHT);
		fieldLot = new JLabel(" ", SwingConstants.RIGHT);
		fieldVal.setText("0");
		fieldLot.setText("0");
		FormLayout ll = new FormLayout(
				"pref, 2dlu, 100px, 6dlu, pref, 2dlu, 100px", "30px");
		CellConstraints cc = new CellConstraints();
		PanelBuilder bb = new PanelBuilder(ll);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		bb.add(new JLabel("   T.Val  "), cc.xy(1, 1));
		bb.add(fieldVal, cc.xy(3, 1));
		bb.add(new JLabel("T.Lot  "), cc.xy(5, 1));
		bb.add(fieldLot, cc.xy(7, 1));

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(b.getPanel(), BorderLayout.NORTH);
		pnlContent.add(table, BorderLayout.CENTER);
		pnlContent.add(bb.getPanel(), BorderLayout.SOUTH);

		model.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				summChanged();
			}
		});
		refresh();
	}

	private static NumberFormat formatter = new DecimalFormat("#,##0  ");

	private void summChanged() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				double val = 0, lot = 0;
				for (int i = 0; i < model.getRowCount(); i++) {
					TradeSummary ts = (TradeSummary) model.getDataByIndex(i);
					val = val + ts.getValtot().doubleValue();
					lot = lot + ts.getLottot().doubleValue();
				}
				fieldVal.setText(formatter.format(val));
				fieldLot.setText(formatter.format(lot));
			}
		});
	}

	private void fieldBrokerAction() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					String newBroker = fieldBroker.getText().trim()
							.toUpperCase();
					String from = C_DATEFORMAT.format((Date) dfFrom.getValue())
							.trim();
					String to = C_DATEFORMAT.format((Date) dfTo.getValue())
							.trim();
					String iv = ((Board) ((IEQTradeApp) apps).getFeedEngine()
							.getStore(FeedStore.DATA_INVESTOR)
							.getDataByIndex(comboInv.getSelectedIndex()))
							.getName();
					String board = ((Board) ((IEQTradeApp) apps)
							.getFeedEngine().getStore(FeedStore.DATA_BOARD)
							.getDataByIndex(comboBoard.getSelectedIndex()))
							.getName();
					if (((Date) dfTo.getValue()).getTime() < ((Date) dfFrom
							.getValue()).getTime()) {
						Utils.showMessage(
								"Aborted, please enter valid date parameter",
								null);
						return;
					} else if ((((Date) dfTo.getValue()).getTime() - ((Date) dfFrom
							.getValue()).getTime()) / 86400000L > 5) {
						Utils.showMessage(
								"Warning, range parameter more than 5days, it will take some time",
								null);
					}

					iv = iv.equals("ALL") ? "%" : iv;
					board = board.equals("ALL") ? "%" : board;
					setEnabled(false);
					fieldBroker.setText(newBroker);
					oldBroker = newBroker;
					fieldName.setText(geBrokerName(newBroker));
					form.setTitle("Buy/Sell Stock Summary on " + oldBroker);
					String result = ((IEQTradeApp) apps)
							.getFeedEngine()
							.getEngine()
							.getConnection()
							.getHistory(
									"TSB" + "|" + oldBroker + "|" + from + "|"
											+ to + "|" + iv + "|" + board);
					model.getDataVector().clear();
					model.refresh();
					if (result != null) {
						String as[] = result.split("\n", -2);
						int aslength = as.length - 1;
						Vector vRow = new Vector(100, 5);
						for (int j = 0; j < aslength; j++) {
							String s1 = as[j];
							if (!s1.trim().equals("")) {
								TradeSummary trade = new TradeSummary(s1);
								trade.calculate();
								trade.setBrokername(getBrokerName(trade
										.getBroker()));
								trade.setStockname(getStockName(trade
										.getStock()));
								vRow.addElement(TradeSummaryDef
										.createTableRow(trade));
							}
						}
						model.addRow(vRow, false, false);
						model.refresh();
					} else {
						Utils.showMessage("no data available", null);
					}
				} catch (Exception ex) {
					Utils.showMessage("failed, please try again", null);
				}
				setEnabled(true);
			}
		});
	}

	protected String getStockName(String code) {
		Stock s = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK)
				.getDataByKey(new Object[] { code }));
		return (s != null) ? s.getName() : "";
	}

	protected String getBrokerName(String code) {
		Broker s = ((Broker) ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_BROKER)
				.getDataByKey(new Object[] { code }));
		return (s != null) ? s.getName() : "";
	}

	private void setEnabled(boolean enabled) {
		fieldBroker.setEnabled(enabled);
		dfFrom.setEnabled(enabled);
		dfTo.setEnabled(enabled);
		comboInv.setEnabled(enabled);
		comboBoard.setEnabled(enabled);
		btnView.setEnabled(enabled);
	}

	private String geBrokerName(String code) {
		String name = "";
		int i = ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_BROKER)
				.getIndexByKey(new Object[] { code });
		if (i > -1) {
			name = ((Broker) ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_BROKER).getDataByIndex(i))
					.getName();
		}
		return name;
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableHisDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 600, 350));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		hSetting.put("broker", oldBroker.toUpperCase());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		fieldName.setForeground(FeedSetting.getColor(FeedSetting.C_FOREGROUND));
		pnlInfo.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
			fieldBroker.setText(par.get("BROKER").toString());
			btnView.doClick();
		}
		show();
	}
}
