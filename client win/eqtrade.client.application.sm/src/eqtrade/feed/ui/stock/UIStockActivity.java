package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Broker;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;

public class UIStockActivity extends UI {
	private JGrid table;
	private JGrid tableTrade;
	private JPanel pnlInfo;

	private FilterTradeSummary filter;
	private FilterTradeSummary filterTrade;
	private JTextField fieldBroker;
	private JLabel fieldName;
	private String oldBroker = "";

	public UIStockActivity(String app) {
		super("Stock Activity by Broker", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_STOCKACTIVITY);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldBroker.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem mnBrokerSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(tableTrade.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMBS)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_BROKERSUMMARY, param);
				}
			}
		});
		mnBrokerSumm.setText("Buy/Sell Broker Summary");
		popupMenu.add(mnBrokerSumm);

		JMenuItem mnBrokerActivity = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(tableTrade.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMBS)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_BROKERACTIVITY, param);
				}
			}
		});
		mnBrokerActivity.setText("Broker Activity by Stock");
		popupMenu.add(mnBrokerActivity);

		JMenuItem mnInvStock = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(tableTrade.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMBS)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_INVSTOCK, param);
				}
			}
		});
		mnInvStock.setText("Foreign/Domestic by Stock");
		popupMenu.add(mnInvStock);

		JMenuItem mnTimeTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(tableTrade.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMBS)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_TIMETRADE, param);
				}
			}
		});
		mnTimeTrade.setText("Time & Trade Summary");
		popupMenu.add(mnTimeTrade);

		JMenuItem mnCompany = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					int i = tableTrade.getMappedRow(tableTrade.getSelectedRow());
					if (i > -1) {
						TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_TRADESUMBS)
								.getDataByIndex(i);
						HashMap param = new HashMap();
						param.put("STOCK", order.getStock());
						apps.getUI().showUI(FeedUI.UI_COMPANYPROFILE, param);
					}
				} catch (Exception ex) {
					Utils.showMessage("cannot open Browser", null);
				}
			}
		});
		mnCompany.setText("Company Profile");
		popupMenu.add(mnCompany);
		JMenuItem mnIntraday = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(tableTrade.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMBS)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_INTRADAY, param);
				}
			}
		});
		mnIntraday.setText("Intraday Chart");
		popupMenu.add(mnIntraday);

		JMenuItem mnTechnical = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(tableTrade.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMBS)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_CHART, param);
				}
			}
		});
		mnTechnical.setText("Technical Chart");
		popupMenu.add(mnTechnical);
		JMenuItem mnTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = tableTrade.getMappedRow(tableTrade.getSelectedRow());
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMBS)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_LIVETRADESTOCK, param);
				}
			}
		});
		mnTrade.setText("Live Trade by Stock");
		popupMenu.add(mnTrade);

		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		propertiesMenu.setText("Properties");
	}

	@Override
	protected void build() {
		createPopup();
		filter = new FilterTradeSummary(pnlContent);
		((FilterColumn) filter.getFilteredData("broker")).setField("ALL");
		filterTrade = new FilterTradeSummary(pnlContent);
		table = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_TRADESUMBROKER), filter,
				(Hashtable) hSetting.get("table"));
		// table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO, TradeSummary.CIDX_STOCK,
				TradeSummary.CIDX_STOCKNAME, TradeSummary.CIDX_BOARD,
				TradeSummary.CIDX_BROKER, TradeSummary.CIDX_BROKERNAME,
				TradeSummary.CIDX_INVESTOR, TradeSummary.CIDX_BUYAVG,
				TradeSummary.CIDX_SELLAVG, TradeSummary.CIDX_BUYVOL,
				TradeSummary.CIDX_SELLVOL, TradeSummary.CIDX_VOLTOT,
				TradeSummary.CIDX_VOLNET, TradeSummary.CIDX_LOTNET,
				TradeSummary.CIDX_VALNET });

		tableTrade = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_TRADESUMBS), filterTrade,
				(Hashtable) hSetting.get("tabletrade"));
		// tableTrade.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		tableTrade.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});

		tableTrade.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_TYPE, Model.CIDX_SEQNO, TradeSummary.CIDX_BOARD,
				TradeSummary.CIDX_BROKER, TradeSummary.CIDX_BROKERNAME,
				TradeSummary.CIDX_INVESTOR, TradeSummary.CIDX_BUYVOL,
				TradeSummary.CIDX_SELLVOL, TradeSummary.CIDX_BUYLOT,
				TradeSummary.CIDX_SELLLOT, TradeSummary.CIDX_VOLTOT,
				TradeSummary.CIDX_LOTTOT, TradeSummary.CIDX_VALTOT,
				TradeSummary.CIDX_FREQTOT, TradeSummary.CIDX_VOLNET });

		pnlInfo = new JPanel();
		fieldBroker = new JTextField("");
		fieldName = new JLabel("please type a broker code");

		FormLayout l = new FormLayout("50px,2dlu,50px, 2dlu, pref:grow, pref",
				"pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(fieldBroker, c.xy(1, 1));
		b.add(fieldName, c.xyw(3, 1, 3));

		JSkinPnl header = new JSkinPnl();
		header.add(b.getPanel(), BorderLayout.NORTH);
		header.add(table, BorderLayout.CENTER);
		table.setPreferredSize(new Dimension(100, 60));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(header, BorderLayout.NORTH);
		pnlContent.add(tableTrade, BorderLayout.CENTER);

		fieldBroker.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				fieldBrokerAction();
			}

			@Override
			public void focusGained(FocusEvent e) {
				fieldBroker.selectAll();
			}
		});
		fieldBroker.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fieldBrokerAction();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		refresh();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (hSetting.get("broker") != null) {
					oldBroker = "";
					fieldBroker.setText((String) hSetting.get("broker"));
					fieldBrokerAction();
				}
			}
		});
	}

	private void fieldBrokerAction() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String newBroker = fieldBroker.getText().trim().toUpperCase();
				if ((newBroker.length() > 0 && !oldBroker.equals(newBroker))) {
					if (oldBroker.length() > 0) {
						((IEQTradeApp) apps).getFeedEngine()
								.unsubscribe(
										FeedParser.PARSER_TRADESUMMBS,
										FeedParser.PARSER_TRADESUMMBS + "|"
												+ oldBroker);
					}
					fieldBroker.setText(newBroker);
					oldBroker = newBroker;
					((FilterColumn) filter.getFilteredData("broker"))
							.setField(newBroker);
					filter.fireFilterChanged();
					((FilterColumn) filterTrade.getFilteredData("broker"))
							.setField(newBroker);
					filterTrade.fireFilterChanged();
					fieldName.setText(geBrokerName(newBroker));
					form.setTitle("Stock Activity by " + oldBroker);
					((IEQTradeApp) apps).getFeedEngine().subscribe(
							FeedParser.PARSER_TRADESUMMBS,
							FeedParser.PARSER_TRADESUMMBS + "|" + oldBroker);
				}
				fieldBroker.selectAll();
			}
		});
	}

	private String geBrokerName(String code) {
		String name = "";
		int i = ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_BROKER)
				.getIndexByKey(new Object[] { code });
		if (i > -1) {
			name = ((Broker) ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_BROKER).getDataByIndex(i))
					.getName();
		}
		return name;
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableStockAcHeaderDef());
			hSetting.put("tabletrade",
					TradeSummaryDef.getTableStockAcDetailDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 600, 350));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		hSetting.put("tabletrade", tableTrade.getTableProperties());
		hSetting.put("broker", oldBroker.toUpperCase());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableTrade.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		fieldName.setForeground(FeedSetting.getColor(FeedSetting.C_FOREGROUND));
		pnlInfo.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
		tableTrade.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
			fieldBroker.setText(par.get("BROKER").toString());
			fieldBrokerAction();
		}
		show();
	}

	@Override
	public void show() {
		if (!form.isVisible())
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_TRADESUMMBROKER,
					FeedParser.PARSER_TRADESUMMBROKER);
		super.show();
	}

	@Override
	public void hide() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_TRADESUMMBROKER,
				FeedParser.PARSER_TRADESUMMBROKER);
		// if (!oldBroker.equals(""))
		// ((IEQTradeApp)apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADESUMMBS,
		// FeedParser.PARSER_TRADESUMMBS+"|"+oldBroker);
		super.hide();
	}

	@Override
	public void close() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_TRADESUMMBROKER,
				FeedParser.PARSER_TRADESUMMBROKER);
		if (!oldBroker.equals(""))
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(
					FeedParser.PARSER_TRADESUMMBS,
					FeedParser.PARSER_TRADESUMMBS + "|" + oldBroker);
		super.close();
	}
}
