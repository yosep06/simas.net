package eqtrade.feed.ui.market;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.vollux.framework.UI;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.News;
import eqtrade.feed.model.NewsDef;

public class UINews extends UI {
	private JGrid table;

	public UINews(String app) {
		super("Market News", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_NEWS);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		table = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_NEWS), null,
				(Hashtable) hSetting.get("table"));
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		// table.getTable().add(popupMenu);
		// table.getTable().addMouseListener(new MyMouseAdapter());
		// table.addMouseListener(new MyCustomMouseAdapter());
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO });
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					int i = table.getMappedRow(table.getSelectedRow());
					if (i > -1) {
						News order = (News) ((IEQTradeApp) apps)
								.getFeedEngine().getStore(FeedStore.DATA_NEWS)
								.getDataByIndex(i);
						HashMap map = new HashMap();
						map.put("content", order.getContent());
						apps.getUI().showUI(FeedUI.UI_DLGNEWS, map);
					}
				}
			}
		});
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		refresh();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", NewsDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 400, 300));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void show() {
		if (!form.isVisible())
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_NEWS, FeedParser.PARSER_NEWS);
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_NEWS, FeedParser.PARSER_NEWS);
	}
}
