package eqtrade.feed.ui.market;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import com.vollux.border.TextBorder;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinForm;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;

public class UITicker extends UI {
	private TickerPanel pnl;

	public UITicker(String app) {
		super("Ticker", app);
		setProperty(C_PROPERTY_NAME, FeedUI.UI_TICKER);
	}

	@Override
	public void focus() {
	}

	@Override
	public void refresh() {
	}

	@Override
	protected void initUI() {
		form = new JSkinForm(title, pnlContent);
		form.setMinimumSize(new Dimension(100, 20));
		form.setAlwaysOnTop(true);
		form.getInsidePanel().setBorder(new EmptyBorder(0, 1, 1, 1));
		form.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		form.getBtnMax().setVisible(false);
		form.getBtnMin().setVisible(false);
		if (icon != null)
			form.setIconImage(icon);
		form.setLocation(((Rectangle) hSetting.get("size")).x,
				((Rectangle) hSetting.get("size")).y);
		form.setSize(new Dimension(((Rectangle) hSetting.get("size")).width,
				((Rectangle) hSetting.get("size")).height));
		form.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				hide();
			}
		});
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		if (hSetting.get("size") == null) {
			hSetting.put("size", new Rectangle(10, 10, 900, 70));
		}
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	protected void build() {
		pnl = new TickerPanel(apps);

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(true);
		pnlContent.add(pnl, BorderLayout.CENTER);
		pnlContent.setBorder(new TextBorder());
		pnlContent.setBackground(Color.black);
	}

	@Override
	public void show() {
		if (!form.isVisible()) {
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
		}
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_GLOBALIDX, FeedParser.PARSER_GLOBALIDX);
	}
}
