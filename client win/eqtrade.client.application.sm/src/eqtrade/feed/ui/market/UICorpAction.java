package eqtrade.feed.ui.market;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.framework.UI.MyCustomMouseAdapter;
import com.vollux.framework.UI.MyMouseAdapter;
import com.vollux.idata.GridModel;
import com.vollux.ui.CustomMouseAdapter;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.CorpAction;
import eqtrade.feed.model.CorpActionDef;
import eqtrade.feed.model.IPO;
import eqtrade.feed.model.IPODef;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.RupsDef;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.AccTypeDef;
import eqtrade.trading.model.Stock;

public class UICorpAction extends UI {
	private JGrid table;
	private JGrid tableRight;
	private JGrid tableWarrant;
	private JGrid tableDividen;
	private JGrid tableIPO;
	private JGrid tableRUPS;
	private JDropDown comboAction;
	private JDropDown comboStock;
	private JButton btnView;
	private JButton btnClear;
	
	private FilterStockSplit filterStockSplit;
	private FilterRight filterRight;
	private FilterWarrant filterWarrant;
	private FilterCorpAction filterDividen;
	protected GridModel gCorpAction;
	private JTabbedPane tab;
	private String oldStock = "";
	private JPopupMenu popMenuIPO,popMenuRUPS,popMenuDividen,popMenuRight,popMenuSplit,popMenuWarrant;
	
	
	public UICorpAction(String app) {
		super("Corp. Action", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_CORPACTION);
		//log.info("test2");
		this.title = "Corp. Action";
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tableDividen.getTable().requestFocus();
				tableIPO.getTable().requestFocus();
				tableRUPS.getTable().requestFocus();
				tableRight.getTable().requestFocus();
				table.getTable().requestFocus();
				tableWarrant.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		//System.err.println("TES createPopup");
		/*popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);*/
		
		popMenuDividen = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				tableDividen.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popMenuDividen.add(propertiesMenu);
		
		popMenuIPO = new JPopupMenu();
		JMenuItem propertiesMenuIPO = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				tableIPO.showProperties();
			}
		});
		propertiesMenuIPO.setText("Properties");
		popMenuIPO.add(propertiesMenuIPO);
		
		popMenuRUPS = new JPopupMenu();
		JMenuItem propertiesMenuRUPS = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				tableRUPS.showProperties();
			}
		});
		propertiesMenuRUPS.setText("Properties");
		popMenuRUPS.add(propertiesMenuRUPS);
		
		popMenuRight = new JPopupMenu();
		JMenuItem propertiesMenuRight = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;
			
			@Override
			public void actionPerformed(ActionEvent e) {
				tableRight.showProperties();
			}
		});
		propertiesMenuRight.setText("Properties");
		popMenuRight.add(propertiesMenuRight);
		
		popMenuSplit = new JPopupMenu();
		JMenuItem propertiewsMenuSplit = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;
			
			@Override
			public void actionPerformed(ActionEvent e) {
				table.showProperties();
			}
		});
		propertiewsMenuSplit.setText("Properties");
		popMenuSplit.add(propertiewsMenuSplit);
		
		popMenuWarrant = new JPopupMenu();
		JMenuItem propertiewsMenuWarrant = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;
			
			@Override
			public void actionPerformed(ActionEvent e) {
				tableWarrant.showProperties();
			}
		});
		propertiewsMenuWarrant.setText("Properties");
		popMenuWarrant.add(propertiewsMenuWarrant);
	}

	@Override
	protected void build() {
		//System.err.println("TES BUILD");
		createPopup();
		oldStock = "";
		tab = new JTabbedPane();
		
		gCorpAction = new GridModel(CorpActionDef.getHeader(), false);
		gCorpAction.setKey(new int[] { CorpAction.CIDX_ACTIONTYPE });
		pnlContent = new JSkinPnl(new BorderLayout());
		comboStock = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);

		filterStockSplit = new FilterStockSplit(pnlContent);
		filterRight = new FilterRight(pnlContent);
		filterWarrant = new FilterWarrant(pnlContent);
		filterDividen = new FilterCorpAction(pnlContent);
				
		((FilterColumn) filterStockSplit.getFilteredData("action")).setField("Stock Split");
		((FilterColumn) filterRight.getFilteredData("action2")).setField("Right Issue");
		((FilterColumn) filterWarrant.getFilteredData("action3")).setField("Warrant");
		
		((FilterColumn) filterDividen.getFilteredData("action")).setField("Deviden Tunai");
		((FilterColumn) filterDividen.getFilteredData("action2")).setField("Deviden Stock");
		((FilterColumn) filterDividen.getFilteredData("action3")).setField("Deviden Stock");
		
		btnClear = new JButton("Clear");
		btnClear.setMnemonic('R');
		btnView = new JButton("View");
		btnView.setMnemonic('V');
		
/*
 * 
 *  
 *  
 *  
 *  
 *  	SEP TOLONG COPYIN KE MAC YA FORM INI
 *  
 *  
 *  
 *
 * 
 * */
		
		tableDividen = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterDividen,(Hashtable) hSetting.get("tableDividen"));
		tableDividen.setColumnHide(new int[] {CorpAction.CIDX_TRANSACTIONDATE, CorpAction.CIDX_LASTTRANSACTIONDATE});
		tableDividen.getTable().getModel().addTableModelListener(new RowChanged());
		tableDividen.getTable().add(popMenuDividen);
		tableDividen.getTable().addMouseListener(new MyMouseAdapter(){
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuDividen.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		tableDividen.getTable().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 1 && e.getClickCount() == 2) {
					popMenuDividen.show(e.getComponent(), e.getX(), e.getY());
					

				} else if (e.isPopupTrigger()) {
					popMenuDividen.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuDividen.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		tableDividen.addMouseListener(new CustomMouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuDividen.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuDividen.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		
		tableIPO = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_IPO), null,(Hashtable) hSetting.get("tableIPO"));
		tableIPO.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,Model.CIDX_SEQNO ,IPO.CIDX_EFFDATE,IPO.CIDX_OFFERSTART,IPO.CIDX_OFFEREND,IPO.CIDX_ALLOTMENT,IPO.CIDX_REFUNDDATE});
		tableIPO.getTable().add(popMenuIPO);
		tableIPO.getTable().addMouseListener(new MyMouseAdapter(){
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuIPO.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		tableIPO.getTable().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 1 && e.getClickCount() == 2) {
					popMenuIPO.show(e.getComponent(), e.getX(), e.getY());
					

				} else if (e.isPopupTrigger()) {
					popMenuIPO.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuIPO.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		tableIPO.addMouseListener(new CustomMouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuIPO.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuIPO.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		tableIPO.getTable().getModel().addTableModelListener(new RowChanged());
		
		tableRUPS = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_RUPS), null,(Hashtable) hSetting.get("tableRUPS"));
		tableRUPS.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,Model.CIDX_SEQNO });
		tableRUPS.getTable().add(popMenuRUPS);
		tableRUPS.getTable().addMouseListener(new MyMouseAdapter(){
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuRUPS.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		tableRUPS.getTable().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 1 && e.getClickCount() == 2) {
					popMenuRUPS.show(e.getComponent(), e.getX(), e.getY());
					

				} else if (e.isPopupTrigger()) {
					popMenuRUPS.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuRUPS.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		tableRUPS.addMouseListener(new CustomMouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuRUPS.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuRUPS.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		tableRUPS.getTable().getModel().addTableModelListener(new RowChanged());
		
		table = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterStockSplit, (Hashtable) hSetting.get("table"));
		table.setColumnHide(new int[] { CorpAction.CIDX_AMOUNT, CorpAction.CIDX_TRANSACTIONDATE, CorpAction.CIDX_LASTTRANSACTIONDATE, CorpAction.CIDX_EXPDATE });
		table.setSortColumn(CorpAction.CIDX_CUMDATE);
		table.setSortAscending(false);
		table.getTable().add(popMenuSplit);
		table.getTable().addMouseListener(new MyMouseAdapter(){
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuSplit.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		table.getTable().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 1 && e.getClickCount() == 2) {
					popMenuSplit.show(e.getComponent(), e.getX(), e.getY());
					

				} else if (e.isPopupTrigger()) {
					popMenuSplit.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuSplit.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		table.addMouseListener(new CustomMouseAdapter(){
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuSplit.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuSplit.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		table.getTable().getModel().addTableModelListener(new RowChanged());
		
		tableRight = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterRight, (Hashtable) hSetting.get("tableRight"));		
		tableRight.setColumnHide(new int[] { CorpAction.CIDX_EXPDATE });		
		tableRight.setSortColumn(CorpAction.CIDX_CUMDATE);
		tableRight.setSortAscending(false);
		tableRight.getTable().add(popMenuRight);
		tableRight.getTable().addMouseListener(new MyMouseAdapter(){
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuRight.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		tableRight.getTable().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 1 && e.getClickCount() == 2) {
					popMenuRight.show(e.getComponent(), e.getX(), e.getY());
					

				} else if (e.isPopupTrigger()) {
					popMenuRight.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuRight.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		tableRight.addMouseListener(new CustomMouseAdapter(){
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuRight.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuRight.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		tableRight.getTable().getModel().addTableModelListener(new RowChanged());
	
		tableWarrant = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterWarrant, (Hashtable) hSetting.get("tableWarrant"));
		tableWarrant.setColumnHide(new int[] { CorpAction.CIDX_RATIO1, CorpAction.CIDX_RATIO2,CorpAction.CIDX_CUMDATE, CorpAction.CIDX_RECORDDATE, CorpAction.CIDX_DISTDATE});
		tableWarrant.setSortColumn(CorpAction.CIDX_TRANSACTIONDATE);
		tableWarrant.setSortAscending(false);
		tableWarrant.getTable().add(popMenuWarrant);
		tableWarrant.getTable().addMouseListener(new MyMouseAdapter(){
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuWarrant.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		tableWarrant.getTable().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 1 && e.getClickCount() == 2) {
					popMenuWarrant.show(e.getComponent(), e.getX(), e.getY());
					

				} else if (e.isPopupTrigger()) {
					popMenuWarrant.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuWarrant.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});

		tableWarrant.addMouseListener(new CustomMouseAdapter(){
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuWarrant.show(e.getComponent(), e.getX(), e.getY());
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuWarrant.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		tableWarrant.getTable().getModel().addTableModelListener(new RowChanged());
				
//		tableDividen.getTable().addMouseListener(new MyMouseAdapter(){
//			@Override
//			public void mouseReleased(MouseEvent e) {
//				if (!e.isPopupTrigger()) {
//					popMenuDividen.show(e.getComponent(), e.getX(), e.getY());
//				}
//			}
//		});
//		
//		tableDividen.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mouseReleased(MouseEvent e) {
//				if (!e.isPopupTrigger()) {
//					popMenuDividen.show(e.getComponent(), e.getX(), e.getY());
//				}
//			}
//		});
		
		/*tableDividen.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popMenuDividen.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});*/
		
		
		
		JPanel pnlInfo = new JPanel();
		pnlInfo.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		FormLayout layout = new FormLayout("pref,2dlu,150px,2dlu,pref,2dlu,80px,2dlu,pref,2dlu,pref","pref");
		CellConstraints cc = new CellConstraints();

		JPanel temp2 = new JPanel(new BorderLayout());	
		temp2.add(table, BorderLayout.CENTER);
		
		JPanel panelRight = new JPanel(new BorderLayout());	
		panelRight.add(tableRight, BorderLayout.CENTER);
		
		JPanel panelWarrant = new JPanel(new BorderLayout());	
		panelWarrant.add(tableWarrant, BorderLayout.CENTER);		
		
		tab.addTab("Dividen", tableDividen);
		tab.addTab("IPO", tableIPO);
		tab.addTab("RUPS & Public Expose", tableRUPS);
		tab.addTab("Right Issue", panelRight);
		tab.addTab("Stock Split", temp2);		
		tab.addTab("Warrant", panelWarrant);
		
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(tab, BorderLayout.CENTER);
		
		refresh();
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (hSetting.get("stock") != null) {
					oldStock = "";					
				}
			}
		});
	}
	
	private TableModel createModelCorpAction(GridModel store, int cidxActiontype) {
		for (int i = 0; i < store.getRowCount(); i++) {
			Vector vorder = new Vector(1);
			vorder.addElement(CorpActionDef.createTableRow((CorpAction) store.realData.elementAt(i)));
			gCorpAction.addRow(vorder, false, false);
		}
		return gCorpAction;
	}

	@Override
	public void loadSetting() {		
		//System.err.println("TES loadSetting");
		hSetting = (Hashtable) FeedSetting.getLayout((String) getProperty(C_PROPERTY_NAME));	
		
		if (hSetting == null) {
			hSetting = new Hashtable();			
			
			hSetting.put("tableDividen", CorpActionDef.getTableDef());
			hSetting.put("tableIPO", IPODef.getTableDef());
			hSetting.put("tableRUPS", RupsDef.getTableDef());
			hSetting.put("tableRight", CorpActionDef.getTableDef());
			hSetting.put("table", CorpActionDef.getTableDef());
			hSetting.put("tableWarrant", CorpActionDef.getTableDef());
		
	    }
		
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 400, 300));
	    }

	/*@Override
	public void loadSetting() {
		
		hSetting = (Hashtable) FeedSetting
		.getLayout((String) getProperty(C_PROPERTY_NAME));

		
		if (hSetting == null) {
			hSetting = new Hashtable();
			
			hSetting.put("table", CorpActionDef.getTableDef());
			hSetting.put("tableIPO", IPODef.getTableDef());
			hSetting.put("tableRUPS", RupsDef.getTableDef());
			hSetting.put("tableRight", CorpActionDef.getTableDef());
			hSetting.put("tableStock", CorpActionDef.getTableDef());
			hSetting.put("tableWarrant", CorpActionDef.getTableDef());
		}
//		hSetting.put("table", CorpActionDef.getTableDef());
//		hSetting.put("tableIPO", IPODef.getTableDef());
//		hSetting.put("tableRUPS", RupsDef.getTableDef());

		
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 400, 300));
		
	}*/
	
	@Override
	public void saveSetting() {

		hSetting.put("size",new Rectangle(form.getX(), form.getY(), form.getWidth(), form.getHeight()));
		
		hSetting.put("tableDividen", tableDividen.getTableProperties());
		hSetting.put("tableIPO", tableIPO.getTableProperties());
		hSetting.put("tableRUPS", tableRUPS.getTableProperties());
		hSetting.put("tableRight", tableRight.getTableProperties());
		hSetting.put("table", table.getTableProperties());
		hSetting.put("tableWarrant", tableWarrant.getTableProperties());
		hSetting.put("stock", oldStock.toUpperCase());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}
	
	void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),"enterAction");

		InputMap inputMapCombo = ((JDropDown) comp).getTextEditor().getInputMap(JComponent.WHEN_FOCUSED);
		inputMapCombo.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),"enterAction");

		inputMapCombo.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0),"tabAction");

		((JDropDown) comp).getTextEditor().getActionMap().put("enterAction", new AbstractAction("enterAction") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(comboStock.getTextEditor())) {
					comboStock.setSelectedIndex(comboStock.getSelectedIndex());
					oldStock=comboStock.getText().trim().toUpperCase();
					comboStock.hidePopup();
					btnView.requestFocus();

				} else if (e.getSource().equals(comboAction.getTextEditor())) {
					comboAction.setSelectedIndex(comboAction.getSelectedIndex());
					comboAction.hidePopup();
					comboStock.requestFocus();
				}
			}
		});
	} 
	
	@Override
	public void refresh() {
		System.err.println("TES refresh");
		tableDividen.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableDividen.setNewFont(FeedSetting.getFont());
		
		tableIPO.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableIPO.setNewFont(FeedSetting.getFont());
		
		tableRUPS.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableRUPS.setNewFont(FeedSetting.getFont());
		
		tableRight.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableRight.setNewFont(FeedSetting.getFont());
		
		table.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
		
		tableWarrant.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableWarrant.setNewFont(FeedSetting.getFont());
		
		
	}

	@Override
	public void show() {
		//System.err.println("TES show");
		if (!form.isVisible())
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_CORPACTION, FeedParser.PARSER_CORPACTION);
		//Thread.sleep(500);
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_IPO, FeedParser.PARSER_IPO);
		//Thread.sleep(500);
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_RUPS, FeedParser.PARSER_RUPS);					
	
			
//			SwingUtilities.invokeLater(new Runnable() {				
//				@Override
//				public void run() {
//					try {
//						((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_CORPACTION, FeedParser.PARSER_CORPACTION);
//						//Thread.sleep(500);
//						((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_IPO, FeedParser.PARSER_IPO);
//						//Thread.sleep(500);
//						((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_RUPS, FeedParser.PARSER_RUPS);					
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//				}
//			});
//			
			super.show();
	}

	@Override
	public void hide() {
		//System.err.println("TES hide");
		super.hide();
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_CORPACTION, FeedParser.PARSER_CORPACTION);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_IPO,FeedParser.PARSER_IPO);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_RUPS, FeedParser.PARSER_RUPS);
	}

	protected class RowChanged implements TableModelListener {
		public void tableChanged(final TableModelEvent e) {
			int size = table.getTable().getRowCount();
			for (int i = 0; i < size; i++) {
				CorpAction cp = (CorpAction) table.getDataByIndex(i);
                
				Vector vorder = new Vector(1);
				vorder.addElement(CorpActionDef.createTableRow(cp));
				gCorpAction.addRow(vorder, false, false);
			}
		}
	}
}
