package eqtrade.trading.ATS;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.InputMap;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.eclipse.swt.layout.GridLayout;
import org.jfree.chart.block.EmptyBlock;
import org.jfree.chart.block.GridArrangement;

import sun.java2d.pipe.SpanShapeRenderer.Simple;

import com.Ostermiller.util.Tabs;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.sun.org.apache.xpath.internal.axes.HasPositionalPredChecker;
import com.vollux.framework.UI;
import com.vollux.ui.JBtn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;
import com.vollux.ui.JTextLabel;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.StockSummary;
import eqtrade.trading.model.Stock;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.OrderIDGenerator;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.PortfolioDef;
import eqtrade.trading.model.Schedule;
import eqtrade.trading.model.UserProfile;

public class UINewSchedule extends UI{/*
	private JDropDown comboStock = null;*/
	private JDropDown fieldStock= null;
	private JText fieldS3 ;
	private JText fieldS2 ;
	private JText fieldS1 ;
	private JText fieldR1 ;
	private JText fieldR2 ;
	private JText fieldR3 ;
	
	private JRadioButton radioSession;
	private JRadioButton radioDay;
	private JRadioButton radioGTC;
	
	private JButton btnSave;
	private JButton btnCancel;
	JPanel buyPanel, sellPanel, entPanel, stockPanel, pnlRS, validPanel, btnPanel ,valPanel;
	private JPanel headerEntry;
	private JDropDown combowatch;
	private JDropDown comboname;
	private JNumber fieldPrice ;
	private JNumber fieldQty;
	private JTextLabel stockTo = new JTextLabel("Stock to Buy");

	public static JTabbedPane tabCondition;
	public static JTabbedPane tabSellBuy;
	
	private JButton save;
	private JButton cancel;
	private ByPricePanel byPrice;
	private ByTrendPanel byTrend;
	private ByDonePanel byDone;
	private String stock;
	private String clientId;
	public static String ClientId;
	private String entryType;

	private static NumberFormat formatter = new DecimalFormat("#,##0 ");
	private static NumberFormat formatter2 = new DecimalFormat("#,##0.00");
	
	public UINewSchedule(String app) {
		super("Auto Trading System", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_NEWSCHEDULE);
	}
	
	private void cleanPivot(){
		fieldS3.setText(null);
		fieldS2.setText(null) ;
		fieldS1.setText(null) ;
		fieldR1.setText(null) ;
		fieldR2.setText(null) ;
		fieldR3.setText(null); 
	}

	@Override
	protected void build() {
		byPrice = new ByPricePanel(apps, form, hSetting);
		byPrice.build();
		byTrend = new ByTrendPanel(apps, form, hSetting);
		byTrend.build();
		byDone = new  ByDonePanel(apps, form, hSetting);
		byDone.build();
		// TODO Auto-generated method stub
		fieldS3 = new JText(false);		fieldS2 = new JText(false);	
		fieldS1 = new JText(false);		fieldR1 = new JText(false);
		fieldR2 = new JText(false);		fieldR3 = new JText(false);
		
		btnSave = new JButton("Save");
		btnCancel = new JButton("Cancel");
		
		radioSession = new JRadioButton("Session");				radioSession.setOpaque(false);
		radioDay = new JRadioButton("Day");						radioDay.setOpaque(false);
		radioGTC = new JRadioButton("GTC (7 trading days)");	radioGTC.setOpaque(false);
		radioGTC.setToolTipText("Good 'Till Cancel");
		radioDay.setToolTipText("DAY(Open 'Till 16.00PM Unless Match)");
		radioSession.setToolTipText("Session(2 Session Withdraw)");
		
		combowatch = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		comboname = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK),Stock.C_NAME,true);
		fieldPrice = new JNumber(Double.class, 0, 0, false, true);	
		fieldQty = new JNumber(Double.class, 0, 0, false, true);
		//ats limit price 10022016
		fieldPrice.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				if (!validPrice((Double)fieldPrice.getValue())) {
					fieldPrice.setValue(null);
					fieldPrice.requestFocus();
				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				
			}
		});
		/*comboStock = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		*/
		
		//registerEvent(combowatch.getTextEditor());
		registerEventCombo(combowatch);		
		//registerEvent(comboname.getTextEditor());
		registerEventCombo(comboname);
		
		combowatch.getTextEditor().addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				stockfocusLost(e);
				//System.out.println("lost focus");

				fieldPrice.setValue(null);
				fieldQty.setValue(null);
				byTrend.clean();
				byDone.clean();
				byPrice.clean();
			}
			
			@Override
			public void focusGained(FocusEvent arg0) {
				
			}
		});
		btnSave.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				sendSchedule();
			}
		});

		//validity
		validPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		validPanel.setBackground(Color.red.darker());
		validPanel.add(new JTextLabel("VALIDITY"));
		validPanel.add(radioSession);
		validPanel.add(radioDay);
		validPanel.add(radioGTC);
		
		pnlRS = new JPanel( new java.awt.GridLayout(2,6));
		pnlRS.setBackground(Color.red.darker());
		pnlRS.add(new JLabel("S3"));		pnlRS.add(new JLabel("S2"));
		pnlRS.add(new JLabel("S1"));		pnlRS.add(new JLabel("R1"));
		pnlRS.add(new JLabel("R2"));		pnlRS.add(new JLabel("R3"));
		pnlRS.add(fieldS3);	pnlRS.add(fieldS2); pnlRS.add(fieldS1);
		pnlRS.add(fieldR1); pnlRS.add(fieldR2); pnlRS.add(fieldR3);
		//stock to watch & technical analysis
		stockPanel = new JPanel();
		stockPanel.setBackground(Color.red.darker());
		FormLayout layout = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,100px, 2dlu, pref, 2dlu, 100px , 2dlu, pref, 2dlu, 100px",
				"pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref");
		PanelBuilder builder = new PanelBuilder(layout,stockPanel);
		stockPanel.setBorder(new EmptyBorder(4,4,4,4));
		CellConstraints cc = new CellConstraints();/*
		builder.add(new JTextLabel("Stock to Watch"),cc.xy(1, 1));
		builder.add(comboStock,cc.xyw(3,1,2));
		builder.add(comboSname,cc.xyw(5,1,9));*/
		builder.add(new JTextLabel("Technical Analysis"),cc.xy(1, 1,CellConstraints.LEFT, CellConstraints.BOTTOM));
		builder.add(pnlRS,cc.xyw(3,1,9));
		builder.add(validPanel,cc.xyw(1, 3, 9));
		
		ButtonGroup group = new ButtonGroup();
		group.add(radioSession);
		group.add(radioDay);
		group.add(radioGTC);
		//headerEntry.setPreferredSize(new Dimension(100, 60));
		radioDay.setSelected(true);
		//panel sell
		sellPanel = new JPanel();
		buyPanel = new JPanel();
		buyPanel.setBackground(Color.red.darker());
		sellPanel.setBackground(Color.green.darker());
		/*
		FormLayout sellLayout = new FormLayout(
				"pref,2dlu,50px,3dlu,pref,2dlu,50px, 2dlu, pref, 2dlu, 20px , 2dlu, pref, 2dlu,50px, pref, 2dlu,50px, pref, 2dlu,50px",
				"pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref");
		PanelBuilder sellBuilder = new PanelBuilder(sellLayout,sellPanel);
		sellBuilder.setBorder(new EmptyBorder(2,2,2,2));
		sellBuilder.add(new JTextLabel("Stockt to Watch"),cc.xy(1, 1));
		sellBuilder.add(combowatch,cc.xyw(3,1,3));
		sellBuilder.add(comboname, cc.xyw(6,1,15));
		sellBuilder.add(new JTextLabel("Price"),cc.xy(1, 3));
		sellBuilder.add(fieldPrice, cc.xy(3,3));
		sellBuilder.add(new JTextLabel("IDR"),cc.xy(5,3));
		sellBuilder.add(new JTextLabel("Qty"),cc.xyw(8, 3,4));
		sellBuilder.add(fieldQty,cc.xyw(12, 3,5));
		sellBuilder.add(new JTextLabel("LOT"), cc.xyw(18, 3,4));*/
		entPanel = new JPanel();
		entPanel.setBackground(Color.red.darker());
		
		FormLayout entLayout = new FormLayout(
				"pref,2dlu,50px,3dlu,pref,2dlu,50px, 2dlu, pref, 2dlu, 20px , 2dlu, pref, 2dlu,50px, pref, 2dlu,50px, pref, 2dlu,60px",
				"pref,2dlu,pref");
		PanelBuilder entBuilder = new PanelBuilder(entLayout,entPanel);
		entBuilder.setBorder(new EmptyBorder(10,10,10,10));
		entBuilder.add(stockTo,cc.xy(1, 1));
		entBuilder.add(combowatch,cc.xyw(3,1,3));
		entBuilder.add(comboname, cc.xyw(6,1,15));
		entBuilder.add(new JTextLabel("Price"),cc.xy(1, 3));
		entBuilder.add(fieldPrice, cc.xy(3,3));
		entBuilder.add(new JTextLabel("IDR"),cc.xy(5, 3));
		entBuilder.add(new JTextLabel("Qty"),cc.xyw(8, 3,4));
		entBuilder.add(fieldQty,cc.xyw(12, 3,5));
		entBuilder.add(new JTextLabel("LOT"), cc.xyw(18, 3,4));
		buyPanel.add(entPanel);
		tabSellBuy = new JTabbedPane();
		tabSellBuy.add("   Buy   ",buyPanel);
		tabSellBuy.add("   Sell   ",sellPanel);
		tabSellBuy.addChangeListener( change);
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveSetting();
				comboname.getTextEditor().setText(null);
				combowatch.getTextEditor().setText(null);
				fieldPrice.setValue(null);
				fieldQty.setValue(null);
				byTrend.clean();
				byDone.clean();
				byPrice.clean();
				cleanPivot();
				clientId = null;
				form.setVisible(false);
				clientId = null;
			}
		});
		tabCondition = new JTabbedPane();
		tabCondition.addTab("By Price", null, byPrice, "Condition By Price allows you to \n Set Action base on the Price Entered \n as atrigger");
		tabCondition.addTab("By Trend",null,byTrend,"Condition By Trend allows you to Set Action base on Market Trend");
		tabCondition.addTab("By Done",null,byDone,"Condition By Done allows you to Set Action on Done Orders");
		tabCondition.setSelectedIndex(0);

		tabCondition.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				if(tabCondition.getSelectedIndex()== 2){
					radioGTC.setVisible(false);
					if(radioGTC.isSelected()){
						radioDay.setSelected(true);
					}
				}else 
				{ radioGTC.setVisible(true);}
			}
		});
		btnPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		btnPanel.add(btnSave);
		btnPanel.add(btnCancel); btnPanel.setBackground(Color.red.darker());
		
		
		valPanel = new JPanel(new BorderLayout());
		valPanel.add(stockPanel,BorderLayout.SOUTH);
		valPanel.add(tabSellBuy, BorderLayout.CENTER);
		valPanel.setBackground(Color.red.darker());
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2,2,2,2));
		pnlContent.setOpaque(true);
		pnlContent.setBackground(Color.red.darker());
		pnlContent.add(btnPanel,BorderLayout.SOUTH);
		pnlContent.add(tabCondition, BorderLayout.CENTER);
		pnlContent.add(valPanel,BorderLayout.NORTH);		
	}
	
	ChangeListener change = new ChangeListener() {
		
		@Override
		public void stateChanged(ChangeEvent e) {
			if(tabSellBuy.getSelectedIndex() == 0){
				stockTo.setText("Stock to Buy");
				buyPanel.add(entPanel);
				entPanel.setBackground(Color.red.darker());
				ByDonePanel.panel.setBackground(Color.red.darker());
				ByDonePanel.quantityPanel.setBackground(Color.red.darker());				
				ByTrendPanel.trendPanel.setBackground(Color.red.darker());
				ByPricePanel.pricePanel.setBackground(Color.red.darker());
				ByTrendPanel.radioDownTrend.setSelected(true);
				ByTrendPanel.AcDown(true);
				ByTrendPanel.AcUp(false);
				ByPricePanel.actStopLost(true);
				ByPricePanel.actProfit(false);
				ByPricePanel.checkStopLost.setSelected(true);
				ByPricePanel.checkProfitTaking.setText("Compare Last Price >=");
				ByPricePanel.checkStopLost.setText("Compare Last Price <=");
//				ByPricePanel.lblprofit.setText("Price ");
//				ByPricePanel.lblstop.setText("Price ");
				stockPanel.setBackground(Color.red.darker());	
				pnlContent.setBackground(Color.red.darker());
				tabSellBuy.setBackground(Color.red.darker());
				validPanel.setBackground(Color.red.darker());
				btnPanel.setBackground(Color.red.darker());
				pnlRS.setBackground(Color.red.darker());
				valPanel.setBackground(Color.red.darker());
			}else if(tabSellBuy.getSelectedIndex() == 1){ 
				stockTo.setText("Stock to Sell");
				sellPanel.add(entPanel);
				entPanel.setBackground(Color.green.darker());
				ByDonePanel.panel.setBackground(Color.green.darker());
				ByDonePanel.quantityPanel.setBackground(Color.green.darker());				
				ByTrendPanel.trendPanel.setBackground(Color.green.darker());
				ByPricePanel.pricePanel.setBackground(Color.green.darker());				
				ByTrendPanel.radioUpTrend.setSelected(true);
				ByTrendPanel.AcDown(false);
				ByTrendPanel.AcUp(true);
				ByPricePanel.actStopLost(false);
				ByPricePanel.actProfit(true);
				ByPricePanel.checkProfitTaking.setSelected(true);
				ByPricePanel.checkProfitTaking.setText("PROFIT TAKING");
				ByPricePanel.checkStopLost.setText("STOP LOST");
//				ByPricePanel.lblprofit.setText("Last Price >=");
//				ByPricePanel.lblstop.setText("Last Price <=");
				stockPanel.setBackground(Color.green.darker());	
				pnlContent.setBackground(Color.green.darker());
				tabSellBuy.setBackground(Color.green.darker());
				validPanel.setBackground(Color.green.darker());
				btnPanel.setBackground(Color.green.darker());
				valPanel.setBackground(Color.green.darker());
				pnlRS.setBackground(Color.green.darker());
			}
			
		}
	};
	
	
	void sendSchedule(){
		if(isValidEntry()){
			int nconfirm = Utils.C_YES_DLG;
			nconfirm = Utils.showConfirmDialog(form, "Entry Schedule", 
					"are you sure to send this schedule?\n"+genConfirm(),Utils.C_YES_DLG);
			if(nconfirm == Utils.C_YES_DLG){
			Schedule schedule = createSchedule();//createSchedule();
			((IEQTradeApp) apps).getTradingEngine().createSchedule(schedule);//System.out.println("alasan...");
			comboname.getTextEditor().setText(null);
			combowatch.getTextEditor().setText(null);
			fieldPrice.setValue(null);
			fieldQty.setValue(null);
			byTrend.clean();
			byDone.clean();
			byPrice.clean();
			cleanPivot();
			clientId = null;
			hide();
			}
			}	
	}
	
	private Schedule createSchedule(){
		Account acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT)
		.getDataByKey(new String[] {getAccountId(clientId)});
		/*Schedule sc = (Schedule) (((IEQTradeApp)apps).getTradingEngine().getStore(TradingStore.DATA_SCHEDULELIST))
		.getDataByKey(new String[] {getScheduleId(alias) });
		*/Calendar cal = Calendar.getInstance();
		//System.out.println("tanggal "+cal.getTime());
		double number = new Double(0);
		String huruf = "";
		Stock sec = (Stock) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK)
				.getDataByKey(new String[] { combowatch.getText() });
		
		double compareLastPrice = 0;
		String description = null;
		String conditionMethod ="";
		Schedule schedule = new Schedule();
		schedule.setcAtsid(OrderIDGenerator.genAts(acc.getAccId()) );//cliendid
		schedule.setcAtsdate(OrderIDGenerator.getTime());
		schedule.setcClientid(acc.getAccId());
		schedule.setcSecid(combowatch.getText());
		schedule.setcVolume(new Double(sec.getLotSize().doubleValue()
				* ((Double)fieldQty.getValue()).doubleValue()));
		description = "IF stock = "+combowatch.getText()+ " and ";
		schedule.setcBuysell(""+tabSellBuy.getSelectedIndex());
		schedule.setcPirce( ((Double)fieldPrice.getValue()).doubleValue() );
		schedule.setcLot( ((Double)fieldQty.getValue()).doubleValue() );
		
		schedule.setcOrderstatus("");
		schedule.setcConditiontype(orderBy());//System.out.println("******"+orderBy());
		
		if(orderBy().equals("P")){
			if(byPrice.checkStopLost.isSelected())
			{//System.out.println("***1***"+orderBy());
			description = description + "Last Price <= "+byPrice.fieldStopPrice.getValue();
				compareLastPrice = ((Double)byPrice.fieldStopPrice.getValue()).doubleValue();
				if(byPrice.radioRemaining.isSelected())
					{
					schedule.setcRemainbidlot(((Double)byPrice.fieldRemainingLot.getValue()).doubleValue());
					description = description + " Remaining lot "+byPrice.fieldRemainingLot.getValue();
					schedule.setcCumulativvedone(number);}
				if(byPrice.radioCumulative.isSelected())
					{
					schedule.setcCumulativvedone(((Double)byPrice.fieldCumulative.getValue()).doubleValue()  );
					description = description + " Cumulative >="+byPrice.fieldCumulative.getValue();
					schedule.setcRemainbidlot(number);}
					conditionMethod ="SL";
			}
			else{	
			compareLastPrice = ((Double) byPrice.fieldProfitPrice.getValue()).doubleValue();
			description = description + " Last Price >= "+byPrice.fieldProfitPrice.getValue();
			conditionMethod ="PT";//none			
			schedule.setcCumulativvedone(number);
			schedule.setcRemainbidlot(number);
			}
			if(byPrice.checkSchedule.isSelected()){
			schedule.setcLabel(byPrice.fieldSchedule.getText());}
			else {schedule.setcLabel("");}
			//none
			schedule.setcReversallimit(number);
			schedule.setcBdRefcode(huruf);
			schedule.setcBdRefopt(huruf);
			schedule.setcBdDonetype(huruf);
			schedule.setcBdDonespec(number);
			
		}else if (orderBy().equals("T")) {//System.out.println("***2***"+orderBy());
			if(byTrend.radioDownTrend.isSelected())
					{
					description = description +"Last Price <= "+byTrend.fieldDownCompare.getValue()+"" +
							"Reversal From Bottom "+byTrend.fieldReverseButton.getValue();
					compareLastPrice = (Double)byTrend.fieldDownCompare.getValue();
					schedule.setcReversallimit( ((Double) byTrend.fieldReverseButton.getValue()).doubleValue() );
					conditionMethod ="DN";}					
				else{
					description = description +"Last Price >= "+byTrend.fieldUpCompare.getValue()+"" +
					"Reversal From Up "+byTrend.fieldReverseTop.getValue();
					compareLastPrice = (Double)byTrend.fieldUpCompare.getValue();
					schedule.setcReversallimit( ((Double) byTrend.fieldReverseTop.getValue()).doubleValue() );			
					conditionMethod ="UP";
				}
			if(byTrend.checkSchedule.isSelected()){
				schedule.setcLabel(byTrend.fieldSchedule.getText());
			}else {schedule.setcLabel("");}
			//none 
			schedule.setcCumulativvedone(number);
			schedule.setcRemainbidlot(number);
			schedule.setcBdRefcode(huruf);
			schedule.setcBdRefopt(huruf);
			schedule.setcBdDonetype(huruf);
			schedule.setcBdDonespec(number);
			
		}else if (orderBy().equals("D")) {//System.out.println("***3***"+orderBy());
			//schedule.setcBdFilter(byDone.bdFilter());
			if(byDone.bdFilter().equals("OrderID"))
				{description = description +" by Order ID "+byDone.fieldOrder.getText();
				schedule.setcBdRefcode(byDone.fieldOrder.getText());
				conditionMethod = "OID";
				schedule.setcBdRefopt(huruf);
				}			
			else if(byDone.bdFilter().equals("StockID")){
				description = description +" by Order Stock "+byDone.fieldStock.getText();
				schedule.setcBdRefcode(byDone.fieldStock.getText());
				schedule.setcBdRefopt(byDone.bdRefOpt());
				conditionMethod = "SID";}
			else if(byDone.bdFilter().equals("Schedule Label")){
				description = description +" by Order Label "+byDone.fieldStock.getText();
				schedule.setcBdRefcode(byDone.fieldSchedule.getText());
				conditionMethod = "LBL";
				schedule.setcBdRefopt(huruf);
			}
			schedule.setcBdDonetype(byDone.bdType());
			if (byDone.bdType().equals("SP")) {
				schedule.setcBdDonespec( ((Double)byDone.fieldSpecific.getValue()).doubleValue());
				description = description +" Specific "+((Double)byDone.fieldSpecific.getValue()).doubleValue()+ " Lot";
			}else{
				schedule.setcBdDonespec(number);}
			//none
			schedule.setcComparelastprice(number);
			schedule.setcReversallimit(number);
			schedule.setcLabel(huruf);
			schedule.setcCumulativvedone(number);
			schedule.setcRemainbidlot(number);
		}
				
		schedule.setcValidity(validasi());
		//System.out.println("get validity "+schedule.getcValidity());
		schedule.setcValiduntil(OrderIDGenerator.getTime());
		schedule.setcComparelastprice(compareLastPrice);
		schedule.setcConditionmethod(conditionMethod);	
		schedule.setComplianceId(acc.getComlianceId());
		
		InetAddress ipHost = null;
		try {
			ipHost = InetAddress.getLocalHost();
		} catch (Exception e) {
			//System.out.println(e);
		}
		schedule.setcDescription(description);
		//schedule.setcEntrybuy(cEntrybuy);
		schedule.setcEntrytime( OrderIDGenerator.getTime());
		schedule.setcEntryterminal(ipHost.toString());
		schedule.setcInvtype(acc.getInvType());
	//	System.out.println(description+"---");
		
		return schedule;
		
	}
	
	Date c = new Date();
	private String validasi(){
		if (radioDay.isSelected()) {
			return "D";
		} else if (radioGTC.isSelected()) {
			return "GTC";
		}else if(radioSession.isSelected()){
			return "S";
		}	
		return null;
	}
	
	String getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { alias },
						new int[] { Account.C_TRADINGID });
		return (acc != null ? acc.getAccId() : "");
	}
	
	String getScheduleId(String alias){
		Schedule sc = (Schedule)((IEQTradeApp)apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_SCHEDULELIST)
				.getDataByField(new String[] {alias},
						new int[] {Schedule.C_ATSID});
		return (sc != null?sc.getcAtsid():"");
	}
	
	private String orderBy(){
		if (tabCondition.getSelectedIndex() == 0) {
			return "P";			
		} else if (tabCondition.getSelectedIndex() == 1) {
			return "T";
		}else {
			return "D";
		}
	}

	
	void stockfocusLost(FocusEvent e){
		try{
			if(!combowatch.getText().trim().equals(""))
			{
				combowatch.getTextEditor().setText(
						combowatch.getText().toUpperCase());
				
				int i = ((IEQTradeApp)apps).getTradingEngine()
						.getStore(TradingStore.DATA_STOCK)
						.getIndexByKey(new String[]{combowatch.getText()});
				if(i>= 0){
					Stock st = (Stock) ((IEQTradeApp) apps)
								.getTradingEngine()
								.getStore(TradingStore.DATA_STOCK)
								.getDataByIndex(i);
					if(st != null){
						comboname.getTextEditor().setText(st.getName());
						calcPivot(combowatch.getText());
					}
				}else {//System.out.println("gagal ... ");
						comboname.getTextEditor().setText(null);
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								combowatch.getTextEditor().requestFocus();
							}
						});
					}
			}
		}catch (Exception e2) {
			// TODO: handle exception
		}
	}
	
	private void registerEventCombo(JDropDown comp) {
		comp.getEditor().getEditorComponent()
				.setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(
				JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap()
				.put("tabAction", new AbstractAction("tabAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(combowatch.getTextEditor())) {
							combowatch.setSelectedIndex(combowatch
									.getSelectedIndex());
						comboname.setSelectedIndex(combowatch.getSelectedIndex());
						calcPivot(combowatch.getText());
							combowatch.hidePopup();
							comboname.requestFocus();
						} else if (e.getSource().equals(
								comboname.getTextEditor())) {
							comboname.setSelectedIndex(comboname
									.getSelectedIndex());
							combowatch.setSelectedIndex(comboname
									.getSelectedIndex());
							calcPivot(combowatch.getText());
							comboname.hidePopup();
							fieldPrice.requestFocus();
						}
					}

				});
	}
	
	
	void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "clientAction");
		comp.getActionMap().put("clientAction",
				new AbstractAction("clientAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						combowatch.getTextEditor().requestFocus();//comboStock.getTextEditor().requestFocus();
					}
				});
		comp.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						form.setVisible(false);
					}
				});
		comp.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton) {
							((JButton) evt.getSource()).doClick();
						} else if (evt.getSource().equals(
								combowatch.getTextEditor())) {
							combowatch.setSelectedIndex(combowatch
									.getSelectedIndex());
							combowatch.hidePopup();
						} else if (evt.getSource().equals(
								comboname.getTextEditor())) {
							comboname.setSelectedIndex(comboname
									.getSelectedIndex());
							comboname.hidePopup();
						} else {
							if (evt.getSource() instanceof JTextField) {
								try {
									((JNumber) evt.getSource()).commitEdit();
								} catch (Exception ex) {	}								
							}
						}
					}
				});
	}

	
	
	void calcPivot(String stock){
		double r3 = 0,r2 = 0,r1 = 0, s1 = 0,s2 = 0,s3 = 0, pivot;
		String board = stock.endsWith("-R")?"TN":"RG";
		StockSummary summ = (StockSummary)((IEQTradeApp) apps)
			.getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY)
			.getDataByKey(new Object[] {stock, board});
		double hight = summ.getHighest().doubleValue();
		double low = summ.getLowest().doubleValue();
		double close = summ.getClosing().doubleValue();
		//System.out.println("hight="+hight+" low="+low+" close="+close );
		if(summ != null){
			pivot = (hight+low+close)/3;
			r1 = 2*pivot-low;
			s1 = 2*pivot-hight;
			r2 = pivot+(r1-s1);
			s2 = pivot-(r1-s1);
			s3 = low-2*(hight-pivot);
			r3 = hight+2*(pivot-low);			
		}
		fieldR1.setText(formatter.format(r1));
		fieldR2.setText(formatter.format(r2));
		fieldR3.setText(formatter.format(r3));
		fieldS1.setText(formatter.format(s1));
		fieldS2.setText(formatter.format(s2));
		fieldS3.setText(formatter.format(s3));	
	}
	
	private String genConfirm() {
		Double lot = (Double)fieldQty.getValue();
		Double price = (Double)fieldPrice.getValue();
		String result = "";
		String sellbuy ;
		if(tabSellBuy.getSelectedIndex()==0)
		sellbuy="BUY";
		else{sellbuy="SELL";}
		result = result.concat(sellbuy + " Order\n");
		result = result.concat("Client        :  " + clientId)
				.concat("\n");
		result = result.concat("Stock        :  "
				+ combowatch.getText().concat("\n"));
		result = result.concat(
				"Price         :  "
						+ formatter.format(price))
				.concat("\n");
		result = result.concat(
				"Lot            :  "
						+ formatter.format(lot))
				.concat("lot\n");
		return result;
	}
	//ats limit price 10022016
	public boolean  validPrice (double profitPrice){
		if (getStock() != null || getStock() != "") {
			String stock = getStock();
			StockSummary summ = (StockSummary)((IEQTradeApp) apps)
					.getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY)
					.getDataByKey(new Object[] {stock,
							stock.endsWith("-R")?"TN":"RG"});
//			double profitPrice = price ;
			double validPriceMax =0 ,validPriceMin=0;
			if ( summ.getClosing() < 200 && summ.getClosing() > 50) {
				validPriceMax = summ.getClosing() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUEHIGHMIN))/100);
				validPriceMin = summ.getClosing() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUELOWMIN))/100);
			} else if (summ.getClosing() > 200 && summ.getClosing() < 500) {
				validPriceMax = summ.getClosing() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUEHIGHMID))/100);
				validPriceMin = summ.getClosing() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUELOWMID))/100);
			} else if (summ.getClosing() > 500){
				validPriceMax = summ.getClosing() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUEHIGHMAX))/100);
				validPriceMin = summ.getClosing() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUELOWMAX))/100);
			} // jika last price tidak ada ambil dari close
			  else if (summ.getPrevious() < 200 && summ.getPrevious() > 50) {
				validPriceMax = summ.getPrevious() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUEHIGHMIN))/100);
				validPriceMin = summ.getPrevious() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUELOWMIN))/100);
			} else if (summ.getPrevious() > 200 && summ.getPrevious() < 500) {
				validPriceMax = summ.getPrevious() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUEHIGHMID))/100);
				validPriceMin = summ.getPrevious() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUELOWMID))/100);
			} else if (summ.getPrevious() > 500) {
				validPriceMax = summ.getPrevious() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUEHIGHMAX))/100);
				validPriceMin = summ.getPrevious() * (Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUELOWMAX))/100);
			}
			System.out.println(Double.parseDouble(TradingSetting.getats(TradingSetting.C_VALUEHIGHMID))+" dds ");
			if((profitPrice > summ.getClosing()+validPriceMax || profitPrice < summ.getClosing()-validPriceMin) && summ.getClosing() > 0){
				Utils.showMessage("You Must Entry Price Between \n" +
						"Last Price  : "+summ.getClosing()+"\n" +
						"Max Price  : "+(summ.getClosing()+validPriceMax)+"\n" +
						"Min Price   : "+(summ.getClosing()-validPriceMin), form);
				return false;
			} else if((profitPrice > summ.getPrevious()+validPriceMax || profitPrice < summ.getPrevious()-validPriceMin) && summ.getPrevious() > 0){
				Utils.showMessage("You Must Entry Price Between \n" +
						"Close Price  : "+summ.getPrevious()+"\n" +
						"Max Price  : "+(summ.getPrevious()+validPriceMax)+"\n" +
						"Min Price   : "+(summ.getPrevious()-validPriceMin), form);
				return false;
			} else {
				return true;
			}
		} else {
			Utils.showMessage("You Must Entry Stock",form);
			return false;
		}
		
		
	}
	boolean isValidEntry(){
		boolean valid = true;
		
		if(fieldPrice.getText().isEmpty() ){
			Utils.showMessage("please enter price ", form);
			valid = false;
		}
		if(fieldQty.getText().isEmpty()){
			Utils.showMessage("please enter Quantity", form);
			valid = false;
		}
		if(tabCondition.getSelectedIndex() == 0){
			if(ByPricePanel.check() != null){
				Utils.showMessage(ByPricePanel.check(), form);				
				valid = false ;}
		}else if(tabCondition.getSelectedIndex() == 1){
			if(ByTrendPanel.check() != null)
				{Utils.showMessage(ByTrendPanel.check(), form);				
				valid = false ;}
		}else if(tabCondition.getSelectedIndex() == 2){
			if(ByDonePanel.check()!=null)
			{Utils.showMessage(ByDonePanel.check(), form);				
			valid = false ;}
		}
		int i = ((IEQTradeApp) apps).getTradingEngine()
		.getStore(TradingStore.DATA_STOCK)
		.getIndexByKey(new String[] { combowatch.getText() });
		if (i < 0) {
			Utils.showMessage("please enter valid stock", form);
			combowatch.getTextEditor().requestFocus();
			valid= false;
		}
		if (tabCondition.getSelectedIndex() == 0) {
			Account acc = (Account) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ACCOUNT)
					.getDataByKey(
							new String[] { getAccountId(clientId) });
			i = ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ACCSTOCK)
					.getIndexByKey(
							new String[] { acc.getAccType(),
									combowatch.getText() });
			if (i < 0) {
				Utils.showMessage("invalid stock for this account type", form);
				combowatch.getTextEditor().requestFocus();//comboStock.getTextEditor().requestFocus();
				valid = false;
			}
		}
		return valid;
		
	}
	public static void jnumberCheck(JNumber fNumber[]){//System.out.println("test "+fNumber.length);
		for(int i=0; fNumber.length > i;i++){
			//System.out.println(i+"--"+fNumber[i].getValue());
		}
	}
	public static void jfieldCheck(JTextField[] jField){
		for(int i=0; jField.length > i;i++){
			//System.out.println(i+"--"+jField[i].getText());
		}
	}
	@Override
	public void focus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
		.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", PortfolioDef.getTableDetailDef());
			hSetting.put("tableOrder", OrderDef.getTableDef());
		}
		try {
			if (hSetting.get("size") == null)
							hSetting.put("size", new Rectangle(20, 20, 414, 450));
		} catch (Exception e) {
			hSetting.put("size", new Rectangle(20, 20, 414, 450));
		}
			
	}

	@Override
	public void refresh() {
		
	}
	public void show(Object param){
		HashMap p = (HashMap) param;
		entryType = (String) p.get("TYPE");
		clientId = (String) p.get("ClientId");
		ClientId = clientId;
		//System.out.println("client "+clientId+" entry "+entryType+" " + ClientId);
		tabCondition.setSelectedIndex(0);
		tabSellBuy.setSelectedIndex(0);
		show();
	}
	// ats limit price 100202016
	public String getStock(){
		return combowatch.getText();
	}
	
	public void show(){
		if(clientId == null)
			{return;}
			super.show();
	}
	
	protected void initUI(){
		super.initUI();
		form.setResizable(true);
		//System.out.println("form size"+form.getSize());
		//form.getBtnClose().setVisible(false);form.pack();
		
	}
	
	@Override
	public void saveSetting() {
		clientId = null;
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
		
	}

}
