package eqtrade.trading.ATS;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.CustomMouseAdapter;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.engine.FeedStore;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.ITradingEngine;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Schedule;
import eqtrade.trading.model.ScheduleDef;
import eqtrade.trading.ui.FilterStockAccount;

public class UIScheduleList extends UI {
	private JGrid table;
	private JDropDown comboID;
	private JDropDown comboNama;
	private JButton btnReload;
	private JButton btnNewSchedule;
//	private JButton btnEditSchedule;
//	private JButton btnDeleteSchedule;
	private FilterStockAccount filterByStock;

	private ITradingEngine engine;
	public UIScheduleList(String app) {
		super("Auto Trading System", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_SL);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem Withdraw = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				//System.out.println(i);
					Schedule sc = (Schedule)((IEQTradeApp) apps)
									.getTradingEngine()
									.getStore(TradingStore.DATA_SCHEDULELIST)
									.getDataByIndex(i);
					//Schedule sc = (Schedule)table.getDataByIndex(table.getSelectedRow());
					//System.out.println("ats order id "+sc.getcAtsid()+" ATS status");	
				if(sc.getcAtsstatus().equalsIgnoreCase("O")){
					((IEQTradeApp) apps).getTradingEngine().withdrawAts(sc);
				}else{
					Utils.showMessage("ats id "+sc.getcAtsid()+" Can not be withdraw", form);
				}
			}
		});
		Withdraw .setText("Withdraw");
		popupMenu.add(Withdraw);
	}

	@Override
	protected void build() {
		createPopup();
		filterByStock = new FilterStockAccount(pnlContent, ((IEQTradeApp) apps)
				.getTradingEngine().getStore(TradingStore.DATA_SCHEDULELIST));

		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_SCHEDULELIST), null,
				(Hashtable) hSetting.get("table"));

		
		comboID = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboNama = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT),Account.C_NAME, true);

		comboID.setBorder(new EmptyBorder(0, 0, 0, 0));
		comboNama.getTextEditor().addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e){
				comboNama.showPopup();
			}
			@Override
			public void focusLost(FocusEvent e){
				/*view();*/	
				if(comboID.getText().equals(""))
				btnNewSchedule.setEnabled(false);
				else if ( comboNama.getSelectedItem() != null ){
					comboID.setSelectedIndex(comboNama
							.getSelectedIndex());
					comboNama.setSelectedIndex(comboNama.getSelectedIndex());
					btnNewSchedule.setEnabled(true);
					btnNewSchedule.requestFocus();
					}
				else btnNewSchedule.setEnabled(false);
			}
		});
		comboID.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				comboID.showPopup();
			}

			@Override
			public void focusLost(FocusEvent e) {
				/*view();*/
				
				if(comboID.getText().equals("") )
				btnNewSchedule.setEnabled(false);
				else if (comboID.getSelectedItem() != null ){
					comboID.setSelectedIndex(comboID
							.getSelectedIndex());
					comboNama.setSelectedIndex(comboID.getSelectedIndex());
					btnNewSchedule.setEnabled(true);
					btnNewSchedule.requestFocus();
					}
				else btnNewSchedule.setEnabled(false);
			}

		});
		registerEventCombo(comboID);
		registerEventCombo(comboNama);
		/*table.getTable().add(popupMenu);*/
		table.getTable().addMouseListener(new MyMouseAdapter());
		table.addMouseListener(new MyCustomMouseAdapter());
		table.setColumnHide(ScheduleDef.columhide);
		//comboNama = new JDropDown();
		btnReload = new JButton("Refresh");
		btnNewSchedule = new JButton("New Schedule");
		btnNewSchedule.setEnabled(false);
//		btnEditSchedule = new JButton("Edit Schedule");
//		btnDeleteSchedule = new JButton("Delete Schedule");
		
		JPanel pnlTop = new JPanel();
		
		
		FormLayout l = new FormLayout(
				"2dlu,50px,2dlu,80px,2dlu,200px,2dlu,120px,2dlu,120px,2dlu,100px,2dlu,120px", "2dlu,pref,2dlu,pref,2dlu");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlTop);
		b.add(new JLabel("Client ID"), c.xy(2, 2));
		b.add(comboID, c.xy(4, 2));
		b.add(comboNama, c.xy(6, 2));
		b.add(btnReload, c.xy(10, 2));
		b.add(btnNewSchedule, c.xy(8, 2));
//		b.add(btnEditSchedule, c.xy(12, 2));
//		b.add(btnDeleteSchedule, c.xy(14, 2));
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlTop, BorderLayout.NORTH);
		pnlContent.add(table, BorderLayout.CENTER);
		/*((FilterColumn) filterByStock.getFilteredData("stock")).setField("");
		filterByStock.fireFilterChanged();*/
		
		btnNewSchedule.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(isReady()){
					if(!comboID.getTextEditor().equals(""))
					{
						Account acc = (Account)((IEQTradeApp)apps).getTradingEngine()
										.getStore(TradingStore.DATA_ACCOUNT)
										.getDataByKey(new String[] {comboID.getTextEditor().getText()});
						System.out.println(acc.getAts());
						if(acc.getAts().equals("1")){
							HashMap param = new HashMap();
							param.put("ClientId", comboID.getText());
							param.put("TYPE", "BUY");
							apps.getUI().showUI(TradingUI.UI_NEWSCHEDULE,param);
						}else{
							Utils.showMessage("To Activate this features, please contact our customer services \n" +
									"by email : cs@sinarmassekuritas.co.id or \n" +
									"by phone : 021 - 5050 7000", form);
						/*	Utils.showMessage(comboID.getTextEditor().getText()+
									" did not allow to create schedule ", form);*/
						}
					}
				}
			}
		});
		btnReload.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {				
				((IEQTradeApp) apps)
				.getTradingEngine().refreshAts("%");
			}
		});
		

		refresh();
	}


	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		//if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", ScheduleDef.getTableDef());
		//}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(30, 30, 820, 300));
	}

	private void registerEventCombo(JDropDown comp) {
		comp.getEditor().getEditorComponent()
				.setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(
				JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap()
				.put("tabAction", new AbstractAction("tabAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(comboID.getTextEditor())) {
							comboID.setSelectedIndex(comboID
									.getSelectedIndex());
							comboNama.setSelectedIndex(comboID.getSelectedIndex());
							btnNewSchedule.setEnabled(true);
							btnNewSchedule.requestFocus();
						} else if (e.getSource().equals(
								comboNama.getTextEditor())) {
							comboID.setSelectedIndex(comboNama
									.getSelectedIndex());
							comboNama.setSelectedIndex(comboNama
									.getSelectedIndex());
							btnNewSchedule.requestFocus();
							
						}
					}

				});
	}
	
	public class MyCustomMouseAdapter extends CustomMouseAdapter {
		public void mousePressed(MouseEvent e) {
			if(e.getButton() == MouseEvent.BUTTON3){
				System.out.println("hahahah");
				popupMenu.show(e.getComponent(), e.getX(), e.getY());
			}
		}

		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}
	}

	public class MyMouseAdapter extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			if(e.getButton() == MouseEvent.BUTTON3){
				System.out.println("hahahah");
				popupMenu.show(e.getComponent(), e.getX(), e.getY());
			}
		}

	
	};
	
	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
	}
	public void show() {
		System.out.println("444");
		if (!apps.getAction().get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).isGranted())
			{return;}
		super.show();
	}
}
