
package eqtrade.trading.ATS;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.StockSummary;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.BrowseOrder;
import eqtrade.trading.model.BrowseOrderDef;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.ScheduleDef;
import eqtrade.trading.ui.FilterStockAccount;

public class UIByStockId extends UI{
	
	private JGrid table;
	
	private JTextField txtStockId;
	private JRadioButton rbBuy;
	private JRadioButton rbSell; 
	private JButton btnOk;
	private JButton btnRefresh;
	private FilterBrowseOrder filterBrowseOrder;
	
	
	public UIByStockId( String app) {
		super("By Stock ID", app);
		type=C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_BYSTOCKID);
	}

	@Override
	protected void build() {
		createPopup();
		filterBrowseOrder = new FilterBrowseOrder(pnlContent, ((IEQTradeApp) apps)
				.getTradingEngine().getStore(TradingStore.DATA_BROWSEORDER));
		Vector temp = new Vector(3);
		temp.addElement("o");
		temp.addElement("O");
		temp.addElement("P");
		((FilterColumn) filterBrowseOrder.getFilteredData("status")).setField(temp);
		filterBrowseOrder.fireFilterChanged();
		
		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_BROWSEORDER), filterBrowseOrder,
				(Hashtable) hSetting.get("table"));
		table.setColumnHide(BrowseOrderDef.columhide);
		btnOk = new JButton("OK");
		btnRefresh = new JButton("Refresh");
		/*table.setColumnHide(new int[] { Order.C_ACCID,Order.C_ACCIDCONTRA,Order.C_BRDID,Order.C_NOTE 
				,Order.C_MATCHTIME,Order.C_DONELOT,Order.C_BALANCELOT,Order.C_AMENDSENDTIME
				,Order.C_WITHDRAWTIME,Order.C_ORDERTYPE,Order.C_MARKETORDERID,Order.C_AMENDTIME,Order.C_ORDERIDCONTRA
				,Order.C_ENTRYRTTIME,Order.C_ORDTYPE});*/
		txtStockId = new JTextField();
		
		rbBuy= new JRadioButton("Buy");
		rbSell = new JRadioButton("Sell");
		
		table.getTable().addMouseListener(new MyMouseAdapter());
		table.addMouseListener(new MyCustomMouseAdapter());
		
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					int i = table.getMappedRow(table.getSelectedRow());//System.out.println("mouse"+i);
					if (i > -1) {
						BrowseOrder bo = (BrowseOrder) ((IEQTradeApp) apps)
								.getTradingEngine()
								.getStore(TradingStore.DATA_BROWSEORDER)
								.getDataByIndex(i);
						txtStockId.setText(bo.getSecid());
						if(bo.getBuyorsell().equals("1"))rbBuy.setSelected(true);
						else rbSell.setSelected(true);
					}
				}
			}
		});
		btnOk.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
					ByDonePanel.fieldStock.setText(txtStockId.getText());
					if(rbBuy.isSelected()) ByDonePanel.radioBuy.setSelected(true);
					else ByDonePanel.radioSell.setSelected(true);
					form.setVisible(false);
					}
				});
			}
		});
		btnRefresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				((IEQTradeApp)apps).getTradingEngine().refreshBrowseAts("%");
			}
		});
		JPanel pnlTop = new JPanel();
		FormLayout l = new FormLayout(
				"2dlu,50px,2dlu,80px,10dlu,40px,2dlu,40px,140dlu,50px,2dlu,100px","2dlu,pref,2dlu");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l,pnlTop);
		

		b.add(new JLabel("Stock ID"), c.xy(2, 2));
		b.add(txtStockId,c.xy(4, 2));
		b.add(rbBuy, c.xy(6, 2));
		b.add(rbSell, c.xy(8, 2));
		b.add(btnOk, c.xy(10, 2));
		b.add(btnRefresh, c.xy(12, 2));
		
		txtStockId.setEnabled(false);
		rbBuy.setEnabled(false);
		rbSell.setEnabled(false);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rbBuy);
		group.add(rbSell);
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2,2,2,2));
		pnlContent.add(pnlTop, BorderLayout.NORTH);
		pnlContent.add(table, BorderLayout.CENTER);
		
		refresh();//filter();
	}
	private String clientId , buysell;
	public void show(Object param){
		HashMap p = (HashMap) param;
		clientId = (String) p.get("ClientId");	
		buysell = (String) p.get("buysell");
		filter();
		show();
	}
	
	public void show(){
		if(clientId == null)
			{return;}
			super.show();
	}
	
	public void filter(){
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				((FilterColumn)filterBrowseOrder.getFilteredData("clientid")).setField(clientId);
				((FilterColumn)filterBrowseOrder.getFilteredData("buysell")).setField(buysell);
				//System.out.println("stock "+clientId);
				filterBrowseOrder.fireFilterChanged();
			}
		});
	}
	
	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
		
	}
	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}
	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
		.getLayout((String) getProperty(C_PROPERTY_NAME));
		//if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", BrowseOrderDef.getTableDef());
		//}
		//if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(30, 30,660, 300));
		
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
		
	}

	@Override
	public void saveSetting() {
		clientId = null;
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
		
	}
	

}
