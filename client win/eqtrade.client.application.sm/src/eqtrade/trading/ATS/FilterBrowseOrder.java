package eqtrade.trading.ATS;

import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.table.TableModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.trading.model.BrowseOrder;
import eqtrade.trading.model.Order;

public class FilterBrowseOrder extends FilterBase{
	private HashMap mapFilter = new HashMap();
	private GridModel dataSchedule;
	private Log log = LogFactory.getLog(getClass());

	public FilterBrowseOrder(Component parent, GridModel _data) {
		super(parent, "filter");
		mapFilter.put("order", new FilterColumn("order", String.class,
				null ,FilterColumn.C_EQUAL));
		mapFilter.put("clientid", new FilterColumn("clientid",String.class,
				null, FilterColumn.C_EQUAL));
		mapFilter.put("status", new FilterColumn("status",Vector.class,
				null, FilterColumn.C_MEMBEROF));
		mapFilter.put("buysell", new FilterColumn("buysell",String.class,
				null, FilterColumn.C_EQUAL));
		
	}

	@Override
	public boolean filter(TableModel model, int row) {
		// TODO Auto-generated method stub
		boolean avail = true;
		try {
			Column val = (Column)model.getValueAt(row, 0);
			BrowseOrder data = (BrowseOrder)val.getSource();//System.out.println(data.getClientid()+" -- "+data.getBuyorsell());
			if(((FilterColumn)mapFilter.get("clientid")).compare(data.getClientid())
					 &&((FilterColumn)mapFilter.get("order")).compare(data.getOrderid())
					 && ((FilterColumn)mapFilter.get("status")).compare(data.getAtsstatus())
					 && ((FilterColumn)mapFilter.get("buysell")).compare(data.getBuyorsell())
					){
				return avail;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Object getFilteredData(String name) {
		// TODO Auto-generated method stub
		return mapFilter.get(name);
	}

	@Override
	public void setFilteredData(Object src, String name) {

		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

}
