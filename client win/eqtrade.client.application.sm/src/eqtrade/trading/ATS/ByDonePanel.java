package eqtrade.trading.ATS;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import quickfix.field.AppStatus;
import quickfix.field.ClOrdID;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.IApp;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinForm;
import com.vollux.ui.JText;
import com.vollux.ui.JTextLabel;

import eqtrade.application.IEQTradeApp;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Order;

public class ByDonePanel extends JPanel{
	public static JPanel panel;
	public static JPanel quantityPanel;
	static JRadioButton radioOrder;
	static JRadioButton radioStock;
	static JRadioButton radioSchedule;
	JRadioButton radioAny;
	JRadioButton radioFull;
	static JRadioButton radioSpecific;
	public static JRadioButton radioBuy;
	public static JRadioButton radioSell;
	
	static JNumber fieldSpecific;
	
	public static JTextField fieldOrder;
	public static JTextField fieldStock;
	public static JTextField fieldSchedule;
	JButton btnBrowse;
	private static IApp apps;
	private JSkinForm form;
	private Hashtable hSetting;

	public ByDonePanel(IApp apps, JSkinForm form, Hashtable hSetting){
		super(new BorderLayout());
		this.apps = apps;
		this.form = form;
		this.hSetting = hSetting;
	}
	private String test;
	protected void build(){
		  test = UINewSchedule.ClientId;
		radioOrder = new JRadioButton("Order ID");			radioOrder.setOpaque(false);		
		radioStock = new JRadioButton("Stock ID");			radioStock.setOpaque(false);
		radioSell = new JRadioButton("On SELL Done");		radioSell.setOpaque(false);
		radioBuy = new JRadioButton("On Buy Done");			radioBuy.setOpaque(false);
		radioSchedule = new JRadioButton("Schedule Label");	radioSchedule.setOpaque(false);
		radioAny = new JRadioButton("Any Quantity");		radioAny.setOpaque(false);
		radioFull = new JRadioButton("Full Match");			radioFull.setOpaque(false);
		radioSpecific = new JRadioButton("Specific");		radioSpecific.setOpaque(false);
		btnBrowse = new JButton("Browse Order");
		fieldOrder = new JTextField();
		fieldStock = new JTextField();
		fieldSchedule = new JTextField();
		fieldSpecific = new JNumber(Double.class, 0,0, false,true);
		fieldSpecific.setSize(50, 25);
		fieldSpecific.setEnabled(false);
		ButtonGroup groupFilter = new ButtonGroup();
		groupFilter.add(radioOrder);
		groupFilter.add(radioStock);
		groupFilter.add(radioSchedule);
		
		ButtonGroup groupStock = new ButtonGroup();
		groupStock.add(radioSell);
		groupStock.add(radioBuy);
		radioBuy.setSelected(true);
		ButtonGroup groupQuantity = new ButtonGroup();
		groupQuantity.add(radioAny);
		groupQuantity.add(radioFull);
		groupQuantity.add(radioSpecific);
		radioAny.setSelected(true);
		quantityPanel = new JPanel();
		quantityPanel.setBackground(Color.red.darker());
		FormLayout quantityLayout = new FormLayout("100px,80px,70px,50px,80px","pref");
		PanelBuilder quantityBuilder = new PanelBuilder(quantityLayout,quantityPanel);
		CellConstraints cc = new CellConstraints();
		quantityBuilder.add(radioAny,cc.xy(1, 1));
		quantityBuilder.add(radioFull,cc.xy(2, 1));
		quantityBuilder.add(radioSpecific,cc.xy(3, 1));
		quantityBuilder.add(fieldSpecific,cc.xy(4, 1));
		quantityBuilder.add(new JTextLabel("LOT"),cc.xy(5, 1));
		
		panel = new JPanel();
		panel.setBackground(Color.red.darker());
		FormLayout layout = new FormLayout(
				"80px,20px,80px,50px,100px,50px,100px",
			"pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref");
		PanelBuilder builder = new PanelBuilder(layout, panel);
		builder.setBorder(new EmptyBorder(4,4,4,4));
		builder.addSeparator("Choose Filter By",cc.xyw(1, 1, 7));
		builder.add(radioOrder,cc.xy(1, 3));
		builder.add(fieldOrder,cc.xyw(2, 3,2));
		builder.add(radioStock, cc.xy(1,5));
		builder.add(fieldStock,cc.xyw(2, 5,2));
		builder.addSeparator("",cc.xyw(3, 5, 5));
		builder.add(radioBuy,cc.xyw(2, 7,2));
		builder.add(radioSell,cc.xyw(4, 7,2));
		builder.addSeparator("",cc.xyw(1, 9, 7));
		builder.add(radioSchedule,cc.xyw(1, 11,2));
		builder.add(fieldSchedule,cc.xy(3, 11));
		builder.addSeparator("",cc.xyw(1, 13, 4));
		builder.add(btnBrowse,cc.xyw(5, 13,2));
		builder.add(quantityPanel,cc.xyw(1, 15,7));
		
		radioOrder.setSelected(true);
		
		radioSpecific.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(radioSpecific.isSelected()){
					fieldSpecific.setEnabled(true);
				}else{
					fieldSpecific.setEnabled(false);
				}
			}
		});
		radioOrder.addActionListener(new selected());
		radioStock.addActionListener(new selected());
		radioSchedule.addActionListener(new selected());
		acOrder(false);
		btnBrowse.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				((IEQTradeApp)apps).getTradingEngine().refreshBrowseAts("%");
				HashMap param = new HashMap();
				param.put("ClientId", UINewSchedule.ClientId);
				param.put("buysell", radioBuy.isSelected()?"1":"2");
				//System.out.println(UINewSchedule.ClientId);
				if(radioOrder.isSelected()){
					apps.getUI().showUI(TradingUI.UI_BYORDERID,param);
				}
				if(radioStock.isSelected()){
					apps.getUI().showUI(TradingUI.UI_BYSTOCKID,param);
				}
				if(radioSchedule.isSelected()){
					apps.getUI().showUI(TradingUI.UI_BYSCHEDULLABEL,param);
				}
				
			}
		});
		this.add(panel);	
	}

	public void clean(){
		fieldOrder.setText(null);
		fieldSchedule.setText(null);
		fieldSpecific.setValue(null);
		fieldStock.setText(null);
	}
	
	public String bdFilter(){
		if (radioOrder.isSelected()) {
			return "OrderID";
		} else if (radioStock.isSelected()) {
			return "StockID";
		} else{
			return "Schedule Label";
		}		
	}
	public String bdRefOpt(){
		if (radioBuy.isSelected()) {
			return "OBD";
		} else if (radioSell.isSelected()) {
			return "OSD";}
		return null;
	}
	
	public String bdType(){
		if (radioAny.isSelected()) {
			return "AQ";
		} else if (radioFull.isSelected()) {
			return "FM";
		} else{
			return "SP";
		}
	}
	static String check(){
		String error = null;
		if(radioOrder.isSelected()){
			if(fieldOrder.getText().isEmpty()){
				error = "Please enter Order";
			}
		}
		if(radioStock.isSelected()){
			if(fieldStock.getText().isEmpty()){
				error = "Please enter Stock";
			}
		}
		if(radioSchedule.isSelected()){
			if(fieldSchedule.getText().isEmpty()){
				error = "Please enter schedule";
				
			}
		}
		if(radioSpecific.isSelected())
				{
					if(fieldSpecific == null)
						error ="Please enter Specific";
					else{
						Order ord = (Order)((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_ORDER)
						.getDataByKey(
								new Object[] {fieldOrder.getText()
										, "0" });
						if(ord.getLot() < (Double)fieldSpecific.getValue()){
							error = "Specific lot bigger than order lot ";
						}
					}
				}
		return error;
	}
	void acOrder(boolean ao){
		radioBuy.setEnabled(ao);
		radioSell.setEnabled(ao);
	}
	class selected implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource() == radioOrder){
				fieldOrder.setEnabled(true);
				fieldSchedule.setEnabled(false);
				fieldStock.setEnabled(false);
				fieldSchedule.setText("");
				fieldStock.setText("");
				acOrder(false);
			}else if(e.getSource() == radioStock){
				fieldOrder.setEnabled(false);
				fieldSchedule.setEnabled(false);
				fieldStock.setEnabled(true);
				fieldOrder.setText("");
				fieldSchedule.setText("");
				acOrder(true);
			}else if(e.getSource() == radioSchedule){
				fieldOrder.setEnabled(false);
				fieldSchedule.setEnabled(true);
				fieldStock.setEnabled(false);
				fieldOrder.setText("");
				fieldStock.setText("");
				acOrder(false);
			}
		}
	
	}
}