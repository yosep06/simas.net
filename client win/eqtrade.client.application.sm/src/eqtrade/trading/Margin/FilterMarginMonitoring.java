package eqtrade.trading.Margin;

import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.table.TableModel;
import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

//import eqtrade.trading.model.Account;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Margin;
import eqtrade.trading.model.Portfolio;

public class FilterMarginMonitoring extends FilterBase {
	private HashMap<String, Object> mapFilter = new HashMap<String, Object>();
	public int rowcount =0;
	private GridModel dataMargin;

	public FilterMarginMonitoring(Component parent, GridModel _dataMargin) {
		super(parent,"filter");
		
		mapFilter.put("margin", new FilterColumn("margin",
				Vector.class, null, FilterColumn.C_MEMBEROF));
		mapFilter.put("acctype", new FilterColumn("acctype",
				String.class,new String("MR"),FilterColumn.C_EQUAL));
		mapFilter.put("stock", new FilterColumn("stock",
				String.class,new String(""),FilterColumn.C_EQUAL));
		this.dataMargin = _dataMargin;
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false; 
		try{
			Column val = (Column) model.getValueAt(row, 0);
			Margin dat = (Margin) val.getSource();
			FilterColumn column = (FilterColumn) mapFilter.get("stock");
			FilterColumn column2 = (FilterColumn) mapFilter.get("margin");			
			
			if( ((FilterColumn)mapFilter.get("acctype") ).compare(dat.getAccType()) ){
				if( ((String) column.getField()).isEmpty() &&
						 column2.getField() == null ) {	
					avail =true;rowcount++;}
						
				else if(!((String) column.getField()).isEmpty() ){
					Portfolio portfolio = (Portfolio) dataMargin
					.getDataByKey(new String[] { dat.getTradingId(),
							(String) column.getField() });
					if (portfolio != null)
						{avail = true;System.out.print("portfolio--");rowcount++;}
						}
				else {
					if( ((FilterColumn)mapFilter.get("margin") ).compare(dat.getAccId())
						)
						{avail = true;System.out.print("margin--");rowcount++;}
				}
				//System.out.println("column2 "+column2.getField()+" column "+column.getField()+" row "+rowcount);
			}
			
			return avail;
		}catch(Exception e){
			e.printStackTrace();return false;
		}
		
	}
	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}
	@SuppressWarnings("unchecked")
	public void setFilter(HashMap<String, Object> map) {
		if(map!=null){
			this.mapFilter = (HashMap<String, Object>) map.clone();
			fireFilterChanged();
		}
	}

}
