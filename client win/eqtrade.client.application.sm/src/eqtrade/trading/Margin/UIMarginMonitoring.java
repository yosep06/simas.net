package eqtrade.trading.Margin;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.print.*;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Vector;
import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.engine.TradingEngine;
//import eqtrade.trading.model.Account;
//import eqtrade.trading.model.AccountDef;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Margin;
import eqtrade.trading.model.MarginDef;
import eqtrade.trading.model.MarginRender;

public class UIMarginMonitoring extends UI{
	private JGrid table;
	private JGrid tabel1;
	//private JDropDown comboNasabah;
	private JButton btnRefresh;
	private JButton btnPrint;
	private JTextField txtnama;
	private JTextField txtStock;
	private JRadioButton rbAccount;
	private JRadioButton rbStock;
	
	private FilterMarginMonitoring filter;
	private FilterStockMargin filterByStockMargin;
	private String oldKode = "";
	private String oldStockMargin ="";
	public UIMarginMonitoring(String app) {
		super("Margin Monitoring", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_MM);

	}
	
	@Override
	protected void build() {
		
		filter  = new  FilterMarginMonitoring(pnlContent, ((IEQTradeApp) apps)
				.getTradingEngine().getStore(TradingStore.DATA_MARGIN));
		filterByStockMargin = new FilterStockMargin(pnlContent, ((IEQTradeApp) apps)
				.getTradingEngine().getStore(TradingStore.DATA_ACCOUNT));
		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_MARGIN), 
				filter, (Hashtable) hSetting.get("table"));
		//table = createTable(((IEQTradeApp) apps).getTradingEngine().getStore(
		//				TradingStore.DATA_MARGIN), 
		//		filterByStockMargin, (Hashtable) hSetting.get("table"));
		
		table.setColumnHide(new int[]{ Margin.C_BUY,Margin.C_SELL,Margin.C_WITHDRAW,
				Margin.C_ADDRESS, Margin.C_BID, Margin.C_BRANCHID, 
				Margin.C_COMPLIANCEID,Margin.C_CUSTID,Margin.C_CUSTTYPE, 
				Margin.C_EMAIL,Margin.C_HANDPHONE, Margin.C_INVTYPE,
				Margin.C_ISCORPORATE,Margin.C_MYACC,Margin.C_DEPOSIT,Margin.C_STOCKVAL,
				Margin.C_OFFER,Margin.C_PHONE,Margin.C_SALESID,Margin.C_STATUS,
				Margin.C_SUBACCOUNT, Margin.C_TRADINGID	,Margin.C_ACCTYPE
		});
	
		btnRefresh = new JButton("Refresh");
		btnPrint = new JButton("Print");
		rbAccount = new JRadioButton("Account Id");
		rbStock	= new JRadioButton("Stock");
		txtStock = new JTextField();
		txtnama =  new JTextField();
		rbAccount.setSelected(true);
		//lblNama = new JLabel("Nama");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setState(false);
				refresh();
			}
		});
		table.getTable().addMouseListener(new MyMouseAdapter());
		table.addMouseListener(new MyCustomMouseAdapter());
		table.setColumnHide(MarginDef.columhide);
		JPanel pnlTop = new JPanel();
		FormLayout l = new FormLayout(
				"2dlu,75px,2dlu,125px,2dlu,50px,2dlu,125px,2dlu,80px,2dlu,80px", "2dlu,pref,2dlu");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlTop);
		//b.add(new JLabel("Nama Nasabah"), c.xy(2, 2));
		//b.add(comboNasabah, c.xy(4, 2));
		//b.add(new JLabel("Accont Id"), c.xy(2, 2));
		b.add(rbAccount, c.xy(2, 2));
		b.add(txtnama, c.xy(4,2));
		b.add(rbStock, c.xy(6, 2));
		b.add(txtStock,c.xy(8, 2));
		b.add(btnRefresh, c.xy(10, 2));
		
		b.add(btnPrint, c.xy(12,2));
		ButtonGroup group = new ButtonGroup();
		group.add(rbAccount);
		group.add(rbStock);
		//b.add(btnClear, c.xy(8, 2));
		txtStock.disable();
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlTop, BorderLayout.NORTH);
		pnlContent.add(table, BorderLayout.CENTER);
		//((FilterColumn) filterByStock.getFilteredData("stock")).setField("");
		//filterByStock.fireFilterChanged();
		txtnama.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e){	
				fieldKodeMargin(null);			
			}
			public void focusGained(FocusEvent e){
				txtnama.selectAll();	
			}
		});
		txtStock.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e){	
				fielKodeStockMargin(null);			
			}
			public void focusGained(FocusEvent e){
				txtStock.selectAll();	
			}
		});
		rbAccount.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				txtnama.enable();
				txtStock.disable();
				txtStock.setText("");
				txtStock.getColorModel();
				txtStock.setRequestFocusEnabled(true);
			}
		});
		rbStock.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				txtStock.enable();
				txtnama.disable();
				txtnama.setText("");
				txtnama.getColorModel();
				txtnama.setRequestFocusEnabled(true);
			}
		});
		txtStock.registerKeyboardAction(new  ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(txtStock.getText().trim().equals("")){
					fieldEmpty();//filter.rowcount=0;
				}else{
					fielKodeStockMargin(null);
				}
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0,true),
		JComponent.WHEN_FOCUSED);
		
		txtnama.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(txtnama.getText().trim().equals("")){
						fieldEmpty();//filter.rowcount=0;
					oldKode="";
					//filter.rowcount=0;
				}else{
						fieldKodeMargin(null);
						System.out.print("aaaaaaaa");
						
				}				
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0,true),
			JComponent.WHEN_FOCUSED);

			btnRefresh.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
				filter.rowcount=0;
				if(rbAccount.isSelected()){
					String[] lg = txtnama.getText().split(";");
					System.out.println("Text Account "+txtnama.getText().equals("")+"--- "+lg.length);
						if(txtnama.getText().trim().equals("") || lg.length > 1){System.out.print("ada");
						/*fieldEmpty();
						int  i = filter.rowcount;
						System.out.println("baris i "+i);
						for(int  a = 0;a<i;a++){
							int  j = table.getMappedRow(a);
							Margin account = (Margin) ((IEQTradeApp) apps)
									.getTradingEngine()
									.getStore(TradingStore.DATA_MARGIN)
									.getDataByIndex(j);
							((IEQTradeApp) apps).getTradingEngine().refreshMargin(
											account.getAccId());
						}*/
							((IEQTradeApp)apps).getTradingEngine().refreshAccount(
								"%Z");
						
						}
						else{System.out.println("jalan ");
						((IEQTradeApp) apps).getTradingEngine().refreshAccount(
										txtnama.getText().trim().toUpperCase());
						fieldKodeMargin(null);
					}
					oldKode="";
					}
				filter.rowcount=0;
				if(rbStock.isSelected())
					{
					if(txtStock.getText().trim().equals("") ){
					/*	fieldEmpty();
					
					int  i = filter.rowcount;
					System.out.println("baris i "+i);
					for(int  a = 0;a<i;a++){
						int  j = table.getMappedRow(a);
						Margin account = (Margin) ((IEQTradeApp) apps)
								.getTradingEngine()
								.getStore(TradingStore.DATA_MARGIN)
								.getDataByIndex(j);
						((IEQTradeApp) apps).getTradingEngine().refreshMargin(
										account.getAccId());
					}*/((IEQTradeApp)apps).getTradingEngine().refreshAccount(
					"%Z");
					}else{
						((IEQTradeApp) apps).getTradingEngine().refreshAccount(
										txtStock.getText().trim().toUpperCase());
					}
//								((IEQTradeApp)apps).getTradingEngine().refreshMargin(
//										"%");
								fielKodeStockMargin(null);
					oldStockMargin="";
						
						//String stock = txtStock.getText();
						//((FilterColumn) filterByStockMargin.getFilteredData("stock")).setField(stock);
						//filterByStockMargin.fireFilterChanged();
						//System.out.print("cek stock");
					}
				}
				
			});		
			btnPrint.addActionListener(new ActionListener() {			
				@Override
				public void actionPerformed(ActionEvent e) {					
					PrinterJob printMargin = PrinterJob.getPrinterJob();
					printMargin.setPrintable(new isiMargin());
					if(printMargin.printDialog()){	
							try {
								isi();
								printMargin.print();
							
							} catch (PrinterException e1) {	
								e1.printStackTrace();
							}								
					}
				}
			});
		refresh();
		if (hSetting.get("stock") != null) {
			txtnama.setText((String) hSetting.get("stock"));
			fieldKodeMargin(null);
		}		
	}	
	
	private  void  fieldEmpty(){
		filter.rowcount = 0;
		((FilterColumn) filter.getFilteredData("margin"))
		.setField(null);
		((FilterColumn) filter.getFilteredData("stock"))
		.setField("");
		filter.fireFilterChanged(); 		
		}
	private void fielKodeStockMargin(final String info){
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String newStockMargin = txtStock.getText().trim().toUpperCase();
				String[] arrStockMargin = Stocks(newStockMargin); 
				String fStock="";filter.rowcount = 0;
				Vector<String> vBoardStock	= new Vector<String>();
				((FilterColumn) filter.getFilteredData("margin")).setField(null);
				((FilterColumn) filter.getFilteredData("stock")).setField("");
				if((newStockMargin.length()>0 && !oldStockMargin.equals(newStockMargin))){
					for(int i=0; i<Stocks(newStockMargin).length;i++){
						txtStock.setText(newStockMargin);
						oldStockMargin= newStockMargin;System.out.println(getKode(arrStockMargin[i]));
						vBoardStock.addElement(arrStockMargin[i]);
						if(arrStockMargin.length>i)
						{
							fStock = fStock+";"+getKode(arrStockMargin[i]);
						}else{
							fStock=getKode(arrStockMargin[i]);
						}
					}//((FilterColumn) filter.getFilteredData("margin")).setField(null);
					((FilterColumn) filter.getFilteredData("stock")).setField(newStockMargin);
					filter.fireFilterChanged();
					System.out.println("row stock "+filter.rowcount);
				}
			}
		});
	}
private void fieldKodeMargin(final String info){
	SwingUtilities.invokeLater(new Runnable() {	
		@Override
		public void run() {			
			String newKode = txtnama.getText().trim().toUpperCase();
			String[] arrKode =  Stocks(newKode); String fname="";
			Vector<String> vBoard = new Vector<String>();filter.rowcount = 0;
			((FilterColumn) filter.getFilteredData("margin")).setField(null);
			((FilterColumn) filter.getFilteredData("stock")).setField("");
			if((newKode.length()>0 && !oldKode.equals(newKode))){
				for(int i=0; i<Stocks(newKode).length;i++){
					txtnama.setText(newKode);
					oldKode=newKode;System.out.println(getKode(arrKode[i]));
					vBoard.addElement(arrKode[i]);
					if(arrKode.length>i)
					{
						fname = fname+";"+getKode(arrKode[i]);
					}else{
						fname = getKode(arrKode[i]);
					}
				}	
					//((FilterColumn) filter.getFilteredData("stock")).setField(null);
					((FilterColumn) filter.getFilteredData("margin")).setField(vBoard);
					filter.fireFilterChanged();
					System.out.println("row margin "+filter.rowcount);
			}
		}

		//private String[] Stock(String newKode) {
			
		//	return null;
		//}
	});
}
/*public String[] isi(){
	int  i = filter.rowcount;
	for(int  a = 0;a<i/2;a++){
		int  j = table.getMappedRow(a);
				Account account = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByIndex(j);
	
	String[][] nama = new String[j][j];
	for(int b=0; b<nama.length;b++ ){
		//data[0]=account.getAccId();
		System.out.print(b);
			for(int c=0;c<nama[b].length;c++){
			//nama[b][c]= {{account.getAccId(),account.getName()},{account.getBid()}};
				System.out.print(c);
			
		}//System.out.print(""+nama[b]);
	}
	}
	return null;
	
}*/
public String[][] isi(){
	int  i = filter.rowcount;
    double jlmh = 0;
	System.out.print("isi row"+i);
	String[] status = new String[10];
	String[][] nama = new String[i][10];
	for(int  a = 0;a<i;a++){
		int  j = table.getMappedRow(a);
				Margin account = (Margin) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_MARGIN)
				.getDataByIndex(j);
	
		//System.out.print(b);
		
		status[0] = account.getAccId();
		status[1] = nama(account.getName())[0];
		status[2] = ""+format.format(account.getCurrentRatio()) ;
		status[3] = ""+format1.format(account.getCreditLimit());
		status[4] = ""+format1.format(account.getNetAC());
		status[5] = ""+format1.format(account.getLQValue());
		status[6] = ""+format1.format(account.getMarketVal());
		status[7] = ""+format1.format(account.getCurrTL());
		status[8] = ""+format1.format(account.getTopup());
		status[9] = ""+format1.format(account.getForceSell());
		jlmh=jlmh+account.getCreditLimit();
			for(int c=0;c<status.length;c++){
					
				nama[a][c] = status[c];
				//nama[b][c] = status[b];
				//System.out.print(nama[b][c]);
				
				
		
	}
	
	}
	PrintMargin.setPrint(nama);
	PrintMargin.setJmlhCL(format1.format(jlmh));
	return nama;
	
	
}


private DecimalFormat format = new DecimalFormat("#");
private DecimalFormat format1 = new DecimalFormat("#,###,###,###");



private String[] Stocks(String fieldStock){
	String[] names ;
		names = fieldStock.split(";");
	return names ;
}
private String[] nama(String cutcut){
	
	String[] cut;
	cut= cutcut.split("-");
	return cut;
	
}
private String getKode(String code) {
	String name = "";
	int i = ((IEQTradeApp) apps).getTradingEngine()
			.getStore(TradingStore.DATA_MARGIN)
			.getIndexByKey(new Object[] { code });
	if (i > -1) {
		name = ((Margin) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_MARGIN).getDataByIndex(i))
				.getAccId();
	}
	return name;
}
	@Override
	public void focus() {
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				txtnama.requestFocus();
			}
		});
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void loadSetting() {
		
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", MarginDef.getTableDef());
			
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(50, 100, 1000, 300));		
	}

	@Override
	public void refresh() {
		
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());	
		
		/*if(!txtnama.getText().trim().equals("")){filter.rowcount=0;
			((IEQTradeApp)apps).getTradingEngine().refreshData(
					txtnama.getText().trim().toUpperCase());	
			
		}
		else{
			((IEQTradeApp) apps).getTradingEngine().refreshAccount(
					txtnama.getText().trim().equals("")? "%":
							txtnama.getText().trim().toUpperCase());
							fieldEmpty();	
		}*/
		oldKode="";
		
		
	}
	public void refreshUI(){
		//String stock = comboStock.getTextEditor().getText();				
	}
	
	public void setState(boolean state) {
		btnRefresh.setEnabled(state);
	}
	
	public void successRefresh() {


		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				setState(true);
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void saveSetting() {
		
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		//TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
			//	hSetting);
	}

	@SuppressWarnings("rawtypes")
	public void rows(){
		filter  = new  FilterMarginMonitoring(pnlContent, ((IEQTradeApp) apps)
				.getTradingEngine().getStore(TradingStore.DATA_MARGIN));
		
		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_MARGIN), 
				filter, (Hashtable) hSetting.get("table"));
	}
	
	
}


