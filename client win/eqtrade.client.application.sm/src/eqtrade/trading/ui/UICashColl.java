package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import com.vollux.framework.UI;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.Utils;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.CashCollDef;

public class UICashColl extends UI {
	private JGrid table;
	private JSkinDlg frame;

	public UICashColl(String app) {
		super("Cash Collateral", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_CASHCOLL);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.getInsidePanel().setBorder(new EmptyBorder(0, 1, 1, 1));
		frame.setContent(pnlContent);
		frame.pack();
		// frame.setLocation(((Rectangle) hSetting.get("size")).x, ((Rectangle)
		// hSetting.get("size")).y);
		frame.setSize(((Rectangle) hSetting.get("size")).width,
				((Rectangle) hSetting.get("size")).height);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_CASHCOLL), null,
				(Hashtable) hSetting.get("table"));
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getTable().add(popupMenu);
		table.getTable().addMouseListener(new MyMouseAdapter());
		table.addMouseListener(new MyCustomMouseAdapter());
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 0, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		refresh();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", CashCollDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 550, 300));
	}

	@Override
	public void saveSetting() {
		hSetting.put("size",
				new Rectangle(frame.getX(), frame.getY(), frame.getWidth(),
						frame.getHeight()));
		hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
	}

	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}

	@Override
	public void show() {
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}

	@Override
	public void show(Object param) {
		HashMap p = (HashMap) param;
		query((String) p.get("CLIENT"));
		show();
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}

	private Thread thread;

	private void query(final String param) {
		if (thread != null) {
			thread.stop();
			thread = null;
		}
		thread = new Thread(new Runnable() {
			@Override
			public void run() {
				((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_CASHCOLL).getDataVector()
						.clear();
				((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_CASHCOLL).refresh();
				((IEQTradeApp) apps).getTradingEngine().refreshCashColl(param);
			}
		});
		thread.start();
	}
}
