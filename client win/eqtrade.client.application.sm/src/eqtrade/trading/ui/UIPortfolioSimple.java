package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.vollux.framework.UI;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedAction;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.Utils;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Portfolio;
import eqtrade.trading.model.PortfolioDef;

public class UIPortfolioSimple extends UI {
	private JGrid table;
	private JButton btnBuy;
	private JButton btnSell;

	public UIPortfolioSimple(String app) {
		super("Simple Portfolio", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_PFSIMPLE);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem mnBrokerSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getSelectedRow();
				if (table.getTable().getRowCount() == 1)
					i = 0;
				if (i >= 0) {
					Portfolio data = (Portfolio) table.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", data.getStock());
					if (((TradingApplication) apps).getFeed().getAction()
							.get(FeedAction.A_SHOWBROKERSUMM).isEnabled()) {
						((TradingApplication) apps).getFeed().getUI()
								.showUI(FeedUI.UI_BROKERSUMMARY, param);
					}
				}
			}
		});
		mnBrokerSumm.setText("Buy/Sell Broker Summary");
		popupMenu.add(mnBrokerSumm);

		JMenuItem mnBrokerActivity = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getSelectedRow();
				if (table.getTable().getRowCount() == 1)
					i = 0;
				if (i >= 0) {
					Portfolio data = (Portfolio) table.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", data.getStock());
					if (((TradingApplication) apps).getFeed().getAction()
							.get(FeedAction.A_SHOWBROKERACTIVITY).isEnabled()) {
						((TradingApplication) apps).getFeed().getUI()
								.showUI(FeedUI.UI_BROKERACTIVITY, param);
					}
				}
			}
		});
		mnBrokerActivity.setText("Today Broker Activity");
		popupMenu.add(mnBrokerActivity);

		JMenuItem mnInvStock = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getSelectedRow();
				if (table.getTable().getRowCount() == 1)
					i = 0;
				if (i >= 0) {
					Portfolio data = (Portfolio) table.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", data.getStock());
					if (((TradingApplication) apps).getFeed().getAction()
							.get(FeedAction.A_SHOWINVSTOCK).isEnabled()) {
						((TradingApplication) apps).getFeed().getUI()
								.showUI(FeedUI.UI_INVSTOCK, param);
					}
				}
			}
		});
		mnInvStock.setText("Foreign/Domestic by Stock");
		popupMenu.add(mnInvStock);

		JMenuItem mnTimeTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getSelectedRow();
				if (table.getTable().getRowCount() == 1)
					i = 0;
				if (i >= 0) {
					Portfolio data = (Portfolio) table.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", data.getStock());
					if (((TradingApplication) apps).getFeed().getAction()
							.get(FeedAction.A_SHOWTIMETRADE).isEnabled()) {
						((TradingApplication) apps).getFeed().getUI()
								.showUI(FeedUI.UI_TIMETRADE, param);
					}
				}
			}
		});
		mnTimeTrade.setText("Time & Trade Summary");
		popupMenu.add(mnTimeTrade);

		JMenuItem mnCompany = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					int i = table.getSelectedRow();
					if (table.getTable().getRowCount() == 1)
						i = 0;
					if (i >= 0) {
						Portfolio data = (Portfolio) table.getDataByIndex(i);
						HashMap param = new HashMap();
						param.put("STOCK", data.getStock());
						if (((TradingApplication) apps).getFeed().getAction()
								.get(FeedAction.A_SHOWCOMPANY).isEnabled()) {
							((TradingApplication) apps).getFeed().getUI()
									.showUI(FeedUI.UI_COMPANYPROFILE, param);
						}
					}
				} catch (Exception ex) {
					Utils.showMessage("cannot open Browser", null);
				}
			}
		});
		mnCompany.setText("Company Profile");
		popupMenu.add(mnCompany);
		JMenuItem mnIntraday = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getSelectedRow();
				if (table.getTable().getRowCount() == 1)
					i = 0;
				if (i >= 0) {
					Portfolio data = (Portfolio) table.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", data.getStock());
					if (((TradingApplication) apps).getFeed().getAction()
							.get(FeedAction.A_SHOWINTRADAY).isEnabled()) {
						((TradingApplication) apps).getFeed().getUI()
								.showUI(FeedUI.UI_INTRADAY, param);
					}
				}
			}
		});
		mnIntraday.setText("Intraday Chart");
		popupMenu.add(mnIntraday);

		JMenuItem mnTechnical = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getSelectedRow();
				if (table.getTable().getRowCount() == 1)
					i = 0;
				if (i >= 0) {
					Portfolio data = (Portfolio) table.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", data.getStock());
					if (((TradingApplication) apps).getFeed().getAction()
							.get(FeedAction.A_SHOWCHART).isEnabled()) {
						((TradingApplication) apps).getFeed().getUI()
								.showUI(FeedUI.UI_CHART, param);
					}
				}
			}
		});
		mnTechnical.setText("Technical Chart");
		popupMenu.add(mnTechnical);
	}

	@Override
	protected void build() {
		createPopup();
		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_PORTFOLIO), null,
				(Hashtable) hSetting.get("table"));
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setColumnHide(PortfolioDef.columnhide3);
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		btnBuy = new JButton("Buy");
		btnSell = new JButton("Sell");

		btnBuy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if (table.getTable().getRowCount() == 1)
					i = 0;
				if (i >= 0) {
					Portfolio data = (Portfolio) table.getDataByIndex(i);
					HashMap param = new HashMap(2);
					param.put("TYPE", "BUY");
					param.put("STOCK", data.getStock());
					param.put("PRICE", data.getLastPrice());
					param.put("BOARD",(data.getStock()).endsWith("-R")? "TN":"RG");
					if (((TradingApplication) apps).getAction()
							.get(TradingAction.A_SHOWBUYORDER).isEnabled()) {
						((TradingApplication) apps).getUI().showUI(
								TradingUI.UI_ENTRYORDER, param);
					}
				}
			}
		});

		btnSell.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int i = table.getSelectedRow();
				if (table.getTable().getRowCount() == 1)
					i = 0;
				if (i >= 0) {
					Portfolio data = (Portfolio) table.getDataByIndex(i);
					HashMap param = new HashMap(2);
					param.put("TYPE", "SELL");
					param.put("STOCK", data.getStock());
					param.put("PRICE", data.getLastPrice());
					param.put("BOARD",(data.getStock()).endsWith("-R")? "TN":"RG");
					if (((TradingApplication) apps).getAction()
							.get(TradingAction.A_SHOWBUYORDER).isEnabled()) {
						((TradingApplication) apps).getUI().showUI(
								TradingUI.UI_ENTRYORDER, param);
					}
				}
			}
		});
		JPanel pnlBtn = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		pnlBtn.add(btnBuy);
		pnlBtn.add(btnSell);

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		pnlContent.add(pnlBtn, BorderLayout.NORTH);
		refresh();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", PortfolioDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 400, 300));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
	}

	@Override
	public void show() {
		if (!apps.getAction().get(TradingAction.A_SHOWPFSIMPLE).isGranted())
			return;
		super.show();
	}

}
