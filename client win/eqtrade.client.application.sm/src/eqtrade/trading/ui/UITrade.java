package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.demo.Mapping;
import com.vollux.framework.UI;
import com.vollux.idata.Column;
import com.vollux.ui.CustomMouseAdapter;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;
import com.vollux.ui.JTextLabel;

import eqtrade.application.IEQTradeApp;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.OrderRender;
import eqtrade.trading.model.Stock;
import eqtrade.trading.model.UserProfile;
import eqtrade.trading.report.OrderReport;
import eqtrade.trading.ui.OrderPanel.ExcelFileFilter;
import eqtrade.trading.ui.OrderPanel.MyCustomMouseAdapter;
import eqtrade.trading.ui.OrderPanel.MyMouseAdapter;

public class UITrade extends UI {
	private JGrid table;
	private FilterTrade filter;
	boolean isLoadingSuperUser = false;
	private JDropDown comboClient;
	private JDropDown comboStock;
	private JDropDown comboBOS;
	private JDropDown comboTaker;
	private JDropDown comboBoard;
	private JDropDown comboFloor;
	private JNumber price;
	protected JText fieldMarketOrderid;
	OrderReport report;

	protected JNumber tCount;
	protected JNumber tLot;
	protected JNumber tValue;

	//yosep 15-09-2015
	protected JNumber avgSell;
	protected JNumber avgBuy;
	
	private JButton btnClear;
	private JButton btnView;

	// for trade summary
	private JGrid tableSumm;
	private FilterTrade filterSumm;
	private JNumber tCountSumm;
	private JNumber tLotSumm;
	private JNumber tValueSumm;
	// private JPopupMenu
	private JPopupMenu popupMenuSumm;
	// private JPopupMenu popupMenu;
	private JMenuItem printExcelSum;
	private JMenuItem printExcel;

	private JTabbedPane tab;
	private JButton btnPrint;
	private CustomSelectionModel customSelection;
	private String sfilterClient = "",sfilterStock= "";

	public UITrade(String app) {
		super("Trade List", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_TRADE);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
		printExcel = new JMenuItem(new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveFile();

			}
		});
		printExcel.setText("Save To Excel");
		popupMenu.add(printExcel);

		popupMenuSumm = new JPopupMenu();
		JMenuItem propertiesMenuSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				tableSumm.showProperties();
			}
		});
		propertiesMenuSumm.setText("Properties");
		popupMenuSumm.add(propertiesMenuSumm);
		printExcelSum = new JMenuItem(new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//System.out.println("Save To Excel Sum");

			}
		});
		printExcelSum.setText("Save To Excel ..");
		printExcelSum.setActionCommand("printExcel");
		popupMenuSumm.add(printExcelSum);

	}

	/*
	 * private void calculateSummary(){ SwingUtilities.invokeLater(new
	 * Runnable() { public void run() { try { //hapus dulu data summarynya
	 * kemudian hitung kembali summarynya berdasarkan data yg muncul pada view
	 * detail. ((IEQTradeApp)
	 * apps).getTradingEngine().getStore(TradingStore.DATA_TRADESUMM
	 * ).getDataVector().clear(); int size = table.getTable().getRowCount(); for
	 * (int i = 0; i < size; i++) { Order trade = (Order)
	 * table.getDataByIndex(i); Order summ = (Order)((IEQTradeApp)
	 * apps).getTradingEngine
	 * ().getStore(TradingStore.DATA_TRADESUMM).getDataByKey(new
	 * Object[]{trade.getId(), trade.getPrice()}); if (summ == null){ summ = new
	 * Order(); summ.fromVector(trade.getData()); Vector row = new Vector(1);
	 * row.addElement(OrderDef.createTableRow(summ)); ((IEQTradeApp)
	 * apps).getTradingEngine
	 * ().getStore(TradingStore.DATA_TRADESUMM).addRow(row, false, false); }
	 * else { summ.setVolume(new Double(summ.getVolume().doubleValue() +
	 * trade.getVolume().doubleValue())); summ.setLot(new
	 * Double(summ.getLot().doubleValue() + trade.getLot().doubleValue()));
	 * summ.setDoneVol(new Double(summ.getDoneVol().doubleValue() +
	 * trade.getDoneVol().doubleValue())); summ.calculate(); } } ((IEQTradeApp)
	 * apps).getTradingEngine().getStore(TradingStore.DATA_TRADESUMM).refresh();
	 * } catch (Exception ex){
	 * 
	 * } } }); }
	 */

	private void calculateSummary() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					// hapus dulu data summarynya kemudian hitung kembali
					// summarynya berdasarkan data yg muncul pada view detail.
					((IEQTradeApp) apps).getTradingEngine()
							.getStore(TradingStore.DATA_TRADESUMM)
							.getDataVector().clear();
					((IEQTradeApp) apps).getTradingEngine()
							.getStore(TradingStore.DATA_TRADESUMM).refresh();
					synchronized (table) {
						int size = table.getTable().getRowCount();
						for (int i = 0; i < size; i++) {
							Order trade = (Order) table.getDataByIndex(i);
							Order summ = (Order) ((IEQTradeApp) apps)
									.getTradingEngine()
									.getStore(TradingStore.DATA_TRADESUMM)
									.getDataByKey(
											new Object[] { trade.getId(),
													trade.getPrice() });
							if (summ == null) {
								summ = new Order();
								summ.fromVector(trade.getData());
								Vector row = new Vector(1);
								row.addElement(OrderDef.createTableRow(summ));
								((IEQTradeApp) apps).getTradingEngine()
										.getStore(TradingStore.DATA_TRADESUMM)
										.addRow(row, false, false);
							} else {
								summ.setVolume(new Double(summ.getVolume()
										.doubleValue()
										+ trade.getVolume().doubleValue()));
								summ.setLot(new Double(summ.getLot()
										.doubleValue()
										+ trade.getLot().doubleValue()));
								summ.setDoneVol(new Double(summ.getDoneVol()
										.doubleValue()
										+ trade.getDoneVol().doubleValue()));
								summ.calculate();
							}
						}
					}
					((IEQTradeApp) apps).getTradingEngine()
							.getStore(TradingStore.DATA_TRADESUMM).refresh();
				} catch (Exception ex) {

				}
			}
		});
	}

	@Override
	protected void build() {
		createPopup();
		filter = new FilterTrade(form);
		((FilterColumn) filter.getFilteredData("taker")).setField("0");
		((FilterColumn) filter.getFilteredData("tradeno")).setField(null);
		((FilterColumn) filter.getFilteredData("user"))
				.setField(((IEQTradeApp) apps).getTradingEngine().getUserId()
						.toUpperCase());

		filterSumm = new FilterTrade(form);
		((FilterColumn) filterSumm.getFilteredData("taker")).setField("0");
		((FilterColumn) filterSumm.getFilteredData("tradeno")).setField(null);
		((FilterColumn) filterSumm.getFilteredData("user"))
				.setField(((IEQTradeApp) apps).getTradingEngine().getUserId()
						.toUpperCase());

		comboClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboStock = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		comboBOS = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_SIDE), Mapping.CIDX_CODE, false);
		comboTaker = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_TAKER), Mapping.CIDX_CODE, false);
		comboBoard = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_BOARDFILTER), Mapping.CIDX_NAME,
				false);
		comboFloor = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_FLOORFILTER), Mapping.CIDX_NAME,
				false);
		price = new JNumber(Double.class, 0, 0, false, true);
		fieldMarketOrderid = new JText(true);
		btnClear = new JButton("Clear");
		btnClear.setMnemonic('R');
		btnView = new JButton("View");
		btnView.setMnemonic('V');
		btnPrint = new JButton("Print");
		btnPrint.setMnemonic('P');

		report = new OrderReport(null, OrderDef.getTableOrderReportDef());
		fieldMarketOrderid.setForeground(Color.black);

		tab = new JTabbedPane();

		registerEvent(comboClient.getTextEditor());
		registerEventCombo(comboClient);
		registerEvent(comboStock.getTextEditor());
		registerEventCombo(comboStock);
		registerEvent(comboBOS);
		registerEvent(comboTaker);
		registerEvent(comboBoard);
		registerEvent(comboFloor);
		registerEvent(price);
		registerEvent(btnClear);
		registerEvent(btnView);

		// Changed 201104225-- Bug by Bima
		/*
		 * btnView.addActionListener(new ActionListener() { public void
		 * actionPerformed(ActionEvent e) { if (isSuperUser()) { String clientid
		 * = comboClient.getText(); if (clientid != null && !clientid.isEmpty())
		 * { setState(false); ((IEQTradeApp)
		 * apps).getTradingEngine().refreshTrade("%");; }
		 * filter.fireFilterChanged(); filterSumm.fireFilterChanged(); } else {
		 * filter.fireFilterChanged(); filterSumm.fireFilterChanged();
		 * calculateSummary(); } } });
		 */

		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (isSuperUser()) {
					String clientid = comboClient.getText();
					if (clientid != null && !clientid.isEmpty()) {
						setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshTrade(
								clientid);
						;
					}

					filter.fireFilterChanged();
					filterSumm.fireFilterChanged();

					if (!sfilterClient.isEmpty()
							&& !isFilterClientAdded(sfilterClient)) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByClient(sfilterClient);
					}

					String accid = getAccountId(clientid);

					if (!accid.isEmpty()) {

						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByClient(accid);
						sfilterClient = accid;
					}
					
					if (!sfilterStock.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByStock(sfilterStock);

					}



					
					String stockid = comboStock.getText();

					if (stockid != null && !stockid.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByStock(stockid.toUpperCase());
						sfilterStock = stockid.toUpperCase();
					}

				} else {
					filter.fireFilterChanged();
					filterSumm.fireFilterChanged();
					calculateSummary();
				}

			}
		});

		btnPrint.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Vector v = getSelected();
				if (v != null && v.size() > 0) {
					report.print(v);
				} else {
					Utils.showMessage("Please select trade order to print ",
							null);
				}

			}
		});

		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				comboClient.getTextEditor().setText("");
				comboStock.getTextEditor().setText("");
				comboTaker.setSelectedIndex(0);
				comboBoard.setSelectedIndex(0);
				comboFloor.setSelectedIndex(1);
				price.setValue(null);
				fieldMarketOrderid.setText("");
				comboBOS.setSelectedIndex(0);
				comboTaker.setSelectedIndex(0);
				((FilterColumn) filter.getFilteredData("account"))
						.setField(null);
				((FilterColumn) filter.getFilteredData("stock")).setField(null);
				((FilterColumn) filter.getFilteredData("bos")).setField(null);
				((FilterColumn) filter.getFilteredData("status"))
						.setField(null);
				((FilterColumn) filter.getFilteredData("taker")).setField("0");
				((FilterColumn) filter.getFilteredData("board")).setField(null);
				((FilterColumn) filter.getFilteredData("price")).setField(null);
				((FilterColumn) filter.getFilteredData("marketid"))
						.setField(null);
				((FilterColumn) filter.getFilteredData("floor")).setField("0");
				filter.fireFilterChanged();

				((FilterColumn) filterSumm.getFilteredData("account"))
						.setField(null);
				((FilterColumn) filterSumm.getFilteredData("stock"))
						.setField(null);
				((FilterColumn) filterSumm.getFilteredData("bos"))
						.setField(null);
				((FilterColumn) filterSumm.getFilteredData("status"))
						.setField(null);
				((FilterColumn) filterSumm.getFilteredData("taker"))
						.setField("0");
				((FilterColumn) filterSumm.getFilteredData("board"))
						.setField(null);
				((FilterColumn) filterSumm.getFilteredData("price"))
						.setField(null);
				((FilterColumn) filterSumm.getFilteredData("marketid"))
						.setField(null);
				((FilterColumn) filterSumm.getFilteredData("floor"))
						.setField("0");
				filterSumm.fireFilterChanged();
			}
		});
		comboClient.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboClient.showPopup();
				} catch (Exception ex) {
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				((FilterColumn) filter.getFilteredData("account"))
						.setField(comboClient.getText().trim().equals("") ? null
								: comboClient.getText().trim().toUpperCase());
				((FilterColumn) filterSumm.getFilteredData("account"))
						.setField(comboClient.getText().trim().equals("") ? null
								: comboClient.getText().trim().toUpperCase());
				filter.fireFilterChanged();
				filterSumm.fireFilterChanged();
			}
		});
		comboStock.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboStock.showPopup();
				} catch (Exception ex) {
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				((FilterColumn) filter.getFilteredData("stock"))
						.setField(comboStock.getText().trim().equals("") ? null
								: comboStock.getText().trim().toUpperCase());
				((FilterColumn) filterSumm.getFilteredData("stock"))
						.setField(comboStock.getText().trim().equals("") ? null
								: comboStock.getText().trim().toUpperCase());
				filter.fireFilterChanged();
				filterSumm.fireFilterChanged();
			}
		});
		comboBOS.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				((FilterColumn) filter.getFilteredData("bos"))
						.setField(comboBOS.getSelectedIndex() == 0 ? null
								: getBOSCode());
				((FilterColumn) filterSumm.getFilteredData("bos"))
						.setField(comboBOS.getSelectedIndex() == 0 ? null
								: getBOSCode());
				filter.fireFilterChanged();
				filterSumm.fireFilterChanged();
			}
		});
		comboTaker.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				((FilterColumn) filter.getFilteredData("taker"))
						.setField(comboTaker.getSelectedIndex() + "");
				((FilterColumn) filterSumm.getFilteredData("taker"))
						.setField(comboTaker.getSelectedIndex() + "");
				filter.fireFilterChanged();
				filterSumm.fireFilterChanged();
			}
		});
		comboBoard.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				((FilterColumn) filter.getFilteredData("board"))
						.setField(comboBoard.getSelectedIndex() == 0 ? null
								: getBoardCode());
				((FilterColumn) filterSumm.getFilteredData("board"))
						.setField(comboBoard.getSelectedIndex() == 0 ? null
								: getBoardCode());
				filter.fireFilterChanged();
				filterSumm.fireFilterChanged();
			}
		});
		comboFloor.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				((FilterColumn) filter.getFilteredData("floor"))
						.setField(comboFloor.getSelectedIndex() == 0 ? null
								: getFloorCode());
				((FilterColumn) filterSumm.getFilteredData("floor"))
						.setField(comboFloor.getSelectedIndex() == 0 ? null
								: getFloorCode());
				filter.fireFilterChanged();
				filterSumm.fireFilterChanged();
			}
		});
		price.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				try {
					price.commitEdit();
				} catch (Exception ex) {
				}
				((FilterColumn) filter.getFilteredData("price")).setField(price
						.getValue());
				((FilterColumn) filterSumm.getFilteredData("price"))
						.setField(price.getValue());
				filter.fireFilterChanged();
				filterSumm.fireFilterChanged();
			}
		});
		fieldMarketOrderid.addFocusListener(new FocusAdapter() {

			@Override
			public void focusLost(FocusEvent e) {
				String mk = null;
				if (!fieldMarketOrderid.getText().isEmpty())
					mk = fieldMarketOrderid.getText();
				((FilterColumn) filter.getFilteredData("marketid"))
						.setField(mk);
				((FilterColumn) filterSumm.getFilteredData("marketid"))
						.setField(mk);
				filter.fireFilterChanged();
				filterSumm.fireFilterChanged();
			}

		});

		JPanel editorPanel = new JPanel();
		FormLayout layout = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,75px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, 100px, 2dlu, pref, 2dlu, pref",
				"pref");
		PanelBuilder builder = new PanelBuilder(layout, editorPanel);
		builder.setBorder(new EmptyBorder(4, 4, 4, 4));
		CellConstraints cc = new CellConstraints();
		builder.add(new JTextLabel("Client", 'C', comboClient.getTextEditor()),
				cc.xy(1, 1));
		builder.add(comboClient, cc.xy(3, 1));
		builder.add(new JTextLabel("Stock", 'S', comboStock.getTextEditor()),
				cc.xy(5, 1));
		builder.add(comboStock, cc.xy(7, 1));
		builder.add(new JTextLabel("Price", 'P', price), cc.xy(9, 1));
		builder.add(price, cc.xy(11, 1));
		builder.add(new JTextLabel("MarketOrderID", 'M', fieldMarketOrderid),
				cc.xy(13, 1));
		builder.add(fieldMarketOrderid, cc.xy(15, 1));
		builder.add(new JTextLabel("B/S", 'B', comboBOS), cc.xy(17, 1));
		builder.add(comboBOS, cc.xy(19, 1));
		builder.add(new JTextLabel("Board", 'R', comboBoard), cc.xy(21, 1));
		builder.add(comboBoard, cc.xy(23, 1));
		builder.add(new JTextLabel("Entry", 'R', comboFloor), cc.xy(25, 1));
		builder.add(comboFloor, cc.xy(27, 1));
		builder.add(comboTaker, cc.xy(29, 1));
		builder.add(btnView, cc.xy(31, 1));
		builder.add(btnClear, cc.xy(33, 1));
		builder.add(btnPrint, cc.xy(35, 1));

		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_ORDER), filter,
				(Hashtable) hSetting.get("table"));
		table.getTable().add(popupMenu);
		table.getTable().addMouseListener(new MyMouseAdapter());
		table.addMouseListener(new MyCustomMouseAdapter());
		table.setColumnHide(OrderDef.columnhide2);
		registerEvent(table);
		registerEvent(table.getTable());
		table.getTable().getModel().addTableModelListener(new RowChanged());

		customSelection = new CustomSelectionModel();
		customSelection
				.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.getTable().setSelectionModel(customSelection);
		table.getTable().setSelectionMode(
				ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		tableSumm = createTable(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_TRADESUMM), filterSumm,
				(Hashtable) hSetting.get("tablesumm"));
		tableSumm.getTable().add(popupMenuSumm);
		tableSumm.getTable().addMouseListener(new MyMouseAdapter());
		tableSumm.addMouseListener(new MyCustomMouseAdapter());
		tableSumm.setColumnHide(OrderDef.columnhide2);
		registerEvent(tableSumm);
		registerEvent(tableSumm.getTable());
		tableSumm.getTable().getModel()
				.addTableModelListener(new RowChangedSumm());

		tCount = new JNumber(Double.class, 0, 0, false, false);
		tLot = new JNumber(Double.class, 0, 0, false, false);
		tValue = new JNumber(Double.class, 0, 0, false, false);

		//yosep 15-09-2015
		avgSell = new JNumber(Double.class, 0, 4, false, false);
		avgBuy  = new JNumber(Double.class, 0, 4, false, false);
		
		tCountSumm = new JNumber(Double.class, 0, 0, false, false);
		tLotSumm = new JNumber(Double.class, 0, 0, false, false);
		tValueSumm = new JNumber(Double.class, 0, 0, false, false);

		JPanel infoPanel = new JPanel();
		FormLayout infLayout = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,80px, 2dlu, pref, 2dlu, 150px, 2dlu, pref, 2dlu, 80px,2dlu ,pref, 2dlu, 80px",
				"pref");
		PanelBuilder infBuilder = new PanelBuilder(infLayout, infoPanel);
		infBuilder.setBorder(new EmptyBorder(4, 4, 4, 4));

		infBuilder.add(new JLabel("Count"), cc.xy(1, 1));
		infBuilder.add(tCount, cc.xy(3, 1));
		infBuilder.add(new JLabel("T.Lot"), cc.xy(5, 1));
		infBuilder.add(tLot, cc.xy(7, 1));
		infBuilder.add(new JLabel("T.Value"), cc.xy(9, 1));
		infBuilder.add(tValue, cc.xy(11, 1));
		infBuilder.add(new JLabel("Avg Sell"), cc.xy(13, 1));//yosep 15-09-2015 avg price
		infBuilder.add(avgSell, cc.xy(15, 1));
		infBuilder.add(new JLabel("Avg Buy") , cc.xy(17, 1));
		infBuilder.add(avgBuy, cc.xy(19, 1));
		

		JPanel infoPanelSumm = new JPanel();
		FormLayout infLayoutSumm = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,80px, 2dlu, pref, 2dlu, 150px",
				"pref");
		PanelBuilder infBuilderSumm = new PanelBuilder(infLayoutSumm,
				infoPanelSumm);
		infBuilderSumm.setBorder(new EmptyBorder(4, 4, 4, 4));

		infBuilderSumm.add(new JLabel("Summary Count"), cc.xy(1, 1));
		infBuilderSumm.add(tCountSumm, cc.xy(3, 1));
		infBuilderSumm.add(new JLabel("T.Lot"), cc.xy(5, 1));
		infBuilderSumm.add(tLotSumm, cc.xy(7, 1));
		infBuilderSumm.add(new JLabel("T.Value"), cc.xy(9, 1));
		infBuilderSumm.add(tValueSumm, cc.xy(11, 1));

		JScrollPane t = new JScrollPane(editorPanel);
		t.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		t.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		JScrollPane b = new JScrollPane(infoPanel);
		b.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		b.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

		JScrollPane bSumm = new JScrollPane(infoPanelSumm);
		bSumm.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		bSumm.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

		pnlContent = new JSkinPnl();
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(t, BorderLayout.NORTH);

		JSkinPnl pnldetail = new JSkinPnl();
		pnldetail.add(table, BorderLayout.CENTER);
		pnldetail.add(b, BorderLayout.PAGE_END);

		JSkinPnl pnlsumm = new JSkinPnl();
		pnlsumm.add(tableSumm, BorderLayout.CENTER);
		pnlsumm.add(bSumm, BorderLayout.PAGE_END);

		tab.addTab("detail", pnldetail);
		tab.addTab("summary", pnlsumm);
		pnlContent.setOpaque(true);
		pnlContent.add(tab, BorderLayout.CENTER);

		try {
			comboBOS.setSelectedIndex(0);
			comboTaker.setSelectedIndex(0);
			comboBoard.setSelectedIndex(0);
			comboFloor.setSelectedIndex(0);
			price.setValue(null);
		} catch (Exception ex) {
		}
		refresh();
	}

	private void registerEventCombo(JDropDown comp) {
		// comp.getEditor().getEditorComponent()
		// .setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(
				JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap()
				.put("enterAction", new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(comboStock.getTextEditor())) {
							comboStock.setSelectedIndex(comboStock
									.getSelectedIndex());
							comboStock.hidePopup();
							price.requestFocus();
						} else if (e.getSource().equals(
								comboClient.getTextEditor())) {
							comboClient.setSelectedIndex(comboClient
									.getSelectedIndex());
							comboClient.hidePopup();
							comboStock.requestFocus();
						}

						// comboStock.requestFocus();

					}

				});
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", OrderDef.getTableTradeDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 800, 300));
		if (hSetting.get("tablesumm") == null) {
			hSetting.put("tablesumm", OrderDef.getTableTradeDef());
		}
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		hSetting.put("tablesumm", tableSumm.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
		((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ORDER).refresh();

		tableSumm.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tableSumm.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tableSumm.setNewFont(TradingSetting.getFont());
		((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_TRADESUMM).refresh();
	}

	String getBOSCode() {
		Mapping m = (Mapping) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_SIDE)
				.getDataByKey(new Object[] { comboBOS.getSelectedItem() });
		return (m != null) ? m.getName() : null;
	}

	String getFloorCode() {
		Mapping m = (Mapping) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_FLOORFILTER)
				.getDataByField(new Object[] { comboFloor.getSelectedItem() },
						new int[] { Mapping.CIDX_NAME });
		return (m != null) ? m.getCode() : null;
	}

	Vector getBoardCode() {
		Vector v = new Vector();
		if (comboBoard.getSelectedIndex() == 0) {
			return null;
		} else {
			Mapping m = (Mapping) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_BOARDFILTER)
					.getDataByField(
							new Object[] { comboBoard.getSelectedItem() },
							new int[] { Mapping.CIDX_NAME });
			if (m != null) {
				Utils.parser(m.getCode(), "|", v);
			}
			return v;
		}
	}

	protected class RowChanged implements TableModelListener {
		@Override
		public void tableChanged(final TableModelEvent e) {
			double count = 0, tlot = 0, tvalue = 0, tSellValue=0, tSellLot=0, tBuyValue=0, tBuyLot=0 ;
			int size = table.getTable().getRowCount();
			for (int i = 0; i < size; i++) {
				Order order = (Order) table.getDataByIndex(i);
				count++;
				tlot = tlot + order.getLot().doubleValue();
				tvalue = tvalue
						+ (order.getVolume().doubleValue() * order.getPrice()
								.doubleValue());
				//yosep 15-09-2015 avgprice
				Stock sec = (Stock) ((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_STOCK)
						.getDataByKey(new String[] { order.getStock() });
				if (order.getBOS().equalsIgnoreCase("2")) {
					tSellValue 	= tSellValue + (order.getVolume().doubleValue() * order.getPrice()
									.doubleValue());
					tSellLot 	= tSellLot + (order.getLot().doubleValue() * sec.getLotSize());
				} else {
					tBuyValue 	= tBuyValue + (order.getVolume().doubleValue() * order.getPrice()
									.doubleValue());
					tBuyLot		= tBuyLot + (order.getLot().doubleValue() * sec.getLotSize());
				}
			}
			tCount.setValue(new Double(count));
			tLot.setValue(new Double(tlot));
			tValue.setValue(new Double(tvalue));
			//yosep 15-09-2015 avgprice
			avgBuy.setValue(tBuyLot > 0 ? new Double(tBuyValue/tBuyLot) : new Double(0));
			avgSell.setValue(tSellLot > 0 ? new Double(tSellValue/tSellLot) : new Double(0));
		}
	}

	protected class RowChangedSumm implements TableModelListener {
		@Override
		public void tableChanged(final TableModelEvent e) {
			double count = 0, tlot = 0, tvalue = 0;
			int size = tableSumm.getTable().getRowCount();
			for (int i = 0; i < size; i++) {
				Order order = (Order) tableSumm.getDataByIndex(i);
				count++;
				tlot = tlot + order.getLot().doubleValue();
				tvalue = tvalue
						+ (order.getVolume().doubleValue() * order.getPrice()
								.doubleValue());
				
			}
			tCountSumm.setValue(new Double(count));
			tLotSumm.setValue(new Double(tlot));
			tValueSumm.setValue(new Double(tvalue));
		}
	}

	void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "secAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "clientAction");
		comp.getActionMap().put("secAction", new AbstractAction("secAction") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent evt) {
				comboStock.getTextEditor().requestFocus();
			}
		});
		comp.getActionMap().put("clientAction",
				new AbstractAction("clientAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						comboClient.getTextEditor().requestFocus();
					}
				});
		comp.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						form.setVisible(false);
					}
				});

		comp.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton) {
							((JButton) evt.getSource()).doClick();
						} else {
							if (evt.getSource() instanceof JNumber) {
								try {
									((JNumber) evt.getSource()).commitEdit();
								} catch (Exception ex) {
								}
								;
							}
						}
					}
				});
	}

	protected boolean isValidRow(int x, int y) {
		return true;
	}

	/*
	 * public class MyMouseAdapter extends MouseAdapter { public void
	 * mousePressed(MouseEvent e) { if (e.isPopupTrigger()) {
	 * 
	 * if (isValidRow(e.getX(), e.getY())) { popupMenu.show(e.getComponent(),
	 * e.getX(), e.getY()); } } }
	 * 
	 * public void mouseReleased(MouseEvent e) { if (e.isPopupTrigger()) { if
	 * (isValidRow(e.getX(), e.getY())) { popupMenu.show(e.getComponent(),
	 * e.getX(), e.getY()); } } } };
	 * 
	 * public class MyCustomMouseAdapter extends CustomMouseAdapter { public
	 * void mousePressed(MouseEvent e) { if (e.isPopupTrigger()) {
	 * 
	 * if (isValidRow(e.getX(), e.getY())) { popupMenu.show(e.getComponent(),
	 * e.getX(), e.getY()); } } }
	 * 
	 * public void mouseReleased(MouseEvent e) { if (e.isPopupTrigger()) { if
	 * (isValidRow(e.getX(), e.getY())) { popupMenu.show(e.getComponent(),
	 * e.getX(), e.getY()); } } } }
	 */

	public void setFilteredMarket(String marketorderid) {

		if (marketorderid != null && marketorderid.isEmpty()) {
			marketorderid = null;
		} else if (marketorderid == null) {
			fieldMarketOrderid.setText("");
		} else {
			fieldMarketOrderid.setText(marketorderid);
		}
		((FilterColumn) filter.getFilteredData("marketid"))
				.setField(marketorderid);
		filter.fireFilterChanged();
	}

	private boolean isSuperUser() {
		// return true;
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		return sales != null ? sales.getProfileId().toUpperCase()
				.equals("SUPERUSER") : false;
		// return false;
	}

	public void setState(boolean state) {
		btnView.setEnabled(state);
		btnClear.setEnabled(state);
		isLoadingSuperUser = !state;
	}

	public void success() {
		if (isLoadingSuperUser) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					setState(true);
					((FilterColumn) filter.getFilteredData("account"))
							.setField(comboClient.getText().trim().equals("") ? null
									: comboClient.getText().trim()
											.toUpperCase());
					((FilterColumn) filterSumm.getFilteredData("account"))
							.setField(comboClient.getText().trim().equals("") ? null
									: comboClient.getText().trim()
											.toUpperCase());
					filter.fireFilterChanged();
					filterSumm.fireFilterChanged();
					calculateSummary();
				}
			});
		}
	}

	public void failed(final String reason) {
		if (isLoadingSuperUser) {

			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					setState(true);
					Utils.showMessage(reason, null);
				}
			});
		}

	}

	public Vector getSelected() {
		Vector v = ((CustomSelectionModel) table.getTable().getSelectionModel())
				.getVSelected();
		Vector result = new Vector(v.size());
		for (int i = 0; i < v.size(); i++) {
			result.addElement(((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ORDER)
					.getDataByIndex(
							table.getMappedRow(((Integer) v.elementAt(i))
									.intValue())));
		}
		return result;
	}

	protected JFileChooser jfc = null;
	protected ExcelFileFilter filterFileExcel = null;

	protected void saveFile() {
		if (jfc == null) {
			filterFileExcel = new ExcelFileFilter();
			filterFileExcel.addExtension("xls");
			filterFileExcel.setDescription(" Microsoft Excel ");
			jfc = new JFileChooser(new File("C:\\"));
			jfc.setFileFilter(filterFileExcel);
			jfc.setAcceptAllFileFilterUsed(false);
			jfc.setDialogTitle("Save Trade To...");
			jfc.setDialogType(JFileChooser.SAVE_DIALOG);
		}
		String filename = "";
		int returnVal = jfc.showSaveDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File ffile = jfc.getSelectedFile();
			filename = ffile.getAbsolutePath();
			if (!filename.endsWith(".xls")) {
				filename = filename + ".xls";
			}
		} else {
			return;
		}
		int i = table.getTable().getRowCount();
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(filename));
			Hashtable prop = table.getTableProperties();
			int[] order = (int[]) prop.get("order");
			int[] width = (int[]) prop.get("width");
			String header = "";
			for (int k = 0; k < order.length; k++) {
				if (width[k] != 0) {
					header = header + OrderDef.getHeader().elementAt(order[k])
							+ "\t";

				}
			}
			out.write(header + "\n");
			for (int j = 0; j < i; j++) {
				Column dat = (Column) table.getTable().getValueAt(j, 0);
				Order ord = (Order) dat.getSource();
				String row = "";
				for (int k = 0; k < order.length; k++) {

					if (width[k] != 0) {
						switch (order[k]) {
						case Order.C_STATUSID:
							row = row
									+ OrderRender.hashStatus.get(ord
											.getData(order[k])) + "\t";
							break;
						case Order.C_BUYORSELL:
							row = row
									+ OrderRender.hashSide.get(ord
											.getData(order[k])) + "\t";
							break;
						case Order.C_ORDERTYPE:
							row = row
									+ OrderRender.hashType.get(ord
											.getData(order[k])) + "\t";
							break;
						case Order.C_ORDTYPE:
							row = row + "limited order\t";
							break;
						default:
							row = row + ord.getData(order[k]) + "\t";
							break;
						}
					}
				}
				out.write(row + "\n");
			}
			out.close();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public class ExcelFileFilter extends FileFilter {

		private Hashtable filters = null;
		private String description = null;
		private String fullDescription = null;
		private boolean useExtensionsInDescription = true;

		public ExcelFileFilter() {
			this.filters = new Hashtable();
		}

		public ExcelFileFilter(String extension) {
			this(extension, null);
		}

		public ExcelFileFilter(String extension, String description) {
			this();
			if (extension != null)
				addExtension(extension);
			if (description != null)
				setDescription(description);
		}

		public ExcelFileFilter(String[] filters) {
			this(filters, null);
		}

		public ExcelFileFilter(String[] filters, String description) {
			this();
			for (int i = 0; i < filters.length; i++) {
				// add filters one by one
				addExtension(filters[i]);
			}
			if (description != null)
				setDescription(description);
		}

		public boolean accept(File f) {
			if (f != null) {
				if (f.isDirectory()) {
					return true;
				}
				String extension = getExtension(f);
				if (extension != null && filters.get(getExtension(f)) != null) {
					return true;
				}
				;
			}
			return false;
		}

		public String getExtension(File f) {
			if (f != null) {
				String filename = f.getName();
				int i = filename.lastIndexOf('.');
				if (i > 0 && i < filename.length() - 1) {
					return filename.substring(i + 1).toLowerCase();
				}
				;
			}
			return null;
		}

		public void addExtension(String extension) {
			if (filters == null) {
				filters = new Hashtable(5);
			}
			filters.put(extension.toLowerCase(), this);
			fullDescription = null;
		}

		public String getDescription() {
			if (fullDescription == null) {
				if (description == null || isExtensionListInDescription()) {
					fullDescription = description == null ? "(" : description
							+ " (";
					// build the description from the extension list
					Enumeration extensions = filters.keys();
					if (extensions != null) {
						fullDescription += "."
								+ (String) extensions.nextElement();
						while (extensions.hasMoreElements()) {
							fullDescription += ", ."
									+ (String) extensions.nextElement();
						}
					}
					fullDescription += ")";
				} else {
					fullDescription = description;
				}
			}
			return fullDescription;
		}

		public void setDescription(String description) {
			this.description = description;
			fullDescription = null;
		}

		public void setExtensionListInDescription(boolean b) {
			useExtensionsInDescription = b;
			fullDescription = null;
		}

		public boolean isExtensionListInDescription() {
			return useExtensionsInDescription;
		}

	}

	public boolean isFilterClientAdded(String client) {
		try {
			String sf = ((IEQTradeApp) apps).getTradingEngine().getConnection()
					.loadFilterByClient();
			String[] sp = sf.split("#");
			for (int i = 0; i < sp.length; i++)
				if (sp[i].trim().toLowerCase().equals(client))
					return true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
				
		return false;

	}

	String getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { alias },
						new int[] { Account.C_TRADINGID });
		return (acc != null ? acc.getAccId() : "");
	}

}
