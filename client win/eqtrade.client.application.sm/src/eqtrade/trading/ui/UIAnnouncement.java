package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import sun.net.www.content.image.jpeg;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;


import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Announcement;
import eqtrade.trading.model.AnnouncementDef;
import eqtrade.trading.model.DueDateDef;

public class UIAnnouncement extends UI{
	private JGrid table;
	private JWebBrowser webBrowser;
	private String LocationUrl;
	private JSkinDlg frame;

	public UIAnnouncement(String app) {
		super("Announcement", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_ANNOUCEMENT);
		
	}
	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				webBrowser.requestFocus();
				
			}
		});
		
	}

	@Override
	protected void build() {
		table = createTable(
				((IEQTradeApp)apps).getTradingEngine()
				.getStore(TradingStore.DATA_ANNOUNCEMENT),null ,
				(Hashtable) hSetting.get("tabel"));
		
		JPanel pnlTabel = new JPanel();
		FormLayout l =new FormLayout("2dlu,570px,2dlu","2dlu,150px,2dlu");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l,pnlTabel);
		b.add(table, c.xy(2, 2));
		pnlTabel.setBorder(new EmptyBorder(5,5,5,5));
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(5,5,5,5));
		pnlContent.add(pnlTabel, BorderLayout.NORTH);
		jalan();
		table.getTable().addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e){
				if(e.getClickCount()==1){
					int i=table.getMappedRow(table.getSelectedRow());
					if(i>-1){
						Announcement ano = (Announcement) ((IEQTradeApp)apps)
							.getTradingEngine().getStore(TradingStore.DATA_ANNOUNCEMENT)
							.getDataByIndex(i);
						//LocationUrl = "http://172.17.17.14:9110/anno/index.php?id="+ano.getcId();
						LocationUrl = "http://trading.simasnet.com/helpdesk/anno/index.php?id="+ano.getcId();
						webBrowser.navigate(LocationUrl);
						webBrowser.requestFocus();
						
					}
				}
			}
		});		
	}
	Thread threadLoad = null;
	private void jalan(){
		NativeInterface.open();
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
					JPanel webBrowserPanel = new JPanel(new BorderLayout());
					webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
					webBrowser = new JWebBrowser();
					//webBrowser.setVisible(true);
					webBrowser.setBarsVisible(false);
					webBrowser.setStatusBarVisible(true);
					
					int i=table.getMappedRow(table.getSelectedRow());
					if(i>-1){
						Announcement ano = (Announcement) ((IEQTradeApp)apps)
							.getTradingEngine().getStore(TradingStore.DATA_ANNOUNCEMENT)
							.getDataByIndex(i);
						//LocationUrl = "http://172.17.17.14:9110/anno/index.php?id="+ano.getcId();
						LocationUrl = "http://trading.simasnet.com/helpdesk/anno/index.php?id="+ano.getcId();
					webBrowser.navigate(LocationUrl);
					webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
					pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
					}
					
					
			}
		});
	}

	

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
		.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tabel", AnnouncementDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 500, 300));
		
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
		
	}

	@Override
	public void saveSetting() {
		
		hSetting.put("size", new Rectangle(frame.getX(),frame.getY(),  
				frame.getWidth(), frame.getHeight()));
		hSetting.put("tabel", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}
	@Override
	protected void initUI() {
		
		frame = new JSkinDlg(title);
		frame.setResizable(false);
		
		frame.setContent(pnlContent);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});frame.setSize(600,600);
	}
	
	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}

	@Override
	public void show() {
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}
	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}
}
