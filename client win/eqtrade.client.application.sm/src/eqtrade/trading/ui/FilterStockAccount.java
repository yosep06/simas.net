package eqtrade.trading.ui;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.trading.model.Account;
import eqtrade.trading.model.Portfolio;

public class FilterStockAccount extends FilterBase {
	private HashMap mapFilter = new HashMap();
	private GridModel dataPortfolio;
	private Log log = LogFactory.getLog(getClass());
	int ax = 0;
	public FilterStockAccount(Component parent, GridModel _dataPortfolio) {
		super(parent, "filter");
		mapFilter.put("stock", new FilterColumn("stock", String.class,
				new String(""), FilterColumn.C_EQUAL));
		this.dataPortfolio = _dataPortfolio;
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			Account dat = (Account) val.getSource();
			FilterColumn column = (FilterColumn) mapFilter.get("stock");

			if (((String) column.getField()).isEmpty()) {
				avail = true;
			} else {

				Portfolio portfolio = (Portfolio) dataPortfolio
						.getDataByKey(new String[] { dat.getTradingId(),
								(String) column.getField() });

				if (portfolio != null)
					{avail = true; ax++;}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return avail;
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

}
