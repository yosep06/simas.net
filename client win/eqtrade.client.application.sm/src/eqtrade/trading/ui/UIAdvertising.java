package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.vollux.framework.UI;
import com.vollux.ui.CustomMouseAdapter;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.model.GlobalIndices;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.NegDealList;
import eqtrade.trading.model.NegDealListDef;
import eqtrade.trading.model.Order;

public class UIAdvertising extends UI {
	private JGrid table;
	private Action negdealAction;
	private FilterAdvertising filter;

	public UIAdvertising(String app) {
		super("Advertising List", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_NEGDEALLIST);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		filter = new FilterAdvertising(form);
		((FilterColumn) filter.getFilteredData("advertising")).setField("0");

		negdealAction = apps.getAction().get(TradingAction.A_SHOWBUYNEGDEAL);
		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_NEGDEALLIST), filter,
				(Hashtable) hSetting.get("table"));
		table.getTable().add(popupMenu);
		table.getTable().addMouseListener(new MyMouseAdapter());
		table.addMouseListener(new MyCustomMouseAdapter());
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
		});
		table.addMouseListener(new CustomMouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
		});

		refresh();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", NegDealListDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 600, 300));
		
		/*((Hashtable) hSetting.get("table")).put("sortcolumn",
				new Integer(NegDealList.C_MARKETORDERID));*/
		hSetting.put("table", NegDealListDef.getTableDef());
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
		((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_NEGDEALLIST).refresh();
	}

	@Override
	public void show() {
		if (!apps.getAction().get(TradingAction.A_SHOWADVERTISING).isGranted())
			return;
		super.show();
	}

	public Vector getSelected() {
		Vector v = ((CustomSelectionModel) table.getTable().getSelectionModel())
				.getVSelected();
		Vector result = new Vector(v.size());
		for (int i = 0; i < v.size(); i++) {
			result.addElement(((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_NEGDEALLIST)
					.getDataByIndex(
							table.getMappedRow(((Integer) v.elementAt(i))
									.intValue())));
		}
		return result;
	}

	protected boolean isAllTemporary(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			NegDealList o = (NegDealList) v.elementAt(i);
			if (!o.getStatusID().equals(Order.C_TEMPORARY))
				return false;
		}
		return result;
	}

	protected boolean isAllOpen(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			NegDealList o = (NegDealList) v.elementAt(i);
			if (!o.getStatusID().equals(Order.C_OPEN)
					&& !o.getStatusID().equals("0"))
				return false;
		}
		return result;
	}

	protected boolean isValidRow(int x, int y, boolean ctrlDown) {
		return isValidRowMulti(x, y, ctrlDown);
	}

	protected boolean isValidRowMulti(int x, int y, boolean ctrlDown) {
		int nRow = -1;
		String status = "";
		NegDealList order = new NegDealList();
		try {
			JTable tblViewList = table.getTable();
			nRow = tblViewList.rowAtPoint(new Point(x, y));

			if (nRow >= 0) {
				if (ctrlDown) {
					if (isAllTemporary(getSelected())) {
						status = Order.C_TEMPORARY;
					} else if (isAllOpen(getSelected())) {
						status = Order.C_OPEN;
					} else
						status = "";
				} else {
					tblViewList.setRowSelectionInterval(nRow, nRow);
					tblViewList.getSelectionModel().addSelectionInterval(nRow,
							nRow);
					NegDealList data = (NegDealList) table.getDataByIndex(nRow);
					status = data.getStatusID();
					order = data;
				}
			}

			for (int i = 0; i < popupMenu.getComponentCount(); i++) {
				Object tool = popupMenu.getComponent(i);
				if (tool instanceof JMenuItem) {
					if (((JMenuItem) tool).getActionCommand().equals("negdeal")) {
						((JMenuItem) tool)
								.setEnabled((negdealAction != null
										&& negdealAction.isEnabled()
										&& nRow >= 0 && ((order.getStatusID()
										.equals(Order.C_OPEN) || order
										.getStatusID().equals("0")) && order
										.getIsAdvertising().equals("0"))));
					}
				}
			}
			return true;
		} catch (Exception z) {
			return false;
		}
	}

	protected boolean isValidRowSingle(int x, int y) {
		int nRow = -1;
		String status = "";
		NegDealList order = new NegDealList();
		try {
			JTable tblViewList = table.getTable();
			nRow = tblViewList.rowAtPoint(new Point(x, y));

			if (nRow >= 0) {
				tblViewList.setRowSelectionInterval(nRow, nRow);
				NegDealList data = (NegDealList) table.getDataByIndex(nRow);
				status = data.getStatusID();
				order = data;
			}

			for (int i = 0; i < popupMenu.getComponentCount(); i++) {
				Object tool = popupMenu.getComponent(i);
				if (tool instanceof JMenuItem) {
					if (((JMenuItem) tool).getActionCommand()
							.equals("withdraw")) {
						((JMenuItem) tool)
								.setEnabled((negdealAction != null
										&& negdealAction.isEnabled()
										&& nRow >= 0 && ((order.getStatusID()
										.equals(Order.C_OPEN) || order
										.getStatusID().equals("0")) && order
										.getBuySell().equals(Order.C_SELL))));
						((JMenuItem) tool)
								.setVisible((negdealAction != null
										&& negdealAction.isEnabled()
										&& nRow >= 0 && ((order.getStatusID()
										.equals(Order.C_OPEN) || order
										.getStatusID().equals("0")) && order
										.getBuySell().equals(Order.C_SELL))));
					}
				}
			}
			return true;
		} catch (Exception z) {
			return false;
		}
	}

	@Override
	protected boolean isValidRow(int x, int y) {
		return true;
	}

	public class MyMouseAdapter extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) {
				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}
	};

	public class MyCustomMouseAdapter extends CustomMouseAdapter {
		@Override
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) {
				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}
	}

}
