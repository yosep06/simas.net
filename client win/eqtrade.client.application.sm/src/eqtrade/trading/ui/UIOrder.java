package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.util.Hashtable;

import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.UserProfile;

public class UIOrder extends UI {
	boolean sales;
	protected OrderPanel orderPanel;
	@Override
	public void show() {
		UserProfile user = (UserProfile) ((IEQTradeApp) apps).getTradingEngine().getStore(
				TradingStore.DATA_USERPROFILE).getDataByField(
						new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
		if (user.getProfileId().equalsIgnoreCase("CUSTOMER"))	{
			sales = true;
		}else{
			sales = false;
		}
		//System.out.println("test,,22 "+user.getProfileId()+" "+sales+" "+user.getUserId());
		OrderPanel.validnot(sales,user.getUserId());
		super.show();
	}

	public UIOrder(String app) {
		super("Order List", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_ORDER);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				orderPanel.tableOrder.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void build() {
		orderPanel = new OrderPanel(apps, form, hSetting);
		orderPanel.build();
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(orderPanel, BorderLayout.CENTER);
		refresh();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tableOrder", OrderDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 800, 300));

	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("tableOrder", orderPanel.tableOrder.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	@Override
	public void refresh() {
//		orderPanel.refresh();
//		((IEQTradeApp) apps).getTradingEngine()
//				.getStore(TradingStore.DATA_ORDER).refresh();
		//System.out.println("refresh");
		String clientid = orderPanel.comboClient.getText();
		((IEQTradeApp) apps).getTradingEngine().refreshOrder(
				clientid.trim().toUpperCase(), "%","%");
	}

	public void executeAction(String actionName) {
		if (orderPanel != null)
			orderPanel.executeAction(actionName);
	}
	
	public void success() {
		orderPanel.success();
	}
	
	public void failed(String status) {
		orderPanel.failed(status);
	}
}
