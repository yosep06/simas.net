package eqtrade.trading.ui;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.trading.model.Portfolio;

public class FilterPortfolio extends FilterBase {
	private HashMap mapFilter = new HashMap();

	public FilterPortfolio(Component parent) {
		super(parent, "filter");
		mapFilter.put("account", new FilterColumn("account", String.class,
				new String(""), FilterColumn.C_EQUAL));
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public int showDialog() {
		return super.showDialog();
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			Portfolio dat = (Portfolio) val.getSource();
			if (((FilterColumn) mapFilter.get("account")).compare(dat
					.getTradingId())) {
				avail = true;
			}
			return avail;
		} catch (Exception ec) {
			return false;
		}
	}
}