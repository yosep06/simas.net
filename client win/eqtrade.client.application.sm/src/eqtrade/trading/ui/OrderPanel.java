package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.sun.security.auth.UserPrincipal;
import com.vollux.demo.Mapping;
import com.vollux.framework.IApp;
import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.model.TableModelFilter;
import com.vollux.ui.CustomMouseAdapter;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinForm;
import com.vollux.ui.JTextLabel;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.ITradingApp;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.OrderIDGenerator;
import eqtrade.trading.engine.TradingEngine;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Notif;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.OrderRender;
import eqtrade.trading.model.OrderStatus;
import eqtrade.trading.model.Stock;
import eqtrade.trading.model.UserProfile;

public class OrderPanel extends JPanel {
	protected final Log log = LogFactory.getLog(getClass());
	JGrid tableOrder;
	FilterOrder filterOrder;
	boolean isLoadingSuperUser = false;
	Hashtable hSetting;
	JDropDown comboClient;
	JDropDown comboStock;
	JDropDown comboBOS;
	JDropDown comboStatus;
	JDropDown comboTaker;
	JDropDown comboBoard;
	JDropDown comboFloor;
	JNumber price;
	JButton btnClear;
	JButton btnView;
	static JButton btnNotif;

	Action amendAction;
	Action withdrawAction;
	Action splitAction;
	Action negdealAction;

	JMenuItem withdrawMenu;
	JMenuItem amendMenu;
	JMenuItem propertiesMenu;
	JMenuItem splitMenu;
	JMenuItem resendMenu;
	JMenuItem printExcelMenu;

	JNumber tCount;
	JNumber oCount;
	JNumber oValue;
	JNumber dCount;
	JNumber dValue;
	JNumber tLot;
	JNumber tValue;
	JPopupMenu popupMenu;
	Vector filterclient = null;
	String client = "", sfilterClient = "", sfilterStock = "";
	static String usernotif = "";

	CustomSelectionModel customSelection;
	IApp apps;
	JSkinForm form;
	
	public OrderPanel(IApp apps, JSkinForm form, Hashtable hSetting) {
		super(new BorderLayout());
		this.apps = apps;
		this.form = form;
		this.hSetting = hSetting;
	}

	protected JGrid createTable(GridModel data, TableModelFilter tmf, Hashtable hsetting) {
		JGrid temptable = new JGrid(data, tmf,
				(String[]) hsetting.get("header"),
				(int[]) hsetting.get("order"), (int[]) hsetting.get("width"));
		temptable.setSortColumn(((Integer) hsetting.get("sortcolumn"))
				.intValue());
		temptable.setSortAscending(((Boolean) hsetting.get("sortascending"))
				.booleanValue());
		// table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		temptable.getTable().setRowSelectionAllowed(false);
		temptable.getTable().setShowGrid(false);
		temptable.getTable().setIntercellSpacing(new Dimension(0, 0));
		temptable.setAlignment((int[]) hsetting.get("alignment"));
		temptable.getTable().setRowSelectionAllowed(true);
		if (hsetting.get("font") != null)
			temptable.setTableFont((Font) hsetting.get("font"));
		temptable.setColumnSortedProperties((boolean[]) hsetting.get("sorted"));
		return temptable;
	}

	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				tableOrder.getTable().requestFocus();
			}
		});
	}

	void infoNotif() {
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		String user = sales.getUserId();
		usernotif = user;
	}

	void initData() {
		infoNotif();
	}

	protected void build() {
		
		System.err.println("isallow : " + isAllowAdvertising());
		
		//System.err.println("is Sales : " + isSalesAdvertising());
		
		createPopup();
		
		amendAction = apps.getAction().get(TradingAction.A_SHOWAMENDORDER);
		withdrawAction = apps.getAction().get(TradingAction.A_SHOWWITHDRAWORDER);
		splitAction = apps.getAction().get(TradingAction.A_SHOWSPLITORDER);
		negdealAction = apps.getAction().get(TradingAction.A_SHOWNEGDEAL);
		loadData();
		filterOrder = new FilterOrder(this);
		((FilterColumn) filterOrder.getFilteredData("taker")).setField("0");
		((FilterColumn) filterOrder.getFilteredData("user")).setField(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase());
		comboClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboStock = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		comboBOS = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_SIDE), Mapping.CIDX_CODE, false);
		comboStatus = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERSTATUS), OrderStatus.C_NAME,false);
		comboTaker = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_TAKER), OrderStatus.C_ID, false);
		comboBoard = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_BOARDFILTER), Mapping.CIDX_NAME,false);
		comboFloor = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_FLOORFILTER), Mapping.CIDX_NAME,false);
		price = new JNumber(Double.class, 0, 0, false, true);
		btnClear = new JButton("Clear");
		btnClear.setMnemonic('R');
		btnView = new JButton("View");
		btnView.setMnemonic('V');
		btnNotif = new JButton("Notification Setup");
		btnNotif.setMnemonic('N');

		registerEvent(comboClient.getTextEditor());
		registerEventCombo(comboClient);

		registerEvent(comboStock.getTextEditor());
		registerEventCombo(comboStock);

		registerEvent(comboBOS);
		registerEvent(comboStatus);
		registerEvent(comboTaker);
		registerEvent(comboBoard);
		registerEvent(comboFloor);
		registerEvent(price);
		registerEvent(btnClear);
		registerEvent(btnView);
		
		btnNotif.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						HashMap param = new HashMap();
						param.put("CLIENT", usernotif);
						apps.getUI().showUI(TradingUI.UI_NOTIFICATION, param);
					}
				});
			}
		});
		
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboClient.getTextEditor().setText("");
				comboStock.getTextEditor().setText("");
				comboBOS.setSelectedIndex(0);
				comboStatus.setSelectedIndex(0);
				comboTaker.setSelectedIndex(0);
				comboBoard.setSelectedIndex(0);
				comboFloor.setSelectedIndex(1);
				price.setValue(null);
				((FilterColumn) filterOrder.getFilteredData("account")).setField(null);
				((FilterColumn) filterOrder.getFilteredData("stock")).setField(null);
				((FilterColumn) filterOrder.getFilteredData("bos")).setField(null);
				((FilterColumn) filterOrder.getFilteredData("status")).setField(null);
				((FilterColumn) filterOrder.getFilteredData("taker")).setField("0");
				((FilterColumn) filterOrder.getFilteredData("board")).setField(null);
				((FilterColumn) filterOrder.getFilteredData("price")).setField(null);
				((FilterColumn) filterOrder.getFilteredData("floor")).setField("0");
				((FilterColumn) filterOrder.getFilteredData("accountv")).setField(filterclient);
				filterOrder.fireFilterChanged();
			}
		});
		
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String clientid = comboClient.getText();
				String secid = comboStock.getText();
				if (clientid != null && !clientid.isEmpty()) {
					setState(true);
					((IEQTradeApp) apps).getTradingEngine().refreshOrder(
							clientid.trim().toUpperCase(), "%","%");
					((FilterColumn) filterOrder.getFilteredData("accountv"))
					.setField(null);
				}else if(secid!=null && !secid.isEmpty() && isSuperUser()){
					setState(true);
					((IEQTradeApp) apps).getTradingEngine().refreshOrder(
							"%", "%",secid.trim().toUpperCase());
					((FilterColumn) filterOrder.getFilteredData("accountv"))
					.setField(null);
				}else{
					((FilterColumn) filterOrder.getFilteredData("accountv"))
							.setField(filterclient);
				}

				if (isSuperUser()) {
					if (!sfilterClient.isEmpty()
							&& !isFilterClientAdded(sfilterClient)) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByClient(sfilterClient);
					}

					String accid = getAccountId(clientid);

					if (accid != null && !accid.isEmpty()) {

						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByClient(accid);
						sfilterClient = accid;
					}

					if (!sfilterStock.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByStock(sfilterStock);

					}

					String stockid = comboStock.getText();

					if (stockid != null && !stockid.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByStock(stockid.toUpperCase());
						sfilterStock = stockid.toUpperCase();
					}

				}

				filterOrder.fireFilterChanged();
			}

		});
		comboClient.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboClient.showPopup();
				} catch (Exception ex) {
				}
			}

			public void focusLost(FocusEvent e) {
				String secid = comboStock.getText();
				String clientid = comboClient.getText();
				if (isSuperUser()) {
					if( clientid != null && !clientid.isEmpty()){
						setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshOrder(
								clientid.trim().toUpperCase(), "%","%");
						((FilterColumn) filterOrder.getFilteredData("accountv"))
						.setField(null);
					}else if (secid!=null && !secid.isEmpty() && clientid != null && !clientid.isEmpty()) {
						setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshOrder(
								clientid.trim().toUpperCase(), "%",secid.trim().toUpperCase());
						((FilterColumn) filterOrder.getFilteredData("accountv"))
						.setField(null);
					} else if (secid!=null && !secid.isEmpty()) {
						
					} else {
						((FilterColumn) filterOrder.getFilteredData("accountv"))
								.setField(filterclient);
					}
					if (!sfilterClient.isEmpty()
							&& !isFilterClientAdded(sfilterClient)) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByClient(sfilterClient);
					}

					String accid = getAccountId(clientid);

					if (accid != null && !accid.isEmpty()) {

						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByClient(accid);
						sfilterClient = accid;
					}

					if (!sfilterStock.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByStock(sfilterStock);

					}

					String stockid = comboStock.getText();

					if (stockid != null && !stockid.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByStock(stockid.toUpperCase());
						sfilterStock = stockid.toUpperCase();
					}
				} else {

				}
				((FilterColumn) filterOrder.getFilteredData("account"))
						.setField(comboClient.getText().trim().equals("") ? null
								: comboClient.getText().trim().toUpperCase());
				filterOrder.fireFilterChanged();
			}
		});
		comboStock.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboStock.showPopup();
				} catch (Exception ex) {
				}
			}

			public void focusLost(FocusEvent e) {

				String secid = comboStock.getText();
				String clientid = comboClient.getText();
				if (isSuperUser()) {
					if(secid!=null && !secid.isEmpty()){
						setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshOrder(
								"%", "%",secid.trim().toUpperCase());
						((FilterColumn) filterOrder.getFilteredData("accountv"))
						.setField(null);
					}else if (secid!=null && !secid.isEmpty() && clientid != null && !clientid.isEmpty()) {
						setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshOrder(
								clientid.trim().toUpperCase(), "%",secid.trim().toUpperCase());
						((FilterColumn) filterOrder.getFilteredData("accountv"))
						.setField(null);
					} else if (clientid != null && !clientid.isEmpty()) {
						
					} else {
						((FilterColumn) filterOrder.getFilteredData("accountv"))
								.setField(filterclient);
					}
					if (!sfilterClient.isEmpty()
							&& !isFilterClientAdded(sfilterClient)) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByClient(sfilterClient);
					}

					String accid = getAccountId(clientid);

					if (accid != null && !accid.isEmpty()) {

						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByClient(accid);
						sfilterClient = accid;
					}

					if (!sfilterStock.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByStock(sfilterStock);

					}

					String stockid = comboStock.getText();

					if (stockid != null && !stockid.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByStock(stockid.toUpperCase());
						sfilterStock = stockid.toUpperCase();
					}
				} else {

				}
				((FilterColumn) filterOrder.getFilteredData("stock"))
						.setField(comboStock.getText().trim().equals("") ? null
								: comboStock.getText().trim().toUpperCase());
				filterOrder.fireFilterChanged();
			}
		});
		comboBOS.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("bos"))
						.setField(comboBOS.getSelectedIndex() == 0 ? null
								: getBOSCode());
				filterOrder.fireFilterChanged();
			}
		});
		comboStatus.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("status"))
						.setField(comboStatus.getSelectedIndex() == 0 ? null
								: getStatusCode());
				filterOrder.fireFilterChanged();
			}
		});
		comboBoard.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("board"))
						.setField(comboBoard.getSelectedIndex() == 0 ? null
								: getBoardCode());
				filterOrder.fireFilterChanged();
			}
		});
		comboFloor.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("floor"))
						.setField(comboFloor.getSelectedIndex() == 0 ? null
								: getFloorCode());
				filterOrder.fireFilterChanged();
			}
		});
		price.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				try {
					price.commitEdit();
				} catch (Exception ex) {
				}
				((FilterColumn) filterOrder.getFilteredData("price"))
						.setField(price.getValue());
				filterOrder.fireFilterChanged();
			}
		});
		comboTaker.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("taker"))
						.setField(comboTaker.getSelectedIndex() + "");
				filterOrder.fireFilterChanged();
			}
		});

		JPanel editorPanel = new JPanel();
		FormLayout layout = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,75px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, 100px, 2dlu, pref,2dlu,pref,2dlu,pref",
				"pref");
		PanelBuilder builder = new PanelBuilder(layout, editorPanel);
		builder.setBorder(new EmptyBorder(4, 4, 4, 4));
		CellConstraints cc = new CellConstraints();
		builder.add(new JTextLabel("Client", 'C', comboClient.getTextEditor()),
				cc.xy(1, 1));
		builder.add(comboClient, cc.xy(3, 1));
		builder.add(new JTextLabel("Stock", 'S', comboStock.getTextEditor()),
				cc.xy(5, 1));
		builder.add(comboStock, cc.xy(7, 1));
		builder.add(new JTextLabel("Price", 'P', price), cc.xy(9, 1));
		builder.add(price, cc.xy(11, 1));
		builder.add(new JTextLabel("B/S", 'B', comboBOS), cc.xy(13, 1));
		builder.add(comboBOS, cc.xy(15, 1));
		builder.add(new JTextLabel("Board", 'R', comboBoard), cc.xy(17, 1));
		builder.add(comboBoard, cc.xy(19, 1));
		builder.add(new JTextLabel("Entry", 'R', comboFloor), cc.xy(21, 1));
		builder.add(comboFloor, cc.xy(23, 1));
		builder.add(new JTextLabel("Status", 'T', comboStatus), cc.xy(25, 1));
		builder.add(comboStatus, cc.xy(27, 1));
		builder.add(comboTaker, cc.xy(29, 1));
		builder.add(btnView, cc.xy(31, 1));
		builder.add(btnClear, cc.xy(33, 1));
		builder.add(btnNotif, cc.xy(35, 1));

		((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER).addTableModelListener(new RowUpdate());
		tableOrder = createTable(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER), filterOrder,(Hashtable) hSetting.get("tableOrder"));
		tableOrder.getTable().add(popupMenu);

		tableOrder.getTable().addMouseListener(new MyMouseAdapter());
		tableOrder.addMouseListener(new MyCustomMouseAdapter());
		tableOrder.setColumnHide(OrderDef.columnhide);
		customSelection = new CustomSelectionModel();
		customSelection.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tableOrder.getTable().setSelectionModel(customSelection);
		tableOrder.getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		
		tableOrder.getTable().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 1 && e.getClickCount() == 2 && isValidRow(e.getX(), e.getY(), false)) {
					
					JTable tblViewList = tableOrder.getTable();
					int nRow = tblViewList.rowAtPoint(new Point(e.getX(), e.getY()));

					if (nRow >= 0) {
						tblViewList.setRowSelectionInterval(nRow, nRow);
						tblViewList.getSelectionModel().addSelectionInterval(
								nRow, nRow);
						Order data = (Order) tableOrder.getDataByIndex(nRow);

						if (data.getStatus().equals(Order.C_FULL_MATCH)
								|| data.getStatus().equals(
										Order.C_PARTIAL_MATCH)) {
							apps.getUI().showUI(TradingUI.UI_TRADE);
							UITrade tradelist = (UITrade) apps.getUI().getForm(
									TradingUI.UI_TRADE);
							tradelist.setFilteredMarket(data.getMarketOrderId());

						}
					}

				} else if (e.isPopupTrigger()) {

					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						//log.info("popup");
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						//log.info("popup2");
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
		});
		tableOrder.addMouseListener(new CustomMouseAdapter() {
			public void mousePressed(MouseEvent e) {

				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						//log.info("popup3");
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {

					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						//log.info("popup4");
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
		});

		registerEvent(tableOrder);
		registerEvent(tableOrder.getTable());
		tableOrder.getTable().getModel().addTableModelListener(new RowChanged());
		tableOrder.getTable().getModel().addTableModelListener(new RowChangeList());

		tCount = new JNumber(Double.class, 0, 0, false, false);
		oCount = new JNumber(Double.class, 0, 0, false, false);
		oValue = new JNumber(Double.class, 0, 0, false, false);
		dCount = new JNumber(Double.class, 0, 0, false, false);
		dValue = new JNumber(Double.class, 0, 0, false, false);
		tLot = new JNumber(Double.class, 0, 0, false, false);
		tValue = new JNumber(Double.class, 0, 0, false, false);
		JPanel infoPanel = new JPanel();
		FormLayout infLayout = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,80px, 2dlu, pref, 2dlu, 110px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 110px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 110px",
				"pref");
		PanelBuilder infBuilder = new PanelBuilder(infLayout, infoPanel);
		infBuilder.setBorder(new EmptyBorder(4, 4, 4, 4));

		infBuilder.add(new JLabel("Count"), cc.xy(1, 1));
		infBuilder.add(tCount, cc.xy(3, 1));
		infBuilder.add(new JLabel("Open Count"), cc.xy(5, 1));
		infBuilder.add(oCount, cc.xy(7, 1));
		infBuilder.add(new JLabel("Open Value"), cc.xy(9, 1));
		infBuilder.add(oValue, cc.xy(11, 1));
		infBuilder.add(new JLabel("Done Count"), cc.xy(13, 1));
		infBuilder.add(dCount, cc.xy(15, 1));
		infBuilder.add(new JLabel("Done Value"), cc.xy(17, 1));
		infBuilder.add(dValue, cc.xy(19, 1));
		infBuilder.add(new JLabel("T.Lot"), cc.xy(21, 1));
		infBuilder.add(tLot, cc.xy(23, 1));
		infBuilder.add(new JLabel("T.Value"), cc.xy(25, 1));
		infBuilder.add(tValue, cc.xy(27, 1));

		this.setBorder(new EmptyBorder(2, 2, 2, 2));
		JScrollPane t = new JScrollPane(editorPanel);
		t.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		t.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.add(t, BorderLayout.NORTH);
		this.add(tableOrder, BorderLayout.CENTER);

		JScrollPane b = new JScrollPane(infoPanel);
		b.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		b.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		this.add(b, BorderLayout.PAGE_END);
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		if (!sales.getProfileId().equalsIgnoreCase("CUSTOMER")) {
			btnNotif.setEnabled(false);
		}
		try {
			comboBOS.setSelectedIndex(0);
			comboStatus.setSelectedIndex(0);
			comboTaker.setSelectedIndex(0);
			comboBoard.setSelectedIndex(0);
			comboFloor.setSelectedIndex(0);
			price.setValue(null);
		} catch (Exception ex) {
		}
		refresh();
		initData();
	}

	private void registerEventCombo(JDropDown comp) {
		InputMap inputMap = comp.getTextEditor().getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "enterAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap().put("enterAction", new AbstractAction("enterAction") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {

				if (e.getSource().equals(comboStock.getTextEditor())) {
					comboStock.setSelectedIndex(comboStock
							.getSelectedIndex());
					comboStock.hidePopup();
					price.requestFocus();
				} else if (e.getSource().equals(
						comboClient.getTextEditor())) {
					comboClient.setSelectedIndex(comboClient
							.getSelectedIndex());
					comboClient.hidePopup();
					comboStock.requestFocus();
				}

				// comboStock.requestFocus();

			}

		});
	}

	public void refresh() {
//		System.out.println("refresh");
		tableOrder.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tableOrder.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tableOrder.setNewFont(TradingSetting.getFont());
		((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ORDER).refresh();
	}

	public void setState(boolean state) {
		btnView.setEnabled(state);
		btnClear.setEnabled(state);
		isLoadingSuperUser = !state;
//		System.out.println("isloadingsuperuser1 "+isLoadingSuperUser);
	}

	public void success() {
//		System.out.println("isloadingsuperuser2 "+isLoadingSuperUser);
		setState(true);
		if (isLoadingSuperUser) {

			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					setState(true);
					((FilterColumn) filterOrder.getFilteredData("account"))
							.setField(comboClient.getText().trim().equals("") ? null
									: comboClient.getText().trim()
											.toUpperCase());
					filterOrder.fireFilterChanged();
				}
			});
		}
	}

	public void failed(final String reason) {
		if (isLoadingSuperUser) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					setState(true);
					Utils.showMessage(reason, null);
				}
			});
		}

	}

	String getBOSCode() {
		Mapping m = (Mapping) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_SIDE)
				.getDataByKey(new Object[] { comboBOS.getSelectedItem() });
		return (m != null) ? m.getName() : null;
	}

	Vector getStatusCode() {
		Vector v = new Vector();
		if (comboStatus.getSelectedIndex() == 0) {
			return null;
		} else {
			OrderStatus os = (OrderStatus) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ORDERSTATUS)
					.getDataByField(
							new Object[] { comboStatus.getSelectedItem() },
							new int[] { OrderStatus.C_NAME });
			if (os != null) {
				Utils.parser(os.getId(), "|", v);
			}
			return v;
		}
	}

	String getFloorCode() {
		Mapping m = (Mapping) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_FLOORFILTER)
				.getDataByField(new Object[] { comboFloor.getSelectedItem() },
						new int[] { Mapping.CIDX_NAME });
		return (m != null) ? m.getCode() : null;
	}

	Vector getBoardCode() {
		Vector v = new Vector();
		if (comboBoard.getSelectedIndex() == 0) {
			return null;
		} else {
			Mapping m = (Mapping) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_BOARDFILTER).getDataByField(new Object[] { comboBoard.getSelectedItem() }, new int[] { Mapping.CIDX_NAME });
			if (m != null) {
				Utils.parser(m.getCode(), "|", v);
			}
			return v;
		}
	}

	void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "secAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "clientAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0), "withdrawAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0), "amendAction");
		inputMap.put(KeyStroke.getKeyStroke("alt W"), "withdrawAction");
		inputMap.put(KeyStroke.getKeyStroke("alt A"), "amendAction");
		inputMap.put(KeyStroke.getKeyStroke("alt I"), "splitAction");
		
		comp.getActionMap().put("amendAction", new AbstractAction("amendAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				if (amendAction == null || !amendAction.isEnabled())
					return;
				Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
				if (order == null) {
					Utils.showMessage("Please select order to amend", form);
				} else {
//					if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
//						Utils.showMessage("access denied, please contact your helpdesk", form);
//						return;
//					}						
					if(order.getBoard().equals("NG")) {
						if(isValidUserProfile().equalsIgnoreCase("SALES") && order.getIsAdvertising().equals("0")) {
							Utils.showMessage("access denied, please contact your helpdesk", form);
							return;											
						} else if(isValidUserProfile().equalsIgnoreCase("CUSTOMER")) {
							Utils.showMessage("access denied, please contact your helpdesk", form);
							return;
						}
					}
					if ((order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH)) //17112014gtc 	{
							&& !order.getOrderType().equals("G")) {
						if (amendAction != null)//gtc
							Global.isOrder = 1;
							amendAction.actionPerformed(new ActionEvent(order, 0, "OrderPanel"));
					}
				}
			}
		});
		
		comp.getActionMap().put("withdrawAction", new AbstractAction("withdrawAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				/*Thread thread = new Thread() {
					@Override
					public void run() {*/
						Vector v = getSelected();
						StringBuffer info = new StringBuffer("");
						final Vector vorder = new Vector();
						if (v.size() == 1) {
							Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
							if (order == null) {
								Utils.showMessage("Please select order to withdraw",form);
								return;
							}
//							if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
//								Utils.showMessage("access denied, please contact your helpdesk",form);
//								return;
//							}
							if(order.getBoard().equals("NG")) {
								if(isValidUserProfile().equalsIgnoreCase("SALES") && order.getIsAdvertising().equals("0")) {
									Utils.showMessage("access denied, please contact your helpdesk", form);
									return;											
								} else if(isValidUserProfile().equalsIgnoreCase("CUSTOMER")) {
									Utils.showMessage("access denied, please contact your helpdesk", form);
									return;
								}
							}
							if (order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH) ||  (order.getStatus().equals(Order.C_ENTRY) && order.getIsBasketOrder().equals("1"))) {
								info.append("are you sure want to withdraw this order?\n");
								info.append(genConfirm(order));
								vorder.add(order);
							}
						} else {									
							if (v.size() <= 20) {
								info.append("are you sure want to withdraw these order?\n");
								for (int i = 0; i < v.size(); i++) {
									Order order = (Order) v.elementAt(i);
//									if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
//										Utils.showMessage("access denied, please contact your helpdesk",form);
//										return;
//									}
									if(order.getBoard().equals("NG")) {
										if(isValidUserProfile().equalsIgnoreCase("SALES") && order.getIsAdvertising().equals("0")) {
											Utils.showMessage("access denied, please contact your helpdesk", form);
											return;											
										} else if(isValidUserProfile().equalsIgnoreCase("CUSTOMER")) {
											Utils.showMessage("access denied, please contact your helpdesk", form);
											return;
										}
									}
									if (order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH) ||  (order.getStatus().equals(Order.C_ENTRY) && order.getIsBasketOrder().equals("1"))) {
										info = info.append(genConfirm(order));
										vorder.add(order);
									}
								}
							} else {
								vorder.addAll(v);
								info.append("are you sure want to withdraw all selected order (" + v.size() + " records) ?\n");
							}
						}

						if (vorder.size() > 0) {
							int nconfirm = Utils.C_YES_DLG;
							nconfirm = Utils.showConfirmDialog(form, "Withdraw Order", info.toString(), Utils.C_YES_DLG);
							if (nconfirm == Utils.C_YES_DLG) {
								((IEQTradeApp) apps).getTradingEngine().withdrawOrderMulti(vorder);
								try {
									int rows = tableOrder.getSelectedRow();
									if (tableOrder.getTable().getRowCount() == 1) rows = 0;
									if (rows >= 0) tableOrder.getTable().setRowSelectionInterval(rows, rows);
								} catch (Exception ex) {
								}
							}
						}
				/*	}
				};

				thread.start();	*/					
			}
		});

		comp.getActionMap().put("splitAction", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
				if (order == null) {
					Utils.showMessage("Please select order to split", form);
				} else {
					if ((order.getStatus().equals(Order.C_FULL_MATCH) && order.getSplitBy().equals("")) || ((order.getStatus().equals(Order.C_AMEND) || order.getStatus().equals(Order.C_WITHDRAW)) && order.getDoneVol().doubleValue() > 0)) {
						if (splitAction != null)
							splitAction.actionPerformed(new ActionEvent(order, 0, ""));
					}
				}
			}
		});
		
		comp.getActionMap().put("secAction", new AbstractAction("secAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				comboStock.getTextEditor().requestFocus();
			}
		});
		
		comp.getActionMap().put("clientAction", new AbstractAction("clientAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				comboClient.getTextEditor().requestFocus();
			}
		});
		
		comp.getActionMap().put("escapeAction", new AbstractAction("escapeAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				form.setVisible(false);
			}
		});

		comp.getActionMap().put("enterAction", new AbstractAction("enterAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				if (evt.getSource() instanceof JButton) {
					((JButton) evt.getSource()).doClick();
				} else {
					if (evt.getSource() instanceof JNumber) {
						try {
							((JNumber) evt.getSource()).commitEdit();
						} catch (Exception ex) {
						}
					}
				}
			}
		});
	}

	protected boolean _withdrawMenu = false;
	protected boolean _sendMenu = false;
	protected boolean _deleteMenu = false;
	protected boolean _splitMenu = false;

	protected void createPopup() {
		popupMenu = new JPopupMenu();
		resendMenu = new JMenuItem(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Vector v = getSelected();
					StringBuffer info = new StringBuffer("");
					if (v.size() == 1) {
						Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
						if (order == null) {
							Utils.showMessage("Please select order to resend", form);
							return;
						}
//						if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
//							Utils.showMessage("access denied, please contact your helpdesk", form);
//							return;
//						}	
						if(order.getBoard().equals("NG")) {
							if(isValidUserProfile().equalsIgnoreCase("SALES") && order.getIsAdvertising().equals("0")) {
								Utils.showMessage("access denied, please contact your helpdesk", form);
								return;											
							} else if(isValidUserProfile().equalsIgnoreCase("CUSTOMER")) {
								Utils.showMessage("access denied, please contact your helpdesk", form);
								return;
							}
						}
						if (order.getStatus().equals(Order.C_REJECTED) || order.getStatus().equals(Order.C_REJECTED2)) {
							info.append("are you sure want to resend this order?\n");
							info.append(genConfirm(order));
						}
					} else {
						if (v.size() <= 20) {
							info.append("are you sure want to resend these order?\n");
							for (int i = 0; i < v.size(); i++) {
								Order order = (Order) v.elementAt(i);
//								if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
//									Utils.showMessage("access denied, please contact your helpdesk", form);
//									return;
//								}
								if(order.getBoard().equals("NG")) {
									if(isValidUserProfile().equalsIgnoreCase("SALES") && order.getIsAdvertising().equals("0")) {
										Utils.showMessage("access denied, please contact your helpdesk", form);
										return;											
									} else if(isValidUserProfile().equalsIgnoreCase("CUSTOMER")) {
										Utils.showMessage("access denied, please contact your helpdesk", form);
										return;
									}
								}
								if (order.getStatus().equals(Order.C_REJECTED) || order.getStatus().equals(Order.C_REJECTED2)) {
									info = info.append(genConfirm(order));
								}
							}
						} else {
							info.append("are you sure want to resend all selected order (" + v.size() + " records) ?\n");
						}
					}

					int nconfirm = Utils.C_YES_DLG;
					nconfirm = Utils.showConfirmDialog(form, "Resend Order", info.toString(), Utils.C_YES_DLG);
					if (nconfirm == Utils.C_YES_DLG) {
						if (v.size() == 1) {
							Order orderCopy = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
							Order order = genOrder(orderCopy);
							if (orderCopy.getStatus().equals(Order.C_REJECTED) || orderCopy.getStatus().equals(Order.C_REJECTED2)) {
								((IEQTradeApp) apps).getTradingEngine().createOrder(order);
							}
						} else if (v.size() > 1) {
							final Vector vrow = new Vector((int) v.size());
							final Vector vorder = new Vector((int) v.size());
							for (int i = 0; i < v.size(); i++) {
								Order orderCopy = (Order) v.elementAt(i);
								Order order = genOrder(orderCopy);
								if (orderCopy.getStatus().equals(Order.C_REJECTED) || orderCopy.getStatus().equals(Order.C_REJECTED2)) {
									vrow.addElement(OrderDef.createTableRow(order));
									vorder.addElement(order);
								}
							}
							((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER).addRow(vrow, false, false);
							((IEQTradeApp) apps).getTradingEngine().createOrderMulti(vorder);
							((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER).refresh();

						}
						try {
							int rows = tableOrder.getSelectedRow();
							if (tableOrder.getTable().getRowCount() == 1)
								rows = 0;
							if (rows >= 0)
								tableOrder.getTable().setRowSelectionInterval(rows, rows);
						} catch (Exception ex) {
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					Utils.logException(ex);
				}
			}
		});
		
		resendMenu.setText("Resend");
		resendMenu.setActionCommand("resend");

		printExcelMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				savetoFile();
			}
		});

		printExcelMenu.setText("save to excel");
		printExcelMenu.setActionCommand("printExcel");

		withdrawMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				try {
					if (withdrawAction == null || !withdrawAction.isEnabled())
						return;
					/*if (!_withdrawMenu) {
						_withdrawMenu = true;*/
						Vector v = getSelected();
						StringBuffer info = new StringBuffer("");
						if (v.size() == 1) {
							Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
							if (order == null) {
								Utils.showMessage("Please select order to withdraw", form);
								return;
							}
							if(order.getBoard().equals("NG")) {
								if(isValidUserProfile().equalsIgnoreCase("SALES") && order.getIsAdvertising().equals("0")) {
									Utils.showMessage("access denied, please contact your helpdesk", form);
									return;											
								} else if(isValidUserProfile().equalsIgnoreCase("CUSTOMER")) {
									Utils.showMessage("access denied, please contact your helpdesk", form);
									return;
								}
							}
							/*if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
								Utils.showMessage("access denied, please contact your helpdesk", form);
								return;
							}*/
							if (order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH) || (order.getStatus().equals(Order.C_ENTRY) && order.getIsBasketOrder().equals("1"))) {
								info.append("are you sure want to withdraw this order?\n");
								info.append(genConfirm(order));
							}
						} else {
							if (v.size() <= 20) {
								info.append("are you sure want to withdraw these order?\n");
								for (int i = 0; i < v.size(); i++) {
									Order order = (Order) v.elementAt(i);
//									if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
//										Utils.showMessage("access denied, please contact your helpdesk", form);
//										return;
//									}
									if(order.getBoard().equals("NG")) {
										if(isValidUserProfile().equalsIgnoreCase("SALES") && order.getIsAdvertising().equals("0")) {
											Utils.showMessage("access denied, please contact your helpdesk", form);
											return;											
										} else if(isValidUserProfile().equalsIgnoreCase("CUSTOMER")) {
											Utils.showMessage("access denied, please contact your helpdesk", form);
											return;
										}
									}
									if (order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH) || (order.getStatus().equals(Order.C_ENTRY) && order.getIsBasketOrder().equals("1"))) {
										info = info.append(genConfirm(order));
									}
								}
							} else {
								info.append("are you sure want to withdraw all selected order (" + v.size() + " records) ?\n");
							}
						}
						int nconfirm = Utils.C_YES_DLG;
						nconfirm = Utils.showConfirmDialog(form, "Withdraw Order", info.toString(), Utils.C_YES_DLG);
						if (nconfirm == Utils.C_YES_DLG) {
							final Vector vorder = new Vector(v.size());
							for (int i = 0; i < v.size(); i++) {
								Order order = (Order) v.elementAt(i);
								if (order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH) || (order.getStatus().equals(Order.C_ENTRY) && order.getIsBasketOrder().equals("1"))) {
									vorder.add(order);
								}
							}
							((IEQTradeApp) apps).getTradingEngine().withdrawOrderMulti(vorder);
							try {
								int rows = tableOrder.getSelectedRow();
								if (tableOrder.getTable().getRowCount() == 1)
									rows = 0;
								if (rows >= 0)
									tableOrder.getTable().setRowSelectionInterval(rows, rows);
							} catch (Exception ex) {
							}
						}
						/*_withdrawMenu = false;
					}*/
				} catch (Exception ex) {
					ex.printStackTrace();
					Utils.logException(ex);
					_withdrawMenu = false;
				}
			}
		});		
		withdrawMenu.setText("Witdraw");
		withdrawMenu.setActionCommand("withdraw");

		amendMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				if (amendAction == null || !amendAction.isEnabled())
					return;
				Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
				if (order == null) {
					Utils.showMessage("Please select order to amend", form);
				} else {
					if(order.getBoard().equals("NG")) {
						if(isValidUserProfile().equalsIgnoreCase("SALES") && order.getIsAdvertising().equals("0")) {
							Utils.showMessage("access denied, please contact your helpdesk", form);
							return;											
						} else if(isValidUserProfile().equalsIgnoreCase("CUSTOMER")) {
							Utils.showMessage("access denied, please contact your helpdesk", form);
							return;
						}
					}
//					if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
//						Utils.showMessage("access denied, please contact your helpdesk", form);
//						return;
//					}					
					if ((order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH))  //17112014gtc{
						&&!order.getOrderType().equals("G")){
						if (amendAction != null)//gtc
							Global.isOrder = 1;
							amendAction.actionPerformed(new ActionEvent(order, 0, "OrderPanel"));
					}
				}
			}
		});
		amendMenu.setText("Amend");
		amendMenu.setActionCommand("amend");

		splitMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				if (splitAction == null || !splitAction.isEnabled())
					return;
				Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
				if (order == null) {
					Utils.showMessage("Please select order to split", form);
				} else {
				//	log.info("user selected split from popup menu, orderid: "	+ order.getId());
					if ((order.getStatus().equals(Order.C_FULL_MATCH) && order
							.getSplitBy().equals(""))
							|| ((order.getStatus().equals(Order.C_AMEND) || order
									.getStatus().equals(Order.C_WITHDRAW)) && order
									.getDoneVol().doubleValue() > 0)) {
						if (splitAction != null)
							splitAction.actionPerformed(new ActionEvent(order,
									0, ""));
					}
				}
			}
		});
		splitMenu.setText("Split Trade");
		splitMenu.setActionCommand("split");

		propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				tableOrder.showProperties();
			}
		});
		propertiesMenu.setText("Properties");

		popupMenu.add(resendMenu);
		popupMenu.add(withdrawMenu);
		popupMenu.add(amendMenu);

		popupMenu.addSeparator();
		popupMenu.add(splitMenu);
		popupMenu.addSeparator();
		popupMenu.add(propertiesMenu);
		popupMenu.add(printExcelMenu);
	}

	private Order genOrder(Order orderCopy) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByKey(
						new String[] { getAccountId(orderCopy.getTradingId()) });
		Stock sec = (Stock) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK)
				.getDataByKey(new String[] { orderCopy.getStock() });
		Order order = new Order();
		order.setOrderIDContra("");
		order.setId(OrderIDGenerator.gen(acc.getTradingId()));
		order.setOrderDate(OrderIDGenerator.getTime());
		order.setExchange(orderCopy.getExchange());
		order.setBoard(orderCopy.getBoard());
		order.setBroker(OrderIDGenerator.getBrokerCode());
		order.setBOS(orderCopy.getBOS());
		order.setOrdType(OrderIDGenerator.getLimitOrder());

		order.setOrderType(orderCopy.getOrderType());
		order.setStock(orderCopy.getStock());
		order.setIsAdvertising("0");
		order.setTradingId(orderCopy.getTradingId());

		order.setInvType(orderCopy.getInvType());
		order.setCurrency(orderCopy.getCurrency());
		order.setContraBroker("");
		order.setContraUser("");
		order.setPrice(orderCopy.getPrice());
		order.setLot(new Double(orderCopy.getLot()));
		order.setVolume(new Double(sec.getLotSize().doubleValue()
				* orderCopy.getLot()));

		Date baskettime = orderCopy.getBasketTime();
		Date currentDate = OrderIDGenerator.getTime();

		if (baskettime != null && baskettime.getTime() > currentDate.getTime()) {
			order.setIsBasketOrder(orderCopy.getIsBasketOrder());
			order.setBasketTime(orderCopy.getBasketTime());
		} else {
			order.setIsBasketOrder("0");
		}

		order.setStatus(Order.C_ENTRY);
		order.setMarketOrderId("");
		order.setDoneVol(new Double(0));
		order.setDoneLot(new Double(0));
		order.setCounter(orderCopy.getCounter());
		order.setSplitTo(orderCopy.getSplitTo());
		order.setComplianceId(acc.getComlianceId());
		order.setAccId(acc.getAccId());
		order.setBalVol(orderCopy.getBalVol());
		order.setBalLot(orderCopy.getBalLot());
		order.setTradeNo("0");
		order.setUser(((IEQTradeApp) apps).getTradingEngine().getUserId()
				.toUpperCase());
		order.setIsFloor("0");
		order.setMarketRef(order.getId());
		return order;
	}

	String getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { alias },
						new int[] { Account.C_TRADINGID });
		return (acc != null ? acc.getAccId() : "");
	}

	public Vector getSelected() {
		Vector v = ((CustomSelectionModel) tableOrder.getTable()
				.getSelectionModel()).getVSelected();
		Vector result = new Vector(v.size());
		for (int i = 0; i < v.size(); i++) {
			result.addElement(((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ORDER)
					.getDataByIndex(
							tableOrder.getMappedRow(((Integer) v.elementAt(i))
									.intValue())));
		}
		return result;
	}

	protected boolean isAllTemporary(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order o = (Order) v.elementAt(i);
			if (!o.getStatus().equals(Order.C_TEMPORARY))
				return false;
		}
		return result;
	}

	protected boolean isAllQueue(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order order = (Order) v.elementAt(i);
			// if (!o.getIsBasketOrder().equals("1"))
			if (!(order.getStatus().equals(Order.C_OPEN)
					|| order.getStatus().equals(Order.C_PARTIAL_MATCH) || (order
					.getStatus().equals(Order.C_ENTRY) && order
					.getIsBasketOrder().equals("1"))))
				return false;
		}
		return result;
	}

	protected boolean isAllOpen(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order o = (Order) v.elementAt(i);
			if (!o.getStatus().equals(Order.C_OPEN)
					&& !o.getStatus().equals(Order.C_PARTIAL_MATCH))
				return false;
		}
		return result;
	}

	protected boolean isValidRow(int x, int y, boolean ctrlDown) {
		return isValidRowMulti(x, y, ctrlDown);
	}

	protected boolean isValidRowMulti(int x, int y, boolean ctrlDown) {
		int nRow = -1;
		String status = "";
		Order order = new Order();
		order.setDoneLot(new Double(0));
		try {
			JTable tblViewList = tableOrder.getTable();
			nRow = tblViewList.rowAtPoint(new Point(x, y));

			if (nRow >= 0) {
				Vector vselected = getSelected();
				nRow = vselected.size();
				if (isAllTemporary(vselected)) {
					status = Order.C_TEMPORARY;
				} else if (isAllOpen(vselected)) {
					status = Order.C_OPEN;
				} else if (isAllRejected(vselected)) {
					status = Order.C_REJECTED;
				} else if (isAllQueue(vselected)) {
					status = Order.C_ENTRY;
				} else
					status = "";

				if (vselected.size() == 1) {
					Order data = (Order) vselected.get(0);
					status = data.getStatus();
					order = data;
					if (status.equals(Order.C_ENTRY))
						status = "";
				}
			}

			for (int i = 0; i < popupMenu.getComponentCount(); i++) {
				Object tool = popupMenu.getComponent(i);
				if (tool instanceof JMenuItem) {
					if (((JMenuItem) tool).getActionCommand()
							.equals("withdraw")) {
						((JMenuItem) tool)
								.setEnabled((withdrawAction != null
										&& withdrawAction.isEnabled()
										&& nRow >= 0 && (status
										.equals(Order.C_OPEN)
										|| status
												.equals(Order.C_SENDING_TEMPORARY)
										|| status.equals(Order.C_PARTIAL_MATCH)
										|| (order.getStatus().equals(
												Order.C_ENTRY) && order
												.getIsBasketOrder().equals("1")) || status
										.equals(Order.C_ENTRY)))//17112014gtc );	
										&&apps.getAction().get(TradingAction.A_SHOWBUYORDER).isGranted());
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"amend")) {
						((JMenuItem) tool)
								.setEnabled((!ctrlDown && amendAction != null
										&& amendAction.isEnabled() && nRow == 1 && (status
										.equals(Order.C_OPEN) || status
										.equals(Order.C_PARTIAL_MATCH)))
										&&apps.getAction().get(TradingAction.A_SHOWBUYORDER).isGranted()//17112014gtc );	
										&& !order.getOrderType().equals("G"));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"split")) {
						((JMenuItem) tool)
								.setEnabled((!ctrlDown
										&& splitAction != null
										&& splitAction.isEnabled()
										&& nRow == 1
										&& ((status.equals(Order.C_FULL_MATCH) && order
												.getSplitBy().equals(""))
												|| status
														.equals(Order.C_WITHDRAW) || status
													.equals(Order.C_AMEND)) && order
										.getDoneVol().doubleValue() > 0));
						// ((JMenuItem)tool).setVisible((splitAction!=null &&
						// splitAction.isEnabled() && nRow>=0 &&
						// (status.equals(Order.C_FULL_MATCH) ||
						// status.equals(Order.C_WITHDRAW) ||
						// status.equals(Order.C_AMEND)) &&
						// order.getDoneLot().doubleValue()>0 ));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"resend")) {
						((JMenuItem) tool).setEnabled((status
								.equals(Order.C_REJECTED))
								|| status.equals(Order.C_REJECTED2)
								&& nRow >= 0
								&& order.getOrderIDContra().isEmpty());
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"printExcel")) {
						((JMenuItem) tool).setEnabled(true);
					}
				}
			}
			return true;
		} catch (Exception z) {
			return false;
		}
	}

	private boolean isAllRejected(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order o = (Order) v.elementAt(i);

			if ((o.getStatus().equals(Order.C_REJECTED) || o.getStatus()
					.equals(Order.C_REJECTED2))
					&& !o.getOrderIDContra().isEmpty()) {
				return false;
			} else if (o.getStatus().equals(Order.C_REJECTED)
					|| o.getStatus().equals(Order.C_REJECTED2)) {

			} else {
				return false;
			}
		}
		return result;
	}

	protected boolean isValidRowSingle(int x, int y) {
		int nRow = -1;
		String status = "";
		Order order = new Order();
		order.setDoneLot(new Double(0));
		try {
			JTable tblViewList = tableOrder.getTable();
			nRow = tblViewList.rowAtPoint(new Point(x, y));

			if (nRow >= 0) {
				tblViewList.setRowSelectionInterval(nRow, nRow);
				Order data = (Order) tableOrder.getDataByIndex(nRow);
				status = data.getStatus();
				order = data;
			}

			for (int i = 0; i < popupMenu.getComponentCount(); i++) {
				Object tool = popupMenu.getComponent(i);
				if (tool instanceof JMenuItem) {
					if (((JMenuItem) tool).getActionCommand()
							.equals("withdraw")) {
						((JMenuItem) tool)
								.setEnabled((withdrawAction != null
										&& withdrawAction.isEnabled()
										&& nRow >= 0 && (status
										.equals(Order.C_OPEN)
										|| status.equals(Order.C_PARTIAL_MATCH) || (order
										.getStatus().equals(Order.C_ENTRY) && order
										.getIsBasketOrder().equals("1")))));
						((JMenuItem) tool)
								.setVisible((withdrawAction != null
										&& withdrawAction.isEnabled()
										&& nRow >= 0 && (status
										.equals(Order.C_OPEN)
										|| status.equals(Order.C_PARTIAL_MATCH) || (order
										.getStatus().equals(Order.C_ENTRY) && order
										.getIsBasketOrder().equals("1")))));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"amend")) {
						((JMenuItem) tool)
								.setEnabled((amendAction != null
										&& amendAction.isEnabled() && nRow >= 0 && (status
										.equals(Order.C_OPEN) || status
										.equals(Order.C_PARTIAL_MATCH))//17112014gtc );	
										&& !order.getOrderType().equals("G")));
						((JMenuItem) tool)
								.setVisible((amendAction != null
										&& amendAction.isEnabled() && nRow >= 0 && (status
										.equals(Order.C_OPEN) || status
										.equals(Order.C_PARTIAL_MATCH))//17112014gtc );	
										&& !order.getOrderType().equals("G")));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"split")) {
						((JMenuItem) tool)
								.setEnabled((splitAction != null
										&& splitAction.isEnabled()
										&& nRow >= 0
										&& ((status.equals(Order.C_FULL_MATCH) && order
												.getSplitBy().equals(""))
												|| status
														.equals(Order.C_WITHDRAW) || status
													.equals(Order.C_AMEND)) && order
										.getDoneVol().doubleValue() > 0));
						((JMenuItem) tool)
								.setVisible((splitAction != null
										&& splitAction.isEnabled()
										&& nRow >= 0
										&& ((status.equals(Order.C_FULL_MATCH) && order
												.getSplitBy().equals(""))
												|| status
														.equals(Order.C_WITHDRAW) || status
													.equals(Order.C_AMEND)) && order
										.getDoneVol().doubleValue() > 0));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"resend")) {

						boolean isenable = false;
					//	log.info(" cek " + order.getOrderIDContra().isEmpty()	+ " " + order.getStatus());
						if ((order.getStatus().equals(Order.C_REJECTED) || order
								.getStatus().equals(Order.C_REJECTED2))
								&& !order.getOrderIDContra().isEmpty()) {
							//log.info("cek false");
							isenable = false;
						} else if (status.equals(Order.C_REJECTED)
								|| status.equals(Order.C_REJECTED2)) {
							//log.info("cek true");
							isenable = true;
						}
						((JMenuItem) tool).setEnabled(isenable);
					}
				}
			}
			return true;
		} catch (Exception z) {
			return false;
		}
	}

	private String genConfirm(Order order) {
		String result = "";
		result = result.concat(order.getTradingId()).concat(" ");
		result = result.concat(order.getBOS().equals(Order.C_BUY) ? "Buy "
				: "Sell ");
		result = result.concat(order.getStock().concat(" "));
		result = result
				.concat("at ")
				.concat(OrderRender.formatter.format((Double) order.getPrice()))
				.concat(" ");
		result = result.concat(
				OrderRender.formatter.format((Double) order.getLot())).concat(
				" lot\n");
		return result;
	}

	protected class RowChangeList implements TableModelListener {
		@Override
		public void tableChanged(TableModelEvent e) {
			int rowcount = tableOrder.getTable().getRowCount();
		}

	}

	protected class RowChanged implements TableModelListener {
		public void tableChanged(final TableModelEvent e) {
			double count = 0, ocount = 0, ovalue = 0, dcount = 0, dvalue = 0, tlot = 0, tvalue = 0;
			int size = tableOrder.getTable().getRowCount();

			for (int i = 0; i < size; i++) {
				Order order = (Order) tableOrder.getDataByIndex(i);
				count++;
				tlot = tlot + order.getLot().doubleValue();
				tvalue = tvalue
						+ (order.getVolume().doubleValue() * order.getPrice()
								.doubleValue());
				if (order.getStatus().equals(Order.C_PARTIAL_MATCH)
						|| order.getStatus().equals(Order.C_OPEN)) {
					ocount++;
					ovalue = ovalue
							+ (order.getBalVol().doubleValue() * order
									.getPrice().doubleValue());
				}
				if (order.getStatus().equals(Order.C_PARTIAL_MATCH)
						|| order.getStatus().equals(Order.C_FULL_MATCH)) {
					dcount++;
					dvalue = dvalue
							+ (order.getDoneVol().doubleValue() * order
									.getPrice().doubleValue());
				}
			}
			tCount.setValue(new Double(count));
			oCount.setValue(new Double(ocount));
			oValue.setValue(new Double(ovalue));
			dCount.setValue(new Double(dcount));
			dValue.setValue(new Double(dvalue));
			tLot.setValue(new Double(tlot));
			tValue.setValue(new Double(tvalue));

			if (e.getType() == TableModelEvent.INSERT) {
				Rectangle r = tableOrder.getTable().getCellRect(size - 1, 0,
						true);
				tableOrder.scrollRectToVisible(r);
			}

		}

		/*
		 * public void tableChanged(final TableModelEvent e) { double count = 0,
		 * ocount = 0, ovalue = 0, dcount = 0, dvalue = 0, tlot = 0, tvalue = 0;
		 * int size = tableOrder.getTable().getRowCount(); for (int i = 0; i <
		 * size; i++) { Order order = (Order) tableOrder.getDataByIndex(i);
		 * count++; tlot = tlot + order.getLot().doubleValue(); tvalue = tvalue
		 * + (order.getVolume().doubleValue() * order.getPrice()
		 * .doubleValue()); if (order.getStatus().equals(Order.C_PARTIAL_MATCH)
		 * || order.getStatus().equals(Order.C_OPEN)) { ocount++; ovalue =
		 * ovalue + (order.getBalVol().doubleValue() * order
		 * .getPrice().doubleValue()); } if
		 * (order.getStatus().equals(Order.C_PARTIAL_MATCH) ||
		 * order.getStatus().equals(Order.C_FULL_MATCH)) { dcount++; dvalue =
		 * dvalue + (order.getDoneVol().doubleValue() * order
		 * .getPrice().doubleValue()); } tCount.setValue(new Double(count));
		 * oCount.setValue(new Double(ocount)); oValue.setValue(new
		 * Double(ovalue)); dCount.setValue(new Double(dcount));
		 * dValue.setValue(new Double(dvalue)); tLot.setValue(new Double(tlot));
		 * tValue.setValue(new Double(tvalue)); } }
		 */
	}

	protected class RowUpdate implements TableModelListener {
		public void tableChanged(final TableModelEvent e) {
			if (e != null) {
				boolean move = true;
				if (move) {
					final int first = e.getFirstRow();
					final int last = e.getLastRow();
					if (first == last) {
						final int n = tableOrder.getTableRow((first));
						if (n > -1) {
							tableOrder.getTable().clearSelection();
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									synchronized (tableOrder.getTable()) {
										try {
											Rectangle rec = tableOrder
													.getTable().getCellRect(n,
															2, false);
											tableOrder.getTable()
													.scrollRectToVisible(rec);
											// tableOrder.getTable()
											// .setRowSelectionInterval(n,
											// n);
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									}
								}
							});
						}
					}
				}
			}
		}
	}

	protected boolean isValidRow(int x, int y) {
		return true;
	}

	public class MyMouseAdapter extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) {

				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}

		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}
	};

	public class MyCustomMouseAdapter extends CustomMouseAdapter {
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) {

				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}

		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}
	}

	public void executeAction(String actionName) {
		tableOrder.getActionMap().get(actionName).actionPerformed(null);
	}

	private boolean isSuperUser() {
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		return sales != null ? sales.getProfileId().toUpperCase()
				.equals("SUPERUSER") : false;
	}
	
	private String isValidUserProfile() {
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		
		//System.err.println("isallow : "+sales.getMenuId().contains("41"));
		return sales != null ? sales.getProfileId().toUpperCase() : "FALSE";
	}

	private boolean isAllowAdvertising() {
		System.err.println("is allow funtion");
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "41" },
						new int[] { UserProfile.C_MENUID });
		
		return sales.isAllowed();
		
	}
	
	protected JFileChooser jfc = null;
	protected ExcelFileFilter filterFile = null;

	protected void savetoFile() {
		if (jfc == null) {
			filterFile = new ExcelFileFilter();
			filterFile.addExtension("xls");
			filterFile.setDescription(" Microsoft Excel ");
			jfc = new JFileChooser(new File("C:\\"));
			jfc.setFileFilter(filterFile);
			jfc.setAcceptAllFileFilterUsed(false);
			jfc.setDialogTitle("Save Order to....");
			jfc.setDialogType(JFileChooser.SAVE_DIALOG);
		}
		String filename = "";
		int returnVal = jfc.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File ffile = jfc.getSelectedFile();
			filename = ffile.getAbsolutePath();
			if (!filename.endsWith(".xls")) {
				filename = filename + ".xls";
			}
		} else {
			return;
		}
		int i = tableOrder.getTable().getRowCount();
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(filename));
			Hashtable prop = tableOrder.getTableProperties();
			int[] order = (int[]) prop.get("order");
			int[] width = (int[]) prop.get("width");
			String header = "";
			for (int k = 0; k < order.length; k++) {
				if (width[k] != 0) {
					header = header + OrderDef.getHeader().elementAt(order[k])
							+ "\t";
				}
			}
			out.write(header + "\n");
			for (int j = 0; j < i; j++) {
				Column dat = (Column) tableOrder.getTable().getValueAt(j, 0);
				Order ord = (Order) dat.getSource();
				String row = "";
				for (int k = 0; k < order.length; k++) {
					if (width[k] != 0) {
						switch (order[k]) {
						case Order.C_STATUSID:
							row = row
									+ OrderRender.hashStatus.get(ord
											.getData(order[k])) + "\t";
							break;
						case Order.C_BUYORSELL:
							row = row
									+ OrderRender.hashSide.get(ord
											.getData(order[k])) + "\t";
							break;
						case Order.C_ORDERTYPE:
							row = row
									+ OrderRender.hashType.get(ord
											.getData(order[k])) + "\t";
							break;
						case Order.C_ORDTYPE:
							row = row + "limited order\t";
							break;
						default:
							row = row + ord.getData(order[k]) + "\t";
							break;
						}
					}
				}
				out.write(row + "\n");
			}
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public class ExcelFileFilter extends FileFilter {
		private Hashtable filters = null;
		private String description = null;
		private String fullDescription = null;
		private boolean useExtensionsInDescription = true;

		public ExcelFileFilter() {
			this.filters = new Hashtable();
		}

		public ExcelFileFilter(String extension) {
			this(extension, null);
		}

		public ExcelFileFilter(String extension, String description) {
			this();
			if (extension != null)
				addExtension(extension);
			if (description != null)
				setDescription(description);
		}

		public ExcelFileFilter(String[] filters) {
			this(filters, null);
		}

		public ExcelFileFilter(String[] filters, String description) {
			this();
			for (int i = 0; i < filters.length; i++) {
				// add filters one by one
				addExtension(filters[i]);
			}
			if (description != null)
				setDescription(description);
		}

		public boolean accept(File f) {
			if (f != null) {
				if (f.isDirectory()) {
					return true;
				}
				String extension = getExtension(f);
				if (extension != null && filters.get(getExtension(f)) != null) {
					return true;
				}
				;
			}
			return false;
		}

		public String getExtension(File f) {
			if (f != null) {
				String filename = f.getName();
				int i = filename.lastIndexOf('.');
				if (i > 0 && i < filename.length() - 1) {
					return filename.substring(i + 1).toLowerCase();
				}
				;
			}
			return null;
		}

		public void addExtension(String extension) {
			if (filters == null) {
				filters = new Hashtable(5);
			}
			filters.put(extension.toLowerCase(), this);
			fullDescription = null;
		}

		public String getDescription() {
			if (fullDescription == null) {
				if (description == null || isExtensionListInDescription()) {
					fullDescription = description == null ? "(" : description + " (";
					// build the description from the extension list
					Enumeration extensions = filters.keys();
					if (extensions != null) {
						fullDescription += "." + (String) extensions.nextElement();
						while (extensions.hasMoreElements()) {
							fullDescription += ", ." + (String) extensions.nextElement();
						}
					}
					fullDescription += ")";
				} else {
					fullDescription = description;
				}
			}
			return fullDescription;
		}

		public void setDescription(String description) {
			this.description = description;
			fullDescription = null;
		}

		public void setExtensionListInDescription(boolean b) {
			useExtensionsInDescription = b;
			fullDescription = null;
		}

		public boolean isExtensionListInDescription() {
			return useExtensionsInDescription;
		}
	}

	public static void validnot(boolean sl, String userid) {
		usernotif = userid;
		if (!sl) {
			btnNotif.setEnabled(false);
		} else {
			btnNotif.setEnabled(true);
		}

	}

	public boolean isFilterClientAdded(String client) {
		try {
		String sf = ((IEQTradeApp) apps).getTradingEngine().getConnection()
				.loadFilterByClient();
		String[] sp = sf.split("#");
		for (int i = 0; i < sp.length; i++)
			if (sp[i].trim().toLowerCase().equals(client))
				return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}
	
	public void loadData() {		
		String sfilter = (((TradingEngine) ((IEQTradeApp) apps).getTradingEngine())).getConnection().loadFilterByClient();

		if (sfilter != null && !sfilter.isEmpty()) {

			String[] sp = sfilter.split("#");
			filterclient = new Vector();
			for (String ss : sp) {
				Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { ss },
						new int[] { Account.C_ACCID });
				filterclient.add(acc.getAccId());
			}
		}else if(isSuperUser() && sfilter == null){
			filterclient = new Vector();
		}
		else{
			filterclient = null;
		}
	}
public static class Global {
		
		public static int isOrder = 0;
	}
}

/*package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultListModel;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.sun.security.auth.UserPrincipal;
import com.vollux.demo.Mapping;
import com.vollux.framework.IApp;
import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.model.TableModelFilter;
import com.vollux.ui.CustomMouseAdapter;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinForm;
import com.vollux.ui.JTextLabel;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.ITradingApp;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.OrderIDGenerator;
import eqtrade.trading.engine.TradingEngine;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Notif;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.OrderRender;
import eqtrade.trading.model.OrderStatus;
import eqtrade.trading.model.Stock;
import eqtrade.trading.model.UserProfile;

public class OrderPanel extends JPanel {
	protected final Log log = LogFactory.getLog(getClass());
	JGrid tableOrder;
	FilterOrder filterOrder;
	boolean isLoadingSuperUser = false;
	Hashtable hSetting;
	JDropDown comboClient;
	JDropDown comboStock;
	JDropDown comboBOS;
	JDropDown comboStatus;
	JDropDown comboTaker;
	JDropDown comboBoard;
	JDropDown comboFloor;
	JNumber price;
	JButton btnClear;
	JButton btnView;
	static JButton btnNotif;

	Action amendAction;
	Action withdrawAction;
	Action splitAction;
	Action negdealAction;

	JMenuItem withdrawMenu;
	JMenuItem amendMenu;
	JMenuItem propertiesMenu;
	JMenuItem splitMenu;
	JMenuItem resendMenu;
	JMenuItem printExcelMenu;

	JNumber tCount;
	JNumber oCount;
	JNumber oValue;
	JNumber dCount;
	JNumber dValue;
	JNumber tLot;
	JNumber tValue;
	JPopupMenu popupMenu;
	Vector filterclient = null;
	String client = "", sfilterClient = "", sfilterStock = "";
	static String usernotif = "";

	CustomSelectionModel customSelection;
	IApp apps;
	JSkinForm form;
	
	public OrderPanel(IApp apps, JSkinForm form, Hashtable hSetting) {
		super(new BorderLayout());
		this.apps = apps;
		this.form = form;
		this.hSetting = hSetting;
	}

	protected JGrid createTable(GridModel data, TableModelFilter tmf,
			Hashtable hsetting) {
		JGrid temptable = new JGrid(data, tmf,
				(String[]) hsetting.get("header"),
				(int[]) hsetting.get("order"), (int[]) hsetting.get("width"));
		temptable.setSortColumn(((Integer) hsetting.get("sortcolumn"))
				.intValue());
		temptable.setSortAscending(((Boolean) hsetting.get("sortascending"))
				.booleanValue());
		// table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		temptable.getTable().setRowSelectionAllowed(false);
		temptable.getTable().setShowGrid(false);
		temptable.getTable().setIntercellSpacing(new Dimension(0, 0));
		temptable.setAlignment((int[]) hsetting.get("alignment"));
		temptable.getTable().setRowSelectionAllowed(true);
		if (hsetting.get("font") != null)
			temptable.setTableFont((Font) hsetting.get("font"));
		temptable.setColumnSortedProperties((boolean[]) hsetting.get("sorted"));
		return temptable;
	}

	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				tableOrder.getTable().requestFocus();
			}
		});
	}

	void infoNotif() {
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		String user = sales.getUserId();
		usernotif = user;
		//System.out.println("NOTIFFF**----" + user);
	}

	void initData() {

		infoNotif();

	}

	protected void build() {
		createPopup();

		
		amendAction = apps.getAction().get(TradingAction.A_SHOWAMENDORDER);
		withdrawAction = apps.getAction().get(TradingAction.A_SHOWWITHDRAWORDER);
		splitAction = apps.getAction().get(TradingAction.A_SHOWSPLITORDER);
		negdealAction = apps.getAction().get(TradingAction.A_SHOWNEGDEAL);
		loadData();
		filterOrder = new FilterOrder(this);
		((FilterColumn) filterOrder.getFilteredData("taker")).setField("0");
		((FilterColumn) filterOrder.getFilteredData("user"))
				.setField(((IEQTradeApp) apps).getTradingEngine().getUserId()
						.toUpperCase());
		//((FilterColumn) filterOrder.getFilteredData("accountv")).setField(filterclient);
		comboClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboStock = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		comboBOS = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_SIDE), Mapping.CIDX_CODE, false);
		comboStatus = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ORDERSTATUS), OrderStatus.C_NAME,
				false);
		comboTaker = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_TAKER), OrderStatus.C_ID, false);
		comboBoard = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_BOARDFILTER), Mapping.CIDX_NAME,
				false);
		comboFloor = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_FLOORFILTER), Mapping.CIDX_NAME,
				false);
		price = new JNumber(Double.class, 0, 0, false, true);
		btnClear = new JButton("Clear");
		btnClear.setMnemonic('R');
		btnView = new JButton("View");
		btnView.setMnemonic('V');
		btnNotif = new JButton("Notification Setup");
		btnNotif.setMnemonic('N');

		registerEvent(comboClient.getTextEditor());
		registerEventCombo(comboClient);

		registerEvent(comboStock.getTextEditor());
		registerEventCombo(comboStock);

		registerEvent(comboBOS);
		registerEvent(comboStatus);
		registerEvent(comboTaker);
		registerEvent(comboBoard);
		registerEvent(comboFloor);
		registerEvent(price);
		registerEvent(btnClear);
		registerEvent(btnView);
		btnNotif.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						HashMap param = new HashMap();
						//System.out.println("+++>>" + usernotif);
						param.put("CLIENT", usernotif);
						apps.getUI().showUI(TradingUI.UI_NOTIFICATION, param);

					}
				});

			}
		});
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboClient.getTextEditor().setText("");
				comboStock.getTextEditor().setText("");
				comboBOS.setSelectedIndex(0);
				comboStatus.setSelectedIndex(0);
				comboTaker.setSelectedIndex(0);
				comboBoard.setSelectedIndex(0);
				comboFloor.setSelectedIndex(1);
				price.setValue(null);
				((FilterColumn) filterOrder.getFilteredData("account"))
						.setField(null);
				((FilterColumn) filterOrder.getFilteredData("stock"))
						.setField(null);
				((FilterColumn) filterOrder.getFilteredData("bos"))
						.setField(null);
				((FilterColumn) filterOrder.getFilteredData("status"))
						.setField(null);
				((FilterColumn) filterOrder.getFilteredData("taker"))
						.setField("0");
				((FilterColumn) filterOrder.getFilteredData("board"))
						.setField(null);
				((FilterColumn) filterOrder.getFilteredData("price"))
						.setField(null);
				((FilterColumn) filterOrder.getFilteredData("floor"))
						.setField("0");
				((FilterColumn) filterOrder.getFilteredData("accountv"))
						.setField(filterclient);
				filterOrder.fireFilterChanged();
			}
		});
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// if (isSuperUser()) {
				//System.out.println(filterclient+" -- ");
				String clientid = comboClient.getText();
				String secid = comboStock.getText();
				if (clientid != null && !clientid.isEmpty()) {
					setState(true);
					((IEQTradeApp) apps).getTradingEngine().refreshOrder(
							clientid.trim().toUpperCase(), "%","%");
					((FilterColumn) filterOrder.getFilteredData("accountv"))
					.setField(null);
				}else if(secid!=null && !secid.isEmpty() && isSuperUser()){
					setState(true);
					((IEQTradeApp) apps).getTradingEngine().refreshOrder(
							"%", "%",secid.trim().toUpperCase());
					((FilterColumn) filterOrder.getFilteredData("accountv"))
					.setField(null);
				}else{
					((FilterColumn) filterOrder.getFilteredData("accountv"))
							.setField(filterclient);
				}

				// }
			//log.info("is superuser:"+isSuperUser());

				if (isSuperUser()) {

					if (!sfilterClient.isEmpty()
							&& !isFilterClientAdded(sfilterClient)) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByClient(sfilterClient);
					}

					String accid = getAccountId(clientid);

					if (accid != null && !accid.isEmpty()) {

						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByClient(accid);
						sfilterClient = accid;
					}

					if (!sfilterStock.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByStock(sfilterStock);

					}

					String stockid = comboStock.getText();

					if (stockid != null && !stockid.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByStock(stockid.toUpperCase());
						sfilterStock = stockid.toUpperCase();
					}

				}

				filterOrder.fireFilterChanged();
			}

		});
		comboClient.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboClient.showPopup();
				} catch (Exception ex) {
				}
			}

			public void focusLost(FocusEvent e) {
				String secid = comboStock.getText();
				String clientid = comboClient.getText();
				if (isSuperUser()) {
					if( clientid != null && !clientid.isEmpty()){
						setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshOrder(
								clientid.trim().toUpperCase(), "%","%");
						((FilterColumn) filterOrder.getFilteredData("accountv"))
						.setField(null);
					}else if (secid!=null && !secid.isEmpty() && clientid != null && !clientid.isEmpty()) {
						setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshOrder(
								clientid.trim().toUpperCase(), "%",secid.trim().toUpperCase());
						((FilterColumn) filterOrder.getFilteredData("accountv"))
						.setField(null);
					} else if (secid!=null && !secid.isEmpty()) {
						
					} else {
						((FilterColumn) filterOrder.getFilteredData("accountv"))
								.setField(filterclient);
					}
					if (!sfilterClient.isEmpty()
							&& !isFilterClientAdded(sfilterClient)) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByClient(sfilterClient);
					}

					String accid = getAccountId(clientid);

					if (accid != null && !accid.isEmpty()) {

						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByClient(accid);
						sfilterClient = accid;
					}

					if (!sfilterStock.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByStock(sfilterStock);

					}

					String stockid = comboStock.getText();

					if (stockid != null && !stockid.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByStock(stockid.toUpperCase());
						sfilterStock = stockid.toUpperCase();
					}
				} else {

				}
				((FilterColumn) filterOrder.getFilteredData("account"))
						.setField(comboClient.getText().trim().equals("") ? null
								: comboClient.getText().trim().toUpperCase());
				filterOrder.fireFilterChanged();
			}
		});
		comboStock.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboStock.showPopup();
				} catch (Exception ex) {
				}
			}

			public void focusLost(FocusEvent e) {

				String secid = comboStock.getText();
				String clientid = comboClient.getText();
				if (isSuperUser()) {
					if(secid!=null && !secid.isEmpty()){
						setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshOrder(
								"%", "%",secid.trim().toUpperCase());
						((FilterColumn) filterOrder.getFilteredData("accountv"))
						.setField(null);
					}else if (secid!=null && !secid.isEmpty() && clientid != null && !clientid.isEmpty()) {
						setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshOrder(
								clientid.trim().toUpperCase(), "%",secid.trim().toUpperCase());
						((FilterColumn) filterOrder.getFilteredData("accountv"))
						.setField(null);
					} else if (clientid != null && !clientid.isEmpty()) {
						
					} else {
						((FilterColumn) filterOrder.getFilteredData("accountv"))
								.setField(filterclient);
					}
					if (!sfilterClient.isEmpty()
							&& !isFilterClientAdded(sfilterClient)) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByClient(sfilterClient);
					}

					String accid = getAccountId(clientid);

					if (accid != null && !accid.isEmpty()) {

						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByClient(accid);
						sfilterClient = accid;
					}

					if (!sfilterStock.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByStock(sfilterStock);

					}

					String stockid = comboStock.getText();

					if (stockid != null && !stockid.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByStock(stockid.toUpperCase());
						sfilterStock = stockid.toUpperCase();
					}
				} else {

				}
				((FilterColumn) filterOrder.getFilteredData("stock"))
						.setField(comboStock.getText().trim().equals("") ? null
								: comboStock.getText().trim().toUpperCase());
				filterOrder.fireFilterChanged();
			}
		});
		comboBOS.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("bos"))
						.setField(comboBOS.getSelectedIndex() == 0 ? null
								: getBOSCode());
				filterOrder.fireFilterChanged();
			}
		});
		comboStatus.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("status"))
						.setField(comboStatus.getSelectedIndex() == 0 ? null
								: getStatusCode());
				filterOrder.fireFilterChanged();
			}
		});
		comboBoard.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("board"))
						.setField(comboBoard.getSelectedIndex() == 0 ? null
								: getBoardCode());
				filterOrder.fireFilterChanged();
			}
		});
		comboFloor.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("floor"))
						.setField(comboFloor.getSelectedIndex() == 0 ? null
								: getFloorCode());
				filterOrder.fireFilterChanged();
			}
		});
		price.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				try {
					price.commitEdit();
				} catch (Exception ex) {
				}
				((FilterColumn) filterOrder.getFilteredData("price"))
						.setField(price.getValue());
				filterOrder.fireFilterChanged();
			}
		});
		comboTaker.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("taker"))
						.setField(comboTaker.getSelectedIndex() + "");
				filterOrder.fireFilterChanged();
			}
		});

		JPanel editorPanel = new JPanel();
		FormLayout layout = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, 100px, 2dlu, pref,2dlu,pref,2dlu,pref",
				"pref");
		PanelBuilder builder = new PanelBuilder(layout, editorPanel);
		builder.setBorder(new EmptyBorder(4, 4, 4, 4));
		CellConstraints cc = new CellConstraints();
		builder.add(new JTextLabel("Client", 'C', comboClient.getTextEditor()),
				cc.xy(1, 1));
		builder.add(comboClient, cc.xy(3, 1));
		builder.add(new JTextLabel("Stock", 'S', comboStock.getTextEditor()),
				cc.xy(5, 1));
		builder.add(comboStock, cc.xy(7, 1));
		builder.add(new JTextLabel("Price", 'P', price), cc.xy(9, 1));
		builder.add(price, cc.xy(11, 1));
		builder.add(new JTextLabel("B/S", 'B', comboBOS), cc.xy(13, 1));
		builder.add(comboBOS, cc.xy(15, 1));
		builder.add(new JTextLabel("Board", 'R', comboBoard), cc.xy(17, 1));
		builder.add(comboBoard, cc.xy(19, 1));
		builder.add(new JTextLabel("Entry", 'R', comboFloor), cc.xy(21, 1));
		builder.add(comboFloor, cc.xy(23, 1));
		builder.add(new JTextLabel("Status", 'T', comboStatus), cc.xy(25, 1));
		builder.add(comboStatus, cc.xy(27, 1));
		builder.add(comboTaker, cc.xy(29, 1));
		builder.add(btnView, cc.xy(31, 1));
		builder.add(btnClear, cc.xy(33, 1));
		builder.add(btnNotif, cc.xy(35, 1));

		//System.out.println("get_store");
		((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER).addTableModelListener(new RowUpdate());
		tableOrder = createTable(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER), filterOrder,(Hashtable) hSetting.get("tableOrder"));
		tableOrder.getTable().add(popupMenu);

		tableOrder.getTable().addMouseListener(new MyMouseAdapter());
		tableOrder.addMouseListener(new MyCustomMouseAdapter());
		tableOrder.setColumnHide(OrderDef.columnhide);
		customSelection = new CustomSelectionModel();
		customSelection.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tableOrder.getTable().setSelectionModel(customSelection);
		tableOrder.getTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tableOrder.getTable().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 1 && e.getClickCount() == 2 && isValidRow(e.getX(), e.getY(), false)) {
					
					JTable tblViewList = tableOrder.getTable();
					int nRow = tblViewList.rowAtPoint(new Point(e.getX(), e.getY()));

					if (nRow >= 0) {
						tblViewList.setRowSelectionInterval(nRow, nRow);
						tblViewList.getSelectionModel().addSelectionInterval(
								nRow, nRow);
						Order data = (Order) tableOrder.getDataByIndex(nRow);

						if (data.getStatus().equals(Order.C_FULL_MATCH)
								|| data.getStatus().equals(
										Order.C_PARTIAL_MATCH)) {
							apps.getUI().showUI(TradingUI.UI_TRADE);
							UITrade tradelist = (UITrade) apps.getUI().getForm(
									TradingUI.UI_TRADE);
							tradelist.setFilteredMarket(data.getMarketOrderId());

						}
					}

				} else if (e.isPopupTrigger()) {

					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						//log.info("popup");
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						//log.info("popup2");
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
		});
		tableOrder.addMouseListener(new CustomMouseAdapter() {
			public void mousePressed(MouseEvent e) {

				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						//log.info("popup3");
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {

					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						//log.info("popup4");
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
		});

		registerEvent(tableOrder);
		registerEvent(tableOrder.getTable());
		tableOrder.getTable().getModel().addTableModelListener(new RowChanged());
		tableOrder.getTable().getModel().addTableModelListener(new RowChangeList());

		tCount = new JNumber(Double.class, 0, 0, false, false);
		oCount = new JNumber(Double.class, 0, 0, false, false);
		oValue = new JNumber(Double.class, 0, 0, false, false);
		dCount = new JNumber(Double.class, 0, 0, false, false);
		dValue = new JNumber(Double.class, 0, 0, false, false);
		tLot = new JNumber(Double.class, 0, 0, false, false);
		tValue = new JNumber(Double.class, 0, 0, false, false);
		JPanel infoPanel = new JPanel();
		FormLayout infLayout = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,80px, 2dlu, pref, 2dlu, 110px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 110px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 110px",
				"pref");
		PanelBuilder infBuilder = new PanelBuilder(infLayout, infoPanel);
		infBuilder.setBorder(new EmptyBorder(4, 4, 4, 4));

		infBuilder.add(new JLabel("Count"), cc.xy(1, 1));
		infBuilder.add(tCount, cc.xy(3, 1));
		infBuilder.add(new JLabel("Open Count"), cc.xy(5, 1));
		infBuilder.add(oCount, cc.xy(7, 1));
		infBuilder.add(new JLabel("Open Value"), cc.xy(9, 1));
		infBuilder.add(oValue, cc.xy(11, 1));
		infBuilder.add(new JLabel("Done Count"), cc.xy(13, 1));
		infBuilder.add(dCount, cc.xy(15, 1));
		infBuilder.add(new JLabel("Done Value"), cc.xy(17, 1));
		infBuilder.add(dValue, cc.xy(19, 1));
		infBuilder.add(new JLabel("T.Lot"), cc.xy(21, 1));
		infBuilder.add(tLot, cc.xy(23, 1));
		infBuilder.add(new JLabel("T.Value"), cc.xy(25, 1));
		infBuilder.add(tValue, cc.xy(27, 1));

		this.setBorder(new EmptyBorder(2, 2, 2, 2));
		JScrollPane t = new JScrollPane(editorPanel);
		t.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		t.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.add(t, BorderLayout.NORTH);
		this.add(tableOrder, BorderLayout.CENTER);

		JScrollPane b = new JScrollPane(infoPanel);
		b.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		b.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		this.add(b, BorderLayout.PAGE_END);
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		if (!sales.getProfileId().equalsIgnoreCase("CUSTOMER")) {
			btnNotif.setEnabled(false);
		}
		try {
			comboBOS.setSelectedIndex(0);
			comboStatus.setSelectedIndex(0);
			comboTaker.setSelectedIndex(0);
			comboBoard.setSelectedIndex(0);
			comboFloor.setSelectedIndex(0);
			price.setValue(null);
		} catch (Exception ex) {
		}
		refresh();
		initData();
	}

	private void registerEventCombo(JDropDown comp) {
		InputMap inputMap = comp.getTextEditor().getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "enterAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap().put("enterAction", new AbstractAction("enterAction") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {

				if (e.getSource().equals(comboStock.getTextEditor())) {
					comboStock.setSelectedIndex(comboStock
							.getSelectedIndex());
					comboStock.hidePopup();
					price.requestFocus();
				} else if (e.getSource().equals(
						comboClient.getTextEditor())) {
					comboClient.setSelectedIndex(comboClient
							.getSelectedIndex());
					comboClient.hidePopup();
					comboStock.requestFocus();
				}

				// comboStock.requestFocus();

			}

		});
	}

	public void refresh() {
//		System.out.println("refresh");
		tableOrder.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tableOrder.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tableOrder.setNewFont(TradingSetting.getFont());
		((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ORDER).refresh();
	}

	public void setState(boolean state) {
		btnView.setEnabled(state);
		btnClear.setEnabled(state);
		isLoadingSuperUser = !state;
//		System.out.println("isloadingsuperuser1 "+isLoadingSuperUser);
	}

	public void success() {
//		System.out.println("isloadingsuperuser2 "+isLoadingSuperUser);
		setState(true);
		if (isLoadingSuperUser) {

			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					setState(true);
					((FilterColumn) filterOrder.getFilteredData("account"))
							.setField(comboClient.getText().trim().equals("") ? null
									: comboClient.getText().trim()
											.toUpperCase());
					filterOrder.fireFilterChanged();
				}
			});
		}
	}

	public void failed(final String reason) {
		if (isLoadingSuperUser) {

			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					setState(true);
					Utils.showMessage(reason, null);
				}
			});
		}

	}

	String getBOSCode() {
		Mapping m = (Mapping) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_SIDE)
				.getDataByKey(new Object[] { comboBOS.getSelectedItem() });
		return (m != null) ? m.getName() : null;
	}

	Vector getStatusCode() {
		Vector v = new Vector();
		if (comboStatus.getSelectedIndex() == 0) {
			return null;
		} else {
			OrderStatus os = (OrderStatus) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ORDERSTATUS)
					.getDataByField(
							new Object[] { comboStatus.getSelectedItem() },
							new int[] { OrderStatus.C_NAME });
			if (os != null) {
				Utils.parser(os.getId(), "|", v);
//				System.out.println("filter statusnya adalah: " + os.getId());
			}
			return v;
		}
	}

	String getFloorCode() {
		Mapping m = (Mapping) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_FLOORFILTER)
				.getDataByField(new Object[] { comboFloor.getSelectedItem() },
						new int[] { Mapping.CIDX_NAME });
		return (m != null) ? m.getCode() : null;
	}

	Vector getBoardCode() {
		Vector v = new Vector();
		if (comboBoard.getSelectedIndex() == 0) {
			return null;
		} else {
			Mapping m = (Mapping) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_BOARDFILTER)
					.getDataByField(
							new Object[] { comboBoard.getSelectedItem() },
							new int[] { Mapping.CIDX_NAME });
			if (m != null) {
				Utils.parser(m.getCode(), "|", v);
			}
			return v;
		}
	}

	void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "secAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "clientAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0), "withdrawAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0), "amendAction");
		inputMap.put(KeyStroke.getKeyStroke("alt W"), "withdrawAction");
		inputMap.put(KeyStroke.getKeyStroke("alt A"), "amendAction");
		inputMap.put(KeyStroke.getKeyStroke("alt I"), "splitAction");
		
		comp.getActionMap().put("amendAction", new AbstractAction("amendAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				if (amendAction == null || !amendAction.isEnabled())
					return;
				Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
				if (order == null) {
					Utils.showMessage("Please select order to amend", form);
				} else {
					if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
						Utils.showMessage("access denied, please contact your helpdesk", form);
						return;
					}					
					if (order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH)) {
						if (amendAction != null)
							amendAction.actionPerformed(new ActionEvent(order, 0, ""));
					}
				}
			}
		});
		
		comp.getActionMap().put("withdrawAction", new AbstractAction("withdrawAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				Thread thread = new Thread() {
					@Override
					public void run() {
						Vector v = getSelected();
						StringBuffer info = new StringBuffer("");
						final Vector vorder = new Vector();
						if (v.size() == 1) {
							Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
							if (order == null) {
								Utils.showMessage("Please select order to withdraw",form);
								return;
							}
							if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
								Utils.showMessage("access denied, please contact your helpdesk",form);
								return;
							}
							if (order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH) || (order.getStatus().equals(Order.C_ENTRY) && order.getIsBasketOrder().equals("1"))) {
								info.append("are you sure want to withdraw this order?\n");
								info.append(genConfirm(order));
								vorder.add(order);
							}
						} else {									
							if (v.size() <= 20) {
								info.append("are you sure want to withdraw these order?\n");
								for (int i = 0; i < v.size(); i++) {
									Order order = (Order) v.elementAt(i);
									if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
										Utils.showMessage("access denied, please contact your helpdesk",form);
										return;
									}
									if (order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH) || (order.getStatus().equals(Order.C_ENTRY) && order.getIsBasketOrder().equals("1"))) {
										info = info.append(genConfirm(order));
										vorder.add(order);
									}
								}
							} else {
								vorder.addAll(v);
								info.append("are you sure want to withdraw all selected order (" + v.size() + " records) ?\n");
							}
						}

						if (vorder.size() > 0) {
							int nconfirm = Utils.C_YES_DLG;

							nconfirm = Utils.showConfirmDialog(form,
									"Withdraw Order", info.toString(),
									Utils.C_YES_DLG);
							if (nconfirm == Utils.C_YES_DLG) {
								((IEQTradeApp) apps).getTradingEngine().withdrawOrderMulti(vorder);

								try {
									int rows = tableOrder
											.getSelectedRow();
									if (tableOrder.getTable()
											.getRowCount() == 1)
										rows = 0;
									if (rows >= 0)
										tableOrder
												.getTable()
												.setRowSelectionInterval(
														rows, rows);
								} catch (Exception ex) {
								}
							}
						}
					}
				};

				thread.start();						
			}
		});

		comp.getActionMap().put("splitAction", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
				if (order == null) {
					Utils.showMessage("Please select order to split", form);
				} else {
					if ((order.getStatus().equals(Order.C_FULL_MATCH) && order.getSplitBy().equals("")) || ((order.getStatus().equals(Order.C_AMEND) || order.getStatus().equals(Order.C_WITHDRAW)) && order.getDoneVol().doubleValue() > 0)) {
						if (splitAction != null)
							splitAction.actionPerformed(new ActionEvent(order, 0, ""));
					}
				}
			}
		});
		
		comp.getActionMap().put("secAction", new AbstractAction("secAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				comboStock.getTextEditor().requestFocus();
			}
		});
		
		comp.getActionMap().put("clientAction", new AbstractAction("clientAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				comboClient.getTextEditor().requestFocus();
			}
		});
		
		comp.getActionMap().put("escapeAction", new AbstractAction("escapeAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				form.setVisible(false);
			}
		});

		comp.getActionMap().put("enterAction", new AbstractAction("enterAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				if (evt.getSource() instanceof JButton) {
					((JButton) evt.getSource()).doClick();
				} else {
					if (evt.getSource() instanceof JNumber) {
						try {
							((JNumber) evt.getSource()).commitEdit();
						} catch (Exception ex) {
						}
					}
				}
			}
		});
	}

	protected boolean _withdrawMenu = false;
	protected boolean _sendMenu = false;
	protected boolean _deleteMenu = false;
	protected boolean _splitMenu = false;

	protected void createPopup() {
		
		
		//advertising
		
		popupMenu = new JPopupMenu();
		resendMenu = new JMenuItem(new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Vector v = getSelected();
					StringBuffer info = new StringBuffer("");
					if (v.size() == 1) {
						Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
						if (order == null) {
							Utils.showMessage("Please select order to resend", form);
							return;
						}
						if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
							Utils.showMessage("access denied, please contact your helpdesk", form);
							return;
						}						
						if (order.getStatus().equals(Order.C_REJECTED) || order.getStatus().equals(Order.C_REJECTED2)) {
							info.append("are you sure want to resend this order?\n");
							info.append(genConfirm(order));
						}
					} else {
						if (v.size() <= 20) {
							info.append("are you sure want to resend these order?\n");
							for (int i = 0; i < v.size(); i++) {
								Order order = (Order) v.elementAt(i);
								if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
									Utils.showMessage("access denied, please contact your helpdesk", form);
									return;
								}
								if (order.getStatus().equals(Order.C_REJECTED) || order.getStatus().equals(Order.C_REJECTED2)) {
									info = info.append(genConfirm(order));
								}
							}
						} else {
							info.append("are you sure want to resend all selected order (" + v.size() + " records) ?\n");
						}
					}

					int nconfirm = Utils.C_YES_DLG;
					nconfirm = Utils.showConfirmDialog(form, "Resend Order", info.toString(), Utils.C_YES_DLG);
					if (nconfirm == Utils.C_YES_DLG) {

						if (v.size() == 1) {
							Order orderCopy = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
							Order order = genOrder(orderCopy);
							if (orderCopy.getStatus().equals(Order.C_REJECTED) || orderCopy.getStatus().equals(Order.C_REJECTED2)) {
								((IEQTradeApp) apps).getTradingEngine().createOrder(order);
							}
						} else if (v.size() > 1) {
							final Vector vrow = new Vector((int) v.size());
							final Vector vorder = new Vector((int) v.size());
							for (int i = 0; i < v.size(); i++) {
								Order orderCopy = (Order) v.elementAt(i);
								Order order = genOrder(orderCopy);
								if (orderCopy.getStatus().equals(Order.C_REJECTED) || orderCopy.getStatus().equals(Order.C_REJECTED2)) {
									vrow.addElement(OrderDef.createTableRow(order));
									vorder.addElement(order);
								}
							}
							((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER).addRow(vrow, false, false);
							((IEQTradeApp) apps).getTradingEngine().createOrderMulti(vorder);
							((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER).refresh();

						}
						try {
							int rows = tableOrder.getSelectedRow();
							if (tableOrder.getTable().getRowCount() == 1)
								rows = 0;
							if (rows >= 0)
								tableOrder.getTable().setRowSelectionInterval(
										rows, rows);
						} catch (Exception ex) {
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					Utils.logException(ex);
				}
			}
		});
		
		resendMenu.setText("Resend");
		resendMenu.setActionCommand("resend");

		printExcelMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				savetoFile();
			}
		});

		printExcelMenu.setText("save to excel");
		printExcelMenu.setActionCommand("printExcel");

		withdrawMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				//log.info("user selected popup");
				try {
					if (withdrawAction == null || !withdrawAction.isEnabled())
						return;
					if (!_withdrawMenu) {
						_withdrawMenu = true;

						Vector v = getSelected();
						//log.info("user selected withdraw from popup menu "+ v.size());
						StringBuffer info = new StringBuffer("");
						if (v.size() == 1) {
							Order order = (Order) tableOrder
									.getDataByIndex(tableOrder.getSelectedRow());
							if (order == null) {
								Utils.showMessage(
										"Please select order to withdraw", form);
								return;
							}
							if (order.getBoard().equals("NG")
									&& !negdealAction.isEnabled()) {
								Utils.showMessage(
										"access denied, please contact your helpdesk",
										form);
								return;
							}
							log.info("open withdraw dialog for confirmation, orderid: "
									+ order.getId()
									+ " with status "
									+ order.getStatus());
							if (order.getStatus().equals(Order.C_OPEN)
									|| order.getStatus().equals(
											Order.C_PARTIAL_MATCH)
									|| (order.getStatus().equals(Order.C_ENTRY) && order
											.getIsBasketOrder().equals("1"))) {
								info.append("are you sure want to withdraw this order?\n");
								info.append(genConfirm(order));
							}
						} else {
							//log.info("open resend dialog for confirmation, for multiple resend (more than 1 row) ");

							if (v.size() <= 20) {
								info.append("are you sure want to withdraw these order?\n");
								for (int i = 0; i < v.size(); i++) {
									Order order = (Order) v.elementAt(i);
									if (order.getBoard().equals("NG")
											&& !negdealAction.isEnabled()) {
										Utils.showMessage(
												"access denied, please contact your helpdesk",
												form);
										return;
									}
									if (order.getStatus().equals(Order.C_OPEN)
											|| order.getStatus().equals(
													Order.C_PARTIAL_MATCH)
											|| (order.getStatus().equals(
													Order.C_ENTRY) && order
													.getIsBasketOrder().equals(
															"1"))) {
										info = info.append(genConfirm(order));
									}
								}
							} else {
								info.append("are you sure want to withdraw all selected order ("
										+ v.size() + " records) ?\n");
							}
						}
						int nconfirm = Utils.C_YES_DLG;
						nconfirm = Utils.showConfirmDialog(form,
								"Withdraw Order", info.toString(),
								Utils.C_YES_DLG);
						if (nconfirm == Utils.C_YES_DLG) {
							final Vector vorder = new Vector(v.size());
							for (int i = 0; i < v.size(); i++) {
								Order order = (Order) v.elementAt(i);
								if (order.getStatus().equals(Order.C_OPEN)
										|| order.getStatus().equals(
												Order.C_PARTIAL_MATCH)
										|| (order.getStatus().equals(
												Order.C_ENTRY) && order
												.getIsBasketOrder().equals("1"))) {
									// ((IEQTradeApp) apps).getTradingEngine()
									// .withdrawOrder(order);
									vorder.add(order);
								}
							}
							((IEQTradeApp) apps).getTradingEngine()
									.withdrawOrderMulti(vorder);
							try {
								int rows = tableOrder.getSelectedRow();
								if (tableOrder.getTable().getRowCount() == 1)
									rows = 0;
								if (rows >= 0)
									tableOrder
											.getTable()
											.setRowSelectionInterval(rows, rows);
							} catch (Exception ex) {
							}
						}
						_withdrawMenu = false;
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					Utils.logException(ex);
					_withdrawMenu = false;
				}
			}
		});
		withdrawMenu.setText("Witdraw");
		withdrawMenu.setActionCommand("withdraw");

		amendMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				if (amendAction == null || !amendAction.isEnabled())
					return;
				Order order = (Order) tableOrder.getDataByIndex(tableOrder.getSelectedRow());
				if (order == null) {
					Utils.showMessage("Please select order to amend", form);
				} else {
					//if(order.getBoard().equals("NG") && )
					if (order.getBoard().equals("NG") && !negdealAction.isEnabled()) {
						Utils.showMessage("access denied, please contact your helpdesk", form);
						return;
					}					
					if (order.getStatus().equals(Order.C_OPEN) || order.getStatus().equals(Order.C_PARTIAL_MATCH)) {
						if (amendAction != null) amendAction.actionPerformed(new ActionEvent(order, 0, ""));
					}
				}
			}
		});
		amendMenu.setText("Amend");
		amendMenu.setActionCommand("amend");

		splitMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				if (splitAction == null || !splitAction.isEnabled())
					return;
				Order order = (Order) tableOrder.getDataByIndex(tableOrder
						.getSelectedRow());
				if (order == null) {
					Utils.showMessage("Please select order to split", form);
				} else {
				//	log.info("user selected split from popup menu, orderid: "	+ order.getId());
					if ((order.getStatus().equals(Order.C_FULL_MATCH) && order
							.getSplitBy().equals(""))
							|| ((order.getStatus().equals(Order.C_AMEND) || order
									.getStatus().equals(Order.C_WITHDRAW)) && order
									.getDoneVol().doubleValue() > 0)) {
						if (splitAction != null)
							splitAction.actionPerformed(new ActionEvent(order,
									0, ""));
					}
				}
			}
		});
		splitMenu.setText("Split Trade");
		splitMenu.setActionCommand("split");

		propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				tableOrder.showProperties();
			}
		});
		propertiesMenu.setText("Properties");

		popupMenu.add(resendMenu);
		popupMenu.add(withdrawMenu);
		popupMenu.add(amendMenu);

		popupMenu.addSeparator();
		popupMenu.add(splitMenu);
		popupMenu.addSeparator();
		popupMenu.add(propertiesMenu);
		popupMenu.add(printExcelMenu);
	}

	private Order genOrder(Order orderCopy) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByKey(
						new String[] { getAccountId(orderCopy.getTradingId()) });
		Stock sec = (Stock) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK)
				.getDataByKey(new String[] { orderCopy.getStock() });
		Order order = new Order();
		order.setOrderIDContra("");
		order.setId(OrderIDGenerator.gen(acc.getTradingId()));
		order.setOrderDate(OrderIDGenerator.getTime());
		order.setExchange(orderCopy.getExchange());
		order.setBoard(orderCopy.getBoard());
		order.setBroker(OrderIDGenerator.getBrokerCode());
		order.setBOS(orderCopy.getBOS());
		order.setOrdType(OrderIDGenerator.getLimitOrder());

		order.setOrderType(orderCopy.getOrderType());
		order.setStock(orderCopy.getStock());
		order.setIsAdvertising("0");
		order.setTradingId(orderCopy.getTradingId());

		order.setInvType(orderCopy.getInvType());
		order.setCurrency(orderCopy.getCurrency());
		order.setContraBroker("");
		order.setContraUser("");
		order.setPrice(orderCopy.getPrice());
		order.setLot(new Double(orderCopy.getLot()));
		order.setVolume(new Double(sec.getLotSize().doubleValue()
				* orderCopy.getLot()));

		Date baskettime = orderCopy.getBasketTime();
		Date currentDate = OrderIDGenerator.getTime();

		if (baskettime != null && baskettime.getTime() > currentDate.getTime()) {
			order.setIsBasketOrder(orderCopy.getIsBasketOrder());
			order.setBasketTime(orderCopy.getBasketTime());
		} else {
			order.setIsBasketOrder("0");
		}

		order.setStatus(Order.C_ENTRY);
		order.setMarketOrderId("");
		order.setDoneVol(new Double(0));
		order.setDoneLot(new Double(0));
		order.setCounter(orderCopy.getCounter());
		order.setSplitTo(orderCopy.getSplitTo());
		order.setComplianceId(acc.getComlianceId());
		order.setAccId(acc.getAccId());
		order.setBalVol(orderCopy.getBalVol());
		order.setBalLot(orderCopy.getBalLot());
		order.setTradeNo("0");
		order.setUser(((IEQTradeApp) apps).getTradingEngine().getUserId()
				.toUpperCase());
		order.setIsFloor("0");
		order.setMarketRef(order.getId());
		return order;
	}

	String getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { alias },
						new int[] { Account.C_TRADINGID });
		return (acc != null ? acc.getAccId() : "");
	}

	public Vector getSelected() {
		Vector v = ((CustomSelectionModel) tableOrder.getTable()
				.getSelectionModel()).getVSelected();
		Vector result = new Vector(v.size());
		for (int i = 0; i < v.size(); i++) {
			result.addElement(((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ORDER)
					.getDataByIndex(
							tableOrder.getMappedRow(((Integer) v.elementAt(i))
									.intValue())));
		}
		return result;
	}

	protected boolean isAllTemporary(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order o = (Order) v.elementAt(i);
			if (!o.getStatus().equals(Order.C_TEMPORARY))
				return false;
		}
		return result;
	}

	protected boolean isAllQueue(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order order = (Order) v.elementAt(i);
			// if (!o.getIsBasketOrder().equals("1"))
			if (!(order.getStatus().equals(Order.C_OPEN)
					|| order.getStatus().equals(Order.C_PARTIAL_MATCH) || (order
					.getStatus().equals(Order.C_ENTRY) && order
					.getIsBasketOrder().equals("1"))))
				return false;
		}
		return result;
	}

	protected boolean isAllOpen(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order o = (Order) v.elementAt(i);
			if (!o.getStatus().equals(Order.C_OPEN)
					&& !o.getStatus().equals(Order.C_PARTIAL_MATCH))
				return false;
		}
		return result;
	}

	protected boolean isValidRow(int x, int y, boolean ctrlDown) {
		return isValidRowMulti(x, y, ctrlDown);
	}

	protected boolean isValidRowMulti(int x, int y, boolean ctrlDown) {
		int nRow = -1;
		String status = "";
		Order order = new Order();
		order.setDoneLot(new Double(0));
		try {
			JTable tblViewList = tableOrder.getTable();
			nRow = tblViewList.rowAtPoint(new Point(x, y));

			if (nRow >= 0) {
				Vector vselected = getSelected();
				nRow = vselected.size();
				if (isAllTemporary(vselected)) {
					status = Order.C_TEMPORARY;
				} else if (isAllOpen(vselected)) {
					status = Order.C_OPEN;
				} else if (isAllRejected(vselected)) {
					status = Order.C_REJECTED;
				} else if (isAllQueue(vselected)) {
					status = Order.C_ENTRY;
				} else
					status = "";

				if (vselected.size() == 1) {
					Order data = (Order) vselected.get(0);
					status = data.getStatus();
					order = data;
					if (status.equals(Order.C_ENTRY))
						status = "";
				}
				// log.info("status "+status);
				
				 * if (ctrlDown) { if (isAllTemporary(getSelected())) { status =
				 * Order.C_TEMPORARY; } else if (isAllOpen(getSelected())) {
				 * status = Order.C_OPEN; } else if
				 * (isAllRejected(getSelected())) { status = Order.C_REJECTED; }
				 * else status = ""; } else {
				 * tblViewList.setRowSelectionInterval(nRow, nRow);
				 * tblViewList.getSelectionModel().addSelectionInterval(nRow,
				 * nRow); Order data = (Order) tableOrder.getDataByIndex(nRow);
				 * status = data.getStatus(); order = data; }
				 }

			for (int i = 0; i < popupMenu.getComponentCount(); i++) {
				Object tool = popupMenu.getComponent(i);
				if (tool instanceof JMenuItem) {
					if (((JMenuItem) tool).getActionCommand()
							.equals("withdraw")) {
						((JMenuItem) tool)
								.setEnabled((withdrawAction != null
										&& withdrawAction.isEnabled()
										&& nRow >= 0 && (status
										.equals(Order.C_OPEN)
										|| status
												.equals(Order.C_SENDING_TEMPORARY)
										|| status.equals(Order.C_PARTIAL_MATCH)
										|| (order.getStatus().equals(
												Order.C_ENTRY) && order
												.getIsBasketOrder().equals("1")) || status
										.equals(Order.C_ENTRY))));
						// ((JMenuItem)tool).setVisible((withdrawAction!=null &&
						// withdrawAction.isEnabled() && nRow>=0 &&
						// (status.equals(Order.C_OPEN) ||
						// status.equals(Order.C_PARTIAL_MATCH) || (
						// order.getStatus().equals(Order.C_ENTRY) &&
						// order.getIsBasketOrder().equals("1")))));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"amend")) {
						((JMenuItem) tool)
								.setEnabled((!ctrlDown && amendAction != null
										&& amendAction.isEnabled() && nRow == 1 && (status
										.equals(Order.C_OPEN) || status
										.equals(Order.C_PARTIAL_MATCH))));
						// ((JMenuItem)tool).setVisible((!ctrlDown &&
						// amendAction!=null && amendAction.isEnabled() &&
						// nRow>=0 && (status.equals(Order.C_OPEN) ||
						// status.equals(Order.C_PARTIAL_MATCH))));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"split")) {
						((JMenuItem) tool)
								.setEnabled((!ctrlDown
										&& splitAction != null
										&& splitAction.isEnabled()
										&& nRow == 1
										&& ((status.equals(Order.C_FULL_MATCH) && order
												.getSplitBy().equals(""))
												|| status
														.equals(Order.C_WITHDRAW) || status
													.equals(Order.C_AMEND)) && order
										.getDoneVol().doubleValue() > 0));
						// ((JMenuItem)tool).setVisible((splitAction!=null &&
						// splitAction.isEnabled() && nRow>=0 &&
						// (status.equals(Order.C_FULL_MATCH) ||
						// status.equals(Order.C_WITHDRAW) ||
						// status.equals(Order.C_AMEND)) &&
						// order.getDoneLot().doubleValue()>0 ));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"resend")) {
						((JMenuItem) tool).setEnabled((status
								.equals(Order.C_REJECTED))
								|| status.equals(Order.C_REJECTED2)
								&& nRow >= 0
								&& order.getOrderIDContra().isEmpty());
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"printExcel")) {
						((JMenuItem) tool).setEnabled(true);
					}
				}
			}
			return true;
		} catch (Exception z) {
			return false;
		}
	}

	private boolean isAllRejected(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order o = (Order) v.elementAt(i);

			if ((o.getStatus().equals(Order.C_REJECTED) || o.getStatus()
					.equals(Order.C_REJECTED2))
					&& !o.getOrderIDContra().isEmpty()) {
				return false;
			} else if (o.getStatus().equals(Order.C_REJECTED)
					|| o.getStatus().equals(Order.C_REJECTED2)) {

			} else {
				return false;
			}
		}
		return result;
	}

	protected boolean isValidRowSingle(int x, int y) {
		int nRow = -1;
		String status = "";
		Order order = new Order();
		order.setDoneLot(new Double(0));
		try {
			JTable tblViewList = tableOrder.getTable();
			nRow = tblViewList.rowAtPoint(new Point(x, y));

			if (nRow >= 0) {
				tblViewList.setRowSelectionInterval(nRow, nRow);
				Order data = (Order) tableOrder.getDataByIndex(nRow);
				status = data.getStatus();
				order = data;
			}

			for (int i = 0; i < popupMenu.getComponentCount(); i++) {
				Object tool = popupMenu.getComponent(i);
				if (tool instanceof JMenuItem) {
					if (((JMenuItem) tool).getActionCommand()
							.equals("withdraw")) {
						((JMenuItem) tool)
								.setEnabled((withdrawAction != null
										&& withdrawAction.isEnabled()
										&& nRow >= 0 && (status
										.equals(Order.C_OPEN)
										|| status.equals(Order.C_PARTIAL_MATCH) || (order
										.getStatus().equals(Order.C_ENTRY) && order
										.getIsBasketOrder().equals("1")))));
						((JMenuItem) tool)
								.setVisible((withdrawAction != null
										&& withdrawAction.isEnabled()
										&& nRow >= 0 && (status
										.equals(Order.C_OPEN)
										|| status.equals(Order.C_PARTIAL_MATCH) || (order
										.getStatus().equals(Order.C_ENTRY) && order
										.getIsBasketOrder().equals("1")))));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"amend")) {
						((JMenuItem) tool)
								.setEnabled((amendAction != null
										&& amendAction.isEnabled() && nRow >= 0 && (status
										.equals(Order.C_OPEN) || status
										.equals(Order.C_PARTIAL_MATCH))));
						((JMenuItem) tool)
								.setVisible((amendAction != null
										&& amendAction.isEnabled() && nRow >= 0 && (status
										.equals(Order.C_OPEN) || status
										.equals(Order.C_PARTIAL_MATCH))));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"split")) {
						((JMenuItem) tool)
								.setEnabled((splitAction != null
										&& splitAction.isEnabled()
										&& nRow >= 0
										&& ((status.equals(Order.C_FULL_MATCH) && order
												.getSplitBy().equals(""))
												|| status
														.equals(Order.C_WITHDRAW) || status
													.equals(Order.C_AMEND)) && order
										.getDoneVol().doubleValue() > 0));
						((JMenuItem) tool)
								.setVisible((splitAction != null
										&& splitAction.isEnabled()
										&& nRow >= 0
										&& ((status.equals(Order.C_FULL_MATCH) && order
												.getSplitBy().equals(""))
												|| status
														.equals(Order.C_WITHDRAW) || status
													.equals(Order.C_AMEND)) && order
										.getDoneVol().doubleValue() > 0));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"resend")) {

						boolean isenable = false;
					//	log.info(" cek " + order.getOrderIDContra().isEmpty()	+ " " + order.getStatus());
						if ((order.getStatus().equals(Order.C_REJECTED) || order
								.getStatus().equals(Order.C_REJECTED2))
								&& !order.getOrderIDContra().isEmpty()) {
							//log.info("cek false");
							isenable = false;
						} else if (status.equals(Order.C_REJECTED)
								|| status.equals(Order.C_REJECTED2)) {
							//log.info("cek true");
							isenable = true;
						}
						((JMenuItem) tool).setEnabled(isenable);
					}
				}
			}
			return true;
		} catch (Exception z) {
			return false;
		}
	}

	private String genConfirm(Order order) {
		String result = "";
		result = result.concat(order.getTradingId()).concat(" ");
		result = result.concat(order.getBOS().equals(Order.C_BUY) ? "Buy "
				: "Sell ");
		result = result.concat(order.getStock().concat(" "));
		result = result
				.concat("at ")
				.concat(OrderRender.formatter.format((Double) order.getPrice()))
				.concat(" ");
		result = result.concat(
				OrderRender.formatter.format((Double) order.getLot())).concat(
				" lot\n");
		return result;
	}

	protected class RowChangeList implements TableModelListener {

		@Override
		public void tableChanged(TableModelEvent e) {
			int rowcount = tableOrder.getTable().getRowCount();
			// tableOrder.getTable().
		}

	}

	protected class RowChanged implements TableModelListener {

		public void tableChanged(final TableModelEvent e) {
			double count = 0, ocount = 0, ovalue = 0, dcount = 0, dvalue = 0, tlot = 0, tvalue = 0;
			int size = tableOrder.getTable().getRowCount();

			for (int i = 0; i < size; i++) {
				Order order = (Order) tableOrder.getDataByIndex(i);
				count++;
				tlot = tlot + order.getLot().doubleValue();
				tvalue = tvalue
						+ (order.getVolume().doubleValue() * order.getPrice()
								.doubleValue());
				if (order.getStatus().equals(Order.C_PARTIAL_MATCH)
						|| order.getStatus().equals(Order.C_OPEN)) {
					ocount++;
					ovalue = ovalue
							+ (order.getBalVol().doubleValue() * order
									.getPrice().doubleValue());
				}
				if (order.getStatus().equals(Order.C_PARTIAL_MATCH)
						|| order.getStatus().equals(Order.C_FULL_MATCH)) {
					dcount++;
					dvalue = dvalue
							+ (order.getDoneVol().doubleValue() * order
									.getPrice().doubleValue());
				}
			}
			tCount.setValue(new Double(count));
			oCount.setValue(new Double(ocount));
			oValue.setValue(new Double(ovalue));
			dCount.setValue(new Double(dcount));
			dValue.setValue(new Double(dvalue));
			tLot.setValue(new Double(tlot));
			tValue.setValue(new Double(tvalue));

			if (e.getType() == TableModelEvent.INSERT) {
				Rectangle r = tableOrder.getTable().getCellRect(size - 1, 0,
						true);
				tableOrder.scrollRectToVisible(r);
			}

		}

		
		 * public void tableChanged(final TableModelEvent e) { double count = 0,
		 * ocount = 0, ovalue = 0, dcount = 0, dvalue = 0, tlot = 0, tvalue = 0;
		 * int size = tableOrder.getTable().getRowCount(); for (int i = 0; i <
		 * size; i++) { Order order = (Order) tableOrder.getDataByIndex(i);
		 * count++; tlot = tlot + order.getLot().doubleValue(); tvalue = tvalue
		 * + (order.getVolume().doubleValue() * order.getPrice()
		 * .doubleValue()); if (order.getStatus().equals(Order.C_PARTIAL_MATCH)
		 * || order.getStatus().equals(Order.C_OPEN)) { ocount++; ovalue =
		 * ovalue + (order.getBalVol().doubleValue() * order
		 * .getPrice().doubleValue()); } if
		 * (order.getStatus().equals(Order.C_PARTIAL_MATCH) ||
		 * order.getStatus().equals(Order.C_FULL_MATCH)) { dcount++; dvalue =
		 * dvalue + (order.getDoneVol().doubleValue() * order
		 * .getPrice().doubleValue()); } tCount.setValue(new Double(count));
		 * oCount.setValue(new Double(ocount)); oValue.setValue(new
		 * Double(ovalue)); dCount.setValue(new Double(dcount));
		 * dValue.setValue(new Double(dvalue)); tLot.setValue(new Double(tlot));
		 * tValue.setValue(new Double(tvalue)); } }
		 
	}

	protected class RowUpdate implements TableModelListener {
		public void tableChanged(final TableModelEvent e) {
			if (e != null) {
				boolean move = true;
				if (move) {
					final int first = e.getFirstRow();
					final int last = e.getLastRow();
					if (first == last) {
						final int n = tableOrder.getTableRow((first));
						if (n > -1) {
							tableOrder.getTable().clearSelection();
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									synchronized (tableOrder.getTable()) {
										try {
											Rectangle rec = tableOrder
													.getTable().getCellRect(n,
															2, false);
											tableOrder.getTable()
													.scrollRectToVisible(rec);
											// tableOrder.getTable()
											// .setRowSelectionInterval(n,
											// n);
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									}
								}
							});
						}
					}
				}
			}
		}
	}

	protected boolean isValidRow(int x, int y) {
		return true;
	}

	public class MyMouseAdapter extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) {

				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}

		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}
	};

	public class MyCustomMouseAdapter extends CustomMouseAdapter {
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) {

				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}

		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}
	}

	public void executeAction(String actionName) {
		tableOrder.getActionMap().get(actionName).actionPerformed(null);
	}

	private boolean isSuperUser() {
		// return true;
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
//		System.out.println(sales.getProfileId() + " ...");
		return sales != null ? sales.getProfileId().toUpperCase()
				.equals("SUPERUSER") : false;
		// return false;
	}

	protected JFileChooser jfc = null;
	protected ExcelFileFilter filterFile = null;

	protected void savetoFile() {
		if (jfc == null) {
			filterFile = new ExcelFileFilter();
			filterFile.addExtension("xls");
			filterFile.setDescription(" Microsoft Excel ");
			jfc = new JFileChooser(new File("C:\\"));
			jfc.setFileFilter(filterFile);
			jfc.setAcceptAllFileFilterUsed(false);
			jfc.setDialogTitle("Save Order to....");
			jfc.setDialogType(JFileChooser.SAVE_DIALOG);
		}
		String filename = "";
		int returnVal = jfc.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File ffile = jfc.getSelectedFile();
			filename = ffile.getAbsolutePath();
			if (!filename.endsWith(".xls")) {
				filename = filename + ".xls";
			}
		} else {
			return;
		}
		int i = tableOrder.getTable().getRowCount();
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(filename));
			Hashtable prop = tableOrder.getTableProperties();
			int[] order = (int[]) prop.get("order");
			int[] width = (int[]) prop.get("width");
			String header = "";
			for (int k = 0; k < order.length; k++) {
				if (width[k] != 0) {
					header = header + OrderDef.getHeader().elementAt(order[k])
							+ "\t";
				}
			}
			out.write(header + "\n");
			for (int j = 0; j < i; j++) {
				Column dat = (Column) tableOrder.getTable().getValueAt(j, 0);
				Order ord = (Order) dat.getSource();
				String row = "";
				for (int k = 0; k < order.length; k++) {
					if (width[k] != 0) {
						switch (order[k]) {
						case Order.C_STATUSID:
							row = row
									+ OrderRender.hashStatus.get(ord
											.getData(order[k])) + "\t";
							break;
						case Order.C_BUYORSELL:
							row = row
									+ OrderRender.hashSide.get(ord
											.getData(order[k])) + "\t";
							break;
						case Order.C_ORDERTYPE:
							row = row
									+ OrderRender.hashType.get(ord
											.getData(order[k])) + "\t";
							break;
						case Order.C_ORDTYPE:
							row = row + "limited order\t";
							break;
						default:
							row = row + ord.getData(order[k]) + "\t";
							break;
						}
					}
				}
				out.write(row + "\n");
			}
			out.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public class ExcelFileFilter extends FileFilter {
		private Hashtable filters = null;
		private String description = null;
		private String fullDescription = null;
		private boolean useExtensionsInDescription = true;

		public ExcelFileFilter() {
			this.filters = new Hashtable();
		}

		public ExcelFileFilter(String extension) {
			this(extension, null);
		}

		public ExcelFileFilter(String extension, String description) {
			this();
			if (extension != null)
				addExtension(extension);
			if (description != null)
				setDescription(description);
		}

		public ExcelFileFilter(String[] filters) {
			this(filters, null);
		}

		public ExcelFileFilter(String[] filters, String description) {
			this();
			for (int i = 0; i < filters.length; i++) {
				// add filters one by one
				addExtension(filters[i]);
			}
			if (description != null)
				setDescription(description);
		}

		public boolean accept(File f) {
			if (f != null) {
				if (f.isDirectory()) {
					return true;
				}
				String extension = getExtension(f);
				if (extension != null && filters.get(getExtension(f)) != null) {
					return true;
				}
				;
			}
			return false;
		}

		public String getExtension(File f) {
			if (f != null) {
				String filename = f.getName();
				int i = filename.lastIndexOf('.');
				if (i > 0 && i < filename.length() - 1) {
					return filename.substring(i + 1).toLowerCase();
				}
				;
			}
			return null;
		}

		public void addExtension(String extension) {
			if (filters == null) {
				filters = new Hashtable(5);
			}
			filters.put(extension.toLowerCase(), this);
			fullDescription = null;
		}

		public String getDescription() {
			if (fullDescription == null) {
				if (description == null || isExtensionListInDescription()) {
					fullDescription = description == null ? "(" : description + " (";
					// build the description from the extension list
					Enumeration extensions = filters.keys();
					if (extensions != null) {
						fullDescription += "." + (String) extensions.nextElement();
						while (extensions.hasMoreElements()) {
							fullDescription += ", ." + (String) extensions.nextElement();
						}
					}
					fullDescription += ")";
				} else {
					fullDescription = description;
				}
			}
			return fullDescription;
		}

		public void setDescription(String description) {
			this.description = description;
			fullDescription = null;
		}

		public void setExtensionListInDescription(boolean b) {
			useExtensionsInDescription = b;
			fullDescription = null;
		}

		public boolean isExtensionListInDescription() {
			return useExtensionsInDescription;
		}
	}

	public static void validnot(boolean sl, String userid) {
//		System.out.println("dsa " + sl);
		usernotif = userid;
		if (!sl) {
			btnNotif.setEnabled(false);
		} else {
			btnNotif.setEnabled(true);
		}

	}

	public boolean isFilterClientAdded(String client) {
		try {
		String sf = ((IEQTradeApp) apps).getTradingEngine().getConnection()
				.loadFilterByClient();
		String[] sp = sf.split("#");
		for (int i = 0; i < sp.length; i++)
			if (sp[i].trim().toLowerCase().equals(client))
				return true;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}
	
	public void loadData(){
		
		String sfilter = (((TradingEngine) ((IEQTradeApp) apps)
				.getTradingEngine())).getConnection().loadFilterByClient();

		//log.info("filter-- " + sfilter);

		if (sfilter != null && !sfilter.isEmpty()) {

			String[] sp = sfilter.split("#");
			filterclient = new Vector();
			for (String ss : sp) {
				Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { ss },
						new int[] { Account.C_ACCID });
				filterclient.add(acc.getAccId());
			}
		}else if(isSuperUser() && sfilter == null){
			filterclient = new Vector();
		}
		else{
			filterclient = null;
		}
	}
}*/