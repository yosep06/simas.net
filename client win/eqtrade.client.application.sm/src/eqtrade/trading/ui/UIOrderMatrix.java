package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

import org.eclipse.swt.internal.ole.win32.IServiceProvider;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.demo.Mapping;
import com.vollux.framework.Action;
import com.vollux.framework.UI;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JTextLabel;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.StockSummary;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.OrderMatrixData;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.OrderIDGenerator;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.Stock;
import eqtrade.trading.model.OrderMatrix;
import eqtrade.trading.model.UserProfile;

public class UIOrderMatrix extends UI {
	JButton btnLoad, btnSave, btnSendAll, btnResetAll;
	JLabel Label, lblBS, lblStock, lblPrice, lblLots, lblAmount, lblClient, lblExecute;
	JTextField fieldLabel, fieldTimer;
	JCheckBox checkBasket;

	JRadioButton Buy1, Buy2, Buy3, Buy4, Buy5;
	JRadioButton Sell1, Sell2, Sell3, Sell4, Sell5;

	JButton btnReset1, btnReset2, btnReset3, btnReset4, btnReset5;
	JButton btnSend1, btnSend2, btnSend3, btnSend4, btnSend5;

	JDropDown fieldStock1, fieldStock2, fieldStock3, fieldStock4, fieldStock5;
	JSpinner fieldPrice1, fieldPrice2, fieldPrice3, fieldPrice4, fieldPrice5;
	JSpinner fieldLot1, fieldLot2, fieldLot3, fieldLot4, fieldLot5;
	JNumber fieldAmount1, fieldAmount2, fieldAmount3, fieldAmount4, fieldAmount5;
	JDropDown comboClient1, comboClient2, comboClient3, comboClient4, comboClient5;
	JDropDown comboName1, comboName2, comboName3, comboName4, comboName5;

	ButtonGroup bs1Group, bs2Group, bs3Group, bs4Group, bs5Group;

	private JTable table;
	
	JPanel topPanel;
	JPanel entPanel;
	JPanel bS1Panel, bs2Panel, bs3Panel, bs4Panel, bs5Panel;
	
	private static String omid1 = null, omid2 = null, omid3 = null, omid4 = null, omid5 = null;
	
	private Action sendAction = null;
	private static SimpleDateFormat dateFormatAll = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	private static NumberFormat formatter = new DecimalFormat("#,##0 ");
	private static SimpleDateFormat formatdate = new SimpleDateFormat("HH:mm:ss");
	
	private int isClick1 = 0, isClick2 = 0, isClick3 = 0, isClick4 = 0, isClick5 = 0;
	private int isupdate = 0;
	private int isDelete1 = 0, isDelete2 = 0, isDelete3 = 0, isDelete4 = 0, isDelete5 = 0;
	
	private OrderMatrixData[] arrayomdata;

	public UIOrderMatrix(String app) {
		super("Order Matrix", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_ORDERMATRIX);
	}

	@Override
	protected void build() {
		sendAction = apps.getAction().get(TradingAction.A_SHOWBUYORDER);

		fieldStock1 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		fieldStock2 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		fieldStock3 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		fieldStock4 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		fieldStock5 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		
		fieldAmount1 = new JNumber(Double.class, 0, 0, true, true);
		fieldAmount2 = new JNumber(Double.class, 0, 0, true, true);
		fieldAmount3 = new JNumber(Double.class, 0, 0, true, true);
		fieldAmount4 = new JNumber(Double.class, 0, 0, true, true);
		fieldAmount5 = new JNumber(Double.class, 0, 0, true, true);

		fieldAmount1.setEnabled(false);
		fieldAmount2.setEnabled(false);
		fieldAmount3.setEnabled(false);
		fieldAmount4.setEnabled(false);
		fieldAmount5.setEnabled(false);

		fieldTimer = new JTextField();
		fieldTimer.setEnabled(false);
		checkBasket = new JCheckBox();
		checkBasket.setOpaque(false);

		IDXPriceSpinnerModel priceModel1 = new IDXPriceSpinnerModel();
		IDXPriceSpinnerModel priceModel2 = new IDXPriceSpinnerModel();
		IDXPriceSpinnerModel priceModel3 = new IDXPriceSpinnerModel();
		IDXPriceSpinnerModel priceModel4 = new IDXPriceSpinnerModel();
		IDXPriceSpinnerModel priceModel5 = new IDXPriceSpinnerModel();

		fieldPrice1 = new JSpinner(priceModel1);
		fieldPrice2 = new JSpinner(priceModel2);
		fieldPrice3 = new JSpinner(priceModel3);
		fieldPrice4 = new JSpinner(priceModel4);
		fieldPrice5 = new JSpinner(priceModel5);

		SpinnerModel lotModel1 = new SpinnerNumberModel(new Double(0),new Double(0), new Double(1000000), new Double(1));
		SpinnerModel lotModel2 = new SpinnerNumberModel(new Double(0),new Double(0), new Double(1000000), new Double(1));
		SpinnerModel lotModel3 = new SpinnerNumberModel(new Double(0),new Double(0), new Double(1000000), new Double(1));
		SpinnerModel lotModel4 = new SpinnerNumberModel(new Double(0),new Double(0), new Double(1000000), new Double(1));
		SpinnerModel lotModel5 = new SpinnerNumberModel(new Double(0),new Double(0), new Double(1000000), new Double(1));

		fieldLot1 = new JSpinner(lotModel1);
		fieldLot2 = new JSpinner(lotModel2);
		fieldLot3 = new JSpinner(lotModel3);
		fieldLot4 = new JSpinner(lotModel4);
		fieldLot5 = new JSpinner(lotModel5);

		comboClient1 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboClient2 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboClient3 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboClient4 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboClient5 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		
		comboName1 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_NAME, true);
		comboName2 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_NAME, true);
		comboName3 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_NAME, true);
		comboName4 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_NAME, true);
		comboName5 = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_NAME, true);
		
		comboName1.setEnabled(false);
		comboName2.setEnabled(false);
		comboName3.setEnabled(false);
		comboName4.setEnabled(false);
		comboName5.setEnabled(false);
		
		Buy1 = new JRadioButton("B");
		Buy2 = new JRadioButton("B");
		Buy3 = new JRadioButton("B");
		Buy4 = new JRadioButton("B");
		Buy5 = new JRadioButton("B");

		Sell1 = new JRadioButton("S");
		Sell2 = new JRadioButton("S");
		Sell3 = new JRadioButton("S");
		Sell4 = new JRadioButton("S");
		Sell5 = new JRadioButton("S");

		bs1Group = new ButtonGroup();
		bs2Group = new ButtonGroup();
		bs3Group = new ButtonGroup();
		bs4Group = new ButtonGroup();
		bs5Group = new ButtonGroup();

		bs1Group.add(Buy1);
		bs1Group.add(Sell1);
		bs2Group.add(Buy2);
		bs2Group.add(Sell2);
		bs3Group.add(Buy3);
		bs3Group.add(Sell3);
		bs4Group.add(Buy4);
		bs4Group.add(Sell4);
		bs5Group.add(Buy5);
		bs5Group.add(Sell5);

		btnLoad = new JButton("Load");
		btnSave = new JButton("Save");
		btnSendAll = new JButton("Send All");
		btnResetAll = new JButton("Reset All");

		fieldLabel = new JTextField();

		btnReset1 = new JButton("Reset");
		btnReset2 = new JButton("Reset");
		btnReset3 = new JButton("Reset");
		btnReset4 = new JButton("Reset");
		btnReset5 = new JButton("Reset");

		btnSend1 = new JButton("Send");
		btnSend2 = new JButton("Send");
		btnSend3 = new JButton("Send");
		btnSend4 = new JButton("Send");
		btnSend5 = new JButton("Send");

		topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		fieldLabel.setColumns(8);
		fieldTimer.setColumns(10);

		topPanel.add(btnLoad);
		topPanel.add(btnSave);
		topPanel.add(btnSendAll);
		topPanel.add(btnResetAll);
		topPanel.add(new JTextLabel("Label"));
		topPanel.add(fieldLabel);
		topPanel.add(new JLabel("Put in Basket"));
		topPanel.add(checkBasket);
		topPanel.add(fieldTimer);

		bS1Panel = new JPanel();
		FormLayout l = new FormLayout("5dlu,30px,2dlu,30px,1dlu,90px,2dlu,100px,2dlu,50px,2dlu,100px,2dlu,80px,2dlu,200px,2dlu,65px,2dlu,65px","2dlu,pref,2dlu");
		CellConstraints c1 = new CellConstraints();
		PanelBuilder bs1 = new PanelBuilder(l, bS1Panel);
		bs1.add(Buy1, c1.xy(2, 2));
		bs1.add(Sell1, c1.xy(4, 2));
		bs1.add(fieldStock1, c1.xy(6, 2));
		bs1.add(fieldPrice1, c1.xy(8, 2));
		bs1.add(fieldLot1, c1.xy(10, 2));
		bs1.add(fieldAmount1, c1.xy(12, 2));
		bs1.add(comboClient1, c1.xy(14, 2));
		bs1.add(comboName1, c1.xy(16, 2));
		bs1.add(btnReset1, c1.xy(18, 2));
		bs1.add(btnSend1, c1.xy(20, 2));

		bs2Panel = new JPanel();
		FormLayout l2 = new FormLayout("5dlu,30px,2dlu,30px,1dlu,90px,2dlu,100px,2dlu,50px,2dlu,100px,2dlu,80px,2dlu,200px,2dlu,65px,2dlu,65px","2dlu,pref,2dlu");
		CellConstraints c2 = new CellConstraints();
		PanelBuilder bs2 = new PanelBuilder(l2, bs2Panel);
		bs2.add(Buy2, c2.xy(2, 2));
		bs2.add(Sell2, c2.xy(4, 2));
		bs2.add(fieldStock2, c2.xy(6, 2));
		bs2.add(fieldPrice2, c2.xy(8, 2));
		bs2.add(fieldLot2, c2.xy(10, 2));
		bs2.add(fieldAmount2, c2.xy(12, 2));
		bs2.add(comboClient2, c2.xy(14, 2));
		bs2.add(comboName2, c2.xy(16, 2));
		bs2.add(btnReset2, c2.xy(18, 2));
		bs2.add(btnSend2, c2.xy(20, 2));

		bs3Panel = new JPanel();
		FormLayout l3 = new FormLayout("5dlu,30px,2dlu,30px,1dlu,90px,2dlu,100px,2dlu,50px,2dlu,100px,2dlu,80px,2dlu,200px,2dlu,65px,2dlu,65px","2dlu,pref,2dlu");
		CellConstraints c3 = new CellConstraints();
		PanelBuilder bs3 = new PanelBuilder(l3, bs3Panel);
		bs3.add(Buy3, c3.xy(2, 2));
		bs3.add(Sell3, c3.xy(4, 2));
		bs3.add(fieldStock3, c3.xy(6, 2));
		bs3.add(fieldPrice3, c3.xy(8, 2));
		bs3.add(fieldLot3, c3.xy(10, 2));
		bs3.add(fieldAmount3, c3.xy(12, 2));
		bs3.add(comboClient3, c3.xy(14, 2));
		bs3.add(comboName3, c3.xy(16, 2));
		bs3.add(btnReset3, c3.xy(18, 2));
		bs3.add(btnSend3, c3.xy(20, 2));

		bs4Panel = new JPanel();
		FormLayout l4 = new FormLayout("5dlu,30px,2dlu,30px,1dlu,90px,2dlu,100px,2dlu,50px,2dlu,100px,2dlu,80px,2dlu,200px,2dlu,65px,2dlu,65px","2dlu,pref,2dlu");
		CellConstraints c4 = new CellConstraints();
		PanelBuilder bs4 = new PanelBuilder(l4, bs4Panel);
		bs4.add(Buy4, c4.xy(2, 2));
		bs4.add(Sell4, c4.xy(4, 2));
		bs4.add(fieldStock4, c4.xy(6, 2));
		bs4.add(fieldPrice4, c4.xy(8, 2));
		bs4.add(fieldLot4, c4.xy(10, 2));
		bs4.add(fieldAmount4, c4.xy(12, 2));
		bs4.add(comboClient4, c4.xy(14, 2));
		bs4.add(comboName4, c4.xy(16, 2));
		bs4.add(btnReset4, c4.xy(18, 2));
		bs4.add(btnSend4, c4.xy(20, 2));
		
		bs5Panel = new JPanel();
		FormLayout l5 = new FormLayout("5dlu,30px,2dlu,30px,1dlu,90px,2dlu,100px,2dlu,50px,2dlu,100px,2dlu,80px,2dlu,200px,2dlu,65px,2dlu,65px","2dlu,pref,2dlu");
		CellConstraints c5 = new CellConstraints();
		PanelBuilder bs5 = new PanelBuilder(l5, bs5Panel);
		bs5.add(Buy5, c4.xy(2, 2));
		bs5.add(Sell5, c4.xy(4, 2));
		bs5.add(fieldStock5, c5.xy(6, 2));
		bs5.add(fieldPrice5, c5.xy(8, 2));
		bs5.add(fieldLot5, c5.xy(10, 2));
		bs5.add(fieldAmount5, c5.xy(12, 2));
		bs5.add(comboClient5, c5.xy(14, 2));
		bs5.add(comboName5, c5.xy(16, 2));
		bs5.add(btnReset5, c5.xy(18, 2));
		bs5.add(btnSend5, c5.xy(20, 2));

		JPanel labelPanel = new JPanel();
		FormLayout lp = new FormLayout("5dlu,75px,2dlu,75px,2dlu,85px,2dlu,70px,2dlu,100px,2dlu,280px,2dlu,100px","2dlu,pref,2dlu");
		CellConstraints clp = new CellConstraints();
		PanelBuilder blp = new PanelBuilder(lp, labelPanel);
		blp.add(new javax.swing.JLabel("Buy/Sell"), clp.xy(2, 2));
		blp.add(new javax.swing.JLabel("Stock"), clp.xy(4, 2));
		blp.add(new javax.swing.JLabel("Price"), clp.xy(6, 2));
		blp.add(new javax.swing.JLabel("Lots"), clp.xy(8, 2));
		blp.add(new javax.swing.JLabel("Amount"), clp.xy(10, 2));
		blp.add(new javax.swing.JLabel("Client"), clp.xy(12, 2));
		blp.add(new javax.swing.JLabel("Execute"), clp.xy(14, 2));

		Buy1.addActionListener(new MyAction());
		Buy2.addActionListener(new MyAction());
		Buy3.addActionListener(new MyAction());
		Buy4.addActionListener(new MyAction());
		Buy5.addActionListener(new MyAction());

		Sell1.addActionListener(new MyAction());
		Sell2.addActionListener(new MyAction());
		Sell3.addActionListener(new MyAction());
		Sell4.addActionListener(new MyAction());
		Sell5.addActionListener(new MyAction());

		btnSend1.addActionListener(new MyAction());
		btnSend2.addActionListener(new MyAction());
		btnSend3.addActionListener(new MyAction());
		btnSend4.addActionListener(new MyAction());
		btnSend5.addActionListener(new MyAction());

		btnResetAll.addActionListener(new MyAction());
		btnSendAll.addActionListener(new MyAction());
		btnSave.addActionListener(new MyAction());
		btnLoad.addActionListener(new MyAction());

		btnReset1.addActionListener(new MyAction());
		btnReset2.addActionListener(new MyAction());
		btnReset3.addActionListener(new MyAction());
		btnReset4.addActionListener(new MyAction());
		btnReset5.addActionListener(new MyAction());

		Buy1.setOpaque(false);
		Buy2.setOpaque(false);
		Buy3.setOpaque(false);
		Buy4.setOpaque(false);
		Buy5.setOpaque(false);

		Sell1.setOpaque(false);
		Sell2.setOpaque(false);
		Sell3.setOpaque(false);
		Sell4.setOpaque(false);
		Sell5.setOpaque(false);

		registerEventCombo(comboClient1);
		registerEventCombo(comboClient2);
		registerEventCombo(comboClient3);
		registerEventCombo(comboClient4);
		registerEventCombo(comboClient5);

		registerEventCombo(comboName1);
		registerEventCombo(comboName2);
		registerEventCombo(comboName3);
		registerEventCombo(comboName4);
		registerEventCombo(comboName5);

		checkBasket.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (checkBasket.isSelected()) {
					fieldTimer.setEnabled(true);
				} else {
					fieldTimer.setEnabled(false);
				}
			}
		});

		comboClient1.getTextEditor().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {
				comboName1.setSelectedIndex(clientfocusLost(comboClient1.getText()));
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				try {
					comboClient1.showPopup();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		comboClient2.getTextEditor().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {
				comboName2.setSelectedIndex(clientfocusLost(comboClient2
						.getText()));
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				try {
					comboClient2.showPopup();
				} catch (Exception e) {

				}
			}
		});

		comboClient3.getTextEditor().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				comboName3.setSelectedIndex(clientfocusLost(comboClient3
						.getText()));
			}

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboClient3.showPopup();
				} catch (Exception e1) {

				}
			}
		});

		comboClient4.getTextEditor().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				comboName4.setSelectedIndex(clientfocusLost(comboClient4
						.getText()));
			}

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboClient4.showPopup();
				} catch (Exception e1) {
				}
			}
		});
		
		comboClient5.getTextEditor().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				comboName5.setSelectedIndex(clientfocusLost(comboClient5.getText()));
			}

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboClient5.showPopup();
				} catch (Exception e1) {
				}
			}
		});

		fieldStock1.getTextEditor().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {				
				changePrice(fieldStock1);
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				try {
					fieldStock1.showPopup();
				} catch (Exception ex) {
					return;
				}				
			}
		});

		fieldStock2.getTextEditor().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {
				changePrice(fieldStock2);
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				fieldStock2.showPopup();
			}
		});

		fieldStock3.getTextEditor().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {
				changePrice(fieldStock3);
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				fieldStock3.showPopup();
			}
		});

		fieldStock4.getTextEditor().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				changePrice(fieldStock4);
			}

			@Override
			public void focusGained(FocusEvent e) {
				fieldStock4.showPopup();
			}
		});

		fieldStock5.getTextEditor().addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				changePrice(fieldStock5);
			}

			@Override
			public void focusGained(FocusEvent e) {
				fieldStock5.showPopup();
			}
		});
		
		((JSpinner.DefaultEditor) fieldLot1.getEditor()).getTextField().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				try {					
					fieldLot1.commitEdit();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				calculate(fieldPrice1, (Double) fieldLot1.getValue(),(Double) fieldPrice1.getValue(),fieldStock1.getText());
			}

			public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextComponent) {
					final JTextComponent textComponent = ((JTextComponent) e.getSource());
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							textComponent.selectAll();
						}
					});
				}
			}
		});
		
		((JSpinner.DefaultEditor) fieldLot2.getEditor()).getTextField().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				try {
					fieldLot2.commitEdit();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				calculate(fieldPrice2, (Double) fieldLot2.getValue(),(Double) fieldPrice2.getValue(),fieldStock2.getText());
			}

			public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextComponent) {
					final JTextComponent textComponent = ((JTextComponent) e.getSource());
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							textComponent.selectAll();
						}
					});
				}
			}
		});
		
		((JSpinner.DefaultEditor) fieldLot3.getEditor()).getTextField().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				try {
					fieldLot3.commitEdit();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				calculate(fieldPrice3, (Double) fieldLot3.getValue(),
						(Double) fieldPrice3.getValue(),
						fieldStock3.getText());
			}

			public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextComponent) {
					final JTextComponent textComponent = ((JTextComponent) e
							.getSource());
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							textComponent.selectAll();
						}
					});
				}
			}
		});
		
		((JSpinner.DefaultEditor) fieldLot4.getEditor()).getTextField().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				try {
					fieldLot4.commitEdit();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				calculate(fieldPrice4, (Double) fieldLot4.getValue(), (Double) fieldPrice4.getValue(), fieldStock4.getText());
			}

			public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextComponent) {
					final JTextComponent textComponent = ((JTextComponent) e.getSource());
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							textComponent.selectAll();
						}
					});
				}
			}
		});

		((JSpinner.DefaultEditor) fieldLot5.getEditor()).getTextField().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				try {
					fieldLot5.commitEdit();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				calculate(fieldPrice5, (Double) fieldLot5.getValue(), (Double) fieldPrice5.getValue(), fieldStock5.getText());
			}

			public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextComponent) {
					final JTextComponent textComponent = ((JTextComponent) e.getSource());
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							textComponent.selectAll();
						}
					});
				}
			}
		});
		
		pnlContent = new JSkinPnl(new GridLayout(7, 0));
		pnlContent.add(topPanel);
		pnlContent.add(labelPanel);
		pnlContent.add(bS1Panel);
		pnlContent.add(bs2Panel);
		pnlContent.add(bs3Panel);
		pnlContent.add(bs4Panel);
		pnlContent.add(bs5Panel);

		refresh();
	}

	@Override
	public void focus() {
		
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(30, 30, 850, 250));
	}

	@Override
	public void saveSetting() {
		hSetting.put("size",new Rectangle(form.getX(), form.getY(), form.getWidth(), form.getHeight()));

		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),hSetting);
	}

	@Override
	public void refresh() {
		reset();
	}

	public void show() {
		if (!apps.getAction().get(TradingAction.A_SHOWORDERMATRIC).isGranted()) {
			return;
		}
		form.setResizable(false);
		super.show();
	}

	@Override
	public void show(Object param) {
		HashMap p = (HashMap) param;
		isupdate = (Integer) p.get("ISUPDATETRUE");
		fieldLabel.setEnabled(false);
		query(p);
		show();
	}

	private Thread thread;

	private void query(final HashMap param) {
		if (thread != null) {
			thread.stop();
			thread = null;
		}

		thread = new Thread(new Runnable() {
			@Override
			public void run() {
				((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIX).getDataVector().clear();
				((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIX).refresh();
				((IEQTradeApp) apps).getTradingEngine().refreshOrderMatrixList((String) param.get("LABEL"),((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase());

				testTampil(param);

				fieldTimer.setText((String) param.get("BASKETTIME"));
				checkBasket.setSelected(param.get("ISBASKET").equals("1") ? true : false);
			}
		});
		thread.start();
	}

	private Thread test;

	private boolean ulang;

	private void testTampil(final HashMap label) {
		if (test != null) {
			test.stop();
			test = null;
		}
		ulang = true;
		test = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					omid1 = "0";
					omid2 = "0";
					omid3 = "0";
					omid4 = "0";
					omid5 = "0";
					
					while (ulang) {
						Thread.sleep(1000);

						fieldLabel.setText((String) label.get("LABEL"));

						int order = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIX).getRowCount();

						if (order > 0) {
							feed(order);
							ulang = false;
						}
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		test.start();
	}

	private void feed(final int order) {
		reset();
		isupdate = 1;
		fieldTimer.setEnabled(false);		
		for (int i = 0; i < order; i++) {
			OrderMatrix om = (OrderMatrix) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIX).getDataByIndex(i);
			fieldLabel.setText(om.getLabel().toString());
			fieldTimer.setText(om.getBasketTime().toString());	
		
			if(om.getIsBasket().equals("1")) {
				
				checkBasket.setSelected(true);
				fieldTimer.setEnabled(true);
			}
			switch (i) {
				case 0:
					setField1(om);
					break;
				case 1:
					setField2(om);
					break;
				case 2:
					setField3(om);
					break;
				case 3:
					setField4(om);
					break;
				case 4:
					setField5(om);
				default:
					break;
			}
		}
	}

	private void setField1(OrderMatrix om) {
		if (om.getBuyorsell().equals("1")) {
			Buy1.setSelected(true);
			bS1Panel.setBackground(Color.red.darker());
		} else {
			Sell1.setSelected(true);
			bS1Panel.setBackground(Color.green.darker());
		}
		
		fieldStock1.getTextEditor().setText(om.getSecid().toUpperCase());
		fieldPrice1.setValue(om.getPrice());
		fieldLot1.setValue(om.getLot());
		fieldAmount1.setValue(om.getAmount());
		comboClient1.getTextEditor().setText(om.getClient());
		comboName1.setSelectedIndex(clientfocusLost(om.getClient()));
		omid1 = om.getOrdermatrixid().toString().trim();
	}

	private void setField2(OrderMatrix om) {
		if (om.getBuyorsell().equals("1")) {
			Buy2.setSelected(true);
			bs2Panel.setBackground(Color.red.darker());
		} else {
			Sell2.setSelected(true);
			bs2Panel.setBackground(Color.green.darker());
		}
		fieldStock2.getTextEditor().setText(om.getSecid().toUpperCase());
		fieldPrice2.setValue(om.getPrice());
		fieldLot2.setValue(om.getLot());
		fieldAmount2.setValue(om.getAmount());
		comboClient2.getTextEditor().setText(om.getClient());
		comboName2.setSelectedIndex(clientfocusLost(om.getClient()));
		omid2 = om.getOrdermatrixid().toString();
	}

	private void setField3(OrderMatrix om) {
		if (om.getBuyorsell().equals("1")) {
			Buy3.setSelected(true);
			bs3Panel.setBackground(Color.red.darker());
		} else {
			Sell3.setSelected(true);
			bs3Panel.setBackground(Color.green.darker());
		}
		fieldStock3.getTextEditor().setText(om.getSecid().toUpperCase());
		fieldPrice3.setValue(om.getPrice());
		fieldLot3.setValue(om.getLot());
		fieldAmount3.setValue(om.getAmount());
		comboClient3.getTextEditor().setText(om.getClient());
		comboName3.setSelectedIndex(clientfocusLost(om.getClient()));
		omid3 = om.getOrdermatrixid().toString();
	}

	private void setField4(OrderMatrix om) {
		if (om.getBuyorsell().equals("1")) {
			Buy4.setSelected(true);
			bs4Panel.setBackground(Color.red.darker());
		} else {
			Sell4.setSelected(true);
			bs4Panel.setBackground(Color.green.darker());
		}
		fieldStock4.getTextEditor().setText(om.getSecid().toUpperCase());
		fieldPrice4.setValue(om.getPrice());
		fieldLot4.setValue(om.getLot());
		fieldAmount4.setValue(om.getAmount());
		comboClient4.getTextEditor().setText(om.getClient());
		comboName4.setSelectedIndex(clientfocusLost(om.getClient()));
		omid4 = om.getOrdermatrixid().toString();
	}
	
	private void setField5(OrderMatrix om) {
		if (om.getBuyorsell().equals("1")) {
			Buy5.setSelected(true);
			bs5Panel.setBackground(Color.red.darker());
		} else {
			Sell5.setSelected(true);
			bs5Panel.setBackground(Color.green.darker());
		}
		fieldStock5.getTextEditor().setText(om.getSecid().toUpperCase());
		fieldPrice5.setValue(om.getPrice());
		fieldLot5.setValue(om.getLot());
		fieldAmount5.setValue(om.getAmount());
		comboClient5.getTextEditor().setText(om.getClient());
		comboName5.setSelectedIndex(clientfocusLost(om.getClient()));
		omid5 = om.getOrdermatrixid().toString();
	}

	boolean _send = false;
	
	private boolean send(boolean closed, Vector vField) {
		boolean send = false;
		if (sendAction == null || !sendAction.isEnabled()) {
			Utils.showMessage("cannot send order, please reconnect..", form);
			send = false;
		} else if (isValidEntry(vField)) {
			String entrytype;
			if ((Boolean) vField.get(4)) {
				entrytype = "BUY";
			} else {
				entrytype = "SELL";
			}
			int nconfirm = Utils.C_YES_DLG;
			
			nconfirm = Utils.showConfirmDialog(form,"Entry Order","are you sure want to send this order?\n" + genConfirm(entrytype, (String) vField.get(2),(String) vField.get(5),(Double) vField.get(6),(Double) vField.get(7)), Utils.C_YES_DLG);
			if (nconfirm == Utils.C_YES_DLG) {
				log.info("ordermatrix|user|"+((IEQTradeApp) apps).getFeedEngine().getUserId()+"|clicked yes on comfirm order at|"+logs());
				sendOrder(closed, vField);
			}
		} else {
			send = true;
		}
		return send;
	}

	private void sendOrder(final boolean closed, final Vector vField) {
		try {
			Vector Vdelete = new Vector();		

			if (isClick1 != 0) {
				Vdelete.add(omid1);
				reset1();
			} else if (isClick2 != 0) {
				Vdelete.add(omid2);
				reset2();
			} else if (isClick3 != 0) {
				Vdelete.add(omid3);
				reset3();
			} else if (isClick4 != 0) {
				Vdelete.add(omid4);
				reset4();
			} else if (isClick5 != 0) {
				Vdelete.add(omid5);
				reset5();
			}

			final double lot = (Double) vField.get(6);
			String client = (String) vField.get(2);
			String stock = (String) vField.get(5);
			Boolean bos = (Boolean) vField.get(4);
			Double price = (Double) vField.get(7);

			log.info("ordermatrix|user|"+((IEQTradeApp) apps).getFeedEngine().getUserId()+ "|trying send single order|client : " + client + "|stock : " +stock + "|lot :  " +lot + "|price :  " + price + "|");
			
			int sendMethod = checkBasket.isSelected() ? 1 : 0;
			
			Order order = genOrder(lot, 1, 1, client, stock, "Day", sendMethod, bos, price);
			
			((IEQTradeApp) apps).getTradingEngine().createOrder(order);
			((IEQTradeApp) apps).getTradingEngine().deleteOrderMatrix(Vdelete);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Thread threadTest;
	
	private void sendOrderAll(final boolean closed, final Vector vFieldAll) {
		if (threadTest != null) {
			threadTest.stop();
			threadTest = null;
		}
		threadTest = new Thread() {
			public void run() {
				try {
					Vector Vdelete = new Vector(vFieldAll.size());
					int sendMethod = checkBasket.isSelected() ? 1 : 0;
					if (omid1 != "" || omid2 != "" || omid3 != "" || omid4 != "" || omid5 != "") {
						Vdelete.add(omid1);
						Vdelete.add(omid2);
						Vdelete.add(omid3);
						Vdelete.add(omid4);
						Vdelete.add(omid5);
					}
					
					final Vector vrow = new Vector(vFieldAll.size());
					final Vector vorder = new Vector(vFieldAll.size());
					Vector vField;

					for (int i = 0; i < vFieldAll.size(); i++) {
						vField = (Vector) vFieldAll.get(i);

						if (!vField.get(5).toString().isEmpty()) {
							DisableAll();
							final double lot = (Double) vField.get(6);
							String client = (String) vField.get(2);
							String stock = (String) vField.get(5);
							Boolean bos = (Boolean) vField.get(4);
							Double price = (Double) vField.get(7);
							
							Order order = genOrder(lot, 1, 1, client, stock, "Day",sendMethod, bos, price);
							log.info("ordermatrix|user|"+((IEQTradeApp) apps).getFeedEngine().getUserId()+ "|trying send all order|client : " + client + "|stock : " +stock + "|lot :  " +lot + "|price :  " + price + "|");
							vrow.addElement(OrderDef.createTableRow(order));
							vorder.addElement(order);							
							
							((IEQTradeApp) apps).getTradingEngine().createOrder(order);
							((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER).refresh();													
							
							try {
								SwingUtilities.invokeAndWait(new Runnable() {
									public void run() {
										((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER).addRow(vrow, false, false);
									}
								});
							} catch (Exception ex) {
								ex.printStackTrace();
							}
							if(vField.size()-i != 1) {
								Thread.sleep(2000);
							}											
						}					
					}					
					Utils.showMessage("All order has been Sent!", form);
					EnableAll();
					reset();
					isupdate = 0;
					fieldLabel.setEnabled(true);
					((IEQTradeApp) apps).getTradingEngine().deleteOrderMatrix(Vdelete);	
				} catch (Exception e) {
					e.printStackTrace();
				}				
			}
		};
		threadTest.start();
	}

	private Order genOrder(double lot, double counter, double totalsplit, String client, String stock, String type, int sendMethod, boolean bos, Double price) {
		String timer = fieldTimer.getText();
		Account acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByKey(new String[] { getAccountId(client) });
		//System.err.println("CLIENT : "+client);
		
		//System.err.println("ACC : "+acc.getAccType());
		Stock sec = (Stock) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getDataByKey(new String[] { stock });
		Order order = new Order();
		order.setOrderIDContra("");
		order.setId(OrderIDGenerator.gen(acc.getTradingId()));
		order.setOrderDate(OrderIDGenerator.getTime());
		order.setExchange("IDX");
		order.setBoard("RG");
		order.setBroker(OrderIDGenerator.getBrokerCode());
		order.setBOS(bos ? "1" : "2");
		order.setOrdType(OrderIDGenerator.getLimitOrder());
		Mapping m = (Mapping) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERTYPE).getDataByField(new Object[] { type },new int[] { Mapping.CIDX_CODE });
		order.setOrderType(m.getDesc());
		order.setStock(stock);
		order.setIsAdvertising("0");
		order.setTradingId(client);
		Mapping ia = (Mapping) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_TRADINGACC).getDataByField( new Object[] { 
					acc.getInvType().equalsIgnoreCase("I") ? "Indonesia" : "Asing" }, new int[] { Mapping.CIDX_CODE });
		order.setInvType(ia.getDesc());
		order.setCurrency("IDR");
		order.setContraBroker("");
		order.setContraUser("");
		order.setPrice(price);
		order.setLot(new Double(lot));
		order.setVolume(new Double(sec.getLotSize().doubleValue() * lot));
		//System.err.println(""+ia.getName());
		//System.err.println("IA :  "+ia.getDesc());
		order.setIsBasketOrder(sendMethod == 0 ? "0" : "1");
		try {
			String dt = dateFormat.format(OrderIDGenerator.getTime());
			dt = dt + "-" + timer;
			order.setBasketTime(sendMethod == 0 ? null : dateFormatAll.parse(dt));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		order.setStatus(Order.C_ENTRY);
		order.setMarketOrderId("");
		order.setDoneVol(new Double(0));
		order.setDoneLot(new Double(0));
		order.setCounter(new Double(counter));
		order.setSplitTo(new Double(totalsplit));
		order.setComplianceId(acc.getComlianceId());
		order.setAccId(acc.getAccId());
		order.setBalVol(new Double(order.getVolume().doubleValue()));
		order.setBalLot(new Double(order.getLot().doubleValue()));
		order.setTradeNo("0");
		order.setUser(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase());
		order.setIsFloor("0");
		order.setMarketRef(order.getId());
		order.buildAmount();

		return order;
	}

	void calculate(JSpinner stocks, Double lot, Double price, String st) {
		double lotSize = -1, volume = 0, value = 0;
		try {
			fieldLot1.commitEdit();
			fieldPrice1.commitEdit();
			int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getIndexByKey(new String[] { st });
			if (i >= 0) {
				Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getDataByIndex(i);
				if (stock != null) {
					lotSize = (stock.getLotSize().doubleValue());
				}
			}
			if (lotSize != -1) {
				volume = lotSize * lot;
				value = volume * price;
			}
			if (stocks == fieldPrice1) {
				fieldAmount1.setValue(value);
			} else if (stocks == fieldPrice2) {
				fieldAmount2.setValue(value);
			} else if (stocks == fieldPrice3) {
				fieldAmount3.setValue(value);
			} else if (stocks == fieldPrice4) {
				fieldAmount4.setValue(value);
			} else if (stocks == fieldPrice5) {
				fieldAmount5.setValue(value);
			}
		} catch (Exception ex) {
			fieldAmount1.setValue(null);
			ex.printStackTrace();
		}
	}

	String getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByField(new String[] { alias },new int[] { Account.C_TRADINGID });
		return (acc != null ? acc.getAccId() : "");
	}

	private void registerEventCombo(JDropDown comp) {
		comp.getEditor().getEditorComponent().setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),"enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap().put("tabAction", new AbstractAction("tabAction") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(comboClient1.getTextEditor())) {
					comboClient1.setSelectedIndex(comboClient1.getSelectedIndex());
					comboName1.setSelectedIndex(comboClient1.getSelectedIndex());
					comboName1.requestFocus();
				} else if (e.getSource().equals(comboName1.getTextEditor())) {
					comboClient1.setSelectedIndex(comboName1.getSelectedIndex());
					comboName1.setSelectedIndex(comboName1.getSelectedIndex());
				}
				if (e.getSource().equals(comboClient2.getTextEditor())) {
					comboClient2.setSelectedIndex(comboClient2.getSelectedIndex());
					comboName2.setSelectedIndex(comboClient2.getSelectedIndex());
					comboName2.requestFocus();
				} else if (e.getSource().equals(comboName2.getTextEditor())) {
					comboClient2.setSelectedIndex(comboName2.getSelectedIndex());
					comboName2.setSelectedIndex(comboName2.getSelectedIndex());
					comboName2.requestFocus();
				}
				if (e.getSource().equals(comboClient3.getTextEditor())) {
					comboClient3.setSelectedIndex(comboClient3.getSelectedIndex());
					comboName3.setSelectedIndex(comboClient3.getSelectedIndex());
					comboName3.requestFocus();
				} else if (e.getSource().equals(comboName3.getTextEditor())) {
					comboClient3.setSelectedIndex(comboName3.getSelectedIndex());
					comboName3.setSelectedIndex(comboName3.getSelectedIndex());
					comboName3.requestFocus();
				}
				if (e.getSource().equals(comboClient4.getTextEditor())) {
					comboClient4.setSelectedIndex(comboClient4.getSelectedIndex());
					comboName4.setSelectedIndex(comboClient4.getSelectedIndex());
					comboName4.requestFocus();
				} else if (e.getSource().equals(comboName4.getTextEditor())) {
					comboClient4.setSelectedIndex(comboName4.getSelectedIndex());
					comboName4.setSelectedIndex(comboName4.getSelectedIndex());
					comboName4.requestFocus();
				}
				if (e.getSource().equals(comboClient5.getTextEditor())) {
					comboClient5.setSelectedIndex(comboClient5.getSelectedIndex());
					comboName5.setSelectedIndex(comboClient5.getSelectedIndex());
					comboName5.requestFocus();
				} else if (e.getSource().equals(comboName5.getTextEditor())) {
					comboClient5.setSelectedIndex(comboName5.getSelectedIndex());
					comboName5.setSelectedIndex(comboName5.getSelectedIndex());
					comboName5.requestFocus();
				}
			}
		});
	}

	private void changePrice(JDropDown stock) {
		StockSummary summ = (StockSummary) ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new Object[] { stock.getText(), "RG" });
		if (stock == fieldStock1) {
			if (summ != null) {
				Double harga = Sell1.isSelected() ? summ.getBestBid() : summ.getBestOffer();
				if (harga < 1)
					harga = summ.getPrevious();
				fieldPrice1.setValue(harga);
			} else {
				fieldPrice1.setValue(new Double(0));
			}
		} else if (stock == fieldStock2) {
			if (summ != null) {
				Double harga = Sell2.isSelected() ? summ.getBestBid() : summ
						.getBestOffer();
				if (harga < 1)
					harga = summ.getPrevious();
				fieldPrice2.setValue(harga);
			} else {
				fieldPrice2.setValue(new Double(0));
			}
		} else if (stock == fieldStock3) {
			if (summ != null) {
				Double harga = Sell3.isSelected() ? summ.getBestBid() : summ
						.getBestOffer();
				if (harga < 1)
					harga = summ.getPrevious();
				fieldPrice3.setValue(harga);
			} else {
				fieldPrice3.setValue(new Double(0));
			}
		} else if (stock == fieldStock4) {
			if (summ != null) {
				Double harga = Sell4.isSelected() ? summ.getBestBid() : summ
						.getBestOffer();
				if (harga < 1)
					harga = summ.getPrevious();
				fieldPrice4.setValue(harga);
			} else {
				fieldPrice4.setValue(new Double(0));
			}
		} else if (stock == fieldStock5) {
			if (summ != null) {
				Double harga = Sell5.isSelected() ? summ.getBestBid() : summ
						.getBestOffer();
				if (harga < 1)
					harga = summ.getPrevious();
				fieldPrice5.setValue(harga);
			} else {
				fieldPrice5.setValue(new Double(0));
			}
		}		
	}

	private String genConfirm(String entryType, String client, String stock, Double lot, Double price) {
		String result = "";
		result = result.concat(entryType + " Order\n");
		result = result.concat("Client        :  " + client.concat("\n"));
		result = result.concat("Stock        :  " + stock.concat("\n"));
		result = result.concat("Price         :  " + formatter.format(price)).concat("\n");
		result = result.concat("Lot            :  " + formatter.format(lot)).concat("lot\n");
		return result;
	}
		
	private boolean isValidEntry(Vector vField) {		
		String stock = (String) vField.get(5);
		Double price = (Double) vField.get(7);
		Double lot = (Double) vField.get(6);
		String client = (String) vField.get(2);
		Boolean buy = (Boolean) vField.get(4);

		int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getIndexByKey(new String[] { stock });
		if (i < 0) {
			Utils.showMessage("please enter valid stock", form);
			return false;
		}
		i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getIndexByKey(new String[] { getAccountId(client) });
		if (i < 0) {
			Utils.showMessage("please enter valid Client", form);
			return false;
		}
		if (buy) {
			Account acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByKey(new String[] { getAccountId(client) });
			i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCSTOCK).getIndexByKey(new String[] { acc.getAccType(), stock });
			if (i < 0) {
				Utils.showMessage("invalid stock for this account type", form);
				return false;
			}
		}
		if (lot != null) {
			if (lot <= 0) {
				Utils.showMessage("lot must be greater than zero", form);
				return false;
			}
		} else {
			Utils.showMessage("please enter valid lot", form);
			return false;
		}
		if (price != null) {
			if (price.doubleValue() <= 0) {
				Utils.showMessage("price must be greater than zero", form);
				return false;
			}
		} else {
			Utils.showMessage("please enter valid price", form);
			return false;
		}
		if(!fieldTimer.getText().equals("")) {
			try {
				Date test = formatdate.parse(fieldTimer.getText());
				String tests = formatdate.format(test);
				if (!tests.equals(fieldTimer.getText()))
					throw new Exception("invalid format");
			} catch (Exception ex) {
				Utils.showMessage("invalid timer format, please use pattern HH:mm:ss\nfor example : 14:25:00",form);
				return false;
			}
		}
		return true;
	}
	
	private boolean isValidEntry2(Vector vField) {		
		/* Sebelum di add ke vFieldAll cek kesni dlu */
		String stock = (String) vField.get(5);
		Double price = (Double) vField.get(7);
		Double lot = (Double) vField.get(6);
		String client = (String) vField.get(2);
		Boolean buy = (Boolean) vField.get(4);

		int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getIndexByKey(new String[] { stock });
		if (i < 0) {
			return false;
		}
		i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getIndexByKey(new String[] { getAccountId(client) });
		if (i < 0) {
			return false;
		}
		if (buy) {
			Account acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByKey(new String[] { getAccountId(client) });
			i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCSTOCK).getIndexByKey(new String[] { acc.getAccType(), stock });
			if (i < 0) {
				return false;
			}
		}
		if (lot != null) {
			if (lot <= 0) {
				return false;
			}
		} else {
			return false;
		}
		if (price != null) {
			if (price.doubleValue() <= 0) {
				return false;
			}
		} else {
			return false;
		}
		if(!fieldTimer.getText().equals("")) {
			try {
				Date test = formatdate.parse(fieldTimer.getText());
				String tests = formatdate.format(test);
				if (!tests.equals(fieldTimer.getText()))
					throw new Exception("invalid format");
			} catch (Exception ex) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isValidSave(Vector vFieldAll) {	
		boolean aksi = false;
		int j = 0;
		int k = 0;
		Vector vField;

		for (int i = 0; i < vFieldAll.size(); i++) {
			
			vField = (Vector) vFieldAll.get(i);
			
			if( vField.get(2).toString().trim().isEmpty() && 
				vField.get(5).toString().trim().isEmpty() && 
				((Double) vField.get(6)).equals(new Double(0)) && 
				((Double) vField.get(7)).equals(new Double(0))) {
				k++;
			} else if (vField.get(2).toString().trim().isEmpty() ||
						vField.get(5).toString().trim().isEmpty() ||
						((Double) vField.get(6)).equals(new Double(0)) || 
						((Double) vField.get(7)).equals(new Double(0))) {
				j++;
			} 
		}
		return j > 0 || k == vFieldAll.size() ? false : true;
	}

	public int clientfocusLost(String client) {
		try {
			Account acc = null;
			int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getIndexByKey(new String[] { getAccountId(client) });
			return i;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return 0;
	}
	
	public class MyAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Vector<Vector<Object>> vFieldAll;
			if (e.getSource() == Buy1) {
				bS1Panel.setBackground(Color.red.darker());
			} else if (e.getSource() == Sell1) {
				bS1Panel.setBackground(Color.green.darker());
			} else if (e.getSource() == Buy2) {
				bs2Panel.setBackground(Color.red.darker());
			} else if (e.getSource() == Sell2) {
				bs2Panel.setBackground(Color.green.darker());
			} else if (e.getSource() == Buy3) {
				bs3Panel.setBackground(Color.red.darker());
			} else if (e.getSource() == Sell3) {
				bs3Panel.setBackground(Color.green.darker());
			} else if (e.getSource() == Buy4) {
				bs4Panel.setBackground(Color.red.darker());
			} else if (e.getSource() == Sell4) {
				bs4Panel.setBackground(Color.green.darker());
			} else if (e.getSource() == Sell5) {
				bs5Panel.setBackground(Color.green.darker());
			} else if (e.getSource() == Buy5) {
				bs5Panel.setBackground(Color.red.darker());
			} else if (e.getSource() == btnSend1) {
				log.info("ordermatrix|user|"+((IEQTradeApp) apps).getFeedEngine().getUserId()+"|Clicked btnSend1 at|"+logs());
				calculate(fieldPrice1, (Double) fieldLot1.getValue(),(Double) fieldPrice1.getValue(), fieldStock1.getTextEditor().getText());
				isClick1 = 1;				
				send(true, vField1());
			} else if (e.getSource() == btnSend2) {
				log.info("ordermatrix|user|"+((IEQTradeApp) apps).getFeedEngine().getUserId()+"|Clicked btnSend2 at|"+logs());
				calculate(fieldPrice2, (Double) fieldLot2.getValue(),(Double) fieldPrice2.getValue(), fieldStock2.getTextEditor().getText());
				isClick2 = 1;
				send(true, vField2());
			} else if (e.getSource() == btnSend3) {
				log.info("ordermatrix|user|"+((IEQTradeApp) apps).getFeedEngine().getUserId()+"|Clicked btnSend3 at|"+logs());
				calculate(fieldPrice3, (Double) fieldLot3.getValue(),(Double) fieldPrice3.getValue(), fieldStock3.getTextEditor().getText());
				isClick3 = 1;
				send(true, vField3());
			} else if (e.getSource() == btnSend4) {
				log.info("ordermatrix|user|"+((IEQTradeApp) apps).getFeedEngine().getUserId()+"|Clicked btnSend4 at|"+logs());
				calculate(fieldPrice4, (Double) fieldLot4.getValue(),(Double) fieldPrice4.getValue(), fieldStock4.getTextEditor().getText());
				isClick4 = 1;
				send(true, vField4());
			} else if (e.getSource() == btnSend5) {
				log.info("ordermatrix|user|"+((IEQTradeApp) apps).getFeedEngine().getUserId()+"|Clicked btnSend5 at|"+logs());
				calculate(fieldPrice5, (Double) fieldLot5.getValue(),(Double) fieldPrice5.getValue(), fieldStock5.getTextEditor().getText());
				isClick5 = 1;
				send(true, vField5());
			} else if (e.getSource() == btnReset1) {
				reset1(); isDelete1++;
			} else if (e.getSource() == btnReset2) {
				reset2(); isDelete2++;
			} else if (e.getSource() == btnReset3) {
				reset3(); isDelete3++;
			} else if (e.getSource() == btnReset4) {
				reset4(); isDelete4++;
			} else if (e.getSource() == btnReset5) {
				reset5(); isDelete5++;
			} else if (e.getSource() == btnSendAll) {
				log.info("ordermatrix|user|"+((IEQTradeApp) apps).getFeedEngine().getUserId()+"|Clicked btnSendAll at|"+logs());
				vFieldAll = new Vector();			
				
				vFieldAll.add(vField1());				
				vFieldAll.add(vField2());
				vFieldAll.add(vField3());				
				vFieldAll.add(vField4());
				vFieldAll.add(vField5());

				if(isValidSave(vFieldAll)) {
					int nconfirm = Utils.C_YES_DLG;
					nconfirm = Utils.showConfirmDialog(form, "Entry Order Matrix","are you sure want to send All order?\n", Utils.C_YES_DLG);
					if (nconfirm == Utils.C_YES_DLG) {						
						sendOrderAll(true, vFieldAll);						
					}	
				} else {
					Utils.showMessage("Please Entry Valid Order", form);
				}
			} else if (e.getSource() == btnResetAll) {
				fieldLabel.setEnabled(true);
				reset();
			} else if (e.getSource() == btnSave) {					
				saveordermatrix();				
			} else if (btnLoad == e.getSource()) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						HashMap param = new HashMap();
						param.put("CLIENT", ((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase());
						apps.getUI().showUI(TradingUI.UI_LOADORDERMATRIX, param);
					}
				});
			}
		}
	}

	private Thread threadSave;
	
	private void saveordermatrix() {		
		if (threadSave != null) {
			threadSave.stop();
			threadSave = null;
		}
		
		threadSave = new Thread(new Runnable() {			
			@Override
			public void run() {
				try {			
					boolean isBasket = checkBasket.isSelected() ? true : false;		
					
					((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).getDataVector().clear();
					((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).refresh();
					((IEQTradeApp) apps).getTradingEngine().refreshOrderMatrix(((IEQTradeApp) apps).getFeedEngine().getUserId());
					
					threadSave.sleep(1000);
					
					if(isBasket && BST().isEmpty()) {
						return;
					} else {
						setOMdata();
						if(isValidOMdata()) {
							if(!fieldLabel.getText().trim().isEmpty()) {
								String Label = fieldLabel.getText().trim();
								OrderMatrix om = (OrderMatrix) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).getDataByKey(new String[] {Label});
								
								if(om == null) {
									// Save Data baru
									((IEQTradeApp) apps).getTradingEngine().createOrderMatrix(Label, arrayomdata, isBasket, BST());
									Utils.showMessage("Save Data Successfully", form);
									fieldLabel.setEnabled(true);
									reset();
								} else {
									if(isupdate > 0) {	
										// Kalo direset dan disave dengan label yang sama
										
										if(isDelete1 != 0) {
											Vector v1 = new Vector();	
											v1.add(omid1);
											isDelete1 = 0;
											((IEQTradeApp) apps).getTradingEngine().deleteOrderMatrix(v1);
										}
										if(isDelete2 != 0) {
											Vector v2 = new Vector();	
											v2.add(omid2);
											isDelete2 = 0;
											((IEQTradeApp) apps).getTradingEngine().deleteOrderMatrix(v2);
										}
										if(isDelete3 != 0) {
											Vector v3 = new Vector();	
											v3.add(omid3);
											isDelete3 = 0;
											((IEQTradeApp) apps).getTradingEngine().deleteOrderMatrix(v3);
										}
										if(isDelete4 != 0) {
											Vector v4 = new Vector();	
											v4.add(omid4);
											isDelete4 = 0;
											((IEQTradeApp) apps).getTradingEngine().deleteOrderMatrix(v4);
										}
										if(isDelete5 != 0) {
											Vector v5 = new Vector();	
											v5.add(omid5);
											isDelete5 = 0;
											((IEQTradeApp) apps).getTradingEngine().deleteOrderMatrix(v5);
										}
									
										// Update data
										((IEQTradeApp) apps).getTradingEngine().createOrderMatrix(Label, arrayomdata, isBasket, BST());
										Utils.showMessage("Update Data Successfully", form);	
										fieldLabel.setEnabled(true);
										reset();
									} else {
										Utils.showMessage("Label already use", form);
									}
								}
							} else {
								Utils.showMessage("Please Fill Label", form);
							}
						} else {
							Utils.showMessage("Please Fill Field", form);
						}				
					}			
				} catch (Exception ex) {
					ex.printStackTrace();
				}				
			}
		});
		threadSave.start();		
	}
		
	private void reset() {
		isupdate = 0;
		omid1 = "0";
		omid2 = "0";
		omid3 = "0";
		omid4 = "0";	
		omid5 = "0";
		
		fieldLabel.setText("");
		
		Buy1.setSelected(true);
		bS1Panel.setBackground(Color.red.darker());
		
		Buy2.setSelected(true);
		bs2Panel.setBackground(Color.red.darker());
		
		Buy3.setSelected(true);
		bs3Panel.setBackground(Color.red.darker());
		
		Buy4.setSelected(true);
		bs4Panel.setBackground(Color.red.darker());
		
		Buy5.setSelected(true);
		bs5Panel.setBackground(Color.red.darker());
		
		checkBasket.setSelected(false);
		fieldTimer.setText("");
		
		reset1();
		reset2();
		reset3();
		reset4();
		reset5();
	}
	
	private void reset1() {
		if(isupdate > 0) fieldLabel.setEnabled(false);
		Buy1.setSelected(true);
		bS1Panel.setBackground(Color.red.darker());
		fieldStock1.getTextEditor().setText("");
		fieldPrice1.setValue(new Double(0));
		fieldLot1.setValue(new Double(0));
		fieldAmount1.setValue(new Double(0));
		comboClient1.setSelectedIndex(-1);
		comboName1.setSelectedIndex(-1);
		isClick1 = 0;
	}

	private void reset2() {
		if(isupdate > 0) fieldLabel.setEnabled(false);
		Buy2.setSelected(true);
		bs2Panel.setBackground(Color.red.darker());
		fieldStock2.getTextEditor().setText("");
		fieldPrice2.setValue(new Double(0));
		fieldLot2.setValue(new Double(0));
		fieldAmount2.setValue(new Double(0));
		comboClient2.getTextEditor().setText("");
		comboName2.getTextEditor().setText("");
		isClick2 = 0;
	}
	
	private void reset3() {
		if(isupdate > 0) fieldLabel.setEnabled(false);
		Buy3.setSelected(true);
		bs3Panel.setBackground(Color.red.darker());
		fieldStock3.getTextEditor().setText("");
		fieldPrice3.setValue(new Double(0));
		fieldLot3.setValue(new Double(0));
		fieldAmount3.setValue(new Double(0));
		comboClient3.getTextEditor().setText("");
		comboName3.getTextEditor().setText("");
		isClick3 = 0;
	}
	
	private void reset4() {
		if(isupdate > 0) fieldLabel.setEnabled(false);
		Buy4.setSelected(true);
		bs4Panel.setBackground(Color.red.darker());
		fieldStock4.getTextEditor().setText("");
		fieldPrice4.setValue(new Double(0));
		fieldLot4.setValue(new Double(0));
		fieldAmount4.setValue(new Double(0));
		comboClient4.getTextEditor().setText("");
		comboName4.getTextEditor().setText("");
		isClick4 = 0;
	}
	
	private void reset5() {
		if(isupdate > 0) fieldLabel.setEnabled(false);
		Buy5.setSelected(true);
		bs5Panel.setBackground(Color.red.darker());
		fieldStock5.getTextEditor().setText("");
		fieldPrice5.setValue(new Double(0));
		fieldLot5.setValue(new Double(0));
		fieldAmount5.setValue(new Double(0));
		comboClient5.getTextEditor().setText("");
		comboName5.getTextEditor().setText("");
		isClick5 = 0;
	}
	
	private Vector vField1() {
		double tempLotDobule = (Double) fieldLot1.getValue();
		int tempLot = (int) tempLotDobule;
		Double volume = tempLot * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));
		Double status = (double) 1;
		String ordermatrixid = "";
		String[] parts = null;
		
		if (!omid1.trim().isEmpty()) {
			parts = omid1.split(",");
			ordermatrixid = parts[0];
		}
				
		Vector vField = new Vector();

		vField.add(ordermatrixid.trim()); // 0
		vField.add(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase().trim()); // 1
		vField.add(comboClient1.getTextEditor().getText().trim()); // 2
		vField.add(fieldLabel.getText().trim()); // 3
		vField.add(Buy1.isSelected() ? true : false); // 4
		vField.add(fieldStock1.getTextEditor().getText().trim()); // 5
		vField.add(fieldLot1.getValue()); // 6
		vField.add(fieldPrice1.getValue()); // 7
		vField.add(fieldAmount1.getValue()); // 8
		vField.add(volume); // 9
		vField.add(comboName1.getTextEditor().getText().trim()); // 10
		
		return vField;
	}	
	
	private Vector vField2() {
		double tempLotDobule = (Double) fieldLot2.getValue();
		int tempLot = (int) tempLotDobule;
		Double volume = tempLot * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));
		Double status = (double) 1;

		String ordermatrixid = "";
		String[] parts = null;
		
		if (omid2 != null) {
			parts = omid2.split(",");
			ordermatrixid = parts[0];
		}
		
		Vector vField2 = new Vector();
		vField2.add(ordermatrixid.trim()); // 0
		vField2.add(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase().trim()); // 1
		vField2.add(comboClient2.getTextEditor().getText().trim()); // 2
		vField2.add(fieldLabel.getText().trim()); // 3
		vField2.add(Buy2.isSelected() ? true : false); // 4
		vField2.add(fieldStock2.getTextEditor().getText().trim()); // 5
		vField2.add(fieldLot2.getValue()); // 6
		vField2.add(fieldPrice2.getValue()); // 7
		vField2.add(fieldAmount2.getValue()); // 8
		vField2.add(volume); // 9
		vField2.add(comboName2.getTextEditor().getText().trim());		
		return vField2;
	}
	
	private Vector vField3() {
//		double tempLotDobule = (Double) fieldLot3.getValue();
//		int tempLot = (int) tempLotDobule;
//		Double volume = tempLot * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));
//		Double status = (double) 1;

		Double a = ((Double) fieldLot3.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)));
		
		String ordermatrixid = "";
		String[] parts = null;
		
		if (omid3 != null) {
			parts = omid3.split(",");
			ordermatrixid = parts[0];
		}
		
		Vector vField3 = new Vector();

		vField3.add(ordermatrixid.trim()); // 0
		vField3.add(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase().trim()); // 1
		vField3.add(comboClient3.getTextEditor().getText().trim()); // 2
		vField3.add(fieldLabel.getText().trim()); // 3
		vField3.add(Buy3.isSelected() ? true : false); // 4
		vField3.add(fieldStock3.getTextEditor().getText().trim()); // 5
		vField3.add(fieldLot3.getValue()); // 6
		vField3.add(fieldPrice3.getValue()); // 7
		vField3.add(fieldAmount3.getValue()); // 8
		vField3.add(((Double) fieldLot3.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)))); // 9
		vField3.add(comboName3.getTextEditor().getText());
		
		return vField3;
	}
	
	private Vector vField4() {
		double tempLotDobule = (Double) fieldLot4.getValue();
		int tempLot = (int) tempLotDobule;
		Double volume = tempLot* Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));
		Double status = (double) 1;

		String ordermatrixid = "";
		String[] parts = null;
		
		if (omid4 != null) {
			parts = omid4.split(",");
			ordermatrixid = parts[0];
		}
		
		Vector vField4 = new Vector();

		vField4.add(ordermatrixid.trim()); // 0
		vField4.add(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase().trim()); // 1
		vField4.add(comboClient4.getTextEditor().getText().trim()); // 2
		vField4.add(fieldLabel.getText().trim()); // 3
		vField4.add(Buy4.isSelected() ? true : false); // 4
		vField4.add(fieldStock4.getTextEditor().getText().trim()); // 5
		vField4.add(fieldLot4.getValue()); // 6
		vField4.add(fieldPrice4.getValue()); // 7
		vField4.add(fieldAmount4.getValue()); // 8
		vField4.add(volume); // 9
		vField4.add(comboName4.getTextEditor().getText()); // 10
		
		return vField4;
	}	

	private Vector vField5() {
		double tempLotDobule = (Double) fieldLot5.getValue();
		int tempLot = (int) tempLotDobule;
		Double volume = tempLot* Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));
		Double status = (double) 1;

		String ordermatrixid = "";
		String[] parts = null;
		
		if (omid5 != null) {
			parts = omid5.split(",");
			ordermatrixid = parts[0];
		}
		
		Vector vField5 = new Vector();

		vField5.add(ordermatrixid.trim()); // 0
		vField5.add(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase().trim()); // 1
		vField5.add(comboClient5.getTextEditor().getText().trim()); // 2
		vField5.add(fieldLabel.getText().trim()); // 3
		vField5.add(Buy5.isSelected() ? true : false); // 5
		vField5.add(fieldStock5.getTextEditor().getText().trim()); // 5
		vField5.add(fieldLot5.getValue()); // 6
		vField5.add(fieldPrice5.getValue()); // 7
		vField5.add(fieldAmount5.getValue()); // 8
		vField5.add(volume); // 9
		vField5.add(comboName5.getTextEditor().getText()); // 10
		
		return vField5;
	}	
	
	private void setOMdata() {		
		arrayomdata = new OrderMatrixData[5];
		
		arrayomdata[0] = new OrderMatrixData();
		
		arrayomdata[0].setNoUrut(1);
		arrayomdata[0].setOrderMatrixID(omid1.isEmpty() ? "" : omid1.replace(",", ""));
		arrayomdata[0].setUserID(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase().trim());
		arrayomdata[0].setClientID(comboClient1.getText().toString().trim());
		arrayomdata[0].setLabel(fieldLabel.getText().trim());
		arrayomdata[0].setBos(Buy1.isSelected() ? 1 : 2);
		arrayomdata[0].setStock(fieldStock1.getText().trim());
		arrayomdata[0].setLot((Double) fieldLot1.getValue());
		arrayomdata[0].setPrice((Double) fieldPrice1.getValue());
		arrayomdata[0].setAmount((Double) fieldLot1.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)) * (Double) fieldPrice1.getValue());
		arrayomdata[0].setVolume((Double) fieldLot1.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)));
		arrayomdata[0].setClientName(comboName1.getText().toString().trim());
		
		arrayomdata[1] = new OrderMatrixData();
		
		arrayomdata[1].setNoUrut(2);
		arrayomdata[1].setOrderMatrixID(omid2.isEmpty() ? "" : omid2.replace(",", ""));
		arrayomdata[1].setUserID(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase().trim());
		arrayomdata[1].setClientID(comboClient2.getText().toString().trim());
		arrayomdata[1].setLabel(fieldLabel.getText().trim());
		arrayomdata[1].setBos(Buy2.isSelected() ? 1 : 2);
		arrayomdata[1].setStock(fieldStock2.getText().trim());
		arrayomdata[1].setLot((Double) fieldLot2.getValue());
		arrayomdata[1].setPrice((Double) fieldPrice2.getValue());
		arrayomdata[1].setAmount((Double) fieldLot2.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)) * (Double) fieldPrice2.getValue());
		arrayomdata[1].setVolume((Double) fieldLot2.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)));
		arrayomdata[1].setClientName(comboName2.getText().toString().trim());
		
		arrayomdata[2] = new OrderMatrixData();
		
		arrayomdata[2].setNoUrut(3);
		arrayomdata[2].setOrderMatrixID(omid3.isEmpty() ? "" : omid3.replace(",", ""));
		arrayomdata[2].setUserID(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase().trim());
		arrayomdata[2].setClientID(comboClient3.getText().toString().trim());
		arrayomdata[2].setLabel(fieldLabel.getText().trim());
		arrayomdata[2].setBos(Buy3.isSelected() ? 1 : 2);
		arrayomdata[2].setStock(fieldStock3.getText().trim());
		arrayomdata[2].setLot((Double) fieldLot3.getValue());
		arrayomdata[2].setPrice((Double) fieldPrice3.getValue());
		arrayomdata[2].setAmount((Double) fieldLot3.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)) * (Double) fieldPrice3.getValue());
		arrayomdata[2].setVolume((Double) fieldLot3.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)));
		arrayomdata[2].setClientName(comboName3.getText().toString().trim());
		
		arrayomdata[3] = new OrderMatrixData();
		
		arrayomdata[3].setNoUrut(4);
		arrayomdata[3].setOrderMatrixID(omid4.isEmpty() ? "" : omid4.replace(",", ""));
		arrayomdata[3].setUserID(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase().trim());
		arrayomdata[3].setClientID(comboClient4.getText().toString().trim());
		arrayomdata[3].setLabel(fieldLabel.getText().trim());
		arrayomdata[3].setBos(Buy4.isSelected() ? 1 : 2);
		arrayomdata[3].setStock(fieldStock4.getText().trim());
		arrayomdata[3].setLot((Double) fieldLot4.getValue());
		arrayomdata[3].setPrice((Double) fieldPrice4.getValue());
		arrayomdata[3].setAmount((Double) fieldLot4.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)) * (Double) fieldPrice4.getValue());
		arrayomdata[3].setVolume((Double) fieldLot4.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)));
		arrayomdata[3].setClientName(comboName4.getText().toString().trim());
		
		arrayomdata[4] = new OrderMatrixData();
		
		arrayomdata[4].setNoUrut(5);
		arrayomdata[4].setOrderMatrixID(omid5.isEmpty() ? "" : omid5.replace(",", ""));
		arrayomdata[4].setUserID(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase().trim());
		arrayomdata[4].setClientID(comboClient5.getText().toString().trim());
		arrayomdata[4].setLabel(fieldLabel.getText().trim());
		arrayomdata[4].setBos(Buy5.isSelected() ? 1 : 2);
		arrayomdata[4].setStock(fieldStock5.getText().trim());
		arrayomdata[4].setLot((Double) fieldLot5.getValue());
		arrayomdata[4].setPrice((Double) fieldPrice5.getValue());
		arrayomdata[4].setAmount((Double) fieldLot5.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)) * (Double) fieldPrice5.getValue());
		arrayomdata[4].setVolume((Double) fieldLot5.getValue() * Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)));
		arrayomdata[4].setClientName(comboName5.getText().toString().trim());
	}
	
	private void DisableAll() {
		this.form.setEnabled(false);
//		field
//		btnLoad.setEnabled(false);
//		btnSendAll
//		btnReset1
//		btnReset2
//		btnReset3
//		btnReset4
//		btnSend1
//		btnSend2
//		btnSend3
//		btnSend4
		
	}
	
	private void EnableAll() {
		this.form.setEnabled(true);
	}
	
	private boolean isValidOMdata() {
		int allEmpty = 0;
		int partialEmpty = 0;
		
		for(int i = 0; i < arrayomdata.length; i++) {
			if (arrayomdata[i].getStock().isEmpty() && arrayomdata[i].getPrice().equals(new Double(0)) && arrayomdata[i].getLot().equals(new Double(0)) && arrayomdata[i].getClientID().isEmpty()) {
				allEmpty++;
			} else if(arrayomdata[i].getStock().isEmpty() || arrayomdata[i].getPrice().equals(new Double(0)) || arrayomdata[i].getLot().equals(new Double(0)) || arrayomdata[i].getClientID().isEmpty()) {
				partialEmpty++;
			}
		}		
		return partialEmpty > 0 || allEmpty == arrayomdata.length ? false : true;
	}
	
	private void deleteOrderMatrix(Vector pvdelete) {
		Vector vdelete = new Vector();

		for (int i = 1; i <= pvdelete.size(); i++) {
			vdelete.add(pvdelete.get(i));
		}

		((IEQTradeApp) apps).getTradingEngine().deleteOrderMatrix(vdelete);
	}
		
	private class IDXPriceSpinnerModel extends SpinnerNumberModel {
		private static final long serialVersionUID = 1L;

		public IDXPriceSpinnerModel() {
			super(new Double(0), new Double(0), new Double(100000000),
					new Double(1));
		}

		public Object getNextValue() {
			Double newValue = new Double(50);
			Double oldValue = (Double) this.getValue();
			if (oldValue < 200)
				newValue = oldValue + 1;
			if (oldValue >= 200)
				newValue = oldValue + 5;
			if (oldValue >= 500)
				newValue = oldValue + 10;
			if (oldValue >= 2000)
				newValue = oldValue + 25;
			if (oldValue >= 5000)
				newValue = oldValue + 50;
			if (newValue < 50)
				newValue = new Double(50);
			return new Double(newValue);
		}

		public Object getPreviousValue() {
			Double newValue = new Double(50);
			Double oldValue = (Double) this.getValue();
			if (oldValue < 200)
				newValue = oldValue - 1;
			if (oldValue > 200)
				newValue = oldValue - 5;
			if (oldValue > 500)
				newValue = oldValue - 10;
			if (oldValue > 2000)
				newValue = oldValue - 25;
			if (oldValue > 5000)
				newValue = oldValue - 50;
			if (newValue < 50)
				newValue = new Double(50);
			return new Double(newValue);
		}
	}
		
	private boolean isValidBST() {
//		isValidBST = is valid basket send time
		if(checkBasket.isSelected()) {
			try {
				Date tempbasketsendtime = formatdate.parse(fieldTimer.getText());
				String basketsendtime = formatdate.format(tempbasketsendtime);
				if (!basketsendtime.equals(fieldTimer.getText()))
					throw new Exception("invalid format");
			} catch (Exception ex) {
				Utils.showMessage("invalid timer format, please use pattern HH:mm:ss\nfor example : 14:25:00", form);
				return false;
			}	
			return true;
		}		
		return false;
	}	
	
	private String BST() {
		// BST = basket send time
		return isValidBST() ? fieldTimer.getText().trim() : "";
	}
	
	private String logs() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime()).toString();
	}
}