package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.NotActiveException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.Notification;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;


import sun.net.www.content.image.jpeg;
import sun.security.krb5.Checksum;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.framework.UI.MyCustomMouseAdapter;
import com.vollux.ui.JBtn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.Utils;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.DueDateDef;
import eqtrade.trading.model.Notif;
import eqtrade.trading.model.NotifDef;

public class UINotification extends UI{
	private JPanel pnlActive;
	private JPanel pnlSms;
	private JPanel pnlEmail;
	private JSkinDlg frame;
	
	

	private JTextField txtNohp1;
	private JTextField txtNohp2;
	private JTextField txtNohp3;
	
	

	private JTextField txtEmail1;
	private JTextField txtEmail2;
	private JTextField txtEmail3;
	
	private JButton btnSave;
	private JButton btnCancel;
	
	private JTabbedPane tabSendBy;
	
	
	private JCheckBox checkActive = new JCheckBox("Notification Active");
	private JCheckBox checkOnSell = new JCheckBox("On Sell Done");
	private JCheckBox checkOnBuy = new JCheckBox("On Buy Done");
	private JCheckBox checkAktifSms = new JCheckBox("Aktif SMS");
	private JCheckBox checkAktifEmail = new JCheckBox("Aktif Email");
	String checkBox ="";
	String checkSell="";
	String checkBuy="";
	String checksms="";
	String checkEmail="";
	private JLabel lblNoHp1 = new JLabel();
	private JLabel lblNoHp2 = new JLabel();
	private JLabel lblNoHp3 = new JLabel();
	private JLabel lblEmail1 = new JLabel();
	private JLabel lblEmail2 = new JLabel();
	private JLabel lblEmail3 = new JLabel();
	boolean Active = true;
	String notif="";
	private JGrid table;
	int act =1 ;
	int checkon = 0;
 	
	public UINotification(String app) {
		super("Simas.Net Notification", app);
		
		setProperty(C_PROPERTY_NAME,TradingUI.UI_NOTIFICATION);
		
	}
	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.getInsidePanel().setBorder(new EmptyBorder(0, 1, 1, 1));
		frame.setContent(pnlContent);
		frame.pack();
		 frame.setLocation(((Rectangle) hSetting.get("size")).x, ((Rectangle)
				 hSetting.get("size")).y);
		frame.setSize(((Rectangle) hSetting.get("size")).width,
				((Rectangle) hSetting.get("size")).height);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
	}
	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
			checkActive.isCursorSet();
			}
		});		
	}
	
	@Override
	protected void build() {
		
		
		lblNoHp1.setForeground(Color.red);
		lblNoHp2.setForeground(Color.red);
		lblNoHp3.setForeground(Color.red);
		lblEmail1.setForeground(Color.red);
		lblEmail2.setForeground(Color.red);
		lblEmail3.setForeground(Color.red);
		
		pnlSms = new JPanel();
		pnlEmail = new JPanel();
		pnlActive = new JPanel();
		tabSendBy = new JTabbedPane();

		txtNohp1 = new JTextField();
		txtNohp2 = new JTextField();
		txtNohp3 = new JTextField();
		/*((AbstractDocument) txtNohp1.getDocument()).setDocumentFilter(new NumericAndLengthFilter(15));
		((AbstractDocument) txtNohp2.getDocument()).setDocumentFilter(new NumericAndLengthFilter(15));
		((AbstractDocument) txtNohp3.getDocument()).setDocumentFilter(new NumericAndLengthFilter(15));*/
		txtEmail1 = new JTextField();
		txtEmail2 = new JTextField();
		txtEmail3 = new JTextField();
		btnSave = new JButton("Save");
		btnCancel = new JButton("Cancel");
		
		//txtNohp1.
		/*isUnchecked();*/
		/*table = createTable(
				((IEQTradeApp)apps).getTradingEngine().getStore(
						TradingStore.DATA_NOTIFICATION), null,
				(Hashtable) hSetting.get("table"));
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getTable().addMouseListener(new MyCustomMouseAdapter());*/
		checkActive.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(checkActive.isSelected()){
				
					isCheked();
				}else{
					isUnchecked();
					checkAktifSms.setSelected(false);
					checkAktifEmail.setSelected(false);
					checkOnBuy.setSelected(false);
					checkOnSell.setSelected(false);
				}
				
			}
		});
		checkAktifEmail.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(checkAktifEmail.isSelected()){
					isCheckEmail();
				}else{
					isUnCheckEmail();
				}
				
			}
		});
		checkAktifSms.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(checkAktifSms.isSelected()){
					isCheckSms();
				}else{
					isUnCheckSms();
				}
				
			}
		});
		checkOnSell.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (checkOnSell.isSelected()) {
					checkSell = String.valueOf(1);
				}else{
					checkSell = String.valueOf(0);
				}
				
			}
		});
		btnSave.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(checkOnBuy.isSelected() || checkOnSell.isSelected())
				{
					if(lblEmail1.getText().isEmpty()&&lblEmail3.getText().isEmpty()&&lblEmail2.getText().isEmpty()
							//&&lblNoHp1.getText().isEmpty()&&lblNoHp3.getText().isEmpty()&&lblNoHp2.getText().isEmpty() 
							&&txtNohp1.getText().matches("\\d*")&&txtNohp2.getText().matches("\\d*")&&txtNohp3.getText().matches("\\d*")
					){
					sendNotif(false);	
					JOptionPane.showMessageDialog(null, "Save Data Successfully.",
							"information", JOptionPane.INFORMATION_MESSAGE);
					close();
					}else{
						JOptionPane.showMessageDialog(null, "entry data not valid",
								"information", JOptionPane.INFORMATION_MESSAGE);
					}
				}else if(!checkActive.isSelected())
				{
					sendNotif(false);	
					JOptionPane.showMessageDialog(null, "Save Data Successfully.",
							"information", JOptionPane.INFORMATION_MESSAGE);
					close();
				}
				else{
						JOptionPane.showMessageDialog(null, "check first buy or sell done",
							"information", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				lblNoHp1.setText("");
				lblNoHp2.setText("");
				lblNoHp3.setText("");
				lblEmail1.setText("");
				lblEmail2.setText("");
				lblEmail3.setText("");
				frame.dispose();
				
			}
		});
		txtNohp1.addKeyListener(new KeyListener() {	
			
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {
			String value = txtNohp1.getText();
			int l = value.length();	
			if(/*EVT.getKeyChar()>='0' && EVT.getKeyChar()<='9' || EVT.getKeyCode()==EVT.VK_BACK_SPACE ||
						EVT.getKeyCode()==EVT.VK_ENTER ||EVT.getKeyCode()==EVT.VK_DELETE||EVT.getKeyCode()==EVT.VK_RIGHT || 
						EVT.getKeyCode()==EVT.VK_LEFT*/value.matches("\\d*"))
				{
					txtNohp1.setEditable(true);
					lblNoHp1.setText("");
				}else{
//					txtNohp1.setEditable(false);
					lblNoHp1.setText("X");
					
				}			
			}			
			@Override
			public void keyPressed(KeyEvent EVT) {
			}
		});
		txtNohp2.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent arg0) {
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				String value = txtNohp2.getText();
				int l = value.length();
				if(value.matches("\\d*")){
					txtNohp2.setEditable(true);
					lblNoHp2.setText("");
				}else{
//					txtNohp2.setEditable(false);
					lblNoHp2.setText("X");
				}
			}
			
			@Override
			public void keyPressed(KeyEvent arg) {
				
				
			}
		});
		txtNohp3.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {
				String value = txtNohp3.getText();
				int l = value.length();
				if(value.matches("\\d*")){
					txtNohp3.setEditable(true);
					lblNoHp3.setText("");
				}else{
//					txtNohp3.setEditable(false);
					lblNoHp3.setText("X");
				}
			}
			@Override
			public void keyPressed(KeyEvent e1) {
				
				
			}
		});
		
		txtEmail1.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				String email1= txtEmail1.getText();
				 Pattern pattern = Pattern.compile (
				         "([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]"
				         + "{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z\\-]+\\.)+))"
				         + "([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)",
				         Pattern.MULTILINE);
				 Matcher m1=pattern.matcher(email1);
				 boolean b1=m1.matches();
				 if(txtEmail1.getText().length()>0){
					 if(b1==true){
						 lblEmail1.setText("");
					 }else{
						 lblEmail1.setText("X");
					 }
				 }else{
					 lblEmail1.setText("");
				 }
					 
				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		txtEmail2.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				String email= txtEmail2.getText();
				 Pattern pattern = Pattern.compile (
				         "([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]"
				         + "{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z\\-]+\\.)+))"
				         + "([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)",
				         Pattern.MULTILINE);
				 Matcher m=pattern.matcher(email);
				 boolean b=m.matches();
				 if(txtEmail2.getText().length()>0){
					 if(b==true){
						 lblEmail2.setText("");
					 }else{
						 lblEmail2.setText("X");
					 }
				 }else{
					 lblEmail2.setText("");
				 }
					 
				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});		
		txtEmail3.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				String email= txtEmail3.getText();
				 Pattern pattern = Pattern.compile (
				         "([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]"
				         + "{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z\\-]+\\.)+))"
				         + "([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)",
				         Pattern.MULTILINE);
				 Matcher m=pattern.matcher(email);
				 boolean b=m.matches();
				 if(txtEmail3.getText().length()>0){
					 if(b==true){
						 lblEmail3.setText("");
					 }else{
						 lblEmail3.setText("X");
					 }
				 }else{
					 lblEmail3.setText("");
				 }
					 
				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		checkOnBuy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(!checkOnBuy.isSelected() && !checkOnSell.isSelected()){
					checkAktifEmail.setSelected(false);
					checkAktifSms.setSelected(false);
					isUnCheckEmail();
					isUnCheckSms();
				}
			}
		});
		checkOnSell.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!checkOnBuy.isSelected()&& !checkOnSell.isSelected()){
					checkAktifEmail.setSelected(false);
					checkAktifSms.setSelected(false);
					isUnCheckEmail();
					isUnCheckSms();
				}
			}
		});
		FormLayout layoutActive = new FormLayout("5dlu,30dlu,40dlu,2dlu,40dlu,pref,5dlu,pref,20dlu,pref,75dlu,40dlu",
				"5dlu,pref,10dlu,pref,2dlu,pref,5dlu,pref,5dlu,100dlu,20dlu,pref");
		PanelBuilder builderActive = new PanelBuilder(layoutActive,pnlActive);
		pnlActive.setBorder(new EmptyBorder(0,0,0,0));
		CellConstraints cellActive = new CellConstraints();
		checkActive.setOpaque(false);
		builderActive.add(checkActive, cellActive.xyw(2, 2,5));
		builderActive.addSeparator("Filter Notification By", cellActive.xyw(2, 4,8));
		builderActive.add(checkOnSell, cellActive.xyw(2, 6,3));
		builderActive.add(checkOnBuy, cellActive.xyw(5, 6,6));
		builderActive.addSeparator("Notification By", cellActive.xyw(2,8,8));
		builderActive.add(tabSendBy, cellActive.xyw(2,10,8));
		builderActive.add(btnSave,cellActive.xy(3, 11));
		builderActive.add(btnCancel,cellActive.xy(5, 11));
		
		FormLayout layoutSms = new FormLayout("5dlu,100dlu,2dlu,100dlu,5dlu,5dlu","5dlu,pref,4dlu,pref,5dlu,pref,5dlu,pref,5dlu");
		PanelBuilder builderSms = new PanelBuilder(layoutSms,pnlSms);
		pnlSms.setBorder(new EmptyBorder(1,1,1,1));
		CellConstraints cellSms = new CellConstraints();
		builderSms.add(checkAktifSms, cellSms.xy(2,2));
		builderSms.add(lblNoHp1, cellSms.xy(4,4));
		builderSms.add(txtNohp1, cellSms.xy(2,4));
		builderSms.add(lblNoHp2, cellSms.xy(4,6));
		builderSms.add(txtNohp2, cellSms.xy(2,6));
		builderSms.add(lblNoHp3, cellSms.xy(4,8));
		builderSms.add(txtNohp3, cellSms.xy(2,8));
		//builderSms.add(txtNohp3, cellSms.xy(4,6));
		
		FormLayout layoutEmail = new FormLayout("5dlu,110dlu,2dlu,110dlu,5dlu,5dlu","5dlu,pref,4dlu,pref,5dlu,pref,5dlu,pref,5dlu");
		PanelBuilder builderEmail = new PanelBuilder(layoutEmail,pnlEmail);
		pnlEmail.setBorder(new EmptyBorder(1,1,1,1));
		CellConstraints cellEmail = new CellConstraints();
		builderEmail.add(checkAktifEmail, cellSms.xy(2,2));
		
		builderEmail.add(txtEmail1, cellEmail.xy(2,4));
		builderEmail.add(lblEmail1, cellEmail.xy(4,4));
		builderEmail.add(txtEmail2, cellEmail.xy(2,6));
		builderEmail.add(lblEmail2, cellEmail.xy(4,6));
		builderEmail.add(txtEmail3, cellEmail.xy(2,8));
		builderEmail.add(lblEmail3, cellEmail.xy(4,8));
		//builderEmail.add(txtEmail3, cellEmail.xy(4,6));
		
		tabSendBy.addTab(" SMS ", pnlSms);
		tabSendBy.addTab(" EMAIL ", pnlEmail);
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2,2,2,2));
		pnlContent.add(pnlActive,BorderLayout.CENTER);
		((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_NOTIFICATION)
				.addTableModelListener(new TableModelListener() {
					
					@Override
					public void tableChanged(TableModelEvent e) {
						infoNotif();
						act =0;
						if(checkActive.isSelected()){
							isCheked();
						}else{
							isUnchecked();
						}
						
					}
				});
		if (act == 1) {
			isUnchecked();
			notAktif();
		}
		//refresh();
		//infoNotif();
	}
	void validEmail(){
		String email1= txtEmail1.getText();
		String email2= txtEmail2.getText();
		String email3= txtEmail3.getText();
		
		 Pattern pattern = Pattern.compile (
		         "([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]"
		         + "{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z\\-]+\\.)+))"
		         + "([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)",
		         Pattern.MULTILINE);
		 
		 Matcher m1=pattern.matcher(email1);
		 Matcher m2=pattern.matcher(email2);
		 Matcher m3=pattern.matcher(email3);
		 boolean b1=m1.matches();
		 boolean b2=m2.matches();
		 boolean b3=m3.matches();
		 /*if(txtEmail1.getText().length()>0){
			 if(b1==true){
				 
					 sendNotif(false);
				 				 
			 }else{
				 JOptionPane.showMessageDialog(null, "You must enter a valid email...!!!","Information", JOptionPane.INFORMATION_MESSAGE);
			 }
		 }else if(txtEmail2.getText().length()>0){
		 
					 if(b2==true){
						 sendNotif(false);
					 }
					 else{
						 JOptionPane.showMessageDialog(null, "You must enter a valid email...!!!","Information", JOptionPane.INFORMATION_MESSAGE); 
					 }
				 }else{
			 sendNotif(false);
		 }*/
		 
		 
		  
	}
	

	

	private Thread threadTest;
	void sendNotif(final boolean closed){
				Notif notif = new Notif();
				
				if (checkActive.isSelected()) {
					checkBox = String.valueOf(1);
				}else{
					checkBox = String.valueOf(0);
				}
				if(checkOnSell.isSelected()){
					checkSell = String.valueOf(1);
				}else{
					checkSell = String.valueOf(0);
				}
				if(checkOnBuy.isSelected()){
					checkBuy = String.valueOf(1);
				}else{
					checkBuy = String.valueOf(0);
				}
				if(checkAktifSms.isSelected()){
					checksms = String.valueOf(1);
				}else{
					checksms = String.valueOf(0);
				}
				if(checkAktifEmail.isSelected()){
					checkEmail = String.valueOf(1);
				}else{
					checkEmail = String.valueOf(0);
				}				
				notif.setcUserid("");
				notif.setcNotif(checkBox);
				notif.setcOnselldone(checkSell);
				notif.setcOnbuydone(checkBuy);
				notif.setcSendsms(checksms);
				notif.setcSendmail(checkEmail);
				
					notif.setcEmail1(txtEmail1.getText());
					notif.setcEmail2(txtEmail2.getText());
					notif.setcEmail3(txtEmail3.getText());
					
					notif.setcMobile1(txtNohp1.getText());
					notif.setcMobile2(txtNohp2.getText());
					notif.setcMobile3(txtNohp3.getText());
				((IEQTradeApp)apps).getTradingEngine().createNotif(notif);
				//System.out.println("\nSend-----"+txtEmail1.getText()+"=="+txtEmail2.getText()+"=="+txtEmail3.getText());
			
		
	}

	private void isCheked(){
		
		isCheckSms();
		isCheckEmail();
		
		checkAktifEmail.setEnabled(true);
		checkAktifSms.setEnabled(true);
		checkOnBuy.setEnabled(true);
		checkOnSell.setEnabled(true);
	}
	private void isUnchecked(){
		isUnCheckSms();
		
		isUnCheckEmail();
		
		checkAktifEmail.setEnabled(false);
		checkAktifSms.setEnabled(false);
		checkOnBuy.setEnabled(false);
		checkOnSell.setEnabled(false);
	}
	private void isCheckSms(){
	
		txtNohp1.setEnabled(true);
		txtNohp2.setEnabled(true);
		txtNohp3.setEnabled(true);
		
	}
	private void isUnCheckSms(){
		
		txtNohp1.setEnabled(false);
		txtNohp2.setEnabled(false);
		txtNohp3.setEnabled(false);
//		txtNohp1.setText("");
//		txtNohp2.setText("");
//		txtNohp3.setText("");
	}
	
	private void isCheckEmail(){
		
		txtEmail1.setEnabled(true);
		txtEmail2.setEnabled(true);
		txtEmail3.setEnabled(true);
	}
	private void isUnCheckEmail(){
		
		txtEmail1.setEnabled(false);
		txtEmail2.setEnabled(false);
		txtEmail3.setEnabled(false);
//		txtEmail1.setText("");
//		txtEmail2.setText("");
//		txtEmail3.setText("");
	}
	
	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
		.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			//hSetting.put("table", NotifDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 280, 350));
		
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
		
	}
	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}
	@Override
	public void show() {
		
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}
	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}
	private Thread thread;
	@Override
	public void show(Object param) {
		HashMap p = (HashMap) param;
		query((String) p.get("CLIENT"));
		notif=p.get("CLIENT").toString();
		//System.out.println("--- "+notif);
		show();
	}
	
	private void query(final String param) {
		if (thread != null) {
			thread.stop();
			thread = null;
		}
		thread = new Thread(new Runnable() {
			@Override
			public void run() {
				((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_NOTIFICATION).getDataVector()
						.clear();
				((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_NOTIFICATION).refresh();
				((IEQTradeApp) apps).getTradingEngine().refreshNotif(param);
				//System.out.println("cek notif ----"+param);
			}
		});
		thread.start();
	}
		
	void infoNotif(){
		/*SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				try{*/
		//System.out.println("---2 "+notif);
					Notif nt = (Notif) ((IEQTradeApp)apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_NOTIFICATION)
					.getDataByField(new String[] {notif}, 
							new int[] {Notif.C_USERID});
					
					//System.out.println("notif user id"+nt.getcNotif());
				//	System.out.println("mobile1...."+nt.getcNotif());
					//System.out.println("NoTiF="+notif+"="+Notif.C_USERID);
					if(nt.getcNotif().equals("1")){
						checkActive.setSelected(true);
					}else{
						checkActive.setSelected(false);
					}
					if(nt.getcOnselldone().equals("1")){
						checkOnSell.setSelected(true);
					}else{
						checkOnSell.setSelected(false);
					}
					if(nt.getcOnbuydone().equals("1")){
						checkOnBuy.setSelected(true);
					}else{
						checkOnBuy.setSelected(false);
					}
					if(nt.getcSendsms().equals("1")){
						checkAktifSms.setSelected(true);
					}else{
						checkAktifSms.setSelected(false);
					}
					if(nt.getcSendmail().equals("1")){
						checkAktifEmail.setSelected(true);
					}else{
						checkAktifEmail.setSelected(false);
					}
					
						
					
					txtNohp1.setText(nt != null ? nt.getcMobile1():"");
					txtNohp2.setText(nt != null ? nt.getcMobile2():"");
					txtNohp3.setText(nt != null ? nt.getcMobile3():"");
					txtEmail1.setText(nt != null ? nt.getcEmail1():"");
					txtEmail2.setText(nt != null ? nt.getcEmail2():"");
					txtEmail3.setText(nt != null ? nt.getcEmail3():"");
				/*}catch (Exception e) {
					e.printStackTrace();
				}
				
					
			}
		});*/
		
		
	}

	void notAktif(){
		txtNohp1.setEnabled(false); txtNohp2.setEnabled(false);
		txtNohp3.setEnabled(false); txtEmail1.setEnabled(false);
		txtEmail2.setEnabled(false); txtEmail3.setEnabled(false);
	}
	
	@Override
	public void saveSetting() {
		hSetting.put("size",new Rectangle(frame.getX(), frame.getY(),
				frame.getWidth(), frame.getHeight()));
		//hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String)getProperty(C_PROPERTY_NAME),hSetting);

		
	}

}
