package eqtrade.trading.app;

import java.awt.AWTEvent;
import java.awt.Toolkit;
import java.awt.event.*;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import quickfix.field.AppStatus;

//import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion.User;
import com.vollux.framework.Action;
import com.vollux.framework.IApp;
import com.vollux.framework.VolluxAction;
import com.vollux.framework.VolluxApp;
import com.vollux.framework.VolluxEvent;
import com.vollux.framework.VolluxUI;
import com.vollux.idata.GridModel;

import eqtrade.application.IEQTradeApp;
import eqtrade.application.UIMaster;
import eqtrade.feed.app.FeedAction;
import eqtrade.feed.app.FeedApplication;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.IFeedApp;
import eqtrade.feed.core.IMessage;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.StockSummary;
import eqtrade.trading.Margin.UIMarginMonitoring;
import eqtrade.trading.core.ITradingApp;
import eqtrade.trading.core.ITradingListener;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingEngine;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.engine.socket.TradingSocketConnection;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Portfolio;
import eqtrade.trading.model.UserProfile;
import eqtrade.trading.ui.UIAmendOrder;
import eqtrade.trading.ui.UIEntryCrossing;
import eqtrade.trading.ui.UIEntryNegdeal;
import eqtrade.trading.ui.UIMarginCall;
import eqtrade.trading.ui.UIMarginCallPopUp;
import eqtrade.trading.ui.UIOrder;
import eqtrade.trading.ui.UIPortfolio;
import eqtrade.trading.ui.UITrade;
import eqtrade.trading.ui.core.UIChgPIN;
import eqtrade.trading.ui.core.UIChgPassword;
import eqtrade.trading.ui.core.UIPIN;
import eqtrade.trading.ui.core.UIPIN_NEGO;
import eqtrade.trading.ui.core.UIReconnect;
import eqtrade.trading.ui.core.UIRefresh;

public class TradingApplication extends VolluxApp implements IEQTradeApp,
		ITradingListener, IMessage {
	private final Log log = LogFactory.getLog(getClass());
	public final static String CONS_TRADING = "trading";
	private ITradingApp tradingApp;
	private IFeedApp feedApp;
	private TradingEventDispatcher event;
	private TradingAction action;
	private TradingUI ui;
	FeedApplication feed;
	private IApp apps;
	public GetUserId GuserId;
	// -------------------------------------------------------------------------------

	@Override
	public void start(String[] args) {
		log.info("starting trading application");
		tradingApp = new TradingEngine();
		tradingApp.start();
		tradingApp.addListener(this);
		event = new TradingEventDispatcher(this);
		action = new TradingAction(this);
		ui = new TradingUI(this);
		Toolkit.getDefaultToolkit().addAWTEventListener(awtKeyEventListener,
				AWTEvent.KEY_EVENT_MASK | AWTEvent.MOUSE_EVENT_MASK);
	}

	@Override
	public VolluxEvent getEventDispatcher() {
		return event;
	}

	@Override
	public VolluxAction getAction() {
		return action;
	}

	@Override
	public VolluxUI getUI() {
		return ui;
	}

	public void setFeedApp(IFeedApp feedApp) {
		this.feedApp = feedApp;
	}

	@Override
	public IFeedApp getFeedEngine() {
		return feedApp;
	}

	@Override
	public ITradingApp getTradingEngine() {
		return tradingApp;
	}

	public void setFeed(FeedApplication feed) {
		this.feed = feed;
	}

	public FeedApplication getFeed() {
		return feed;
	}

	@Override
	public void changePwdFailed(String msg) {
		((UIChgPassword) ui.getForm(TradingUI.UI_CHGPASS)).failed(msg);
	}

	@Override
	public void changePwdOK() {
		((UIChgPassword) ui.getForm(TradingUI.UI_CHGPASS)).success();
	}

	@Override
	public void changePINFailed(String msg) {
		((UIChgPIN) ui.getForm(TradingUI.UI_CHGPIN)).failed(msg);
	}

	@Override
	public void changePINOK() {
		((UIChgPIN) ui.getForm(TradingUI.UI_CHGPIN)).success();
	}

	@Override
	public void checkPINFailed(String msg) {
		((UIPIN) ui.getForm(TradingUI.UI_PIN)).failed(msg); 
	}

	@Override
	public void checkPINOK() {
		try {
			UserProfile sales = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
			//System.out.println("pin profileid : "+sales.getProfileId().toUpperCase());
			if ((sales.getProfileId().toUpperCase().equals("SALES") || sales.getProfileId().toUpperCase().equals("SUPERUSER")) && sales.isAllowed()) {
				
				applySecurity();
				getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
				getFeed().getAction().get(FeedAction.A_PINOFF)
					.setGranted(false);
				getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
				getFeed().getAction().get(FeedAction.A_PINOFF)
					.setEnabled(false);
				getFeed().getAction().get(FeedAction.A_SHOWCASHDEPOSIT)
					.setEnabled(false);
	//			getFeed().getAction().get(FeedAction.A_SHOWCASHWITH)
	//				.setEnabled(false);
				action.get(TradingAction.A_SHOWCHGPIN).setEnabled(false);
				action.get(TradingAction.A_SHOWCHGPIN).setGranted(false);
			
				UIMaster.changeIcon(UIMaster.lblBuy, new ImageIcon(UIMaster.class.getResource("buy.jpg")));
				UIMaster.changeIcon(UIMaster.lblWithdraw, new ImageIcon(UIMaster.class.getResource("withdraw.jpg")));
				UIMaster.changeIcon(UIMaster.lblAmend, new ImageIcon(UIMaster.class.getResource("amend.jpg")));
				UIMaster.changeIcon(UIMaster.lblSell, new ImageIcon(UIMaster.class.getResource("sell.jpg")));
				stopPinTimeout();// stop pin  
			} 
			else
			{
				((UIPIN) ui.getForm(TradingUI.UI_PIN)).success();
				applySecurity();
				System.out.println("Second Stage");
				((UIMaster) getUIMain()).checkIconLogin();
				startPinTimeout(30);	
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	@Override
	public void disconnected() {
		((UIMaster) getUIMain()).setProperty("trading_disconnect", null);
		action.setState(Action.C_DISCONNECTED_STATE);
		ui.showUI(TradingUI.UI_RECONNECT);
		getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
		if (feed.getFeedEngine().getUserId().equals("")) {
			feed.getAction().get(FeedAction.A_SAVE).setEnabled(false);
			feed.getAction().get(FeedAction.A_LOAD).setEnabled(false);
		}

	}

	public void close() {
		getUI().save();
		getTradingEngine().logout();
		action.setState(Action.C_NOTREADY_STATE);
		disableTrading();
		TradingSetting.save();
		getUI().close();
		((UIMaster) getUIMain()).setProperty("trading_logout", null);
		getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
		if (feed.getFeedEngine().getUserId().equals("")) {
			feed.getAction().get(FeedAction.A_SAVE).setEnabled(false);
			feed.getAction().get(FeedAction.A_LOAD).setEnabled(false);
		}
		getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(false);
		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(false);
	//yosep 27112014
		((UIPortfolio) ui.getForm(TradingUI.UI_PF)).LoadDefault();
	}

	@Override
	public void killed(String msg) {
		close();
		getFeed().close();
		Utils.showMessage("You have been sign out because you signed in on a different computer or device",
				null);
	}

	@Override
	public void loginFailed(String msg) {
		((eqtrade.feed.ui.core.UILogon) getFeed().getUI().getForm(
				FeedUI.UI_LOGON)).tradingFailed(msg);
		action.setState(Action.C_NOTREADY_STATE);
	}

	@Override
	public void loginOK() {
		disableTrading();
		TradingSetting.load();
		action.setState(Action.C_READY_STATE);
		((UIMaster) getUIMain()).setProperty("trading_login", null);
		getUIMain().show();
		ui.load();//apps.getUIMain().show();
		getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
		updatePFLastPrice(); 

		if (feed.getFeedEngine().getUserId().equals("")) {
			feed.getAction().get(FeedAction.A_SAVE).setEnabled(true);
			feed.getAction().get(FeedAction.A_LOAD).setEnabled(true);
		}
		getFeed().logonSuccessfully();
		UserProfile sales = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
		
		if(TradingSetting.getcAnnouncement()){
    		ui.showUI(ui.UI_ANNOUCEMENT);
		} 
		
		if (sales != null) {
			
			System.out.println("found user with profileid : " + sales.getProfileId()+" user id= "+sales.getUserId());
			
			if ((sales.getProfileId().toUpperCase().equals("SALES") || sales.getProfileId().toUpperCase().equals("SUPERUSER")) && sales.isAllowed()) {
				ui.cekLogin(sales.getUserId());			
				getTradingEngine().checkPINSLS(getTradingEngine().getUserId(), "LETMEPASS");
				getFeed().getAction().get(FeedAction.A_SHOWLINKACCOUNT).setGranted(false);
				getFeed().getAction().get(FeedAction.A_SHOWLOGINSIMASPOL).setGranted(false);
				getFeed().getAction().get(FeedAction.A_SHOWLINKACCOUNT).setEnabled(false);
				getFeed().getAction().get(FeedAction.A_SHOWLOGINSIMASPOL).setEnabled(false);

				//System.out.println("found user : " + getTradingEngine().getUserId());
				/*applySecurity();
				getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
				getFeed().getAction().get(FeedAction.A_PINOFF)
						.setGranted(false);
				getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
				getFeed().getAction().get(FeedAction.A_PINOFF)
						.setEnabled(false);
				getFeed().getAction().get(FeedAction.A_SHOWCASHDEPOSIT)
						.setEnabled(false);
				getFeed().getAction().get(FeedAction.A_SHOWCASHWITH)
						.setEnabled(false);
				action.get(TradingAction.A_SHOWCHGPIN).setEnabled(false);
				action.get(TradingAction.A_SHOWCHGPIN).setGranted(false);*/
			
				
			} else if (sales.getProfileId().toUpperCase().equals("CUSTOMER")) {
				
				/*
				 * action.get(TradingAction.A_SHOWNEGDEAL).setGranted(false);
				 * action.get(TradingAction.A_SHOWBUYADVERTISING)
				 * .setGranted(false);
				 * action.get(TradingAction.A_SHOWSELLADVERTISING).setGranted(
				 * false);
				 * action.get(TradingAction.A_SHOWBUYNEGDEAL).setGranted(false);
				 * action
				 * .get(TradingAction.A_SHOWSELLNEGDEAL).setGranted(false);
				 * action.get(TradingAction.A_SHOWCROSSING).setGranted(false);
				 */
				action.get(TradingAction.A_SHOWORDERMATRIC).setGranted(false);
				action.get(TradingAction.A_SHOWBUYORDER).setGranted(false);
				action.get(TradingAction.A_SHOWSELLORDER).setGranted(false);
				action.get(TradingAction.A_SHOWAMENDORDER).setGranted(false);
				action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(false);
				
				action.get(TradingAction.A_SHOWADVERTISING).setEnabled(true);
				action.get(TradingAction.A_SHOWADVERTISING).setGranted(true);


				/*
				 * action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(true);
				 * action
				 * .get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(true);
				 * action.get(TradingAction.A_SHOWSELLADVERTISING)
				 * .setEnabled(true);
				 * action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(true);
				 * action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(true);
				 * action.get(TradingAction.A_SHOWCROSSING).setEnabled(true);
				 */
				action.get(TradingAction.A_SHOWORDERMATRIC).setEnabled(true);
				action.get(TradingAction.A_SHOWBUYORDER).setEnabled(true);
				action.get(TradingAction.A_SHOWSELLORDER).setEnabled(true);
				action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(true);
				action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(true);
				action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setEnabled(true);
				getFeed().getAction().get(FeedAction.A_SHOWLINKACCOUNT).setGranted(true);
				getFeed().getAction().get(FeedAction.A_SHOWLOGINSIMASPOL).setGranted(true);
				getFeed().getAction().get(FeedAction.A_SHOWLINKACCOUNT).setEnabled(true);
				getFeed().getAction().get(FeedAction.A_SHOWLOGINSIMASPOL).setEnabled(true);
//				TradingSetting.setPopup(true); yosep 27042016 popup customer
				
				Account ac = (Account) getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByIndex(0);
				if (ac != null) {
					if (ac.getInvestorAccount().isEmpty()) {
						// TODO: kasih popup
						Utils.showMessage("                              Anda belum mempunyai Investor Account.\n                                          Untuk informasi lebih lanjut,\n      silahkan hubungi kami di 021-3925550 atau email cs@sinarmassekuritas.co.id\nMohon mengabaikan pesan ini, jika Anda telah mengisi formulir pembukaan rekening.",
								null);
					}//yosep expktp
					if (ac.getcExpktp().equals("1")) {
						Utils.showMessage("                                            Kartu indentitas Bapak/Ibu yang terdaftar sudah kadaluarsa.\n   Mohon kesediaan Bapak/Ibu untuk melakukan update Kartu Identitas dengan mengirimkannya ke cabang kami yang terdekat.\n               Untuk informasi lebih lanjut, silahkan hubungi kami di 021-50507000 atau email cs@sinarmassekuritas.co.id.\n                                                     Terima kasih atas perhatian dan kerjasamanya.",
								null);
					}
				}	
			}
		}

	}

	public void disableTrading() {
		action.get(TradingAction.A_SHOWSPLITORDER).setGranted(false);
		UserProfile simple = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "38" }, new int[] { UserProfile.C_MENUID });
		action.get(TradingAction.A_SHOWPFSIMPLE).setGranted(simple == null ? false : simple.isAllowed());
		if (simple == null || !simple.isAllowed()) {
			if (getUI().getForm(TradingUI.UI_PFSIMPLE).isReady())
				getUI().getForm(TradingUI.UI_PF).close();
		}
		if (getUI().getForm(TradingUI.UI_ENTRYADVERTISING).isReady())
			getUI().getForm(TradingUI.UI_ENTRYADVERTISING).close();
		if (getUI().getForm(TradingUI.UI_ENTRYCROSSING).isReady())
			getUI().getForm(TradingUI.UI_ENTRYCROSSING).close();
		if (getUI().getForm(TradingUI.UI_ENTRYNEGDEAL).isReady())
			getUI().getForm(TradingUI.UI_ENTRYNEGDEAL).close();
		
		if (getUI().getForm(TradingUI.UI_ORDERMATRIX).isReady())
			getUI().getForm(TradingUI.UI_ORDERMATRIX).close();
		
		if (getUI().getForm(TradingUI.UI_ENTRYORDER).isReady())
			getUI().getForm(TradingUI.UI_ENTRYORDER).close();
		if (getUI().getForm(TradingUI.UI_NEGDEALLIST).isReady())
			getUI().getForm(TradingUI.UI_NEGDEALLIST).close();
		if (getUI().getForm(TradingUI.UI_SL).isReady())
			getUI().getForm(TradingUI.UI_SL).close();
		if (getUI().getForm(TradingUI.UI_NEWSCHEDULE).isReady())
			getUI().getForm(TradingUI.UI_NEWSCHEDULE).close();
		if (getUI().getForm(TradingUI.UI_BYORDERID).isReady())
			getUI().getForm(TradingUI.UI_BYORDERID).close();
		if (getUI().getForm(TradingUI.UI_BYSCHEDULLABEL).isReady())
			getUI().getForm(TradingUI.UI_BYSCHEDULLABEL).close();
		if (getUI().getForm(TradingUI.UI_BYSTOCKID).isReady())
			getUI().getForm(TradingUI.UI_BYSTOCKID).close();
//		log.info("stockid "+getUI().getForm(TradingUI.UI_BYSTOCKID).isReady()+" " +
//				" order id "+getUI().getForm(TradingUI.UI_BYORDERID).isReady()+" " +
//						"new schedule "+getUI().getForm(TradingUI.UI_NEWSCHEDULE).isReady()+" " +
//								"schedule id "+getUI().getForm(TradingUI.UI_BYSCHEDULLABEL).isReady()+" " +
//										"list schedule "+getUI().getForm(TradingUI.UI_SL).isReady());
		action.get(TradingAction.A_SHOWNEGDEAL).setGranted(false);
		action.get(TradingAction.A_SHOWBUYADVERTISING).setGranted(false);
		action.get(TradingAction.A_SHOWSELLADVERTISING).setGranted(false);
		action.get(TradingAction.A_SHOWBUYNEGDEAL).setGranted(false);
		action.get(TradingAction.A_SHOWSELLNEGDEAL).setGranted(false);
		action.get(TradingAction.A_SHOWCROSSING).setGranted(false);
		
		action.get(TradingAction.A_SHOWORDERMATRIC).setGranted(false);
		
		action.get(TradingAction.A_SHOWBUYORDER).setGranted(false);
		action.get(TradingAction.A_SHOWSELLORDER).setGranted(false);
		action.get(TradingAction.A_SHOWAMENDORDER).setGranted(false);
		action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(false);
		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setGranted(false);//new schedule

		action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(true);
		action.get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(true);
		action.get(TradingAction.A_SHOWSELLADVERTISING).setEnabled(true);
		action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(true);
		action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(true);
		action.get(TradingAction.A_SHOWCROSSING).setEnabled(true);
		
		action.get(TradingAction.A_SHOWORDERMATRIC).setEnabled(true);
		
		action.get(TradingAction.A_SHOWBUYORDER).setEnabled(true);
		action.get(TradingAction.A_SHOWSELLORDER).setEnabled(true);
		action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(true);
		action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(true);
		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setEnabled(true);// new schedule

		/*
		 * action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(false);
		 * action.get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(false);
		 * action.get(TradingAction.A_SHOWSELLADVERTISING).setEnabled(false);
		 * action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(false);
		 * action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(false);
		 * action.get(TradingAction.A_SHOWCROSSING).setEnabled(false);
		 * action.get(TradingAction.A_SHOWBUYORDER).setEnabled(true);
		 * action.get(TradingAction.A_SHOWSELLORDER).setEnabled(false);
		 * action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(false);
		 * action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(false);
		 */

		getFeed().getAction().get(FeedAction.A_PINON).setGranted(true);
		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(false);
		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(true);
		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(false);

		action.get(TradingAction.A_SHOWCHGPIN).setEnabled(false);
		action.get(TradingAction.A_SHOWCHGPIN).setGranted(false);

		action.get(TradingAction.A_SHOWADVERTISING).setEnabled(true);
		action.get(TradingAction.A_SHOWADVERTISING).setGranted(true);
		
		
		UIMaster.changeIcon(UIMaster.lblBuy, new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));
		UIMaster.changeIcon(UIMaster.lblWithdraw, new ImageIcon(UIMaster.class.getResource("withdraw_hover.jpg")));
		UIMaster.changeIcon(UIMaster.lblAmend, new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
		UIMaster.changeIcon(UIMaster.lblSell, new ImageIcon(UIMaster.class.getResource("sell_hover.jpg")));

	}

	public void applySecurity() {
		getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(true);
		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(true);		
		action.get(TradingAction.A_SHOWCHGPIN).setEnabled(true);
		action.get(TradingAction.A_SHOWCHGPIN).setGranted(true);
/* ============================================================================================================================================================================ */	
		UserProfile split = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "36" }, new int[] { UserProfile.C_MENUID });
		action.get(TradingAction.A_SHOWSPLITORDER).setGranted(split == null ? false : split.isAllowed());
		action.get(TradingAction.A_SHOWSPLITORDER).setEnabled(split == null ? false : split.isAllowed());
/* ============================================================================================================================================================================ */		
		UserProfile simple = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "38" }, new int[] { UserProfile.C_MENUID });
		action.get(TradingAction.A_SHOWPFSIMPLE).setGranted(simple == null ? false : simple.isAllowed());
		action.get(TradingAction.A_SHOWPFSIMPLE).setEnabled(simple == null ? false : simple.isAllowed());
/* ============================================================================================================================================================================ */			
		UserProfile nego = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "39" }, new int[] { UserProfile.C_MENUID });
//		nego.setIsAllowed(nego.getProfileId().equalsIgnoreCase("SALES") ? "1" : nego.getIsAllowed());//nego sales
		action.get(TradingAction.A_SHOWNEGDEAL).setGranted(nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWBUYNEGDEAL).setGranted(nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWSELLNEGDEAL).setGranted(nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWCROSSING).setGranted(nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWCROSSING).setEnabled(nego == null ? false : nego.isAllowed());
/* ============================================================================================================================================================================ */					
		UserProfile advertising = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "41" }, new int[] { UserProfile.C_MENUID });
		action.get(TradingAction.A_SHOWBUYADVERTISING).setGranted(advertising == null ? false : advertising.isAllowed());
		action.get(TradingAction.A_SHOWSELLADVERTISING).setGranted(advertising == null ? false : advertising.isAllowed());
//		action.get(TradingAction.A_SHOWADVERTISING).setGranted(advertising == null ? false : advertising.isAllowed());
		action.get(TradingAction.A_SHOWADVERTISING).setGranted(true);
		action.get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(advertising == null ? false : advertising.isAllowed());
		action.get(TradingAction.A_SHOWSELLADVERTISING).setEnabled(advertising == null ? false : advertising.isAllowed());
//		action.get(TradingAction.A_SHOWADVERTISING).setEnabled(advertising == null ? false : advertising.isAllowed());
		action.get(TradingAction.A_SHOWADVERTISING).setEnabled(true);
		
/* ============================================================================================================================================================================ */			
		UserProfile trading = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
		action.get(TradingAction.A_SHOWBUYORDER).setGranted(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWSELLORDER).setGranted(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWAMENDORDER).setGranted(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setGranted(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWORDERMATRIC).setGranted(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWORDERMATRIC).setEnabled(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWBUYORDER).setEnabled(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWSELLORDER).setEnabled(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setEnabled(trading == null ? false : trading.isAllowed());
	}

	public void applySecurityDummy() {
		getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(true);
		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(true);
		action.get(TradingAction.A_SHOWCHGPIN).setEnabled(true);
		action.get(TradingAction.A_SHOWCHGPIN).setGranted(true);

		// for negdeal, split, and simple pf
		// UserProfile split =
		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
		// Object[]{"36"}, new int[]{UserProfile.C_MENUID});
		UserProfile split = new UserProfile();
		split.setIsAllowed("1");
		action.get(TradingAction.A_SHOWSPLITORDER).setGranted(
				split == null ? false : split.isAllowed());
		action.get(TradingAction.A_SHOWSPLITORDER).setEnabled(
				split == null ? false : split.isAllowed());
		// UserProfile simple =
		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
		// Object[]{"38"}, new int[]{UserProfile.C_MENUID});
		UserProfile simple = new UserProfile();
		simple.setIsAllowed("1");
		System.out.println("order menunya adalah: "
				+ (simple == null ? false : simple.isAllowed()));
		action.get(TradingAction.A_SHOWPFSIMPLE).setGranted(
				simple == null ? false : simple.isAllowed());
		action.get(TradingAction.A_SHOWPFSIMPLE).setEnabled(
				simple == null ? false : simple.isAllowed());
		// UserProfile nego =
		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
		// Object[]{"39"}, new int[]{UserProfile.C_MENUID});
		UserProfile nego = new UserProfile();
		nego.setIsAllowed("1");
		action.get(TradingAction.A_SHOWNEGDEAL).setGranted(
				nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWBUYADVERTISING).setGranted(
				nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWSELLADVERTISING).setGranted(
				nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWBUYNEGDEAL).setGranted(
				nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWSELLNEGDEAL).setGranted(
				nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWCROSSING).setGranted(
				nego == null ? false : nego.isAllowed());

		action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(
				nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(
				nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWSELLADVERTISING).setEnabled(
				nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(
				nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(
				nego == null ? false : nego.isAllowed());
		action.get(TradingAction.A_SHOWCROSSING).setEnabled(
				nego == null ? false : nego.isAllowed());
		// UserProfile trading =
		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
		// Object[]{"40"}, new int[]{UserProfile.C_MENUID});
		UserProfile trading = new UserProfile();
		trading.setIsAllowed("1");
		action.get(TradingAction.A_SHOWBUYORDER).setGranted(
				trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWSELLORDER).setGranted(
				trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWAMENDORDER).setGranted(
				trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(
				trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWBUYORDER).setEnabled(
				trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWSELLORDER).setEnabled(
				trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(
				trading == null ? false : trading.isAllowed());
		action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(
				trading == null ? false : trading.isAllowed());
	}

	@Override
	public void loginStatus(String msg) {
		((eqtrade.feed.ui.core.UILogon) getFeed().getUI().getForm(
				FeedUI.UI_LOGON)).setStatus(msg);
	}

	@Override
	public void reconnectFailed(String msg) {
		if (ui.getForm(TradingUI.UI_RECONNECT).isReady())
			((UIReconnect) ui.getForm(TradingUI.UI_RECONNECT)).failed(msg);
	}

	@Override
	public void reconnectOK() {
		UserProfile sales = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
		action.setState(Action.C_READY_STATE);
		((UIReconnect) ui.getForm(TradingUI.UI_RECONNECT)).success();
		((UIMaster) getUIMain()).setProperty("trading_login", null);
		getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
		if (feed.getFeedEngine().getUserId().equals(""))   {
			feed.getAction().get(FeedAction.A_SAVE).setEnabled(true);
			feed.getAction().get(FeedAction.A_LOAD).setEnabled(true);
		}
		if (sales.getProfileId().toUpperCase().equals("CUSTOMER")){
		//#Valdhy 20150227 -- Logout PIN jika terjadi trading reconnecting
			disableTrading();
		}
		
	}

	@Override
	public void reconnectStatus(String msg) {
		((UIReconnect) ui.getForm(TradingUI.UI_RECONNECT)).setStatus(msg);
	}

	@Override
	public void refreshFailed(String msg) {
		((UIRefresh) ui.getForm(TradingUI.UI_REFRESH)).failed(msg);
		((UIOrder) ui.getForm(TradingUI.UI_ORDER)).failed(msg);
		((UITrade) ui.getForm(TradingUI.UI_TRADE)).failed(msg);
	}

	@Override
	public void refreshOK() {
		((UIRefresh) ui.getForm(TradingUI.UI_REFRESH)).success();
		((UITrade) ui.getForm(TradingUI.UI_TRADE)).success();
		((UIOrder) ui.getForm(TradingUI.UI_ORDER)).success();
	}

	@Override
	public void refreshOKportfolio() {
		((UIPortfolio) ui.getForm(TradingUI.UI_PF)).successRefresh();
	}

	@Override
	public void refreshStatus(String msg) {
		((UIRefresh) ui.getForm(TradingUI.UI_REFRESH)).setStatus(msg);
	}

	@Override
	public void newMessage(Model arg0) {
		/*StockSummary ss = (StockSummary) arg0;
		if (ss.getBoard().equals("RG")) {
			int i = getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO)
					.getRowCount();
			for (int x = 0; x < i; x++) {
				Portfolio p = (Portfolio) getTradingEngine().getStore(
						TradingStore.DATA_PORTFOLIO).getDataByIndex(x);
				if (p != null) {
					//if (p.getStock().equals(ss.getCode())) {
					//}
					if (p.getStock().equals(ss.getCode())) {
					 p.setLastPrice(ss.getClosing());
					}
				}
			}
			getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).refresh();
		}*/
	}
	
	private void updatePFLastPrice(){
        int i = getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).getRowCount();
        for (int x = 0; x < i; x++) {
            Portfolio p = (Portfolio) getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).getDataByIndex(x);
            if (p != null) {
                StockSummary ss = (StockSummary)getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new Object[]{p.getStock(), "RG"});
                if (ss!=null){
                    p.setLastPrice(ss.getClosing());
                }
            }
        }
        getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).refresh();
    }
	
	// --------------- PIN TIMEOUT --------------------
	
	javax.swing.Timer tmrPinTimeout;
	boolean pinTimeoutActivated;
	
	public void startPinTimeout(int minutes) {
		tmrPinTimeout = new javax.swing.Timer(minutes*60000, new ActionListener(){
				public void actionPerformed(ActionEvent e) {
					// logout PIN
					stopPinTimeout();
					disableTrading();
					getUI().showUI(TradingUI.UI_PIN);
					
				}
			}
		);
		tmrPinTimeout.start();
		pinTimeoutActivated = tmrPinTimeout.isRunning();
	}
	
	public void stopPinTimeout() {
		tmrPinTimeout.stop();
		pinTimeoutActivated = tmrPinTimeout.isRunning();
	}
	
	public void restartPinTimeout() {
		tmrPinTimeout.restart();
	}
	
	public boolean timeoutActivated() {
		return pinTimeoutActivated;
	}
	
	AWTEventListener awtKeyEventListener = new AWTEventListener() {
		public void eventDispatched(AWTEvent event) {
			if (timeoutActivated()) {
				if (event.getID()==	MouseEvent.MOUSE_PRESSED) restartPinTimeout();
				if (event.getID()==	KeyEvent.KEY_PRESSED) restartPinTimeout();
			}
		}
	};
	
	@Override
	public void checkPIN_NEGO_OK(String form) {
		// TODO Auto-generated method stub
		System.out.println("cek ok"+TradingAction.A_SHOWNEGDEAL.toString()+" --"+form);
		((UIPIN_NEGO) ui.getForm(TradingUI.UI_PIN_NEGO)).hide();
		System.out.println("cek ok"+TradingAction.A_SHOWNEGDEAL.toString()+" --"+form);
		if(form.equalsIgnoreCase(TradingAction.A_SHOWNEGDEAL.toString())){System.out.println(form);
		((UIEntryNegdeal) ui.getForm(TradingUI.UI_ENTRYNEGDEAL)).confirm();
		//((UIEntryNegdeal) ui.getForm(TradingUI.UI_PIN_NEGO)).hide();
		}else if(form.equalsIgnoreCase(TradingAction.A_SHOWCROSSING.toString())){
		((UIEntryCrossing) ui.getForm(TradingUI.UI_ENTRYCROSSING)).confirm();			
		}else if(form.equalsIgnoreCase(TradingAction.A_SHOWAMENDORDER.toString())){
			((UIAmendOrder) ui.getForm(TradingUI.UI_AMENDORDER)).confirm();	
		}
		((UIPIN_NEGO) ui.getForm(TradingUI.UI_PIN_NEGO)).setState(true);
		//((UIEntryNegdeal) ui.getForm(TradingUI.UI_PIN_NEGO)).hide();
		//((UIPIN_NEGO) ui.getForm(TradingUI.UI_PIN_NEGO)).hide();
		//((UIPIN_NEGO) ui.getForm(TradingUI.UI_PIN_NEGO)).berhasil();
	}

	@Override
	public void checkPIN_NEGO_FAILED(String msg) {
		// TODO Auto-generated method stub
		((UIPIN_NEGO) ui.getForm(TradingUI.UI_PIN_NEGO)).failed(msg);
		if(msg.equalsIgnoreCase(TradingAction.A_SHOWNEGDEAL.toString())){System.out.println(msg);
		((UIEntryNegdeal) ui.getForm(TradingUI.UI_ENTRYNEGDEAL)).nconfirm = 1;
		}else if(msg.equalsIgnoreCase(TradingAction.A_SHOWCROSSING.toString())){
		((UIEntryCrossing) ui.getForm(TradingUI.UI_ENTRYCROSSING)).nconfirm = 1;
		}
	}

	@Override
	public void refreshOKmargin() {
		// TODO Auto-generated method stub
		((UIMarginMonitoring) ui.getForm(TradingUI.UI_MM)).successRefresh();		
	}

	@Override
	public void addMarginCall(final GridModel arg0) {
		// TODO Auto-generated method stub
		if (ui.getForm(TradingUI.UI_MARGINCALLPOPUP).isReady()) {
			if (ui.getForm(TradingUI.UI_MARGINCALLPOPUP).isShow()) {
				ui.getForm(TradingUI.UI_MARGINCALLPOPUP).close();
			}
		}
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				((UIMarginCallPopUp) ui.getForm(TradingUI.UI_MARGINCALLPOPUP)).addMargin(arg0);
				ui.showUI(TradingUI.UI_MARGINCALLPOPUP);
			}
		});
//		((UIMarginCallPopUp) ui.getForm(TradingUI.UI_MARGINCALLPOPUP)).show(arg0);
	}
}

//package eqtrade.trading.app;
//
//import java.awt.AWTEvent;
//import java.awt.Toolkit;
//import java.awt.event.*;
//
//import javax.swing.ImageIcon;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//
//import quickfix.field.AppStatus;
//
//
////import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion.User;
//import com.vollux.framework.Action;
//import com.vollux.framework.IApp;
//import com.vollux.framework.VolluxAction;
//import com.vollux.framework.VolluxApp;
//import com.vollux.framework.VolluxEvent;
//import com.vollux.framework.VolluxUI;
//
//import eqtrade.application.IEQTradeApp;
//import eqtrade.application.UIMaster;
//import eqtrade.feed.app.FeedAction;
//import eqtrade.feed.app.FeedApplication;
//import eqtrade.feed.app.FeedUI;
//import eqtrade.feed.core.IFeedApp;
//import eqtrade.feed.core.IMessage;
//import eqtrade.feed.core.Utils;
//import eqtrade.feed.engine.FeedParser;
//import eqtrade.feed.engine.FeedStore;
//import eqtrade.feed.model.Model;
//import eqtrade.feed.model.StockSummary;
//import eqtrade.trading.Margin.UIMarginMonitoring;
//import eqtrade.trading.core.ITradingApp;
//import eqtrade.trading.core.ITradingListener;
//import eqtrade.trading.core.TradingSetting;
//import eqtrade.trading.engine.TradingEngine;
//import eqtrade.trading.engine.TradingStore;
//import eqtrade.trading.model.Account;
//import eqtrade.trading.model.Portfolio;
//import eqtrade.trading.model.UserProfile;
//import eqtrade.trading.ui.UIOrder;
//import eqtrade.trading.ui.UIPortfolio;
//import eqtrade.trading.ui.UITrade;
//import eqtrade.trading.ui.core.UIChgPIN;
//import eqtrade.trading.ui.core.UIChgPassword;
//import eqtrade.trading.ui.core.UIPIN;
//import eqtrade.trading.ui.core.UIReconnect;
//import eqtrade.trading.ui.core.UIRefresh;
//
//public class TradingApplication extends VolluxApp implements IEQTradeApp,
//		ITradingListener, IMessage {
//	private final Log log = LogFactory.getLog(getClass());
//	public final static String CONS_TRADING = "trading";
//	private ITradingApp tradingApp;
//	private IFeedApp feedApp;
//	private TradingEventDispatcher event;
//	private TradingAction action;
//	private TradingUI ui;
//	FeedApplication feed;
//	private IApp apps;
//	public GetUserId GuserId;
//
//	// -------------------------------------------------------------------------------
//
//	@Override
//	public void start(String[] args) {
//		// log.info("starting trading application");
//		tradingApp = new TradingEngine();
//		tradingApp.start();
//		tradingApp.addListener(this);
//		event = new TradingEventDispatcher(this);
//		action = new TradingAction(this);
//		ui = new TradingUI(this);
//		Toolkit.getDefaultToolkit().addAWTEventListener(awtKeyEventListener,
//				AWTEvent.KEY_EVENT_MASK | AWTEvent.MOUSE_EVENT_MASK);
//	}
//
//	@Override
//	public VolluxEvent getEventDispatcher() {
//		return event;
//	}
//
//	@Override
//	public VolluxAction getAction() {
//		return action;
//	}
//
//	@Override
//	public VolluxUI getUI() {
//		return ui;
//	}
//
//	public void setFeedApp(IFeedApp feedApp) {
//		this.feedApp = feedApp;
//	}
//
//	@Override
//	public IFeedApp getFeedEngine() {
//		return feedApp;
//	}
//
//	@Override
//	public ITradingApp getTradingEngine() {
//		return tradingApp;
//	}
//
//	public void setFeed(FeedApplication feed) {
//		this.feed = feed;
//	}
//
//	public FeedApplication getFeed() {
//		return feed;
//	}
//
//	@Override
//	public void changePwdFailed(String msg) {
//		((UIChgPassword) ui.getForm(TradingUI.UI_CHGPASS)).failed(msg);
//	}
//
//	@Override
//	public void changePwdOK() {
//		((UIChgPassword) ui.getForm(TradingUI.UI_CHGPASS)).success();
//	}
//
//	@Override
//	public void changePINFailed(String msg) {
//		((UIChgPIN) ui.getForm(TradingUI.UI_CHGPIN)).failed(msg);
//	}
//
//	@Override
//	public void changePINOK() {
//		((UIChgPIN) ui.getForm(TradingUI.UI_CHGPIN)).success();
//	}
//
//	@Override
//	public void checkPINFailed(String msg) {
//		((UIPIN) ui.getForm(TradingUI.UI_PIN)).failed(msg);
//	}
//
//	@Override
//	public void checkPINOK() {
//		log.info("CHECK PIN OK");
//		UserProfile sales = (UserProfile) getTradingEngine().getStore(
//				TradingStore.DATA_USERPROFILE).getDataByField(
//				new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
//		
//		UserProfile salesnego = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "39" }, new int[] { UserProfile.C_MENUID });
//		
//		log.info("pin profileid : " + sales.getProfileId().toUpperCase() + " "	+ sales.isAllowed());
//		
//		//if ((sales.getProfileId().toUpperCase().equals("SALES") || sales.getProfileId().toUpperCase().equals("SUPERUSER")) && sales.isAllowed()) {
//		//if (!sales.getProfileId().equals("CUSTOMER") && (sales.isAllowed() || salesnego.isAllowed())) {
//		if ((sales.getProfileId().toUpperCase().equals("SALES") || sales.getProfileId().toUpperCase().equals("SUPERUSER")) && sales.isAllowed()) {
//			applySecurity();
//			getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
//			getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(false);
//			getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
//			getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(false);
//			getFeed().getAction().get(FeedAction.A_SHOWCASHDEPOSIT).setEnabled(false);
//			action.get(TradingAction.A_SHOWCHGPIN).setEnabled(false);
//			action.get(TradingAction.A_SHOWCHGPIN).setGranted(false);
//
//			UIMaster.changeIcon(UIMaster.lblBuy,new ImageIcon(UIMaster.class.getResource("buy.jpg")));
//			UIMaster.changeIcon(UIMaster.lblWithdraw, new ImageIcon(UIMaster.class.getResource("withdraw.jpg")));
//			UIMaster.changeIcon(UIMaster.lblAmend,new ImageIcon(UIMaster.class.getResource("amend.jpg")));
//			UIMaster.changeIcon(UIMaster.lblSell,new ImageIcon(UIMaster.class.getResource("sell.jpg")));
//		} else {
//			((UIPIN) ui.getForm(TradingUI.UI_PIN)).success();
//			applySecurity();
//			((UIMaster) getUIMain()).checkIconLogin();
//			startPinTimeout(30);
//		}
//
//		log.info("done checkpinok : " + sales.getProfileId().toUpperCase() + " " + sales.isAllowed());
//	}
//
//	@Override
//	public void disconnected() {
//		((UIMaster) getUIMain()).setProperty("trading_disconnect", null);
//		action.setState(Action.C_DISCONNECTED_STATE);
//		ui.showUI(TradingUI.UI_RECONNECT);
//		getFeedEngine().getEngine().getParser()
//				.get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
//		if (feed.getFeedEngine().getUserId().equals("")) {
//			feed.getAction().get(FeedAction.A_SAVE).setEnabled(false);
//			feed.getAction().get(FeedAction.A_LOAD).setEnabled(false);
//		}
//
//	}
//
//	public void close() {
//		getUI().save();
//		getTradingEngine().logout();
//		action.setState(Action.C_NOTREADY_STATE);
//		disableTrading();
//		TradingSetting.save();
//		getUI().close();
//		((UIMaster) getUIMain()).setProperty("trading_logout", null);
//		getFeedEngine().getEngine().getParser()
//				.get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
//		if (feed.getFeedEngine().getUserId().equals("")) {
//			feed.getAction().get(FeedAction.A_SAVE).setEnabled(false);
//			feed.getAction().get(FeedAction.A_LOAD).setEnabled(false);
//		}
//		getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
//		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(false);
//		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
//		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(false);
//
//	}
//
//	@Override
//	public void killed(String msg) {
//		close();
//		getFeed().close();
//		Utils.showMessage("You have been sign out because you signed in on a different computer or device",null);
//		Runtime.getRuntime().halt(0);
//		System.exit(-1);
//	}
//
//	@Override
//	public void loginFailed(String msg) {
//		((eqtrade.feed.ui.core.UILogon) getFeed().getUI().getForm(
//				FeedUI.UI_LOGON)).tradingFailed(msg);
//		action.setState(Action.C_NOTREADY_STATE);
//	}
//
//	@Override
//	public void loginOK() {
//		disableTrading();
//		TradingSetting.load();
//		action.setState(Action.C_READY_STATE);
//		((UIMaster) getUIMain()).setProperty("trading_login", null);
//		getUIMain().show();
//		ui.load();// apps.getUIMain().show();
//		// getFeedEngine().getEngine().getParser()
//		// .get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
//		updatePFLastPrice();
//
//		if (feed.getFeedEngine().getUserId().equals("")) {
//			feed.getAction().get(FeedAction.A_SAVE).setEnabled(true);
//			feed.getAction().get(FeedAction.A_LOAD).setEnabled(true);
//		}
//		getFeed().logonSuccessfully();
//		UserProfile sales = (UserProfile) getTradingEngine().getStore(
//				TradingStore.DATA_USERPROFILE).getDataByField(
//				new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
//		
////		UserProfile salesnego = (UserProfile) getTradingEngine().getStore(
////				TradingStore.DATA_USERPROFILE).getDataByField(
////				new Object[] { "39" }, new int[] { UserProfile.C_MENUID });
//		
//		if (TradingSetting.getcAnnouncement()) {
//			ui.showUI(ui.UI_ANNOUCEMENT);
//		}
//		if (sales != null) {
//			//if ((sales.getProfileId().toUpperCase().equals("SALES") || sales.getProfileId().toUpperCase().equals("SUPERUSER")) && sales.isAllowed()) {
//			// 20140305 if ((sales.getProfileId().toUpperCase().equals("SALES") || sales.getProfileId().toUpperCase().equals("SUPERUSER")) && sales.isAllowed()) {
//			//if (sales.getProfileId().toUpperCase().equals("SUPERUSER") && sales.isAllowed()) {
//			//if (!sales.getProfileId()..toUpperCase().equals("CUSTOMER") && (sales.isAllowed() || salesnego.isAllowed())) {
//			if ((sales.getProfileId().toUpperCase().equals("SALES") || sales.getProfileId().toUpperCase().equals("SUPERUSER")) && sales.isAllowed()) {
//				ui.cekLogin(sales.getUserId());
//				getTradingEngine().checkPINSLS(getTradingEngine().getUserId(),"LETMEPASS");				
//			} else if (sales.getProfileId().toUpperCase().equals("CUSTOMER")) {
//				
//				action.get(TradingAction.A_SHOWBUYORDER).setGranted(false);
//				action.get(TradingAction.A_SHOWSELLORDER).setGranted(false);
//				action.get(TradingAction.A_SHOWAMENDORDER).setGranted(false);
//				action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(false);
//
//				action.get(TradingAction.A_SHOWADVERTISING).setEnabled(false);
//				action.get(TradingAction.A_SHOWADVERTISING).setGranted(false);
//
//			
//				action.get(TradingAction.A_SHOWBUYORDER).setEnabled(true);
//				action.get(TradingAction.A_SHOWSELLORDER).setEnabled(true);
//				action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(true);
//				action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(true);
//				action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setEnabled(true);
//				
//				TradingSetting.setPopup(true);
//
//				Account ac = (Account) getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByIndex(0);
//				if (ac != null) {
//					if (ac.getInvestorAccount().isEmpty()) {
//						Utils.showMessage(
//								"                              Anda belum mempunyai Investor Account.\n                                          Untuk informasi lebih lanjut,\n      silahkan hubungi kami di 021-3925550 atau email cs@sinarmassekuritas.co.id\nMohon mengabaikan pesan ini, jika Anda telah mengisi formulir pembukaan rekening.",
//								null);
//					}
//				}
//			}
//		}
//	}
//
//	public void disableTrading() {
//		action.get(TradingAction.A_SHOWSPLITORDER).setGranted(false);
//		UserProfile simple = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "38" }, new int[] { UserProfile.C_MENUID });
//		action.get(TradingAction.A_SHOWPFSIMPLE).setGranted(simple == null ? false : simple.isAllowed());
//		if (simple == null || !simple.isAllowed()) {
//			if (getUI().getForm(TradingUI.UI_PFSIMPLE).isReady())
//				getUI().getForm(TradingUI.UI_PF).close();
//		}
//		if (getUI().getForm(TradingUI.UI_ENTRYADVERTISING).isReady())
//			getUI().getForm(TradingUI.UI_ENTRYADVERTISING).close();
//		if (getUI().getForm(TradingUI.UI_ENTRYCROSSING).isReady())
//			getUI().getForm(TradingUI.UI_ENTRYCROSSING).close();
//		if (getUI().getForm(TradingUI.UI_ENTRYNEGDEAL).isReady())
//			getUI().getForm(TradingUI.UI_ENTRYNEGDEAL).close();
//		if (getUI().getForm(TradingUI.UI_ENTRYORDER).isReady())
//			getUI().getForm(TradingUI.UI_ENTRYORDER).close();
//		if (getUI().getForm(TradingUI.UI_NEGDEALLIST).isReady())
//			getUI().getForm(TradingUI.UI_NEGDEALLIST).close();
//		if (getUI().getForm(TradingUI.UI_SL).isReady())
//			getUI().getForm(TradingUI.UI_SL).close();
//		if (getUI().getForm(TradingUI.UI_NEWSCHEDULE).isReady())
//			getUI().getForm(TradingUI.UI_NEWSCHEDULE).close();
//		if (getUI().getForm(TradingUI.UI_BYORDERID).isReady())
//			getUI().getForm(TradingUI.UI_BYORDERID).close();
//		if (getUI().getForm(TradingUI.UI_BYSCHEDULLABEL).isReady())
//			getUI().getForm(TradingUI.UI_BYSCHEDULLABEL).close();
//		if (getUI().getForm(TradingUI.UI_BYSTOCKID).isReady())
//			getUI().getForm(TradingUI.UI_BYSTOCKID).close();
//		
//		action.get(TradingAction.A_SHOWNEGDEAL).setGranted(false);
//		action.get(TradingAction.A_SHOWBUYADVERTISING).setGranted(false);
//		action.get(TradingAction.A_SHOWSELLADVERTISING).setGranted(false);
//		action.get(TradingAction.A_SHOWBUYNEGDEAL).setGranted(false);
//		action.get(TradingAction.A_SHOWSELLNEGDEAL).setGranted(false);
//		action.get(TradingAction.A_SHOWCROSSING).setGranted(false);
//		action.get(TradingAction.A_SHOWBUYORDER).setGranted(false);
//		action.get(TradingAction.A_SHOWSELLORDER).setGranted(false);
//		action.get(TradingAction.A_SHOWAMENDORDER).setGranted(false);
//		action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(false);
//		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setGranted(false);
//
//		action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(true);
//		action.get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(true);
//		action.get(TradingAction.A_SHOWSELLADVERTISING).setEnabled(true);
//		action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(true);
//		action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(true);
//		action.get(TradingAction.A_SHOWCROSSING).setEnabled(true);
//		action.get(TradingAction.A_SHOWBUYORDER).setEnabled(true);
//		action.get(TradingAction.A_SHOWSELLORDER).setEnabled(true);
//		action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(true);
//		action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(true);
//		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setEnabled(true);
//		
//		getFeed().getAction().get(FeedAction.A_PINON).setGranted(true);
//		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(false);
//		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(true);
//		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(false);
//
//		action.get(TradingAction.A_SHOWCHGPIN).setEnabled(false);
//		action.get(TradingAction.A_SHOWCHGPIN).setGranted(false);
//
//		UIMaster.changeIcon(UIMaster.lblBuy,new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));
//		UIMaster.changeIcon(UIMaster.lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw_hover.jpg")));
//		UIMaster.changeIcon(UIMaster.lblAmend,new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
//		UIMaster.changeIcon(UIMaster.lblSell,new ImageIcon(UIMaster.class.getResource("sell_hover.jpg")));
//	}
//
//	public void applySecurity() {
//		getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
//		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(true);
//		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
//		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(true);		
//		action.get(TradingAction.A_SHOWCHGPIN).setEnabled(true);
//		action.get(TradingAction.A_SHOWCHGPIN).setGranted(true);
///* ============================================================================================================================================================================ */	
//		UserProfile split = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "36" }, new int[] { UserProfile.C_MENUID });
//		action.get(TradingAction.A_SHOWSPLITORDER).setGranted(split == null ? false : split.isAllowed());
//		action.get(TradingAction.A_SHOWSPLITORDER).setEnabled(split == null ? false : split.isAllowed());
///* ============================================================================================================================================================================ */		
//		UserProfile simple = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "38" }, new int[] { UserProfile.C_MENUID });
//		action.get(TradingAction.A_SHOWPFSIMPLE).setGranted(simple == null ? false : simple.isAllowed());
//		action.get(TradingAction.A_SHOWPFSIMPLE).setEnabled(simple == null ? false : simple.isAllowed());
///* ============================================================================================================================================================================ */			
//		UserProfile nego = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "39" }, new int[] { UserProfile.C_MENUID });
//		action.get(TradingAction.A_SHOWNEGDEAL).setGranted(nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWBUYNEGDEAL).setGranted(nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWSELLNEGDEAL).setGranted(nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWCROSSING).setGranted(nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWCROSSING).setEnabled(nego == null ? false : nego.isAllowed());
///* ============================================================================================================================================================================ */					
//		UserProfile advertising = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "41" }, new int[] { UserProfile.C_MENUID });
//		action.get(TradingAction.A_SHOWBUYADVERTISING).setGranted(advertising == null ? false : advertising.isAllowed());
//		action.get(TradingAction.A_SHOWSELLADVERTISING).setGranted(advertising == null ? false : advertising.isAllowed());
//		action.get(TradingAction.A_SHOWADVERTISING).setGranted(advertising == null ? false : advertising.isAllowed());
//		action.get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(advertising == null ? false : advertising.isAllowed());
//		action.get(TradingAction.A_SHOWSELLADVERTISING).setEnabled(advertising == null ? false : advertising.isAllowed());
//		action.get(TradingAction.A_SHOWADVERTISING).setEnabled(advertising == null ? false : advertising.isAllowed());
//		
///* ============================================================================================================================================================================ */			
//		UserProfile trading = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
//		action.get(TradingAction.A_SHOWBUYORDER).setGranted(trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWSELLORDER).setGranted(trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWAMENDORDER).setGranted(trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setGranted(trading == null ? false : trading.isAllowed());
//
//		action.get(TradingAction.A_SHOWBUYORDER).setEnabled(trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWSELLORDER).setEnabled(trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setEnabled(trading == null ? false : trading.isAllowed());
//	}
//
//	public void applySecurityDummy() {
//		getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
//		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(true);
//		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
//		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(true);
//		action.get(TradingAction.A_SHOWCHGPIN).setEnabled(true);
//		action.get(TradingAction.A_SHOWCHGPIN).setGranted(true);
//
//		// for negdeal, split, and simple pf
//		// UserProfile split =
//		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
//		// Object[]{"36"}, new int[]{UserProfile.C_MENUID});
//		UserProfile split = new UserProfile();
//		split.setIsAllowed("1");
//		action.get(TradingAction.A_SHOWSPLITORDER).setGranted(
//				split == null ? false : split.isAllowed());
//		action.get(TradingAction.A_SHOWSPLITORDER).setEnabled(
//				split == null ? false : split.isAllowed());
//		// UserProfile simple =
//		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
//		// Object[]{"38"}, new int[]{UserProfile.C_MENUID});
//		UserProfile simple = new UserProfile();
//		simple.setIsAllowed("1");
//		/*
//		 * System.out.println("order menunya adalah: " + (simple == null ? false
//		 * : simple.isAllowed()));
//		 */
//		action.get(TradingAction.A_SHOWPFSIMPLE).setGranted(
//				simple == null ? false : simple.isAllowed());
//		action.get(TradingAction.A_SHOWPFSIMPLE).setEnabled(
//				simple == null ? false : simple.isAllowed());
//		// UserProfile nego =
//		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
//		// Object[]{"39"}, new int[]{UserProfile.C_MENUID});
//		UserProfile nego = new UserProfile();
//		nego.setIsAllowed("1");
//		action.get(TradingAction.A_SHOWNEGDEAL).setGranted(
//				nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWBUYADVERTISING).setGranted(
//				nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWSELLADVERTISING).setGranted(
//				nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWBUYNEGDEAL).setGranted(
//				nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWSELLNEGDEAL).setGranted(
//				nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWCROSSING).setGranted(
//				nego == null ? false : nego.isAllowed());
//
//		action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(
//				nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(
//				nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWSELLADVERTISING).setEnabled(
//				nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(
//				nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(
//				nego == null ? false : nego.isAllowed());
//		action.get(TradingAction.A_SHOWCROSSING).setEnabled(
//				nego == null ? false : nego.isAllowed());
//		// UserProfile trading =
//		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
//		// Object[]{"40"}, new int[]{UserProfile.C_MENUID});
//		UserProfile trading = new UserProfile();
//		trading.setIsAllowed("1");
//		action.get(TradingAction.A_SHOWBUYORDER).setGranted(
//				trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWSELLORDER).setGranted(
//				trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWAMENDORDER).setGranted(
//				trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(
//				trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWBUYORDER).setEnabled(
//				trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWSELLORDER).setEnabled(
//				trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(
//				trading == null ? false : trading.isAllowed());
//		action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(
//				trading == null ? false : trading.isAllowed());
//	}
//
//	@Override
//	public void loginStatus(String msg) {
//		((eqtrade.feed.ui.core.UILogon) getFeed().getUI().getForm(
//				FeedUI.UI_LOGON)).setStatus(msg);
//	}
//
//	@Override
//	public void reconnectFailed(String msg) {
//		if (ui.getForm(TradingUI.UI_RECONNECT).isReady())
//			((UIReconnect) ui.getForm(TradingUI.UI_RECONNECT)).failed(msg);
//	}
//
//	@Override
//	public void reconnectOK() {
//		action.setState(Action.C_READY_STATE);
//		((UIReconnect) ui.getForm(TradingUI.UI_RECONNECT)).success();
//		((UIMaster) getUIMain()).setProperty("trading_login", null);
//		getFeedEngine().getEngine().getParser()
//				.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
//		if (feed.getFeedEngine().getUserId().equals("")) {
//			feed.getAction().get(FeedAction.A_SAVE).setEnabled(true);
//			feed.getAction().get(FeedAction.A_LOAD).setEnabled(true);
//		}
//	}
//
//	@Override
//	public void reconnectStatus(String msg) {
//		((UIReconnect) ui.getForm(TradingUI.UI_RECONNECT)).setStatus(msg);
//	}
//
//	@Override
//	public void refreshFailed(String msg) {
//		((UIRefresh) ui.getForm(TradingUI.UI_REFRESH)).failed(msg);
//		((UIOrder) ui.getForm(TradingUI.UI_ORDER)).failed(msg);
//		((UITrade) ui.getForm(TradingUI.UI_TRADE)).failed(msg);
//	}
//
//	@Override
//	public void refreshOK() {
//		((UIRefresh) ui.getForm(TradingUI.UI_REFRESH)).success();
//		((UITrade) ui.getForm(TradingUI.UI_TRADE)).success();
//		((UIOrder) ui.getForm(TradingUI.UI_ORDER)).success();
//	}
//
//	@Override
//	public void refreshOKportfolio() {
//		((UIPortfolio) ui.getForm(TradingUI.UI_PF)).successRefresh();
//	}
//
//	@Override
//	public void refreshStatus(String msg) {
//		((UIRefresh) ui.getForm(TradingUI.UI_REFRESH)).setStatus(msg);
//	}
//
//	@Override
//	public void newMessage(Model arg0) {
//		
//	}
//
//	private void updatePFLastPrice() {
//		int i = getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO)
//				.getRowCount();
//		for (int x = 0; x < i; x++) {
//			Portfolio p = (Portfolio) getTradingEngine().getStore(
//					TradingStore.DATA_PORTFOLIO).getDataByIndex(x);
//			if (p != null) {
//				StockSummary ss = (StockSummary) getFeedEngine().getStore(
//						FeedStore.DATA_STOCKSUMMARY).getDataByKey(
//						new Object[] { p.getStock(), "RG" });
//				if (ss != null) {
//					p.setLastPrice(ss.getClosing());
//				}
//			}
//		}
//		getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).refresh();
//	}
//
//	// --------------- PIN TIMEOUT --------------------
//
//	javax.swing.Timer tmrPinTimeout;
//	boolean pinTimeoutActivated;
//
//	public void startPinTimeout(int minutes) {
//		tmrPinTimeout = new javax.swing.Timer(minutes * 60000,
//				new ActionListener() {
//					public void actionPerformed(ActionEvent e) {
//						// logout PIN
//						stopPinTimeout();
//						disableTrading();
//						getUI().showUI(TradingUI.UI_PIN);
//
//					}
//				});
//		tmrPinTimeout.start();
//		pinTimeoutActivated = tmrPinTimeout.isRunning();
//	}
//
//	public void stopPinTimeout() {
//		tmrPinTimeout.stop();
//		pinTimeoutActivated = tmrPinTimeout.isRunning();
//	}
//
//	public void restartPinTimeout() {
//		tmrPinTimeout.restart();
//	}
//
//	public boolean timeoutActivated() {
//		return pinTimeoutActivated;
//	}
//
//	AWTEventListener awtKeyEventListener = new AWTEventListener() {
//		public void eventDispatched(AWTEvent event) {
//			// reset timer
//			if (timeoutActivated()) {
//				if (event.getID() == MouseEvent.MOUSE_PRESSED)
//					restartPinTimeout();
//				if (event.getID() == KeyEvent.KEY_PRESSED)
//					restartPinTimeout();
//			}
//		}
//	};
//
//	@Override
//	public void refreshOKmargin() {
//		((UIMarginMonitoring) ui.getForm(TradingUI.UI_MM)).successRefresh();
//	}
//	
//	
//	public boolean isSales() {
//		boolean isAllow = false;
//		UserProfile salesadv = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "41" }, new int[] { UserProfile.C_MENUID });
//		if(salesadv.getProfileId().equalsIgnoreCase("SALES")) {
//			isAllow = true;
//		}
//		return isAllow;
//	}
//}
//
////package eqtrade.trading.app;
////
////import java.awt.AWTEvent;
////import java.awt.Toolkit;
////import java.awt.event.*;
////
////import javax.swing.ImageIcon;
////
////import org.apache.commons.logging.Log;
////import org.apache.commons.logging.LogFactory;
////
////import quickfix.field.AppStatus;
////
//////import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion.User;
////import com.vollux.framework.Action;
////import com.vollux.framework.IApp;
////import com.vollux.framework.VolluxAction;
////import com.vollux.framework.VolluxApp;
////import com.vollux.framework.VolluxEvent;
////import com.vollux.framework.VolluxUI;
////
////import eqtrade.application.IEQTradeApp;
////import eqtrade.application.UIMaster;
////import eqtrade.feed.app.FeedAction;
////import eqtrade.feed.app.FeedApplication;
////import eqtrade.feed.app.FeedUI;
////import eqtrade.feed.core.IFeedApp;
////import eqtrade.feed.core.IMessage;
////import eqtrade.feed.core.Utils;
////import eqtrade.feed.engine.FeedParser;
////import eqtrade.feed.engine.FeedStore;
////import eqtrade.feed.model.Model;
////import eqtrade.feed.model.StockSummary;
////import eqtrade.trading.Margin.UIMarginMonitoring;
////import eqtrade.trading.core.ITradingApp;
////import eqtrade.trading.core.ITradingListener;
////import eqtrade.trading.core.TradingSetting;
////import eqtrade.trading.engine.TradingEngine;
////import eqtrade.trading.engine.TradingStore;
////import eqtrade.trading.model.Account;
////import eqtrade.trading.model.Portfolio;
////import eqtrade.trading.model.UserProfile;
////import eqtrade.trading.ui.UIOrder;
////import eqtrade.trading.ui.UIPortfolio;
////import eqtrade.trading.ui.UITrade;
////import eqtrade.trading.ui.core.UIChgPIN;
////import eqtrade.trading.ui.core.UIChgPassword;
////import eqtrade.trading.ui.core.UIPIN;
////import eqtrade.trading.ui.core.UIReconnect;
////import eqtrade.trading.ui.core.UIRefresh;
////
////public class TradingApplication extends VolluxApp implements IEQTradeApp,
////		ITradingListener, IMessage {
////	private final Log log = LogFactory.getLog(getClass());
////	public final static String CONS_TRADING = "trading";
////	private ITradingApp tradingApp;
////	private IFeedApp feedApp;
////	private TradingEventDispatcher event;
////	private TradingAction action;
////	private TradingUI ui;
////	FeedApplication feed;
////	private IApp apps;
////	public GetUserId GuserId;
////
////	// -------------------------------------------------------------------------------
////
////	@Override
////	public void start(String[] args) {
////		// log.info("starting trading application");
////		tradingApp = new TradingEngine();
////		tradingApp.start();
////		tradingApp.addListener(this);
////		event = new TradingEventDispatcher(this);
////		action = new TradingAction(this);
////		ui = new TradingUI(this);
////		Toolkit.getDefaultToolkit().addAWTEventListener(awtKeyEventListener,
////				AWTEvent.KEY_EVENT_MASK | AWTEvent.MOUSE_EVENT_MASK);
////	}
////
////	@Override
////	public VolluxEvent getEventDispatcher() {
////		return event;
////	}
////
////	@Override
////	public VolluxAction getAction() {
////		return action;
////	}
////
////	@Override
////	public VolluxUI getUI() {
////		return ui;
////	}
////
////	public void setFeedApp(IFeedApp feedApp) {
////		this.feedApp = feedApp;
////	}
////
////	@Override
////	public IFeedApp getFeedEngine() {
////		return feedApp;
////	}
////
////	@Override
////	public ITradingApp getTradingEngine() {
////		return tradingApp;
////	}
////
////	public void setFeed(FeedApplication feed) {
////		this.feed = feed;
////	}
////
////	public FeedApplication getFeed() {
////		return feed;
////	}
////
////	@Override
////	public void changePwdFailed(String msg) {
////		((UIChgPassword) ui.getForm(TradingUI.UI_CHGPASS)).failed(msg);
////	}
////
////	@Override
////	public void changePwdOK() {
////		((UIChgPassword) ui.getForm(TradingUI.UI_CHGPASS)).success();
////	}
////
////	@Override
////	public void changePINFailed(String msg) {
////		((UIChgPIN) ui.getForm(TradingUI.UI_CHGPIN)).failed(msg);
////	}
////
////	@Override
////	public void changePINOK() {
////		((UIChgPIN) ui.getForm(TradingUI.UI_CHGPIN)).success();
////	}
////
////	@Override
////	public void checkPINFailed(String msg) {
////		((UIPIN) ui.getForm(TradingUI.UI_PIN)).failed(msg);
////	}
////
////	@Override
////	public void checkPINOK() {
////		log.info("CHECK PIN OK");
////		UserProfile sales = (UserProfile) getTradingEngine().getStore(
////				TradingStore.DATA_USERPROFILE).getDataByField(
////				new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
////		log.info("pin profileid : " + sales.getProfileId().toUpperCase() + " "
////				+ sales.isAllowed());
////		if ((sales.getProfileId().toUpperCase().equals("SALES") || sales
////				.getProfileId().toUpperCase().equals("SUPERUSER"))
////				&& sales.isAllowed()) {
////
////			applySecurity();
////			getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
////			getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(false);
////			getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
////			getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(false);
////			getFeed().getAction().get(FeedAction.A_SHOWCASHDEPOSIT)
////					.setEnabled(false);
////			// getFeed().getAction().get(FeedAction.A_SHOWCASHWITH)
////			// .setEnabled(false);
////			action.get(TradingAction.A_SHOWCHGPIN).setEnabled(false);
////			action.get(TradingAction.A_SHOWCHGPIN).setGranted(false);
////
////			UIMaster.changeIcon(UIMaster.lblBuy,
////					new ImageIcon(UIMaster.class.getResource("buy.jpg")));
////			UIMaster.changeIcon(UIMaster.lblWithdraw, new ImageIcon(
////					UIMaster.class.getResource("withdraw.jpg")));
////			UIMaster.changeIcon(UIMaster.lblAmend,
////					new ImageIcon(UIMaster.class.getResource("amend.jpg")));
////			UIMaster.changeIcon(UIMaster.lblSell,
////					new ImageIcon(UIMaster.class.getResource("sell.jpg")));
////		} else {
////			((UIPIN) ui.getForm(TradingUI.UI_PIN)).success();
////			applySecurity();
////			// System.out.println("Second Stage");
////			((UIMaster) getUIMain()).checkIconLogin();
////			startPinTimeout(30);
////		}
////
////		log.info("done checkpinok : " + sales.getProfileId().toUpperCase() + " " + sales.isAllowed());
////	}
////
////	@Override
////	public void disconnected() {
////		((UIMaster) getUIMain()).setProperty("trading_disconnect", null);
////		action.setState(Action.C_DISCONNECTED_STATE);
////		ui.showUI(TradingUI.UI_RECONNECT);
////		getFeedEngine().getEngine().getParser().get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
////		if (feed.getFeedEngine().getUserId().equals("")) {
////			feed.getAction().get(FeedAction.A_SAVE).setEnabled(false);
////			feed.getAction().get(FeedAction.A_LOAD).setEnabled(false);
////		}
////
////	}
////
////	public void close() {
////		getUI().save();
////		getTradingEngine().logout();
////		action.setState(Action.C_NOTREADY_STATE);
////		disableTrading();
////		TradingSetting.save();
////		getUI().close();
////		((UIMaster) getUIMain()).setProperty("trading_logout", null);
////		getFeedEngine().getEngine().getParser().get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
////		if (feed.getFeedEngine().getUserId().equals("")) {
////			feed.getAction().get(FeedAction.A_SAVE).setEnabled(false);
////			feed.getAction().get(FeedAction.A_LOAD).setEnabled(false);
////		}
////		getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
////		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(false);
////		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
////		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(false);
////
////	}
////
////	@Override
////	public void killed(String msg) {
////		close();
////		getFeed().close();
////		Utils.showMessage("You have been sign out because you signed in on a different computer or device",null);
////	}
////
////	@Override
////	public void loginFailed(String msg) {
////		((eqtrade.feed.ui.core.UILogon) getFeed().getUI().getForm(
////				FeedUI.UI_LOGON)).tradingFailed(msg);
////		action.setState(Action.C_NOTREADY_STATE);
////	}
////
////	@Override
////	public void loginOK() {
////		disableTrading();
////		TradingSetting.load();
////		action.setState(Action.C_READY_STATE);
////		((UIMaster) getUIMain()).setProperty("trading_login", null);
////		getUIMain().show();
////		ui.load();
////		updatePFLastPrice();
////
////		if (feed.getFeedEngine().getUserId().equals("")) {
////			feed.getAction().get(FeedAction.A_SAVE).setEnabled(true);
////			feed.getAction().get(FeedAction.A_LOAD).setEnabled(true);
////		}
////		getFeed().logonSuccessfully();
////		UserProfile sales = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
////		
////		if (TradingSetting.getcAnnouncement()) {
////			ui.showUI(ui.UI_ANNOUCEMENT);
////		}
////		if (sales != null) {		
////
////			//if ((sales.getProfileId().toUpperCase().equals("SALES") || sales.getProfileId().toUpperCase().equals("SUPERUSER")) && sales.isAllowed()) {
////			if(sales.getProfileId().toUpperCase().equals("SUPERUSER"))	{
////				ui.cekLogin(sales.getUserId());
////				getTradingEngine().checkPINSLS(getTradingEngine().getUserId(),"LETMEPASS");
////			} else if (sales.getProfileId().toUpperCase().equals("SALES")) {
////				
////				action.get(TradingAction.A_SHOWBUYORDER).setGranted(true);
////				action.get(TradingAction.A_SHOWSELLORDER).setGranted(true);
////				action.get(TradingAction.A_SHOWAMENDORDER).setGranted(true);
////				action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(true);
////				action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setGranted(true);
////				action.get(TradingAction.A_SHOWADVERTISING).setGranted(false);	
////				
////				action.get(TradingAction.A_SHOWBUYORDER).setEnabled(true);
////				action.get(TradingAction.A_SHOWSELLORDER).setEnabled(true);	
////				action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(true);
////				action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(true);
////				action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setEnabled(true); 
////				action.get(TradingAction.A_SHOWADVERTISING).setEnabled(true);
////				
////			} else if (sales.getProfileId().toUpperCase().equals("CUSTOMER")) {
////				action.get(TradingAction.A_SHOWBUYORDER).setGranted(false);
////				action.get(TradingAction.A_SHOWSELLORDER).setGranted(false);
////				action.get(TradingAction.A_SHOWAMENDORDER).setGranted(false);
////				action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(false);
////
////				action.get(TradingAction.A_SHOWADVERTISING).setEnabled(false);
////				action.get(TradingAction.A_SHOWADVERTISING).setGranted(false);
////
////				
////				action.get(TradingAction.A_SHOWBUYORDER).setEnabled(true);
////				action.get(TradingAction.A_SHOWSELLORDER).setEnabled(true);
////				action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(true);
////				action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(true);
////				action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setEnabled(true);
////				TradingSetting.setPopup(true);
////
////				Account ac = (Account) getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByIndex(0);
////				if (ac != null) {
////					if (ac.getInvestorAccount().isEmpty()) {
////						Utils.showMessage("                              Anda belum mempunyai Investor Account.\n                                          Untuk informasi lebih lanjut,\n      silahkan hubungi kami di 021-3925550 atau email cs@sinarmassekuritas.co.id\nMohon mengabaikan pesan ini, jika Anda telah mengisi formulir pembukaan rekening.",null);
////					}
////				}
////			}
////		}
////
////	}
////
////	public void disableTrading() {
////		action.get(TradingAction.A_SHOWSPLITORDER).setGranted(false);
////		UserProfile simple = (UserProfile) getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "38" }, new int[] { UserProfile.C_MENUID });
////		action.get(TradingAction.A_SHOWPFSIMPLE).setGranted(
////				simple == null ? false : simple.isAllowed());
////		if (simple == null || !simple.isAllowed()) {
////			if (getUI().getForm(TradingUI.UI_PFSIMPLE).isReady())
////				getUI().getForm(TradingUI.UI_PF).close();
////		}
////		if (getUI().getForm(TradingUI.UI_ENTRYADVERTISING).isReady())
////			getUI().getForm(TradingUI.UI_ENTRYADVERTISING).close();
////		if (getUI().getForm(TradingUI.UI_ENTRYCROSSING).isReady())
////			getUI().getForm(TradingUI.UI_ENTRYCROSSING).close();
////		if (getUI().getForm(TradingUI.UI_ENTRYNEGDEAL).isReady())
////			getUI().getForm(TradingUI.UI_ENTRYNEGDEAL).close();
////		if (getUI().getForm(TradingUI.UI_ENTRYORDER).isReady())
////			getUI().getForm(TradingUI.UI_ENTRYORDER).close();
////		if (getUI().getForm(TradingUI.UI_NEGDEALLIST).isReady())
////			getUI().getForm(TradingUI.UI_NEGDEALLIST).close();
////		if (getUI().getForm(TradingUI.UI_SL).isReady())
////			getUI().getForm(TradingUI.UI_SL).close();
////		if (getUI().getForm(TradingUI.UI_NEWSCHEDULE).isReady())
////			getUI().getForm(TradingUI.UI_NEWSCHEDULE).close();
////		if (getUI().getForm(TradingUI.UI_BYORDERID).isReady())
////			getUI().getForm(TradingUI.UI_BYORDERID).close();
////		if (getUI().getForm(TradingUI.UI_BYSCHEDULLABEL).isReady())
////			getUI().getForm(TradingUI.UI_BYSCHEDULLABEL).close();
////		if (getUI().getForm(TradingUI.UI_BYSTOCKID).isReady())
////			getUI().getForm(TradingUI.UI_BYSTOCKID).close();
////		
////		action.get(TradingAction.A_SHOWNEGDEAL).setGranted(false);
////		action.get(TradingAction.A_SHOWBUYADVERTISING).setGranted(false);
////		action.get(TradingAction.A_SHOWSELLADVERTISING).setGranted(false);
////		action.get(TradingAction.A_SHOWBUYNEGDEAL).setGranted(false);
////		action.get(TradingAction.A_SHOWSELLNEGDEAL).setGranted(false);
////		action.get(TradingAction.A_SHOWCROSSING).setGranted(false);
////		action.get(TradingAction.A_SHOWBUYORDER).setGranted(false);
////		action.get(TradingAction.A_SHOWSELLORDER).setGranted(false);
////		action.get(TradingAction.A_SHOWAMENDORDER).setGranted(false);
////		action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(false);
////		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setGranted(false);// new
////																			// schedule
////
////		action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(true);
////		action.get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(true);
////		action.get(TradingAction.A_SHOWSELLADVERTISING).setEnabled(true);
////		action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(true);
////		action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(true);
////		action.get(TradingAction.A_SHOWCROSSING).setEnabled(true);
////		action.get(TradingAction.A_SHOWBUYORDER).setEnabled(true);
////		action.get(TradingAction.A_SHOWSELLORDER).setEnabled(true);
////		action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(true);
////		action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(true);
////		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setEnabled(true);// new
////																			// schedule
////
////		getFeed().getAction().get(FeedAction.A_PINON).setGranted(true);
////		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(false);
////		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(true);
////		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(false);
////
////		action.get(TradingAction.A_SHOWCHGPIN).setEnabled(false);
////		action.get(TradingAction.A_SHOWCHGPIN).setGranted(false);
////
////		UIMaster.changeIcon(UIMaster.lblBuy,new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));
////		UIMaster.changeIcon(UIMaster.lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw_hover.jpg")));
////		UIMaster.changeIcon(UIMaster.lblAmend,new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
////		UIMaster.changeIcon(UIMaster.lblSell,new ImageIcon(UIMaster.class.getResource("sell_hover.jpg")));
////	}
////
////	public void applySecurity() {
////		getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
////		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(true);
////		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
////		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(true);
////		action.get(TradingAction.A_SHOWCHGPIN).setEnabled(true);
////		action.get(TradingAction.A_SHOWCHGPIN).setGranted(true);
////
////		// for negdeal, split, and simple pf
////		UserProfile split = (UserProfile) getTradingEngine().getStore(
////				TradingStore.DATA_USERPROFILE).getDataByField(
////				new Object[] { "36" }, new int[] { UserProfile.C_MENUID });
////		action.get(TradingAction.A_SHOWSPLITORDER).setGranted(
////				split == null ? false : split.isAllowed());
////		action.get(TradingAction.A_SHOWSPLITORDER).setEnabled(
////				split == null ? false : split.isAllowed());
////		UserProfile simple = (UserProfile) getTradingEngine().getStore(
////				TradingStore.DATA_USERPROFILE).getDataByField(
////				new Object[] { "38" }, new int[] { UserProfile.C_MENUID });
////		/*
////		 * System.out.println("order menunya adalah: " + (simple == null ? false
////		 * : simple.isAllowed()));
////		 */
////		action.get(TradingAction.A_SHOWPFSIMPLE).setGranted(
////				simple == null ? false : simple.isAllowed());
////		action.get(TradingAction.A_SHOWPFSIMPLE).setEnabled(
////				simple == null ? false : simple.isAllowed());
////		UserProfile nego = (UserProfile) getTradingEngine().getStore(
////				TradingStore.DATA_USERPROFILE).getDataByField(
////				new Object[] { "39" }, new int[] { UserProfile.C_MENUID });
////		action.get(TradingAction.A_SHOWNEGDEAL).setGranted(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWBUYADVERTISING).setGranted(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWSELLADVERTISING).setGranted(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWBUYNEGDEAL).setGranted(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWSELLNEGDEAL).setGranted(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWCROSSING).setGranted(
////				nego == null ? false : nego.isAllowed());
////
////		// action.get(TradingAction.A_SHOWADVERTISING).setEnabled(
////		// nego == null ? false : nego.isAllowed());
////		// if(nego != null && !nego.isAllowed())
////		action.get(TradingAction.A_SHOWADVERTISING).setEnabled(
////				nego == null ? false : nego.isAllowed());
////
////		action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWSELLADVERTISING).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWCROSSING).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		UserProfile trading = (UserProfile) getTradingEngine().getStore(
////				TradingStore.DATA_USERPROFILE).getDataByField(
////				new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
////		/*
////		 * System.out.println("order menunya adalah: " + (trading == null ?
////		 * false : simple.isAllowed()));
////		 */
////
////		log.info("showbuyorder_is_granted_"
////				+ (trading == null ? false : trading.isAllowed()));
////
////		action.get(TradingAction.A_SHOWBUYORDER).setGranted(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWSELLORDER).setGranted(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWAMENDORDER).setGranted(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setGranted(
////				trading == null ? false : trading.isAllowed());
////
////		action.get(TradingAction.A_SHOWBUYORDER).setEnabled(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWSELLORDER).setEnabled(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWAUTOTRADINGSYSTEM).setEnabled(
////				trading == null ? false : trading.isAllowed());
////	}
////
////	public void applySecurityDummy() {
////		getFeed().getAction().get(FeedAction.A_PINON).setGranted(false);
////		getFeed().getAction().get(FeedAction.A_PINOFF).setGranted(true);
////		getFeed().getAction().get(FeedAction.A_PINON).setEnabled(false);
////		getFeed().getAction().get(FeedAction.A_PINOFF).setEnabled(true);
////		action.get(TradingAction.A_SHOWCHGPIN).setEnabled(true);
////		action.get(TradingAction.A_SHOWCHGPIN).setGranted(true);
////
////		// for negdeal, split, and simple pf
////		// UserProfile split =
////		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
////		// Object[]{"36"}, new int[]{UserProfile.C_MENUID});
////		UserProfile split = new UserProfile();
////		split.setIsAllowed("1");
////		action.get(TradingAction.A_SHOWSPLITORDER).setGranted(
////				split == null ? false : split.isAllowed());
////		action.get(TradingAction.A_SHOWSPLITORDER).setEnabled(
////				split == null ? false : split.isAllowed());
////		// UserProfile simple =
////		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
////		// Object[]{"38"}, new int[]{UserProfile.C_MENUID});
////		UserProfile simple = new UserProfile();
////		simple.setIsAllowed("1");
////		/*
////		 * System.out.println("order menunya adalah: " + (simple == null ? false
////		 * : simple.isAllowed()));
////		 */
////		action.get(TradingAction.A_SHOWPFSIMPLE).setGranted(
////				simple == null ? false : simple.isAllowed());
////		action.get(TradingAction.A_SHOWPFSIMPLE).setEnabled(
////				simple == null ? false : simple.isAllowed());
////		// UserProfile nego =
////		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
////		// Object[]{"39"}, new int[]{UserProfile.C_MENUID});
////		UserProfile nego = new UserProfile();
////		nego.setIsAllowed("1");
////		action.get(TradingAction.A_SHOWNEGDEAL).setGranted(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWBUYADVERTISING).setGranted(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWSELLADVERTISING).setGranted(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWBUYNEGDEAL).setGranted(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWSELLNEGDEAL).setGranted(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWCROSSING).setGranted(
////				nego == null ? false : nego.isAllowed());
////
////		action.get(TradingAction.A_SHOWNEGDEAL).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWBUYADVERTISING).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWSELLADVERTISING).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWBUYNEGDEAL).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWSELLNEGDEAL).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		action.get(TradingAction.A_SHOWCROSSING).setEnabled(
////				nego == null ? false : nego.isAllowed());
////		// UserProfile trading =
////		// (UserProfile)getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new
////		// Object[]{"40"}, new int[]{UserProfile.C_MENUID});
////		UserProfile trading = new UserProfile();
////		trading.setIsAllowed("1");
////		action.get(TradingAction.A_SHOWBUYORDER).setGranted(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWSELLORDER).setGranted(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWAMENDORDER).setGranted(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWWITHDRAWORDER).setGranted(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWBUYORDER).setEnabled(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWSELLORDER).setEnabled(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWAMENDORDER).setEnabled(
////				trading == null ? false : trading.isAllowed());
////		action.get(TradingAction.A_SHOWWITHDRAWORDER).setEnabled(
////				trading == null ? false : trading.isAllowed());
////	}
////
////	@Override
////	public void loginStatus(String msg) {
////		((eqtrade.feed.ui.core.UILogon) getFeed().getUI().getForm(
////				FeedUI.UI_LOGON)).setStatus(msg);
////	}
////
////	@Override
////	public void reconnectFailed(String msg) {
////		if (ui.getForm(TradingUI.UI_RECONNECT).isReady())
////			((UIReconnect) ui.getForm(TradingUI.UI_RECONNECT)).failed(msg);
////	}
////
////	@Override
////	public void reconnectOK() {
////		action.setState(Action.C_READY_STATE);
////		((UIReconnect) ui.getForm(TradingUI.UI_RECONNECT)).success();
////		((UIMaster) getUIMain()).setProperty("trading_login", null);
////		getFeedEngine().getEngine().getParser()
////				.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
////		if (feed.getFeedEngine().getUserId().equals("")) {
////			feed.getAction().get(FeedAction.A_SAVE).setEnabled(true);
////			feed.getAction().get(FeedAction.A_LOAD).setEnabled(true);
////		}
////	}
////
////	@Override
////	public void reconnectStatus(String msg) {
////		((UIReconnect) ui.getForm(TradingUI.UI_RECONNECT)).setStatus(msg);
////	}
////
////	@Override
////	public void refreshFailed(String msg) {
////		((UIRefresh) ui.getForm(TradingUI.UI_REFRESH)).failed(msg);
////		((UIOrder) ui.getForm(TradingUI.UI_ORDER)).failed(msg);
////		((UITrade) ui.getForm(TradingUI.UI_TRADE)).failed(msg);
////	}
////
////	@Override
////	public void refreshOK() {
////		((UIRefresh) ui.getForm(TradingUI.UI_REFRESH)).success();
////		((UITrade) ui.getForm(TradingUI.UI_TRADE)).success();
////		((UIOrder) ui.getForm(TradingUI.UI_ORDER)).success();
////	}
////
////	@Override
////	public void refreshOKportfolio() {
////		((UIPortfolio) ui.getForm(TradingUI.UI_PF)).successRefresh();
////	}
////
////	@Override
////	public void refreshStatus(String msg) {
////		((UIRefresh) ui.getForm(TradingUI.UI_REFRESH)).setStatus(msg);
////	}
////
////	@Override
////	public void newMessage(Model arg0) {
////		/*
////		 * StockSummary ss = (StockSummary) arg0; if
////		 * (ss.getBoard().equals("RG")) { int i =
////		 * getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO)
////		 * .getRowCount(); for (int x = 0; x < i; x++) { Portfolio p =
////		 * (Portfolio) getTradingEngine().getStore(
////		 * TradingStore.DATA_PORTFOLIO).getDataByIndex(x); if (p != null) { //if
////		 * (p.getStock().equals(ss.getCode())) { //} if
////		 * (p.getStock().equals(ss.getCode())) {
////		 * p.setLastPrice(ss.getClosing()); } } }
////		 * getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).refresh(); }
////		 */
////	}
////
////	private void updatePFLastPrice() {
////		int i = getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO)
////				.getRowCount();
////		for (int x = 0; x < i; x++) {
////			Portfolio p = (Portfolio) getTradingEngine().getStore(
////					TradingStore.DATA_PORTFOLIO).getDataByIndex(x);
////			if (p != null) {
////				StockSummary ss = (StockSummary) getFeedEngine().getStore(
////						FeedStore.DATA_STOCKSUMMARY).getDataByKey(
////						new Object[] { p.getStock(), "RG" });
////				if (ss != null) {
////					p.setLastPrice(ss.getClosing());
////				}
////			}
////		}
////		getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).refresh();
////	}
////
////	// --------------- PIN TIMEOUT --------------------
////
////	javax.swing.Timer tmrPinTimeout;
////	boolean pinTimeoutActivated;
////
////	public void startPinTimeout(int minutes) {
////		tmrPinTimeout = new javax.swing.Timer(minutes * 60000,
////				new ActionListener() {
////					public void actionPerformed(ActionEvent e) {
////						// logout PIN
////						stopPinTimeout();
////						disableTrading();
////						getUI().showUI(TradingUI.UI_PIN);
////
////					}
////				});
////		tmrPinTimeout.start();
////		pinTimeoutActivated = tmrPinTimeout.isRunning();
////	}
////
////	public void stopPinTimeout() {
////		tmrPinTimeout.stop();
////		pinTimeoutActivated = tmrPinTimeout.isRunning();
////	}
////
////	public void restartPinTimeout() {
////		tmrPinTimeout.restart();
////	}
////
////	public boolean timeoutActivated() {
////		return pinTimeoutActivated;
////	}
////
////	AWTEventListener awtKeyEventListener = new AWTEventListener() {
////		public void eventDispatched(AWTEvent event) {
////			// reset timer
////			if (timeoutActivated()) {
////				if (event.getID() == MouseEvent.MOUSE_PRESSED)
////					restartPinTimeout();
////				if (event.getID() == KeyEvent.KEY_PRESSED)
////					restartPinTimeout();
////			}
////		}
////	};
////
////	@Override
////	public void refreshOKmargin() {
////		((UIMarginMonitoring) ui.getForm(TradingUI.UI_MM)).successRefresh();
////	}
////
////}
