package eqtrade.trading.app;

import java.awt.event.ActionEvent;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.KeyStroke;

import org.apache.commons.logging.LogFactory;
import org.jfree.util.Log;


import com.vollux.framework.Action;
import com.vollux.framework.IApp;
import com.vollux.framework.MenuProperty;
import com.vollux.framework.VolluxAction;
import com.vollux.framework.VolluxApp;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedApplication;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.Utils;

public class TradingAction extends VolluxAction {
	private final org.apache.commons.logging.Log log = LogFactory
			.getLog(getClass());
	public final static Integer A_SHOWORDER = new Integer(0);
	public final static Integer A_SHOWTRADE = new Integer(1);
	public final static Integer A_SHOWACCOUNT = new Integer(2);
	public final static Integer A_SHOWPF = new Integer(3);
	public final static Integer A_SHOWBUYORDER = new Integer(4);
	public final static Integer A_SHOWSELLORDER = new Integer(5);
	public final static Integer A_SHOWAMENDORDER = new Integer(6);
	public final static Integer A_SHOWWITHDRAWORDER = new Integer(7);
	public final static Integer A_SHOWDELETEORDER = new Integer(8);
	public final static Integer A_SHOWMARGINCALL = new Integer(9);

	public final static Integer A_SHOWLOGIN = new Integer(25);
	public final static Integer A_SHOWLOGOUT = new Integer(26);
	public final static Integer A_SHOWRECONNECT = new Integer(27);
	public final static Integer A_SHOWCHGPIN = new Integer(28);
	public final static Integer A_SHOWCONN = new Integer(29);
	public final static Integer A_SHOWCOLOUR = new Integer(30);
	public final static Integer A_SHOWREFRESH = new Integer(31);

	public final static Integer A_SHOWBUYADVERTISING = new Integer(32);
	public final static Integer A_SHOWSELLADVERTISING = new Integer(33);
	public final static Integer A_SHOWBUYNEGDEAL = new Integer(34);
	public final static Integer A_SHOWSELLNEGDEAL = new Integer(35);

	public final static Integer A_SHOWSPLITORDER = new Integer(36);
	public final static Integer A_SHOWCROSSING = new Integer(37);
	public final static Integer A_SHOWPFSIMPLE = new Integer(38);
	public final static Integer A_SHOWNEGDEAL = new Integer(39);

	public final static Integer A_SHOWFOREX = new Integer(40);

	public final static Integer A_SHOWADVERTISING = new Integer(41);
	public final static Integer A_SHOWMARGINMONITORING = new Integer(42);
	public final static Integer A_SHOWTESTINGORDER = new Integer(43);
	public final static Integer A_SHOWTESTINGORDERSELL = new Integer(44);
	public final static Integer A_SHOWAUTOTRADINGSYSTEM = new Integer(45);
	public final static Integer A_SHOWANNOUNCEMENT = new Integer(46);
	public final static Integer A_SHOWORDERMATRIC = new Integer(47);
	public final static Integer A_SHOWGTC = new Integer(48);
	
	public TradingAction(IApp apps) {
		super(apps);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buildAction() {		
		Calendar cal = Calendar.getInstance();
		Calendar currentcal = Calendar.getInstance();
		
		cal.set(2012, Calendar.JUNE, 24);
		currentcal.set(currentcal.get(Calendar.YEAR),
		currentcal.get(Calendar.MONTH), currentcal.get(Calendar.DAY_OF_MONTH));

		map.put(A_SHOWCHGPIN, new Action(A_SHOWCHGPIN, "Change PIN",
				new MenuProperty(0, "Session", 2, false, false),
				TradingApplication.CONS_TRADING) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				apps.getUI().showUI(TradingUI.UI_CHGPIN);
			}

		});
		map.put(A_SHOWCONN, new Action(A_SHOWCONN, "Change Trading Connection",
				new MenuProperty(0, "Session", 3, false, false),
				VolluxApp.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				apps.getUI().showUI(TradingUI.UI_CONNECTION);
			}

			@Override
			public void setState(int newState) {
				if (newState == C_LOCKED_STATE) {
					this.setEnabled(false);
				} else if (newState == C_DISCONNECTED_STATE) {
					this.setEnabled(false);
				} else {
					this.setEnabled(newState == C_READY_STATE ? false : true);
				}
			}
		});
		map.put(A_SHOWCOLOUR, new Action(A_SHOWCOLOUR,
				"Change Colours & Font Trading", new MenuProperty(5, "Tools",
						1, false, false), VolluxApp.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(TradingUI.UI_COLOUR);
				} else
					showLogin();
			}

		});

		map.put(A_SHOWREFRESH, new Action(A_SHOWREFRESH, "Refresh Trading",
				new MenuProperty(0, "Session", 5, false, false),
				TradingApplication.CONS_TRADING) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				apps.getUI().showUI(TradingUI.UI_REFRESH);
			}
		});
		map.put(A_SHOWRECONNECT, new Action(A_SHOWRECONNECT,
				"Reconnect Trading", new MenuProperty(0, "Session", 6, false,
						false), TradingApplication.CONS_TRADING) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				apps.getUI().showUI(TradingUI.UI_RECONNECT);
			}

			@Override
			public void setState(int newState) {
				if (newState == C_LOCKED_STATE) {
					this.setEnabled(false);
				} else if (newState == C_DISCONNECTED_STATE) {
					this.setEnabled(true);
				} else {
					this.setEnabled(newState == C_READY_STATE ? false : false);
				}
			}
		});
		//Mulai masuk menu pembatasan
		if(cal.after(currentcal)) {
			map.put(A_SHOWGTC,
					new Action(A_SHOWGTC, "GTC_List", null, new MenuProperty(1,
							"*Transaction", 14, false, false),
							TradingApplication.CONS_TRADING) {
						private static final long serialVersionUID = 1L;
	
						@Override
						public void actionPerformed(ActionEvent event) {
							if (isReadyState()) {
								apps.getUI().showUI(TradingUI.UI_GTC);
							} else
								showLogin();
						}
					});
			
			map.put(A_SHOWBUYORDER, new Action(A_SHOWBUYORDER, "Buy Order",
					KeyStroke.getKeyStroke("F2"), new MenuProperty(1,
							"*Transaction", 7, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					
					if (isReadyState()) {
						HashMap param = new HashMap();
						param.put("TYPE", "BUY");
	
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
	
							apps.getUI().showUI(TradingUI.UI_ENTRYORDER, param);
						}
					} else {
						showLogin();
					}
				}
	
			});
			map.put(A_SHOWSELLORDER, new Action(A_SHOWSELLORDER, "Sell Order",
					KeyStroke.getKeyStroke("F4"), new MenuProperty(1,
							"*Transaction", 8, false, true),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
	
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "SELL");
							apps.getUI().showUI(TradingUI.UI_ENTRYORDER , param);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWBUYADVERTISING, new Action(A_SHOWBUYADVERTISING,
					"Buy Advertising", KeyStroke.getKeyStroke("ctrl F2"),
					new MenuProperty(1, "*Transaction", 9, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "BUY");
							apps.getUI().showUI(TradingUI.UI_ENTRYADVERTISING,
									param);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWSELLADVERTISING, new Action(A_SHOWSELLADVERTISING,
					"Sell Advertising", KeyStroke.getKeyStroke("ctrl F4"),
					new MenuProperty(1, "*Transaction", 10, false, true),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "SELL");
							apps.getUI().showUI(TradingUI.UI_ENTRYADVERTISING,
									param);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWSELLNEGDEAL, new Action(A_SHOWSELLNEGDEAL,
					"Entry Neg Deal", KeyStroke.getKeyStroke("shift F4"),
					new MenuProperty(1, "*Transaction", 11, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
	
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "SELL");
							apps.getUI().showUI(TradingUI.UI_ENTRYNEGDEAL, param);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWBUYNEGDEAL, new Action(A_SHOWBUYNEGDEAL,
					"Confirm Neg Deal", KeyStroke.getKeyStroke("shift F2"),
					new MenuProperty(1, "*Transaction", 12, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "BUY");
							apps.getUI().showUI(TradingUI.UI_ENTRYNEGDEAL, param);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWCROSSING, new Action(A_SHOWCROSSING, "Crossing Neg Deal",
					KeyStroke.getKeyStroke("shift F5"), new MenuProperty(1,
							"*Transaction", 13, false, true),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "SELL");
							apps.getUI().showUI(TradingUI.UI_ENTRYCROSSING, param);
						}
					} else
						showLogin();
				}
			});
		} else {
			map.put(A_SHOWGTC,
					new Action(A_SHOWGTC, "GTC List", null, new MenuProperty(1,
							"Transaction", 14, false, false),
							TradingApplication.CONS_TRADING) {
						private static final long serialVersionUID = 1L;
	
						@Override
						public void actionPerformed(ActionEvent event) {
							if (isReadyState()) {
								
								apps.getUI().showUI(TradingUI.UI_GTC);
							} else
								showLogin();
						}
					});
			
			map.put(A_SHOWBUYORDER, new Action(A_SHOWBUYORDER, "Buy Order",
					KeyStroke.getKeyStroke("F2"), new MenuProperty(1,
							"Transaction", 7, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					
					if (isReadyState()) {
						HashMap param = new HashMap();
						param.put("TYPE", "BUY");
	
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
	
							apps.getUI().showUI(TradingUI.UI_ENTRYORDER, param);
						}
					} else {
						showLogin();
					}
				}
	
			});
			map.put(A_SHOWSELLORDER, new Action(A_SHOWSELLORDER, "Sell Order",
					KeyStroke.getKeyStroke("F4"), new MenuProperty(1,
							"Transaction", 8, false, true),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
	
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "SELL");
							apps.getUI().showUI(TradingUI.UI_ENTRYORDER , param);
						}
					} else
						showLogin();
				}
			});
			
			map.put(A_SHOWBUYADVERTISING, new Action(A_SHOWBUYADVERTISING,
					"Buy Advertising", KeyStroke.getKeyStroke("ctrl F2"),
					new MenuProperty(1, "Transaction", 9, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "BUY");
							apps.getUI().showUI(TradingUI.UI_ENTRYADVERTISING,
									param);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWSELLADVERTISING, new Action(A_SHOWSELLADVERTISING,
					"Sell Advertising", KeyStroke.getKeyStroke("ctrl F4"),
					new MenuProperty(1, "Transaction", 10, false, true),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "SELL");
							apps.getUI().showUI(TradingUI.UI_ENTRYADVERTISING,
									param);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWSELLNEGDEAL, new Action(A_SHOWSELLNEGDEAL,
					"Entry Neg Deal", KeyStroke.getKeyStroke("shift F4"),
					new MenuProperty(1, "Transaction", 11, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
	
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "SELL");
							apps.getUI().showUI(TradingUI.UI_ENTRYNEGDEAL, param);
						}
					} else
						showLogin();
				}
			});
		
		
		
			map.put(A_SHOWBUYNEGDEAL, new Action(A_SHOWBUYNEGDEAL,
					"Confirm Neg Deal", KeyStroke.getKeyStroke("shift F2"),
					new MenuProperty(1, "Transaction", 12, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "BUY");
							apps.getUI().showUI(TradingUI.UI_ENTRYNEGDEAL, param);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWCROSSING, new Action(A_SHOWCROSSING, "Crossing Neg Deal",
					KeyStroke.getKeyStroke("shift F5"), new MenuProperty(1,
							"Transaction", 13, false, true),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							HashMap param = new HashMap();
							param.put("TYPE", "SELL");
							apps.getUI().showUI(TradingUI.UI_ENTRYCROSSING, param);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWAUTOTRADINGSYSTEM, new Action(A_SHOWAUTOTRADINGSYSTEM, "Auto Trading System",  new MenuProperty(1, "Transaction", 8, false,true),
							 TradingApplication.CONS_TRADING) {
							 private static final long serialVersionUID = 1L;

							 @Override
							 public void actionPerformed(ActionEvent e) {
							 if(isReadyState()){
								 if(!isGranted()){
									 ((TradingApplication) apps).getUI().showUI(TradingUI.UI_PIN);
								 }else{
									 apps.getUI().showUI(TradingUI.UI_SL);
									}
							 }else{
							 showLogin();
							 }

					}
			});
			map.put(A_SHOWORDERMATRIC, new Action(A_SHOWORDERMATRIC,"Order Matrix",
				new MenuProperty(1, 
						"Transaction", 8, false, true),
						TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
			
				@Override
				 public void actionPerformed(ActionEvent e) {
					 if(isReadyState()) {
						 if(!isGranted()) {
							 ((TradingApplication) apps).getUI().showUI(TradingUI.UI_PIN);
						 } else {
							 apps.getUI().showUI(TradingUI.UI_ORDERMATRIX);
						 }
					 } else {
						 showLogin();
					 }
				}
			});			
		}
		//End pembatasan		
		
		map.put(A_SHOWAMENDORDER, new Action(A_SHOWAMENDORDER, "Amend Order",
				TradingApplication.CONS_TRADING) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {

				HashMap param = new HashMap();
				param.put("ORDER", event.getSource());
				param.put("OrderPanel", event.getActionCommand());
				apps.getUI().showUI(TradingUI.UI_AMENDORDER, param);
			}
		});
		map.put(A_SHOWWITHDRAWORDER, new Action(A_SHOWWITHDRAWORDER,
				"Withdraw Order", TradingApplication.CONS_TRADING) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
			}
		});
		map.put(A_SHOWSPLITORDER, new Action(A_SHOWSPLITORDER, "Split Order",
				TradingApplication.CONS_TRADING) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
				HashMap param = new HashMap();
				param.put("ORDER", event.getSource());
				apps.getUI().showUI(TradingUI.UI_SPLITORDER, param);
			}
		});
		map.put(A_SHOWDELETEORDER, new Action(A_SHOWDELETEORDER,
				"Delete Order", TradingApplication.CONS_TRADING) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent event) {
			}
		});
		
		//Mulai pembatasan menu
		if(cal.after(currentcal)) {
			map.put(A_SHOWORDER, new Action(A_SHOWORDER, "Order List", KeyStroke
							.getKeyStroke("ctrl F6"), new MenuProperty(1,
							"*Transaction", 14, false, false),
							TradingApplication.CONS_TRADING) {
						private static final long serialVersionUID = 1L;
	
						@Override
						public void actionPerformed(ActionEvent event) {
							if (isReadyState()) {
								apps.getUI().showUI(TradingUI.UI_ORDER);
							} else
								showLogin();
						}
					});
			map.put(A_SHOWTRADE, new Action(A_SHOWTRADE, "Trade List", KeyStroke
							.getKeyStroke("ctrl F7"), new MenuProperty(1,
							"*Transaction", 15, false, false),
							TradingApplication.CONS_TRADING) {
						private static final long serialVersionUID = 1L;
	
						@Override
						public void actionPerformed(ActionEvent event) {
							if (isReadyState()) {
								apps.getUI().showUI(TradingUI.UI_TRADE);
							} else
								showLogin();
						}
					});
			map.put(A_SHOWACCOUNT, new Action(A_SHOWACCOUNT, "Account List",
					KeyStroke.getKeyStroke("ctrl F8"), new MenuProperty(1,
							"*Transaction", 16, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(TradingUI.UI_ACCOUNT);
					} else {
						showLogin();
					}
				}
			});
			map.put(A_SHOWPF, new Action(A_SHOWPF, "*Portfolio", KeyStroke
								.getKeyStroke("ctrl F11"), new MenuProperty(1,
								"*Transaction", 17, false, false),
								TradingApplication.CONS_TRADING) {
							private static final long serialVersionUID = 1L;
			
							@Override
							public void actionPerformed(ActionEvent event) {
								if (isReadyState()) {
									apps.getUI().showUI(TradingUI.UI_PF);
								} else
									showLogin();
							}
						});
			map.put(A_SHOWPFSIMPLE, new Action(A_SHOWPFSIMPLE, "Simple Portfolio",
					KeyStroke.getKeyStroke("ctrl P"), new MenuProperty(1,
							"*Transaction", 18, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(TradingUI.UI_PFSIMPLE);
					} else
						showLogin();
	
				}
			});
			//Yang Dibuat Sendiri
			///////////////////////////////////////////////
			map.put(A_SHOWMARGINMONITORING, new Action(A_SHOWMARGINMONITORING, 
					"Margin Monitoring", new MenuProperty(1,"*Transaction", 19, 
							false, false),TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;			
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					//log.info("margin monitoring"+isReadyState()+" "+isGranted());
					if(isReadyState()){
						apps.getUI().showUI(TradingUI.UI_MM);
					}else 
						showLogin();
				}
			});
			///////////////////////////////////////////////
			map.put(A_SHOWNEGDEAL, new Action(A_SHOWNEGDEAL, "Neg Deal List",
					new MenuProperty(1, "*Transaction", 20, true, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							apps.getUI().showUI(TradingUI.UI_NEGDEALLIST);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWADVERTISING, new Action(A_SHOWADVERTISING,
					"Advertising List", new MenuProperty(1, "*Transaction", 21,
							false, false), TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					//log.info("advertising list "+isReadyState()+" "+isGranted());
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							apps.getUI().showUI(TradingUI.UI_ADVERTISING);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWAUTOTRADINGSYSTEM,  new Action(A_SHOWAUTOTRADINGSYSTEM, "Auto Trading System", 
							 new MenuProperty(1,
							 "*Transaction", 8, false,true),
							 TradingApplication.CONS_TRADING) {
							 private static final long serialVersionUID = 1L;

							 @Override
							 public void actionPerformed(ActionEvent e) {
							 if(isReadyState()){
								 if(!isGranted()){
									 ((TradingApplication) apps).getUI().showUI(
												TradingUI.UI_PIN);
								 }else{
									 apps.getUI().showUI(TradingUI.UI_SL);
									}
							 }else{
							 showLogin();
							 }

					}
			});
			map.put(A_SHOWORDERMATRIC, new Action(A_SHOWORDERMATRIC, "Order Matrix", 
							 new MenuProperty(1,
							 "*Transaction", 8, false,true),
							 TradingApplication.CONS_TRADING) {
							 private static final long serialVersionUID = 1L;

							 @Override
							 public void actionPerformed(ActionEvent e) {
							 if(isReadyState()){
								 if(!isGranted()){
									 ((TradingApplication) apps).getUI().showUI(
												TradingUI.UI_PIN);
								 }else{
									 apps.getUI().showUI(TradingUI.UI_ORDERMATRIX);
									}
							 }else{
							 showLogin();
							 }

					}
			});
			
			
		} else {
			map.put(A_SHOWORDER, new Action(A_SHOWORDER, "Order List", KeyStroke
							.getKeyStroke("ctrl F6"), new MenuProperty(1,
							"Transaction", 14, false, false),
							TradingApplication.CONS_TRADING) {
						private static final long serialVersionUID = 1L;
	
						@Override
						public void actionPerformed(ActionEvent event) {
							if (isReadyState()) {
							//	System.out.println("test.2.");
								apps.getUI().showUI(TradingUI.UI_ORDER);
							} else
								showLogin();
						}
					});
			map.put(A_SHOWTRADE, new Action(A_SHOWTRADE, "Trade List", KeyStroke
							.getKeyStroke("ctrl F7"), new MenuProperty(1,
							"Transaction", 15, false, false),
							TradingApplication.CONS_TRADING) {
						private static final long serialVersionUID = 1L;
	
						@Override
						public void actionPerformed(ActionEvent event) {
							if (isReadyState()) {
								apps.getUI().showUI(TradingUI.UI_TRADE);
							} else
								showLogin();
						}
					});
			map.put(A_SHOWACCOUNT, new Action(A_SHOWACCOUNT, "Account List",
					KeyStroke.getKeyStroke("ctrl F8"), new MenuProperty(1,
							"Transaction", 16, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(TradingUI.UI_ACCOUNT);
					} else {
						showLogin();
					}
				}
			});
			map.put(A_SHOWPF,new Action(A_SHOWPF, "Portfolio", KeyStroke
								.getKeyStroke("ctrl F11"), new MenuProperty(1,
								"Transaction", 17, false, false),
								TradingApplication.CONS_TRADING) {
							private static final long serialVersionUID = 1L;
			
							@Override
							public void actionPerformed(ActionEvent event) {
								if (isReadyState()) {
									apps.getUI().showUI(TradingUI.UI_PF);
								} else
									showLogin();
								// Utils.showMessage("Under Development", null);
							}
						});
			//else
					
			map.put(A_SHOWPFSIMPLE, new Action(A_SHOWPFSIMPLE, "Simple Portfolio",
					KeyStroke.getKeyStroke("ctrl P"), new MenuProperty(1,
							"Transaction", 18, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				@Override
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(TradingUI.UI_PFSIMPLE);
					} else
						showLogin();
	
				}
			});
			//Yang Dibuat Sendiri
			map.put(A_SHOWMARGINMONITORING, new Action(A_SHOWMARGINMONITORING, 
					"Margin Monitoring", new MenuProperty(1,"Transaction", 19, 
							false, false),TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;			
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					//log.info("margin monitoring"+isReadyState()+" "+isGranted());
					if(isReadyState()){
						apps.getUI().showUI(TradingUI.UI_MM);
					}else 
						showLogin();
				}
			});
			map.put(A_SHOWNEGDEAL, new Action(A_SHOWNEGDEAL, "Neg Deal List",
					new MenuProperty(1, "Transaction", 20, true, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							apps.getUI().showUI(TradingUI.UI_NEGDEALLIST);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWADVERTISING, new Action(A_SHOWADVERTISING,
					"Advertising List", new MenuProperty(1, "Transaction", 21,
							false, false), TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					//log.info("advertising list "+isReadyState()+" "+isGranted());
					if (isReadyState()) {
						if (!isGranted()) {
							((TradingApplication) apps).getUI().showUI(
									TradingUI.UI_PIN);
						} else {
							apps.getUI().showUI(TradingUI.UI_ADVERTISING);
						}
					} else
						showLogin();
				}
			});
			map.put(A_SHOWAUTOTRADINGSYSTEM, new Action(A_SHOWAUTOTRADINGSYSTEM, "Auto Trading System", 
							 new MenuProperty(1,
							 "Transaction", 8, false,true),
							 TradingApplication.CONS_TRADING) {
							 private static final long serialVersionUID = 1L;

							 @Override
							 public void actionPerformed(ActionEvent e) {
							 if(isReadyState()){
								 if(!isGranted()){
									 ((TradingApplication) apps).getUI().showUI(
												TradingUI.UI_PIN);
								 }else{
									 apps.getUI().showUI(TradingUI.UI_SL);
									}
							 }else{
							 showLogin();
							 }

					}
			});
			map.put(A_SHOWORDERMATRIC, new Action(A_SHOWORDERMATRIC,"Order Matrix",
							new MenuProperty(1, 
									"Transaction", 8, false, true),
									TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
						
						@Override
						 public void actionPerformed(ActionEvent e) {
							 if(isReadyState()){
								 if(!isGranted()){
									 ((TradingApplication) apps).getUI().showUI(
												TradingUI.UI_PIN);
								 }else{
									 apps.getUI().showUI(TradingUI.UI_ORDERMATRIX);
									}
							 }else{
							 showLogin();
							 }
						}
				}
			);		

		}
	}
	
	public void deleteAction(int actionType){
		map.remove(actionType);
	}

	protected void showLogin() {
		((TradingApplication) apps).getFeed().getUI().showUI(FeedUI.UI_LOGON);
	}

	@Override
	public void setState(int newState) {
		super.setState(newState);

		Iterator i = this.map.keySet().iterator();
		while (i.hasNext()) {
			Integer code = (Integer) i.next();
			Action a = (Action) this.map.get(code);
			if (isValidCode(code))
				a.setState(Action.C_READY_STATE);
		}

	}

	public boolean isValidCode(Integer code) {
		if (code == A_SHOWCHGPIN || code == A_SHOWCONN || code == A_SHOWREFRESH
				|| code == A_SHOWRECONNECT)
			return false;

		return true;
	}

	public boolean isReadyState() {
		return getState() == Action.C_READY_STATE;
	}
}