package eqtrade.trading.market.chart.indicator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBoxMenuItem;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;

public class AvgPricePlugin extends ChartIndicatorPlugin{
	private AvgPriceIndicator avgPrice = null;
	  private boolean show = false;

	  public AvgPricePlugin(IChartController controller) {
	    super(controller, "Avg Price");
	    this.avgPrice = new AvgPriceIndicator(this.controller, "Avg Price", "");
	  }

	  protected void build() {
	    this.menu = new JCheckBoxMenuItem(this.name);
	    this.menu.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent arg0) {
	        if (!AvgPricePlugin.this.show)
	          AvgPricePlugin.this.avgPrice.show();
	        else {
	          AvgPricePlugin.this.avgPrice.hide();
	        }
	        AvgPricePlugin.this.show = (!AvgPricePlugin.this.show);
	        AvgPricePlugin.this.menu.setSelected(AvgPricePlugin.this.show);
	      }
	    });
	  }
}
