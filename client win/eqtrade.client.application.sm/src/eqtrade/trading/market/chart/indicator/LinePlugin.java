package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JComboBox;
import javax.swing.JLabel;

public class LinePlugin extends ChartIndicatorPlugin{
	  private LineIndicator line = null;
	  private JComboBox combo;
	  private JText color;

	  public LinePlugin(IChartController controller)
	  {
	    super(controller, "OHLC");
	    line = new LineIndicator(controller, "Line", "");
	  }

	  protected void build() {
	    super.build();
	    combo = ChartUtil.getComboOHLC();
	    color = ChartUtil.getFieldColor();
	    combo.setSelectedItem("Close");
	    color.setBackground(LineIndicator.DEFAULT_COLOR);
	    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("OHLC:"), cc.xy(1, 1));
	    builder.add(combo, cc.xy(3, 1));
	    builder.add(new JLabel("Color:"), cc.xy(1, 3));
	    builder.add(color, cc.xy(3, 3));
	  }
	  public void show() {
	    line.show();
	  }
	  public void hide() {
	    line.hide();
	  }
	  public void init() {
	    combo.setSelectedItem(line.getType());
	    color.setBackground(line.getColor());
	  }
	  public void update() {
	    line.setType(combo.getSelectedItem().toString());
	    line.setColor(color.getBackground());
	  }
}
