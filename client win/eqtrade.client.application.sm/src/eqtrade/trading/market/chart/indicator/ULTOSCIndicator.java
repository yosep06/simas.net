package eqtrade.trading.market.chart.indicator;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class ULTOSCIndicator extends ChartIndicator{

	  public static final Color DEFAULT_COLOR = Color.orange;
	  public static final int DEFAULT_SHORT_PERIOD = 7;
	  public static final int DEFAULT_MEDIUM_PERIOD = 14;
	  public static final int DEFAULT_LONG_PERIOD = 28;
	  private Color color = DEFAULT_COLOR;
	  private int shortPeriod = 7;
	  private int mediumPeriod = 14;
	  private int longPeriod = 28;

	  public ULTOSCIndicator(IChartController controller, String name, String securities)
	  {
	    super(controller, "TOP", name, "line", securities);
	    build();
	  }

	  public ULTOSCIndicator(IChartController controller, String name, String securities, int shortPeriod, int mediumPeriod, int longPeriod, Color color) {
	    super(controller, "TOP", name, "line", securities);
	    this.shortPeriod = shortPeriod;
	    this.mediumPeriod = mediumPeriod;
	    this.longPeriod = longPeriod;
	    this.color = color;
	    build();
	  }

	  protected void build() {
	    this.render = new XYLineAndShapeRenderer(true, false);
	    this.render.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    this.render.setSeriesPaint(0, this.color);
	    this.render.setSeriesStroke(0, new BasicStroke(1.65F));
	  }

	  public void calculate() {
	    double[] inReal = this.controller.getDataset().getCloseArray();
	    int startIdx = 0;
	    int endIdx = inReal.length - 1;
	    MInteger outBegIdx = new MInteger();
	    MInteger outNbElement = new MInteger();
	    double[] inHigh = this.controller.getDataset().getHighArray();
	    double[] inLow = this.controller.getDataset().getLowArray();
	    double[] inClose = this.controller.getDataset().getCloseArray();

	    double[] outReal = getOutputArray(inReal, this.controller.getTalibCore().ultOscLookback(this.shortPeriod, this.mediumPeriod, this.longPeriod));

	    this.controller.getTalibCore().ultOsc(startIdx, endIdx, inHigh, inLow, inClose, this.shortPeriod, this.mediumPeriod, this.longPeriod, outBegIdx, outNbElement, outReal);

	    if (outReal != null) {
	      TimeSeries timeseries = new TimeSeries(this.name, "", "", Minute.class);
	      int count2 = this.controller.getDataset().getDateArray().length;
	      int count = outReal.length;
	      for (int i = count; i > 0; i--) {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.controller.getDataset().getDateArray()[(count2 - 1)])), outReal[(i - 1)]);
	        count2--;
	      }
	      this.xydataset = new TimeSeriesCollection(timeseries);
	    }
	  }

	  public void show() {
	    this.render.setSeriesPaint(0, this.color);
	    calculate();
	    this.controller.getDataset().addListener(this);
	    this.controller.getContainer().addPlot(this.position, this.name, this.xydataset, null, this.render);
	  }

	  public void hide() {
	    this.controller.getDataset().removeListener(this);
	    this.controller.getContainer().removePlot(this.position, this.name);
	  }

	  public void chartChanged()
	  {
	    ((TimeSeriesCollection)this.xydataset).removeAllSeries();
	    calculate();
	    this.controller.getContainer().updateDataset(this.position, this.name, this.xydataset);
	  }

	  public Color getColor() {
	    return this.color;
	  }

	  public void setColor(Color color) {
	    this.color = color;
	  }

	  public int getLongPeriod() {
	    return this.longPeriod;
	  }

	  public void setLongPeriod(int longPeriod) {
	    this.longPeriod = longPeriod;
	  }

	  public int getMediumPeriod() {
	    return this.mediumPeriod;
	  }

	  public void setMediumPeriod(int mediumPeriod) {
	    this.mediumPeriod = mediumPeriod;
	  }

	  public int getShortPeriod() {
	    return this.shortPeriod;
	  }

	  public void setShortPeriod(int shortPeriod) {
	    this.shortPeriod = shortPeriod;
	  }
}
