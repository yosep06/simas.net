package eqtrade.trading.market.chart.indicator;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;
import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class STOCHIndicator extends ChartIndicator{
	  public static final Color DEFAULT_COLOR = Color.red;
	  public static final int DEFAULT_PERIOD = 3;
	  public static final int DEFAULT_FAST_K_PERIOD = 5;
	  public static final int DEFAULT_FAST_D_PERIOD = 3;
	  public static final int DEFAULT_FAST_D_MA_TYPE = 3;
	  public static final Color DEFAULT_D_COLOR = Color.white;
	  public static final int DEFAULT_SLOW_K_PERIOD = 3;
	  public static final int DEFAULT_SLOW_K_MA_TYPE = 0;
	  public static final int DEFAULT_SLOW_D_PERIOD = 3;
	  public static final int DEFAULT_SLOW_D_MA_TYPE = 0;
	  private Color color = DEFAULT_COLOR;
	  private int period = 3;
	  private int fastKPeriod = 5;
	  private Color dcolor = DEFAULT_D_COLOR;
	  private int fastDPeriod = 3;
	  private int fastDMAType = 3;

	  private int slowKPeriod = 3;
	  private int slowKMAType = 0;
	  private int slowDPeriod = 3;
	  private int slowDMAType = 0;
	  protected XYDataset xydataset2;
	  protected XYItemRenderer render2;

	  public STOCHIndicator(IChartController controller, String name, String securities)
	  {
	    super(controller, "TOP", name, "line", securities);
	    build();
	  }

	  public STOCHIndicator(IChartController controller, String name, String securities, int period, int fastKPeriod, int fastDPeriod, int fastDMAType, Color color, Color dcolor) {
	    super(controller, "TOP", name, "line", securities);
	    this.period = period;
	    this.fastKPeriod = fastKPeriod;
	    this.fastDPeriod = fastDPeriod;
	    this.fastDMAType = fastDMAType;
	    this.color = color;
	    this.dcolor = dcolor;
	    build();
	  }

	  protected void build() {
	    this.render = new XYLineAndShapeRenderer(true, false);
	    this.render.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    this.render.setSeriesPaint(0, this.color);
	    this.render.setSeriesStroke(0, new BasicStroke(1.65F));
	    this.render2 = new XYLineAndShapeRenderer(true, false);
	    this.render2.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    this.render2.setSeriesPaint(0, this.dcolor);
	    this.render2.setSeriesStroke(0, new BasicStroke(1.0F, 1, 1, 1.0F, new float[] { 3.0F, 3.0F }, 0.0F));
	  }

	  public void calculate() {
	    double[] inReal = this.controller.getDataset().getCloseArray();
	    int startIdx = 0;
	    int endIdx = inReal.length - 1;
	    MInteger outBegIdx = new MInteger();
	    MInteger outNbElement = new MInteger();

	    double[] outSlowK = getOutputArray(inReal, this.controller.getTalibCore().stochLookback(this.fastKPeriod, this.slowKPeriod, getTA_MAType(this.slowKMAType), this.slowDPeriod, getTA_MAType(this.slowDMAType)));
	    double[] outSlowD = getOutputArray(inReal, this.controller.getTalibCore().stochLookback(this.fastKPeriod, this.slowKPeriod, getTA_MAType(this.slowKMAType), this.slowDPeriod, getTA_MAType(this.slowDMAType)));

	    this.controller.getTalibCore().stoch(startIdx, endIdx, this.controller.getDataset().getHighArray(), this.controller.getDataset().getLowArray(), this.controller.getDataset().getCloseArray(), this.fastKPeriod, this.slowKPeriod, getTA_MAType(this.slowKMAType), this.slowDPeriod, getTA_MAType(this.slowDMAType), outBegIdx, outNbElement, outSlowK, outSlowD);

	    if (outSlowK != null) {
	      TimeSeries timeseries = new TimeSeries(this.name + "-K", "", "", Minute.class);
	      int count2 = this.controller.getDataset().getDateArray().length;
	      int count = outSlowK.length;
	      for (int i = count; i > 0; i--) {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.controller.getDataset().getDateArray()[(count2 - 1)])), outSlowK[(i - 1)]);
	        count2--;
	      }
	      this.xydataset = new TimeSeriesCollection(timeseries);
	    }

	    if (outSlowD != null) {
	      TimeSeries timeseries = new TimeSeries(this.name + "-D", "", "", Minute.class);
	      int count2 = this.controller.getDataset().getDateArray().length;
	      int count = outSlowD.length;
	      for (int i = count; i > 0; i--) {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.controller.getDataset().getDateArray()[(count2 - 1)])), outSlowD[(i - 1)]);
	        count2--;
	      }
	      this.xydataset2 = new TimeSeriesCollection(timeseries);
	    }
	  }

	  public void show() {
	    this.render.setSeriesPaint(0, this.color);
	    this.render2.setSeriesPaint(0, this.dcolor);
	    calculate();
	    this.controller.getDataset().addListener(this);
	    this.controller.getContainer().addPlot(this.position, this.name + "-K", this.xydataset, null, this.render);
	    this.controller.getContainer().addPlot(this.position, this.name + "-D", this.xydataset2, null, this.render2);
	  }

	  public void hide() {
	    this.controller.getDataset().removeListener(this);
	    this.controller.getContainer().removePlot(this.position, this.name + "-K");
	    this.controller.getContainer().removePlot(this.position, this.name + "-D");
	  }

	  public void chartChanged()
	  {
	    ((TimeSeriesCollection)this.xydataset).removeAllSeries();
	    ((TimeSeriesCollection)this.xydataset2).removeAllSeries();
	    calculate();
	    this.controller.getContainer().updateDataset(this.position, this.name + "-K", this.xydataset);
	    this.controller.getContainer().updateDataset(this.position, this.name + "-D", this.xydataset2);
	  }

	  public Color getColor() {
	    return this.color;
	  }

	  public void setColor(Color color) {
	    this.color = color;
	  }

	  public Color getDcolor() {
	    return this.dcolor;
	  }

	  public void setDcolor(Color dcolor) {
	    this.dcolor = dcolor;
	  }

	  public int getFastDMAType() {
	    return this.fastDMAType;
	  }

	  public void setFastDMAType(int fastDMAType) {
	    this.fastDMAType = fastDMAType;
	  }

	  public int getFastDPeriod() {
	    return this.fastDPeriod;
	  }

	  public void setFastDPeriod(int fastDPeriod) {
	    this.fastDPeriod = fastDPeriod;
	  }

	  public int getFastKPeriod() {
	    return this.fastKPeriod;
	  }

	  public void setFastKPeriod(int fastKPeriod) {
	    this.fastKPeriod = fastKPeriod;
	  }

	  public int getPeriod() {
	    return this.period;
	  }

	  public void setPeriod(int period) {
	    this.period = period;
	  }

	  public int getSlowDMAType() {
	    return this.slowDMAType;
	  }

	  public void setSlowDMAType(int slowDMAType) {
	    this.slowDMAType = slowDMAType;
	  }

	  public int getSlowDPeriod() {
	    return this.slowDPeriod;
	  }

	  public void setSlowDPeriod(int slowDPeriod) {
	    this.slowDPeriod = slowDPeriod;
	  }

	  public int getSlowKMAType() {
	    return this.slowKMAType;
	  }

	  public void setSlowKMAType(int slowKMAType) {
	    this.slowKMAType = slowKMAType;
	  }

	  public int getSlowKPeriod() {
	    return this.slowKPeriod;
	  }

	  public void setSlowKPeriod(int slowKPeriod) {
	    this.slowKPeriod = slowKPeriod;
	  }
}
