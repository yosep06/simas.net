package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;

public class CandlePlugin extends ChartIndicatorPlugin{
	  private CandleIndicator candle = null;
	  private JText colorUp;
	  private JText colorDown;

	  public CandlePlugin(IChartController controller)
	  {
	    super(controller, "Candle Stick");
	    candle = new CandleIndicator(controller, "Candle Stick", "");
	    menu.setSelected(true);
	    candle.show();
	    show = true;
	  }

	  protected void build() {
	    super.build();
	    colorUp = ChartUtil.getFieldColor();
	    colorDown = ChartUtil.getFieldColor();
	    colorUp.setBackground(CandleIndicator.DEFAULT_COLOR_UP);
	    colorDown.setBackground(CandleIndicator.DEFAULT_COLOR_DOWN);
	    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("Color Up:"), cc.xy(1, 1));
	    builder.add(colorUp, cc.xy(3, 1));
	    builder.add(new JLabel("Color Down:"), cc.xy(1, 3));
	    builder.add(colorDown, cc.xy(3, 3));
	  }

	  public void show() {
	    candle.show();
	  }
	  public void hide() {
	    candle.hide();
	  }
	  public void init() {
	    colorUp.setBackground(candle.getColorUp());
	    colorDown.setBackground(candle.getColorDown());
	  }
	  public void update() {
	    candle.setColorUp(colorUp.getBackground());
	    candle.setColorDown(colorDown.getBackground());
	  }
}
