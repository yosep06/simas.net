package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JNumber;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class STOCHPlugin extends ChartIndicatorPlugin{
	 private STOCHIndicator stoch = null;
	  private JText color;
	  private JNumber fastK;
	  private JNumber slowK;
	  private JComboBox slowKMA;
	  private JText colorD;
	  private JNumber slowD;
	  private JComboBox slowDMA;

	  public STOCHPlugin(IChartController controller)
	  {
	    super(controller, "Stochastic - STOCH");
	    stoch = new STOCHIndicator(controller, "STOCH", "");
	    menu.setSelected(true);
	    stoch.show();
	    show = true;
	  }

	  protected void build() {
	    super.build();
	    color = ChartUtil.getFieldColor();
	    fastK = ChartUtil.getFieldNumber(0, 0);
	    slowK = ChartUtil.getFieldNumber(0, 0);
	    slowKMA = ChartUtil.getComboMA();
	    colorD = ChartUtil.getFieldColor();
	    slowD = ChartUtil.getFieldNumber(0, 0);
	    slowDMA = ChartUtil.getComboMA();

	    color.setBackground(STOCHIndicator.DEFAULT_COLOR);
	    fastK.setValue(new Double(5.0D));
	    slowK.setValue(new Double(3.0D));
	    slowKMA.setSelectedIndex(0);
	    colorD.setBackground(STOCHIndicator.DEFAULT_D_COLOR);
	    slowD.setValue(new Double(3.0D));
	    slowDMA.setSelectedIndex(0);

	    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("Color:"), cc.xy(1, 1));
	    builder.add(color, cc.xy(3, 1));
	    builder.add(new JLabel("Fast %K Period:"), cc.xy(1, 3));
	    builder.add(fastK, cc.xy(3, 3));
	    builder.add(new JLabel("Slow %K Period:"), cc.xy(1, 5));
	    builder.add(slowK, cc.xy(3, 5));
	    builder.add(new JLabel("Slow %K MA Type"), cc.xy(1, 7));
	    builder.add(slowKMA, cc.xy(3, 7));
	    builder.add(new JLabel("%D Color"), cc.xy(1, 9));
	    builder.add(colorD, cc.xy(3, 9));
	    builder.add(new JLabel("Slow %D Period"), cc.xy(1, 11));
	    builder.add(slowD, cc.xy(3, 11));
	    builder.add(new JLabel("Slow %D MA Type"), cc.xy(1, 13));
	    builder.add(slowDMA, cc.xy(3, 13));
	  }
	  public void show() {
	    stoch.show();
	  }
	  public void hide() {
	    stoch.hide();
	  }
	  public void init() {
	    color.setBackground(stoch.getColor());
	    fastK.setValue(new Double(stoch.getFastKPeriod()));
	    slowK.setValue(new Double(stoch.getSlowKPeriod()));
	    slowKMA.setSelectedIndex(stoch.getSlowKMAType());
	    colorD.setBackground(stoch.getDcolor());
	    slowD.setValue(new Double(stoch.getSlowDPeriod()));
	    slowDMA.setSelectedIndex(stoch.getSlowDMAType());
	  }
	  public void update() {
	    stoch.setColor(color.getBackground());
	    stoch.setFastKPeriod(((Double)fastK.getValue()).intValue());
	    stoch.setSlowKPeriod(((Double)slowK.getValue()).intValue());
	    stoch.setSlowKMAType(slowKMA.getSelectedIndex());
	    stoch.setDcolor(colorD.getBackground());
	    stoch.setSlowDPeriod(((Double)slowD.getValue()).intValue());
	    stoch.setSlowDMAType(slowDMA.getSelectedIndex());
	  }
}
