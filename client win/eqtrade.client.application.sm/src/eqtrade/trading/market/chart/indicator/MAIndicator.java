package eqtrade.trading.market.chart.indicator;

import com.tictactec.ta.lib.MInteger;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;

import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class MAIndicator extends ChartIndicator{
	  public static final int DEFAULT_PERIOD = 14;
	  public static final int DEFAULT_TYPE = 0;
	  public static final Color DEFAULT_COLOR = Color.blue;

	  private int type = 0;
	  private int period = 14;
	  private Color color = DEFAULT_COLOR;

	  public MAIndicator(IChartController controller, String name, String securities) {
	    super(controller, "MIDDLE", name, "line", securities);
	    build();
	  }

	  public MAIndicator(IChartController controller, String name, String securities, int period, int type, Color color) {
	    super(controller, "MIDDLE", name, "line", securities);
	    this.period = period;
	    this.type = type;
	    this.color = color;
	    build();
	  }

	  protected void build() {
	    this.render = new XYLineAndShapeRenderer(true, false);
	    this.render.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    this.render.setSeriesPaint(0, this.color);
	    this.render.setSeriesStroke(0, new BasicStroke(1.0F));
	  }

	  public void calculate() {
	    double[] inReal = this.controller.getDataset().getCloseArray();
	    int startIdx = 0;
	    int endIdx = inReal.length - 1;
	    MInteger outBegIdx = new MInteger();
	    MInteger outNbElement = new MInteger();

	    double[] outReal = (double[])null;

	    switch (this.type) {
	    case 0:
	      outReal = getOutputArray(inReal, this.controller.getTalibCore().smaLookback(this.period));
	      this.controller.getTalibCore().sma(startIdx, endIdx, inReal, this.period, outBegIdx, outNbElement, outReal);
	      break;
	    case 1:
	      outReal = getOutputArray(inReal, this.controller.getTalibCore().emaLookback(this.period));
	      this.controller.getTalibCore().ema(startIdx, endIdx, inReal, this.period, outBegIdx, outNbElement, outReal);
	      break;
	    case 2:
	      outReal = getOutputArray(inReal, this.controller.getTalibCore().wmaLookback(this.period));
	      this.controller.getTalibCore().wma(startIdx, endIdx, inReal, this.period, outBegIdx, outNbElement, outReal);
	      break;
	    case 3:
	      outReal = getOutputArray(inReal, this.controller.getTalibCore().demaLookback(this.period));
	      this.controller.getTalibCore().dema(startIdx, endIdx, inReal, this.period, outBegIdx, outNbElement, outReal);
	      break;
	    case 4:
	      outReal = getOutputArray(inReal, this.controller.getTalibCore().temaLookback(this.period));
	      this.controller.getTalibCore().tema(startIdx, endIdx, inReal, this.period, outBegIdx, outNbElement, outReal);
	      break;
	    case 5:
	      outReal = getOutputArray(inReal, this.controller.getTalibCore().trimaLookback(this.period));
	      this.controller.getTalibCore().trima(startIdx, endIdx, inReal, this.period, outBegIdx, outNbElement, outReal);
	      break;
	    case 6:
	      outReal = getOutputArray(inReal, this.controller.getTalibCore().kamaLookback(this.period));
	      this.controller.getTalibCore().kama(startIdx, endIdx, inReal, this.period, outBegIdx, outNbElement, outReal);
	    }

	    if (outReal != null) {
	      TimeSeries timeseries = new TimeSeries(this.name, "", "", Minute.class);
	      int count2 = this.controller.getDataset().getDateArray().length;
	      int count = outReal.length;
	      for (int i = count; i > 0; i--)
	      {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.controller.getDataset().getDateArray()[(count2 - 1)])), outReal[(i - 1)]);
	        count2--;
	      }
	      this.xydataset = new TimeSeriesCollection(timeseries);
	    }
	  }

	  public void show() {
	    this.render.setSeriesPaint(0, this.color);
	    calculate();
	    this.controller.getDataset().addListener(this);
	    this.controller.getContainer().addPlot(this.position, this.name, this.xydataset, null, this.render);
	  }

	  public void hide() {
	    this.controller.getDataset().removeListener(this);
	    this.controller.getContainer().removePlot(this.position, this.name);
	  }

	  public void chartChanged()
	  {
	    ((TimeSeriesCollection)this.xydataset).removeAllSeries();
	    calculate();
	    this.controller.getContainer().updateDataset(this.position, this.name, this.xydataset);
	  }

	  public Color getColor() {
	    return this.color;
	  }

	  public void setColor(Color color) {
	    this.color = color;
	  }

	  public int getPeriod() {
	    return this.period;
	  }

	  public void setPeriod(int period) {
	    this.period = period;
	  }

	  public int getType() {
	    return this.type;
	  }

	  public void setType(int type) {
	    this.type = type;
	  }
}
