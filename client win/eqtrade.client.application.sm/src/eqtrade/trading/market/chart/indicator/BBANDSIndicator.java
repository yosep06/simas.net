package eqtrade.trading.market.chart.indicator;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;

import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class BBANDSIndicator extends ChartIndicator {
	public static final Color DEFAULT_COLOR = Color.yellow;
	  public static final int DEFAULT_PERIOD = 14;
	  public static final double DEFAULT_UPPER_DEVIATION = 2.0D;
	  public static final double DEFAULT_LOWER_DEVIATION = 2.0D;
	  public static final int DEFAULT_MA_TYPE = 0;
	  public static final String DEFAULT_INPUT = "Close";
	  private Color color = DEFAULT_COLOR;
	  private int period = 14;
	  private double upperDeviation = 2.0D;
	  private double lowerDeviation = 2.0D;
	  private int maType = 0;
	  private String input = "Close";
	  protected XYDataset xydataset2;
	  protected XYItemRenderer render2;
	  protected XYDataset xydataset3;
	  protected XYItemRenderer render3;

	  public BBANDSIndicator(IChartController controller, String name, String securities)
	  {
	    super(controller, "MIDDLE", name, "line", securities);
	    build();
	  }

	  public BBANDSIndicator(IChartController controller, String name, String securities, int period, int upperDeviation, int lowerDeviation, int maType, Color color) {
	    super(controller, "MIDDLE", name, "line", securities);
	    period = period;
	    upperDeviation = upperDeviation;
	    lowerDeviation = lowerDeviation;
	    maType = maType;
	    color = color;
	    build();
	  }

	  protected void build()
	  {
	    render = new XYLineAndShapeRenderer(true, false);
	    render.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy"), new DecimalFormat("0.00")));
	    render.setSeriesPaint(0, color);
	    render.setSeriesStroke(0, new BasicStroke(1.0F, 1, 1, 1.0F, new float[] { 4.0F, 4.0F }, 0.0F));
	    render.setSeriesPaint(1, color);
	    render.setSeriesStroke(1, new BasicStroke(1.0F, 1, 1, 1.0F, new float[] { 4.0F, 4.0F }, 0.0F));
	    render2 = new XYLineAndShapeRenderer(true, false);
	    render2.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    render2.setSeriesPaint(0, color);
	    render2.setSeriesPaint(1, color);
	    render2.setSeriesStroke(0, new BasicStroke(1.0F, 1, 1, 1.0F, new float[] { 2.0F, 2.0F }, 0.0F));
	    render3 = new XYLineAndShapeRenderer(true, false);
	    render3.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    render3.setSeriesPaint(0, color);
	    render3.setSeriesStroke(0, new BasicStroke(1.0F));
	  }

	  public void calculate() {
	    double[] inReal = getInputData(input);
	    int startIdx = 0;
	    int endIdx = inReal.length - 1;
	    MInteger outBegIdx = new MInteger();
	    MInteger outNbElement = new MInteger();

	    double[] outUpper = getOutputArray(inReal, controller.getTalibCore().bbandsLookback(period, upperDeviation, lowerDeviation, getTA_MAType(maType)));
	    double[] outMiddle = getOutputArray(inReal, controller.getTalibCore().bbandsLookback(period, upperDeviation, lowerDeviation, getTA_MAType(maType)));
	    double[] outLower = getOutputArray(inReal, controller.getTalibCore().bbandsLookback(period, upperDeviation, lowerDeviation, getTA_MAType(maType)));

	    controller.getTalibCore().bbands(startIdx, endIdx, inReal, period, upperDeviation, lowerDeviation, getTA_MAType(maType), outBegIdx, outNbElement, outUpper, outMiddle, outLower);

	    TimeSeries timeseries = new TimeSeries(name + "-Upper", "", "", Minute.class);
	    TimeSeries timeseries2 = new TimeSeries(name + "-Middle", "", "", Minute.class);
	    TimeSeries timeseries3 = new TimeSeries(name + "-Lower", "", "", Minute.class);
	    int count2 = controller.getDataset().getDateArray().length;
	    int count = outUpper.length;
	    for (int i = count; i > 0; i--) {
	      timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(controller.getDataset().getDateArray()[(count2 - 1)])), outUpper[(i - 1)]);
	      timeseries2.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(controller.getDataset().getDateArray()[(count2 - 1)])), outMiddle[(i - 1)]);
	      timeseries3.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(controller.getDataset().getDateArray()[(count2 - 1)])), outLower[(i - 1)]);
	      count2--;
	    }
	    xydataset = new TimeSeriesCollection(timeseries);
	    ((TimeSeriesCollection)xydataset).addSeries(timeseries3);
	    xydataset2 = new TimeSeriesCollection(timeseries2);
	  }

	  public void show()
	  {
	    render.setSeriesPaint(0, color);
	    render.setSeriesPaint(1, color);
	    render2.setSeriesPaint(0, color);
	    render3.setSeriesPaint(0, color);
	    calculate();
	    controller.getDataset().addListener(this);
	    controller.getContainer().addPlot(position, name + "-Middle", xydataset2, null, render2);
	    controller.getContainer().addPlot(position, name + "-Upper", xydataset, null, render);
	  }

	  public void hide()
	  {
	    controller.getDataset().removeListener(this);
	    controller.getContainer().removePlot(position, name + "-Upper");
	    controller.getContainer().removePlot(position, name + "-Middle");
	  }

	  public void chartChanged()
	  {
	    ((TimeSeriesCollection)xydataset).removeAllSeries();
	    ((TimeSeriesCollection)xydataset2).removeAllSeries();

	    calculate();
	    controller.getContainer().updateDataset(position, name + "-Upper", xydataset);
	    controller.getContainer().updateDataset(position, name + "-Middle", xydataset2);
	  }

	  public Color getColor()
	  {
	    return color;
	  }

	  public void setColor(Color color) {
	    this.color = color;
	  }

	  public double getLowerDeviation() {
	    return lowerDeviation;
	  }

	  public void setLowerDeviation(double lowerDeviation) {
	    this.lowerDeviation = lowerDeviation;
	  }

	  public int getMaType() {
	    return maType;
	  }

	  public void setMaType(int maType) {
	    this.maType = maType;
	  }

	  public int getPeriod() {
	    return period;
	  }

	  public void setPeriod(int period) {
	    this.period = period;
	  }

	  public double getUpperDeviation() {
	    return upperDeviation;
	  }

	  public void setUpperDeviation(double upperDeviation) {
	    this.upperDeviation = upperDeviation;
	  }

	  public String getInput() {
	    return input;
	  }

	  public void setInput(String input) {
	    this.input = input;
	  }
}
