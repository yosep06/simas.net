package eqtrade.trading.market.chart.core;

import com.tictactec.ta.lib.Core;

import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.ext.IChartDataset;
import eqtrade.trading.market.chart.ext.IChartDatasource;
import eqtrade.trading.market.chart.source.YahooSource;

public class ChartController implements IChartController {
	protected ChartDataset dataset;
	  protected IChartDatasource source;
	  protected ChartContainer container;
	  protected Core talibCore;

	  public ChartController()
	  {
	    this.talibCore = new Core();
	    this.source = new YahooSource();
	    this.dataset = new ChartDataset(this.source);
	    this.container = new ChartContainer(this.dataset);
	  }

	  public ChartController(IChartDatasource ds)
	  {
	    this.source = ds;
	    this.talibCore = new Core();
	    this.dataset = new ChartDataset(this.source);
	    this.container = new ChartContainer(this.dataset);
	  }

	  public Core getTalibCore() {
	    return this.talibCore;
	  }

	  public void setDatasource(IChartDatasource ds) {
	    this.source = ds;
	    this.dataset.setDatasource(this.source);
	  }

	  public IChartDataset getDataset() {
	    return this.dataset;
	  }

	  public ChartContainer getContainer() {
	    return this.container;
	  }
}
