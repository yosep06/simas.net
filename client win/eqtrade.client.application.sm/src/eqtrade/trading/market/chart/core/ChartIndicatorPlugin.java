package eqtrade.trading.market.chart.core;

import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.ext.IChartEditor;
import eqtrade.trading.market.chart.ext.IIndicator;
import com.vollux.ui.JSkinPnl;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;

public class ChartIndicatorPlugin implements IIndicator, IChartEditor{
	  protected JCheckBoxMenuItem menu;
	  protected String name;
	  protected IChartController controller;
	  protected IndicatorEditor editor;
	  protected JSkinPnl panel;
	  protected boolean show;

	  public ChartIndicatorPlugin(IChartController controller, String name)
	  {
	    this.name = name;
	    this.controller = controller;
	    build();
	  }

	  protected void build() {
	    this.panel = new JSkinPnl(new FlowLayout());
	    this.editor = new IndicatorEditor(this.name, this);
	    this.menu = new JCheckBoxMenuItem(this.name);
	    this.menu.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent arg0) {
	        int result = ChartIndicatorPlugin.this.editor.ShowEditor();
	        if (result == 0) {
	          ChartIndicatorPlugin.this.hide();
	          ChartIndicatorPlugin.this.menu.setSelected(true);
	          ChartIndicatorPlugin.this.show();
	          ChartIndicatorPlugin.this.show = true;
	        } else if (result == 2) {
	          ChartIndicatorPlugin.this.hide();
	          ChartIndicatorPlugin.this.menu.setSelected(false);
	          ChartIndicatorPlugin.this.show = false;
	        } else {
	          ChartIndicatorPlugin.this.menu.setSelected(ChartIndicatorPlugin.this.show);
	        }
	      }
	    });
	  }

	  public JMenuItem getMenu() {
	    return this.menu;
	  }

	  public JSkinPnl getPanel() {
	    return this.panel;
	  }
	  public boolean isValidEntry() {
	    return true;
	  }

	  public void show()
	  {
	  }

	  public void hide()
	  {
	  }

	  public void init()
	  {
	  }

	  public void update()
	  {
	  }
}
