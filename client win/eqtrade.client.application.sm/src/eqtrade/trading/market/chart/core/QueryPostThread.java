package eqtrade.trading.market.chart.core;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import eqtrade.trading.market.chart.ext.ICallback;
import eqtrade.trading.market.chart.ext.IResult;
import eqtrade.trading.market.chart.model.Chart;
import eqtrade.trading.market.chart.util.ChartUtil;

public class QueryPostThread extends Thread{
	  protected HttpClient httpClient;
	  protected Vector method;
	  protected ICallback callback;
	  protected IResult process;
	  protected Vector info;
	  protected String proxyip = "";
	  protected String proxyport = "";
	  protected boolean useproxy = false;
	  protected String stock;
	  protected String maxdt;

	  public QueryPostThread(String stock, String maxdt, HttpClient httpClient, Vector info, Vector method, ICallback callback, IResult result)
	  {
	    this.httpClient = httpClient;
	    this.method = method;
	    this.callback = callback;
	    this.process = result;
	    this.info = info;
	    this.stock = stock;
	    this.maxdt = maxdt;
	    loadProxy();
	  }

	  protected void loadProxy()
	  {
	  }

	  public void run()
	  {
	    if (this.useproxy)
	      this.httpClient.getHostConfiguration().setProxy(this.proxyip, Integer.parseInt(this.proxyport));
	    else {
	      this.httpClient.getHostConfiguration().setProxyHost(null);
	    }
	    boolean success = true;
	    for (int counter = 0; (counter < this.method.size()) && (success); )
	      try {
	        if (this.callback != null) this.callback.setStatus("loading " + this.info.get(counter) + "...");
	        this.httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
	        int statusCode = this.httpClient.executeMethod((PostMethod)this.method.get(counter));
	        if (statusCode == 200) {
	          long length = ((PostMethod)this.method.get(counter)).getResponseContentLength();
	          InputStream is = ((PostMethod)this.method.get(counter)).getResponseBodyAsStream();
	          BufferedInputStream bis = new BufferedInputStream(is);
	          ByteArrayOutputStream fos = new ByteArrayOutputStream();
	          byte[] byteTemp = new byte[1024];
	          int count = bis.read(byteTemp);
	          int total = count;
	          while ((count != -1) && (count <= 1024)) {
	            if (this.callback != null) this.callback.setStatus("receiving " + this.info.get(counter) + " " + new Double(fos.size() * 100 / length).doubleValue() + "% ");
	            fos.write(byteTemp, 0, count);
	            total += count;
	            count = bis.read(byteTemp);
	          }
	          if (this.callback != null) this.callback.setStatus("receiving " + this.info.get(counter) + " " + new Double(fos.size() * 100 / length).doubleValue() + "% ");
	          if (count != -1) {
	            fos.write(byteTemp, 0, count);
	          }
	          fos.close();
	          bis.close();
	          JSONObject obj = null;
	          String error = (String)obj.get("ERR");
	          if (!error.equals("")) {
	            success = false;
	            this.process.receiveData(new Vector());
	            if (this.callback != null) this.callback.failed(error); 
	          }
	          else {
	            JSONArray rst = (JSONArray)obj.get("RST");
	            for (int i = 0; i < rst.size(); i++) {
	              JSONArray content = (JSONArray)rst.get(i);
	              JSONArray detail = (JSONArray)content.get(1);
	              Vector rows = new Vector(100, 25);
	              for (int j = 0; j < detail.size(); j++) {
	                if (this.callback != null) this.callback.setStatus("processing data (" + j + " of " + detail.size() + ") ");
	                JSONArray row = (JSONArray)detail.get(j);
	                Chart c = new Chart();
	                c.setStock(this.stock);
	                c.setDate((String)row.get(1));
	                c.setTime("");
	                c.setOpen(new Double(((BigDecimal)row.get(2)).doubleValue()).toString());
	                c.setHigh(new Double(((BigDecimal)row.get(3)).doubleValue()).toString());
	                c.setLow(new Double(((BigDecimal)row.get(4)).doubleValue()).toString());
	                c.setClose(new Double(((BigDecimal)row.get(5)).doubleValue()).toString());
	                c.setVolume(new Double(((BigDecimal)row.get(6)).doubleValue()).toString());
	                rows.add(0, c);
	              }

	              this.process.receiveData(rows);
	            }
	          }
	        } else {
	          success = false;
	          this.process.receiveData(new Vector());
	          if (this.callback != null) this.callback.failed("failed, data server not ready try again later"); 
	        }
	      }
	      catch (Exception ex) {
	        success = false;
	        ex.printStackTrace();
	        this.process.receiveData(new Vector());
	        if (this.callback != null) this.callback.failed("failed, data server not ready try again later"); 
	      }
	      finally {
	        ((PostMethod)this.method.get(counter)).releaseConnection();
	        //ret;
	      }
	    if ((this.callback != null) && (success)) this.callback.success(); 
	  }

	  public Chart getCurrentHLOC()
	  {
	    try {
	      Date maxDate = ChartUtil.C_FORMAT2.parse(this.maxdt);
	      Date currDate = ChartUtil.C_FORMAT2.parse(ChartUtil.C_FORMAT2.format(new Date()));
	      if (currDate.getTime() <= maxDate.getTime())
	      {
	        return null;
	      }
	      return null;
	    } catch (Exception ex) {
	    }
	    return null;
	  }
}
