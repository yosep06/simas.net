package eqtrade.trading.market.chart.core;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartTheme;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SegmentedTimeline;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

import eqtrade.trading.market.chart.ext.IChartDataset;
import eqtrade.trading.market.chart.ext.IChartPlot;

public class ChartContainerDaily implements IChartPlot {
	public static final String C_MIDDLE = "MIDDLE";
	  public static final SimpleDateFormat holidayFormat = new SimpleDateFormat("ddMMyyyy HHmm");
	  public static final SimpleDateFormat holidayFormat2 = new SimpleDateFormat("ddMMyyyy");
	  public static final SimpleDateFormat holidayFormat3 = new SimpleDateFormat("HHmm");
	  public static final SimpleDateFormat fridayFormat = new SimpleDateFormat("E");
	  protected MultiplePlot middlePlot;
	  protected CombinedDomainXYPlot allPlot;
	  protected JFreeChart jfreechart = null;
	  protected ChartPanel panel;
//	  protected DateAxis timeAxis;
	  private CustomDateAxis timeAxis;//update-280814
	  protected NumberAxis middleAxis;
	  protected SegmentedTimeline segment;
	  protected CandlestickRenderer candleRender;
	  protected XYLineAndShapeRenderer lineRender;
	  protected XYBarRenderer barRender;
	  protected IChartDataset dataset;

	  public ChartContainerDaily()
	  {
	    this.panel = new ChartPanel(createChart());
	  }

	  public ChartContainerDaily(IChartDataset dataset) {
	    this.dataset = dataset;

	    this.panel = new ChartPanel(createChart());
	  }

	  public CustomDateAxis getDateAxis() {//update-280814 DateAxis
	    return this.timeAxis;
	  }

	  public NumberAxis getNumberAxis() {
	    return this.middleAxis;
	  }

	  public XYPlot getMiddlePlot() {
	    return this.middlePlot;
	  }

	  public void addPlot(String position, String id, XYDataset dataset, ValueAxis axis, XYItemRenderer render)
	  {
	    this.middlePlot.addPlot(id, dataset, axis, render);
	  }

	  public void removePlot(String position, String id) {
	    this.middlePlot.removePlot(id);
	  }

	  public void updateDataset(String position, String id, XYDataset dataset) {
	    this.middlePlot.updateDataset(id, dataset);
	  }

	  public boolean isExistsPlot(String id) {
	    return this.middlePlot.isExistsPlot(id);
	  }

	  public ChartPanel getPanel() {
	    return this.panel;
	  }

	  public XYPlot getPlot() {
	    return this.allPlot;
	  }

	  protected static SegmentedTimeline nineToFour() {
	    SegmentedTimeline timeline = new SegmentedTimeline(900000L, 28, 68);
	    timeline.setStartTime(SegmentedTimeline.firstMondayAfter1900() + 36L * timeline.getSegmentSize());
	    timeline.setBaseTimeline(SegmentedTimeline.newMondayThroughFridayTimeline());
	    return timeline;
	  }

	  public SegmentedTimeline getTimeline() {
	    return this.segment;
	  }

	  protected JFreeChart createChart()
	  {
	    this.timeAxis = new CustomDateAxis("", new Date(), new Date(), getTimeline());//update-280814
	    //this.timeAxis = new DateAxis("");
	    this.timeAxis.setLabel("");
	    this.timeAxis.setDateFormatOverride(new SimpleDateFormat("dd-MM-yyyy"));

	    this.segment = SegmentedTimeline.newMondayThroughFridayTimeline();
	    this.timeAxis.setTimeline(this.segment);
	    this.timeAxis.setDateFormatOverride(new SimpleDateFormat("dd-MM-yyyy"));

	    this.middleAxis = new NumberAxis("value");
	    this.middleAxis.setAutoRangeIncludesZero(false);
	    this.candleRender = new CandlestickRenderer(4.0D);
	    this.candleRender.setDrawVolume(false);

	    this.lineRender = new XYLineAndShapeRenderer(true, false);
	    this.lineRender.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy"), new DecimalFormat("0.00")));
	    this.lineRender.setSeriesPaint(0, Color.blue);

	    this.barRender = new XYBarRenderer();

	    this.middlePlot = new MultiplePlot("MIDDLE", null, this.timeAxis, this.middleAxis, this.candleRender);
	    this.allPlot = new CombinedDomainXYPlot(this.timeAxis);
	    this.allPlot.add(this.middlePlot, 100);
	    this.allPlot.setGap(1.0D);
	    this.middlePlot.setDomainGridlinesVisible(false);
	    this.middlePlot.setRangeGridlinesVisible(true);
	    this.middleAxis.setLabel("");
	    this.middlePlot.setForegroundAlpha(0.95F);

	    this.jfreechart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, this.allPlot, true);

	    this.middlePlot.setDomainCrosshairVisible(true);
	    this.middlePlot.setRangeCrosshairVisible(true);
	    this.middlePlot.setRangeCrosshairLockedOnData(true);
	    this.middlePlot.setDomainCrosshairLockedOnData(true);
	    StandardChartTheme.createDarknessTheme().apply(this.jfreechart);
	    return this.jfreechart;
	  }
}
