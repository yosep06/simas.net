package eqtrade.feed.app;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;


import org.jfree.util.Log;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;
import com.vollux.framework.Action;
import com.vollux.framework.IApp;
import com.vollux.framework.MenuProperty;
import com.vollux.framework.VolluxAction;
import com.vollux.framework.VolluxApp;

import eqtrade.application.IEQTradeApp;
import eqtrade.application.UIMaster;
import eqtrade.feed.core.Encryption;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.ui.quote.UIMultiBestQuote;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;

public class FeedAction extends VolluxAction {
	public final static Integer A_EXIT = new Integer(0);
	public final static Integer A_SHOWLOCK = new Integer(1);
	public final static Integer A_SHOWCASHDEPOSIT = new Integer(2);
	public final static Integer A_SHOWCASHWITH = new Integer(3);
	public final static Integer A_SHOWREPORT = new Integer(4);
	public final static Integer A_HELP = new Integer(5);
	
	public final static Integer A_SHOWQUOTEDETAIL = new Integer(6);
	public final static Integer A_SHOWBESTQUOTE = new Integer(7);
	public final static Integer A_SHOWNGQUOTE = new Integer(8);
	public final static Integer A_SHOWSELECTEDQUOTE = new Integer(9);
	public final static Integer A_SHOWQUOTESUMM = new Integer(10);
	public final static Integer A_SHOWLIVETRADE = new Integer(11);
	public final static Integer A_SHOWLIVETRADESTOCK = new Integer(12);
	public final static Integer A_SHOWTIMETRADE = new Integer(13);
	public final static Integer A_SHOWTIMETRADEPRICE = new Integer(14);
	public final static Integer A_SHOWTOPSTOCK = new Integer(15);
	public final static Integer A_SHOWSTOCKSUMMARY = new Integer(16);
	public final static Integer A_SHOWSTOCKACTIVITY = new Integer(17);
	public final static Integer A_SHOWSTOCKLIST = new Integer(18);
	public final static Integer A_SHOWBROKERSUMM = new Integer(19);
	public final static Integer A_SHOWBROKERLIST = new Integer(20);
	public final static Integer A_SHOWTOPBROKER = new Integer(21);
	public final static Integer A_SHOWBROKERACTIVITY = new Integer(22);

	public final static Integer A_SHOWINDICES = new Integer(23);
	public final static Integer A_SHOWMARKETSUMM = new Integer(24);
	public final static Integer A_SHOWCHART = new Integer(25);
	public final static Integer A_SHOWNEWS = new Integer(26);

	public final static Integer A_SHOWLOGIN = new Integer(27);
	public final static Integer A_SHOWLOGOUT = new Integer(28);
	public final static Integer A_SHOWRECONNECT = new Integer(29);
	public final static Integer A_SHOWCHGPASS = new Integer(30);
	public final static Integer A_SHOWCONN = new Integer(31);
	public final static Integer A_SHOWCOLOUR = new Integer(32);
	public final static Integer A_SHOWCORPACTION = new Integer(33);

	public final static Integer A_SHOWINVMARKET = new Integer(34);
	public final static Integer A_SHOWINVSTOCK = new Integer(35);

	public final static Integer A_SHOWGLOBALNEWS = new Integer(36);
	public final static Integer A_SHOWWORLDIDXCURR = new Integer(37);
	public final static Integer A_SHOWTICKER = new Integer(38);

	public final static Integer A_SHOWCALC = new Integer(39);
	public final static Integer A_SHOWCONTACT = new Integer(40);
	public final static Integer A_SHOWCOMPANY = new Integer(41);

	
	public final static Integer A_SHOWUSERGUIDE = new Integer(42);
	public final static Integer A_SHOWFORGOT = new Integer(43);
	public final static Integer A_SHOWINTRADAY = new Integer(44);
	public final static Integer A_SHOWCHARTCOMPARE = new Integer(45);
	public final static Integer A_SHOWSHORTCUT = new Integer(46);

	public final static Integer A_SAVE = new Integer(47);
	public final static Integer A_LOAD = new Integer(48);

	public final static Integer A_PINON = new Integer(49);
	public final static Integer A_PINOFF = new Integer(50);

	public final static Integer A_RESTOREALL = new Integer(51);
	public final static Integer A_MINIMIZEALL = new Integer(52);
	public static JFileChooser fc;

	public final static Integer A_SHOWIPO = new Integer(53);
	public final static Integer A_SHOWRUPS = new Integer(54);
	
	
	public final static Integer A_SHOWUSERMANUAL = new Integer(55);
	public final static Integer A_SHOWYMCS1 = new Integer(56);
	public final static Integer A_SHOWYMCS2 = new Integer(57);
	public final static Integer A_SHOWYMCS3 = new Integer(58);
	public final static Integer A_SHOWYMCS4 = new Integer(59);
	public final static Integer A_SHOWYMRS1 = new Integer(60);
	public final static Integer A_SHOWYMRS2 = new Integer(61);
	public final static Integer A_SHOWYMRS3 = new Integer(62);
	public final static Integer A_SHOWYMRS4 = new Integer(63);
	public final static Integer A_SHOWYMRS5 = new Integer(64);
	public final static Integer A_SHOWEMAIL = new Integer(65);
	public final static Integer A_SHOWOFFICE = new Integer(66);
	public final static Integer A_SHOWABOUT = new Integer(67);
	public final static Integer A_SHOWPICKOFDAY = new Integer(68);
	public final static Integer A_SHOWDAILYNEWS = new Integer(69);
	public final static Integer A_SHOWHISTORICAL = new Integer(70);
	//public final static Integer A_SHOWQUEUEORDER = new Integer(71);
	public final static Integer A_SHOWSTWOSTOCKCOMPARISON = new Integer(72);
	public final static Integer A_SHOWSTOCKCOMPARISONBYSECTOR = new Integer(73);
	public final static Integer A_SHOWANNOUNCEMENT = new Integer(74);
	public final static Integer A_DELETE = new Integer(75);
	public final static Integer A_SHOWSTOCKDETAIL = new Integer(71);	
	public final static Integer A_SHOWDOWNLOADMARKETINFO = new Integer(76);
	public final static Integer A_SHOWLINKACCOUNT = new Integer(77);
	public final static Integer A_SHOWLOGINSIMASPOL = new Integer(78);
	public final static Integer A_SHOWLIVETRADEBROKER = new Integer(79);
		
	

	public FeedAction(IApp apps) {
		super(apps);
		fc = new JFileChooser(new File("layout/"));
		fc.addChoosableFileFilter(new FileFilter() {
			public boolean accept(File file) {
				String filename = file.getName();
				return filename.endsWith(".scr");
			}

			public String getDescription() {
				return "*.scr";
			}
		});
		fc.enableInputMethods(false);
		fc.setAcceptAllFileFilterUsed(false);
		fc.setCurrentDirectory(new File("layout/"));
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fc.setMultiSelectionEnabled(false);
		disableButton(fc);
		//#Valdhy20150108
		try {
					AutoDeleteSession();
		} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
		}
	}

	

	public static void disableButton(Container c) {
		int len = c.getComponentCount();
		for (int i = 0; i < len; i++) {
			Component comp = c.getComponent(i);
			if (comp instanceof JButton) {
				JButton b = (JButton) comp;
				Icon icon = b.getIcon();
				if (icon != null)
					b.setVisible(false);
			} else if (comp instanceof JComboBox) {
				comp.setEnabled(false);
			} else if (comp instanceof JToggleButton) {
				comp.setVisible(false);
			} else if (comp instanceof Container) {
				disableButton((Container) comp);
			}
		}
	}

	public final static String getFileName(String name) {
		int i = name.lastIndexOf('.');
		if (i > 0 && i < name.length() - 1) {
			return name.substring(0, i);
		}
		return name;
	}

	public void buildAction() {
		//String struserid = ((IEQTradeApp) apps).getFeedEngine().getUserId();
		Calendar cal = Calendar.getInstance();
		Calendar currentcal = Calendar.getInstance();
		
		cal.set(2012, Calendar.MAY, 20);
		currentcal.set(currentcal.get(Calendar.YEAR),
		currentcal.get(Calendar.MONTH), currentcal.get(Calendar.DAY_OF_MONTH));
		
		
		map.put(A_DELETE, new  Action(A_DELETE,"Delete Session", 
				new  MenuProperty(0, "Session", 2.1, false, false),
				FeedApplication.CONS_CORE) {
				private  static  final  long  serialVersionUID  = 1L;
				@Override
				public  void  actionPerformed(ActionEvent arg0) {
				/*File file = new  File("data\\session");
				File file1 = new  File("data\\static");

				deleteCache(file);
				deleteCache(file1);
				JOptionPane.showMessageDialog(null, "Delete Session & Static Berhasil", "Delete Session", JOptionPane.INFORMATION_MESSAGE);
				 */
					int result = 0;
					try{
						result = Utils.showConfirmDialog(null,
								"Confirmation", 
								" Need Exit to Delete Session ? "
								, 0);				
					}catch (Exception ex) {
								//result = 0;
								return;
					}
				
					if(result == 0){
							try {
								if (FeedAction.this.getState() != Action.C_NOTREADY_STATE) {
									apps.getUI().save();
									((IEQTradeApp) apps).getFeedEngine().stop();
									((IEQTradeApp) apps).getFeedEngine().logout("","",0);// yosep other device
									FeedSetting.saveLayout("_default");									
								}
								//System.exit(-1);
							} catch (Exception ex) {
								//System.out.println("catch");
								return;
								
								//System.exit(-1);
							}
							try {
								if (((FeedApplication) apps).getTrading().getAction().getState() != Action.C_NOTREADY_STATE) {
									((FeedApplication) apps).getTrading().getUI().save();
									((IEQTradeApp) apps).getTradingEngine().stop();
									((IEQTradeApp) apps).getTradingEngine().logout();
									TradingSetting.saveLayout("_tradingdefault");
								}
							} catch (Exception ex) {
								return;
							}
							
							try{
								File file = new  File("data\\session");
								File file1 = new  File("data\\static");

								deleteCache(file);
								deleteCache(file1);
								JOptionPane.showMessageDialog(null, "Delete Session & Static Berhasil", "Delete Session", JOptionPane.INFORMATION_MESSAGE);

							} catch (Exception ex){
								return;
							}
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									// Runtime.getRuntime().exit(0); 20140116
									//Runtime.getRuntime().exit(-1);
									
									Runtime.getRuntime().halt(0);
									System.exit(-1);
									//System.exit(-1);
									
								}
							});
						}
				}
		});
		map.put(A_SAVE, new Action(A_SAVE, "Save workspace", new MenuProperty(
				0, "Session", 9998, true, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				fc.setCurrentDirectory(new File("data/layout/"));
				int result = fc.showSaveDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					apps.getUI().save();
					((FeedApplication) apps).getTrading().getUI().save();
					File selFile = fc.getSelectedFile();
					/*System.out.println("saving workspace name: "
							+ getFileName(selFile.getName()));*/
					if (selFile != null) {
						FeedSetting.saveLayout(getFileName(selFile.getName()));
						TradingSetting.saveLayout(getFileName(selFile.getName()));
					}
				}
			}
		});
		map.put(A_LOAD, new Action(A_LOAD, "Open workspace", new MenuProperty(
				0, "Session", 9999, false, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				fc.setCurrentDirectory(new File("data/layout/"));
				int result = fc.showOpenDialog(null);
				if (result == JFileChooser.APPROVE_OPTION) {
					File selFile = fc.getSelectedFile();
					File fTest = new File("data/layout/"
							+ getFileName(selFile.getName()) + ".scr");
					if (fTest != null && fTest.exists()) {
						/*System.out.println("loading workspace name: "
								+ getFileName(selFile.getName()));*/
						apps.getUI().close();
						FeedSetting.loadLayout(getFileName(selFile.getName()));
						apps.getUI().load();

						((FeedApplication) apps).getTrading().getUI().close();
						TradingSetting.loadLayout(getFileName(selFile.getName()));
						((FeedApplication) apps).getTrading().getUI().load();
					}
				}
			}
		});
		map.put(A_EXIT,
				new Action(A_EXIT, "Exit", KeyStroke.getKeyStroke("ctrl X"),
						new MenuProperty(0, "Session", 10000, true, false),
						VolluxApp.CONS_CORE) {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent event) {
						int result = 0;
						try {
							result = Utils.showConfirmDialog(null,
									"Confirmation", " Exit Application ? ", 0);
						} catch (Exception ex) {
							//result = 0;
							return;
						}
						//System.out.println("result : "+result);
						if (result == 0) {
							try {
								if (FeedAction.this.getState() != Action.C_NOTREADY_STATE) {
									apps.getUI().save();
									((IEQTradeApp) apps).getFeedEngine().stop();
									((IEQTradeApp) apps).getFeedEngine().logout("","",0);// yosep other device
									FeedSetting.saveLayout("_default");									
								}
								//System.exit(-1);
							} catch (Exception ex) {
								//System.out.println("catch");
								return;
								
								//System.exit(-1);
							}
							try {
								if (((FeedApplication) apps).getTrading().getAction().getState() != Action.C_NOTREADY_STATE) {
									((FeedApplication) apps).getTrading().getUI().save();
									((IEQTradeApp) apps).getTradingEngine().stop();
									((IEQTradeApp) apps).getTradingEngine().logout();
									TradingSetting.saveLayout("_tradingdefault");
								}
							} catch (Exception ex) {
								return;
							}
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									// Runtime.getRuntime().exit(0); 20140116
									//Runtime.getRuntime().exit(-1);
									
									Runtime.getRuntime().halt(0);
									System.exit(-1);
									//System.exit(-1);
									
								}
							});
						}
					}

					public void setState(int newState) {
						if (newState == C_LOCKED_STATE) {
							this.setEnabled(false);
						} else if (newState == C_DISCONNECTED_STATE) {
							this.setEnabled(true);
						} else {
							this.setEnabled(newState == C_READY_STATE ? true
									: true);
						}
					}
				});
		// map.put(A_SHOWLOCK, new Action(A_SHOWLOCK, "Lock/Unlock",
		// KeyStroke.getKeyStroke("ctrl L"), FeedApplication.CONS_CORE) {
		// private static final long serialVersionUID = 1L;
		// public void actionPerformed(ActionEvent event){
		// //eventDispatcher.pushEvent(new Event(RulePlugin.E_SHOW_LOCK));
		// }
		// public void setState(int newState){
		// if (newState == C_LOCKED_STATE){
		// this.setEnabled(true);
		// } else if (newState == C_DISCONNECTED_STATE){
		// this.setEnabled(true);
		// } else {
		// this.setEnabled(newState == C_READY_STATE ? true : true);
		// }
		// }
		// });
		if(cal.after(currentcal)) {
			map.put(A_SHOWNGQUOTE, new Action(A_SHOWNGQUOTE, "Negotation Quote",
					new MenuProperty(3, "*Stock Analysis", 1, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_NGQUOTE);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWBESTQUOTE, new Action(A_SHOWBESTQUOTE, "Best Quote",
					KeyStroke.getKeyStroke("ctrl Q"), 
					new MenuProperty(3, "*Stock Analysis", 0, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						int max = ((UIMultiBestQuote) apps.getUI().getForm(FeedUI.UI_BESTQUOTE)).getCountBestQuote();
						if (max <8 ) {
							apps.getUI().showUI(FeedUI.UI_BESTQUOTE);	
						} else {
							Utils.showMessage(" You can only open max 8 Bestquote ", null);
						}
					} else
						showLogin();
				}
			});	
			map.put(A_SHOWSELECTEDQUOTE, new Action(A_SHOWSELECTEDQUOTE, "Selected Quote", 
					new MenuProperty(3,"*Stock Analysis|Stock Summary", 0, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_SELECTEDQUOTE);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWQUOTESUMM, new Action(A_SHOWQUOTESUMM, "Quote Summary",
					new MenuProperty(3, "*Stock Analysis|Stock Summary", 1, false,false), 
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_QUOTESUMMARY);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWINVMARKET, new Action(A_SHOWINVMARKET, "Foreign/Domestic By Market", 
					new MenuProperty(3,"*Stock Analysis", 2, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_INVMARKET);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWINVSTOCK, new Action(A_SHOWINVSTOCK,
					"Foreign/Domestic By Stock", new MenuProperty(3,
							"*Stock Analysis", 3, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_INVSTOCK);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWBROKERSUMM, new Action(A_SHOWBROKERSUMM,
					"Buy/Sell Broker Summary", new MenuProperty(3,
							"*Stock Analysis", 4, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_BROKERSUMMARY);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWBROKERACTIVITY, new Action(A_SHOWBROKERACTIVITY,
					"Broker Activity by Stock", new MenuProperty(3,
							"*Stock Analysis", 5, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_BROKERACTIVITY);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWCOMPANY, new Action(A_SHOWCOMPANY, "Company Profile",
					new MenuProperty(3, "*Stock Analysis", 6, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					try {
						// com.vollux.framework.Utils.openBrowser("http://www.sinarmassekuritas.co.id/emiten/");
						if (isReadyState()) {
							apps.getUI().showUI(FeedUI.UI_COMPANYPROFILE);
						} else
							showLogin();
					} catch (Exception ex) {
						Utils.showMessage("cannot retrieve data", null);
					}
				}
			});
			map.put(A_SHOWINTRADAY, new Action(A_SHOWINTRADAY, "Intraday Chart",
					new MenuProperty(3, "*Stock Analysis", 7, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_INTRADAY);
					} else
						showLogin();
				}
			});
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//Stock Comparison
			map.put(A_SHOWSTWOSTOCKCOMPARISON, new Action(A_SHOWSTWOSTOCKCOMPARISON,"*By Company Profile",
					new MenuProperty(3, "*Stock Analysis|*Stock Comparison", 1, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent arg0) {
					if(isReadyState()){
						apps.getUI().showUI(FeedUI.UI_STOCKCOMPARISON2);
					}else
						showLogin();				
				}
			});
			map.put(A_SHOWSTOCKCOMPARISONBYSECTOR, new Action(A_SHOWSTOCKCOMPARISONBYSECTOR,"*By Financial Data",
					new MenuProperty(3, "*Stock Analysis|*Stock Comparison", 1, false,false),
					FeedApplication.CONS_FEED) {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(isReadyState()){
						apps.getUI().showUI(FeedUI.UI_STOCKCOMPARISONBYSECTOR);
					}else
						showLogin();			
				}
			});
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//Stock Comparison
			map.put(A_SHOWSTOCKDETAIL, new Action(A_SHOWSTOCKDETAIL, "Stock Detail",
					new MenuProperty(3, "Stock Analysis", 8, false, false),
					FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;
			
			public void actionPerformed(ActionEvent event) {
				try {
					//System.out.println("try1");
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_STOCKDETAIL);
					} else
						showLogin();
				}
				catch (Exception ex) {
					Utils.showMessage("cannot retrieve data", null);
				}					
			}
		});
		}
		else
		{
			map.put(A_SHOWNGQUOTE, new Action(A_SHOWNGQUOTE, "Negotation Quote",
					new MenuProperty(3, "Stock Analysis", 1, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_NGQUOTE);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWBESTQUOTE, new Action(A_SHOWBESTQUOTE, "Best Quote",
					KeyStroke.getKeyStroke("ctrl Q"), 
					new MenuProperty(3, "Stock Analysis", 0, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						int max = ((UIMultiBestQuote) apps.getUI().getForm(FeedUI.UI_BESTQUOTE)).getCountBestQuote();
						if (max <8 ) {
							apps.getUI().showUI(FeedUI.UI_BESTQUOTE);	
						} else {
							Utils.showMessage(" You can only open max 8 Bestquote ", null);
						}
					} else
						showLogin();
				}
			});	
			map.put(A_SHOWSELECTEDQUOTE, new Action(A_SHOWSELECTEDQUOTE, "Selected Quote", 
					new MenuProperty(3,"Stock Analysis|Stock Summary", 0, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_SELECTEDQUOTE);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWQUOTESUMM, new Action(A_SHOWQUOTESUMM, "Quote Summary",
					new MenuProperty(3, "Stock Analysis|Stock Summary", 1, false,false), 
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_QUOTESUMMARY);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWINVMARKET, new Action(A_SHOWINVMARKET, "Foreign/Domestic By Market", 
					new MenuProperty(3,"Stock Analysis", 2, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_INVMARKET);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWINVSTOCK, new Action(A_SHOWINVSTOCK,
					"Foreign/Domestic By Stock", new MenuProperty(3,
							"Stock Analysis", 3, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_INVSTOCK);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWBROKERSUMM, new Action(A_SHOWBROKERSUMM,
					"Buy/Sell Broker Summary", new MenuProperty(3,
							"Stock Analysis", 4, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_BROKERSUMMARY);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWBROKERACTIVITY, new Action(A_SHOWBROKERACTIVITY,
					"Broker Activity by Stock", new MenuProperty(3,
							"Stock Analysis", 5, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_BROKERACTIVITY);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWCOMPANY, new Action(A_SHOWCOMPANY, "Company Profile",
					new MenuProperty(3, "Stock Analysis", 6, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					try {
						if (isReadyState()) {
							apps.getUI().showUI(FeedUI.UI_COMPANYPROFILE);
						} else
							showLogin();
					} catch (Exception ex) {
						Utils.showMessage("cannot retrieve data", null);
					}
				}
			});
			map.put(A_SHOWINTRADAY, new Action(A_SHOWINTRADAY, "Intraday Chart",
					new MenuProperty(3, "Stock Analysis", 7, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_INTRADAY);
					} else
						showLogin();
				}
			});
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//Stock Comparison
			map.put(A_SHOWSTWOSTOCKCOMPARISON, new Action(A_SHOWSTWOSTOCKCOMPARISON,"By Company Profile",
					new MenuProperty(3, "Stock Analysis|Stock Comparison", 1, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent arg0) {
					if(isReadyState()){
						apps.getUI().showUI(FeedUI.UI_STOCKCOMPARISON2);
					}else
						showLogin();				
				}
			});
			map.put(A_SHOWSTOCKCOMPARISONBYSECTOR, new Action(A_SHOWSTOCKCOMPARISONBYSECTOR,"By Financial Data",
					new MenuProperty(3, "Stock Analysis|Stock Comparison", 1, false,false),
					FeedApplication.CONS_FEED) {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(isReadyState()){
						apps.getUI().showUI(FeedUI.UI_STOCKCOMPARISONBYSECTOR);
					}else
						showLogin();			
				}
			});
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//Stock Comparison	
			
		map.put(A_SHOWSTOCKDETAIL, new Action(A_SHOWSTOCKDETAIL, "Stock Detail",
					new MenuProperty(3, "Stock Analysis", 8, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
			
				public void actionPerformed(ActionEvent event) {
					//System.out.println("try2");
					try {
						if (isReadyState()) {
							apps.getUI().showUI(FeedUI.UI_STOCKDETAIL);
						} else
							showLogin();
					}
					catch (Exception ex) {
						Utils.showMessage("cannot retrieve data", null);
						ex.printStackTrace();
					}					
				}
			});
		}
		
		
		map.put(A_SHOWLIVETRADE, new Action(A_SHOWLIVETRADE, "Live Trade",
				KeyStroke.getKeyStroke("ctrl L"), new MenuProperty(4,
						"Market Analysis", 0, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_LIVETRADE);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWLIVETRADESTOCK, new Action(A_SHOWLIVETRADESTOCK,
				"Live Trade By Stock", new MenuProperty(4, "Market Analysis",
						1, false, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_LIVETRADESTOCK);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWLIVETRADEBROKER, new Action(A_SHOWLIVETRADEBROKER,
				"Live Trade By Broker", new MenuProperty(4, "Market Analysis",
						2, false, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_LIVETRADEBROKER);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWMARKETSUMM, new Action(A_SHOWMARKETSUMM,
				"Market Chart & Detail", KeyStroke.getKeyStroke("ctrl M"),
				new MenuProperty(4, "Market Analysis", 3, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_MARKETSUMMARY);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWCHART, new Action(A_SHOWCHART, "Technical Charting",
				KeyStroke.getKeyStroke("ctrl C"), new MenuProperty(4,
						"Market Analysis", 4, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_CHART);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWCHARTCOMPARE, new Action(A_SHOWCHARTCOMPARE,
				"Chart Comparison", new MenuProperty(4, "Market Analysis", 5.1,
						false, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_CHARTCOMPARE);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWTOPBROKER, new Action(A_SHOWTOPBROKER, "Top Broker",
				new MenuProperty(4, "Market Analysis", 6, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_TOPBROKER);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWTOPSTOCK, new Action(A_SHOWTOPSTOCK, "Top Stock",
				new MenuProperty(4, "Market Analysis", 7, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_TOPSTOCK);
				} else
					showLogin();

			}
		});
		map.put(A_SHOWSTOCKSUMMARY, new Action(A_SHOWSTOCKSUMMARY,
				"Buy/Sell Stock Summary", new MenuProperty(4,
						"Market Analysis", 8, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_STOCKSUMMARY);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWSTOCKACTIVITY, new Action(A_SHOWSTOCKACTIVITY,
				"Stock Activity by Broker", new MenuProperty(4,
						"Market Analysis", 9, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_STOCKACTIVITY);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWTIMETRADE, new Action(A_SHOWTIMETRADE,
				"Time & Trade Summary", new MenuProperty(4, "Market Analysis",
						10, false, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_TIMETRADE);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWTIMETRADEPRICE, new Action(A_SHOWTIMETRADEPRICE,
				"Time & Trade Summary By Price", new MenuProperty(4,
						"Market Analysis", 11, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_TIMETRADEPRICE);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWINDICES, new Action(A_SHOWINDICES, "Indices Sectoral",
				KeyStroke.getKeyStroke("ctrl I"), new MenuProperty(4,
						"Market Analysis", 12, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_INDICES);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWWORLDIDXCURR, new Action(A_SHOWWORLDIDXCURR,
				"World Indices & Exchange Rate", new MenuProperty(4,
						"Market Analysis", 13, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_WORLDIDXCURR);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWSTOCKLIST, new Action(A_SHOWSTOCKLIST, "Stock List",
				new MenuProperty(4, "Market Analysis", 14, true, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_STOCKLIST);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWBROKERLIST, new Action(A_SHOWBROKERLIST, "Broker List",
				new MenuProperty(4, "Market Analysis", 15, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_BROKERLIST);
				} else
					showLogin();
			}
		});
		
		
		if(cal.after(currentcal)) {
				
				map.put(A_SHOWGLOBALNEWS, new Action(A_SHOWGLOBALNEWS,"*News", 
						new MenuProperty(5, "*News", 0, false,
								false), FeedApplication.CONS_FEED) {
					private static final long serialVersionUID = 1L;
		
					public void actionPerformed(ActionEvent event) {
						if (isReadyState()) {
							apps.getUI().showUI(FeedUI.UI_GLOBALNEWS);
						} else
							showLogin();
					}
				});
				map.put(A_SHOWTICKER, new Action(A_SHOWTICKER, "Ticker",
						new MenuProperty(5, "*News", 1, false, false),
						FeedApplication.CONS_FEED) {
					private static final long serialVersionUID = 1L;
		
					public void actionPerformed(ActionEvent event) {
						if (isReadyState()) {
							apps.getUI().showUI(FeedUI.UI_TICKER);
						} else
							showLogin();
					}
				});
				map.put(A_SHOWCORPACTION, new Action(A_SHOWCORPACTION, "Corp. Action",
						new MenuProperty(5, "*News", 2, false, false),
						FeedApplication.CONS_FEED) {
					private static final long serialVersionUID = 1L;
		
					public void actionPerformed(ActionEvent event) {
						if (isReadyState()) {
							apps.getUI().showUI(FeedUI.UI_CORPACTION);
						} else
							showLogin();
					}
				});
				map.put(A_SHOWDOWNLOADMARKETINFO, new Action(A_SHOWDOWNLOADMARKETINFO, "Download Market Info",
						new MenuProperty(5, "News", 3, false, false),
						FeedApplication.CONS_FEED) {
					private static final long serialVersionUID = 1L;
		
					public void actionPerformed(ActionEvent event) {
						if (isReadyState()) {
							apps.getUI().showUI(FeedUI.UI_DOWNLOADMARKETINFO);
						} else
							showLogin();
					}
				});
		}
		else
		{
			map.put(A_SHOWGLOBALNEWS, new Action(A_SHOWGLOBALNEWS,"News", 
					new MenuProperty(5, "News", 0, false,
							false), FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_GLOBALNEWS);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWTICKER, new Action(A_SHOWTICKER, "Ticker",
					new MenuProperty(5, "News", 1, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_TICKER);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWCORPACTION, new Action(A_SHOWCORPACTION, "Corp. Action",
					new MenuProperty(5, "News", 2, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_CORPACTION);
					} else
						showLogin();
				}
			});
			map.put(A_SHOWANNOUNCEMENT, new Action(A_SHOWANNOUNCEMENT,"Announcement",
					new MenuProperty(5, "News", 3, false, false),
					TradingApplication.CONS_TRADING) {
				private static final long serialVersionUID = 1L;
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					if(isReadyState()){
						apps.getUI().showUI(FeedUI.UI_ANNOUNCEMENT);
						
					}else{
						showLogin();
					}
						
					
				}
			});
				map.put(A_SHOWDOWNLOADMARKETINFO, new Action(A_SHOWDOWNLOADMARKETINFO, "Download Market Info",
					new MenuProperty(5, "News", 4, false, false),
					FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_DOWNLOADMARKETINFO);
					} else
						showLogin();
				}
			});
		}
		
		map.put(A_SHOWPICKOFDAY, new Action(A_SHOWPICKOFDAY, "Pick Of The Day",
				new MenuProperty(6, "Research", 0, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_PICKOFTHEDAY);
				} else
					showLogin();
			}
		}); 
		map.put(A_SHOWDAILYNEWS, new Action(A_SHOWDAILYNEWS, "Daily News",
				new MenuProperty(6, "Research", 1, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_DAILYNEWS);
				} else
					showLogin();
			}
		});
		//YOSEP REMOVE HISTORICAL 300915
		/*map.put(A_SHOWHISTORICAL, new Action(A_SHOWHISTORICAL, "Historical Research",
				new MenuProperty(6, "Research", 2, true, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_HISTORICAL);
					
				} else
					showLogin();
			}
		});*/
		
		map.put(A_SHOWLINKACCOUNT, new Action(A_SHOWLINKACCOUNT, "Link Account",
				new MenuProperty(7, "Reksa Dana", 0, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_LINKACCOUNT);
					
				} else
					showLogin();
			}
		});
		map.put(A_SHOWLOGINSIMASPOL, new Action(A_SHOWLOGINSIMASPOL, "Login Simas ROL",
				new MenuProperty(7, "Reksa Dana", 1, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_LOGINSIMASROLL);
					
				} else
					showLogin();
			}
		});
		/*
		map.put(A_SHOWDAILYNEWS, new Action(A_SHOWDAILYNEWS, "Daily News",
				new MenuProperty(6, "Research", 1, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				try {
					java.io.BufferedInputStream bis = null;
			        try {
			            Calendar cal=Calendar.getInstance();
			            Calendar cal1=Calendar.getInstance();
			            Calendar cal2=Calendar.getInstance();
			            Calendar cal3=Calendar.getInstance();
			            //int year=cal.get(Calendar.YEAR);
			            Date date = new Date();
			            cal1.add(Calendar.DATE, -1);
			            cal2.add(Calendar.DATE, -2);
			            cal3.add(Calendar.DATE, -3);
			            //SimpleDateFormat sdf;
			         //   cal.set(Calendar.YEAR,Date);
			           int day=cal.get(Calendar.DAY_OF_WEEK);
			            int daybefore=day-1;
			            if (daybefore==0){
			            	daybefore=3;
			            }else if(daybefore==6){
			            	daybefore=2;
			            }else {
			            	daybefore=1;
			            }
			            String nama_tahun="Tahun%20" + new SimpleDateFormat("yyyy").format(date);
			            String nama_bulan=new SimpleDateFormat("MMM").format(date);
			            String nama_folder=new SimpleDateFormat("yyyyMMdd").format(date);
			            String nama_file =new SimpleDateFormat("yyyyMMdd").format(date)+"%20Daily%20Research.pdf";
			            String nama_direktori="C:/Simasnet.v2/research/www.sinarmassekuritas.co.id/info/research/"+"Tahun " + new SimpleDateFormat("yyyy").format(date)+"/"+nama_bulan+"/"+nama_folder;
			            bis = new java.io.BufferedInputStream(new java.net.URL("http://www.sinarmassekuritas.co.id/info/research/"+nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file).openStream());
			            
			            boolean exists = (new File(nama_direktori)).exists();
			                        if (!exists) {
			                            try {
			                                (new File(nama_direktori)).mkdirs();
			                            } catch (Exception e) {

			                                System.exit(103);
			                                //e.printStackTrace();
			                            }
			                        }
			                        
			            java.io.FileOutputStream fos = new java.io.FileOutputStream(nama_direktori+"/"+nama_file);
			            if (!(new File(nama_direktori+"/"+nama_file)).exists()){
			            	if (daybefore!=2 && daybefore!=3){
			            	nama_file =new SimpleDateFormat("yyyyMMdd").format(cal)+"%20Daily%20Research.pdf";
			            	}else if(daybefore==2){
			            		nama_file =new SimpleDateFormat("yyyyMMdd").format(cal2)+"%20Daily%20Research.pdf";
			            	}else if(daybefore==3){
			            		nama_file =new SimpleDateFormat("yyyyMMdd").format(cal3)+"%20Daily%20Research.pdf";
			            	}
			            }
			            java.io.BufferedOutputStream bos = new java.io.BufferedOutputStream(fos, 1024);
			            
			            byte[] data = new byte[1024];
			            int x=0;
			            while((x=bis.read(data,0,1024))>=0) {
			                bos.write(data,0,x);
			            }
			            bos.close();
			            bis.close();
			            fos.close();


			            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+ nama_direktori+"/"+nama_file);
			        } catch (Exception ex) {
			    		ex.printStackTrace();
			    	  }
				} catch (Exception ex) {
					ex.printStackTrace();
					Utils.showMessage("failed to open user manual document",
							null);
				}
			}

			public void setState(int newState) {
				this.setEnabled(true);
			}
});*/
		map.put(A_SHOWCOLOUR, new Action(A_SHOWCOLOUR,
				"Define Colours & Font Feed", new MenuProperty(8, "Tools", 0,
						false, false), FeedApplication.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().showUI(FeedUI.UI_COLOUR);
				} else
					showLogin();
			}
		});
		map.put(A_SHOWSHORTCUT, new Action(A_SHOWSHORTCUT,
				"Define Custom Shortcut", new MenuProperty(8, "Tools", 1,
						false, false), FeedApplication.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				apps.getUI().showUI(FeedUI.UI_DLGSHORTCUT);
			}

			public void setState(int newState) {
				setEnabled(true);
			}
		});
		map.put(A_RESTOREALL, new Action(A_RESTOREALL, "Restore All",
				new MenuProperty(8, "Tools", 1.2, true, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().restoreAll();
					((FeedApplication) apps).getTrading().getUI().restoreAll();
				} else
					showLogin();
			}
		});
		map.put(A_MINIMIZEALL, new Action(A_MINIMIZEALL, "Minimize All",
				new MenuProperty(8, "Tools", 1.4, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				if (isReadyState()) {
					apps.getUI().minimizeAll();
					((FeedApplication) apps).getTrading().getUI().minimizeAll();
				} else
					showLogin();
			}
		});
		map.put(A_SHOWCALC, new Action(A_SHOWCALC, "Calculator",
				new MenuProperty(8, "Tools", 2, true, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				try {
					Runtime.getRuntime().exec("calc");
				} catch (Exception ex) {
					Utils.showMessage("cannot open calculator application",
							null);
				}
			}
		});
		//map.put(A_SHOWUSERMANUAL, new Action(A_SHOWUSERMANUAL, "User Manual",
		//		new MenuProperty(7, "My Account", 1, false, false),
		//		FeedApplication.CONS_FEED) {
		//	private static final long serialVersionUID = 1L;

		//	public void actionPerformed(ActionEvent event) {
		//		try {
		//			Runtime.getRuntime().exec(
		//					"rundll32 url.dll,FileProtocolHandler "
		//							+ "usermanual.pdf");
		//		} catch (Exception ex) {
		//			ex.printStackTrace();
		//			Utils.showMessage("failed to open user manual document",
		//					null);
		//		}
		//	}

		//	public void setState(int newState) {
		//		this.setEnabled(true);
		//	}
		//});
		//map.put(A_SHOWUSERGUIDE, new Action(A_SHOWUSERGUIDE, "User Guide",
		//		new MenuProperty(7, "My Account", 2, false, false),
		//		FeedApplication.CONS_FEED) {
		//	private static final long serialVersionUID = 1L;

		//	public void actionPerformed(ActionEvent event) {
		//		try {
		//			Runtime.getRuntime().exec(
		//					"rundll32 url.dll,FileProtocolHandler "
		//							+ "userguide.pdf");
		//		} catch (Exception ex) {
		//			ex.printStackTrace();
		//			Utils.showMessage("failed to open user guide document",
		//					null);
		//		}
		//	}

		//	public void setState(int newState) {
		//		this.setEnabled(true);
		//	}
		//});
		map.put(A_SHOWCASHDEPOSIT, new Action(A_SHOWCASHDEPOSIT, "Cash Management",
				new MenuProperty(9, "My Account", 1, false, false),
				FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				try {
					// com.vollux.framework.Utils.openBrowser("http://www.sinarmassekuritas.co.id/emiten/");
					
					
					if (isReadyState()) {
						//HashMap param = new HashMap();
						//param.put("TYPE", "DEPOSIT");
						apps.getUI().showUI(FeedUI.UI_CASHMANAGEMENT);
						//apps.getUI().showUI(FeedUI.UI_CASHMANAGEMENT);
					} else
						showLogin();
				} catch (Exception ex) {
					Utils.showMessage("cannot retrieve data", null);
				}
			}
		});
	/*	map.put(A_SHOWCASHDEPOSIT, new Action(A_SHOWCASHDEPOSIT,"Cash Deposit", 
				new MenuProperty(7, "My Account|Cash Management", 1, false, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				try {
					com.vollux.framework.Utils.openBrowser("http://trading.simasnet.com/cash_management/cash_deposit.php?uid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId());
				} catch (Exception ex) {
					Utils.showMessage(
							"cannot open Browser",
							null);
				}
			}
					public void setState(int newState) {
						this.setEnabled(true);
					}
		});
		
		map.put(A_SHOWCASHWITH, new Action(A_SHOWCASHWITH,"Cash Withdrawal", 
				new MenuProperty(7, "My Account|Cash Management", 2, false, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				try {
					com.vollux.framework.Utils.openBrowser("http://trading.simasnet.com/cash_management/cash_withdraw.php?uid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId());
				} catch (Exception ex) {
					Utils.showMessage(
							"cannot open Browser",
							null);
				}
			}
					public void setState(int newState) {
						this.setEnabled(true);
					}
		});
		
		map.put(A_SHOWCASHWITH, new Action(A_SHOWCASHWITH,"Cash Withdrawal", 
				new MenuProperty(8, "My Account|Cash Management", 2, false, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				try {
					// com.vollux.framework.Utils.openBrowser("http://www.sinarmassekuritas.co.id/emiten/");
					if (isReadyState()) {
						//apps.getUI().showUI(FeedUI.UI_CASHWITHDRAW);
						HashMap param = new HashMap();
						param.put("TYPE", "WITHDRAW");
						apps.getUI().showUI(FeedUI.UI_CASHMANAGEMENT, param);
					} else
						showLogin();
				} catch (Exception ex) {
					Utils.showMessage("cannot retrieve data", null);
				}
			}
		}); */
		map.put(A_SHOWREPORT, new Action(A_SHOWREPORT,"Reporting", 
				new MenuProperty(8, "My Account", 2, true, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				try {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_REPORTING);
					} else
						showLogin();
					//com.vollux.framework.Utils.openBrowser("http://trading.simasnet.com/report/index.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId());
				} catch (Exception ex) {
					Utils.showMessage(
							"cannot open Browser",
							null);
				}
			}
					public void setState(int newState) {
						this.setEnabled(true);
					}
		});
		/*
		map.put(A_SHOWREPORT, new Action(A_SHOWREPORT,"Reporting", 
				new MenuProperty(8, "My Account", 3, true, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				try {
					if (isReadyState()) {
						apps.getUI().showUI(FeedUI.UI_REPORTING);
					} else
						showLogin();
				} catch (Exception ex) {
					Utils.showMessage(
							"cannot open Browser",
							null);
				}
			}
					public void setState(int newState) {
						this.setEnabled(true);
					}
		});
		*/
		
		if(cal.after(currentcal)) {
			map.put(A_HELP, new Action(A_HELP,"User Manual", 
					new MenuProperty(9, "My Account|*Help", 3, false, false), FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;
	
				public void actionPerformed(ActionEvent event) {
							try {
								Runtime.getRuntime().exec(
										"rundll32 url.dll,FileProtocolHandler "
												+ "usermanual.pdf");
							} catch (Exception ex) {
								ex.printStackTrace();
								Utils.showMessage("failed to open user manual document",
										null);
							}
						}
	
						public void setState(int newState) {
							this.setEnabled(true);
						}
			});
			
//			map.put(A_SHOWYMCS1, new Action(A_SHOWYMCS1,"Customer Service 1", 
//					new MenuProperty(9, "My Account|*Help", 4, true, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//	
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?cs.sms1");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//	
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMCS2, new Action(A_SHOWYMCS2,"Customer Service 2", 
//					new MenuProperty(9, "My Account|*Help", 5, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//	
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?cs.sms2");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//	
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMCS3, new Action(A_SHOWYMCS3,"*Helpdesk 1", 
//					new MenuProperty(9, "My Account|*Help", 6, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//	
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?hd.sms1");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//	
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMCS4, new Action(A_SHOWYMCS4,"*Helpdesk 2", 
//					new MenuProperty(9, "My Account|*Help", 7, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//	
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?hd.sms2");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//	
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMRS1, new Action(A_SHOWYMRS1,"Research 1", 
//					new MenuProperty(9, "My Account|*Help", 8, true, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//	
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?smsresearch1");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//	
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMRS2, new Action(A_SHOWYMRS2,"Research 2", 
//					new MenuProperty(9, "My Account|*Help", 9, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//	
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?smsresearch2");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//	
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMRS3, new Action(A_SHOWYMRS3,"Research 3", 
//					new MenuProperty(9, "My Account|*Help", 10, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//	
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?smsresearch3");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//	
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMRS4, new Action(A_SHOWYMRS4,"Research 4", 
//					new MenuProperty(9, "My Account|*Help", 11, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//	
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?smsresearch4");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//				
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//				
//				map.put(A_SHOWYMRS5, new Action(A_SHOWYMRS5,"Research 5", 
//						new MenuProperty(9, "My Account|*Help", 12, false, false), FeedApplication.CONS_FEED) {
//					private static final long serialVersionUID = 1L;
//	
//					public void actionPerformed(ActionEvent event) {
//						try {
//							com.vollux.framework.Utils
//									.openBrowser("ymsgr:sendIM?smsresearch5");
//						} catch (Exception ex) {
//							Utils.showMessage(
//									"cannot open Yahoo Messenger application", null);
//						}
//					}
//	
//					public void setState(int newState) {
//						this.setEnabled(true);
//					}
//				});
	
				map.put(A_SHOWEMAIL, new Action(A_SHOWEMAIL, "Email Helpdesk",
						new MenuProperty(9, "My Account|*Help", 13, true, false),
						FeedApplication.CONS_CORE) {
					private static final long serialVersionUID = 1L;
	
					public void actionPerformed(ActionEvent event) {
						try {
							if (isReadyState()) {
								apps.getUI().showUI(FeedUI.UI_EMAILHELPDESK);
							} else
								showLogin();
							//com.vollux.framework.Utils.openBrowser("http://www.sinarmassekuritas.co.id/simas.net/contact_Us_v2.asp");
						} catch (Exception ex) {
							Utils.showMessage(
									"cannot open Browser",
									null);
						}
					}
	
					public void setState(int newState) {
						this.setEnabled(true);
					}
				});
				
				map.put(A_SHOWOFFICE, new Action(A_SHOWOFFICE, "Our Offices",
						new MenuProperty(9, "My Account|*Help", 14, false, false),
						FeedApplication.CONS_CORE) {
					private static final long serialVersionUID = 1L;
	
					public void actionPerformed(ActionEvent event) {
						try {
							if (isReadyState()) {
								apps.getUI().showUI(FeedUI.UI_OUROFFICE);
							} else
								showLogin();
							//com.vollux.framework.Utils.openBrowser("http://www.sinarmassekuritas.co.id/gen_cUs_v2.asp");
						} catch (Exception ex) {
							Utils.showMessage(
									"cannot open Browser",
									null);
						}
					}
	
					public void setState(int newState) {
						this.setEnabled(true);
					}
				});
		}
		else
		{
			map.put(A_HELP, new Action(A_HELP,"User Manual", 
					new MenuProperty(9, "My Account|Help", 3, false, false), FeedApplication.CONS_FEED) {
				private static final long serialVersionUID = 1L;

				public void actionPerformed(ActionEvent event) {
							try {
								Runtime.getRuntime().exec(
										"rundll32 url.dll,FileProtocolHandler "
												+ "usermanual.pdf");
							} catch (Exception ex) {
								ex.printStackTrace();
								Utils.showMessage("failed to open user manual document",
										null);
							}
						}

						public void setState(int newState) {
							this.setEnabled(true);
						}
			});
			
//			map.put(A_SHOWYMCS1, new Action(A_SHOWYMCS1,"Customer Service 1", 
//					new MenuProperty(9, "My Account|Help", 4, true, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?cs.sms1");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMCS2, new Action(A_SHOWYMCS2,"Customer Service 2", 
//					new MenuProperty(9, "My Account|Help", 5, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?cs.sms2");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMCS3, new Action(A_SHOWYMCS3,"Helpdesk 1", 
//					new MenuProperty(9, "My Account|Help", 6, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?hd.sms1");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMCS4, new Action(A_SHOWYMCS4,"Helpdesk 2", 
//					new MenuProperty(9, "My Account|Help", 7, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?hd.sms2");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMRS1, new Action(A_SHOWYMRS1,"Research 1", 
//					new MenuProperty(9, "My Account|Help", 8, true, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?smsresearch1");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMRS2, new Action(A_SHOWYMRS2,"Research 2", 
//					new MenuProperty(9, "My Account|Help", 9, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?smsresearch2");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMRS3, new Action(A_SHOWYMRS3,"Research 3", 
//					new MenuProperty(9, "My Account|Help", 10, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?smsresearch3");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//			
//			map.put(A_SHOWYMRS4, new Action(A_SHOWYMRS4,"Research 4", 
//					new MenuProperty(9, "My Account|Help", 11, false, false), FeedApplication.CONS_FEED) {
//				private static final long serialVersionUID = 1L;
//
//				public void actionPerformed(ActionEvent event) {
//					try {
//						com.vollux.framework.Utils
//								.openBrowser("ymsgr:sendIM?smsresearch4");
//					} catch (Exception ex) {
//						Utils.showMessage(
//								"cannot open Yahoo Messenger application", null);
//					}
//				}
//				
//				public void setState(int newState) {
//					this.setEnabled(true);
//				}
//			});
//				
//				map.put(A_SHOWYMRS5, new Action(A_SHOWYMRS5,"Research 5", 
//						new MenuProperty(9, "My Account|Help", 12, false, false), FeedApplication.CONS_FEED) {
//					private static final long serialVersionUID = 1L;
//
//					public void actionPerformed(ActionEvent event) {
//						try {
//							com.vollux.framework.Utils
//									.openBrowser("ymsgr:sendIM?smsresearch5");
//						} catch (Exception ex) {
//							Utils.showMessage(
//									"cannot open Yahoo Messenger application", null);
//						}
//					}
//
//					public void setState(int newState) {
//						this.setEnabled(true);
//					}
//				});

				map.put(A_SHOWEMAIL, new Action(A_SHOWEMAIL, "Email Helpdesk",
						new MenuProperty(9, "My Account|Help", 13, true, false),
						FeedApplication.CONS_CORE) {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent event) {
						try {
							if (isReadyState()) {
								apps.getUI().showUI(FeedUI.UI_EMAILHELPDESK);
							} else
								showLogin();
							//com.vollux.framework.Utils.openBrowser("http://www.sinarmassekuritas.co.id/simas.net/contact_Us_v2.asp");
						} catch (Exception ex) {
							Utils.showMessage(
									"cannot open Browser",
									null);
						}
					}

					public void setState(int newState) {
						this.setEnabled(true);
					}
				});
				
				map.put(A_SHOWOFFICE, new Action(A_SHOWOFFICE, "Our Offices",
						new MenuProperty(9, "My Account|Help", 14, false, false),
						FeedApplication.CONS_CORE) {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent event) {
						try {
							if (isReadyState()) {
								apps.getUI().showUI(FeedUI.UI_OUROFFICE);
							} else
								showLogin();
							//com.vollux.framework.Utils.openBrowser("http://www.sinarmassekuritas.co.id/gen_cUs_v2.asp");
						} catch (Exception ex) {
							Utils.showMessage(
									"cannot open Browser",
									null);
						}
					}

					public void setState(int newState) {
						this.setEnabled(true);
					}
				});

		}
			
		//map.put(A_SHOWCONTACT, new Action(A_SHOWCONTACT,"Contact Us (Yahoo Messenger)", 
		//		new MenuProperty(7, "My Account", 3, false, false), FeedApplication.CONS_FEED) {
		//	private static final long serialVersionUID = 1L;

		//	public void actionPerformed(ActionEvent event) {
		//		try {
		//			com.vollux.framework.Utils
		//					.openBrowser("ymsgr:sendIM?cs.sms2");
		//		} catch (Exception ex) {
		//			Utils.showMessage(
		//					"cannot open Yahoo Messenger application", null);
		//		}
		//	}

		//	public void setState(int newState) {
		//		this.setEnabled(true);
		//	}
		//});
		map.put(A_SHOWABOUT, new Action(A_SHOWABOUT, "About", 
				new MenuProperty(9, "My Account", 15, false, false), FeedApplication.CONS_FEED) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				Utils.showMessage("Simas.net version:" + Encryption.getVersion(), null);
			}

			public void setState(int newState) {
				this.setEnabled(true);
			}
		});

		map.put(A_SHOWLOGIN,
				new Action(A_SHOWLOGIN, "Logon", KeyStroke
						.getKeyStroke("ctrl F"), new MenuProperty(0, "Session",
						0, false, false), FeedApplication.CONS_CORE) {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent event) {
						apps.getUI().showUI(FeedUI.UI_LOGON);
					}

					public void setState(int newState) {
						if (newState == C_LOCKED_STATE) {
							this.setEnabled(false);
						} else if (newState == C_DISCONNECTED_STATE) {
							this.setEnabled(false);
						} else {
							this.setEnabled(newState == C_READY_STATE ? false
									: true);
						}
					}
				});
		map.put(A_SHOWLOGOUT, new Action(A_SHOWLOGOUT, "Logout",
				new MenuProperty(0, "Session", 1, false, false),
				FeedApplication.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				int result = Utils.showConfirmDialog(null, "Confirmation",
						" Logout  Application ? ", 0);
				if (result == 0) {
					((FeedApplication) apps).close();
					((FeedApplication) apps).getTrading().close();
				}
			}

			public void setState(int newState) {
				if (newState == C_LOCKED_STATE) {
					this.setEnabled(false);
				} else if (newState == C_DISCONNECTED_STATE) {
					this.setEnabled(true);
				} else {
					this.setEnabled(newState == C_READY_STATE ? true : false);
				}
			}
		});
		map.put(A_PINON, new Action(A_PINON, "Logon Trading", new MenuProperty(
				0, "Session", 1.1, true, false), FeedApplication.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				((FeedApplication) apps).getTrading().getUI()
						.showUI(TradingUI.UI_PIN);
			}
		});
		map.put(A_PINOFF, new Action(A_PINOFF, "Logout Trading",
				new MenuProperty(0, "Session", 1.2, false, true),
				FeedApplication.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				int result = Utils.showConfirmDialog(null, "Confirmation",
						" Logout Trading ? ", 0);
				if (result == 0) {
					((FeedApplication) apps).getTrading().disableTrading();
				}
			}
		});
		map.put(A_SHOWCHGPASS, new Action(A_SHOWCHGPASS, "Change Password",
				new MenuProperty(0, "Session", 2, false, false),
				FeedApplication.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				((FeedApplication) apps).getTrading().getUI()
						.showUI(TradingUI.UI_CHGPASS);
			}
		});
		map.put(A_SHOWFORGOT, new Action(A_SHOWFORGOT, "Forgot Password",
				new MenuProperty(0, "Session", 2.1, true, true),
				FeedApplication.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				try {
					apps.getUI().showUI(FeedUI.UI_FORGOTPASSWORD);
				} catch (Exception ex) {
					Utils.showMessage("cannot open Browser", null);
				}
			}

			public void setState(int newState) {
				this.setEnabled(true);
			}
		});
		map.put(A_SHOWCONN, new Action(A_SHOWCONN, "Change Feed Connection",
				new MenuProperty(0, "Session", 3, false, false),
				FeedApplication.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				apps.getUI().showUI(FeedUI.UI_CONNECTION);
			}

			public void setState(int newState) {
				if (newState == C_LOCKED_STATE) {
					this.setEnabled(false);
				} else if (newState == C_DISCONNECTED_STATE) {
					this.setEnabled(false);
				} else {
					this.setEnabled(newState == C_READY_STATE ? false : true);
				}
			}
		});
		map.put(A_SHOWRECONNECT, new Action(A_SHOWRECONNECT, "Reconnect Feed",
				new MenuProperty(0, "Session", 5, false, false),
				FeedApplication.CONS_CORE) {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent event) {
				apps.getUI().showUI(FeedUI.UI_RECONNECT);
			}

			public void setState(int newState) {
				if (newState == C_LOCKED_STATE) {
					this.setEnabled(false);
				} else if (newState == C_DISCONNECTED_STATE) {
					this.setEnabled(true);
				} else {
					this.setEnabled(newState == C_READY_STATE ? false : false);
				}
			}
		});
	}

	@Override
	public void setState(int newState) {
		super.setState(newState);
		Iterator i = this.map.keySet().iterator();
		while (i.hasNext()) {
			Integer code = (Integer) i.next();
			Action a = (Action) this.map.get(code);
			if (isValidCode(code))
				a.setState(Action.C_READY_STATE);
		}
	}

	public boolean isValidCode(Integer code) {

		if (code == A_SHOWLOGIN || code == A_SHOWLOGOUT || code == A_PINOFF
				|| code == A_PINON || code == A_SHOWCHGPASS
				|| code == A_SHOWFORGOT || code == A_SHOWCONN || code == A_LOAD || code == A_SAVE)
			return false;

		return true;
	}

	 public  static  void  deleteCache(File folder) {
		    File[] files = folder.listFiles();
		    if(files!=null) { //some JVMs return null for empty dirs
		        for(File f: files) {
		           //System.out.println ("-- "+f.getName() );
		           //if(f.getName().endsWith(".cache") ){
		           f.delete();
		           
		           //}
		        }
		    }
		    
		}
	public boolean isReadyState() {
		return getState() == Action.C_READY_STATE;
	}

	protected void showLogin() {
		apps.getUI().showUI(FeedUI.UI_LOGON);
	}
	public void AutoDeleteSession() throws InterruptedException {
		//System.err.println("AutoDeleteSession START");
		Timer tmr = new Timer();
		tmr.schedule(new TimerTask() {
			
		
			public void run() {
				Date now = new Date();
				Date midnite = new Date();
				Date pm12 = new Date();
				Date pm15 = new Date();
				SimpleDateFormat sdf;
				sdf = new SimpleDateFormat("HH:mm:ss");
			    
			    try {
					midnite = sdf.parse("00:00:00:00");
					pm12 = sdf.parse("15:00:00:00");
					pm15 = sdf.parse("14:00:00:00");
				} catch (ParseException e) {
					e.printStackTrace();
				} catch (java.text.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    
			    now = new Date(((FeedApplication) apps).getFeedEngine().getEngine().getConnection().getTime());
			    
			    String midnite_str =  sdf.format(midnite).trim();
			    String pm12_str = sdf.format(pm12).trim();
			    String pm15_str = sdf.format(pm15).trim();
			    String now_str = sdf.format(now).trim();
			    
			    //System.err.println(midnite_str);
			    //System.err.println(now_str); || now_str.equals(pm12_str) ||now_str.equals(pm15_str)

			    if (now_str.equals(midnite_str)) {
					try{
						//System.err.println("now = midnite");
						File file = new  File("data//session");
						File file1 = new  File("data//static");

						deleteCache(file);
						deleteCache(file1);
						
						((IEQTradeApp) apps).getFeedEngine().getStore(
								FeedStore.DATA_QUOTE).getDataVector().clear();
						((IEQTradeApp) apps).getFeedEngine().getStore(
								FeedStore.DATA_TRADEPRICE).getDataVector().clear();
						
						((IEQTradeApp) apps).getFeedEngine().getStore(
								FeedStore.DATA_QUOTE).refresh();
						((IEQTradeApp) apps).getFeedEngine().getStore(
								FeedStore.DATA_TRADEPRICE).refresh();
						// arie close midnigth
//						((FeedApplication) apps).close();
//						((FeedApplication) apps).getTrading().close(); 
						
						//System.err.println("Delete Session & Static Berhasil");
						//JOptionPane.showMessageDialog(null, "Delete Session & Static Berhasil", "Delete Session", JOptionPane.INFORMATION_MESSAGE);
						
						//TODO panggil restart.bat
						Runtime.getRuntime().exec("cmd /c start restart.bat");

						//TODO exit v2
						Runtime.getRuntime().halt(0);
						System.exit(-1);
					} catch (Exception ex){
						return;
					}
				}
		    }
		}, 0, 1000);

	}

}