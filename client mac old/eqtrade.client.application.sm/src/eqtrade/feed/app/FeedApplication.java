package eqtrade.feed.app;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vollux.framework.Action;
import com.vollux.framework.VolluxAction;
import com.vollux.framework.VolluxApp;
import com.vollux.framework.VolluxEvent;
import com.vollux.framework.VolluxUI;

import eqtrade.application.IEQTradeApp;
import eqtrade.application.UIMaster;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedApp;
import eqtrade.feed.core.IFeedListener;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedEngine;
import eqtrade.feed.ui.core.UIChgPwd;
import eqtrade.feed.ui.core.UILogon;
import eqtrade.feed.ui.core.UIReconnect;
import eqtrade.feed.ui.quote.UIBestQuote;
import eqtrade.feed.ui.quote.UIMultiBestQuote;
import eqtrade.feed.ui.trade.UITimeTrade;
import eqtrade.feed.ui.trade.UITimeTradePrice;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.ITradingApp;

public class FeedApplication extends VolluxApp implements IEQTradeApp,
		IFeedListener {
	private final Log log = LogFactory.getLog(getClass());
	public final static String CONS_FEED = "feed";
	private IFeedApp feedApp;
	private ITradingApp tradingApp;
	private FeedEventDispatcher event;
	private FeedAction action;
	private FeedUI ui;
	private TradingApplication trading;
	//yosep enforcementpass
//	private HashMap isExpired; 
		
	// -------------------------------------------------------------------------------

	@Override
	public void start(String[] args) {
		//log.info("starting feed application");
		feedApp = new FeedEngine();
		feedApp.start();
		feedApp.addListener(this);
		event = new FeedEventDispatcher(this);
		action = new FeedAction(this);
		ui = new FeedUI(this);
	}

	@Override
	public VolluxEvent getEventDispatcher() {
		return event;
	}

	@Override
	public VolluxAction getAction() {
		return action;
	}

	@Override
	public VolluxUI getUI() {
		return ui;
	}

	@Override
	public IFeedApp getFeedEngine() {
		return feedApp;
	}

	public void setTradingApp(ITradingApp tradingApp) {
		this.tradingApp = tradingApp;
	}

	@Override
	public ITradingApp getTradingEngine() {
		return tradingApp;
	}

	public void setTrading(TradingApplication app) {
		this.trading = app;
	}

	public TradingApplication getTrading() {
		return trading;
	}

	@Override
	public void changePwdFailed(String msg) {
		((UIChgPwd) ui.getForm(FeedUI.UI_CHGPWD)).failed(msg);
	}

	@Override
	public void changePwdOK() {
		((UIChgPwd) ui.getForm(FeedUI.UI_CHGPWD)).success();
	}

	@Override
	public void disconnected() {
		((UIMaster) getUIMain()).setProperty("feed_disconnect", null);
		action.setState(Action.C_DISCONNECTED_STATE);
		ui.showUI(FeedUI.UI_RECONNECT);
	}

	@Override
	public void killed() {
		close();
		getTrading().close();
		//Utils.showMessage("your session has been expired or killed", null);
		Utils.showMessage("You have been sign out because you signed in on a different computer or device",null);
		Runtime.getRuntime().halt(0);
		System.exit(-1);
	}

	public void close() {
		getUI().save();
		getFeedEngine().logout("","",0);//yosep other device
		action.setState(Action.C_NOTREADY_STATE);
		FeedSetting.save();
		getUI().close();
		((UIMaster) getUIMain()).setProperty("feed_logout", null);
		((UIMaster) getUIMain()).setProperty("userid", "");
		
	}

	@Override
	public void loginFailed(String msg) {
		((UILogon) ui.getForm(FeedUI.UI_LOGON)).failed(msg);
		action.setState(Action.C_NOTREADY_STATE);
	}

	@Override
	public void loginOK() {
		((UILogon) ui.getForm(FeedUI.UI_LOGON)).doLoginTrading();
		// loginTradingOK();
	}

	public void logonFeedOnly() {
		getAction().get(FeedAction.A_PINON).setGranted(false);
		getAction().get(FeedAction.A_PINOFF).setGranted(false);
		getAction().get(FeedAction.A_PINON).setEnabled(false);
		getAction().get(FeedAction.A_PINOFF).setEnabled(false);
		action.get(TradingAction.A_SHOWCHGPIN).setEnabled(false);
		action.get(TradingAction.A_SHOWCHGPIN).setGranted(false);
		getAction().get(FeedAction.A_SHOWLINKACCOUNT).setEnabled(false);
		getAction().get(FeedAction.A_SHOWLOGINSIMASPOL).setEnabled(false);
		getAction().get(FeedAction.A_SHOWLINKACCOUNT).setGranted(false);
		getAction().get(FeedAction.A_SHOWLOGINSIMASPOL).setGranted(false);
		logonSuccessfully();
	}

	public void logonSuccessfully() {
		((UILogon) ui.getForm(FeedUI.UI_LOGON)).success();
		action.setState(Action.C_READY_STATE);
		FeedSetting.load();
		getUIMain().show();
		((UIMaster) getUIMain()).setProperty("feed_login", null);
		((UIMaster) getUIMain()).setProperty("userid", feedApp.getEngine()
				.getConnection().getUserId());System.out.println(feedApp.getEngine().getConnection().getCountFeedLogin() +" 99 ");
		if (feedApp.getEngine().getConnection().getCountFeedLogin() < 1) {//yosep other device
			ui.load();
		}
		/*//yosep enforcementpass
		if (getIsExpired() != null) {
		System.err.println(getIsExpired()+" isExpired3");
			trading.getUI().showUI(TradingUI.UI_CHGPASS,getIsExpired());
		}*/
	}
	
	// yosep other device
	@Override
	public void falseloginStatus(String msg) {
		((UILogon) ui.getForm(FeedUI.UI_LOGON)).setStatusFalse(msg);
		action.setState(Action.C_READY_STATE);
		((UIMaster) getUIMain()).setProperty("feed_login", null);
		((UIMaster) getUIMain()).setProperty("userid", feedApp.getEngine()
				.getConnection().getUserId());
		
	}
	
	@Override
	public void loginStatus(String msg) {
		((UILogon) ui.getForm(FeedUI.UI_LOGON)).setStatus(msg);
	}

	@Override
	public void reconnectFailed(String msg) {
		if (ui.getForm(FeedUI.UI_RECONNECT).isReady())
			((UIReconnect) ui.getForm(FeedUI.UI_RECONNECT)).failed(msg);
	}

	@Override
	public void reconnectOK() {
		action.setState(Action.C_READY_STATE);
		((UIReconnect) ui.getForm(FeedUI.UI_RECONNECT)).success();
		((UIMaster) getUIMain()).setProperty("feed_login", null);
	}

	@Override
	public void reconnectStatus(String msg) {
		((UIReconnect) ui.getForm(FeedUI.UI_RECONNECT)).setStatus(msg);
	}
	
	// yosep fixingTimetrade
	boolean hist = true;
	@Override
	public void finisTimeTrade(boolean hist) {
			if (((UITimeTrade)ui.getForm(FeedUI.UI_TIMETRADE)).isReady()) {
				((UITimeTrade)ui.getForm(FeedUI.UI_TIMETRADE)).setFieldStock(hist);
			}
			if (((UITimeTradePrice)ui.getForm(FeedUI.UI_TIMETRADEPRICE)).isReady()) {
				((UITimeTradePrice)ui.getForm(FeedUI.UI_TIMETRADEPRICE)).setFieldStock(hist);
				
			}
		this.hist = hist;
	}
	public boolean getHist(){
		return this.hist;
	}
	//yosep enforcementpass START
	/*@Override
	public void changePwd(String isExpired) {
//		((UIChgPassword)trading.getUI().getForm(TradingUI.UI_CHGPASS)).show(isExpired);
		this.isExpired  =new HashMap();
		this.isExpired.put("pesan", isExpired);
		System.err.println(getIsExpired()+" isExpired");
//		trading.getUI().showUI(TradingUI.UI_CHGPASS,param);
	}
	
	public HashMap getIsExpired(){
		return this.isExpired;
	}

	@Override
	public void forgotPwd() {
		// TODO Auto-generated method stub
		ui.showUI(FeedUI.UI_FORGOTPASSWORD);
	}
	//yosep enforcementpass END
*/
}
