package eqtrade.feed.ui.chart;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.ui.LengthAdjustmentType;

//import vollux.chart.core.ChartContrainerIntraday;
//import vollux.chart.core.CustomIntradayAxis;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IMessage;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Indices;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.RunningTrade;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockSummary;
import eqtrade.trading.market.chart.core.ChartContrainerIntraday;
import eqtrade.trading.market.chart.core.CustomIntradayAxis;

public class UIIntraday extends UI implements IMessage {
	private JPanel pnlInfo;
	private JPanel pnlInput;
	private ChartContrainerIntraday chart;
	private TimeSeriesCollection xydataset;
	private TimeSeries timeseries;
	private XYItemRenderer render;

	private JLabel fieldLast;
	private JLabel fieldChange;
	private JLabel fieldPercent;
	private JLabel fieldOpen;
	private JLabel fieldHigh;
	private JLabel fieldLow;
	private JLabel fieldPrev;
	private ValueMarker marker;
	private ValueMarker marker2;

	private static NumberFormat formatter2 = new DecimalFormat("#,##0  ");
	private static NumberFormat formatter = new DecimalFormat("#,##0.00  ");
	private static NumberFormat formatter3 = new DecimalFormat("#,##0.000  ");

	private JTextField fieldStock;
	private JLabel fieldName;
	private String oldStock = "";

	public UIIntraday(String app) {
		super("Chart Intraday", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_INTRADAY);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		fieldLast = new JLabel(" ", SwingConstants.RIGHT);
		fieldChange = new JLabel(" ", SwingConstants.RIGHT);
		fieldPercent = new JLabel(" ", SwingConstants.RIGHT);
		fieldOpen = new JLabel(" ", SwingConstants.RIGHT);
		fieldHigh = new JLabel(" ", SwingConstants.RIGHT);
		fieldLow = new JLabel(" ", SwingConstants.RIGHT);
		fieldPrev = new JLabel(" ", SwingConstants.RIGHT);
		fieldLast.setFont(new Font("dialog", 1, 20));

		pnlInfo = new JPanel(new BorderLayout());
		FormLayout l = new FormLayout(
				"pref,2dlu,50px:grow, 4dlu, pref, 2dlu, pref, 4dlu, pref, 2dlu, 30px:grow, 4dlu, pref, 2dlu, 30px:grow",
				"pref, 2dlu, pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		// b.add(new JLabel("last"), c.xywh(1,1,1,3));
		b.add(fieldLast, c.xywh(1, 1, 3, 3));
		b.add(new JLabel("chg"), c.xy(5, 1));
		b.add(fieldChange, c.xy(7, 1));
		b.add(new JLabel("%"), c.xy(5, 3));
		b.add(fieldPercent, c.xy(7, 3));

		b.add(new JLabel("high"), c.xy(9, 1));
		b.add(fieldHigh, c.xy(11, 1));
		b.add(new JLabel("low"), c.xy(9, 3));
		b.add(fieldLow, c.xy(11, 3));
		b.add(new JLabel("open"), c.xy(13, 1));
		b.add(fieldOpen, c.xy(15, 1));
		b.add(new JLabel("prev"), c.xy(13, 3));
		b.add(fieldPrev, c.xy(15, 3));

		pnlInput = new JPanel(new BorderLayout());
		fieldStock = new JTextField("");
		fieldName = new JLabel("please type a stock code");

		FormLayout ll = new FormLayout("50px,2dlu,50px, 2dlu, pref:grow, pref",
				"pref");
		PanelBuilder bb = new PanelBuilder(ll, pnlInput);
		pnlInput.setBorder(new EmptyBorder(5, 5, 5, 5));
		bb.add(fieldStock, c.xy(1, 1));
		bb.add(fieldName, c.xyw(3, 1, 3));

		fieldStock.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				fieldStock.selectAll();
			}
		});
		fieldStock.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fieldStockAction();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		refresh();
		if (hSetting.get("stock") != null) {
			fieldStock.setText((String) hSetting.get("stock"));
			fieldStockAction();
		}

		chart = new ChartContrainerIntraday();
		chart.getNumberAxis().setNumberFormatOverride(
				new DecimalFormat("#,##0"));
		chart.getPanel().setPreferredSize(new Dimension(200, 200));
		// try {
		// chart.getDateAxis().setRange(timeFormat.parse(dateFormat.format(new
		// Date())+" 092500"), timeFormat.parse(dateFormat.format(new
		// Date())+" 160000"));
		// } catch (Exception ex){
		// ex.printStackTrace();
		// }
		XYPlot xyplot = chart.getPlot();
		xyplot.setDomainAxis(new CustomIntradayAxis("", new Date(), chart
				.getTimeline()));
		marker = new ValueMarker(0);
		marker.setLabelOffsetType(LengthAdjustmentType.EXPAND);
		marker.setPaint(Color.green);
		marker.setOutlineStroke(new BasicStroke(2F));
		chart.getMiddlePlot().addRangeMarker(marker);

		marker2 = new ValueMarker(0);
		marker2.setLabelOffsetType(LengthAdjustmentType.EXPAND);
		marker2.setPaint(Color.green);
		marker2.setOutlineStroke(new BasicStroke(2F));
		chart.getMiddlePlot().addDomainMarker(marker2);
		try {
			marker2.setValue(timeFormat.parse(
					dateFormat.format(new Date()) + " 130000").getTime());
		} catch (Exception ex) {
		}
		StandardChartTheme.createDarknessTheme().apply(
				chart.getPanel().getChart());
		render = new XYLineAndShapeRenderer(true, false);
		timeseries = new TimeSeries("");
		xydataset = new TimeSeriesCollection(timeseries);
		chart.addPlot("", name, xydataset, null, render);
		render.setBaseToolTipGenerator(new StandardXYToolTipGenerator(
				"({1}, {2})", new SimpleDateFormat("HH:mm:ss"),
				new DecimalFormat("0.00")));
		render.setSeriesPaint(0, Color.yellow);
		render.setBasePaint(Color.white);
		render.setSeriesStroke(0, new BasicStroke(1F));

		pnlContent = new JSkinPnl();
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(bb.getPanel(), BorderLayout.NORTH);
		pnlContent.add(chart.getPanel(), BorderLayout.CENTER);
		pnlContent.add(b.getPanel(), BorderLayout.SOUTH);
		((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCKSUMMARY)
				.addTableModelListener(new TableModelListener() {
					@Override
					public void tableChanged(TableModelEvent e) {
						priceChanged();
					}
				});
		((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCKSUMMARY).refresh();
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_INDICES)
				.addTableModelListener(new TableModelListener() {
					@Override
					public void tableChanged(TableModelEvent e) {
						priceChanged();
					}
				});
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_INDICES)
				.refresh();

		refresh();
	}

	private void fieldStockAction() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String newStock = fieldStock.getText().trim().toUpperCase();
				if ((newStock.length() > 0 && !oldStock.equals(newStock))) {
					fieldStock.setText(newStock);
					oldStock = newStock;
					priceChanged();
					fieldName.setText(geStockName(newStock));
					queryChart();
				}
				fieldStock.selectAll();
			}
		});
	}

	private String geStockName(String code) {
		String name = "";
		int i = ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK)
				.getIndexByKey(new Object[] { code });
		if (i > -1) {
			name = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_STOCK).getDataByIndex(i))
					.getName();
		} else {
			i = ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_INDICES)
					.getIndexByKey(new Object[] { code });
			if (i > 1) {
				name = ((Indices) ((IEQTradeApp) apps).getFeedEngine()
						.getStore(FeedStore.DATA_INDICES).getDataByIndex(i))
						.getCode();
			}
		}
		return name;
	}

	private void priceChanged() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				StockSummary ss = (StockSummary) ((IEQTradeApp) apps)
						.getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY)
						.getDataByKey(new Object[] { oldStock, "RG" });
				Indices index = (Indices) ((IEQTradeApp) apps).getFeedEngine()
						.getStore(FeedStore.DATA_INDICES)
						.getDataByKey(new Object[] { oldStock });
				if (ss != null) {
					marker.setValue(ss.getPrevious().doubleValue());
					fieldLast.setText(formatter2.format(ss.getClosing()));
					fieldPercent.setText(formatter.format(ss.getPercent()));
					fieldChange.setText(formatter2.format(ss.getChange()));
					fieldOpen.setText(formatter2.format(ss.getOpening()));
					fieldHigh.setText(formatter2.format(ss.getHighest()));
					fieldLow.setText(formatter2.format(ss.getLowest()));
					fieldPrev.setText(formatter2.format(ss.getPrevious()));
					fieldLast
							.setForeground(ss.getChange().doubleValue() > 0 ? FeedSetting
									.getColor(FeedSetting.C_PLUS)
									: (ss.getChange().doubleValue() < 0) ? FeedSetting
											.getColor(FeedSetting.C_MINUS)
											: FeedSetting
													.getColor(FeedSetting.C_ZERO));
					fieldPercent
							.setForeground(ss.getChange().doubleValue() > 0 ? FeedSetting
									.getColor(FeedSetting.C_PLUS)
									: (ss.getChange().doubleValue() < 0) ? FeedSetting
											.getColor(FeedSetting.C_MINUS)
											: FeedSetting
													.getColor(FeedSetting.C_ZERO));
					fieldChange
							.setForeground(ss.getChange().doubleValue() > 0 ? FeedSetting
									.getColor(FeedSetting.C_PLUS)
									: (ss.getChange().doubleValue() < 0) ? FeedSetting
											.getColor(FeedSetting.C_MINUS)
											: FeedSetting
													.getColor(FeedSetting.C_ZERO));
					fieldOpen.setForeground(ss.getOpening().doubleValue() > ss
							.getPrevious().doubleValue() ? FeedSetting
							.getColor(FeedSetting.C_PLUS)
							: (ss.getOpening().doubleValue() < ss.getPrevious()
									.doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldHigh.setForeground(ss.getHighest().doubleValue() > ss
							.getClosing().doubleValue() ? FeedSetting
							.getColor(FeedSetting.C_PLUS)
							: (ss.getHighest().doubleValue() < ss.getClosing()
									.doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldLow.setForeground(ss.getLowest().doubleValue() > ss
							.getClosing().doubleValue() ? FeedSetting
							.getColor(FeedSetting.C_PLUS)
							: (ss.getLowest().doubleValue() < ss.getClosing()
									.doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldPrev.setForeground(FeedSetting
							.getColor(FeedSetting.C_ZERO));
				} else if (index != null) {
					marker.setValue(index.getPrev().doubleValue());
					fieldLast.setText(formatter3.format(index.getLast()));
					fieldPercent.setText(formatter3.format(index.getPercent()));
					fieldChange.setText(formatter3.format(index.getChange()));
					fieldOpen.setText(formatter3.format(index.getOpen()));
					fieldHigh.setText(formatter3.format(index.getHigh()));
					fieldLow.setText(formatter3.format(index.getLow()));
					fieldPrev.setText(formatter3.format(index.getPrev()));
					fieldLast
							.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
									.getColor(FeedSetting.C_PLUS)
									: (index.getChange().doubleValue() < 0) ? FeedSetting
											.getColor(FeedSetting.C_MINUS)
											: FeedSetting
													.getColor(FeedSetting.C_ZERO));
					fieldPercent
							.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
									.getColor(FeedSetting.C_PLUS)
									: (index.getChange().doubleValue() < 0) ? FeedSetting
											.getColor(FeedSetting.C_MINUS)
											: FeedSetting
													.getColor(FeedSetting.C_ZERO));
					fieldChange
							.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
									.getColor(FeedSetting.C_PLUS)
									: (index.getChange().doubleValue() < 0) ? FeedSetting
											.getColor(FeedSetting.C_MINUS)
											: FeedSetting
													.getColor(FeedSetting.C_ZERO));
					fieldOpen
							.setForeground(index.getOpen().doubleValue() > index
									.getPrev().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (index
									.getOpen().doubleValue() < index.getPrev()
									.doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldHigh
							.setForeground(index.getHigh().doubleValue() > index
									.getLast().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (index
									.getHigh().doubleValue() < index.getLast()
									.doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldLow.setForeground(index.getLow().doubleValue() > index
							.getLast().doubleValue() ? FeedSetting
							.getColor(FeedSetting.C_PLUS)
							: (index.getLow().doubleValue() < index.getLast()
									.doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldPrev.setForeground(FeedSetting
							.getColor(FeedSetting.C_ZERO));
					try {
						Date t = timeFormat.parse(dateFormat.format(new Date())
								+ " " + index.getTranstime());
						timeseries.addOrUpdate(
								Second.parseSecond(datetimeFormat.format(t)),
								index.getLast());
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} else {
					fieldLast.setText("- ");
					fieldPercent.setText("- ");
					fieldChange.setText("- ");
					fieldOpen.setText("- ");
					fieldHigh.setText("- ");
					fieldLow.setText("- ");
					fieldPrev.setText("- ");
				}
			}
		});
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 400, 300));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("stock", oldStock.toUpperCase());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		pnlInfo.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		priceChanged();
	}

	Thread threadLoad = null;

	private void queryChart() {
		// SwingUtilities.invokeLater(new Runnable(){
		if (threadLoad != null) {
			threadLoad.stop();
			threadLoad = null;
		}
		threadLoad = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					xydataset.removeSeries(timeseries);
					timeseries.clear();
					String result = ((IEQTradeApp) apps).getFeedEngine()
							.getEngine().getConnection()
							.getHistory("CHI" + "|" + oldStock);
					if (result != null) {
						String as[] = result.split("\n", -2);
						int aslength = as.length - 1;
						for (int j = 0; j < aslength; j++) {
							String s1 = as[j];
							if (!s1.trim().equals("")) {
								String detail[] = s1.split("\\|");
								Date t = timeFormat.parse(dateFormat
										.format(new Date()) + " " + detail[0]);
								try {
									timeseries.addOrUpdate(Second
											.parseSecond(datetimeFormat
													.format(t)), Double
											.parseDouble(detail[2]));
								} catch (Exception ex) {
									ex.printStackTrace();
									/*System.out.println("error processing: "
											+ s1);*/
								}
							}
						}
						xydataset.addSeries(timeseries);
						chart.updateDataset("", name, xydataset);
						// chart.getPanel().repaint();
					} else {
						xydataset.addSeries(timeseries);
						chart.updateDataset("", name, xydataset);
						// Utils.showMessage("no data available",
						// UIIntraday.this.form);
						fieldName.setText("no data available");
					}
				} catch (Exception ex) {
					oldStock = "";
					fieldName.setText("failed, please try again");
					// sUtils.showMessage("failed, please try again",
					// UIIntraday.this.form);
					ex.printStackTrace();
				}
			}
		});
		threadLoad.start();
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
			fieldStock.setText(par.get("STOCK").toString());
			fieldStockAction();
		} else {
			String str = (String) hSetting.get("stock");
			if (str != null) {
				fieldStock.setText(str);
				fieldStockAction();
			}
		}
		show();
	}

	@Override
	public void show() {
		if (!form.isVisible()) {
			((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
					.get(FeedParser.PARSER_RUNNINGTRADE).addListener(this);
			((IEQTradeApp) apps).getFeedEngine().subscribe(
					FeedParser.PARSER_STOCKSUMMARY,
					FeedParser.PARSER_STOCKSUMMARY);
		}
		super.show();
	}

	@Override
	public void hide() {
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_RUNNINGTRADE).removeListener(this);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_STOCKSUMMARY, FeedParser.PARSER_STOCKSUMMARY);
		super.close();
		oldStock = "";
		fieldStock.setText("");
	}

	@Override
	public void close() {
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_RUNNINGTRADE).removeListener(this);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_STOCKSUMMARY, FeedParser.PARSER_STOCKSUMMARY);
		super.close();
		oldStock = "";
		fieldStock.setText("");
	}

	@Override
	public void newMessage(Model arg0) {
		RunningTrade rt = (RunningTrade) arg0;
		if (rt.getStock().equals(oldStock)) {
			try {
				Date t = timeFormat.parse(dateFormat.format(new Date()) + " "
						+ rt.getTxTime());
				timeseries.addOrUpdate(
						Second.parseSecond(datetimeFormat.format(t)),
						rt.getLast());
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	private static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat(
			"yyyy-MM-dd HHmmss");
	private static SimpleDateFormat datetimeFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
}
