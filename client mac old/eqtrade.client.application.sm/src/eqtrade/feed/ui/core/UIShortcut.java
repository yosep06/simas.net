package eqtrade.feed.ui.core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.framework.VolluxShortcut;
import com.vollux.model.Shortcut;
import com.vollux.model.ShortcutDef;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.EQTrade;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;

public final class UIShortcut extends UI {
	private JSkinDlg frame;
	private JButton btnOK;
	private JButton btnCancel;
	private JGrid table;
	private JLabel lblShortcut;
	private JText fieldShortcut;
	private String title;

	public UIShortcut(String app) {
		super("Custom Shortcut Editor", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_DLGSHORTCUT);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.requestFocus();
			}
		});
	}

	@Override
	protected void build() {
		// createPopup();
		fieldShortcut = new JText(true);
		fieldShortcut.setForeground(Color.black);
		lblShortcut = new JLabel("Enter new shortcut:", SwingConstants.RIGHT);
		lblShortcut.setDisplayedMnemonic('s');
		lblShortcut.setLabelFor(fieldShortcut);
		fieldShortcut.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				fieldShortcut.selectAll();
			}

			@Override
			public void focusLost(FocusEvent e) {
				focuslost();
			}
		});
		fieldShortcut.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				focuslost();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		lblShortcut.setDisplayedMnemonic('S');
		lblShortcut.setLabelFor(fieldShortcut);

		table = createTable(VolluxShortcut.getModel(), null,
				(Hashtable) hSetting.get("table"));
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getTable().setRowSelectionAllowed(true);
		table.getTable().getSelectionModel()
				.addListSelectionListener(new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						int i = table.getSelectedRow();
						if (i >= 0) {
							Shortcut sc = (Shortcut) VolluxShortcut.getModel()
									.getDataByIndex(table.getMappedRow(i));
							fieldShortcut.setText(sc.getShortcut());
						}
					}
				});

		btnOK = new JButton("OK");
		btnOK.setMnemonic('O');
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnOKAction();
			}
		});
		btnCancel = new JButton("Cancel");
		btnCancel.setMnemonic('C');
		btnCancel.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				hide();
			}
		});
		registerEvent(btnOK);
		registerEvent(btnCancel);

		pnlContent = new JSkinPnl();
		pnlContent.add(buildEditor(), BorderLayout.CENTER);
		pnlContent.add(buildButton(), BorderLayout.SOUTH);
	}

	private void focuslost() {
		int i = table.getSelectedRow();
		boolean duf = false;
		if (i >= 0 && !fieldShortcut.getText().equals("")) {
			i = table.getMappedRow(i);
			for (int j = 0; j < VolluxShortcut.getModel().getRowCount(); j++) {
				Shortcut cs = (Shortcut) VolluxShortcut.getModel()
						.getDataByIndex(j);
				if (cs.getShortcut().equals(
						fieldShortcut.getText().toUpperCase().trim())
						&& (j != i)) {
					duf = true;
					break;
				}
			}
			if (!duf) {
				Shortcut sc = (Shortcut) VolluxShortcut.getModel()
						.getDataByIndex(i);
				sc.setShortcut(fieldShortcut.getText().toUpperCase().trim());
				VolluxShortcut.getModel().refresh();
				table.getTable().requestFocus();
			} else {
				fieldShortcut.setText("");
				JOptionPane.showMessageDialog(frame,
						"Duplicate Shortcut, please try another shortcut",
						"Error", JOptionPane.ERROR_MESSAGE);
			}
		} else {
			i = table.getMappedRow(i);
			Shortcut sc = (Shortcut) VolluxShortcut.getModel()
					.getDataByIndex(i);
			sc.setShortcut("");
			VolluxShortcut.getModel().refresh();
			table.getTable().requestFocus();
			fieldShortcut.setText("");

		}
	}

	private JSkinPnl buildEditor() {
		JSkinPnl pnlresult = new JSkinPnl();
		FormLayout layout = new FormLayout("200px, 2dlu, 100px",
				"250px, 2dlu, pref");
		PanelBuilder builder = new PanelBuilder(layout, pnlresult);
		CellConstraints cc = new CellConstraints();
		builder.setDefaultDialogBorder();
		builder.add(table, cc.xywh(1, 1, 3, 1));
		builder.add(lblShortcut, cc.xy(1, 3));
		builder.add(fieldShortcut, cc.xy(3, 3));
		return pnlresult;
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.setResizable(false);
		frame.setContent(pnlContent);
		frame.pack();
		frame.setLocation(((Rectangle) hSetting.get("size")).x,
				((Rectangle) hSetting.get("size")).y);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
	}

	private void btnOKAction() {
		save();
		VolluxShortcut.saveModel();
		apps.getUI().refresh();
		hide();
	}

	private JSkinPnl buildButton() {
		JSkinPnl pnlButton = new JSkinPnl();
		FormLayout layoutButton = new FormLayout(
				"pref:grow, 2dlu, pref, 2dlu, pref", "pref");
		PanelBuilder builderBtn = new PanelBuilder(layoutButton, pnlButton);
		builderBtn.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		builderBtn.add(this.btnOK, cc.xy(3, 1));
		builderBtn.add(this.btnCancel, cc.xy(5, 1));
		return pnlButton;
	}

	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}

	@Override
	public void hide() {
		saveSetting();
		frame.setVisible(false);
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", ShortcutDef.getTableDef());
			hSetting.put("size", new Rectangle(20, 20, 600, 400));
		}
	}

	@Override
	public void refresh() {
		if (frame != null) {
			frame.updateUI();
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}

	private void registerEvent(JComponent comm) {
		InputMap inputMap = comm
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		comm.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						btnCancel.doClick();
					}
				});
		comm.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton)
							((JButton) evt.getSource()).doClick();
						else
							btnOK.doClick();
					}
				});
	}

	@Override
	public void saveSetting() {
		hSetting.put("size",
				new Rectangle(frame.getX(), frame.getY(), frame.getWidth(),
						frame.getHeight()));
		hSetting.put("table", table.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	public void setState(boolean state) {
		btnOK.setEnabled(state);
		btnCancel.setEnabled(state);
	}

	@Override
	public void show() {
		load();
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}

	@Override
	public void show(Object param) {
		show();
	}

	private void load() {
	}

	private void save() {
	}

}
