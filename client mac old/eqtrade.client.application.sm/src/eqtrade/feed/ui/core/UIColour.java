package eqtrade.feed.ui.core;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.EQTrade;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;

public final class UIColour extends UI {
	private JSkinDlg frame;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnDefault;

	private JText tBack = new JText(false);
	private JText tBack2 = new JText(false);
	private JText tNormal = new JText(false);
	private JText tUp = new JText(false);
	private JText tDown = new JText(false);
	private JText tFlat = new JText(false);
	private JText tCrossing = new JText(false);
	private JText tForeign = new JText(false);
	private JText tDomestic = new JText(false);
	private JTextField tBroker1 = new JTextField();
	private JTextField tBroker2 = new JTextField();
	private JTextField tBroker3 = new JTextField();
	private JText tBrokerColor1 = new JText(false);
	private JText tBrokerColor2 = new JText(false);
	private JText tBrokerColor3 = new JText(false);

	private JDropDown cbFont = new JDropDown();
	private JDropDown cbStyle = new JDropDown();
	private JNumber textSize = new JNumber(Integer.class, 0, 0, true);

	private JText tAgri = new JText(false);
	private JText tMining = new JText(false);
	private JText tBasic = new JText(false);
	private JText tMisc = new JText(false);
	private JText tConsumer = new JText(false);
	private JText tProperty = new JText(false);
	private JText tInfra = new JText(false);
	private JText tFinance = new JText(false);
	private JText tTrade = new JText(false);
	private JText tb1 = new JText(false);
	private JText tb2 = new JText(false);
	private JText tb3 = new JText(false);

	private JDialog dialog = null;
	private ActionListener okListener = null;
	private JColorChooser colorChooser = new JColorChooser();
	private int numChooser = 1;

	public UIColour(String app) {
		super("Change Feed Colour", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_COLOUR);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			}
		});
	}

	protected void reset() {
		FeedSetting.defaultColor();
		load();
	}

	@Override
	protected void build() {
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		String[] strFontList = ge.getAvailableFontFamilyNames();

		for (int i = 0; i < strFontList.length; i++) {
			cbFont.addItem((strFontList[i]).toLowerCase());
		}
		cbStyle.addItem(new String("Plain"));
		cbStyle.addItem(new String("Bold"));
		cbStyle.addItem(new String("Italic"));

		textSize.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				textSize.selectAll();
			}

			@Override
			public void focusLost(FocusEvent e) {
				try {
					textSize.commitEdit();
					if (((Integer) textSize.getValue()).intValue() <= 0) {
						textSize.setValue(new Integer(12));
					}
				} catch (Exception ex) {
					textSize.setValue(new Integer(12));
				}
			}
		});

		textSize.setHorizontalAlignment(SwingConstants.RIGHT);

		tBroker1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				tBroker1.selectAll();
			}
		});
		tBroker2.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				tBroker2.selectAll();
			}
		});
		tBroker3.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				tBroker3.selectAll();
			}
		});
		btnOK = new JButton("OK");
		btnOK.setMnemonic('O');
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnOKAction();
			}
		});
		btnCancel = new JButton("Cancel");
		btnCancel.setMnemonic('C');
		btnCancel.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				hide();
			}
		});
		btnDefault = new JButton("Default");
		btnDefault.setMnemonic('D');
		btnDefault.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				reset();
			}
		});
		registerEvent(btnOK);
		registerEvent(btnCancel);
		registerEvent(btnDefault);

		JSkinPnl pnlEntry = new JSkinPnl();
		FormLayout layoutEntry = new FormLayout(
				"pref, 2dlu, 100px,2dlu,pref, 2dlu, 100px,2dlu,pref, 2dlu, 100px",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		PanelBuilder pnlBuilder = new PanelBuilder(layoutEntry, pnlEntry);
		pnlBuilder.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		pnlBuilder.add(new JLabel("Back 1"), cc.xy(1, 1));
		pnlBuilder.add(tBack, cc.xy(3, 1));
		pnlBuilder.add(new JLabel("Back 2"), cc.xy(5, 1));
		pnlBuilder.add(tBack2, cc.xy(7, 1));
		pnlBuilder.add(new JLabel("Normal"), cc.xy(9, 1));
		pnlBuilder.add(tNormal, cc.xy(11, 1));
		pnlBuilder.add(new JLabel("Up"), cc.xy(1, 3));
		pnlBuilder.add(tUp, cc.xy(3, 3));
		pnlBuilder.add(new JLabel("Down"), cc.xy(5, 3));
		pnlBuilder.add(tDown, cc.xy(7, 3));
		pnlBuilder.add(new JLabel("Flat"), cc.xy(9, 3));
		pnlBuilder.add(tFlat, cc.xy(11, 3));
		pnlBuilder.add(new JLabel("Crossing"), cc.xy(1, 5));
		pnlBuilder.add(tCrossing, cc.xy(3, 5));
		pnlBuilder.add(new JLabel("Domestic"), cc.xy(5, 5));
		pnlBuilder.add(tDomestic, cc.xy(7, 5));
		pnlBuilder.add(new JLabel("Foreign"), cc.xy(9, 5));
		pnlBuilder.add(tForeign, cc.xy(11, 5));
		pnlBuilder.add(new JLabel("Broker 1"), cc.xy(1, 7));
		pnlBuilder.add(tBroker1, cc.xy(3, 7));
		pnlBuilder.add(new JLabel("Broker 2"), cc.xy(5, 7));
		pnlBuilder.add(tBroker2, cc.xy(7, 7));
		pnlBuilder.add(new JLabel("Broker 3"), cc.xy(9, 7));
		pnlBuilder.add(tBroker3, cc.xy(11, 7));
		pnlBuilder.add(new JLabel(""), cc.xy(1, 9));
		pnlBuilder.add(tBrokerColor1, cc.xy(3, 9));
		pnlBuilder.add(new JLabel(""), cc.xy(5, 9));
		pnlBuilder.add(tBrokerColor2, cc.xy(7, 9));
		pnlBuilder.add(new JLabel(""), cc.xy(9, 9));
		pnlBuilder.add(tBrokerColor3, cc.xy(11, 9));

		pnlBuilder.add(new JLabel("Agriculture"), cc.xy(1, 11));
		pnlBuilder.add(tAgri, cc.xy(3, 11));
		pnlBuilder.add(new JLabel("Mining"), cc.xy(5, 11));
		pnlBuilder.add(tMining, cc.xy(7, 11));
		pnlBuilder.add(new JLabel("Industry"), cc.xy(9, 11));
		pnlBuilder.add(tBasic, cc.xy(11, 11));

		pnlBuilder.add(new JLabel("Misc"), cc.xy(1, 13));
		pnlBuilder.add(tMisc, cc.xy(3, 13));
		pnlBuilder.add(new JLabel("Consumer"), cc.xy(5, 13));
		pnlBuilder.add(tConsumer, cc.xy(7, 13));
		pnlBuilder.add(new JLabel("Property"), cc.xy(9, 13));
		pnlBuilder.add(tProperty, cc.xy(11, 13));

		pnlBuilder.add(new JLabel("Infrastructure"), cc.xy(1, 15));
		pnlBuilder.add(tInfra, cc.xy(3, 15));
		pnlBuilder.add(new JLabel("Finance"), cc.xy(5, 15));
		pnlBuilder.add(tFinance, cc.xy(7, 15));
		pnlBuilder.add(new JLabel("Trade"), cc.xy(9, 15));
		pnlBuilder.add(tTrade, cc.xy(11, 15));

		pnlBuilder.add(new JLabel("Broker Dealer"), cc.xy(1, 17));
		pnlBuilder.add(tb1, cc.xy(3, 17));
		pnlBuilder.add(new JLabel("Broker Foreign"), cc.xy(5, 17));
		pnlBuilder.add(tb2, cc.xy(7, 17));
		pnlBuilder.add(new JLabel("Broker Underwriter"), cc.xy(9, 17));
		pnlBuilder.add(tb3, cc.xy(11, 17));

		pnlBuilder.add(new JLabel("Font"), cc.xy(1, 19));
		pnlBuilder.add(cbFont, cc.xy(3, 19));
		pnlBuilder.add(new JLabel("Style"), cc.xy(5, 19));
		pnlBuilder.add(cbStyle, cc.xy(7, 19));
		pnlBuilder.add(new JLabel("Size"), cc.xy(9, 19));
		pnlBuilder.add(textSize, cc.xy(11, 19));

		pnlContent = new JSkinPnl();
		pnlContent.add(pnlEntry, BorderLayout.CENTER);
		pnlContent.add(buildButton(), BorderLayout.SOUTH);
		createEvent();
	}

	private void createMouseListener(final JComponent comp, final int no) {
		comp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (arg0.getClickCount() == 2 && arg0.getButton() == 1) {
					colorChooser.setColor(comp.getBackground());
					numChooser = no;
					dialog.setLocationRelativeTo(frame);
					dialog.setVisible(true);
				}
			}
		});
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.setResizable(false);
		frame.setContent(pnlContent);
		frame.pack();
		frame.setLocation(((Rectangle) hSetting.get("size")).x,
				((Rectangle) hSetting.get("size")).y);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
	}

	private void btnOKAction() {
		save();
		apps.getUI().refresh();
		hide();
	}

	private JSkinPnl buildButton() {
		JSkinPnl pnlButton = new JSkinPnl();
		FormLayout layoutButton = new FormLayout(
				"pref, pref:grow, 2dlu, pref, 2dlu, pref", "pref");
		PanelBuilder builderBtn = new PanelBuilder(layoutButton, pnlButton);
		builderBtn.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		builderBtn.add(this.btnDefault, cc.xy(1, 1));
		builderBtn.add(this.btnOK, cc.xy(4, 1));
		builderBtn.add(this.btnCancel, cc.xy(6, 1));
		return pnlButton;
	}

	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}

	@Override
	public void hide() {
		saveSetting();
		frame.setVisible(false);
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("size", new Rectangle(20, 20, 600, 400));
		}
	}

	@Override
	public void refresh() {
		if (frame != null) {
			frame.updateUI();
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}

	private void registerEvent(JComponent comm) {
		InputMap inputMap = comm
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		comm.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						btnCancel.doClick();
					}
				});
		comm.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton)
							((JButton) evt.getSource()).doClick();
						else
							btnOK.doClick();
					}
				});
	}

	@Override
	public void saveSetting() {
		hSetting.put("size",
				new Rectangle(frame.getX(), frame.getY(), frame.getWidth(),
						frame.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	public void setState(boolean state) {
		btnOK.setEnabled(state);
		btnCancel.setEnabled(state);
	}

	@Override
	public void show() {
		load();
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}

	@Override
	public void show(Object param) {
		show();
	}

	private void load() {
		tBack.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tBack2.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN));
		tNormal.setBackground(FeedSetting.getColor(FeedSetting.C_FOREGROUND));
		tUp.setBackground(FeedSetting.getColor(FeedSetting.C_PLUS));
		tDown.setBackground(FeedSetting.getColor(FeedSetting.C_MINUS));
		tFlat.setBackground(FeedSetting.getColor(FeedSetting.C_ZERO));
		tCrossing.setBackground(FeedSetting.getColor(FeedSetting.C_TS));
		tForeign.setBackground(FeedSetting.getColor(FeedSetting.C_FOREIGN));
		tDomestic.setBackground(FeedSetting.getColor(FeedSetting.C_DOMESTIC));
		tBrokerColor1.setBackground(FeedSetting
				.getColor(FeedSetting.C_BROKERCOLOR1));
		tBrokerColor2.setBackground(FeedSetting
				.getColor(FeedSetting.C_BROKERCOLOR2));
		tBrokerColor3.setBackground(FeedSetting
				.getColor(FeedSetting.C_BROKERCOLOR3));
		tBroker1.setText((String) FeedSetting.getSetting(FeedSetting.C_BROKER1));
		tBroker2.setText((String) FeedSetting.getSetting(FeedSetting.C_BROKER2));
		tBroker3.setText((String) FeedSetting.getSetting(FeedSetting.C_BROKER3));

		tAgri.setBackground(FeedSetting.getColor(FeedSetting.C_AGRICULTURE));
		tMining.setBackground(FeedSetting.getColor(FeedSetting.C_MINING));
		tBasic.setBackground(FeedSetting.getColor(FeedSetting.C_BASICINDUSTRY));
		tMisc.setBackground(FeedSetting.getColor(FeedSetting.C_MISCELLANEOUS));
		tConsumer.setBackground(FeedSetting.getColor(FeedSetting.C_CONSUMER));
		tProperty.setBackground(FeedSetting.getColor(FeedSetting.C_PROPERTY));
		tInfra.setBackground(FeedSetting.getColor(FeedSetting.C_INFRASTRUCTURE));
		tFinance.setBackground(FeedSetting.getColor(FeedSetting.C_FINANCE));
		tTrade.setBackground(FeedSetting.getColor(FeedSetting.C_TRADE));

		tb1.setBackground(FeedSetting.getColor(FeedSetting.C_BGOV));
		tb2.setBackground(FeedSetting.getColor(FeedSetting.C_BFOREIGN));
		tb3.setBackground(FeedSetting.getColor(FeedSetting.C_BLOCAL));

		Font font = FeedSetting.getFont();
		cbFont.setSelectedItem(new String(font.getName().toLowerCase()));
		cbStyle.setSelectedIndex(font.getStyle());
		textSize.setValue(new Integer(font.getSize()));
	}

	private void save() {
		FeedSetting.putColor(FeedSetting.C_BACKGROUND, tBack.getBackground());
		FeedSetting.putColor(FeedSetting.C_BACKGROUNDEVEN,
				tBack2.getBackground());
		FeedSetting.putColor(FeedSetting.C_FOREGROUND, tNormal.getBackground());
		FeedSetting.putColor(FeedSetting.C_PLUS, tUp.getBackground());
		FeedSetting.putColor(FeedSetting.C_MINUS, tDown.getBackground());
		FeedSetting.putColor(FeedSetting.C_ZERO, tFlat.getBackground());
		FeedSetting.putColor(FeedSetting.C_TS, tCrossing.getBackground());
		FeedSetting.putColor(FeedSetting.C_FOREIGN, tForeign.getBackground());
		FeedSetting.putColor(FeedSetting.C_DOMESTIC, tDomestic.getBackground());
		FeedSetting.putColor(FeedSetting.C_BROKERCOLOR1,
				tBrokerColor1.getBackground());
		FeedSetting.putColor(FeedSetting.C_BROKERCOLOR2,
				tBrokerColor2.getBackground());
		FeedSetting.putColor(FeedSetting.C_BROKERCOLOR3,
				tBrokerColor3.getBackground());

		FeedSetting.putColor(FeedSetting.C_AGRICULTURE, tAgri.getBackground());
		FeedSetting.putColor(FeedSetting.C_MINING, tMining.getBackground());
		FeedSetting.putColor(FeedSetting.C_BASICINDUSTRY,
				tBasic.getBackground());
		FeedSetting
				.putColor(FeedSetting.C_MISCELLANEOUS, tMisc.getBackground());
		FeedSetting.putColor(FeedSetting.C_CONSUMER, tConsumer.getBackground());
		FeedSetting.putColor(FeedSetting.C_PROPERTY, tProperty.getBackground());
		FeedSetting.putColor(FeedSetting.C_INFRASTRUCTURE,
				tInfra.getBackground());
		FeedSetting.putColor(FeedSetting.C_FINANCE, tFinance.getBackground());
		FeedSetting.putColor(FeedSetting.C_TRADE, tTrade.getBackground());

		FeedSetting.putColor(FeedSetting.C_BGOV, tb1.getBackground());
		FeedSetting.putColor(FeedSetting.C_BFOREIGN, tb2.getBackground());
		FeedSetting.putColor(FeedSetting.C_BLOCAL, tb3.getBackground());

		FeedSetting.putSetting(FeedSetting.C_BROKER1, tBroker1.getText().trim()
				.toUpperCase());
		FeedSetting.putSetting(FeedSetting.C_BROKER2, tBroker2.getText().trim()
				.toUpperCase());
		FeedSetting.putSetting(FeedSetting.C_BROKER3, tBroker3.getText().trim()
				.toUpperCase());
		String name = (String) cbFont.getSelectedItem();
		int style = cbStyle.getSelectedIndex();
		int size = ((Integer) textSize.getValue()).intValue();
		FeedSetting.putFont(new Font(name, style, size));
	}

	private void createEvent() {
		okListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch (numChooser) {
				case 1:
					tBack.setBackground(colorChooser.getColor());
					break;
				case 2:
					tBack2.setBackground(colorChooser.getColor());
					break;
				case 3:
					tNormal.setBackground(colorChooser.getColor());
					break;
				case 4:
					tUp.setBackground(colorChooser.getColor());
					break;
				case 5:
					tDown.setBackground(colorChooser.getColor());
					break;
				case 6:
					tFlat.setBackground(colorChooser.getColor());
					break;
				case 7:
					tCrossing.setBackground(colorChooser.getColor());
					break;
				case 8:
					tForeign.setBackground(colorChooser.getColor());
					break;
				case 9:
					tDomestic.setBackground(colorChooser.getColor());
					break;
				case 10:
					tBrokerColor1.setBackground(colorChooser.getColor());
					break;
				case 11:
					tBrokerColor2.setBackground(colorChooser.getColor());
					break;
				case 12:
					tBrokerColor3.setBackground(colorChooser.getColor());
					break;
				case 13:
					tAgri.setBackground(colorChooser.getColor());
					break;
				case 14:
					tMining.setBackground(colorChooser.getColor());
					break;
				case 15:
					tBasic.setBackground(colorChooser.getColor());
					break;
				case 16:
					tMisc.setBackground(colorChooser.getColor());
					break;
				case 17:
					tConsumer.setBackground(colorChooser.getColor());
					break;
				case 18:
					tProperty.setBackground(colorChooser.getColor());
					break;
				case 19:
					tInfra.setBackground(colorChooser.getColor());
					break;
				case 20:
					tFinance.setBackground(colorChooser.getColor());
					break;
				case 21:
					tTrade.setBackground(colorChooser.getColor());
					break;
				case 22:
					tb1.setBackground(colorChooser.getColor());
					break;
				case 23:
					tb2.setBackground(colorChooser.getColor());
					break;
				case 24:
					tb3.setBackground(colorChooser.getColor());
					break;
				default:
					break;
				}
			}
		};
		createMouseListener(tBack, 1);
		createMouseListener(tBack2, 2);
		createMouseListener(tNormal, 3);
		createMouseListener(tUp, 4);
		createMouseListener(tDown, 5);
		createMouseListener(tFlat, 6);
		createMouseListener(tCrossing, 7);
		createMouseListener(tForeign, 8);
		createMouseListener(tDomestic, 9);
		createMouseListener(tBrokerColor1, 10);
		createMouseListener(tBrokerColor2, 11);
		createMouseListener(tBrokerColor3, 12);

		createMouseListener(tAgri, 13);
		createMouseListener(tMining, 14);
		createMouseListener(tBasic, 15);
		createMouseListener(tMisc, 16);
		createMouseListener(tConsumer, 17);
		createMouseListener(tProperty, 18);
		createMouseListener(tInfra, 19);
		createMouseListener(tFinance, 20);
		createMouseListener(tTrade, 21);
		createMouseListener(tb1, 22);
		createMouseListener(tb2, 23);
		createMouseListener(tb3, 24);

		dialog = JColorChooser.createDialog(frame, "Pick a Colour", true,
				colorChooser, okListener, null);
		dialog.setModal(true);
	}
}
