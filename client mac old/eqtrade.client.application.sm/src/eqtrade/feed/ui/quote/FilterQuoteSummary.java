package eqtrade.feed.ui.quote;

import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.StockSummary;

public class FilterQuoteSummary extends FilterBase {
	private HashMap mapFilter = new HashMap();

	public FilterQuoteSummary(Component parent) {
		super(parent, "filter");
		mapFilter.put("stock", new FilterColumn("stock", Vector.class, null,FilterColumn.C_MEMBEROF));
		mapFilter.put("board", new FilterColumn("board", String.class,new String("RG"), FilterColumn.C_EQUAL));
		mapFilter.put("freq", new FilterColumn("freq", Double.class, null,FilterColumn.C_GREATER));
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public int showDialog() {
		return super.showDialog();
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			StockSummary dat = (StockSummary) val.getSource();
			if (((FilterColumn) mapFilter.get("board")).compare(dat.getBoard()) && ((FilterColumn) mapFilter.get("stock")).compare(dat.getCode()) && ((FilterColumn) mapFilter.get("freq")).compare(dat.getTradedFrequency())) {
				avail = true;
			}
			return avail;
		} catch (Exception ec) {
			return false;
		}
	}
}