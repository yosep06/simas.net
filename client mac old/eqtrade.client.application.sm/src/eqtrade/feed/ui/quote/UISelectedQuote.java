package eqtrade.feed.ui.quote;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.Keymap;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.vollux.framework.UI;
import com.vollux.model.FilteredAndSortedTableModel;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Board;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;
import eqtrade.feed.ui.core.UIDlgStock;

public class UISelectedQuote extends UI {
	private JGrid table;
	private FilterQuoteSummary filter;
	private Vector vStock = new Vector();
	private Vector vStockTn= new Vector();
	private JMenuItem addMenu;
	private JMenuItem editMenu;
	private JMenuItem removeMenu;
	private JPopupMenu popupMenu2;
	private JPanel pnlboard;
	private JDropDown comboBoard;
	private FilterBoard filterBoard;
	
	public UISelectedQuote(String app) {
		super("Selected Quote", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_SELECTEDQUOTE);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		popupMenu2 = new JPopupMenu();
		JMenuItem mnBrokerSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_BROKERSUMMARY, param);
				}
			}
		});
		mnBrokerSumm.setText("Buy/Sell Broker Summary");
		popupMenu.add(mnBrokerSumm);

		JMenuItem mnBrokerActivity = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_BROKERACTIVITY, param);
				}
			}
		});
		mnBrokerActivity.setText("Broker Activity by Stock");
		popupMenu.add(mnBrokerActivity);

		JMenuItem mnInvStock = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_INVSTOCK, param);
				}
			}
		});
		mnInvStock.setText("Foreign/Domestic by Stock");
		popupMenu.add(mnInvStock);

		JMenuItem mnTimeTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_TIMETRADE, param);
				}
			}
		});
		mnTimeTrade.setText("Time & Trade Summary");
		popupMenu.add(mnTimeTrade);

		JMenuItem mnCompany = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					int i = table.getMappedRow(table.getSelectedRow());
					if (i > -1) {
						StockSummary order = (StockSummary) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_STOCKSUMMARY)
								.getDataByIndex(i);
						// com.vollux.framework.Utils.openBrowser("http://www.sinarmassekuritas.co.id/emiten/"+
						// (order.getCode().equals("") ? "" :(
						// order.getCode()+".html")));
						HashMap param = new HashMap();
						param.put("STOCK", order.getCode());
						apps.getUI().showUI(FeedUI.UI_COMPANYPROFILE, param);
					}
				} catch (Exception ex) {
					Utils.showMessage("cannot open Browser", null);
				}
			}
		});
		mnCompany.setText("Company Profile");
		popupMenu.add(mnCompany);
		JMenuItem mnIntraday = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_INTRADAY, param);
				}
			}
		});
		mnIntraday.setText("Intraday Chart");
		popupMenu.add(mnIntraday);

		JMenuItem mnTechnical = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_CHART, param);
				}
			}
		});
		mnTechnical.setText("Technical Chart");
		popupMenu.add(mnTechnical);

		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		addMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				UI dlg = apps.getUI().getForm(FeedUI.UI_DLGSTOCK);
				HashMap map = new HashMap();
				map.put("type", UIDlgStock.C_ADD);
				apps.getUI().showUI(FeedUI.UI_DLGSTOCK, map);
				if (UIDlgStock.C_RESULT == UIDlgStock.C_OK) {
					if(comboBoard.getText().equals("RG")){
						if (!vStock.contains(dlg.getProperty("stock")))
							vStock.add(dlg.getProperty("stock"));
					}else {
						vStockTn.add(dlg.getProperty("stock"));
					}
					filter.fireFilterChanged();
					focus();
				}
			}
		});
		addMenu.setText("Add Stock");
		addMenu.setActionCommand("Add");
		popupMenu2.add(addMenu);

		editMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					UI dlg = apps.getUI().getForm(FeedUI.UI_DLGSTOCK);
					HashMap map = new HashMap();
					map.put("type", UIDlgStock.C_EDIT);
					map.put("stock", order.getCode());
					apps.getUI().showUI(FeedUI.UI_DLGSTOCK, map);
					if (UIDlgStock.C_RESULT == UIDlgStock.C_OK) {
						if(comboBoard.getText().equals("RG")){
							vStock.remove(order.getCode());
							vStock.add(dlg.getProperty("stock"));
						}else{
							vStockTn.remove(order.getCode());
							vStockTn.add(dlg.getProperty("stock"));
						}
						filter.fireFilterChanged();
					}
				}
			}
		});
		editMenu.setText("Edit Stock");
		editMenu.setActionCommand("Edit");
		popupMenu2.add(editMenu);

		removeMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					if(comboBoard.getText().equals("RG")){					
						vStock.remove(order.getCode());
					}else{
						vStockTn.remove(order.getCode());
					}
					filter.fireFilterChanged();
				}
			}
		});
		removeMenu.setText("Remove Stock");
		removeMenu.setActionCommand("Remove");
		popupMenu2.add(removeMenu);
	}

	@Override
	protected void build() {
		createPopup();
		filter = new FilterQuoteSummary(pnlContent);
		((FilterColumn) filter.getFilteredData("stock")).setField(vStock);
		
		table = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY), filter,(Hashtable) hSetting.get("table"));
		table.getTable().add(popupMenu);
		
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popupMenu2.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					popupMenu2.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		table.getTable().registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				addMenu.doClick();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, 0, true),JComponent.WHEN_FOCUSED);
		
		table.getTable().registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				removeMenu.doClick();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0, true),JComponent.WHEN_FOCUSED);

		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE, Model.CIDX_SEQNO, StockSummary.CIDX_REMARKS, StockSummary.CIDX_BOARD, StockSummary.CIDX_FOREIGNERS, StockSummary.CIDX_INDIVIDUALINDEX, StockSummary.CIDX_AVG, StockSummary.CIDX_TRADEDLOT });
		
		FilteredAndSortedTableModel m = new FilteredAndSortedTableModel(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_BOARD));
		filterBoard = new FilterBoard(pnlContent);
		m.setFilter(filterBoard);
		comboBoard = new JDropDown(m, Board.CIDX_NAME);
		comboBoard.setBorder(new EmptyBorder(0, 0, 0, 0));
		comboBoard.setSelectedItem("RG");
		
		pnlboard = new JPanel();
		FormLayout l = new FormLayout("50px,2dlu,50px","pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlboard);
		pnlboard.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(comboBoard, c.xy(1, 1));
		
		comboBoard.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {

				((FilterColumn) filter.getFilteredData("board")).setField(comboBoard.getText().toString());
				if(comboBoard.getText().equals("RG")) {
					((FilterColumn) filter.getFilteredData("stock")).setField(vStock);
				} else {
					((FilterColumn) filter.getFilteredData("stock")).setField(vStockTn);
				}
				filter.fireFilterChanged();
			}
		});
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		pnlContent.add(pnlboard, BorderLayout.NORTH);
		refresh();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", StockSummaryDef.getTableDef());
		}
		
		if (hSetting.get("size") == null) hSetting.put("size", new Rectangle(20, 20, 400, 300));
		
		vStock = (Vector) hSetting.get("stockrg");
		vStockTn = (Vector) hSetting.get("stocktn");
		
		if (vStock == null) vStock = new Vector();
		
		if (vStockTn == null) vStockTn = new Vector();
		
		// tambahan supaya tidak delete scr.
		
		//System.out.println("before hsetting : "+hSetting);
		((Hashtable) hSetting.get("table")).put("header", StockSummaryDef.dataHeader.clone());
		//((Hashtable) hSetting.get("table")).put("order", StockSummaryDef.defaultColumnOrder.clone());
		//((Hashtable) hSetting.get("table")).put("sorted", StockSummaryDef.columnsort.clone());
		//System.out.println("after hsetting : "+hSetting);
	}

	@Override
	public void saveSetting() {
		hSetting.put("size",new Rectangle(form.getX(), form.getY(), form.getWidth(), form.getHeight()));
		hSetting.put("stock",((FilterColumn) filter.getFilteredData("stock")).getField());
		hSetting.put("stockrg", vStock);
		hSetting.put("stocktn", vStockTn);
		hSetting.put("table", table.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
	}
}