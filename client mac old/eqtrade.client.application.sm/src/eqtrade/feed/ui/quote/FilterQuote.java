package eqtrade.feed.ui.quote;

import java.util.HashMap;

import javax.swing.JComponent;
import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.Quote;

public class FilterQuote extends FilterBase {
	private HashMap mapFilter = new HashMap();

	public FilterQuote(JComponent parent) {
		super(parent, "filter");
		mapFilter.put("stock", new FilterColumn("stock", String.class,
				new String(""), FilterColumn.C_EQUAL));
		mapFilter.put("bo", new FilterColumn("bo", String.class, null,
				FilterColumn.C_EQUAL));
		mapFilter.put("board", new FilterColumn("board", String.class, null,
				FilterColumn.C_EQUAL));
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public int showDialog() {
		return super.showDialog();
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			Quote dat = (Quote) val.getSource();
			if (((FilterColumn) mapFilter.get("stock")).compare(dat.getCode())
					&& ((FilterColumn) mapFilter.get("bo")).compare(dat
							.getSide())
					&& ((FilterColumn) mapFilter.get("board")).compare(dat
							.getBoard())) {
				avail = true;
			}
			return avail;
		} catch (Exception ec) {
			return false;
		}
	}
}