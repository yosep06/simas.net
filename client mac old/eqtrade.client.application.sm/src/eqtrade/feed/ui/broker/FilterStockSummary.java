package eqtrade.feed.ui.broker;

import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.StockSummary;

public class FilterStockSummary extends FilterBase {
	private HashMap mapFilter = new HashMap();

	public FilterStockSummary(Component parent) {
		super(parent, "filter");
		mapFilter.put("stock", new FilterColumn("stock", String.class,
				new String(""), FilterColumn.C_EQUAL));
		mapFilter.put("board", new FilterColumn("board", String.class,
				new String("RG"), FilterColumn.C_EQUAL));
		mapFilter.put("multistock", new FilterColumn("stock",
				Vector.class, null, FilterColumn.C_MEMBEROF));
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public int showDialog() {
		return super.showDialog();
	}

	@Override
	public boolean filter(TableModel model, int row) {
	
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			StockSummary dat = (StockSummary) val.getSource();
			if (((FilterColumn) mapFilter.get("board")).compare(dat.getBoard())
					&& ((FilterColumn) mapFilter.get("stock")).compare(dat
						.getCode())
					&& ((FilterColumn) mapFilter.get("multistock")).compare(dat
							.getCode())) {
				avail = true;
			}
			return avail;
		} catch (Exception ec) {
			return false;
		}
	}
}