package eqtrade.feed.ui.news;

import java.awt.Component;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.Rups;

public class FilterStockRup extends FilterBase{

	public FilterStockRup(Component parent) {
		super(parent, "filter");
		mapFilter.put("stock", new FilterColumn("stock", String.class, null, 
				FilterColumn.C_EQUAL));
		
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try{
			Column val = (Column) model.getValueAt(row, 0);
			Rups dat = (Rups)val.getSource();
			if( ((FilterColumn)mapFilter.get("stock")).compare(dat.getStock())){
				avail = true;
			}return avail;
		}catch (Exception e) {
			return false;		
		}
		
	}

	@Override
	public Object getFilteredData(String name) {
		// TODO Auto-generated method stub
		return mapFilter.get(name);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
		
	}

}
