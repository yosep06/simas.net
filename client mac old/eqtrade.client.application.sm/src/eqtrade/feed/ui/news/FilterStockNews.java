package eqtrade.feed.ui.news;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;


import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;


import eqtrade.feed.model.StockNews;

public class FilterStockNews extends FilterBase {
private HashMap mapFilter = new HashMap();

	public FilterStockNews(Component parent) {
		super(parent, "filter");
		mapFilter.put("newsstock", new FilterColumn("newsstock",
				String.class,null, FilterColumn.C_EQUAL));
	}
	
	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();		
	}
	
	public void setFilter(HashMap map){
		if(map !=null){
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}
	public HashMap getFilter(){
		return mapFilter;
	}
	public int showDialog(){
		return super.showDialog();
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try{
			Column val = (Column) model.getValueAt(row, 0);
			StockNews dat = (StockNews) val.getSource();
			if(((FilterColumn) mapFilter.get("newsstock")).compare(dat.getNewsstock())){
				avail = true;
			}
			return avail;
		} catch (Exception e) {
			return false;
		}	
	}
}