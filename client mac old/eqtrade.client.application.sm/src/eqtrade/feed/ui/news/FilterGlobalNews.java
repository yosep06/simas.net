package eqtrade.feed.ui.news;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.Article;

public class FilterGlobalNews extends FilterBase {
	private HashMap mapFilter = new HashMap();

	public FilterGlobalNews(Component parent) {
		super(parent, "filter");
		mapFilter.put("subject", new FilterColumn("subject", String.class, null, FilterColumn.C_EQUAL));
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public int showDialog() {
		return super.showDialog();
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			Article dat = (Article) val.getSource();
			if (((FilterColumn) mapFilter.get("subject")).compare(dat.getSubject())) {
				avail = true;
			}
			return avail;
		} catch (Exception ec) {
			return false;
		}
	}
}