package eqtrade.feed.ui.news;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.CorpAction;
import eqtrade.feed.model.Rups;
import eqtrade.feed.model.StockNews;

public class FilterDevidenNews extends FilterBase{
	private HashMap mapFilter = new HashMap();
	
	public FilterDevidenNews(Component parent) {
		super(parent, "filter");
		mapFilter.put("newsstock", new FilterColumn("newsstock",
				String.class,null, FilterColumn.C_EQUAL));
	}

	

	@Override
	public Object getFilteredData(String name) {
		// TODO Auto-generated method stub
		return mapFilter.get(name);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
		
	}
	public HashMap getFilter(){
		return mapFilter;
	}
	public int showDialog(){
		return super.showDialog();
	}
	@Override
	public boolean filter(TableModel model, int row) {
		// TODO Auto-generated method stub
		boolean avail = false;
		try{
			Column val = (Column) model.getValueAt(row, 0);
			StockNews dat = (StockNews) val.getSource();
			CorpAction dat1 = (CorpAction) val.getSource();
			Rups dat2 = (Rups) val.getSource();

			
			if(((FilterColumn) mapFilter.get("newsstock")).compare(dat1.getStock())){
				avail = true;
			}
			return avail;
		}catch (Exception e) {
			return false;
		}
	}

}
