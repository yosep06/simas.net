package eqtrade.feed.ui.market;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.LengthAdjustmentType;

//import vollux.chart.core.ChartContrainerIntraday;
//import vollux.chart.core.CustomIntradayAxis;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Indices;
import eqtrade.feed.model.MarketDef;
import eqtrade.feed.model.Summary;
import eqtrade.trading.market.chart.core.ChartContrainerIntraday;
import eqtrade.trading.market.chart.core.CustomIntradayAxis;

public class UIMarketSummary extends UI {
	private JGrid table;
	private JPanel pnlInfo;
	private ChartContrainerIntraday chart;
	private XYDataset xydataset;
	private TimeSeries timeseries;
	private XYItemRenderer render;

	private JLabel fieldAdvancer;
	private JLabel fieldDecliner;
	private JLabel fieldUnchange;
	private JLabel fieldUntrade;
	private JLabel fieldIndices;
	private JLabel fieldChange;
	private JLabel fieldPercent;

	private JLabel fieldOpen;
	private JLabel fieldHigh;
	private JLabel fieldLow;
	private JLabel fieldPrev;
	private ValueMarker marker;
	private ValueMarker marker2;

	private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	private static NumberFormat formatter2 = new DecimalFormat("#,##0.000  ");

	public UIMarketSummary(String app) {
		super("Market Chart & Detail", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_MARKETSUMMARY);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		table = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_MARKET), null,
				(Hashtable) hSetting.get("table"));
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		fieldAdvancer = new JLabel(" ", SwingConstants.RIGHT);
		fieldDecliner = new JLabel(" ", SwingConstants.RIGHT);
		fieldUnchange = new JLabel(" ", SwingConstants.RIGHT);
		fieldUntrade = new JLabel(" ", SwingConstants.RIGHT);
		fieldIndices = new JLabel(" ", SwingConstants.RIGHT);
		fieldChange = new JLabel(" ", SwingConstants.RIGHT);
		fieldPercent = new JLabel(" ", SwingConstants.RIGHT);
		fieldOpen = new JLabel(" ", SwingConstants.RIGHT);
		fieldHigh = new JLabel(" ", SwingConstants.RIGHT);
		fieldLow = new JLabel(" ", SwingConstants.RIGHT);
		fieldPrev = new JLabel(" ", SwingConstants.RIGHT);
		fieldIndices.setFont(new Font("dialog", 1, 20));

		pnlInfo = new JPanel();
		FormLayout l = new FormLayout(
				"pref,2dlu,pref, 10dlu, pref, 2dlu, 50px:grow, 10dlu, pref, 2dlu, 50px:grow",
				"pref, 2dlu, pref, 2dlu, pref,2dlu, pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(new JLabel("adv"), c.xy(1, 1));
		b.add(fieldAdvancer, c.xy(3, 1));
		b.add(new JLabel("dec"), c.xy(1, 3));
		b.add(fieldDecliner, c.xy(3, 3));
		b.add(new JLabel("unchg"), c.xy(1, 5));
		b.add(fieldUnchange, c.xy(3, 5));
		b.add(new JLabel("untrade"), c.xy(1, 7));
		b.add(fieldUntrade, c.xy(3, 7));

		b.add(new JLabel("last"), c.xywh(5, 1, 1, 3));
		b.add(fieldIndices, c.xywh(7, 1, 1, 3));
		b.add(new JLabel("chg"), c.xy(5, 5));
		b.add(fieldChange, c.xy(7, 5));
		b.add(new JLabel("%"), c.xy(5, 7));
		b.add(fieldPercent, c.xy(7, 7));

		b.add(new JLabel("open"), c.xy(9, 1));
		b.add(fieldOpen, c.xy(11, 1));
		b.add(new JLabel("high"), c.xy(9, 3));
		b.add(fieldHigh, c.xy(11, 3));
		b.add(new JLabel("low"), c.xy(9, 5));
		b.add(fieldLow, c.xy(11, 5));
		b.add(new JLabel("prev"), c.xy(9, 7));
		b.add(fieldPrev, c.xy(11, 7));

		chart = new ChartContrainerIntraday();
		chart.getPanel().setPreferredSize(new Dimension(200, 200));
		// try {
		// chart.getDateAxis().setRange(timeFormat.parse(dateFormat.format(new
		// Date())+" 092500"), timeFormat.parse(dateFormat.format(new
		// Date())+" 160000"));
		// } catch (Exception ex){}
		XYPlot xyplot = chart.getPlot();
		xyplot.setDomainAxis(new CustomIntradayAxis("", new Date(), chart
				.getTimeline()));
		marker = new ValueMarker(0);
		marker.setLabelOffsetType(LengthAdjustmentType.EXPAND);
		marker.setPaint(Color.green);
		marker.setOutlineStroke(new BasicStroke(2F));
		chart.getMiddlePlot().addRangeMarker(marker);

		marker2 = new ValueMarker(0);
		marker2.setLabelOffsetType(LengthAdjustmentType.EXPAND);
		marker2.setPaint(Color.green);
		marker2.setOutlineStroke(new BasicStroke(2F));
		chart.getMiddlePlot().addDomainMarker(marker2);
		try {
			marker2.setValue(timeFormat.parse(
					dateFormat.format(new Date()) + " 130000").getTime());
		} catch (Exception ex) {
		}

		StandardChartTheme.createDarknessTheme().apply(
				chart.getPanel().getChart());
		render = new XYLineAndShapeRenderer(true, false);
		timeseries = new TimeSeries("composite");
		xydataset = new TimeSeriesCollection(timeseries);
		chart.addPlot("", name, xydataset, null, render);
		render.setBaseToolTipGenerator(new StandardXYToolTipGenerator(
				"({1}, {2})", new SimpleDateFormat("HH:mm:ss"),
				new DecimalFormat("0.00")));
		render.setSeriesPaint(0, Color.yellow);
		render.setBasePaint(Color.white);
		render.setSeriesStroke(0, new BasicStroke(1F));

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		JSkinPnl header = new JSkinPnl();
		header.add(chart.getPanel(), BorderLayout.CENTER);
		header.add(b.getPanel(), BorderLayout.SOUTH);
		pnlContent.add(header, BorderLayout.NORTH);
		pnlContent.add(table, BorderLayout.CENTER);
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_INDICES)
				.addTableModelListener(new TableModelListener() {
					@Override
					public void tableChanged(TableModelEvent e) {
						indicesChanged();
					}
				});
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_INDICES)
				.refresh();
		((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_MARKETSUMM)
				.addTableModelListener(new TableModelListener() {
					@Override
					public void tableChanged(TableModelEvent e) {
						summChanged();
					}
				});
		((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_MARKETSUMM).refresh();
		refresh();
	}

	private void indicesChanged() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Indices index = (Indices) ((IEQTradeApp) apps).getFeedEngine()
						.getStore(FeedStore.DATA_INDICES)
						.getDataByKey(new Object[] { "COMPOSITE" });
				if (index != null) {
					marker.setValue(index.getPrev().doubleValue());
					fieldIndices.setText(formatter2.format(index.getLast()));
					fieldPercent.setText(formatter2.format(index.getPercent()));
					fieldChange.setText(formatter2.format(index.getChange()));
					fieldOpen.setText(formatter2.format(index.getOpen()));
					fieldHigh.setText(formatter2.format(index.getHigh()));
					fieldLow.setText(formatter2.format(index.getLow()));
					fieldPrev.setText(formatter2.format(index.getPrev()));
					fieldIndices
							.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
									.getColor(FeedSetting.C_PLUS)
									: (index.getChange().doubleValue() < 0) ? FeedSetting
											.getColor(FeedSetting.C_MINUS)
											: FeedSetting
													.getColor(FeedSetting.C_ZERO));
					fieldPercent
							.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
									.getColor(FeedSetting.C_PLUS)
									: (index.getChange().doubleValue() < 0) ? FeedSetting
											.getColor(FeedSetting.C_MINUS)
											: FeedSetting
													.getColor(FeedSetting.C_ZERO));
					fieldChange
							.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
									.getColor(FeedSetting.C_PLUS)
									: (index.getChange().doubleValue() < 0) ? FeedSetting
											.getColor(FeedSetting.C_MINUS)
											: FeedSetting
													.getColor(FeedSetting.C_ZERO));
					fieldOpen
							.setForeground(index.getOpen().doubleValue() > index
									.getPrev().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (index
									.getOpen().doubleValue() < index.getPrev()
									.doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldHigh
							.setForeground(index.getHigh().doubleValue() > index
									.getLast().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (index
									.getHigh().doubleValue() < index.getLast()
									.doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldLow.setForeground(index.getLow().doubleValue() > index
							.getLast().doubleValue() ? FeedSetting
							.getColor(FeedSetting.C_PLUS)
							: (index.getLow().doubleValue() < index.getLast()
									.doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldPrev.setForeground(FeedSetting
							.getColor(FeedSetting.C_ZERO));
					try {
						if (interval == Integer.parseInt(index.getTranstime()) 
								&& transtime.equals(index.getTranstime())) //&& (Integer.parseInt(index.getTranstime())%5)<1
						{
							Date t = timeFormat.parse(dateFormat.format(new Date())
								+ " " + index.getTranstime());
							timeseries.addOrUpdate(
								Second.parseSecond(datetimeFormat.format(t)),
								index.getLast());
							interval = Integer.parseInt(index.getTranstime())+5;
							transtime = index.getTranstime();
							timeseries.removeAgedItems(false);
						} else if (interval<Integer.parseInt(index.getTranstime())){
							interval = Integer.parseInt(index.getTranstime());
							transtime = index.getTranstime();
						}
					} catch (Exception ex) {
						ex.printStackTrace();log.info(ex.getMessage());
					}
				} else {
					fieldIndices.setText("- ");
					fieldPercent.setText("- ");
					fieldChange.setText("- ");
					fieldOpen.setText("- ");
					fieldHigh.setText("- ");
					fieldLow.setText("- ");
					fieldPrev.setText("- ");
				}
			}
		});
	}

	int interval = 0;
	String transtime = "";
	private void summChanged() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Summary summ = (Summary) ((IEQTradeApp) apps).getFeedEngine()
						.getStore(FeedStore.DATA_MARKETSUMM)
						.getDataByKey(new Object[] { "SUMMARY" });
				if (summ != null) {
					fieldAdvancer.setText(formatter.format(summ.getInfo1()
							.doubleValue()));
					fieldDecliner.setText(formatter.format(summ.getInfo2()
							.doubleValue()));
					fieldUnchange.setText(formatter.format(summ.getInfo3()
							.doubleValue()));
					fieldUntrade.setText(formatter.format(summ.getInfo4()
							.doubleValue()));
				}
			}
		});
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", MarketDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 400, 480));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		pnlInfo.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
		fieldAdvancer.setForeground(FeedSetting.getColor(FeedSetting.C_PLUS));
		fieldDecliner.setForeground(FeedSetting.getColor(FeedSetting.C_MINUS));
		fieldUnchange.setForeground(FeedSetting.getColor(FeedSetting.C_ZERO));
		indicesChanged();
	}

	Thread threadLoad = null;

	private void queryChart() {
		// SwingUtilities.invokeLater(new Runnable(){
		if (threadLoad != null) {
			threadLoad.stop();
			threadLoad = null;
		}
		threadLoad = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					((TimeSeriesCollection) xydataset).removeAllSeries();
					timeseries.clear();
					String result = ((IEQTradeApp) apps).getFeedEngine()
							.getEngine().getConnection()
							.getHistory("CHI" + "|" + "COMPOSITE");
					if (result != null) {
						String as[] = result.split("\n", -2);
						int aslength = as.length - 1;
						for (int j = 0; j < aslength; j++) {
							String s1 = as[j];
							if (!s1.trim().equals("")) {
								String detail[] = s1.split("\\|");
								if ((Integer.parseInt(detail[0]) % 5)<1) {
									Date t = timeFormat.parse(dateFormat
											.format(new Date()) + " " + detail[0]);

//									 log.info(detail[0]+
//									 "   "+Double.parseDouble(detail[2]));

									try {
										timeseries.addOrUpdate(Second
												.parseSecond(datetimeFormat
														.format(t)), Double
												.parseDouble(detail[2]));
									} catch (Exception ex) {
										ex.printStackTrace();
	//									log.info("error processing: "
	//											+ s1+" "+ex.getMessage());
	//									System.out.println("error processing: "

	//											+ s1);
									}
									interval = Integer.parseInt(detail[0]);
									transtime = detail[0];
								}
							}
						}
						((TimeSeriesCollection) xydataset)
								.addSeries(timeseries);
						chart.updateDataset("", name, xydataset);
						// chart.getPanel().repaint();
					} else {
						((TimeSeriesCollection) xydataset)
								.addSeries(timeseries);
						chart.updateDataset("", name, xydataset);
						//System.out.println("new data not available");
					}
					timeseries.removeAgedItems(false);
				} catch (Exception ex) {
					ex.printStackTrace();
					log.info("error processing:  "+ex.getMessage());
				}
			}
		});
		threadLoad.start();
	}

	@Override
	public void show() {
		if (!form.isVisible()) {
			queryChart();
		}
		super.show();
	}

	@Override
	public void close(){
		try {
			((TimeSeriesCollection) xydataset).removeAllSeries();
			timeseries.clear();
			super.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	private static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat(
			"yyyy-MM-dd HHmmss");
	private static SimpleDateFormat datetimeFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
}
