package eqtrade.feed.ui.investor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.MarketDef;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;
import eqtrade.feed.ui.stock.FilterTradeSummary;

public class UIInvByMarket extends UI {
	private JGrid table;
	private JGrid tableInv;
	private JGrid tableInvStock;
	private FilterMarket filter;
	private FilterTradeSummary filterInv;
	private FilterTradeSummary filterInvStock;
	public UIInvByMarket(String app) {
		super("Foreign/Domestic by Market", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_INVMARKET);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		filter = new FilterMarket(pnlContent);
		filterInv = new FilterTradeSummary(pnlContent);
		((FilterColumn) filterInv.getFilteredData("broker")).setField("ALL");
		((FilterColumn) filterInv.getFilteredData("stock")).setField("ALL");
		((FilterColumn) filterInv.getFilteredData("investor")).setField(null);

		filterInvStock = new FilterTradeSummary(pnlContent);
		table = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_MARKET), filter,
				(Hashtable) hSetting.get("table"));
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 1) {
					apps.getUI().showUI(FeedUI.UI_MARKETSUMMARY);
				}
			}
		});

		tableInv = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_TRADESUMINV), filterInv,
				(Hashtable) hSetting.get("tableinv"));
		// tableInv.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableInv.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO, TradeSummary.CIDX_BOARD,
				TradeSummary.CIDX_STOCK, TradeSummary.CIDX_STOCKNAME,
				TradeSummary.CIDX_BROKER, TradeSummary.CIDX_BROKERNAME,
				TradeSummary.CIDX_BUYLOT, TradeSummary.CIDX_SELLLOT,
				TradeSummary.CIDX_LOTTOT, TradeSummary.CIDX_LOTNET,
				TradeSummary.CIDX_FREQTOT, TradeSummary.CIDX_BUYAVG,
				TradeSummary.CIDX_SELLAVG });

		tableInv.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if(!e.isPopupTrigger() && e.getClickCount() == 1){
					int i = tableInv.getMappedRow(tableInv.getSelectedRow());//System.out.println("mouse"+i);
						if (i > -1) {
							TradeSummary trade = (TradeSummary) ((IEQTradeApp) apps)
									.getFeedEngine()
									.getStore(FeedStore.DATA_TRADESUMINV)
									.getDataByIndex(i);
							((FilterColumn) filterInvStock.getFilteredData("investor"))
									.setField(trade.getInvestor());
							filterInvStock.fireFilterChanged();
						}
					}
			}
		});
		
		tableInvStock = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_TRADESUMMARKETINV),
				 filterInvStock, (Hashtable) hSetting.get("tableinvstock"));
		tableInvStock.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO, TradeSummary.CIDX_BOARD,
				TradeSummary.CIDX_STOCKNAME,TradeSummary.CIDX_INVESTOR,TradeSummary.CIDX_BROKER,
				 TradeSummary.CIDX_BROKERNAME,
				TradeSummary.CIDX_BUYLOT, TradeSummary.CIDX_SELLLOT,
				TradeSummary.CIDX_LOTTOT, TradeSummary.CIDX_LOTNET,
				TradeSummary.CIDX_FREQTOT, TradeSummary.CIDX_BUYAVG,
				TradeSummary.CIDX_SELLAVG });		
		
		
		table.setPreferredSize(new Dimension(100, 50));
		tableInv.setPreferredSize(new Dimension(100, 150));
		
		JSkinPnl headerpnl = new JSkinPnl(new BorderLayout());
		headerpnl.setBorder(new EmptyBorder(2,2,2,2));
		headerpnl.add(table, BorderLayout.NORTH);
		headerpnl.add(tableInv, BorderLayout.CENTER);
				
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(headerpnl, BorderLayout.NORTH);
//		pnlContent.add(tableInv, BorderLayout.CENTER);
		pnlContent.add(tableInvStock, BorderLayout.CENTER);
		refresh();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", MarketDef.getTableDef());
			hSetting.put("tableinv", TradeSummaryDef.getTableInvDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 600, 350));
		
		if (hSetting.get("tableinvstock") == null) 
			hSetting.put("tableinvstock", TradeSummaryDef.getTableMarketInvDef());
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		hSetting.put("tableinv", tableInv.getTableProperties());
		hSetting.put("tableinvstock", TradeSummaryDef.getTableMarketInvDef());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableInv.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableInvStock.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
		tableInv.setNewFont(FeedSetting.getFont());
		tableInvStock.setNewFont(FeedSetting.getFont());
	}

	// public void show() {
	// if
	// (!form.isVisible())((IEQTradeApp)apps).getFeedEngine().subscribe(FeedParser.PARSER_TRADESUMMARY,
	// FeedParser.PARSER_TRADESUMMARY);
	// super.show();
	// }
	//
	// public void hide() {
	// ((IEQTradeApp)apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADESUMMARY,
	// FeedParser.PARSER_TRADESUMMARY);
	// super.hide();
	// }
	//
	// public void close() {
	// ((IEQTradeApp)apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADESUMMARY,
	// FeedParser.PARSER_TRADESUMMARY);
	// super.close();
	// }
	@Override
	public void show() {
		//System.out.println("show");
//		if (!form.isVisible())
			if (!((IEQTradeApp)apps).getUI().getForm(FeedUI.UI_INVSTOCK).isShow()) {
				((IEQTradeApp) apps).getFeedEngine().subscribe(
						FeedParser.PARSER_TRADESUMMSTOCKINV,
						FeedParser.PARSER_TRADESUMMSTOCKINV);	
			}
//		if(fieldStock.getText().length()>3)
//		{
//		fieldStockAction(null);
//    	fieldStockActionInv();
//    	fieldStockActionView();
//    	backTo(0);
//		}
		super.show();//System.out.println("cobaa#############show");
		 
	
	}

	@Override
	public void hide() {
		//System.out.println("hide");
		if (!((IEQTradeApp)apps).getUI().getForm(FeedUI.UI_INVSTOCK).isShow()) {
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(
					FeedParser.PARSER_TRADESUMMSTOCKINV,
					FeedParser.PARSER_TRADESUMMSTOCKINV);	
		}
		super.hide();
	}

	@Override
	public void close() {
		//System.out.println("close");
		if (!((IEQTradeApp)apps).getUI().getForm(FeedUI.UI_INVSTOCK).isShow()) {
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(
					FeedParser.PARSER_TRADESUMMSTOCKINV,
					FeedParser.PARSER_TRADESUMMSTOCKINV);	
		}
		super.close();
	}
	@Override
	public boolean isShow() {
		try {
		return (form.isVisible());	
		} catch (NullPointerException e) {
		// TODO: handle exception
			return false;
		}
	}
}
