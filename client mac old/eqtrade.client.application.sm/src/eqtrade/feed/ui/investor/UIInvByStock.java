package eqtrade.feed.ui.investor;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import sun.swing.BakedArrayList;



import net.sf.jasperreports.engine.xml.JRPenFactory.Right;
import net.sf.nachocalendar.CalendarFactory;
import net.sf.nachocalendar.components.DateField;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.lowagie.text.SplitCharacter;
import com.sun.org.apache.bcel.internal.generic.GETSTATIC;
import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IMessage;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Board;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.News;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;
import eqtrade.feed.ui.broker.FilterStockSummary;
import eqtrade.feed.ui.news.FilterCurrency;
import eqtrade.feed.ui.stock.FilterTradeSummary;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Order;

public class UIInvByStock extends UI implements IMessage{
	private JGrid table;
	private JGrid tableInv;
	private JPanel pnlInfo;
	private JPopupMenu RigthpopupMenu;	
	private FilterStockSummary filter;
	private FilterTradeSummary filterInv;
	private JTextField fieldStock;
	private JLabel fieldName;
	private JLabel fieldInfo;
	private JButton BtnView;
	private DateField DateFrom;
	private DateField DateTo;
	private JLabel FieldFrom;
	private JLabel FieldTo;
	private String oldStock = "";
	private int lengthField=0;
	private String[] arrField;
	int data_cek;
	String[] array_stock = {};
	String[] array_investor = {"D","F"};
	private GridModel model;
	private GridModel memmodel;
	private GridModel modelInv;
	Calendar calendar = Calendar.getInstance();
	public UIInvByStock(String app) {
		super("Foreign/Domestic by Stock", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_INVSTOCK);
	}

	@Override 
	public void focus() {
		//System.out.println("Focus");
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldStock.requestFocus();
			}
		});
	}
	protected void createRightPopup(){
		//System.out.println("RightPopUp");
	RigthpopupMenu = new JPopupMenu();
		
		JMenuItem Delete = new JMenuItem(new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				
				if (i > -1) {
				StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY)
							.getDataByIndex(i);
				String[] sName = Stocks(fieldStock.getText()); String fName ="";
				for (int a =0; a<sName.length;a++){
						if(sName[a].equals(order.getCode()))
						{	sName[a] = "";		}
						if(sName.length>0){	
							fName	= fName+";"+sName[a];
							fName	= fName.replace(";;", ";");	
							fName = removeCharAt(fName, 1);			}
						else{fName = sName[0];}
					}
			//	System.out.println("remove =="+removeCharAt(fName,1));
				
				fieldStock.setText(fName);
				
				if (fName.equals("")) {	
					fieldEmpty();
					}else if(table.getSelectedRow() == 0 ){backTo(1);}
				else{ 					
					backTo(0);
					}
				fieldStockAction(null);
				fieldStockActionInv();
				fieldStockActionView();
				backTo(0);
				
				}					
			}
		});
		Delete.setText("Delete");
		RigthpopupMenu.add(Delete);
	}
	public static String removeCharAt(String s, int pos) {
		//System.out.println("removeChart");
		char c=' '; StringBuffer buf = new StringBuffer( s );   
		if(s.substring(0,pos).equals(";"))
		   	{s=s.replaceFirst(";","");}
		if(s.endsWith(";"))
			{s=s.substring(0,s.length()-1);	}   
		return s;
	}
	private void backTo(final int c){
	
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			
			public void run() {				
				int i = table.getMappedRow(c);
				if(i>0){
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY)
							.getDataByIndex(i);
					
					((FilterColumn) filterInv.getFilteredData("stock"))
							.setField(order.getCode());
					filterInv.fireFilterChanged();	
				}
			}
		});
	
	}
	private void getRow(){//System.out.println("PopUp"); 
	int countArr=0;
		String[] sName = Stocks(fieldStock.getText()); 
		int i = table.getMappedRow(table.getSelectedRow());		
		StockSummary order = (StockSummary) ((IEQTradeApp) apps)
				.getFeedEngine()
				.getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY)
				.getDataByIndex(i);
		//System.out.println("order"+order);
		for (int a =0; a<sName.length;a++){ //System.out.println("arrfield[] "+arrField[a]+" sName[]"+sName[a]+"a ="+a);
		if(!arrField[a].equals(sName[a]))
				{countArr=a; break;}			
		}
		if(!arrField[arrField.length-1].equals(sName[sName.length-1])&&table.getSelectedRow()<1  ){
			countArr++;//System.out.println("arrleng "+arrField.length+" snaleng "+sName.length);
		}
		//System.out.println("arrfield[] "+arrField[countArr]+" order[]"+order.getCode()+"a ="+countArr);
		if(table.getSelectedRow()==0 
					&&order.getCode().equals(arrField[countArr]))backTo(1);
		else{// System.out.println(table.getSelectedRow());
			if(order.getCode().equals(arrField[countArr]))
			backTo(0);
			else{//System.out.println("TOOloooongggg");
			} }
	}
	private void fieldEmpty(){
		//System.out.println("fieldEmpty");
		((FilterColumn) filter.getFilteredData("multistock"))
		.setField(null);
		filter.fireFilterChanged();	
		((FilterColumn) filterInv.getFilteredData("stock"))
		.setField(null);
		filterInv.fireFilterChanged();
		fieldName.setText("please type a stock code");
		form.setTitle("Foreign/Domestic by " );
		oldStock = "";
	}
	String isHoliday(){
		String from = C_DATEFORMAT.format((Date) DateFrom.getValue())
				.trim();
		String to = C_DATEFORMAT.format((Date) DateTo.getValue())
				.trim();
		if (to.equalsIgnoreCase(C_DATEFORMAT.format(new Date()))&& !from.equalsIgnoreCase(C_DATEFORMAT.format(new Date()))) {//jika to = tgl sekarang  = mundur satu hari
			calendar.setTime(new Date(System.currentTimeMillis()-24*60*60*1000) );
			int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
			int day = 24;
			if (dayOfWeek == Calendar.SUNDAY) {
				day = 42;
			} else if (dayOfWeek == Calendar.SATURDAY){
				day = 24;
			}
			to = C_DATEFORMAT.format(new Date(System.currentTimeMillis()-day*60*60*1000) );			
		}
		return to;
		
	}
	@Override
	protected void createPopup() {
		//System.out.println("PopUp");
		popupMenu = new JPopupMenu();
		JMenuItem mnTimeTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_TIMETRADE, param);
				}
			}
		});
		mnTimeTrade.setText("Time & Trade Summary");
		popupMenu.add(mnTimeTrade);

		JMenuItem mnCompany = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					int i = table.getMappedRow(table.getSelectedRow());
					if (i > -1) {
						StockSummary order = (StockSummary) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY)
								.getDataByIndex(i);
						HashMap param = new HashMap();
						param.put("STOCK", order.getCode());
						// com.vollux.framework.Utils.openBrowser("http://www.sinarmassekuritas.co.id/emiten/"+
						// (order.getCode().equals("") ? "" :(
						// order.getCode()+".html")));
						apps.getUI().showUI(FeedUI.UI_COMPANYPROFILE, param);
					}
				} catch (Exception ex) {
					Utils.showMessage("cannot open Browser", null);
				}
			}
		});
		mnCompany.setText("Company Profile");
		popupMenu.add(mnCompany);
		JMenuItem mnIntraday = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_INTRADAY, param);
				}
			}
		});
		mnIntraday.setText("Intraday Chart");
		popupMenu.add(mnIntraday);

		JMenuItem mnTechnical = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_CHART, param);
				}
			}
		});
		mnTechnical.setText("Technical Chart");
		popupMenu.add(mnTechnical);
	}
	public static final SimpleDateFormat C_DATEFORMAT = new SimpleDateFormat(
	"yyyy-MM-dd");
	@Override
	protected void build() {
		//System.out.println("build");
		createPopup();
		createRightPopup();//klik kanan
		model = new GridModel(TradeSummaryDef.getHeader(), false);
		model.setKey(new int[]{TradeSummary.CIDX_STOCK, TradeSummary.CIDX_INVESTOR});
		memmodel = new GridModel(TradeSummaryDef.getHeader(), false);
		memmodel.setKey(new int[]{TradeSummary.CIDX_STOCK, TradeSummary.CIDX_INVESTOR});
		modelInv = new GridModel(StockSummaryDef.getHeader(), false);
		modelInv.setKey(new int [] {StockSummary.CIDX_CODE});
		
		oldStock = "";
		filter = new FilterStockSummary(pnlContent);//fungsi filtering
		//System.out.println("filter "+filter );
		filterInv = new FilterTradeSummary(pnlContent);
		((FilterColumn) filterInv.getFilteredData("broker")).setField("ALL");
		((FilterColumn) filterInv.getFilteredData("stock")).setField("");
		((FilterColumn) filterInv.getFilteredData("investor")).setField(null);
			
		table = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY), filter,(Hashtable) hSetting.get("table"));
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO, StockSummary.CIDX_REMARKS,
				StockSummary.CIDX_BESTBID, StockSummary.CIDX_BESTBIDVOLUME,
				StockSummary.CIDX_BESTOFFER, StockSummary.CIDX_BESTOFFERVOLUME,
				StockSummary.CIDX_BOARD, StockSummary.CIDX_FOREIGNERS,
				StockSummary.CIDX_INDIVIDUALINDEX, StockSummary.CIDX_PREVIOUS,
				StockSummary.CIDX_AVG, StockSummary.CIDX_OPENING,
				StockSummary.CIDX_CHANGE, StockSummary.CIDX_CLOSING,
				StockSummary.CIDX_PERCENT, StockSummary.CIDX_PREVIOUS,
				StockSummary.CIDX_HIGHEST, StockSummary.CIDX_LOWEST, 
				StockSummary.CIDX_SECTOR});
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}else if(e.getButton() == MouseEvent.BUTTON3//!e.isPopupTrigger() && 
						){
					RigthpopupMenu.show(e.getComponent(),e.getX(),e.getY());
				}
				else if(!e.isPopupTrigger() && e.getClickCount() == 1){
					int i = table.getMappedRow(table.getSelectedRow());//System.out.println("mouse"+i);
						if (i > -1) {
							StockSummary order = (StockSummary) ((IEQTradeApp) apps)
									.getFeedEngine()
									.getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY)
									.getDataByIndex(i);

							((FilterColumn) filterInv.getFilteredData("stock"))
									.setField(order.getCode());
							filterInv.fireFilterChanged();
						}
					}
			}
		});

		/*tableInv = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_TRADESUMSTOCKINV), filterInv,
				(Hashtable) hSetting.get("tableinv"));*/
		tableInv = createTable(
				model, filterInv,
				(Hashtable) hSetting.get("tableinv"));
		
		// tableInv.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableInv.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO, TradeSummary.CIDX_BOARD,
				TradeSummary.CIDX_STOCK, TradeSummary.CIDX_STOCKNAME,
				TradeSummary.CIDX_BROKER, TradeSummary.CIDX_BROKERNAME,
				TradeSummary.CIDX_BUYLOT, TradeSummary.CIDX_SELLLOT,
				TradeSummary.CIDX_LOTTOT, TradeSummary.CIDX_LOTNET,
				TradeSummary.CIDX_FREQTOT, TradeSummary.CIDX_BUYAVG,
				TradeSummary.CIDX_SELLAVG });

		pnlInfo = new JPanel();
		fieldStock = new JTextField("");
		fieldName = new JLabel("please type a stock code");
		fieldInfo = new JLabel("");
		FieldFrom = new JLabel("From");
		FieldTo = new JLabel(" To");
		BtnView = new JButton("View");
		DateFrom = CalendarFactory.createDateField();
		DateFrom.setDateFormat(C_DATEFORMAT);
		DateFrom.setValue(new Date());
		DateTo = CalendarFactory.createDateField();
		DateTo.setDateFormat(C_DATEFORMAT);
		DateTo.setValue(new Date());
		FormLayout l = new FormLayout("200px,2dlu,pref, 2dlu, 200px, pref,40px,pref,80px,1dlu,25px,1dlu,80px,1dlu,70px",
				"pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(fieldStock, c.xy(1, 1));
		b.add(fieldName, c.xy(5, 1));
		b.add(fieldInfo, c.xy(5, 1));
		b.add(FieldFrom, c.xy(7,1));
		b.add(DateFrom, c.xy(9, 1));
		b.add(FieldTo, c.xy(11,1));
		b.add(DateTo, c.xy(13, 1));
		b.add(BtnView, c.xy(15,1));
		int i = table.getMappedRow(0); //System.out.println(i);
		JSkinPnl header = new JSkinPnl();
		header.add(b.getPanel(), BorderLayout.NORTH);
		header.add(table, BorderLayout.CENTER);
		table.setPreferredSize(new Dimension(100, 100));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(header, BorderLayout.NORTH);
		pnlContent.add(tableInv, BorderLayout.CENTER);	
		Date date = new Date(((IEQTradeApp) apps).getFeedEngine()
				.getEngine().getConnection().getTime());
		String jam_data = new SimpleDateFormat("h").format(date);
		
		Integer jm_data = Integer.valueOf(jam_data);
		//System.out.println("jam data : "+jm_data);
		BtnView.addActionListener(new ActionListener() {
			
			@Override
			
			public void actionPerformed(ActionEvent evt) {
			
				if ((((Date) DateTo.getValue()).getTime() - ((Date) DateFrom
						.getValue()).getTime()) / 86400000L > 5) {
					Utils.showMessage(
							"Aborted, range parameter more than 5 days",
							null);
					return;
					
				}else
				{
					//System.out.println("registerKeyboard");
					((FilterColumn) filter.getFilteredData("stock"))
					.setField(null);
					((FilterColumn) filterInv.getFilteredData("stock"))
					.setField(null);
					filter.fireFilterChanged();
					filterInv.fireFilterChanged();
					
					if(fieldStock.getText().equals(""))
					{fieldEmpty();}
					else {
						if(fieldStock.getText().length() <6 && fieldStock.getText().length()>3){	
							((FilterColumn) filterInv.getFilteredData("stock"))
								.setField(fieldStock.getText());
							filterInv.fireFilterChanged();}
						else {
							if(lengthField == fieldStock.getText().length() ||lengthField < fieldStock.getText().length() ){ 
							//fungsi1
//							fieldStockAction(null);
//							fieldStockActionInv();
//							fieldStockActionView();
							}
							else {if(lengthField > fieldStock.getText().length()
									&&fieldStock.getText().length()>0){getRow();}
								 else backTo(0);//28-3-13
							}					
						}
					}
					//fungsi2		
					fieldStockAction(null);
					fieldStockActionInv();					
					fieldStockActionView();
					backTo(0);
					
					}
				}
				
			
		});
//===== Refresh 50 detik ================================
//		if(fieldStock.getText()!=""){
//			Timer timer = new Timer();
//
//			timer.schedule( new TimerTask() {
//			
//			    public void run() {
//			    	if(fieldStock.getText().length()>3 && form.isVisible())
//			    	{
//			    	//System.out.println("lima 50 detik");
//			    	fieldStockAction(null);
//			    	fieldStockActionInv();
//			    	fieldStockActionView();
//			    	backTo(0);
//			    	}
//			    }
//			 }, 0, 50*1000);
//		}
//===== End Refresh ========================================
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_TRADESUMSTOCKINV)
		.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				stockInvChange();
			}
		});
		fieldStock.registerKeyboardAction(new ActionListener() {
			
			@Override
			
			public void actionPerformed(ActionEvent evt) {
//				System.out.println("registerKeyboard");
//			if(fieldStock.getText().equals("")){
//				fieldEmpty();}
//				else {if(fieldStock.getText().length() <6 && fieldStock.getText().length()>3){	
//				System.out.println("enter "+fieldStock.getText());
//				
//				((FilterColumn) filterInv.getFilteredData("stock"))
//						.setField(fieldStock.getText());
//				filterInv.fireFilterChanged();
//				}
//				else {System.out.println("else key "+lengthField +" field"+fieldStock.getText().length());
//					if(lengthField == fieldStock.getText().length() ||lengthField < fieldStock.getText().length() ){ 
//					//fungsi1
//					fieldStockAction(null);
//					fieldStockActionInv();
//					fieldStockActionView();
//					}
//					else {if(lengthField > fieldStock.getText().length()
//							&&fieldStock.getText().length()>0){getRow();}
//						 else backTo(0);}					
//				}
//			}
//			//fungsi2		
//			fieldStockAction(null);
//			fieldStockActionInv();
//			fieldStockActionView();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		refresh();
		if (hSetting.get("stock") != null) {
			fieldStock.setText((String) hSetting.get("stock"));
			fieldStockAction(null);
		}//System.out.println("cobaa#############build");
	}

	protected void stockInvChange() {
		// TODO Auto-generated method stub
		
	}

	private void fieldStockAction(final String info) {
		SwingUtilities.invokeLater(new Runnable() {
				@Override
				
				public void run() {
					
					//System.out.println(fieldStock.getText().trim().toUpperCase().substring(-1));
			
						String newStock = fieldStock.getText().trim().toUpperCase();
						if(newStock.substring(newStock.length()-1,newStock.length()).equals(";"))
						{
							
						}else
						{
							newStock = newStock+";";

						}
						//System.out.println(newStock);
						String[] arrStock = Stocks(newStock);String fName ="";
						
						Vector<String> vboard = new Vector<String>();
						Vector<String> vboard_thsi = new Vector<String>();
						fieldStockActionInv();
						((FilterColumn) filter.getFilteredData("stock"))
								.setField(null);
						filter.fireFilterChanged();
						((FilterColumn) filterInv.getFilteredData("stock"))
						.setField(null);
						filterInv.fireFilterChanged();
				
						
						if ((newStock.length() > 0 && !oldStock.equals(newStock))) {					
							for (int i =0 ; i < Stocks(newStock).length;i++){
								fieldStock.setText(newStock);
								oldStock = newStock;
								vboard.addElement(arrStock[i].trim());
								vboard_thsi.addElement(arrStock[i].trim());
								if(arrStock.length>0)
									{fName = geStockName(arrStock[i].trim())+";"+fName;}
								else{fName = geStockName(arrStock[i].trim()); }
								}
								((FilterColumn) filter.getFilteredData("multistock"))
										.setField(vboard);
								
								//System.out.println("vboard : "+vboard);
								filter.fireFilterChanged();
								filterInv.fireFilterChanged();					
								fieldName.setText(fName);
								form.setTitle("Foreign/Domestic by " + oldStock);	
								lengthField = fieldStock.getText().length();
								arrField = arrStock;
								
						}
						fieldInfo.setText(info == null ? "" : info);
						fieldStock.selectAll();
							
						
					}
			});
		}
	private void fieldStockActionView() {// buat table bawah
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					String newStock = fieldStock.getText().trim().toUpperCase();
					
					
					array_stock=fieldStock.getText().trim().toUpperCase().split(";");
					
					String from = C_DATEFORMAT.format((Date) DateFrom.getValue())
							.trim();
					String to = isHoliday();/*C_DATEFORMAT.format((Date) DateTo.getValue())
							.trim();
					
					if (to.equalsIgnoreCase(C_DATEFORMAT.format(new Date()))&& !from.equalsIgnoreCase(C_DATEFORMAT.format(new Date()))) {//jika to = tgl sekarang  = mundur satu hari
						calendar.setTime(new Date(System.currentTimeMillis()-24*60*60*1000) );
						int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
						int day = 24;
						if (dayOfWeek == Calendar.SUNDAY) {
							day = 42;
						} else if (dayOfWeek == Calendar.SATURDAY){
							day = 24;
						}
						to = C_DATEFORMAT.format(new Date(System.currentTimeMillis()-day*60*60*1000) );
						
					}*/
					
					DateFormat forrmatter;
					Date date_from;
					Date date_to;
					String result=null;
						forrmatter = new SimpleDateFormat("yyyy-mm-dd");
						date_from = (Date)forrmatter.parse(from);
						date_to = (Date)forrmatter.parse(to);
						
						
						
						if (((Date) DateTo.getValue()).getTime() < ((Date) DateFrom
								.getValue()).getTime() && date_from.compareTo(date_to)>0) {
							Utils.showMessage(
									"Aborted, please enter valid date parameter",
									null);
							return;
						} else if ((((Date) DateTo.getValue()).getTime() - ((Date) DateFrom
								.getValue()).getTime()) / 86400000L > 5) {
							Utils.showMessage(
									"Warning, range parameter more than 5days, it will take some time",
									null);
							return;
						}
						
						fieldStock.setText(newStock);
						oldStock = newStock;
						
						form.setTitle("Foreign/Domestic by " + oldStock);

						if(newStock!="")
						{
							result = ((IEQTradeApp) apps)
							.getFeedEngine()
							.getEngine()
							.getConnection()
							.getHistory(
									"THSI" + "|" + from + "|"
											+ to);
						}	
					
					/*((IEQTradeApp) apps).getFeedEngine().getStore(
							FeedStore.DATA_TRADESUMSTOCKINV).getDataVector().clear();*/
					model.getDataVector().clear();
					memmodel.getDataVector().clear();

					//System.out.println("result stock "+newStock);
					if (to.equalsIgnoreCase(C_DATEFORMAT.format(new Date())) && from.equalsIgnoreCase(C_DATEFORMAT.format(new Date()))){
						for (int i = 0; i < array_stock.length; i++) {
							for (int j = 0; j < array_investor.length; j++) {
								int a =((IEQTradeApp)apps).getFeedEngine()
										.getStore(FeedStore.DATA_TRADESUMMARKETINV).getIndexByKey(new Object[]{array_stock[i],array_investor[j]}) ;
								if ( a > -1) {	
									TradeSummary trade = (TradeSummary)((IEQTradeApp)apps).getFeedEngine()
											.getStore(FeedStore.DATA_TRADESUMMARKETINV).getDataByKey(new Object[]{array_stock[i],array_investor[j]});
									genTrade(trade,trade);								
								}								
							}
						}
					}
					else if (!result.equals("null") && result != null) {
						String as[] = result.split("\n", -2);
						int aslength = as.length - 1;
						Vector vRow = new Vector(100, 5);
						for (int j = 0; j < aslength; j++) {
							String s1 = as[j];
							if (!s1.trim().equals("")) {
								TradeSummary trade = new TradeSummary(s1);
								trade.calculate();
								trade.setStockname(getStockName(trade
										.getStock()));
								if(trade!=null)
								{
//									vRow.addElement(TradeSummaryDef
//											.createTableRow(trade));
									TradeSummary memtrade = (TradeSummary) memmodel.getDataByKey(new String[] { trade.getStock(), trade.getInvestor()} );
									genTrade(trade,memtrade);
								}
								
							}
						}
						if (C_DATEFORMAT.format((Date) DateTo.getValue()).equalsIgnoreCase(C_DATEFORMAT.format(new Date())) ){
							for (int i = 0; i < array_stock.length; i++) {
								for (int j = 0; j < array_investor.length; j++) {
									int a =((IEQTradeApp)apps).getFeedEngine()
											.getStore(FeedStore.DATA_TRADESUMMARKETINV).getIndexByKey(new Object[]{array_stock[i],array_investor[j]}) ;
									if ( a > -1) {	
										TradeSummary newtrade = (TradeSummary)((IEQTradeApp)apps).getFeedEngine()
												.getStore(FeedStore.DATA_TRADESUMMARKETINV).getDataByKey(new Object[]{array_stock[i],array_investor[j]});
										int mem = memmodel.getIndexByKey(new String[] { newtrade.getStock(), newtrade.getInvestor()} );
										if (mem > -1) {

											TradeSummary memtrade = (TradeSummary) memmodel.getDataByKey(new String[] { newtrade.getStock(), newtrade.getInvestor()} );
											TradeSummary oldtrade = (TradeSummary) model.getDataByKey(new String[] { newtrade.getStock(), newtrade.getInvestor()});
											newtrade.setType("THSI");
											
											oldtrade.setBuyvol(new Double( memtrade.getBuyvol().doubleValue() +newtrade.getBuyvol().doubleValue()));
											oldtrade.setBuyval(new Double(  memtrade.getBuyval().doubleValue() + newtrade.getBuyval().doubleValue()));
											oldtrade.setBuyfreq(new Double( memtrade.getBuyfreq().doubleValue() + newtrade.getBuyfreq().doubleValue()));                
											oldtrade.setSellvol(new Double(  memtrade.getSellvol().doubleValue() + newtrade.getSellvol().doubleValue()));
											oldtrade.setSellval(new Double( memtrade.getSellval().doubleValue() + newtrade.getSellval().doubleValue()));
											oldtrade.setSellfreq(new Double( memtrade.getSellfreq().doubleValue() + newtrade.getSellfreq().doubleValue()));    
											
											
											oldtrade.calculate();
													
											try {
												 oldtrade.setBuyavg(new Double(oldtrade.getBuyval().doubleValue() / oldtrade.getBuyvol().doubleValue()));
									            } catch (Exception ex){
									            	oldtrade.setBuyavg(new Double(0));
									            }
								            try {
								            	oldtrade.setSellavg(new Double(oldtrade.getSellval().doubleValue() / oldtrade.getSellvol().doubleValue()));
								            } catch (Exception ex){
								            	oldtrade.setSellavg(new Double(0));
								            }
								            model.refresh();
										} 								
									}								
								}
							}
						}
						filterInv.fireFilterChanged();
						/*if(newStock!=""){//28-3-13
						((IEQTradeApp) apps).getFeedEngine().getStore(
								FeedStore.DATA_TRADESUMSTOCKINV).addRow(vRow, true, true);
						}
						((IEQTradeApp) apps).getFeedEngine().getStore(
								FeedStore.DATA_TRADESUMSTOCKINV).refresh();
						*/
						if(newStock!=""){//28-3-13
//							model.addRow(vRow, true, true);
//							memmodel.addRow(vRow, true, true);
							}
							model.refresh();
													
					} else {
						tableInv.getTable().clearSelection();
						Utils.showMessage("no data available", null);
					}
				
				} catch (Exception ex) {
					Utils.showMessage("failed, please try again "+ex.getMessage(), null);
					ex.printStackTrace();
				}
			}
		});
	}
	private void fieldStockActionInv() {//buat table atas
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					String newStock = fieldStock.getText().trim().toUpperCase();
					String from = C_DATEFORMAT.format((Date) DateFrom.getValue())
							.trim();
					String to = isHoliday();//C_DATEFORMAT.format((Date) DateTo.getValue()).trim();
					DateFormat forrmatter;
					Date date_from;
					Date date_to;
					forrmatter = new SimpleDateFormat("yyyy-mm-dd");
					date_from = (Date)forrmatter.parse(from);
					date_to = (Date)forrmatter.parse(to);
					
					
					
					if (((Date) DateTo.getValue()).getTime() < ((Date) DateFrom
							.getValue()).getTime() && date_from.compareTo(date_to)>0) {
						Utils.showMessage(
								"Aborted, please enter valid date parameter",
								null);
						return;
					} else if ((((Date) DateTo.getValue()).getTime() - ((Date) DateFrom
							.getValue()).getTime()) / 86400000L > 5) {
						Utils.showMessage(
								"Warning, range parameter more than 5days, it will take some time",
								null);
						return;
					}
					String result_stock=null;
					fieldStock.setText(newStock);
					oldStock = newStock;
					
					if(newStock!="")
					{
						result_stock = ((IEQTradeApp) apps)
						.getFeedEngine()
						.getEngine()
						.getConnection()
						.getHistory(
								"THSS" + "|" + from + "|"
										+ to);
						
					}
					
					filter.fireFilterChanged();
					//System.out.println("result stock "+result_stock);
					((IEQTradeApp)apps).getFeedEngine().getStore( 
							FeedStore.DATA_STOCKSUMMARY_HISTORY).getDataVector().clear();
					modelInv.getDataVector().clear();
					
					if (to.equalsIgnoreCase(C_DATEFORMAT.format(new Date())) && from.equalsIgnoreCase(C_DATEFORMAT.format(new Date()))){
						for (int i = 0; i < array_stock.length; i++) {
							int a =((IEQTradeApp)apps).getFeedEngine()
									.getStore(FeedStore.DATA_STOCKSUMMARY).getIndexByKey(new Object[]{array_stock[i],"RG"}) ;
							if ( a > -1) {	
								StockSummary stock = (StockSummary)((IEQTradeApp)apps).getFeedEngine()
										.getStore(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new Object[]{array_stock[i],"RG"});
								genStock(stock);								
							}							
						}
					} else if (!result_stock.equals("null")) {
						String as[] = result_stock.split("\n", -2);
						int aslength = as.length - 1;
					
						Vector vRow = new Vector(100, 5);
						for (int j = 0; j < aslength; j++) {
							String s1 = as[j];
							if (!s1.trim().equals("")) {//System.out.println(s1+" - *");
								StockSummary stock = new StockSummary(s1);
								Stock s = (Stock)((IEQTradeApp)apps).getFeedEngine().getStore(FeedStore.DATA_STOCK).getDataByKey(new String[]{stock.getCode()});
								if(s!=null)
								{
									stock.setName(s.getName());	
								}																			
//								vRow.addElement(StockSummaryDef
//										.createTableRow(trade));
								genStock(stock);
							}
						}

						if (C_DATEFORMAT.format((Date) DateTo.getValue()).equalsIgnoreCase(C_DATEFORMAT.format(new Date())) ){
							for (int i = 0; i < array_stock.length; i++) {
								int a =((IEQTradeApp)apps).getFeedEngine()
										.getStore(FeedStore.DATA_STOCKSUMMARY).getIndexByKey(new Object[]{array_stock[i],"RG"}) ;
								if ( a > -1) {	
									TradeSummary newtrade = (TradeSummary)((IEQTradeApp)apps).getFeedEngine()
											.getStore(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new Object[]{array_stock[i],"RG"});
									int mem = memmodel.getIndexByKey(new String[] { newtrade.getStock(), newtrade.getInvestor()} );
									if (mem > -1) {

										StockSummary oldtrade = (StockSummary) ((IEQTradeApp)apps).getFeedEngine().getStore( 
												FeedStore.DATA_STOCKSUMMARY_HISTORY).getDataByKey(new String[] { newtrade.getStock()} );
										StockSummary memtrade = (StockSummary) modelInv.getDataByKey(new String[] { newtrade.getStock()});
										//calc
										
							            ((IEQTradeApp)apps).getFeedEngine().getStore( 
												FeedStore.DATA_STOCKSUMMARY_HISTORY).refresh();
									} 								
								}								
							}
						}
						filterInv.fireFilterChanged();
						if(newStock!=""){ //28-3-13
//						
//						((IEQTradeApp)apps).getFeedEngine().getStore( 
//								FeedStore.DATA_STOCKSUMMARY_HISTORY).addRow(vRow, true, true);
						}
					} else {
						Utils.showMessage("no data available", null);
						tableInv.getTable().clearSelection();
					}
				} catch (Exception ex) {
					Utils.showMessage("failed, please try again", null);
					ex.printStackTrace();
					
				}
				
			}
		});
		
	}


	private String[] Stocks(String fieldStock){
		
		String[] names ;
			names = fieldStock.split(";");
		return names ;
	}
	
	private String geStockName(String code) {
		
		String name = "";
		int i = ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK)
				.getIndexByKey(new Object[] { code });
		//System.out.println("Nilai i = "+i);
		if (i > -1) {
			name = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_STOCK).getDataByIndex(i))
					.getName();
		}
		//System.out.println("name = "+name);
		return name;
	}
	protected String getStockName(String code) {
		Stock s = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK)
				.getDataByKey(new Object[] { code }));
		return (s != null) ? s.getName() : "";
	}
	@Override
	public void loadSetting() {
		//System.out.println("loadSetting");
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", StockSummaryDef.getTableDef());
			hSetting.put("tableinv", TradeSummaryDef.getTableInvDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 800, 350));
		//System.out.println("cobaa#############load");
		((Hashtable) hSetting.get("table")).put("header",StockSummaryDef.dataHeader.clone());
	}

	@Override
	public void saveSetting() {
		//System.out.println("saveSetting");
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		hSetting.put("tableinv", tableInv.getTableProperties());
		//hSetting.put("stock", oldStock.toUpperCase());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		//System.out.println("refresh");
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableInv.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		fieldName.setForeground(FeedSetting.getColor(FeedSetting.C_FOREGROUND));
		fieldInfo.setForeground(FeedSetting.getColor(FeedSetting.C_FOREGROUND));

		pnlInfo.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
		tableInv.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void show(Object param) {
		//System.out.println("show(Object param)");
		if (param != null) {
			HashMap par = (HashMap) param;
			fieldStock.setText(par.get("STOCK").toString());
			fieldStockAction(null);
			
		}
		show();
	}

	@Override
	public void show() {
		//System.out.println("show");
		if (!form.isVisible())
			{
			if (!((IEQTradeApp)apps).getUI().getForm(FeedUI.UI_INVMARKET).isShow()) {
				((IEQTradeApp) apps).getFeedEngine().subscribe(
						FeedParser.PARSER_TRADESUMMSTOCKINV,
						FeedParser.PARSER_TRADESUMMSTOCKINV);	
			}			((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
			.get(FeedParser.PARSER_TRADESUMMSTOCKINV).addListener(this);
			((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
			.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
			}
//		if(fieldStock.getText().length()>3)
//		{
//		fieldStockAction(null);
//    	fieldStockActionInv();
//    	fieldStockActionView();
//    	backTo(0);
//		}
		super.show();//System.out.println("cobaa#############show");
		 
	
	}

	@Override
	public void hide() {
		//System.out.println("hide");
		if (!((IEQTradeApp)apps).getUI().getForm(FeedUI.UI_INVMARKET).isShow()) {
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(
					FeedParser.PARSER_TRADESUMMSTOCKINV,
					FeedParser.PARSER_TRADESUMMSTOCKINV);	
		}
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
		.get(FeedParser.PARSER_TRADESUMMSTOCKINV).removeListener(this);
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
		.get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
		super.hide();
	}

	@Override
	public void close() {
		//System.out.println("close");
		if (!((IEQTradeApp)apps).getUI().getForm(FeedUI.UI_INVMARKET).isShow()) {
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(
					FeedParser.PARSER_TRADESUMMSTOCKINV,
					FeedParser.PARSER_TRADESUMMSTOCKINV);	
		}		
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
		.get(FeedParser.PARSER_TRADESUMMSTOCKINV).removeListener(this);
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
		.get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
		super.close();
	}

	@Override
	public void newMessage(Model args) {
		// TODO Auto-generated method stub
		if (args instanceof TradeSummary) {
			TradeSummary newtrade = (TradeSummary)args;
			if (newtrade.getBoard().equals("RG") && Arrays.asList(array_stock).contains(newtrade.getStock())
					&&  C_DATEFORMAT.format((Date) DateTo.getValue())
					.trim().equals(C_DATEFORMAT.format(new Date()))
					) {
				TradeSummary memtrade = (TradeSummary) memmodel.getDataByKey(new String[] { newtrade.getStock(), newtrade.getInvestor()} );
				genTrade(newtrade,memtrade);
			}
		} else if (args instanceof StockSummary){
			StockSummary newstock = (StockSummary) args;
			if (Arrays.asList(array_stock).contains(newstock.getCode()) &&  C_DATEFORMAT.format((Date) DateTo.getValue())
					.trim().equals(C_DATEFORMAT.format(new Date()))
					) {
				genStock(newstock);
			}
		}
		
	}
	
	void genTrade(TradeSummary newtrade, TradeSummary memtrade){
		int mem = model.getIndexByKey(new String[] { newtrade.getStock(), newtrade.getInvestor()} );
		if (mem > -1) {
			TradeSummary oldtrade = (TradeSummary) model.getDataByKey(new String[] { newtrade.getStock(), newtrade.getInvestor()});
			newtrade.setType("THSI");
			if ( C_DATEFORMAT.format((Date) DateTo.getValue()).trim().equals(C_DATEFORMAT.format(new Date())) && 
				C_DATEFORMAT.format((Date) DateFrom.getValue()).trim().equals(C_DATEFORMAT.format(new Date()))) {
				oldtrade.setBuyvol(new Double( newtrade.getBuyvol().doubleValue()));
				oldtrade.setBuyval(new Double( newtrade.getBuyval().doubleValue()));
				oldtrade.setBuyfreq(new Double( newtrade.getBuyfreq().doubleValue()));                
				oldtrade.setSellvol(new Double( newtrade.getSellvol().doubleValue()));
				oldtrade.setSellval(new Double(  newtrade.getSellval().doubleValue()));
				oldtrade.setSellfreq(new Double(  newtrade.getSellfreq().doubleValue()));    
				
			} else {
				oldtrade.setBuyvol(new Double(memtrade.getBuyvol().doubleValue() +newtrade.getBuyvol().doubleValue()));
				oldtrade.setBuyval(new Double(memtrade.getBuyval().doubleValue() + newtrade.getBuyval().doubleValue()));
				oldtrade.setBuyfreq(new Double(memtrade.getBuyfreq().doubleValue() + newtrade.getBuyfreq().doubleValue()));                
				oldtrade.setSellvol(new Double(memtrade.getSellvol().doubleValue() + newtrade.getSellvol().doubleValue()));
				oldtrade.setSellval(new Double(memtrade.getSellval().doubleValue() + newtrade.getSellval().doubleValue()));
				oldtrade.setSellfreq(new Double(memtrade.getSellfreq().doubleValue() + newtrade.getSellfreq().doubleValue()));    
			}
			oldtrade.calculate();
			try {
				 oldtrade.setBuyavg(new Double(oldtrade.getBuyval().doubleValue() / oldtrade.getBuyvol().doubleValue()));
	            } catch (Exception ex){
	            	oldtrade.setBuyavg(new Double(0));
	            }
            try {
            	oldtrade.setSellavg(new Double(oldtrade.getSellval().doubleValue() / oldtrade.getSellvol().doubleValue()));
            } catch (Exception ex){
            	oldtrade.setSellavg(new Double(0));
            }
            model.refresh();
			memmodel.refresh();
		} else {
			newtrade.setType("THSI");
			TradeSummary newtrade2 = new TradeSummary();
			newtrade2.fromVector(newtrade.getData());
			Vector vRow = new Vector(100, 5);
			vRow.addElement(TradeSummaryDef
					.createTableRow(newtrade));
			Vector vRow2 = new Vector(100, 5);
			vRow2.addElement(TradeSummaryDef
					.createTableRow(newtrade2));
			model.addRow(vRow, true, true);
			memmodel.addRow(vRow2, true, true);
			memmodel.refresh();
            model.refresh();
		}
	}

	void genStock(StockSummary newstock){
		int a =((IEQTradeApp)apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY).getIndexByKey(new Object[]{newstock.getCode(),"RG"}) ;
		if ( a > -1) {
			StockSummary stock = (StockSummary) ((IEQTradeApp)apps).getFeedEngine()
												.getStore(FeedStore.DATA_STOCKSUMMARY_HISTORY)
												.getDataByKey(new Object[]{newstock.getCode()}) ;

			StockSummary oldstock = (StockSummary) modelInv.getDataByKey(new Object[]{newstock.getCode()}) ;

			
			if ( C_DATEFORMAT.format((Date) DateTo.getValue()).trim().equals(C_DATEFORMAT.format(new Date())) && 
					C_DATEFORMAT.format((Date) DateFrom.getValue()).trim().equals(C_DATEFORMAT.format(new Date()))) {
				stock.fromVector(newstock.getData());
			} else {System.out.println(stock.getTradedFrequency().doubleValue() +" - "+ oldstock.getTradedFrequency().doubleValue() +" + "+ newstock.getTradedFrequency().doubleValue());
				stock.setTradedFrequency(oldstock.getTradedFrequency().doubleValue() + newstock.getTradedFrequency().doubleValue());
				stock.setTradedValue( oldstock.getTradedValue().doubleValue() + newstock.getTradedValue().doubleValue()) ;
				stock.setTradedLot(oldstock.getTradedLot().doubleValue() + newstock.getTradedLot().doubleValue());
//				oldstock.fromVector(newstock.getData());
			}
			((IEQTradeApp)apps).getFeedEngine().getStore( 
					FeedStore.DATA_STOCKSUMMARY_HISTORY).refresh();
//			modelInv.refresh();
			
		} else {
			StockSummary newtrade2 = new StockSummary();
			newtrade2.fromVector(newstock.getData());
			Vector vRow = new Vector(100, 5);
			vRow.addElement(StockSummaryDef
					.createTableRow(newstock));
			Vector vRow2 = new Vector(100, 5);
			vRow2.addElement(StockSummaryDef
					.createTableRow(newtrade2));
			((IEQTradeApp)apps).getFeedEngine().getStore( 
					FeedStore.DATA_STOCKSUMMARY_HISTORY).addRow(vRow, true, true);
			modelInv.addRow(vRow2, true, true);
			modelInv.refresh();
			((IEQTradeApp)apps).getFeedEngine().getStore( 
					FeedStore.DATA_STOCKSUMMARY_HISTORY).refresh();
		}
		
	}
	
	@Override
	public boolean isShow() {
		try {
		return (form.isVisible());	
		} catch (NullPointerException e) {
		// TODO: handle exception
			return false;
		}
	}
}
