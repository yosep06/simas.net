package eqtrade.feed.ui.reksadana;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.io.File;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserCommandEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserListener;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserNavigationEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserWindowOpeningEvent;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserWindowWillOpenEvent;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedApplication;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.TradeSummaryDef;
import eqtrade.trading.app.TradingUI;

public class UILoginSimasROL extends UI {

	private  JWebBrowser webBrowser;// = new JWebBrowser();
	private String locationUrl = "";
	
	public UILoginSimasROL( String app) {
		super("Login Simas ROL", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_LOGINSIMASROLL);
	}

	@Override
	protected void build() {
		JPanel pnlInfo = new JPanel();
	    FormLayout l = new FormLayout("100px,2dlu,100px, 2dlu, pref:grow, pref",
			"pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlInfo, BorderLayout.NORTH);
		jalan();		
	}

	private void jalan() {
		//	UIUtils.setPreferredLookAndFeel();
		    NativeInterface.open();
		    webBrowser = new JWebBrowser();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
				    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
				    //simas-rol.dev/login_check.asp?close=1 link close 
				    webBrowser.setBarsVisible(false);
				    webBrowser.setStatusBarVisible(false);
				    //locationUrl="simas-rol.dev/login_check.asp?useridv2=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    locationUrl="rol.sinarmas-am.co.id/login_check.asp?useridv2=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    webBrowser.navigate(locationUrl);
				    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
				    pnlContent.add(webBrowser, BorderLayout.CENTER);
					//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
				    
					webBrowser.addWebBrowserListener(new WebBrowserListener() {
						
						@Override
						public void windowWillOpen(WebBrowserWindowWillOpenEvent arg0) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void windowOpening(WebBrowserWindowOpeningEvent arg0) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void windowClosing(WebBrowserEvent arg0) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void titleChanged(WebBrowserEvent arg0) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void statusChanged(WebBrowserEvent arg0) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void locationChanging(WebBrowserNavigationEvent arg0) {
							// TODO Auto-generated method stub
							String loc = arg0.getNewResourceLocation();
							System.out.println(loc+" -- ");
							if (loc.equalsIgnoreCase("http://rol.sinarmas-am.co.id/login_check.asp?close=1")) {
								hide();
							}
						}
						
						@Override
						public void locationChanged(WebBrowserNavigationEvent arg0) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void locationChangeCanceled(WebBrowserNavigationEvent arg0) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void loadingProgressChanged(WebBrowserEvent arg0) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void commandReceived(WebBrowserCommandEvent arg0) {
							// TODO Auto-generated method stub
							
						}
					});
		      }
		    });
		}
	
	@Override
	public void focus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadSetting() {
		// TODO Auto-generated method stub
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 950, 750));
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);

	}
	
	public void show(){
		 startend("show");
//		//locationUrl="rol.sinarmas-am.co.id/login_check.asp?useridv2=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
//		 locationUrl="simas-rol.dev/login_check.asp?useridv2=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
//		    //locationUrl="http://trading.simasnet.com/rep-dev/nextg.portfolio.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
		    webBrowser.navigate(locationUrl);
		    super.show();
	}
	
	public void hide(){
		System.out.println("hide");
		 startend("hide");
		
		super.hide();
	}
	
	void startend(final String link){
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub

			    NativeInterface.open();
				if(link.equals("show")){
					locationUrl="rol.sinarmas-am.co.id/login_check.asp?useridv2=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				}else{
					locationUrl = "file:///" + new File("data").getAbsolutePath()
							+ File.separator + "loading" + ".htm";
				}				
				webBrowser.navigate(locationUrl);	
			}
		});

	}

}
