package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Rectangle;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import java.net.*;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.TradeSummaryDef;

public class UIPickOfTheDay extends UI{
	static Runnable pageloader;
	static Thread pageThread;
	private  JWebBrowser webBrowser;
	private String locationUrl = "";
	private String oldStock = "";
	protected static final String LS = System.getProperty("line.separator");
	
	public UIPickOfTheDay(String app) {
		super("Pick Of The Day", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_PICKOFTHEDAY);
		//super(new BorderLayout());
	    
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			//	fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
	}
	
	
	
	@Override
	protected void build() {
		
		JPanel pnlInfo = new JPanel();
	    FormLayout l = new FormLayout("50px,2dlu,50px, 2dlu, pref:grow, pref",
			"pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlInfo, BorderLayout.NORTH);
		jalan();
			}

	Thread threadLoad = null;
	
	private void jalan() {
		//UIUtils.setPreferredLookAndFeel();
	    NativeInterface.open();
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	    	  java.io.BufferedInputStream bis = null;
	    	
	    	  	JPanel webBrowserPanel = new JPanel(new BorderLayout());
			    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
			    webBrowser = new JWebBrowser();
			    webBrowser.setBarsVisible(false);
			    webBrowser.setStatusBarVisible(true);
			    //Date date = new Date();
			    Date date = new Date(((IEQTradeApp) apps).getFeedEngine()
						.getEngine().getConnection().getTime());
			    Calendar cal=Calendar.getInstance();
	            Calendar cal1=Calendar.getInstance();
	            String nama_url="http://www.sinarmassekuritas.co.id/info/research/";
			    String nama_tahun="Tahun%20" + new SimpleDateFormat("yyyy").format(date);
			    String nama_bulan=new SimpleDateFormat("MM").format(date);
	            Integer nm_bln = Integer.valueOf(nama_bulan);
	            switch (nm_bln) {
				case 1:
					nama_bulan="Jan";
					break;
				case 2:
					nama_bulan="Feb";
					break;
				case 3:
					nama_bulan="Mar";
					break;
				case 4:
					nama_bulan="Apr";
					break;
				case 5:
					nama_bulan="May";
					break;
				case 6:
					nama_bulan="Jun";
					break;
				case 7:
					nama_bulan="Jul";
					break;
				case 8:
					nama_bulan="Aug";
					break;
				case 9:
					nama_bulan="Sep";
					break;
				case 10:
					nama_bulan="Oct";
					break;
				case 11:
					nama_bulan="Nov";
					break;
				case 12:
					nama_bulan="Dec";
					break;
				default:
					break;
				}
	            String nama_folder=new SimpleDateFormat("yyyyMMdd").format(date);
	            String nama_file =new SimpleDateFormat("yyyyMMdd").format(date)+"%20picks%20of%20the%20day.pdf";
	            boolean isexist=exists(nama_url + nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file);
	            int i=-1;     
	            while(!isexist){
	            	//System.out.println("i "+i);
	            	cal1.add(Calendar.DATE, i);
	            	nama_tahun="Tahun%20" + new SimpleDateFormat("yyyy").format(cal1.getTime());
	            	 nama_bulan=new SimpleDateFormat("MM").format(date);
		             nm_bln = Integer.valueOf(nama_bulan);
		            switch (nm_bln) {
					case 1:
						nama_bulan="Jan";
						break;
					case 2:
						nama_bulan="Feb";
						break;
					case 3:
						nama_bulan="Mar";
						break;
					case 4:
						nama_bulan="Apr";
						break;
					case 5:
						nama_bulan="May";
						break;
					case 6:
						nama_bulan="Jun";
						break;
					case 7:
						nama_bulan="Jul";
						break;
					case 8:
						nama_bulan="Aug";
						break;
					case 9:
						nama_bulan="Sep";
						break;
					case 10:
						nama_bulan="Oct";
						break;
					case 11:
						nama_bulan="Nov";
						break;
					case 12:
						nama_bulan="Dec";
						break;
					default:
						break;
					}
	            	nama_folder=new SimpleDateFormat("yyyyMMdd").format(cal1.getTime());
	            	nama_file=new SimpleDateFormat("yyyyMMdd").format(cal1.getTime())+"%20picks%20of%20the%20day.pdf";
	            	isexist=exists(nama_url+ nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file);
	            	//System.out.println("isi url"+nama_url+ nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file);
	            	//System.out.println("isi flag "+isexist);
	            	
	            }
	            locationUrl="http://www.sinarmassekuritas.co.id/info/research/" + nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file;
			    webBrowser.navigate(locationUrl);
			    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
			
	    		pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
	
	  		
	      }  
	      
	    });
	 //   NativeInterface.runEventPump();
	}
	
	 public static boolean exists(String URLName){
		    try {
		      HttpURLConnection.setFollowRedirects(false);
		      // note : you may also need
		      //        HttpURLConnection.setInstanceFollowRedirects(false)
		      HttpURLConnection con =
		         (HttpURLConnection) new URL(URLName).openConnection();
		      con.setRequestMethod("HEAD");
		      return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		    }
		    catch (Exception e) {
		       e.printStackTrace();
		       return false;
		    }
		  }

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 750, 450));
	}

	@Override
	public void saveSetting() {
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
		//	fieldStock.setText(par.get("STOCK").toString());
		//	fieldStockAction();
		}
		show();
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void close() {
		super.close();
	}

}
