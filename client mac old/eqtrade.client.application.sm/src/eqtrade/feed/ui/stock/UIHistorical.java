package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Hashtable;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.TradeSummaryDef;

public class UIHistorical extends UI{
	static Runnable pageloader;
	static Thread pageThread;
	private JWebBrowser webBrowser;
	private JEditorPane contentsArea;
	private String locationUrl = "";
	private JPanel webBrowserPanel;
	private JPanel pnlInfo;
	private JTextField fieldStock;
	private JButton btnMenu;
	private JLabel fieldName;
	private String oldStock = "";
	protected static final String LS = System.getProperty("line.separator");
	
	public UIHistorical(String app) {
		super("Historical", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_HISTORICAL);
		//super(new BorderLayout());
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			//	fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
	}

	
	@Override
	protected void build() {
		btnMenu = new JButton("Back To Main");
			   
		JPanel pnlInfo = new JPanel();
	    FormLayout l = new FormLayout("5px, pref, 5px",
			"5px, pref, 5px");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		b.add(btnMenu, c.xy(2, 2));
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlInfo, BorderLayout.NORTH);
		jalan();
		
		btnMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				locationUrl="http://www.sinarmassekuritas.co.id/simas.net/research_v2.asp";
			    webBrowser.navigate(locationUrl);
			}
		});	
	}

	Thread threadLoad = null;
	
	private void jalan() {
		//UIUtils.setPreferredLookAndFeel();
	    NativeInterface.open();
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
			    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
			    webBrowser = new JWebBrowser();
			    webBrowser.setBarsVisible(false);
			    webBrowser.setStatusBarVisible(false);
			    locationUrl="http://www.sinarmassekuritas.co.id/simas.net/research_v2.asp";
			    webBrowser.navigate(locationUrl);
			    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
			
	    		pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
	
	  		
	    	  
	      }
	    });
	 //   NativeInterface.runEventPump();
	}
	
	

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 1000, 700));
	}

	@Override
	public void saveSetting() {
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
		//	fieldStock.setText(par.get("STOCK").toString());
		//	fieldStockAction();
		}
		show();
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void close() {
		super.close();
	}
}
