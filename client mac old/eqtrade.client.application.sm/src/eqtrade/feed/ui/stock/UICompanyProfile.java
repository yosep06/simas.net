package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Indices;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.TradeSummaryDef;

public class UICompanyProfile extends UI {
	static Runnable pageloader;
	static Thread pageThread;

	private static JEditorPane contentsArea;
	private static String locationUrl = "";

	private JTextField fieldStock;
	private JLabel fieldName;
	private String oldStock = "";

	public UICompanyProfile(String app) {
		super("Company Profile", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_COMPANYPROFILE);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
	}

	private void getThePage() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					//log.info("test1");
					//log.info("hasil location "+locationUrl);
					contentsArea.setPage(locationUrl);
				} catch (Exception e) {
					e.printStackTrace();
					fieldName.setText("failed, please try again");
					locationUrl = "file:///"
							+ new File("data").getAbsolutePath()
							+ File.separator + "blank" + ".htm";
					getThePage();
				}
			}
		});
	}

	@Override
	protected void build() {
		//log.info("test");
		createPopup();
		contentsArea = new JEditorPane();
		contentsArea.setEditable(false);
		contentsArea.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent event) {
				if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					locationUrl = event.getURL().toString();
					try {
						contentsArea.setPage(locationUrl);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		});

		JPanel pnlInfo = new JPanel();
		fieldStock = new JTextField("");
		fieldName = new JLabel("please type a stock code");

		FormLayout l = new FormLayout("75px,2dlu,50px, 2dlu, pref:grow, pref",
				"pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(fieldStock, c.xy(1, 1));
		b.add(fieldName, c.xyw(3, 1, 3));

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlInfo, BorderLayout.NORTH);
		pnlContent.add(new JScrollPane(contentsArea), BorderLayout.CENTER);
		fieldStock.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				fieldStock.selectAll();
			}
		});
		fieldStock.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fieldStockAction();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);

		refresh();

		/*if (hSetting.get("stock") != null) {
			fieldStock.setText((String) hSetting.get("stock"));
			fieldStockAction();
		}*/
	}

	Thread threadLoad = null;

	private void fieldStockAction() {
		if (threadLoad != null) {
			threadLoad.stop();
			threadLoad = null;
		}
		threadLoad = new Thread(new Runnable() {
			@Override
			public void run() {
				String newStock = fieldStock.getText().trim().toUpperCase();
                fieldStock.setText(newStock);
                fieldStock.selectAll();
                if(!oldStock.equals(newStock)){
				locationUrl = "file:///" + new File("data").getAbsolutePath()
						+ File.separator + "loading" + ".htm";
				getThePage();
				
				if ((newStock.length() > 0 /*&& !oldStock.equals(newStock)*/)) {
					fieldStock.setText(newStock);
					oldStock = newStock;
					fieldName.setText(geStockName(newStock));
					
					if (fieldName.getText().equals("")) {
						File ftest = new File("data/temp/" + oldStock + ".zip");
						if (ftest.exists()) {
							locationUrl = "file:///"
									+ new File("data/temp/"
											+ oldStock.toLowerCase() + "/")
											.getAbsolutePath() + File.separator
									+ oldStock.toLowerCase() + ".htm";
							getThePage();
						} else {
							try {
								byte[] result = ((IEQTradeApp) apps)
										.getFeedEngine().getEngine()
										.getConnection()
										.getFile("FILE" + "|" + oldStock);
								//FileOutputStream fos = new FileOutputStream(
								//		"data/temp/" + oldStock + ".zip");
								
								FileOutputStream fos = new FileOutputStream("data/temp/" + oldStock + ".htm");
								fos.write(result);
								fos.close();
								//unzip(new ZipFile("data/temp/" + oldStock
							//			+ ".zip"), new File("data/temp"));
								locationUrl = "file:///"
										+ new File("data/temp/"
												+ oldStock.toLowerCase() + ".htm")
												.getAbsolutePath();
										
								getThePage();
							} catch (Exception ex) {
								oldStock = "";
								fieldName.setText("failed, please try again");
								locationUrl = "file:///"
										+ new File("data").getAbsolutePath()
										+ File.separator + "blank" + ".htm";
								getThePage();
								// Utils.showMessage("failed, please try again",
								// UICompanyProfile.this.form);
								ex.printStackTrace();
							}
						}
					} else {
						locationUrl = "file:///"
								+ new File("data").getAbsolutePath()
								+ File.separator + "blank" + ".htm";
						getThePage();
					}
				}
				fieldStock.selectAll();
			}
			}
		});
		threadLoad.start();
	}

	private String geStockName(String code) {
		String name = "";
		//log.info("isi "+code);
		 int i = ((IEQTradeApp) apps).getTradingEngine()
		 .getStore(FeedStore.DATA_STOCK)
		 .getIndexByKey(new Object[] { code });
		 //log.info("i :"+i);
		//  Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine()
		// .getStore(FeedStore.DATA_STOCK).getDataByIndex(i);
		//  log.info("nama "+stock.getName());
		//Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine()
		//.getStore(FeedStore.DATA_STOCK)
		//.getDataByKey(new Object[] { code });
	//	log.info("stock"+stock.getName());
		//if (stock != null) {
		 //int i = ((IEQTradeApp) apps).getTradingEngine()
		//		.getStore(FeedStore.DATA_STOCK)
		//		.getIndexByKey(new Object[] { code });
		//log.info("i "+i);
		if (i > -1) {
			name = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_STOCK).getDataByIndex(i))
					.getName();
		}
		//	name=stock.getName();
		
		return name;
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 750, 450));
	}

	@Override
	public void saveSetting() {
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
			fieldStock.setText(par.get("STOCK").toString());
			fieldStockAction();
		}
		show();
	}

	@Override
	public void show() {
		if (hSetting.get("stock") != null && fieldStock.getText().equals("")) {
			fieldStock.setText((String) hSetting.get("stock"));
			fieldStockAction();
		}
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void close() {
		super.close();
	}

	public static void unzip(ZipFile zipFile, File jiniHomeParentDir) {
		Enumeration files = zipFile.entries();
		File f = null;
		FileOutputStream fos = null;

		while (files.hasMoreElements()) {
			try {
				ZipEntry entry = (ZipEntry) files.nextElement();
				InputStream eis = zipFile.getInputStream(entry);
				byte[] buffer = new byte[1024];
				int bytesRead = 0;

				f = new File(jiniHomeParentDir.getAbsolutePath()
						+ File.separator + entry.getName());

				if (entry.isDirectory()) {
					f.mkdirs();
					continue;
				} else {
					f.getParentFile().mkdirs();
					f.createNewFile();
				}

				fos = new FileOutputStream(f);

				while ((bytesRead = eis.read(buffer)) != -1) {
					fos.write(buffer, 0, bytesRead);
				}
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			} finally {
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException e) {
						// ignore
					}
				}
			}
		}
	}

}
