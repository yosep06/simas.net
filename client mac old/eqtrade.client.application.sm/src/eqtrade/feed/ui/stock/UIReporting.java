package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Hashtable;
import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserNavigationParameters;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.TradeSummaryDef;

public class UIReporting extends UI{
	static Runnable pageloader;
	static Thread pageThread;
	private  JWebBrowser webBrowser;
	private  JEditorPane contentsArea;
	private String locationUrl = "";
	private  JPanel webBrowserPanel;
	private JPanel pnlInfo;
	private JPanel pnlPortfolio;
	private JPanel pnlTC;
	private JPanel pnlTA;
	private JPanel pnlStatement;
	//#Valdhy20150507
	private JPanel pnlPNL;
	private JTextField fieldStock;
	private JLabel fieldName;
	private String oldStock = "";
	private String entryType = "DEPOSIT";
	private JTabbedPane tab;
	
	private JWebBrowser webz;
	private JButton btnPrin;
	
	protected static final String LS = System.getProperty("line.separator");
	
	public UIReporting(String app) {
		super("Reporting", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_REPORTING);
		//super(new BorderLayout());
	    
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			//	fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
	}
	
	
	
	@Override
	protected void build() {
		tab = new JTabbedPane();
		btnPrin = new JButton("PRINT");
		JPanel pnlInfo = new JPanel();
	    FormLayout l = new FormLayout("100px,2dlu,100px, 2dlu, pref:grow, pref",
			"pref");
		CellConstraints c = new CellConstraints();
		//PanelBuilder b = new PanelBuilder(l, pnlInfo);
		//pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		pnlPortfolio = new JPanel(new BorderLayout());
		pnlPortfolio.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlTC = new JPanel(new BorderLayout());
		pnlTC.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlTA = new JPanel(new BorderLayout());
		pnlTA.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlStatement = new JPanel(new BorderLayout());
		pnlStatement.setBorder(new EmptyBorder(5, 5, 5, 5));
		//#Valdhy20150507
		pnlPNL = new JPanel(new BorderLayout());
		pnlPNL.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		jalan();
		jalan1();
		jalan2();
		jalan3();
		//#Valdhy20150507
		jalan4();
		
		tab.addTab("Portfolio", pnlPortfolio);
		tab.addTab("Trade Confirmation", pnlTC);
		tab.addTab("Trade Activity", pnlTA);
		tab.addTab("Statement", pnlStatement);
		//#Valdhy20150507
		tab.addTab("Profit/Loss", pnlPNL);
		
		//log.info("entry type "+entryType);
		if(entryType.equals("DEPOSIT")){
			tab.setSelectedIndex(0);
		}else {
			tab.setSelectedIndex(1);
		}
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
//		pnlContent.add(btnPrin, BorderLayout.NORTH);
		pnlContent.add(tab, BorderLayout.CENTER);
		
		btnPrin.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				webz.print(true);
				
			}
		});
	}

	Thread threadLoad = null;
	
	private void jalan() {
	    NativeInterface.open();
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
			    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
			    webBrowser = new JWebBrowser();
			    webBrowser.setBarsVisible(false);
			    webBrowser.setStatusBarVisible(true);			    
			   webz = new JWebBrowser();
			    //locationUrl="http://trading.simasnet.com/report/nextg.portfolio.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();		
			    locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=1";	
			    webBrowser.navigate(locationUrl);		    
			    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
			    pnlPortfolio.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
	    		
			    
			    
	      }
	    });
	}
	private void jalan1() {
		//	UIUtils.setPreferredLookAndFeel();
		    NativeInterface.open();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
				    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
				    webBrowser = new JWebBrowser();
				    webBrowser.setBarsVisible(false);
				    webBrowser.setStatusBarVisible(true);
				    //locationUrl="http://trading.simasnet.com/report/nextg.tc.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    //locationUrl="http://trading.simasnet.com/rep-dev/nextg.tc.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=2";	
					webBrowser.navigate(locationUrl);
				    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
				    pnlTC.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		    		//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		
		  		
		    	  
		      }
		    });
		}

	private void jalan2() {
		//	UIUtils.setPreferredLookAndFeel();
		    NativeInterface.open();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
				    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
				    webBrowser = new JWebBrowser();
				    webBrowser.setBarsVisible(false);
				    webBrowser.setStatusBarVisible(true);
				    locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=3";	
				    //locationUrl="http://trading.simasnet.com/report/nextg.tradeactivity.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    //locationUrl="http://trading.simasnet.com/rep-dev/nextg.tradeactivity.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    webBrowser.navigate(locationUrl);
				    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
				    pnlTA.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		    		//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		
		  		
		    	  
		      }
		    });
		}

	private void jalan3() {
		//	UIUtils.setPreferredLookAndFeel();
		    NativeInterface.open();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
				    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
				    webBrowser = new JWebBrowser();
				    webBrowser.setBarsVisible(false);
				    webBrowser.setStatusBarVisible(true);
				    locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=4";	
				    //locationUrl="http://trading.simasnet.com/report/nextg.statement.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    //locationUrl="http://trading.simasnet.com/rep-dev/nextg.statement.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    webBrowser.navigate(locationUrl);
				    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
				    pnlStatement.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		    		//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		
		  		
		    	  
		      }
		    });
		}
	
	//#Valdhy20150507
	private void jalan4() {
	    NativeInterface.open();
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
			    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
			    webBrowser = new JWebBrowser();
			    webBrowser.setBarsVisible(false);
			    webBrowser.setStatusBarVisible(true);
			    locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=5";	
			    webBrowser.navigate(locationUrl);
			    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
			    pnlPNL.add(new JScrollPane(webBrowser), BorderLayout.CENTER);    	  
	      }
	    });
	}


	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 950, 650));
	}

	@Override
	public void saveSetting() {
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap p = (HashMap) param;
			entryType = (String) p.get("TYPE");
		//	fieldStock.setText(par.get("STOCK").toString());
		//	fieldStockAction();
		}
		show();
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void close() {
		super.close();
	}
}
