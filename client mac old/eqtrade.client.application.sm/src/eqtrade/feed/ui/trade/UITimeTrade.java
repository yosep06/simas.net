package eqtrade.feed.ui.trade;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.idata.Column;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.RunningTrade;
import eqtrade.feed.model.RunningTradeDef;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;
import eqtrade.feed.model.Subscriber;
import eqtrade.feed.model.TradePrice;
import eqtrade.feed.model.TradePriceDef;

public class UITimeTrade extends UI {
	private JGrid table;
	private JGrid tableTrade;
	private JTable tempTable;
	private JPanel pnlInfo;
	private JLabel fieldPrev;
	private JLabel fieldHigh;
	private JLabel fieldLow;
	private JLabel fieldChange;
	private JLabel fieldLast;

	private FilterTradePrice filter;
	private FilterTrade filterTrade;
	private JTextField fieldStock;
	private JLabel fieldName;
	private String oldStock = "";
	private JButton btnMore;
	private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	private static NumberFormat formatter2 = new DecimalFormat("#,##0.00");

	public UITimeTrade(String app) {
		super("Time & Trade Summary", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_TIMETRADE);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem mnBrokerSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORYM)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_BROKERSUMMARY, param);
				}
			}
		});
		mnBrokerSumm.setText("Buy/Sell Broker Summary");
		popupMenu.add(mnBrokerSumm);

		JMenuItem mnBrokerActivity = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORYM)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_BROKERACTIVITY, param);
				}
			}
		});
		mnBrokerActivity.setText("Broker Activity by Stock");
		popupMenu.add(mnBrokerActivity);

		JMenuItem mnInvStock = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORYM)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_INVSTOCK, param);
				}
			}
		});
		mnInvStock.setText("Foreign/Domestic by Stock");
		popupMenu.add(mnInvStock);

		JMenuItem mnCompany = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					int i = table.getMappedRow(table.getSelectedRow());
					if (i > -1) {
						RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_TRADEHISTORYM)
								.getDataByIndex(i);
						HashMap param = new HashMap();
						param.put("STOCK", order.getStock());
						apps.getUI().showUI(FeedUI.UI_COMPANYPROFILE, param);
					}
				} catch (Exception ex) {
					Utils.showMessage("cannot open Browser", null);
				}
			}
		});
		mnCompany.setText("Company Profile");
		popupMenu.add(mnCompany);
		JMenuItem mnIntraday = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORYM)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_INTRADAY, param);
				}
			}
		});
		mnIntraday.setText("Intraday Chart");
		popupMenu.add(mnIntraday);

		JMenuItem mnTechnical = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORYM)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					//param.put("STOCK", fieldStock.getText().trim().toUpperCase());
					//System.out.println("ORDER GET STOCK : "+order.getStock());
					apps.getUI().showUI(FeedUI.UI_CHART, param);
				}
			}
		});
		mnTechnical.setText("Technical Chart");
		popupMenu.add(mnTechnical);
		JMenuItem mnTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					RunningTrade order = (RunningTrade) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADEHISTORYM)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getStock());
					apps.getUI().showUI(FeedUI.UI_LIVETRADESTOCK, param);
				}
			}
		});
		mnTrade.setText("Live Trade By Stock");
		popupMenu.add(mnTrade);

		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
	}

	public static boolean[] columnsort = new boolean[] { false, false, false,false, false, false, false, false, false, false, false, false,false, false, false, false, false, false, false, false };

	AdjustmentListener adjustmentListener = new AdjustmentListener() {

		@Override
		public void adjustmentValueChanged(AdjustmentEvent e) {
			// TODO Auto-generated method stub
			int val = tableTrade.getVerticalScrollBar().getValue();
			if (e.getSource().equals(tableTrade.getVerticalScrollBar())){
				int extent = tableTrade.getVerticalScrollBar().getModel().getExtent();
				if (extent+val == tableTrade.getVerticalScrollBar().getMaximum()) {
					
					int a = tableTrade.getTable().getRowCount();
//					System.out.println("ORDER ID KE "+a);
					if (a >= 30) {
						String marketorderid = tableTrade.getTable().getModel().getValueAt(a-1,2).toString();
						System.out.println("ORDER ID KE :"+Integer.toString(a)+" = "+marketorderid);
						//get history 
						getHistory(marketorderid);
					}
					
				}
			}
		}
		
	};
	
	private void getHistory(final String seqno) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					String result_stock=null;
				
					result_stock = ((IEQTradeApp) apps)
						.getFeedEngine()
						.getEngine()
						.getConnection()
						.getHistory(
								"MTH" + "|" + oldStock + "|"
										+ seqno);
//					System.out.println("result: "+result_stock);
					if (!result_stock.equals("null")) {
						String as[] = result_stock.split("\n", -2);
						int aslength = as.length - 1;
						Vector<String> v = new Vector<String>(Arrays.asList(result_stock));
						Vector vRow = new Vector(100, 5);
						for (int j = 0; j < aslength; j++) {
							String s1 = as[j];
							
							if (!s1.trim().equals("")) {
								RunningTrade trade = new RunningTrade(s1);
								vRow.addElement(RunningTradeDef.createTableRow(trade));
							}
						}
//						if(newStock!=""){ //28-3-13
						
						((IEQTradeApp)apps).getFeedEngine().getStore( 
								FeedStore.DATA_TRADEHISTORYM).addRow(vRow, true, true);
//						}
						((IEQTradeApp) apps).getFeedEngine().getStore(
								FeedStore.DATA_TRADEHISTORYM).refresh();
					} else {
//						Utils.showMessage("no data available", null);
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		});
	}
	
	@Override
	protected void build() {
		oldStock = "";
		createPopup();
		
		filter = new FilterTradePrice(pnlContent);
		filterTrade = new FilterTrade(pnlContent);
		
		table = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_TRADEPRICE), filter,(Hashtable) hSetting.get("table"));
		// table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE, Model.CIDX_SEQNO });
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				JTable tblViewList = table.getTable();
				int nRow = tblViewList.rowAtPoint(e.getPoint());
				int nCol = tblViewList.columnAtPoint(e.getPoint());
				String name = table.getTable().getColumnModel().getColumn(nCol).getHeaderValue().toString();
				if (!e.isPopupTrigger() && nRow >= 0&& name.toUpperCase().equals("TRADE")&& e.getClickCount() == 1) {
					TradePrice data = (TradePrice) table.getDataByIndex(nRow);
					HashMap param = new HashMap(2);
					param.put("STOCK", oldStock);
					param.put("PRICE", data.getPrice());
					apps.getUI().showUI(FeedUI.UI_TIMETRADEPRICE, param);
				} else if (!e.isPopupTrigger()
						&& !name.toUpperCase().equals("TRADE")
						&& e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});

		tableTrade = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_TRADEHISTORYM), filterTrade,(Hashtable) hSetting.get("tabletrade"));
		// tableTrade.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		tableTrade.setColumnHide(new int[] { RunningTrade.CIDX_STOCK, Model.CIDX_HEADER, Model.CIDX_TYPE, Model.CIDX_SEQNO});
		//tableTrade.setColumnHide(new int[] {4, 0, 1, 2, 5});
		tableTrade.setColumnSortedProperties(columnsort);
		//tableTrade.setSortColumn(RunningTrade.CIDX_TXTIME);
		//tableTrade.setSortColumn(Model.CIDX_SEQNO);
		tableTrade.setSortColumn(RunningTrade.CIDX_TRADENO);
		tableTrade.setSortAscending(false);		
		tableTrade.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		//yosep set maximum
		tableTrade.getVerticalScrollBar().addAdjustmentListener(adjustmentListener);
		
		pnlInfo = new JPanel();
		fieldStock = new JTextField("");
		fieldName = new JLabel("please type a stock code");

		fieldPrev = new JLabel(" ", SwingConstants.RIGHT);
		fieldHigh = new JLabel(" ", SwingConstants.RIGHT);
		fieldLow = new JLabel(" ", SwingConstants.RIGHT);
		fieldChange = new JLabel(" ", SwingConstants.RIGHT);
		fieldLast = new JLabel(" ", SwingConstants.RIGHT);

		btnMore = new JButton("More");
		
		FormLayout l = new FormLayout("75px,2dlu,150px, pref","pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(fieldStock, c.xy(1, 1));
		b.add(fieldName, c.xyw(3, 1, 2));
		b.add(new JLabel("Last"), c.xy(1, 3));
		b.add(fieldLast, c.xy(3, 3));
		b.add(new JLabel("Hi"), c.xy(1, 5));
		b.add(fieldHigh, c.xy(3, 5));
		b.add(new JLabel("Lo"), c.xy(1, 7));
		b.add(fieldLow, c.xy(3, 7));
		b.add(new JLabel("Close"), c.xy(1, 9));
		b.add(fieldPrev, c.xy(3, 9));
		b.add(new JLabel("Chg"), c.xy(1, 11));
		b.add(fieldChange, c.xy(3, 11));

		b.add(btnMore, c.xy(3, 13));
		
		JSkinPnl header = new JSkinPnl();
		header.add(b.getPanel(), BorderLayout.WEST);
		header.add(table, BorderLayout.CENTER);
		table.setPreferredSize(new Dimension(100, 100));
		JPanel pnlInfo = new JPanel();
		FormLayout inf = new FormLayout("pref,2dlu,80px,2dlu,pref,2dlu,80px,pref,2dlu,80px,2dlu,pref,2dlu,80px");
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(header, BorderLayout.NORTH);
		pnlContent.add(tableTrade, BorderLayout.CENTER);
		table.getTable().getModel().addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
				changeRow();
			}
		});
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
				//System.out.println("MASUK TABLECHANGE BUILD");
				changeRow();
				
				}
		});

		fieldStock.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				fieldStockAction();
			}

			public void focusGained(FocusEvent e) {
				fieldStock.selectAll();
			}
		});
		fieldStock.registerKeyboardAction(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				fieldStockAction();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),JComponent.WHEN_FOCUSED);
		
		refresh();
		
		btnMore.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int a = tableTrade.getTable().getRowCount();
					String marketorderid = tableTrade.getTable().getModel().getValueAt(a-1,2).toString();
					System.out.println("ORDER ID KE :"+Integer.toString(a)+" = "+marketorderid);
					//get history 
					getHistory(marketorderid);
			}
		});
		
		if (hSetting.get("stock") != null) {
			fieldStock.setText((String) hSetting.get("stock"));
			fieldStockAction();
		}
	}

	private void changeRow() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					StockSummary summ = (StockSummary) ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new Object[] { oldStock, "RG" });
					if (summ != null) {
						fieldChange.setText(getChange(summ.getClosing().doubleValue(), summ.getPrevious().doubleValue()));
						fieldPrev.setText(formatter.format(summ.getPrevious().doubleValue()));
						fieldChange.setForeground(summ.getClosing()
								.doubleValue() > summ.getPrevious()
								.doubleValue() ? FeedSetting
								.getColor(FeedSetting.C_PLUS) : (summ
								.getClosing().doubleValue() < summ
								.getPrevious().doubleValue()) ? FeedSetting
								.getColor(FeedSetting.C_MINUS) : FeedSetting
								.getColor(FeedSetting.C_ZERO));
						fieldPrev.setForeground(FeedSetting
								.getColor(FeedSetting.C_FOREGROUND));

						fieldLast.setText(formatter.format(summ.getClosing()
								.doubleValue()));
						fieldHigh.setText(formatter.format(summ.getHighest()
								.doubleValue()));
						fieldLow.setText(formatter.format(summ.getLowest()
								.doubleValue()));
						fieldLast
								.setForeground(summ.getClosing().doubleValue() > summ
										.getPrevious().doubleValue() ? FeedSetting
										.getColor(FeedSetting.C_PLUS)
										: (summ.getClosing().doubleValue() < summ
												.getPrevious().doubleValue()) ? FeedSetting
												.getColor(FeedSetting.C_MINUS)
												: FeedSetting
														.getColor(FeedSetting.C_ZERO));
						fieldHigh
								.setForeground(summ.getHighest().doubleValue() > summ
										.getClosing().doubleValue() ? FeedSetting
										.getColor(FeedSetting.C_PLUS)
										: (summ.getHighest().doubleValue() < summ
												.getClosing().doubleValue()) ? FeedSetting
												.getColor(FeedSetting.C_MINUS)
												: FeedSetting
														.getColor(FeedSetting.C_ZERO));
						fieldLow.setForeground(summ.getLowest().doubleValue() > summ
								.getPrevious().doubleValue() ? FeedSetting
								.getColor(FeedSetting.C_PLUS) : (summ
								.getLowest().doubleValue() < summ.getPrevious()
								.doubleValue()) ? FeedSetting
								.getColor(FeedSetting.C_MINUS) : FeedSetting
								.getColor(FeedSetting.C_ZERO));
					} else {
						fieldChange.setText("- ");
						fieldPrev.setText("- ");
						fieldLast.setText("- ");
						fieldHigh.setText("- ");
						fieldLow.setText("- ");
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});

	}

	private String getChange(double slastprice, double nprev) {
		String schange = " ";

		if (slastprice > 0) {
			double ntemp = slastprice - nprev;
			double ndbl = ((double) (ntemp * 100) / nprev);
			int ntmpdbl = (int) (ndbl * 100);
			double npersen = (double) ntmpdbl / 100;
			
			if (ntemp > 0) {
				schange = "+" + formatter.format(ntemp) + "(" + formatter2.format(npersen) + "%) ";
			} else if (ntemp == 0) {
				schange = "" + formatter.format(ntemp) + "(0.00%) ";
			} else {
				schange = "" + formatter.format(ntemp) + "(" + formatter2.format(npersen) + "%) ";
			}
		}
		return schange;
	}

	private void fieldStockAction() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				String newStock = fieldStock.getText().trim().toUpperCase();
				if (newStock.length() > 0 && !oldStock.equals(newStock)) {
					//System.out.println("NEW STOCK : "+newStock);
					//System.out.println("OLD STOCK : "+oldStock);

					setFieldStock(false);//yosep fixing timetrade
					if (oldStock.length() > 0) {
						((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADEPRICE,FeedParser.PARSER_TRADEPRICE + "|" + oldStock+ "#RG");
						((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADEHISTORYM,FeedParser.PARSER_TRADEHISTORYM + "|"+ oldStock);
					}
					fieldStock.setText(newStock);
					oldStock = newStock;
					
					((FilterColumn) filter.getFilteredData("stock")).setField(newStock);
					filter.fireFilterChanged();
					
					((FilterColumn) filterTrade.getFilteredData("stock")).setField(newStock);
					filterTrade.fireFilterChanged();
					
					fieldName.setText(geStockName(newStock));
					form.setTitle(oldStock + " Time & Trade Summary");
					((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_TRADEPRICE,FeedParser.PARSER_TRADEPRICE + "|" + oldStock+ "#RG");
					
					((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_TRADEHISTORYM, FeedParser.PARSER_TRADEHISTORYM + "|" + oldStock);
					
					
					changeRow();
					
					//tempTable = tableTrade.getTable();
					int a = tableTrade.getTable().getRowCount();
					//System.out.println("ROW COUNT"+Integer.toString(a));
					
					for(int i = 0; i<a; i++) {
						String marketorderid = tableTrade.getTable().getModel().getValueAt(i,8).toString();
						//System.out.println("ORDER ID KE :"+Integer.toString(i)+" = "+marketorderid);
						
					}
					
					setFieldStock(true);
					
					//System.out.println("STRING ASD : "+asd);
				}
				fieldStock.selectAll();
			}
		});
	}

	private String geStockName(String code) {
		String name = "";
		int i = ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK)
				.getIndexByKey(new Object[] { code });
		if (i > -1) {
			name = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_STOCK).getDataByIndex(i))
					.getName();
		}
		return name;
	}

	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradePriceDef.getTableDef());
			hSetting.put("tabletrade", RunningTradeDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 450, 350));
	}

	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		hSetting.put("tabletrade", tableTrade.getTableProperties());
		hSetting.put("stock", oldStock.toUpperCase());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	public void refresh() {
		table.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		fieldName.setForeground(FeedSetting.getColor(FeedSetting.C_FOREGROUND));
		pnlInfo.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
		tableTrade.setNewFont(FeedSetting.getFont());
		changeRow();
	}

	public void show(Object param) {
//		if (canClose) {
			if (param != null) {
				HashMap par = (HashMap) param;
				fieldStock.setText(par.get("STOCK").toString());
				fieldStockAction();
				}
//		}else{
//			Utils.showMessage("Please Wait", null);

//		}
		show();
	}

	public void hide() {
		// if (!oldStock.equals("")){
		// ((IEQTradeApp)apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADEPRICE,
		// FeedParser.PARSER_TRADEPRICE+"|"+oldStock+"#RG");
		// ((IEQTradeApp)apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADEHISTORYM,
		// FeedParser.PARSER_TRADEHISTORYM+"|"+oldStock);
		// }
		// super.hide();
		close();
	}

	public void close() {
//		if (canClose) {// yosep fixingTimetrade
			if (!oldStock.equals("")) {
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADEPRICE,FeedParser.PARSER_TRADEPRICE + "|" + oldStock + "#RG");
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADEHISTORYM,FeedParser.PARSER_TRADEHISTORYM + "|" + oldStock);
			}
			super.close();
//		}
	}
	
	protected class RowChange implements TableModelListener {
		@Override
		public void tableChanged(TableModelEvent e) {
			// TODO Auto-generated method stub
			double count = 0;
			int size = tableTrade.getTable().getRowCount();
			//System.out.println("88 "+size);			
		}
		
	}
	// yosep fixingTimetrade =====
	boolean canClose=true;
	public void setFieldStock(boolean hist){
		try {
			fieldStock.setEnabled(hist);
			canClose = hist;
		} catch (Exception e) {
		}
	}
	
	public String getOldStock(){
		return oldStock;
	}
}
