package eqtrade.feed.ui.trade;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IMessage;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.RunningTrade;
import eqtrade.feed.model.RunningTradeDef;
import eqtrade.feed.ui.core.UIDlgBroker;

public class UILiveTradeBroker extends UI implements IMessage {
	private JGrid table;
	private GridModel model;
	private int MAXROW = 50;
	private boolean scroll = true;
	private String oldBroker = "";
	private JMenuItem editMenu;
	private JCheckBoxMenuItem scrollMenu;
	private HashMap broker_list = new HashMap(); // Valdhy20140820

	public UILiveTradeBroker(String app) {
		super("Live Trade By Broker", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_LIVETRADEBROKER);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		
		editMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				
				if (i > -1) {
					UI dlg = apps.getUI().getForm(FeedUI.UI_DLGBROKER);

					HashMap map = new HashMap();
					map.put("type", UIDlgBroker.C_EDIT);
					map.put("broker", oldBroker);
					apps.getUI().showUI(FeedUI.UI_DLGBROKER, map);
					
					// Valdhy20140820 -- Split String and put to array
					String str = dlg.getProperty("broker").toString();
					String delimiter = ";";
					String[] arr_str;
					arr_str = str.split(delimiter);
					
					// Valdhy20140820 -- Set array to HashMap
					broker_list.clear();
					
					for(int j =0; j < arr_str.length ; j++){
						broker_list.put(arr_str[j], arr_str[j]);
					}
					
					if (UIDlgBroker.C_RESULT == UIDlgBroker.C_OK) {
						if (!oldBroker.equals(dlg.getProperty("broker"))) {
							clearTable();
							oldBroker = (String) (dlg.getProperty("broker"));
							form.setTitle("Live trade By " + oldBroker);
						}
					}
				}
			}
		});
		editMenu.setText("Edit Broker");
		editMenu.setActionCommand("Edit");
		popupMenu.add(editMenu);

		scrollMenu = new JCheckBoxMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				scroll = scrollMenu.isSelected();
				currentrow = 0;
				table.getTable().setRowSelectionAllowed(!scroll);
			}
		});
		scrollMenu.setText("Scrolling");
		scrollMenu.setActionCommand("Scrolling");
		popupMenu.add(scrollMenu);
		scrollMenu.setSelected(scroll);
		propertiesMenu.setText("Properties");
		// popupMenu.addSeparator();
		// popupMenu.add(propertiesMenu);
		
	}

	private void clearTable() {
		synchronized (model) {
			RunningTrade r = new RunningTrade();
			for (int i = 0; i < MAXROW; i++) {
				RunningTrade rt = (RunningTrade) model.getDataByIndex(i);
				rt.fromVector(r.getData());
			}
		}
		model.refresh();
	}

	@Override
	protected void build() {
		createPopup();
		model = new GridModel(RunningTradeDef.getHeader(), false);
		Vector vrow = new Vector(MAXROW);
		for (int i = 0; i < MAXROW; i++) {
			vrow.addElement(RunningTradeDef.createTableRow(new RunningTrade()));
		}
		model.addRow(vrow, false, false);
		table = createTable(model, null, (Hashtable) hSetting.get("table"));
		table.setColumnHide(new int[] { RunningTrade.CIDX_TRADENO,
				RunningTrade.CIDX_BESTBID, RunningTrade.CIDX_BESTBIDLOT,
				RunningTrade.CIDX_BESTOFFER, RunningTrade.CIDX_BESTOFFERLOT,
				RunningTrade.CIDX_PREVIOUS });
		// table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		// table.getTable().getTableHeader().setReorderingAllowed(false);

		table.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO });

		table.getTable().add(popupMenu);
		// table.getTable().addMouseListener(new MyMouseAdapter());
		// table.addMouseListener(new MyCustomMouseAdapter());
		table.getTable().setEnabled(false);
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		table.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				editMenu.doClick();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, 0, true),
				JComponent.WHEN_FOCUSED);
		table.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				scrollMenu.doClick();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0, true),
				JComponent.WHEN_FOCUSED);

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.add(table, BorderLayout.CENTER);
		refresh();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().setRowSelectionAllowed(!scroll);
				form.setTitle("Live trade By "
						+ ((oldBroker == "") ? "Broker" : oldBroker));
			}
		});
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", RunningTradeDef.getTableDef());
		}
		oldBroker = (hSetting.get("broker") == null) ? "" : (String) hSetting
				.get("broker");
		scroll = (hSetting.get("scroll") == null) ? true : ((Boolean) hSetting
				.get("scroll")).booleanValue();
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 500, 450));
		
		// Valdhy20140821 -- Split String and put to array
		String str = oldBroker;
		String delimiter = ";";
		String[] arr_str;
		arr_str = str.split(delimiter);
		
		// Valdhy20140821 -- Set array to HashMap
		for(int j =0; j < arr_str.length ; j++){
			broker_list.put(arr_str[j], arr_str[j]);
		}
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		hSetting.put("scroll", new Boolean(scroll));
		hSetting.put("broker", oldBroker);
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
			clearTable();
			oldBroker = (par.get("BROKER").toString());
			form.setTitle("Live trade By " + oldBroker);
		}
		show();
	}

	@Override
	public void show() {
		if (!form.isVisible()) {
			((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
					.get(FeedParser.PARSER_RUNNINGTRADE).addListener(this);
			// loadSetting();
			// form.setLocation(((Rectangle)hSetting.get("size")).x,
			// ((Rectangle)hSetting.get("size")).y);
			// form.setSize(new
			// Dimension(((Rectangle)hSetting.get("size")).width,
			// ((Rectangle)hSetting.get("size")).height));
			// refresh();
		}
		super.show();
	}

	@Override
	public void hide() {
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_RUNNINGTRADE).removeListener(this);
		super.hide();
	}

	@Override
	public void close() {
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_RUNNINGTRADE).removeListener(this);
		super.close();
	}

	int currentrow = 0;

	@Override
	public void newMessage(Model arg0) {
		//System.err.println("TES");
		
		// Valdhy20140820 -- Filter Data dengan HashMap
		//if (((RunningTrade) arg0).getSeller().equals(oldBroker) || ((RunningTrade) arg0).getBuyer().equals(oldBroker)){
		if (( broker_list.get(((RunningTrade) arg0).getSeller()) != null)
				|| ( broker_list.get(((RunningTrade) arg0).getBuyer()) != null)){
			if (scroll) {
				int to = MAXROW - 1;
				int from = MAXROW - 2;
				for (int j = 0; j < MAXROW - 1; j++) {
					RunningTrade rtfrom = (RunningTrade) model
							.getDataByIndex(from);
					RunningTrade rtto = (RunningTrade) model.getDataByIndex(to);
					rtto.fromVector(rtfrom.getData());
					from--;
					to--;
				}
				RunningTrade rt = (RunningTrade) model.getDataByIndex(0);
				rt.fromVector(arg0.getData());
				model.refresh();
			} else {
				// int rowcount =
				// (int)table.getViewport().getExtentSize().getHeight() /
				// table.getTable().getRowHeight();
				int rowcount = (int) table.getViewport().getVisibleRect()
						.getHeight()
						/ table.getTable().getRowHeight();
				RunningTrade rt = (RunningTrade) model
						.getDataByIndex(currentrow);
				rt.fromVector(arg0.getData());
				model.refresh();
				table.getTable()
						.setRowSelectionInterval(currentrow, currentrow);
				currentrow = (currentrow + 1 >= rowcount || currentrow + 1 >= MAXROW) ? 0
						: currentrow + 1;
			}
		}
	}
}
