package eqtrade.trading.market.chart.source;

import java.text.SimpleDateFormat;
import java.util.Vector;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

import eqtrade.trading.market.chart.core.QueryGetThread;
import eqtrade.trading.market.chart.ext.ICallback;
import eqtrade.trading.market.chart.ext.IChartDatasource;
import eqtrade.trading.market.chart.ext.IResult;
import eqtrade.trading.market.chart.util.ChartUtil;


public class YahooSource implements IChartDatasource{
	private static final SimpleDateFormat date = new SimpleDateFormat("dd");
	  private static final SimpleDateFormat month = new SimpleDateFormat("M");
	  private static final SimpleDateFormat year = new SimpleDateFormat("yyyy");

	  private static String url = "http://ichart.finance.yahoo.com/table.csv?ignore=.csv";
	  private HttpClient client;

	  public YahooSource()
	  {
	    client = new HttpClient();
	  }

	  public void getData(String type, String stock, String from, String to, boolean adjusted, IResult result, ICallback callback) {
	    StringBuffer urlbuff = new StringBuffer(url);
	    try {
	      urlbuff.append("&s=").append(stock.toUpperCase()).append(!stock.startsWith("^") ? ".JK" : "");
	      urlbuff.append("&d=").append(Integer.parseInt(month.format(ChartUtil.C_FORMAT2.parse(to))) - 1);
	      urlbuff.append("&e=").append(date.format(ChartUtil.C_FORMAT2.parse(to)));
	      urlbuff.append("&f=").append(year.format(ChartUtil.C_FORMAT2.parse(to)));
	      urlbuff.append("&g=d");
	      urlbuff.append("&a=").append(Integer.parseInt(month.format(ChartUtil.C_FORMAT2.parse(from))) - 1);
	      urlbuff.append("&b=").append(date.format(ChartUtil.C_FORMAT2.parse(from)));
	      urlbuff.append("&c=").append(year.format(ChartUtil.C_FORMAT2.parse(from))); } catch (Exception localException) {
	    }
	    Vector p = new Vector(1);
	    GetMethod get = new GetMethod(urlbuff.toString());
	    p.add(get);
	    Vector info = new Vector(1);
	    info.add("data");
	    QueryGetThread th = new QueryGetThread(stock, this.client, info, p, callback, result);
	    th.start();
	  }
}
