package eqtrade.trading.market.chart.ext;

import java.util.Date;

import org.jfree.data.xy.XYDataset;

public abstract interface IChartDataset {
	public abstract XYDataset getOHLCDataset();
	  public abstract XYDataset getVolDataset();
	  public abstract XYDataset getOpenDataset();
	  public abstract XYDataset getHighDataset();
	  public abstract XYDataset getLowDataset();
	  public abstract XYDataset getCloseDataset();
	  public abstract Date[] getDateArray();
	  public abstract Object[] getOHLCArray();
	  public abstract double[] getVolArray();
	  public abstract double[] getOpenArray();
	  public abstract double[] getHighArray();
	  public abstract double[] getLowArray();
	  public abstract double[] getCloseArray();
	  public abstract void loadData(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, ICallback paramICallback);
	  public abstract void setDatasource(IChartDatasource paramIChartDatasource);
	  public abstract void addListener(IChartListener paramIChartListener);
	  public abstract void removeListener(IChartListener paramIChartListener);
}
