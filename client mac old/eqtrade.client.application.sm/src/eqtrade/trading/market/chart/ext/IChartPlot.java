package eqtrade.trading.market.chart.ext;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;

public abstract interface IChartPlot {
	 public abstract void addPlot(String paramString1, String paramString2, XYDataset paramXYDataset, ValueAxis paramValueAxis, XYItemRenderer paramXYItemRenderer);
	  public abstract void removePlot(String paramString1, String paramString2);
	  public abstract void updateDataset(String paramString1, String paramString2, XYDataset paramXYDataset);
	  public abstract boolean isExistsPlot(String paramString);
}
