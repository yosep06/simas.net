package eqtrade.trading.market.chart.ext;

import java.util.Vector;

public abstract interface IResult {
	  public abstract void receiveData(Vector paramVector);

}
