package eqtrade.trading.market.chart.ext;

public abstract interface ICallback {
	public abstract void failed(String paramString);
	  public abstract void success();
	  public abstract void setStatus(String paramString);
	  public abstract void setWait(boolean paramBoolean);
}
