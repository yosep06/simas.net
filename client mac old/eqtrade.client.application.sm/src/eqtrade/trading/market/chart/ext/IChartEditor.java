package eqtrade.trading.market.chart.ext;

import com.vollux.ui.JSkinPnl;

public abstract interface IChartEditor {
	  public abstract JSkinPnl getPanel();
	  public abstract boolean isValidEntry();
	  public abstract void init();
	  public abstract void update();
}
