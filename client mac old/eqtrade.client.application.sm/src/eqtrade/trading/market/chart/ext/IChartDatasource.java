package eqtrade.trading.market.chart.ext;

public abstract interface IChartDatasource {
	  public abstract void getData(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, IResult paramIResult, ICallback paramICallback);
}
