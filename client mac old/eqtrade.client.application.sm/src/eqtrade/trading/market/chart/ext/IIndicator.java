package eqtrade.trading.market.chart.ext;

import javax.swing.JMenuItem;

public abstract interface IIndicator {
	  public abstract JMenuItem getMenu();
}
