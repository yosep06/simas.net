package eqtrade.trading.market.chart.core;

import java.util.HashMap;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;

public class MultiplePlot extends XYPlot{
	  protected static final long serialVersionUID = 5663730542999342829L;
	  protected HashMap child = new HashMap(10);
	  protected int count = 1;
	  protected String name;

	  public MultiplePlot(String name)
	  {
	    this.name = name;
	    setDomainGridlinesVisible(true);
	    setRangeGridlinesVisible(true);
	  }

	  public MultiplePlot(String name, XYDataset dataset, ValueAxis axis, ValueAxis range, XYItemRenderer render) {
	    super(dataset, axis, range, render);
	    this.name = name;
	    setDomainGridlinesVisible(true);
	    setRangeGridlinesVisible(true);
	  }

	  public String getName() {
	    return this.name;
	  }

	  public void addPlot(String id, XYDataset dataset, ValueAxis axis, XYItemRenderer render) {
	    setDataset(this.count, dataset);
	    if (axis != null) {
	      setRangeAxis(this.count, axis);
	      mapDatasetToRangeAxis(this.count, this.count);
	    }
	    setRenderer(this.count, render);
	    this.child.put(id, this.count);
	    this.count += 1;
	  }

	  public void removePlot(String id) {
	    Object o = this.child.get(id);
	    if (o != null) {
	      int pos = Integer.parseInt((String)o);
	      setDataset(pos, null);
	      setRenderer(pos, null);
	      this.child.remove(id);
	    }
	  }

	  public void updateDataset(String id, XYDataset dataset) {
	    Object o = this.child.get(id);
	    if (o != null) {
	      int pos = (Integer)o;
	      setDataset(pos, dataset);
	    }
	  }

	  public boolean isExistsPlot(String id) {
	    return this.child.get(id) != null;
	  }
}
