package eqtrade.trading.market.chart.core;

import org.jfree.chart.axis.DateAxis;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTick;
import org.jfree.chart.axis.SegmentedTimeline;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.TextAnchor;
public class CustomDateAxis extends DateAxis{
	 private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	  private static final long serialVersionUID = 4239630992518135526L;
	  private Date from;
	  private Date to;

	  public CustomDateAxis(String s, Date from, Date to, SegmentedTimeline timeline)
	  {
	    super(s);
	    this.from = from;
	    this.to = to;
	    setTimeline(timeline);
	    setAutoRange(true);
	  }

	  public void setRangeDate(Date from, Date to) {
	    this.from = from;
	    this.to = to;
	    setRange(from, to);
	  }

	  public List refreshTicks(Graphics2D graphics2d, AxisState axisstate, Rectangle2D rectangle2d, RectangleEdge rectangleedge) {
	    ArrayList arraylist = new ArrayList();
	    try {
	      arraylist.add(new DateTick(this.from, dateFormat.format(this.from), TextAnchor.TOP_CENTER, TextAnchor.CENTER_LEFT, 0.0D));
	      long selisih = this.to.getTime() - this.from.getTime();
	      long four = selisih / 4L;
	      arraylist.add(new DateTick(new Date(this.from.getTime() + four), dateFormat.format(new Date(this.from.getTime() + four)), TextAnchor.TOP_CENTER, TextAnchor.CENTER, 0.0D));
	      arraylist.add(new DateTick(new Date(this.from.getTime() + four * 2L), dateFormat.format(new Date(this.from.getTime() + four * 2L)), TextAnchor.TOP_CENTER, TextAnchor.CENTER, 0.0D));
	      arraylist.add(new DateTick(new Date(this.from.getTime() + four * 3L), dateFormat.format(new Date(this.from.getTime() + four * 3L)), TextAnchor.TOP_CENTER, TextAnchor.CENTER, 0.0D));
	      if (this.from.getTime() + four * 4L <= this.to.getTime())
	        arraylist.add(new DateTick(new Date(this.from.getTime() + four * 4L), dateFormat.format(new Date(this.from.getTime() + four * 4L)), TextAnchor.TOP_CENTER, TextAnchor.CENTER_RIGHT, 0.0D));
	      else
	        arraylist.add(new DateTick(this.to, dateFormat.format(this.to), TextAnchor.TOP_CENTER, TextAnchor.CENTER_RIGHT, 0.0D));
	    } catch (Exception localException) {
	    }
	    return arraylist;
	  }
}
