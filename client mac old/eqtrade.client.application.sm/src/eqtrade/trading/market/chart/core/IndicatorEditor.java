package eqtrade.trading.market.chart.core;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;
import com.vollux.util.Util;

import eqtrade.trading.market.chart.ext.IChartEditor;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public class IndicatorEditor {
	 protected JSkinDlg dlg;
	  protected String name;
	  protected JButton btnOK;
	  protected JButton btnCancel;
	  protected JButton btnRemove;
	  protected JSkinPnl panel;
	  protected IChartEditor editor;
	  public static final int C_OK = 0;
	  public static final int C_CANCEL = 1;
	  public static final int C_REMOVE = 2;
	  protected int result = 1;

	  public IndicatorEditor(String name, IChartEditor editor) {
	    this.name = name;
	    this.editor = editor;
	    build();
	  }

	  protected void build() {
	    this.dlg = new JSkinDlg(this.name);
	    this.dlg.setModal(true);
	    this.dlg.setResizable(false);
	    this.btnOK = new JButton("OK");
	    this.btnOK.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent arg0) {
	        IndicatorEditor.this.result = 0;
	        if (IndicatorEditor.this.editor.isValidEntry()) {
	          IndicatorEditor.this.editor.update();
	          IndicatorEditor.this.dlg.setVisible(false);
	        }
	      }
	    });
	    this.btnCancel = new JButton("Cancel");
	    this.btnCancel.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent arg0) {
	        IndicatorEditor.this.result = 1;
	        IndicatorEditor.this.dlg.setVisible(false);
	      }
	    });
	    this.btnRemove = new JButton("Remove");
	    this.btnRemove.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent arg0) {
	        IndicatorEditor.this.result = 2;
	        IndicatorEditor.this.dlg.setVisible(false);
	      }
	    });
	    this.panel = new JSkinPnl(new BorderLayout());
	    JSkinPnl pnlBtn = new JSkinPnl(new FlowLayout());
	    pnlBtn.add(this.btnOK);
	    pnlBtn.add(this.btnCancel);
	    pnlBtn.add(this.btnRemove);
	    this.panel.add(this.editor.getPanel(), "Center");
	    this.panel.add(pnlBtn, "South");
	    this.dlg.setContent(this.panel);
	    this.dlg.pack();
	  }

	  public int ShowEditor() {
	    this.result = 1;
	    this.editor.init();
	    this.dlg.pack();
	    Util.showFrameToCenter(this.dlg);

	    return this.result;
	  }
}
