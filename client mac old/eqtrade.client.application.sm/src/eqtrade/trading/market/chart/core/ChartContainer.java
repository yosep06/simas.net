package eqtrade.trading.market.chart.core;


import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartTheme;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SegmentedTimeline;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;

import eqtrade.trading.market.chart.ext.IChartDataset;
import eqtrade.trading.market.chart.ext.IChartPlot;

public class ChartContainer implements IChartPlot{

	  public static final String C_MIDDLE = "MIDDLE";
	  public static final String C_TOP = "TOP";
	  public static final String C_BOTTOM = "BOTTOM";
	  public static final String C_BOTTOM2 = "BOTTOM2";
	  public static final SimpleDateFormat holidayFormat = new SimpleDateFormat("ddMMyyyy HHmm");
	  public static final SimpleDateFormat holidayFormat2 = new SimpleDateFormat("ddMMyyyy");
	  public static final SimpleDateFormat holidayFormat3 = new SimpleDateFormat("HHmm");
	  public static final SimpleDateFormat fridayFormat = new SimpleDateFormat("E");
	  protected MultiplePlot middlePlot;
	  protected MultiplePlot bottomPlot;
	  protected MultiplePlot bottomPlot2;
	  protected MultiplePlot topPlot;
	  protected CombinedDomainXYPlot allPlot;
	  protected JFreeChart jfreechart = null;
	  protected ChartPanel panel;
	  protected DateAxis timeAxis;
	  protected NumberAxis middleAxis;
	  protected NumberAxis topAxis;
	  protected NumberAxis bottomAxis;
	  protected NumberAxis bottomAxis2;
	  protected SegmentedTimeline segment;
	  protected CandlestickRenderer candleRender;
	  protected XYLineAndShapeRenderer lineRender;
	  protected XYBarRenderer barRender;
	  protected IChartDataset dataset;

	  public ChartContainer(IChartDataset dataset)
	  {
	    this.dataset = dataset;
	    panel = new ChartPanelCH(createChart());
	  }

	  public void addPlot(String position, String id, XYDataset dataset, ValueAxis axis, XYItemRenderer render)
	  {
	    if (position.equals("MIDDLE"))
	      middlePlot.addPlot(id, dataset, axis, render);
	    else if (position.equals("TOP"))
	      topPlot.addPlot(id, dataset, axis, render);
	    else if (position.equals("BOTTOM2"))
	      bottomPlot2.addPlot(id, dataset, axis, render);
	    else
	      bottomPlot.addPlot(id, dataset, axis, render);
	  }

	  public XYPlot getPlot()
	  {
	    return allPlot;
	  }

	  public SegmentedTimeline getTimeline() {
	    return segment;
	  }

	  public XYPlot getMiddlePlot() {
	    return middlePlot;
	  }

	  public void removePlot(String position, String id)
	  {
	    if (position.equals("MIDDLE"))
	      middlePlot.removePlot(id);
	    else if (position.equals("TOP"))
	      topPlot.removePlot(id);
	    else if (position.equals("BOTTOM2"))
	      bottomPlot2.removePlot(id);
	    else
	      bottomPlot.removePlot(id);
	  }

	  public void updateDataset(String position, String id, XYDataset dataset)
	  {
	    if (position.equals("MIDDLE"))
	      middlePlot.updateDataset(id, dataset);
	    else if (position.equals("TOP"))
	      topPlot.updateDataset(id, dataset);
	    else if (position.equals("BOTTOM2"))
	      bottomPlot2.updateDataset(id, dataset);
	    else
	      bottomPlot.updateDataset(id, dataset);
	  }

	  public boolean isExistsPlot(String id)
	  {
	    return (middlePlot.isExistsPlot(id)) || (topPlot.isExistsPlot(id)) || (bottomPlot.isExistsPlot(id)) || (bottomPlot2.isExistsPlot(id));
	  }

	  public ChartPanel getPanel() {
	    return panel;
	  }

	  protected static SegmentedTimeline nineToFour() {
	    SegmentedTimeline timeline = new SegmentedTimeline(900000L, 28, 68);
	    timeline.setStartTime(SegmentedTimeline.firstMondayAfter1900() + 36L * timeline.getSegmentSize());
	    timeline.setBaseTimeline(SegmentedTimeline.newMondayThroughFridayTimeline());
	    return timeline;
	  }

	  protected static SegmentedTimeline session1() {
	    SegmentedTimeline timeline = new SegmentedTimeline(900000L, 14, 82);
	    timeline.setStartTime(SegmentedTimeline.firstMondayAfter1900() + 36L * timeline.getSegmentSize());
	    timeline.setBaseTimeline(nineToFour());
	    return timeline;
	  }

	  protected static SegmentedTimeline session2() {
	    SegmentedTimeline timeline = new SegmentedTimeline(900000L, 10, 86);
	    timeline.setStartTime(SegmentedTimeline.firstMondayAfter1900() + 54L * timeline.getSegmentSize());
	    timeline.setBaseTimeline(session1());
	    return timeline;
	  }

	  protected void changeSegmentation(int type, long from, long to) {
	    if (type == 0) {
	      segment = nineToFour();
	      try {
	        segment.addBaseTimelineExclusions(from, to);
	        Calendar cal = Calendar.getInstance();
	        cal.setTimeInMillis(from);
	        while (cal.getTimeInMillis() <= to) {
	          String date = holidayFormat2.format(cal.getTime());
	          String hr = fridayFormat.format(cal.getTime()).toUpperCase();
	          segment.addException(holidayFormat.parse(date + " 0900").getTime(), holidayFormat.parse(date + " 0859").getTime());
	          if (hr.equals("FRI")) {
	            segment.addException(holidayFormat.parse(date + " 1131").getTime(), holidayFormat.parse(date + " 1359").getTime());
	          }
	          else if ((hr.equals("SUN")) || (hr.equals("SAT")))
	            segment.addBaseTimelineException(holidayFormat2.parse(date).getTime());
	          else {
	            segment.addException(holidayFormat.parse(date + " 1201").getTime(), holidayFormat.parse(date + " 1328").getTime());
	          }
	          cal.add(5, 1);
	        }
	      } catch (Exception ex) {
	        ex.printStackTrace();
	      }
	      timeAxis.setTimeline(segment);
	      timeAxis.setDateFormatOverride(new SimpleDateFormat("dd-MMM-yy HH:mm"));
	    } else {
	      segment = SegmentedTimeline.newMondayThroughFridayTimeline();
	      timeAxis.setTimeline(segment);
	      timeAxis.setDateFormatOverride(new SimpleDateFormat("dd-MM-yyyy"));
	    }
	  }

	  protected JFreeChart createChart() {
	    timeAxis = new DateAxis("");
	    timeAxis.setDateFormatOverride(new SimpleDateFormat("dd-MM-yyyy"));
	    timeAxis.setVerticalTickLabels(true);
	    timeAxis.setAxisLinePaint(Color.white);
	    segment = SegmentedTimeline.newMondayThroughFridayTimeline();

	    timeAxis.setTimeline(segment);
	    middleAxis = new NumberAxis("");
	    middleAxis.setAutoRangeIncludesZero(false);
	    topAxis = new NumberAxis("");
	    bottomAxis = new NumberAxis("");
	    bottomAxis2 = new NumberAxis("");
	    candleRender = new CandlestickRenderer(4.0D);
	    candleRender.setDrawVolume(false);

	    lineRender = new XYLineAndShapeRenderer(true, false);
	    lineRender.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy"), new DecimalFormat("0.00")));
	    lineRender.setSeriesPaint(0, Color.blue);

	    barRender = new XYBarRenderer();
	    barRender.setDrawBarOutline(true);

	    middlePlot = new MultiplePlot("MIDDLE", null, timeAxis, middleAxis, candleRender);
	    topPlot = new MultiplePlot("TOP", null, timeAxis, topAxis, lineRender);
	    bottomPlot = new MultiplePlot("BOTTOM", null, timeAxis, bottomAxis, barRender);
	    bottomPlot2 = new MultiplePlot("BOTTOM2", null, timeAxis, bottomAxis2, lineRender);
	    allPlot = new CombinedDomainXYPlot(timeAxis);
	    allPlot.add(middlePlot, 55);
	    allPlot.add(topPlot, 15);

	    allPlot.add(bottomPlot2, 30);
	    allPlot.setGap(1.0D);
	    middlePlot.setDomainGridlinesVisible(false);
	    middlePlot.setRangeGridlinesVisible(true);
	    topPlot.setDomainGridlinesVisible(false);
	    topPlot.setRangeGridlinesVisible(true);
	    bottomPlot2.setDomainGridlinesVisible(false);
	    bottomPlot2.setRangeGridlinesVisible(true);
	    bottomAxis.setAutoRangeIncludesZero(true);
	    bottomAxis.setLabel("in millions");
	    middleAxis.setLabel("price");
	    middlePlot.setForegroundAlpha(0.95F);
	    bottomPlot.setForegroundAlpha(0.95F);
	    bottomPlot2.setForegroundAlpha(0.95F);

	    jfreechart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, allPlot, true);

	    middlePlot.setDomainCrosshairVisible(true);
	    middlePlot.setRangeCrosshairVisible(true);
	    middlePlot.setRangeCrosshairLockedOnData(true);
	    middlePlot.setDomainCrosshairLockedOnData(true);
	    topPlot.setDomainCrosshairVisible(true);
	    topPlot.setRangeCrosshairVisible(true);
	    topPlot.setRangeCrosshairLockedOnData(true);
	    topPlot.setDomainCrosshairLockedOnData(true);
	    bottomPlot.setDomainCrosshairVisible(true);
	    bottomPlot.setRangeCrosshairVisible(true);
	    bottomPlot.setRangeCrosshairLockedOnData(true);
	    bottomPlot.setDomainCrosshairLockedOnData(true);
	    bottomPlot2.setDomainCrosshairVisible(true);
	    bottomPlot2.setRangeCrosshairVisible(true);
	    bottomPlot2.setRangeCrosshairLockedOnData(true);
	    bottomPlot2.setDomainCrosshairLockedOnData(true);
	    StandardChartTheme.createDarknessTheme().apply(jfreechart);
	    return jfreechart;
	  }
}
