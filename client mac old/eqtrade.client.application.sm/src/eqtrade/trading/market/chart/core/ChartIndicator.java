package eqtrade.trading.market.chart.core;

import com.tictactec.ta.lib.MAType;

import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.ext.IChartListener;

import java.util.HashMap;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
public abstract class ChartIndicator implements IChartListener{
	
	  protected String position;
	  protected String name;
	  protected String type;
	  protected String securities;
	  protected HashMap parameter;
	  protected XYDataset xydataset;
	  protected NumberAxis axis;
	  protected XYItemRenderer render;
	  protected IChartController controller;

	  public ChartIndicator(IChartController controller, String position, String name, String type, String securities)
	  {
	    this.controller = controller;
	    this.position = position;
	    this.name = name;
	    this.type = type;
	    this.securities = securities;
	    this.parameter = new HashMap();
	    build();
	  }

	  protected double[] getInputData(String code) {
	    if (code.equals("High"))
	      return this.controller.getDataset().getHighArray();
	    if (code.equals("Low"))
	      return this.controller.getDataset().getLowArray();
	    if (code.equals("Open")) {
	      return this.controller.getDataset().getOpenArray();
	    }
	    return this.controller.getDataset().getCloseArray(); } 
	  protected abstract void build();

	  public abstract void calculate();

	  public abstract void show();

	  public abstract void hide();

	  public Object getParameter(String id) { return this.parameter.get(id); }

	  public void setParameter(String id, Object value)
	  {
	    this.parameter.put(id, value);
	  }

	  public NumberAxis getAxis() {
	    return this.axis;
	  }

	  public void setAxis(NumberAxis axis) {
	    this.axis = axis;
	  }

	  public XYDataset getDataset() {
	    return this.xydataset;
	  }

	  public void setDataset(XYDataset dataset) {
	    this.xydataset = dataset;
	  }

	  public String getName() {
	    return this.name;
	  }

	  public void setName(String name) {
	    this.name = name;
	  }

	  public String getPosition() {
	    return this.position;
	  }

	  public void setPosition(String position) {
	    this.position = position;
	  }

	  public XYItemRenderer getRender() {
	    return this.render;
	  }

	  public void setRender(XYItemRenderer render) {
	    this.render = render;
	  }

	  public String getStockcode() {
	    return this.securities;
	  }

	  public void setStockcode(String stockcode) {
	    this.securities = stockcode;
	  }

	  protected double[] getOutputArray(double[] barData, int lookback)
	  {
	    int startIdx = 0;
	    int endIdx = barData.length - 1;

	    int temp = Math.max(lookback, startIdx);
	    int allocationSize = temp > endIdx ? 0 : endIdx - temp + 1;
	    return new double[allocationSize];
	  }

	  protected MAType getTA_MAType(int type) {
	    MAType maType = MAType.Sma;
	    switch (type) {
	    case 0:
	      maType = MAType.Sma;
	      break;
	    case 1:
	      maType = MAType.Ema;
	      break;
	    case 2:
	      maType = MAType.Wma;
	      break;
	    case 3:
	      maType = MAType.Dema;
	      break;
	    case 4:
	      maType = MAType.Tema;
	      break;
	    case 5:
	      maType = MAType.Trima;
	      break;
	    case 6:
	      maType = MAType.Kama;
	      break;
	    case 7:
	      maType = MAType.Mama;
	      break;
	    case 8:
	      maType = MAType.T3;
	    }

	    return maType;
	  }

}
