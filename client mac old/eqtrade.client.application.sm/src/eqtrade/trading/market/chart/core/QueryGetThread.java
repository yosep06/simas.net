package eqtrade.trading.market.chart.core;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Vector;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;

import eqtrade.trading.market.chart.ext.ICallback;
import eqtrade.trading.market.chart.ext.IResult;
import eqtrade.trading.market.chart.model.Chart;

public class QueryGetThread extends Thread {
	  protected HttpClient httpClient;
	  protected Vector method;
	  protected ICallback callback;
	  protected IResult process;
	  protected Vector info;
	  protected String stock;

	  public QueryGetThread(String stock, HttpClient httpClient, Vector info, Vector method, ICallback callback, IResult result)
	  {
	    this.httpClient = httpClient;
	    this.method = method;
	    this.callback = callback;
	    this.process = result;
	    this.info = info;
	    this.stock = stock;
	  }

	  public void run() {
	    this.httpClient.getHostConfiguration().setProxyHost(null);
	    boolean success = true;
	    for (int counter = 0; (counter < this.method.size()) && (success); )
	      try {
	        Vector row = new Vector(50, 5);
	        if (this.callback != null) this.callback.setStatus("loading " + this.info.get(counter) + "...");
	        this.httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
	        int statusCode = this.httpClient.executeMethod((GetMethod)this.method.get(counter));
	        if (statusCode == 200) {
	          BufferedReader in = new BufferedReader(new InputStreamReader(((GetMethod)this.method.get(counter)).getResponseBodyAsStream()));
	          String line = null;
	          while ((line = in.readLine()) != null)
	          {
	            if (!line.startsWith("Date")) {
	              String[] item = line.split(",");
	              if (item.length >= 6) {
	                Chart c = new Chart();
	                c.setStock(this.stock);
	                c.setDate(item[0]);
	                c.setTime("");
	                c.setOpen(item[1].replace(',', '.'));
	                c.setHigh(item[2].replace(',', '.'));
	                c.setLow(item[3].replace(',', '.'));
	                c.setClose(item[4].replace(',', '.'));
	                c.setVolume(item[5]);
	                row.add(0, c);
	              }
	            }
	          }
	          in.close();
	          this.process.receiveData(row);
	        } else {
	          success = false;
	          this.process.receiveData(new Vector());
	          if (this.callback != null) this.callback.failed("failed, data server not ready try again later"); 
	        }
	      }
	      catch (Exception ex) {
	        success = false;
	        ex.printStackTrace();
	        this.process.receiveData(new Vector());
	        if (this.callback != null) this.callback.failed("failed, data server not ready try again later"); 
	      }
	      finally {
	        ((GetMethod)this.method.get(counter)).releaseConnection();
	        
			//ret;
	      }
	    if ((this.callback != null) && (success)) this.callback.success();
	  }
}
