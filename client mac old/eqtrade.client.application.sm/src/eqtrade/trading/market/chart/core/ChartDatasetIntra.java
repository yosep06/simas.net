package eqtrade.trading.market.chart.core;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.DefaultHighLowDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.OHLCDataset;
import org.jfree.data.xy.XYDataset;

import eqtrade.trading.market.chart.ext.ICallback;
import eqtrade.trading.market.chart.ext.IChartDataset;
import eqtrade.trading.market.chart.ext.IChartDatasource;
import eqtrade.trading.market.chart.ext.IChartListener;
import eqtrade.trading.market.chart.ext.IResult;
import eqtrade.trading.market.chart.model.Chart;

public class ChartDatasetIntra implements IChartDataset, IResult{
	  protected Vector vtData = new Vector(50, 10);
	  protected Vector vtListener = new Vector(10, 5);

	  protected OHLCDataset ohlcDataset = new DefaultHighLowDataset("Series", new Date[0], new double[0], new double[0], new double[0], new double[0], new double[0]);
	  protected IntervalXYDataset volDataset = new TimeSeriesCollection();
	  protected IntervalXYDataset openDataset = new TimeSeriesCollection();
	  protected IntervalXYDataset highDataset = new TimeSeriesCollection();
	  protected IntervalXYDataset lowDataset = new TimeSeriesCollection();
	  protected IntervalXYDataset closeDataset = new TimeSeriesCollection();
	  protected Date[] date = new Date[this.vtData.size()];
	  protected double[] high = new double[this.vtData.size()];
	  protected double[] low = new double[this.vtData.size()];
	  protected double[] open = new double[this.vtData.size()];
	  protected double[] close = new double[this.vtData.size()];
	  protected double[] vol = new double[this.vtData.size()];
	  protected Object[] object = new Object[5];
	  protected IChartDatasource ds;

	  public ChartDatasetIntra(IChartDatasource ds)
	  {
	    this.ds = ds;
	  }

	  public void setDatasource(IChartDatasource ds) {
	    this.ds = ds;
	  }

	  public void loadData(String type, String stock, String from, String to, boolean adjusted, ICallback callback) {
	    this.ds.getData(type, stock, from, to, adjusted, this, callback);
	  }

	  public void receiveData(Vector data) {
	    this.vtData = data;
	    try {
	      this.date = new Date[this.vtData.size()];
	      this.high = new double[this.vtData.size()];
	      this.low = new double[this.vtData.size()];
	      this.open = new double[this.vtData.size()];
	      this.close = new double[this.vtData.size()];
	      this.vol = new double[this.vtData.size()];

	      for (int i = 0; i < this.vtData.size(); i++) {
	        Chart chart = (Chart)this.vtData.elementAt(i);
	        Date dtTemp = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(chart.getDate());
	        this.date[i] = dtTemp;
	        this.high[i] = Double.parseDouble(chart.getHigh());
	        this.low[i] = Double.parseDouble(chart.getLow());
	        this.open[i] = Double.parseDouble(chart.getOpen());
	        this.close[i] = Double.parseDouble(chart.getClose());
	        this.vol[i] = Double.parseDouble(chart.getVolume());
	      }

	      this.object[0] = this.open;
	      this.object[1] = this.high;
	      this.object[2] = this.low;
	      this.object[3] = this.close;
	      this.object[4] = this.vol;

	      this.ohlcDataset = createOHLC();
	      this.volDataset = createVol();
	      this.openDataset = createOpen();
	      this.highDataset = createHigh();
	      this.lowDataset = createLow();
	      this.closeDataset = createClose();
	    } catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    chartChanged();
	  }

	  public Date[] getDateArray() {
	    return this.date;
	  }

	  public XYDataset getOHLCDataset() {
	    return this.ohlcDataset;
	  }

	  public XYDataset getVolDataset() {
	    return this.volDataset;
	  }

	  public XYDataset getOpenDataset() {
	    return this.openDataset;
	  }

	  public XYDataset getHighDataset() {
	    return this.highDataset;
	  }

	  public XYDataset getLowDataset() {
	    return this.lowDataset;
	  }

	  public XYDataset getCloseDataset() {
	    return this.closeDataset;
	  }

	  public Object[] getOHLCArray() {
	    return this.object;
	  }
	  public double[] getVolArray() {
	    return this.vol;
	  }
	  public double[] getOpenArray() {
	    return this.open;
	  }
	  public double[] getHighArray() {
	    return this.high;
	  }
	  public double[] getLowArray() {
	    return this.low;
	  }
	  public double[] getCloseArray() {
	    return this.close;
	  }

	  protected OHLCDataset createOHLC() throws Exception {
	    return new DefaultHighLowDataset("OHLC", this.date, this.high, this.low, this.open, this.close, this.vol);
	  }

	  protected IntervalXYDataset createVol() throws Exception {
	    TimeSeries timeseries = new TimeSeries("volume", "", "", Minute.class);
	    for (int i = 0; i < this.vtData.size(); i++) {
	      Chart chart = (Chart)this.vtData.elementAt(i);
	      Date dtTemp = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(chart.getDate());

	      timeseries.add(Minute.parseMinute(chart.getDate()), Double.parseDouble(chart.getVolume()) / 1000000.0D);
	    }
	    TimeSeriesCollection timeseriescollection = new TimeSeriesCollection(timeseries);
	    return timeseriescollection;
	  }
	  protected IntervalXYDataset createOpen() throws Exception {
	    TimeSeries timeseries = new TimeSeries("open price", "", "", Minute.class);
	    for (int i = 0; i < this.vtData.size(); i++) {
	      Chart chart = (Chart)this.vtData.elementAt(i);
	      Date dtTemp = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(chart.getDate());

	      timeseries.add(Minute.parseMinute(chart.getDate()), Double.parseDouble(chart.getOpen()));
	    }
	    TimeSeriesCollection timeseriescollection = new TimeSeriesCollection(timeseries);
	    return timeseriescollection;
	  }
	  protected IntervalXYDataset createHigh() throws Exception {
	    TimeSeries timeseries = new TimeSeries("high price", "", "", Minute.class);
	    for (int i = 0; i < this.vtData.size(); i++) {
	      Chart chart = (Chart)this.vtData.elementAt(i);
	      Date dtTemp = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(chart.getDate());

	      timeseries.add(Minute.parseMinute(chart.getDate()), Double.parseDouble(chart.getHigh()));
	    }
	    TimeSeriesCollection timeseriescollection = new TimeSeriesCollection(timeseries);
	    return timeseriescollection;
	  }
	  protected IntervalXYDataset createLow() throws Exception {
	    TimeSeries timeseries = new TimeSeries("low price", "", "", Minute.class);
	    for (int i = 0; i < this.vtData.size(); i++) {
	      Chart chart = (Chart)this.vtData.elementAt(i);
	      Date dtTemp = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(chart.getDate());

	      timeseries.add(Minute.parseMinute(chart.getDate()), Double.parseDouble(chart.getLow()));
	    }
	    TimeSeriesCollection timeseriescollection = new TimeSeriesCollection(timeseries);
	    return timeseriescollection;
	  }

	  protected IntervalXYDataset createClose() throws Exception {
	    TimeSeries timeseries = new TimeSeries("closing price", "", "", Minute.class);
	    for (int i = 0; i < this.vtData.size(); i++) {
	      Chart chart = (Chart)this.vtData.elementAt(i);
	      Date dtTemp = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(chart.getDate());

	      timeseries.add(Minute.parseMinute(chart.getDate()), Double.parseDouble(chart.getClose()));
	    }
	    TimeSeriesCollection timeseriescollection = new TimeSeriesCollection(timeseries);
	    return timeseriescollection;
	  }

	  public void addListener(IChartListener listener) {
	    this.vtListener.add(listener);
	  }

	  public void removeListener(IChartListener listener) {
	    this.vtListener.remove(listener);
	  }

	  public void chartChanged() {
	    for (int i = 0; i < this.vtListener.size(); i++)
	      ((IChartListener)this.vtListener.get(i)).chartChanged();
	  }

}
