package eqtrade.trading.market.chart.util;

import com.vollux.ui.JNumber;
import com.vollux.ui.JText;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;

public class ChartUtil {
	  public static final String C_CHRT_LINE = "line based";
	  public static final String C_CHRT_STICK = "stick based";
	  public static final String C_CHRT_CANDLESTICK = "candlestick based";
	  public static final String C_CHRT_DOT = "dot based";
	  public static final String C_STOCK = "stock";
	  public static final String C_DATE = "date";
	  public static final String C_TIME = "time";
	  public static final String C_HIGH = "high";
	  public static final String C_LOW = "low";
	  public static final String C_OPEN = "open";
	  public static final String C_CLOSE = "close";
	  public static final String C_VOLUME = "volume";
	  public static final String C_FRM_DATETIME = "yyyy-MM-dd";
	  public static final String C_FRM_DATETIMEINTRA = "yyyy-MM-dd HH:mm";
	  public static final String C_FRM_DATE = "yyMMdd";
	  public static final String C_FRM_TIME = "HHmmss";
	  public static final SimpleDateFormat C_FORMAT = new SimpleDateFormat(" MM/dd/yyyy ");
	  public static final SimpleDateFormat C_FORMAT2 = new SimpleDateFormat("yyyy/MM/dd");
	  public static final SimpleDateFormat C_FORMAT3 = new SimpleDateFormat("yyyy-MM-dd");
	  
	  public static JComboBox getComboOHLC(){
	    return new JComboBox(new Object[] { "High", "Low", "Open", "Close" });
	  }
	  
	  public static JComboBox getComboMA() {
		 return new JComboBox(new Object[] { "Simple", "Exponential", "Weighted", "Double Exponential", "Triple Exponential", "Triangular", "Kaufman Adaptive" });
	  }
	  public static JText getFieldColor() {
		    JText comp = new JText(false);
		    comp.addMouseListener(new MouseAdapter() {
		      public void mouseClicked(MouseEvent arg0) {
		        if ((arg0.getClickCount() == 2) && (arg0.getButton() == 1)) {
		          Color initialBackground = ChartUtil.getBackground();
		          Color background = JColorChooser.showDialog(null, "Color", initialBackground);
		          if (background != null) ChartUtil.setBackground(background);
		        }
		      }
		    });
		    return comp;
		  }
	

	protected static void setBackground(Color background) {
		
		
	}

	protected static Color getBackground() {
		
		return Color.black;
	}

	public static JNumber getFieldNumber(int maxdigit, int maxfraksi) {
		    return new JNumber(Double.class, maxdigit, maxfraksi, true);
		  }


	  
}
