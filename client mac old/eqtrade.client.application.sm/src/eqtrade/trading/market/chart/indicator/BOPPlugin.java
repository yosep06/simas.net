package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JLabel;
public class BOPPlugin extends ChartIndicatorPlugin{
	 private BOPIndicator bop = null;
	  private JText color;

	  public BOPPlugin(IChartController controller)
	  {
	    super(controller, "Balance of Power - BOP");
	    this.bop = new BOPIndicator(this.controller, "BOP", "");
	  }

	  protected void build() {
	    super.build();
	    this.color = ChartUtil.getFieldColor();
	    this.color.setBackground(ATRIndicator.DEFAULT_COLOR);
	    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, this.panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("Color:"), cc.xy(1, 1));
	    builder.add(this.color, cc.xy(3, 1));
	  }
	  public void show() {
	    this.bop.show();
	  }
	  public void hide() {
	    this.bop.hide();
	  }
	  public void init() {
	    this.color.setBackground(this.bop.getColor());
	  }
	  public void update() {
	    this.bop.setColor(this.color.getBackground());
	  }
}
