package eqtrade.trading.market.chart.indicator;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JNumber;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JComboBox;
import javax.swing.JLabel;
public class BBANDSPlugin extends ChartIndicatorPlugin {
	  private BBANDSIndicator bbands = null;
	  private JText color;
	  private JComboBox comboInput;
	  private JNumber period;
	  private JComboBox comboMA;
	  private JNumber upperBand;
	  private JNumber lowerBand;

	  public BBANDSPlugin(IChartController controller)
	  {
	    super(controller, "Bollinger Bands - BBANDS");
	    bbands = new BBANDSIndicator(controller, "BBANDS", "");
	  }

	  protected void build() {
	    super.build();
	    color = ChartUtil.getFieldColor();
	    comboInput = ChartUtil.getComboOHLC();
	    period = ChartUtil.getFieldNumber(2, 0);
	    comboMA = ChartUtil.getComboMA();
	    upperBand = ChartUtil.getFieldNumber(0, 2);
	    lowerBand = ChartUtil.getFieldNumber(0, 2);
	    color.setBackground(BBANDSIndicator.DEFAULT_COLOR);
	    comboInput.setSelectedItem("Close");
	    period.setValue(new Double(14.0D));
	    comboMA.setSelectedIndex(0);
	    upperBand.setValue(new Double(2.0D));
	    lowerBand.setValue(new Double(2.0D));
	    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("Color:"), cc.xy(1, 1));
	    builder.add(color, cc.xy(3, 1));
	    builder.add(new JLabel("Input:"), cc.xy(1, 3));
	    builder.add(comboInput, cc.xy(3, 3));
	    builder.add(new JLabel("Period:"), cc.xy(1, 5));
	    builder.add(period, cc.xy(3, 5));
	    builder.add(new JLabel("MA Type:"), cc.xy(1, 7));
	    builder.add(comboMA, cc.xy(3, 7));
	    builder.add(new JLabel("Upper Band:"), cc.xy(1, 9));
	    builder.add(upperBand, cc.xy(3, 9));
	    builder.add(new JLabel("Lower Band:"), cc.xy(1, 11));
	    builder.add(lowerBand, cc.xy(3, 11));
	  }

	  public void show() {
	    bbands.show();
	  }
	  public void hide() {
	    bbands.hide();
	  }
	  public void init() {
	    color.setBackground(bbands.getColor());
	    comboInput.setSelectedItem(bbands.getInput());
	    period.setValue(new Double(bbands.getPeriod()));
	    comboMA.setSelectedIndex(bbands.getMaType());
	    upperBand.setValue(new Double(bbands.getUpperDeviation()));
	    lowerBand.setValue(new Double(bbands.getLowerDeviation()));
	  }
	  public void update() {
	    bbands.setColor(color.getBackground());
	    bbands.setInput(comboInput.getSelectedItem().toString());
	    bbands.setPeriod(((Double)period.getValue()).intValue());
	    bbands.setMaType(comboMA.getSelectedIndex());
	    bbands.setUpperDeviation(((Double)upperBand.getValue()).doubleValue());
	    bbands.setLowerDeviation(((Double)lowerBand.getValue()).doubleValue());
	  }
}
