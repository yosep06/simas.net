package eqtrade.trading.market.chart.indicator;
import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.TimeSeriesCollection;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;
public class LineIndicator extends ChartIndicator{
	 public static final Color DEFAULT_COLOR = Color.green.darker();
	  public static final String DEFAULT_TYPE = "Close";
	  private Color color = DEFAULT_COLOR;
	  private String type = "Close";

	  public LineIndicator(IChartController controller, String name, String securities) {
	    super(controller, "MIDDLE", name, "line", securities);
	    build();
	  }

	  public LineIndicator(IChartController controller, String name, String securities, Color color) {
	    super(controller, "MIDDLE", name, "line", securities);
	    this.color = color;
	    build();
	  }

	  protected void build()
	  {
	    this.render = new XYLineAndShapeRenderer(true, false);
	    this.render.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    this.render.setSeriesPaint(0, this.color);
	    this.render.setSeriesStroke(0, new BasicStroke(1.65F));
	  }

	  public void calculate() {
	    if (this.type.equals("High"))
	      this.xydataset = this.controller.getDataset().getHighDataset();
	    else if (this.type.equals("Low"))
	      this.xydataset = this.controller.getDataset().getLowDataset();
	    else if (this.type.equals("Open"))
	      this.xydataset = this.controller.getDataset().getOpenDataset();
	    else
	      this.xydataset = this.controller.getDataset().getCloseDataset();
	  }

	  public void show()
	  {
	    this.render.setSeriesPaint(0, this.color);
	    calculate();
	    this.controller.getDataset().addListener(this);
	    this.controller.getContainer().addPlot(this.position, this.name, this.xydataset, null, this.render);
	  }

	  public void hide() {
	    this.controller.getDataset().removeListener(this);
	    this.controller.getContainer().removePlot(this.position, this.name);
	  }

	  public void chartChanged()
	  {
	    ((TimeSeriesCollection)this.xydataset).removeAllSeries();
	    calculate();
	    this.controller.getContainer().updateDataset(this.position, this.name, this.xydataset);
	  }

	  public Color getColor() {
	    return this.color;
	  }

	  public void setColor(Color color) {
	    this.color = color;
	  }

	  public String getType() {
	    return this.type;
	  }

	  public void setType(String type) {
	    this.type = type;
	  }
}
