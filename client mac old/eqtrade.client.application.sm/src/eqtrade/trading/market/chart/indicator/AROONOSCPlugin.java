package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JNumber;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JLabel;


public class AROONOSCPlugin extends ChartIndicatorPlugin
{
  private AROONOSCIndicator aroonosc = null;
  private JText color;
  private JNumber period;

  public AROONOSCPlugin(IChartController controller)
  {
    super(controller, "Aroon Oscillator- AROONOSC");
    this.aroonosc = new AROONOSCIndicator(controller, "AROONOSC", "");
  }

  protected void build() {
    super.build();
    this.color = ChartUtil.getFieldColor();
    this.period = ChartUtil.getFieldNumber(0, 0);
    this.color.setBackground(AROONIndicator.DEFAULT_COLOR);
    this.period.setValue(new Double(7.0D));
    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
    PanelBuilder builder = new PanelBuilder(layout, this.panel);
    CellConstraints cc = new CellConstraints();
    builder.add(new JLabel("Color Up:"), cc.xy(1, 1));
    builder.add(this.color, cc.xy(3, 1));
    builder.add(new JLabel("Medium Period:"), cc.xy(1, 3));
    builder.add(this.period, cc.xy(3, 3));
  }
  public void show() {
    this.aroonosc.show();
  }
  public void hide() {
    this.aroonosc.hide();
  }
  public void init() {
    this.color.setBackground(this.aroonosc.getColor());
    this.period.setValue(new Double(this.aroonosc.getPeriod()));
  }
  public void update() {
    this.aroonosc.setColor(this.color.getBackground());
    this.aroonosc.setPeriod(((Double)this.period.getValue()).intValue());
  }
}