package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JNumber;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JComboBox;
import javax.swing.JLabel;

public class STOCHRSIPlugin extends ChartIndicatorPlugin{
	  private STOCHRSIIndicator stochrsi = null;
	  private JText color;
	  private JNumber period;
	  private JNumber fastK;
	  private JText colorD;
	  private JNumber fastD;
	  private JComboBox fastDMA;

	  public STOCHRSIPlugin(IChartController controller)
	  {
	    super(controller, "Stochastic RSI - STOCHRSI");
	    stochrsi = new STOCHRSIIndicator(controller, "STOCHRSI", "");
	  }

	  protected void build() {
	    super.build();
	    color = ChartUtil.getFieldColor();
	    period = ChartUtil.getFieldNumber(0, 0);
	    fastK = ChartUtil.getFieldNumber(0, 0);
	    colorD = ChartUtil.getFieldColor();
	    fastD = ChartUtil.getFieldNumber(0, 0);
	    fastDMA = ChartUtil.getComboMA();

	    color.setBackground(STOCHRSIIndicator.DEFAULT_COLOR);
	    period.setValue(new Double(3.0D));
	    fastK.setValue(new Double(5.0D));
	    colorD.setBackground(STOCHRSIIndicator.DEFAULT_D_COLOR);
	    fastD.setValue(new Double(3.0D));
	    fastDMA.setSelectedIndex(0);
	    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("Color:"), cc.xy(1, 1));
	    builder.add(color, cc.xy(3, 1));
	    builder.add(new JLabel("Period:"), cc.xy(1, 3));
	    builder.add(period, cc.xy(3, 3));
	    builder.add(new JLabel("Fast %K Period:"), cc.xy(1, 5));
	    builder.add(fastK, cc.xy(3, 5));
	    builder.add(new JLabel("Fast %D Period:"), cc.xy(1, 7));
	    builder.add(fastD, cc.xy(3, 7));
	    builder.add(new JLabel("Fast %D MA Type:"), cc.xy(1, 9));
	    builder.add(fastDMA, cc.xy(3, 9));
	    builder.add(new JLabel("%D Color"), cc.xy(1, 11));
	    builder.add(colorD, cc.xy(3, 11));
	  }
	  public void show() {
	    stochrsi.show();
	  }
	  public void hide() {
	    stochrsi.hide();
	  }
	  public void init() {
	    color.setBackground(stochrsi.getColor());
	    period.setValue(new Double(stochrsi.getPeriod()));
	    fastK.setValue(new Double(stochrsi.getFastKPeriod()));
	    fastD.setValue(new Double(stochrsi.getFastDPeriod()));
	    fastDMA.setSelectedIndex(stochrsi.getFastDMAType());
	  }
	  public void update() {
	    stochrsi.setColor(color.getBackground());
	    stochrsi.setPeriod(((Double)period.getValue()).intValue());
	    stochrsi.setFastKPeriod(((Double)fastK.getValue()).intValue());
	    stochrsi.setFastDPeriod(((Double)fastD.getValue()).intValue());
	    stochrsi.setFastDMAType(fastDMA.getSelectedIndex());
	  }
}
