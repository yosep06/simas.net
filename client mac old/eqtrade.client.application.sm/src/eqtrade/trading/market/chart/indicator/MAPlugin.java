package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JNumber;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class MAPlugin extends ChartIndicatorPlugin{
	 private MAIndicator ma = null;
	  private MAIndicator ma2 = null;
	  private MAIndicator ma3 = null;
	  private JComboBox comboMA1;
	  private JComboBox comboMA2;
	  private JComboBox comboMA3;
	  private JNumber period1;
	  private JNumber period2;
	  private JNumber period3;
	  private JText color1;
	  private JText color2;
	  private JText color3;

	  public MAPlugin(IChartController controller)
	  {
	    super(controller, "Moving Average - MA");
	    this.ma = new MAIndicator(this.controller, "MA", "", 7, 0, Color.blue);
	    this.ma2 = new MAIndicator(this.controller, "MA2", "", 14, 0, Color.red);
	    this.ma3 = new MAIndicator(this.controller, "MA3", "", 21, 0, Color.gray);
	  }

	  protected void build() {
	    super.build();
	    this.comboMA1 = ChartUtil.getComboMA();
	    this.period1 = ChartUtil.getFieldNumber(0, 0);
	    this.color1 = ChartUtil.getFieldColor();
	    this.comboMA2 = ChartUtil.getComboMA();
	    this.period2 = ChartUtil.getFieldNumber(0, 0);
	    this.color2 = ChartUtil.getFieldColor();
	    this.comboMA3 = ChartUtil.getComboMA();
	    this.period3 = ChartUtil.getFieldNumber(0, 0);
	    this.color3 = ChartUtil.getFieldColor();
	    this.comboMA1.setSelectedIndex(0);
	    this.color1.setBackground(Color.blue);
	    this.period1.setValue(new Double(7.0D));
	    this.comboMA2.setSelectedIndex(0);
	    this.color2.setBackground(Color.red);
	    this.period2.setValue(new Double(14.0D));
	    this.comboMA3.setSelectedIndex(0);
	    this.color3.setBackground(Color.gray);
	    this.period3.setValue(new Double(21.0D));
	    FormLayout layout = new FormLayout("pref, 2dlu, pref, 2dlu, 50px, 2dlu", "pref, 2dlu, pref, 2dlu, pref,2dlu, pref, 2dlu, pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, this.panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("MA1:"), cc.xy(1, 1));
	    builder.add(this.comboMA1, cc.xy(3, 1));
	    builder.add(this.period1, cc.xy(5, 1));
	    builder.add(this.color1, cc.xywh(3, 3, 3, 1));
	    builder.add(new JLabel("MA2:"), cc.xy(1, 5));
	    builder.add(this.comboMA2, cc.xy(3, 5));
	    builder.add(this.period2, cc.xy(5, 5));
	    builder.add(this.color2, cc.xywh(3, 7, 3, 1));
	    builder.add(new JLabel("MA3:"), cc.xy(1, 9));
	    builder.add(this.comboMA3, cc.xy(3, 9));
	    builder.add(this.period3, cc.xy(5, 9));
	    builder.add(this.color3, cc.xywh(3, 11, 3, 1));
	  }

	  public void show() {
	    this.ma.show();
	    this.ma2.show();
	    this.ma3.show();
	  }
	  public void hide() {
	    this.ma.hide();
	    this.ma2.hide();
	    this.ma3.hide();
	  }
	  public void init() {
	    this.comboMA1.setSelectedIndex(this.ma.getType());
	    this.color1.setBackground(this.ma.getColor());
	    this.period1.setValue(new Double(this.ma.getPeriod()));
	    this.comboMA2.setSelectedIndex(this.ma2.getType());
	    this.color2.setBackground(this.ma2.getColor());
	    this.period2.setValue(new Double(this.ma2.getPeriod()));
	    this.comboMA3.setSelectedIndex(this.ma3.getType());
	    this.color3.setBackground(this.ma3.getColor());
	    this.period3.setValue(new Double(this.ma3.getPeriod()));
	  }
	  public void update() {
	    this.ma.setType(this.comboMA1.getSelectedIndex());
	    this.ma.setColor(this.color1.getBackground());
	    this.ma.setPeriod(((Double)this.period1.getValue()).intValue());
	    this.ma2.setType(this.comboMA2.getSelectedIndex());
	    this.ma2.setColor(this.color2.getBackground());
	    this.ma2.setPeriod(((Double)this.period2.getValue()).intValue());
	    this.ma3.setType(this.comboMA3.getSelectedIndex());
	    this.ma3.setColor(this.color3.getBackground());
	    this.ma3.setPeriod(((Double)this.period3.getValue()).intValue());
	  }
}
