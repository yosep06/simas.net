package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JNumber;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class MACDPlugin extends ChartIndicatorPlugin{
	  private MACDIndicator macd = null;
	  private JText color;
	  private JNumber fastPeriod;
	  private JComboBox fastMA;
	  private JNumber slowPeriod;
	  private JComboBox slowMA;
	  private JText colorSignal;
	  private JNumber signalPeriod;
	  private JComboBox signalMA;

	  public MACDPlugin(IChartController controller)
	  {
	    super(controller, "Moving Average Convergence/Divergence - MACD");
	    macd = new MACDIndicator(controller, "MACD", "");
	    menu.setSelected(true);
	    macd.show();
	    show = true;
	  }

	  protected void build() {
	    super.build();
	    color = ChartUtil.getFieldColor();
	    fastPeriod = ChartUtil.getFieldNumber(0, 0);
	    fastMA = ChartUtil.getComboMA();
	    slowPeriod = ChartUtil.getFieldNumber(0, 0);
	    slowMA = ChartUtil.getComboMA();
	    colorSignal = ChartUtil.getFieldColor();
	    signalPeriod = ChartUtil.getFieldNumber(0, 0);
	    signalMA = ChartUtil.getComboMA();

	    color.setBackground(MACDIndicator.DEFAULT_COLOR);
	    fastPeriod.setValue(new Double(12.0D));
	    fastMA.setSelectedIndex(1);
	    slowPeriod.setValue(new Double(26.0D));
	    slowMA.setSelectedIndex(1);
	    colorSignal.setBackground(MACDIndicator.DEFAULT_SIGNAL_COLOR);
	    signalPeriod.setValue(new Double(9.0D));
	    signalMA.setSelectedIndex(1);

	    FormLayout layout = new FormLayout("pref, 2dlu, pref, 2dlu, 50px", "pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("Color:"), cc.xy(1, 1));
	    builder.add(color, cc.xywh(3, 1, 3, 1));
	    builder.add(new JLabel("Fast Period:"), cc.xy(1, 3));
	    builder.add(fastMA, cc.xy(3, 3));
	    builder.add(fastPeriod, cc.xy(5, 3));
	    builder.add(new JLabel("Slow Period:"), cc.xy(1, 5));
	    builder.add(slowMA, cc.xy(3, 5));
	    builder.add(slowPeriod, cc.xy(5, 5));
	    builder.add(new JLabel("Signal Color:"), cc.xy(1, 7));
	    builder.add(colorSignal, cc.xywh(3, 7, 3, 1));
	    builder.add(new JLabel("Signal Period:"), cc.xy(1, 9));
	    builder.add(signalMA, cc.xy(3, 9));
	    builder.add(signalPeriod, cc.xy(5, 9));
	  }
	  public void show() {
	    macd.show();
	  }
	  public void hide() {
	    macd.hide();
	  }
	  public void init() {
	    color.setBackground(macd.getColor());
	    fastPeriod.setValue(new Double(macd.getFastPeriod()));
	    fastMA.setSelectedIndex(macd.getFastMaType());
	    slowPeriod.setValue(new Double(macd.getSlowPeriod()));
	    slowMA.setSelectedIndex(macd.getSlowMaType());
	    colorSignal.setBackground(macd.getSignalColor());
	    signalPeriod.setValue(new Double(macd.getSignalPeriod()));
	    signalMA.setSelectedIndex(macd.getSignalMaType());
	  }

	  public void update() {
	    macd.setColor(color.getBackground());
	    macd.setFastPeriod(((Double)fastPeriod.getValue()).intValue());
	    macd.setFastMaType(fastMA.getSelectedIndex());
	    macd.setSlowPeriod(((Double)slowPeriod.getValue()).intValue());
	    macd.setSlowMaType(slowMA.getSelectedIndex());
	    macd.setSignalColor(colorSignal.getBackground());
	    macd.setSignalPeriod(((Double)signalPeriod.getValue()).intValue());
	    macd.setSignalMaType(signalMA.getSelectedIndex());
	  }
}
