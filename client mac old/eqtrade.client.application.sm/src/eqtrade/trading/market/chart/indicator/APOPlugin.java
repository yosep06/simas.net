package eqtrade.trading.market.chart.indicator;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JNumber;
import com.vollux.ui.JText;
import javax.swing.JComboBox;
import javax.swing.JLabel;

public class APOPlugin extends ChartIndicatorPlugin{
	 private APOIndicator apo = null;
	  private JText color;
	  private JNumber fastPeriod;
	  private JNumber slowPeriod;
	  private JComboBox maType;

	  public APOPlugin(IChartController controller)
	  {
	    super(controller, "Absolute Price Oscillator - APO");
	    apo = new APOIndicator(controller, "APOs", "");
	  }

	  protected void build() {
	    super.build();
	    color = ChartUtil.getFieldColor();
	    color.setBackground(ULTOSCIndicator.DEFAULT_COLOR);
	    fastPeriod = ChartUtil.getFieldNumber(0, 0);
	    slowPeriod = ChartUtil.getFieldNumber(0, 0);
	    maType = ChartUtil.getComboMA();

	    color.setBackground(APOIndicator.DEFAULT_COLOR);
	    fastPeriod.setValue(new Double(3.0D));
	    slowPeriod.setValue(new Double(10.0D));
	    maType.setSelectedIndex(0);
	    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("Color:"), cc.xy(1, 1));
	    builder.add(color, cc.xy(3, 1));
	    builder.add(new JLabel("Fast Period:"), cc.xy(1, 3));
	    builder.add(fastPeriod, cc.xy(3, 3));
	    builder.add(new JLabel("Slow Period:"), cc.xy(1, 5));
	    builder.add(slowPeriod, cc.xy(3, 5));
	    builder.add(new JLabel("MA Type:"), cc.xy(1, 7));
	    builder.add(maType, cc.xy(3, 7));
	  }
	  public void show() {
	    apo.show();
	  }
	  public void hide() {
	    apo.hide();
	  }
	  public void init() {
	    color.setBackground(apo.getColor());
	    fastPeriod.setValue(new Double(apo.getFastPeriod()));
	    slowPeriod.setValue(new Double(apo.getSlowPeriod()));
	    maType.setSelectedIndex(apo.getMaType());
	  }
	  public void update() {
	    apo.setColor(color.getBackground());
	    apo.setFastPeriod(((Double)fastPeriod.getValue()).intValue());
	    apo.setSlowPeriod(((Double)slowPeriod.getValue()).intValue());
	    apo.setMaType(maType.getSelectedIndex());
	  }
}
