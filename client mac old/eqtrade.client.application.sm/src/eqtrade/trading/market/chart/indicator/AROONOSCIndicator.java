package eqtrade.trading.market.chart.indicator;

import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;

import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class AROONOSCIndicator extends ChartIndicator {
	  public static final Color DEFAULT_COLOR = Color.white;
	  public static final int DEFAULT_PERIOD = 14;
	  private Color color = DEFAULT_COLOR;
	  private int period = 14;

	  public AROONOSCIndicator(IChartController controller, String name, String securities) {
	    super(controller, "TOP", name, "line", securities);
	    build();
	  }

	  protected void build() {
	    this.render = new XYLineAndShapeRenderer(true, false);
	    this.render.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    this.render.setSeriesPaint(0, this.color);
	    this.render.setSeriesStroke(0, new BasicStroke(1.65F));
	  }

	  public void calculate() {
	    double[] inReal = this.controller.getDataset().getCloseArray();
	    int startIdx = 0;
	    int endIdx = inReal.length - 1;
	    MInteger outBegIdx = new MInteger();
	    MInteger outNbElement = new MInteger();
	    double[] inHigh = this.controller.getDataset().getHighArray();
	    double[] inLow = this.controller.getDataset().getLowArray();

	    double[] outReal = getOutputArray(inReal, this.controller.getTalibCore().aroonOscLookback(this.period));
	    this.controller.getTalibCore().aroonOsc(startIdx, endIdx, inHigh, inLow, this.period, outBegIdx, outNbElement, outReal);

	    if (outReal != null) {
	      TimeSeries timeseries = new TimeSeries(this.name, "", "", Minute.class);
	      int count2 = this.controller.getDataset().getDateArray().length;
	      int count = outReal.length;
	      for (int i = count; i > 0; i--) {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.controller.getDataset().getDateArray()[(count2 - 1)])), outReal[(i - 1)]);
	        count2--;
	      }
	      this.xydataset = new TimeSeriesCollection(timeseries);
	    }
	  }

	  public void show() {
	    this.render.setSeriesPaint(0, this.color);
	    calculate();
	    this.controller.getDataset().addListener(this);
	    this.controller.getContainer().addPlot(this.position, this.name, this.xydataset, null, this.render);
	  }

	  public void hide() {
	    this.controller.getDataset().removeListener(this);
	    this.controller.getContainer().removePlot(this.position, this.name);
	  }

	  public void chartChanged()
	  {
	    ((TimeSeriesCollection)this.xydataset).removeAllSeries();
	    calculate();
	    this.controller.getContainer().updateDataset(this.position, this.name, this.xydataset);
	  }

	  public Color getColor() {
	    return this.color;
	  }

	  public void setColor(Color color) {
	    this.color = color;
	  }

	  public int getPeriod() {
	    return this.period;
	  }

	  public void setPeriod(int period) {
	    this.period = period;
	  }
}
