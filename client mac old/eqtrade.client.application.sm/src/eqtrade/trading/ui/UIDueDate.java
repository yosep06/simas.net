package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.Utils;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.DueDateDef;
import eqtrade.trading.model.UserProfile;

public class UIDueDate extends UI{
	private JGrid table;
	private JSkinDlg frame;
	private JButton btnOk;
	
	public UIDueDate(String app) {
		super("Due Date", app);
		// TODO Auto-generated constructor stub
		setProperty(C_PROPERTY_NAME, TradingUI.UI_DUEDATE);
	}
	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.getInsidePanel().setBorder(new EmptyBorder(0, 1, 1, 1));
		frame.setContent(pnlContent);
		frame.pack();
		 frame.setLocation(((Rectangle) hSetting.get("size")).x, ((Rectangle)
				 hSetting.get("size")).y);
		frame.setSize(((Rectangle) hSetting.get("size")).width,
				((Rectangle) hSetting.get("size")).height);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
	}
	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}
	@Override
	protected void build() {
		btnOk = new JButton("OK");
		table = createTable(
				((IEQTradeApp)apps).getTradingEngine().getStore(
						TradingStore.DATA_DUEDATE), null,
				(Hashtable) hSetting.get("table"));
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getTable().addMouseListener(new MyCustomMouseAdapter());
		JPanel pnlTop = new JPanel();

		FormLayout l = new FormLayout(
				"2dlu,50px,2dlu,80px,2dlu,80px,2dlu,80px", "2dlu,pref,2dlu");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlTop);
		//b.add(btnRefresh, c.xy(4, 2));
		b.add(btnOk,c.xy(4, 2));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 0, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		pnlContent.add(pnlTop, BorderLayout.SOUTH);
		btnOk.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
			frame.dispose();			
			}
		});
		refresh();
		
	}

	@Override
	public void focus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
		.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", DueDateDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 350, 180));
		
	}
	/*@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}*/
	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
		
	}
	@Override
	public void show() {
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}
	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}
	private Thread thread;
	@Override
	public void show(Object param) {
		HashMap p = (HashMap) param;
		query((String) p.get("CLIENT"));
		show();
	}
	private void query(final String param) {
		if (thread != null) {
			thread.stop();
			thread = null;
		}
		thread = new Thread(new Runnable() {
			@Override
			public void run() {
				((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_DUEDATE).getDataVector()
						.clear();
				((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_DUEDATE).refresh();
				((IEQTradeApp) apps).getTradingEngine().refreshDueDate(param);
			}
		});
		thread.start();
	}
	
	@Override
	public void saveSetting() {
		hSetting.put("size",
				new Rectangle(frame.getX(), frame.getY(), frame.getWidth(),
						frame.getHeight()));
		hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
		
	}
	

}
