package eqtrade.trading.ui;

import java.awt.Component;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterColumn;

import eqtrade.trading.model.Order;

public class FilterTrade extends FilterOrder {
	// private HashMap mapFilter = new HashMap();

	public FilterTrade(Component parent) {
		super(parent);
		mapFilter.put("tradeno", new FilterColumn("tradeno", String.class,
				null, FilterColumn.C_EQUAL));
	}

	@Override
	public boolean filter(TableModel model, int row) {
		Column val = (Column) model.getValueAt(row, 0);
		Order dat = (Order) val.getSource();
		boolean isfilter = super.filter(model, row) && isTrade(dat)
				&& dat.getVolume().doubleValue() > 0;

		return isfilter;
	}
}