package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.Action;
import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.IEQTradeApp;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.OrderIDGenerator;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.Portfolio;
import eqtrade.trading.model.Stock;

public class UISplitOrder extends UI {
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
	JText fieldStock = null;
	JText fieldStockName;
	JText fieldClient = null;
	JNumber fieldNewPrice;
	JNumber fieldNewLot;
	JNumber fieldNewValue;
	JText fieldClientName;
	JNumber fieldTradingLimit;
	JNumber fieldStockLimit;
	JText fieldBoard = null;
	JText fieldStatus = null;
	JNumber fieldOldPrice;
	JNumber fieldOldLot;
	JNumber fieldOldValue;
	JNumber fieldOldShare;
	JNumber fieldDone;
	JNumber fieldDoneShare;
	JNumber fieldNewShare;
	JDropDown comboNewClient;
	JText fieldNewClientName;
	JText fieldIA;
	JButton btnSave;
	JButton btnCancel;
	JButton btnDelete;
	JButton btnSplit;
	JLabel labelTA;
	JLabel labelOverlimit;

	private JPanel pnlEntry;
	private String entryType = "BUY";
	private Action splitAction = null;

	private JGrid table;
	private GridModel model;
	private double balance;

	public UISplitOrder(String app) {
		super("Split Trade", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_SPLITORDER);
	}

	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
			}
		});
	}

	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	protected void build() {
		model = new GridModel(OrderDef.getHeader(), false);
		table = createTable(model, null, (Hashtable) hSetting.get("table"));
		table.setPreferredSize(new Dimension(300, 300));
		table.setColumnHide(OrderDef.columnhide3);
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getTable().getTableHeader().setReorderingAllowed(false);

		splitAction = apps.getAction().get(TradingAction.A_SHOWSPLITORDER);
		fieldTradingLimit = new JNumber(Double.class, 0, 0, true, false);
		fieldStockLimit = new JNumber(Double.class, 0, 0, true, false);
		fieldClient = new JText(false);
		fieldStock = new JText(false);
		fieldBoard = new JText(false);
		fieldStatus = new JText(false);
		fieldClientName = new JText(false);
		fieldStockName = new JText(false);
		fieldNewPrice = new JNumber(Double.class, 0, 0, true, false);
		fieldNewLot = new JNumber(Double.class, 0, 0, true);
		fieldNewValue = new JNumber(Double.class, 0, 0, true, false);
		fieldOldPrice = new JNumber(Double.class, 0, 0, true, false);
		fieldOldLot = new JNumber(Double.class, 0, 0, true, false);
		fieldOldValue = new JNumber(Double.class, 0, 0, true, false);
		fieldOldShare = new JNumber(Double.class, 0, 0, true, false);
		fieldDone = new JNumber(Double.class, 0, 0, true, false);
		fieldDoneShare = new JNumber(Double.class, 0, 0, true, false);
		fieldNewShare = new JNumber(Double.class, 0, 0, true);
		comboNewClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		fieldNewClientName = new JText(false);
		fieldIA = new JText(false);
		labelTA = new JLabel("Trading Limit");
		labelOverlimit = new JLabel("");
		labelOverlimit.setHorizontalAlignment(JLabel.CENTER);

		btnSave = new JButton("Save");
		btnCancel = new JButton("Cancel");
		btnSplit = new JButton("Split");
		btnDelete = new JButton("Delete");

		registerEvent(fieldNewPrice);
		registerEvent(fieldNewLot);
		registerEvent(fieldNewShare);
		registerEvent(comboNewClient.getTextEditor());
		registerEvent(btnSave);
		registerEvent(btnCancel);
		registerEvent(btnSplit);
		registerEvent(btnDelete);

		fieldNewLot.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				if (fieldNewLot.isEditable())
					calculate();
			}
		});

		fieldNewShare.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				if (fieldNewShare.isEditable())
					calculateLot();
			}
		});

		fieldNewPrice.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				calculate();
			}
		});

		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				send();
			}
		});

		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					Order order = (Order) model.getDataByIndex(i);
					balance = balance + order.getVolume().doubleValue();
					fieldDoneShare.setValue(new Double(balance));
					fieldDone.setValue(new Double(balance / 100));
					model.deleteRow(i);
					comboNewClient.getTextEditor().setText("");
					fieldNewShare.setValue(new Double(balance));
					fieldNewLot.setValue(new Double(balance / 100));
					calculate();
					if (order.getBoard().equals("NG")) {
						fieldNewShare.requestFocus();
					} else {
						fieldNewLot.requestFocus();
					}
				}
			}
		});
		btnSplit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (isValidEntry()) {
					int i = model.getIndexByField(new Object[] { comboNewClient.getText().toUpperCase().trim() },new int[] { Order.C_TRADINGID });
					if (i >= 0) {
						Utils.showMessage("this client already selected, please choose another client",form);
						comboNewClient.getTextEditor().requestFocus();
						return;
					}
					Account account = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByField(new Object[] { comboNewClient.getText().toUpperCase().trim() },new int[] { Account.C_TRADINGID });
					Order o = new Order();
					o.setId(OrderIDGenerator.gen(comboNewClient.getText()));
					o.setPrice((Double) fieldNewPrice.getValue());
					o.setLot((Double) fieldNewLot.getValue());
					o.setVolume((Double) fieldNewShare.getValue());
					o.setTradingId(comboNewClient.getText());
					o.setStatus(Order.C_SPLIT);
					o.setBOS(order.getBOS());
					o.setAccId(account.getAccId());
					Vector v = new Vector(1);
					v.addElement(OrderDef.createTableRow(o));
					model.addRow(v, false, false);
					balance = balance - ((Double) fieldNewShare.getValue()).doubleValue();
					fieldDoneShare.setValue(new Double(balance));
					fieldDone.setValue(new Double(balance / 100));
					comboNewClient.getTextEditor().setText("");
					fieldNewShare.setValue(new Double(balance));
					fieldNewLot.setValue(new Double(balance / 100));
					calculate();
					if (balance == 0) {
						btnSave.requestFocus();
					} else if (order.getBoard().equals("NG")) {
						fieldNewShare.requestFocus();
					} else {
						fieldNewLot.requestFocus();
					}
				}
			}
		});
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clearForm();
				hide();
			}
		});

		comboNewClient.getTextEditor().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				clientfocusLost(arg0);
			}

			public void focusGained(FocusEvent e) {
				try {
					if (((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getRowCount() == 1) {
						comboNewClient.setSelectedIndex(0);
					} else {
						comboNewClient.showPopup();
					}
				} catch (Exception ex) {
				}
			}
		});

		pnlEntry = new JPanel();
		FormLayout lay = new FormLayout("pref, 2dlu, 75px, 2dlu, pref, 2dlu, 75px, 2dlu, pref","pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref");
		PanelBuilder builder = new PanelBuilder(lay, pnlEntry);
		builder.setBorder(new EmptyBorder(4, 4, 4, 4));
		CellConstraints cc = new CellConstraints();
		builder.add(new JLabel("Board"), cc.xy(1, 3));
		builder.add(fieldBoard, cc.xy(3, 3));
		builder.add(new JLabel("Stock"), cc.xy(1, 5));
		builder.add(fieldStock, cc.xy(3, 5));
		builder.add(fieldStockName, cc.xyw(5, 5, 5));
		builder.add(new JLabel("Price"), cc.xy(1, 7));
		builder.add(fieldOldPrice, cc.xy(3, 7));
		builder.add(new JLabel("Quantity"), cc.xy(1, 9));
		builder.add(fieldOldLot, cc.xy(3, 9));
		builder.add(new JLabel("Lot(s)"), cc.xy(5, 9));
		builder.add(fieldOldShare, cc.xy(7, 9));
		builder.add(new JLabel("Share(s)"), cc.xy(9, 9));
		builder.add(new JLabel("Done"), cc.xy(1, 11));
		builder.add(fieldDone, cc.xy(3, 11));
		builder.add(new JLabel("Lot(s)"), cc.xy(5, 11));
		builder.add(fieldDoneShare, cc.xy(7, 11));
		builder.add(new JLabel("Share(s)"), cc.xy(9, 11));
		builder.add(new JLabel("Client"), cc.xy(1, 13));
		builder.add(fieldClient, cc.xy(3, 13));
		builder.add(fieldClientName, cc.xyw(5, 13, 5));
		builder.add(new JLabel("Trading Acc"), cc.xy(1, 15));
		builder.add(fieldIA, cc.xy(3, 15));
		builder.add(new JLabel("Status"), cc.xy(5, 15));
		builder.add(fieldStatus, cc.xy(7, 15));
		builder.add(new JLabel("Amount"), cc.xy(1, 17));
		builder.add(fieldOldValue, cc.xyw(3, 17, 7));
		builder.addSeparator("Split Values", cc.xyw(1, 19, 9));
		builder.add(new JLabel("Quantity"), cc.xy(1, 21));
		builder.add(fieldNewLot, cc.xy(3, 21));
		builder.add(new JLabel("Lot(s)"), cc.xy(5, 21));
		builder.add(fieldNewShare, cc.xy(7, 21));
		builder.add(new JLabel("Share(s)"), cc.xy(9, 21));
		builder.add(new JLabel("Client"), cc.xy(1, 23));
		builder.add(comboNewClient, cc.xy(3, 23));
		builder.add(fieldNewClientName, cc.xyw(5, 23, 5));
		builder.add(new JLabel("Price"), cc.xy(1, 25));
		builder.add(fieldNewPrice, cc.xy(3, 25));
		builder.add(new JLabel("Amount"), cc.xy(5, 25));
		builder.add(fieldNewValue, cc.xyw(7, 25, 3));
		builder.add(labelTA, cc.xy(1, 27));
		builder.add(fieldTradingLimit, cc.xyw(3, 27, 3));
		builder.add(fieldStockLimit, cc.xyw(3, 27, 3));
		builder.add(labelOverlimit, cc.xyw(7, 27, 3));

		JSkinPnl temp3 = new JSkinPnl();
		FormLayout lay3 = new FormLayout("pref, 2dlu, pref:grow, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref","pref");
		PanelBuilder builder3 = new PanelBuilder(lay3, temp3);
		builder3.add(btnSplit, cc.xy(5, 1));
		builder3.add(btnDelete, cc.xy(7, 1));
		builder3.add(btnSave, cc.xy(9, 1));
		builder3.add(btnCancel, cc.xy(11, 1));
		builder.add(temp3, cc.xywh(1, 29, 9, 1));

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(10, 10, 10, 10));
		pnlContent.add(pnlEntry, BorderLayout.CENTER);
		pnlContent.add(table, BorderLayout.EAST);
		clearForm();
		refresh();
	}

	void changeTL() {
		String acc = getAccountId(fieldClient.getText().toUpperCase().trim());
		if (!acc.equals("")) {
			Account ac = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByKey(new String[] { acc });
			if (ac != null) {
				fieldTradingLimit.setValue(ac.getCurrTL());
			}
		}
	}

	void clientfocusLost(FocusEvent e) {
		try {
			comboNewClient.getTextEditor().setText(comboNewClient.getText().toUpperCase());
			Account acc = null;
			int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getIndexByKey(new String[] { getAccountId(comboNewClient.getText()) });
			if (i >= 0) {
				acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByIndex(i);
				if (acc != null) {
					fieldNewClientName.setText(acc.getName());
					fieldTradingLimit.setValue(acc.getCurrTL());
				}
			} else {
				fieldNewClientName.setText("");
				fieldTradingLimit.setValue(null);
			}
			changeMaxLot();
			calculate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", OrderDef.getTableDef());
			hSetting.put("typeEntry", "BUY");
			hSetting.put("confirm", new Boolean(true));
		}
		entryType = (String) hSetting.get("typeEntry");
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 400, 300));
	}

	public void saveSetting() {
		hSetting.put(	"size",new Rectangle(form.getX(), form.getY(), form.getWidth(), form.getHeight()));
		hSetting.put("typeEntry", entryType);
		hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),hSetting);
	}

	public void refresh() {
		table.getViewport().setBackground(TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
	}

	protected void initUI() {
		super.initUI();
		form.setResizable(false);
		form.getBtnClose().setVisible(false);
		form.pack();
		form.getBtnMin().setVisible(false);
	}

	private Order order;

	public void show(Object param) {
		HashMap p = (HashMap) param;
		order = (Order) p.get("ORDER");
		entryType = order.getBOS().equals(Order.C_BUY) ? "BUY" : "SELL";
		form.setTitle("SPLIT Trade " + entryType);
		pnlEntry.setBackground(entryType.equals("BUY") ? Color.red.darker()	: Color.green.darker());
		fillForm();
		form.pack();
		show();
	}

	private void fillForm() {
		model.getDataVector().clear();
		model.refresh();
		fieldStock.setText(order.getStock());
		fieldClient.setText(order.getTradingId());
		fieldOldPrice.setValue(order.getPrice());
		fieldOldLot.setValue(order.getLot());
		fieldOldValue.setValue(new Double(order.getVolume().doubleValue() * order.getPrice().doubleValue()));
		fieldNewPrice.setValue(order.getPrice());
		fieldNewLot.setValue(order.getDoneLot());
		fieldBoard.setText(order.getBoard());
		fieldStatus.setText(order.getStatus());
		fieldOldShare.setValue(order.getVolume());
		fieldDone.setValue(order.getDoneLot());
		fieldDoneShare.setValue(order.getDoneVol());
		fieldIA.setText(order.getInvType());
		fieldNewShare.setValue(order.getDoneVol());
		if (entryType.equals("BUY")) {
			labelTA.setText("Trading Limit");
			fieldTradingLimit.setVisible(true);
			fieldStockLimit.setVisible(false);
		} else {
			labelTA.setText("Stock Balance");
			fieldTradingLimit.setVisible(false);
			fieldStockLimit.setVisible(true);
		}
		if (order.getBoard().equals("NG")) {
			fieldNewLot.setEditable(false);
			fieldNewLot.setEnabled(false);
			fieldNewShare.setEditable(true);
			fieldNewShare.setEnabled(true);
		} else {
			fieldNewLot.setEditable(true);
			fieldNewLot.setEnabled(true);
			fieldNewShare.setEditable(false);
			fieldNewShare.setEnabled(false);
		}
		clientfocusLost(null);
		Account acc = null;
		int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getIndexByKey(new String[] { getAccountId(fieldClient.getText()) });
		if (i >= 0) {
			acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByIndex(i);
			if (acc != null) {
				fieldClientName.setText(acc.getName());
				fieldTradingLimit.setValue(acc.getCurrTL());
			}
		} else {
			fieldClientName.setText("");
			fieldTradingLimit.setValue(null);
		}
		changeMaxLot();
		i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getIndexByKey(new String[] { fieldStock.getText() });
		if (i >= 0) {
			Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getDataByIndex(i);
			if (stock != null) {
				fieldStockName.setText(stock.getName());
			}
		} else {
			fieldStockName.setText("");
		}
		calculate();
		balance = order.getDoneVol().doubleValue();
		fieldNewLot.requestFocus();
	}

	void changeMaxLot() {
		try {
			int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).getIndexByKey(new String[] {getAccountId(comboNewClient.getText()),fieldStock.getText() });
			if (i >= 0) {
				Portfolio pf = (Portfolio) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).getDataByIndex(i);
				if (pf != null) {
					double lotsize = Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));
					int lt = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getIndexByKey(new String[] { fieldStock.getText() });
					if (lt >= 0) {
						Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getDataByIndex(lt);
						if (stock != null) {
							lotsize = (stock.getLotSize().doubleValue());
						}
					}
					fieldStockLimit.setValue(new Double(pf.getAvailableVolume().doubleValue() / lotsize));
				}
			} else {
				fieldStockLimit.setValue(new Double(0));
			}
		} catch (Exception ex) {
		}
	}

	void calculateLot() {
		try {
			double lotSize = 100;
			double share = ((Double) fieldNewShare.getValue()).doubleValue();
			int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getIndexByKey(new String[] { fieldStock.getText() });
			if (i >= 0) {
				Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getDataByIndex(i);
				if (stock != null) {
					lotSize = (stock.getLotSize().doubleValue());
				}
			}
			fieldNewLot.setValue(new Double(share / lotSize));
		} catch (Exception ex) {
		}
		calculate();
	}

	void calculate() {
		double lotSize = -1, price = 0, volume = 0, lot = 0;
		String client = "", newClient = "";
		try {
			lot = ((Double) fieldNewLot.getValue()).doubleValue();
			price = ((Double) fieldNewPrice.getValue()).doubleValue();
			client = fieldClient.getText();
			newClient = comboNewClient.getTextEditor().getText();
			int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getIndexByKey(new String[] {fieldStock.getText()});
			if (i >= 0) {
				Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getDataByIndex(i);
				if (stock != null) {
					lotSize = (stock.getLotSize().doubleValue());
				}
			}
			if (lotSize != -1) {
				volume = lotSize * lot;
				fieldNewValue.setValue(new Double(volume * price));
				fieldNewShare.setValue(new Double(volume));
			} else {
				fieldNewValue.setValue(null);
				fieldNewShare.setValue(null);
			}
		} catch (Exception ex) {
			fieldNewValue.setValue(null);
			fieldNewShare.setValue(null);
		}
		try {
			if (entryType.equals("BUY")) {
				double limit = ((Double) fieldTradingLimit.getValue()).doubleValue();
				if (client.equals(newClient)) {
					if ((volume * price - ((Double) fieldOldValue.getValue()).doubleValue()) > limit)
						labelOverlimit.setText("OVER LIMIT");
					else
						labelOverlimit.setText("");
				} else {
					if (volume * price > limit)
						labelOverlimit.setText("OVER LIMIT");
					else
						labelOverlimit.setText("");
				}
			} else {
				double limit = ((Double) fieldStockLimit.getValue()).doubleValue();
				if (!client.equals(newClient)) {
					if (lot > limit)
						labelOverlimit.setText("SHORT SELL");
					else
						labelOverlimit.setText("");
				} else {
					labelOverlimit.setText("");
				}
			}
		} catch (Exception ex) {
			labelOverlimit.setText("");
		}
	}

	private boolean isValidEntry() {
		order = (Order) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER).getDataByKey(new String[] { order.getId(), "0" });
		if (order == null) {
			Utils.showMessage("invalid order, please select order first", form);
			hide();
			return false;
		}
		if (order.getBoard().equals("NG")) {
			try {
				fieldNewShare.commitEdit();
				calculateLot();
			} catch (Exception ex) {
			}
		} else {
			try {
				fieldNewLot.commitEdit();
				calculate();
			} catch (Exception ex) {
			}
		}
		Double newShare = (Double) fieldNewShare.getValue();
		if (newShare != null) {
			if (newShare.doubleValue() <= 0) {
				Utils.showMessage("split share must be greater then zero", form);
				fieldNewLot.requestFocus();
				return false;
			}
		} else {
			Utils.showMessage("please enter valid split share", form);
			fieldNewLot.requestFocus();
			return false;
		}
		int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getIndexByKey(new String[] { getAccountId(comboNewClient.getText()) });
		if (i < 0) {
			Utils.showMessage("please enter valid Client", form);
			comboNewClient.getTextEditor().requestFocus();
			return false;
		} else if (order.getTradingId().equals(comboNewClient.getText())) {
			Utils.showMessage("please enter valid Client", form);
			comboNewClient.getTextEditor().requestFocus();
			return false;
		}
		if (balance < newShare.doubleValue()) {
			Utils.showMessage("split quantity cannot be greater than balance",form);
			fieldNewLot.requestFocus();
			return false;
		}
		return true;
	}

	void clearForm() {
		try {
			fieldClient.setText("");
			fieldNewLot.setValue(null);
			fieldNewPrice.setValue(null);
			fieldStock.setText("");
			fieldOldValue.setValue(null);
			fieldTradingLimit.setValue(null);
			fieldBoard.setText("");
			fieldStatus.setText("");
			fieldIA.setText("");
			fieldOldShare.setValue(null);
			fieldNewClientName.setText("");
			comboNewClient.getTextEditor().setText("");
			fieldNewShare.setValue(null);
		} catch (Exception ex) {
			
		}
	}

	boolean _send = false;

	private void send() {
		try {
			if (!_send) {
				_send = true;
				if (splitAction == null || !splitAction.isEnabled()) {
					Utils.showMessage("cannot split order, please reconnect..",form);
					_send = false;
				} else if (model.getRowCount() > 0) {
					int nconfirm = Utils.C_YES_DLG;
					nconfirm = Utils.showConfirmDialog(form, "Split Order","are you sure want to split this order?\n",Utils.C_YES_DLG);
					if (nconfirm == Utils.C_YES_DLG) {
						splitOrder();
					}
				} else {
					Utils.showMessage("please fullfill the destination first",form);
				}
				_send = false;
			}
		} catch (Exception ex) {
			//log.error(Utils.logException(ex));
			_send = false;
		}
	}

	void splitOrder() {
		order = (Order) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDER).getDataByKey(new String[] { order.getId(), "0" });
		if (order == null) {
			Utils.showMessage("invalid order, please select order first", form);
		} else {
		//	log.info("user aggree to split order: " + order.getId());
			// do request split here
			new Thread(new Runnable() {
				public void run() {
					try {
						btnSplit.setEnabled(false);
						btnSave.setEnabled(false);
						btnDelete.setEnabled(false);
						btnCancel.setEnabled(false);

						JSONObject param = new JSONObject();
						param.put("porderidfrom", order.getId());
						JSONArray arrayId = new JSONArray();
						JSONArray accid = new JSONArray();
						JSONArray volume = new JSONArray();
						JSONArray lot = new JSONArray();
						int total = model.getRowCount();
						for (int i = 0; i < total; i++) {
							Order order = (Order) model.getDataByIndex(i);
							arrayId.add(order.getId());
							accid.add(order.getAccId());
							volume.add(order.getVolume());
							lot.add(order.getLot());
						}
						
						param.put("porderidto", arrayId);
						param.put("paccidto", accid);
						param.put("pvolumeto", volume);
						param.put("plotto", lot);
						param.put("psplitby", ((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase());
						param.put("psplitdate",dateFormat.format(OrderIDGenerator.getTime()));
						param.put("psplitterminal",OrderIDGenerator.getTerminal());
						param.put("psplitcounter",new Double(model.getRowCount()));
						String result = ((IEQTradeApp) apps).getTradingEngine().splitOrder(param.toString());
						JSONObject obj = (JSONObject) JSONValue.parse(result);
						btnSplit.setEnabled(true);
						btnSave.setEnabled(true);
						btnDelete.setEnabled(true);
						btnCancel.setEnabled(true);
						
						if (obj.get("psukses").toString().equals("1")) {
							clearForm();
							hide();
						} else {
							Utils.showMessage(obj.get("pdesc").toString(), form);
						}
					} catch (Exception ex) {
						ex.printStackTrace();
						Utils.showMessage("internal error", form);
					}
				}
			}).start();
		}
	}

	void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),"enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0), "priceAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0), "qtyAction");
		
		comp.getActionMap().put("priceAction",new AbstractAction("priceAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				fieldNewPrice.requestFocus();
			}
		});
		comp.getActionMap().put("qtyAction", new AbstractAction("qtyAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				fieldNewLot.requestFocus();
			}
		});
		comp.getActionMap().put("escapeAction",new AbstractAction("escapeAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				btnCancel.doClick();
			}
		});
		comp.getActionMap().put("enterAction",new AbstractAction("enterAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				if (evt.getSource() instanceof JButton) {
					((JButton) evt.getSource()).doClick();
				} else {
					if (evt.getSource() instanceof JNumber) {
						try {
							((JNumber) evt.getSource()).commitEdit();
						} catch (Exception ex) {
						}
						;
					}
					btnSplit.doClick();
				}
			}
		});
	}

	String getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByField(new String[] { alias },new int[] { Account.C_TRADINGID });
		return (acc != null ? acc.getAccId() : "");
	}

}
