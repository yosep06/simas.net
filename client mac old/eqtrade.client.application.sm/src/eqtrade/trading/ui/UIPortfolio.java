package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;
import com.vollux.ui.JTextLabel;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.IMessage;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockSummary;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.Portfolio;
import eqtrade.trading.model.PortfolioDef;
import eqtrade.trading.model.UserProfile;

public class UIPortfolio extends UI implements IMessage{
	private JGrid table;
	private JDropDown comboClient;
	// private JText fieldName;
	private JDropDown fieldName;
	private JText fieldOffice;
	private JText fieldSales;
	private JText fieldCategory;
	private JText fieldPhone;
	private JText fieldHP;
	private JText fieldEmail;

	private JText fieldSingleID;
	private JText fieldVirtualAcc;
	private JText fieldIA;

	private JNumber fieldCreditLimit;
	private JNumber fieldCurrentRatio;
	private JNumber fieldTradingLimit;

	private JNumber fieldDeposit;
	private JNumber fieldBid;
	private JNumber fieldStockValue;
	private JNumber fieldBuy;
	private JNumber fieldOffer;
	private JNumber fieldMarketValue;
	private JNumber fieldSell;
	private JNumber fieldLQValue;
	private JNumber fieldUnreliazedValue;
	private JNumber fieldNetAC;

	private JNumber fieldTopUp;
	private JNumber fieldForceSell;
	private JNumber fieldWithdraw;

	private JNumber tStockV;
	private JNumber tUnrealized;
	private JNumber tMarketV;

	private JTabbedPane tab;

	private JButton btnRefresh;
	private JButton btnDuoDate;
	private FilterPortfolio pfFilter;
	private String client = "";
	private OrderPanel orderPanel;
	private Vector<Portfolio> vports = new Vector<Portfolio>();
	public UIPortfolio(String app) {
		super("Portfolio", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_PF);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}
	@Override
	public void close() {
		DateNow = false;
		super.close();
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
		//log.info("close...");
	}

	@Override
	public void hide() {
		DateNow = false;
		super.hide();
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
		//log.info("close...");

	}

	public void show(){
		DateNow = true;
		updatePFLastPrice();
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
		.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
		super.show();
		//#Valdhy20141111
//		LoadDefault();
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	void fillInfo() {
		Account account = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { client },
						new int[] { Account.C_TRADINGID });
		// fieldName.setText(account != null ? account.getName() : "");
		if (account != null)
			fieldName.setSelectedItem(account.getName());

		fieldOffice.setText(account != null ? "" : "");
		fieldSales.setText(account != null ? account.getSalesId() : "");
		fieldCategory.setText(account != null ? account.getAccType() : "");
		fieldPhone.setText(account != null ? account.getPhone() : "");
		fieldHP.setText(account != null ? account.getHandphone() : "");
		fieldEmail.setText(account != null ? account.getEmail() : "");

		fieldSingleID.setText(account != null ? account.getSingleID() : "");
		if (account.getVirtualAccount().isEmpty())
			fieldVirtualAcc.setText(account != null ? ""  : "");
		else
			fieldVirtualAcc.setText(account != null ? account.getVirtualAccount().substring(0, 4)+" "+account.getVirtualAccount().substring(4, 8) + " "+account.getVirtualAccount().substring(8, 14)+" "+account.getVirtualAccount().substring(14, 16)  : "");
		System.err.println(" -- "+ account.getInvestorAccount().length());
		if (account.getInvestorAccount().isEmpty())
			fieldIA.setText(account != null ? "" : "");
		else
			fieldIA.setText(account != null ? account.getInvestorAccount().substring(0, 3)+" "+account.getInvestorAccount().substring(3, 5)+" "+account.getInvestorAccount().substring(5, 10)+" "+account.getInvestorAccount().substring(10, 13) : "");

		fieldCreditLimit.setValue(account != null ? account.getCreditLimit()
				: null);
		fieldCurrentRatio.setValue(account != null ? account.getCurrentRatio()
				: null);
		fieldTradingLimit
				.setValue(account != null ? account.getCurrTL() : null);
		fieldDeposit.setValue(account != null ? account.getDeposit() : null);
		fieldWithdraw.setValue(account != null ? account.getWithdraw() : null);
		fieldBid.setValue(account != null ? account.getBid() : null);
//Perbaikan market price yang mengikuti close price.
		//		fieldStockValue
//				.setValue(account != null ? account.getStockVal() : null);
		fieldBuy.setValue(account != null ? account.getBuy() : null);
		fieldOffer.setValue(account != null ? account.getOffer() : null);
//		fieldMarketValue.setValue(account != null ? account.getMarketVal()
//				: null);
		fieldSell.setValue(account != null ? account.getSell() : null);
		fieldLQValue.setValue(account != null ? account.getLQValue() : null);
		fieldNetAC.setValue(account != null ? account.getNetAC() : null);
		fieldTopUp.setValue(account != null ? account.getTopup() : null);
		fieldForceSell
				.setValue(account != null ? account.getForceSell() : null);

		Double unrealized =(double) 0;
		if(account!= null){
			//log.info("market val "+account.getMarketVal().doubleValue()+":"+account.getStockVal().doubleValue());

			unrealized = account.getMarketVal().doubleValue() - account.getStockVal().doubleValue();
		}

//		fieldUnreliazedValue.setValue(unrealized);

	}

	@Override
	protected void build() {
		createPopup();
		orderPanel = new OrderPanel(apps, form, hSetting);
		orderPanel.build();
		orderPanel.comboClient.setEnabled(false);
		((FilterColumn) orderPanel.filterOrder.getFilteredData("account"))
				.setField("");
		orderPanel.filterOrder.fireFilterChanged();

		tab = new JTabbedPane();
		pfFilter = new FilterPortfolio(form);
		((FilterColumn) pfFilter.getFilteredData("account")).setField("");
		comboClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		// fieldName = new JText(true);
		fieldName = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT), Account.C_NAME, true);
		fieldOffice = new JText(false);
		fieldSales = new JText(false);
		fieldCategory = new JText(false);
		fieldPhone = new JText(false);
		fieldHP = new JText(false);
		fieldEmail = new JText(false);

		fieldSingleID = new JText(false);
		fieldVirtualAcc = new JText(false);
		fieldIA = new JText(false);

		fieldCreditLimit = new JNumber(Double.class, 0, 0, false, false);
		fieldCurrentRatio = new JNumber(Double.class, 0, 2, false, false);
		fieldTradingLimit = new JNumber(Double.class, 0, 0, false, false);
		fieldDeposit = new JNumber(Double.class, 0, 0, false, false);
		fieldWithdraw = new JNumber(Double.class, 0, 0, false, false);
		fieldBid = new JNumber(Double.class, 0, 0, false, false);
		fieldStockValue = new JNumber(Double.class, 0, 0, false, false);
		fieldBuy = new JNumber(Double.class, 0, 0, false, false);
		fieldOffer = new JNumber(Double.class, 0, 0, false, false);
		fieldMarketValue = new JNumber(Double.class, 0, 0, false, false);
		fieldSell = new JNumber(Double.class, 0, 0, false, false);
		fieldLQValue = new JNumber(Double.class, 0, 0, false, false);
		fieldNetAC = new JNumber(Double.class, 0, 0, false, false);
		fieldTopUp = new JNumber(Double.class, 0, 0, false, false);
		fieldForceSell = new JNumber(Double.class, 0, 0, false, false);
		fieldUnreliazedValue = new JNumber(Double.class, 0, 0, false, false);
		btnRefresh = new JButton("Refresh");
		btnDuoDate = new JButton("Duedate");
		btnRefresh.setMnemonic('F');

		fieldBid.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			}
		});

		fieldBid.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				tab.setSelectedIndex(2);
			}
		});
		fieldOffer.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				tab.setSelectedIndex(2);
			}
		});
		fieldMarketValue.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				tab.setSelectedIndex(1);
			}
		});

		fieldStockValue.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				tab.setSelectedIndex(1);
			}
		});

		fieldLQValue.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				tab.setSelectedIndex(1);
			}
		});
		fieldDeposit.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				btnRefresh.requestFocus();
				comboClient.getTextEditor().requestFocus();
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						HashMap param = new HashMap();
						param.put("CLIENT", client);
						apps.getUI().showUI(TradingUI.UI_CASHCOLL, param);
					}
				});
			}
		});

		/*fieldBuy.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				//btnRefresh.requestFocus();
				//comboClient.getTextEditor().requestFocus();
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						HashMap param = new HashMap();
						param.put("CLIENT", client);
						param.put("BUYSELL", "B");
						apps.getUI().showUI(TradingUI.UI_BUYSELLDETAIL, param);
					}
				});
			}
		});

		fieldSell.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				//btnRefresh.requestFocus();
				//comboClient.getTextEditor().requestFocus();
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						HashMap param = new HashMap();
						param.put("CLIENT", client);
						param.put("BUYSELL", "S");
						apps.getUI().showUI(TradingUI.UI_BUYSELLDETAIL, param);
					}
				});
			}
		});*/
		//Tambahan Due date
		btnDuoDate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						HashMap param = new HashMap();
						System.out.println(">>>>"+client);
						param.put("CLIENT", client);
						apps.getUI().showUI(TradingUI.UI_DUEDATE,param);
					}
				});

			}
		})	;

		registerEvent(comboClient.getTextEditor());
		registerEventCombo(comboClient);

		registerEvent(fieldName.getTextEditor());
		registerEventCombo(fieldName);

		registerEvent(btnRefresh);

		((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.addTableModelListener(new TableModelListener() {
					@Override
					public void tableChanged(TableModelEvent e) {
						fillInfo();
					}
				});



		comboClient.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				comboClient.showPopup();
			}

			public void focusLost(FocusEvent e) {
				String newclient = comboClient.getText().trim().toUpperCase();
				// if (!newclient.equals(client)) {
				client = newclient;
				((FilterColumn) pfFilter.getFilteredData("account"))
						.setField(comboClient.getText().trim().toUpperCase()
								.equals("") ? "" : comboClient.getText().trim()
								.toUpperCase());
				pfFilter.fireFilterChanged();
				orderPanel.comboClient.getTextEditor().setText(
						comboClient.getText().trim().toUpperCase());
				((FilterColumn) orderPanel.filterOrder
						.getFilteredData("account")).setField(comboClient
						.getText().trim().equals("") ? "" : comboClient
						.getText().trim().toUpperCase());
				orderPanel.filterOrder.fireFilterChanged();
				fillInfo();
				checkExpKtp();
//				updatePortfolio();
				updatePFLastPrice();
				// }
			}
		});

		fieldName.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				fieldName.showPopup();
			}

			@Override
			public void focusLost(FocusEvent e) {
				String newclient = comboClient.getText().trim().toUpperCase();
				// if (!newclient.equals(client)) {
				client = newclient;
				((FilterColumn) pfFilter.getFilteredData("account"))
						.setField(comboClient.getText().trim().toUpperCase()
								.equals("") ? "" : comboClient.getText().trim()
								.toUpperCase());
				pfFilter.fireFilterChanged();
				orderPanel.comboClient.getTextEditor().setText(
						comboClient.getText().trim().toUpperCase());
				((FilterColumn) orderPanel.filterOrder
						.getFilteredData("account")).setField(comboClient
						.getText().trim().equals("") ? "" : comboClient
						.getText().trim().toUpperCase());
				orderPanel.filterOrder.fireFilterChanged();
				fillInfo();
			}

		});

		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!comboClient.getText().trim().equals("")) {

					/*if (isSuperUser()) {
						((IEQTradeApp) apps).getTradingEngine().refreshData(
								comboClient.getText().trim().toUpperCase());
					} else {*/
					setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshAccount(
								comboClient.getText().trim().equals("") ? "%"
										: comboClient.getText().trim()
												.toUpperCase());
						((IEQTradeApp) apps)
								.getTradingEngine()
								.refreshPortfolio(
										comboClient.getText().trim().equals("") ? "%"
												: comboClient.getText().trim()
														.toUpperCase(), "%");
					updatePortfolio();
					//}
				}
			}
		});

		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_PORTFOLIO), pfFilter,
				(Hashtable) hSetting.get("table"));
		table.getTable().add(popupMenu);
		table.getTable().addMouseListener(new MyMouseAdapter());
		table.addMouseListener(new MyCustomMouseAdapter());
		table.setColumnHide(PortfolioDef.columnhidedetail);
		table.getTable().getModel()
		.addTableModelListener(new RowChanged());
		registerEvent(table);
		registerEvent(table.getTable());
		//count
		tStockV= new JNumber(Double.class, 0, 0, false, false);
		tUnrealized = new JNumber(Double.class, 0, 0, false, false);
		tMarketV = new JNumber(Double.class, 0, 0, false, false);

		JPanel infoPanel = new JPanel();
		FormLayout infLayout = new FormLayout(
				"pref,3dlu,110px,10dlu,pref,3dlu,110px, 10dlu, pref, 3dlu, 110px, 10dlu, pref","pref");
		PanelBuilder infBuilder = new PanelBuilder(infLayout, infoPanel);
		infBuilder.setBorder(new EmptyBorder(4, 4, 4, 4));
		CellConstraints cc = new CellConstraints();
		infBuilder.add(new JLabel("Stock Val"), cc.xy(1, 1));
		infBuilder.add(tStockV, cc.xy(3, 1));
		infBuilder.add(new JLabel("Market Val"), cc.xy(5, 1));
		infBuilder.add(tMarketV, cc.xy(7, 1));
		infBuilder.add(new JLabel("Unrealized"), cc.xy(9, 1));
		infBuilder.add(tUnrealized, cc.xy(11, 1));
		JScrollPane b = new JScrollPane(infoPanel);
		b.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		b.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		JSkinPnl stockList = new JSkinPnl();
		stockList.add(b,BorderLayout.SOUTH);
		stockList.add(table, BorderLayout.CENTER);

		JPanel editorPanel = new JPanel();
		FormLayout layout = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,130px, 2dlu, pref, 2dlu, 130px , 2dlu, pref, 2dlu, 130px",
				"pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref");
		PanelBuilder builder = new PanelBuilder(layout, editorPanel);
		builder.setBorder(new EmptyBorder(4, 4, 4, 4));
		//CellConstraints cc = new CellConstraints();
		builder.addSeparator("Client Info", cc.xyw(1, 1, 11));
		builder.add(btnRefresh, cc.xy(15, 1));
		builder.add(btnDuoDate,cc.xyw(12, 1,2));
		builder.add(new JTextLabel("Client", 'C', comboClient.getTextEditor()),
				cc.xy(1, 3));
		builder.add(comboClient, cc.xy(3, 3));
		builder.add(fieldName, cc.xyw(5, 3, 3));
		builder.add(new JLabel("Office"), cc.xy(9, 3));
		builder.add(fieldOffice, cc.xy(11, 3));
		builder.add(new JLabel("Sales"), cc.xy(13, 3));
		builder.add(fieldSales, cc.xy(15, 3));
		builder.add(new JLabel("Category"), cc.xy(1, 5));
		builder.add(fieldCategory, cc.xy(3, 5));
		builder.add(new JLabel("Phone"), cc.xy(5, 5));
		builder.add(fieldPhone, cc.xy(7, 5));
		builder.add(new JLabel("HP"), cc.xy(9, 5));
		builder.add(fieldHP, cc.xy(11, 5));
		builder.add(new JLabel("Email"), cc.xy(13, 5));
		builder.add(fieldEmail, cc.xy(15, 5));

		builder.add(new JLabel("Single ID"), cc.xy(5, 7));
		builder.add(fieldSingleID, cc.xy(7, 7));
		builder.add(new JLabel("Virtual A/C"), cc.xy(9, 7));
		builder.add(fieldVirtualAcc, cc.xy(11, 7));
		builder.add(new JLabel("Investor A/C"), cc.xy(13, 7));
		builder.add(fieldIA, cc.xy(15, 7));


		builder.addSeparator("Credit Info", cc.xyw(1, 9, 15));
		builder.add(new JLabel("Credit Limit"), cc.xy(5, 11));
		builder.add(fieldCreditLimit, cc.xy(7, 11));
		builder.add(new JLabel("Current Ratio"), cc.xy(9, 11));
		builder.add(fieldCurrentRatio, cc.xy(11, 11));
		builder.add(new JLabel("Trading Limit"), cc.xy(13, 11));
		builder.add(fieldTradingLimit, cc.xy(15, 11));
		builder.addSeparator("Limit Info", cc.xyw(1, 13, 15));
		builder.add(new JLabel("Deposit"), cc.xy(5, 15));
		builder.add(fieldDeposit, cc.xy(7, 15));
		builder.add(new JLabel("Bid"), cc.xy(9, 15));
		builder.add(fieldBid, cc.xy(11, 15));
		builder.add(new JLabel("Stock Value"), cc.xy(13, 15));
		builder.add(fieldStockValue, cc.xy(15, 15));

		builder.add(new JLabel("Cash Withdraw"), cc.xy(5, 17));
		builder.add(fieldWithdraw, cc.xy(7, 17));
		builder.add(new JLabel("Offer"), cc.xy(9, 17));
		builder.add(fieldOffer, cc.xy(11, 17));

		builder.add(new JLabel("Net A/C"), cc.xy(9, 19));
		builder.add(fieldNetAC, cc.xy(11, 19));

		builder.add(new JLabel("Market Value"), cc.xy(13, 17));
		builder.add(fieldMarketValue, cc.xy(15, 17));

		builder.add(new JLabel("Buy"), cc.xy(5, 19));
		builder.add(fieldBuy, cc.xy(7, 19));
		builder.add(new JLabel("LQ Value"), cc.xy(13, 19));
		builder.add(fieldLQValue, cc.xy(15, 19));
		builder.add(new JLabel("Sell"), cc.xy(5, 21));
		builder.add(fieldSell, cc.xy(7, 21));

		builder.add(new JLabel("Unrealized Value"), cc.xy(13, 21));
		builder.add(fieldUnreliazedValue, cc.xy(15, 21));

		builder.addSeparator("", cc.xyw(1, 23, 15));
		builder.add(new JLabel("Top Up"), cc.xy(13, 25));
		builder.add(fieldTopUp, cc.xy(15, 25));
		builder.add(new JLabel("Force Sell"), cc.xy(13, 27));
		builder.add(fieldForceSell, cc.xy(15, 27));

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.setOpaque(true);
		tab.addTab("Portfolio", editorPanel);
		tab.addTab("Stock List", stockList);
		tab.addTab("Order", orderPanel);
		pnlContent.add(tab, BorderLayout.CENTER);
		refresh();
		startTimer();

		initData();

	}
	private void updatePFLastPrice() {
		int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO)
				.getRowCount();
		String newclient = comboClient.getText();
		vports.clear();
		for (int x = 0; x < i; x++) {
			Portfolio p = (Portfolio) ((IEQTradeApp) apps).getTradingEngine().getStore(
					TradingStore.DATA_PORTFOLIO).getDataByIndex(x);
			if (p != null && p.getAccountId().equals(newclient.trim().toUpperCase())) {
				int t = (int)((IEQTradeApp)apps).getFeedEngine().getStore(FeedStore.DATA_STOCK)
						.getIndexByKey(new Object[]{p.getStock()});
				if (t>0) {

					Stock st = (Stock)((IEQTradeApp)apps).getFeedEngine().getStore(FeedStore.DATA_STOCK)
							.getDataByIndex(t);//.getDataByKey(new Object[]{p.getStock()});
					int a = (int) ((IEQTradeApp) apps).getFeedEngine().getStore(
							FeedStore.DATA_STOCKSUMMARY).getIndexByKey(
							new Object[] { p.getStock(), st.getStockType().equalsIgnoreCase("RGHI")?"TN":"RG" });
					if (a > 0) {
						StockSummary ss = (StockSummary) ((IEQTradeApp) apps).getFeedEngine().getStore(
								FeedStore.DATA_STOCKSUMMARY).getDataByIndex(a);
	//							getDataByKey(
	//							new Object[] { p.getStock(), st.getStockType().equalsIgnoreCase("RGHI")?"TN":"RG" });
	//									new Object[] { p.getStock(), });// yosep Rigth Portfolio

						vports.add(p);
						if (ss != null) {
							p.setLastPrice(ss.getClosing());
						}
					}

				}
			}
		}
//		System.out.println(vports.size()+ " portfolio size");
		((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).refresh();
	}
	protected void updatePortfolio() {
		//log.info("updat_portfolio_stock");
		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);

		String newclient = comboClient.getText();
		if (newclient != null && !newclient.isEmpty()) {
			newclient = newclient.trim().toUpperCase();
			vports.clear();
			vports = getPortfolioByClient(newclient);
			for (Portfolio pp : vports) {
				StockSummary ss = (StockSummary) ((IEQTradeApp) apps)
						.getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY)
						.getDataByKey(new Object[] { pp.getStock(), "RG" });
				if(ss != null)
				pp.setLastPrice(ss.getClosing());
			}

			((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
					.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
		}
		//log.info("updat_portfolio_stock_done");

	}

	private Vector<Portfolio> getPortfolioByClient(String newclient) {
		int i = ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_PORTFOLIO).getRowCount();
		Vector<Portfolio> vports = new Vector<Portfolio>();
		for (int x = 0; x < i; x++) {
			Portfolio p = (Portfolio) ((IEQTradeApp) apps).getTradingEngine()
					.getStore(TradingStore.DATA_PORTFOLIO).getDataByIndex(x);
			if (p.getTradingId().equals(newclient)) {
				vports.add(p);
			}

		}
		return vports;
	}

	private void initData() {
		// TODO Auto-generated method stub
		if (comboClient.getModel() != null
				&& comboClient.getModel().getSize() > 0) {
			comboClient.setSelectedIndex(0);
			String newclient = comboClient.getText().trim().toUpperCase();
			// if (!newclient.equals(client)) {
			client = newclient;
			((FilterColumn) pfFilter.getFilteredData("account"))
					.setField(comboClient.getText().trim().toUpperCase()
							.equals("") ? "" : comboClient.getText().trim()
							.toUpperCase());
			pfFilter.fireFilterChanged();
			orderPanel.comboClient.getTextEditor().setText(
					comboClient.getText().trim().toUpperCase());
			((FilterColumn) orderPanel.filterOrder.getFilteredData("account"))
					.setField(comboClient.getText().trim().equals("") ? ""
							: comboClient.getText().trim().toUpperCase());
			orderPanel.filterOrder.fireFilterChanged();
			fillInfo();
			// }

		}
	}

	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", PortfolioDef.getTableDetailDef());
			hSetting.put("tableOrder", OrderDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 850, 350));
	}

	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		hSetting.put("tableOrder", orderPanel.tableOrder.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
		orderPanel.refresh();
	}
	protected class RowChanged implements TableModelListener {

		public void tableChanged(final TableModelEvent e) {
			double count = 0, tlot = 0, tvalue = 0,tmarket = 0;
			int size = table.getTable().getRowCount();

			for (int i = 0; i < size; i++) {
				Portfolio order = (Portfolio) table.getDataByIndex(i);
				count++;
				tlot = tlot + order.getStockVal().doubleValue();
				tvalue = tvalue + order.getPLValue().doubleValue();
				tmarket = tmarket+ order.getValueLastPrice();

			}
			tStockV.setValue(new Double(tlot));
			tUnrealized.setValue(new Double(tvalue));
			tMarketV.setValue(new Double(tmarket));
			//penambahan dr rmi
			//yosep 27112014
			if (comboClient.getTextEditor().getText().equals("")) {
				fieldStockValue.setValue(0);
				fieldUnreliazedValue.setValue(0);
				fieldMarketValue.setValue(0);
			}else{
				fieldStockValue.setValue(new Double(tlot));
				fieldUnreliazedValue.setValue(new Double(tvalue));
				fieldMarketValue.setValue(new Double(tmarket));
			}

			if (e.getType() == TableModelEvent.INSERT) {
				Rectangle r = table.getTable().getCellRect(size - 1, 0,
						true);
				table.scrollRectToVisible(r);
			}

		}
	}
	private void registerEventCombo(JDropDown comp) {
		comp.getEditor().getEditorComponent()
				.setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(
				JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap()
				.put("tabAction", new AbstractAction("tabAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(comboClient.getTextEditor())) {
							comboClient.setSelectedIndex(comboClient
									.getSelectedIndex());
							fieldName.setSelectedIndex(comboClient
									.getSelectedIndex());
							comboClient.hidePopup();
							btnRefresh.requestFocus();
						} else if (e.getSource().equals(
								fieldName.getTextEditor())) {
							fieldName.setSelectedIndex(fieldName
									.getSelectedIndex());
							comboClient.setSelectedIndex(fieldName
									.getSelectedIndex());
							fieldName.hidePopup();
							btnRefresh.requestFocus();
						}
					}

				});
	}

	void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "clientAction");
		comp.getActionMap().put("clientAction",
				new AbstractAction("clientAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						comboClient.getTextEditor().requestFocus();
					}
				});
		comp.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						form.setVisible(false);
					}
				});
		comp.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton) {
							((JButton) evt.getSource()).doClick();
						} else if (evt.getSource().equals(
								comboClient.getTextEditor())) {
							comboClient.setSelectedIndex(comboClient
									.getSelectedIndex());
							comboClient.hidePopup();
							btnRefresh.requestFocus();
						} else if (evt.getSource().equals(
								fieldName.getTextEditor())) {
							fieldName.setSelectedIndex(fieldName
									.getSelectedIndex());
							fieldName.hidePopup();
							comboClient.setSelectedIndex(fieldName
									.getSelectedIndex());
							btnRefresh.requestFocus();
						} else {
							if (evt.getSource() instanceof JTextField) {
								try {
									((JNumber) evt.getSource()).commitEdit();
								} catch (Exception ex) {
								}
								;
							}
						}
					}
				});
	}

	private boolean isSuperUser() {
		// return true;
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		return sales != null ? sales.getProfileId().toUpperCase()
				.equals("SUPERUSER") : false;
		// ret
	}
	public void setState(boolean state) {
		btnRefresh.setEnabled(state);
	}
	public void successRefresh() {


			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					setState(true);
					updatePFLastPrice();
//					updatePortfolio();
				}
			});
	}
	public void success() {
		if (orderPanel != null)
			orderPanel.success();
	}

	@Override
	public void newMessage(Model model) {
		/*StockSummary ss = (StockSummary) model;
		String newclient = comboClient.getText();
		if (newclient != null && !newclient.isEmpty()) {
			newclient = newclient.trim().toUpperCase();
			Portfolio pp = getTempPortfolioByStock(ss.getCode());
			if (pp != null) {
				log.info("update portfolio_" + pp.getStock() + ":"+ ss.getClosing());
				pp.setLastPrice(ss.getClosing());

				((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_PORTFOLIO).refresh();
			}

		}*/
		StockSummary ss = (StockSummary) model;
		String newclient = comboClient.getText();
		if (newclient != null && !newclient.isEmpty()) {
			newclient = newclient.trim().toUpperCase();
			Portfolio pp = (Portfolio) ((IEQTradeApp) apps).getTradingEngine()
					.getStore(TradingStore.DATA_PORTFOLIO)
					.getDataByKey(new Object[] {
							newclient,
							ss.getCode() });

			if (pp != null && pp.getAccountId().equals(newclient.trim().toUpperCase())) {
				StockSummary s = (StockSummary) ((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_STOCKSUMMARY).getDataByKey(
						new Object[] { pp.getStock(), "RG" });
				if (s != null) {
					pp.setLastPrice(s.getClosing());
					((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO).refresh();
				}
			}

		}
	}

	public Portfolio getTempPortfolioByStock(String stock) {
		for (Portfolio pp : vports) {
			if (pp.getStock().equals(stock))
				return pp;
		}

		return null;
	}

	private Timer timer ;
	private boolean DateNow = false;

	private void startTimer() {
		timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (DateNow) {
					((IEQTradeApp) apps).getTradingEngine()
					.getStore(TradingStore.DATA_PORTFOLIO).refresh();
				}
			}
		});
		timer.setInitialDelay(1000);
		timer.start();
		refresh();
	}
	//#Valdhy20141111
	public void LoadDefault() {
		try {
			//System.out.println("clear");
			comboClient.getTextEditor().setText("");
			fieldName.getTextEditor().setText("");
			fieldOffice.setText("");
			fieldSales.setText("");

			fieldCategory.setText("");
			fieldPhone.setText("");
			fieldHP.setText("");
			fieldEmail.setText("");

			fieldSingleID.setText("");
			fieldVirtualAcc.setText("");
			fieldIA.setText("");

			fieldCreditLimit.setValue(0);
			fieldCurrentRatio.setValue(0);
			fieldTradingLimit.setValue(0);

			fieldDeposit.setValue(0);
			fieldBid.setValue(0);
			fieldStockValue.setValue(0);

			fieldWithdraw.setValue(0);
			fieldOffer.setValue(0);
			fieldMarketValue.setValue(0);

			fieldBuy.setValue(0);
			fieldNetAC.setValue(0);
			fieldLQValue.setValue(0);

			fieldSell.setValue(0);
			fieldUnreliazedValue.setValue(0);

			fieldTopUp.setValue(0);
			fieldForceSell.setValue(0);

			tStockV.setValue(0);
			tMarketV.setValue(0);
			tUnrealized.setValue(0);
			((FilterColumn) pfFilter.getFilteredData("account"))
			.setField("");
			orderPanel.comboClient.getTextEditor().setText("");
			((FilterColumn) orderPanel.filterOrder.getFilteredData("account"))
			.setField("");

			hSetting = new Hashtable();
			hSetting.put("table", PortfolioDef.getTableDetailDef());
			hSetting.put("tableOrder", OrderDef.getTableDef());
			client = "";

			/*comboClient.setSelectedIndex(0);
			fieldName.setSelectedIndex(0);
			orderPanel.btnClear.doClick();
			btnRefresh.doClick();
			//fieldName.setSelectedIndex (0);
			//btnRefresh.doClick();
			client  = comboClient.getTextEditor().getText();
			fillInfo();
			*/
		} catch (Exception ex) {

		}
	}

	void checkExpKtp(){
		//yosep expktp
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		Account account = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { client },
						new int[] { Account.C_TRADINGID });
		if (sales.getUsertype().toUpperCase().equals("SL" )) {
			if (account.getcExpktp().equals("1")) {
				//Utils.showMessage("This account have expaired ID Test", form);
				Utils.showMessage("      Kartu indentitas Nasabah "+account.getName()+" yang terdaftar sudah kadaluarsa.\n   Mohon kesediaan dan bantuan Bapak/Ibu untuk melakukan followup Kartu Identitas nasabah.\n                               Terima kasih atas perhatian dan kerjasamanya.",
						form);

			}
		}
	}

}

//package eqtrade.trading.ui;
//
//import java.awt.BorderLayout;
//import java.awt.Rectangle;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.awt.event.FocusAdapter;
//import java.awt.event.FocusEvent;
//import java.awt.event.KeyEvent;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.util.HashMap;
//import java.util.Hashtable;
//import java.util.Vector;
//import javax.swing.AbstractAction;
//import javax.swing.InputMap;
//import javax.swing.JButton;
//import javax.swing.JComponent;
//import javax.swing.JLabel;
//import javax.swing.JMenuItem;
//import javax.swing.JPanel;
//import javax.swing.JPopupMenu;
//import javax.swing.JScrollPane;
//import javax.swing.JTabbedPane;
//import javax.swing.JTextField;
//import javax.swing.KeyStroke;
//import javax.swing.ScrollPaneConstants;
//import javax.swing.SwingUtilities;
//import javax.swing.border.EmptyBorder;
//import javax.swing.event.TableModelEvent;
//import javax.swing.event.TableModelListener;
//
//import com.jgoodies.forms.builder.PanelBuilder;
//import com.jgoodies.forms.layout.CellConstraints;
//import com.jgoodies.forms.layout.FormLayout;
//import com.vollux.framework.UI;
//import com.vollux.ui.FilterColumn;
//import com.vollux.ui.JDropDown;
//import com.vollux.ui.JGrid;
//import com.vollux.ui.JNumber;
//import com.vollux.ui.JSkinPnl;
//import com.vollux.ui.JText;
//import com.vollux.ui.JTextLabel;
//
//import eqtrade.application.IEQTradeApp;
//import eqtrade.feed.core.IMessage;
//import eqtrade.feed.engine.FeedParser;
//import eqtrade.feed.engine.FeedStore;
//import eqtrade.feed.model.Model;
//import eqtrade.feed.model.Stock;
//import eqtrade.feed.model.StockSummary;
//import eqtrade.trading.app.TradingUI;
//import eqtrade.trading.core.TradingSetting;
//import eqtrade.trading.engine.TradingStore;
//import eqtrade.trading.model.Account;
//import eqtrade.trading.model.Order;
//import eqtrade.trading.model.OrderDef;
//import eqtrade.trading.model.Portfolio;
//import eqtrade.trading.model.PortfolioDef;
//import eqtrade.trading.model.UserProfile;
//
//public class UIPortfolio extends UI implements IMessage{
//	private JGrid table;
//	private JDropDown comboClient;
//	// private JText fieldName;
//	private JDropDown fieldName;
//	private JText fieldOffice;
//	private JText fieldSales;
//	private JText fieldCategory;
//	private JText fieldPhone;
//	private JText fieldHP;
//	private JText fieldEmail;
//
//	private JText fieldSingleID;
//	private JText fieldVirtualAcc;
//	private JText fieldIA;
//
//	private JNumber fieldCreditLimit;
//	private JNumber fieldCurrentRatio;
//	private JNumber fieldTradingLimit;
//
//	private JNumber fieldDeposit;
//	private JNumber fieldBid;
//	private JNumber fieldStockValue;
//	private JNumber fieldBuy;
//	private JNumber fieldOffer;
//	private JNumber fieldMarketValue;
//	private JNumber fieldSell;
//	private JNumber fieldLQValue;
//	private JNumber fieldUnreliazedValue;
//	private JNumber fieldNetAC;
//
//	private JNumber fieldTopUp;
//	private JNumber fieldForceSell;
//	private JNumber fieldWithdraw;
//
//	private JNumber tStockV;
//	private JNumber tUnrealized;
//	private JNumber tMarketV;
//
//	private JTabbedPane tab;
//
//	private JButton btnRefresh;
//	private JButton btnDuoDate;
//	private FilterPortfolio pfFilter;
//	private String client = "";
//	private OrderPanel orderPanel;
//	private Vector<Portfolio> vports = new Vector<Portfolio>();
//	public UIPortfolio(String app) {
//		super("Portfolio", app);
//		type = C_FRAME;
//		setProperty(C_PROPERTY_NAME, TradingUI.UI_PF);
//	}
//
//	@Override
//	public void focus() {
//		SwingUtilities.invokeLater(new Runnable() {
//			@Override
//			public void run() {
//				table.getTable().requestFocus();
//			}
//		});
//	}
//
//	@Override
//	public void close() {
//		super.close();
//		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
//				.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
//		//log.info("close...");
//	}
//
//	@Override
//	public void hide() {
//
//		super.hide();
//		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
//				.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
//		//log.info("close...");
//
//	}
//
//	@Override
//	protected void createPopup() {
//		popupMenu = new JPopupMenu();
//		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
//			private static final long serialVersionUID = 1L;
//
//			@Override
//			public void actionPerformed(ActionEvent arg0) {
//				table.showProperties();
//			}
//		});
//		propertiesMenu.setText("Properties");
//		popupMenu.add(propertiesMenu);
//	}
//
//	void fillInfo() {
//		Account account = (Account) ((IEQTradeApp) apps)
//				.getTradingEngine()
//				.getStore(TradingStore.DATA_ACCOUNT)
//				.getDataByField(new String[] { client },
//						new int[] { Account.C_TRADINGID });
//		// fieldName.setText(account != null ? account.getName() : "");
//		if (account != null)
//			fieldName.setSelectedItem(account.getName());
//
//		fieldOffice.setText(account != null ? "" : "");
//		fieldSales.setText(account != null ? account.getSalesId() : "");
//		fieldCategory.setText(account != null ? account.getAccType() : "");
//		fieldPhone.setText(account != null ? account.getPhone() : "");
//		fieldHP.setText(account != null ? account.getHandphone() : "");
//		fieldEmail.setText(account != null ? account.getEmail() : "");
//
//		fieldSingleID.setText(account != null ? account.getSingleID() : "");
//		if (account.getVirtualAccount().isEmpty())
//			fieldVirtualAcc.setText(account != null ? ""  : "");
//		else
//			fieldVirtualAcc.setText(account != null ? account.getVirtualAccount().substring(0, 4)+" "+account.getVirtualAccount().substring(4, 8) + " "+account.getVirtualAccount().substring(8, 14)+" "+account.getVirtualAccount().substring(14, 16)  : "");
//
//		if (account.getInvestorAccount().isEmpty())
//			fieldIA.setText(account != null ? "" : "");
//		else
//			fieldIA.setText(account != null ? account.getInvestorAccount().substring(0, 3)+" "+account.getInvestorAccount().substring(3, 5)+" "+account.getInvestorAccount().substring(5, 10)+" "+account.getInvestorAccount().substring(10, 13) : "");
//
//		fieldCreditLimit.setValue(account != null ? account.getCreditLimit()
//				: null);
//		fieldCurrentRatio.setValue(account != null ? account.getCurrentRatio()
//				: null);
//		fieldTradingLimit
//				.setValue(account != null ? account.getCurrTL() : null);
//		fieldDeposit.setValue(account != null ? account.getDeposit() : null);
//		fieldWithdraw.setValue(account != null ? account.getWithdraw() : null);
//		fieldBid.setValue(account != null ? account.getBid() : null);
////Perbaikan market price yang mengikuti close price.
//		//		fieldStockValue
////				.setValue(account != null ? account.getStockVal() : null);
//		fieldBuy.setValue(account != null ? account.getBuy() : null);
//		fieldOffer.setValue(account != null ? account.getOffer() : null);
////		fieldMarketValue.setValue(account != null ? account.getMarketVal()
////				: null);
//		fieldSell.setValue(account != null ? account.getSell() : null);
//		fieldLQValue.setValue(account != null ? account.getLQValue() : null);
//		fieldNetAC.setValue(account != null ? account.getNetAC() : null);
//		fieldTopUp.setValue(account != null ? account.getTopup() : null);
//		fieldForceSell
//				.setValue(account != null ? account.getForceSell() : null);
//
//		Double unrealized =(double) 0;
//		if(account!= null){
//			//log.info("market val "+account.getMarketVal().doubleValue()+":"+account.getStockVal().doubleValue());
//
//			unrealized = account.getMarketVal().doubleValue() - account.getStockVal().doubleValue();
//		}
//
////		fieldUnreliazedValue.setValue(unrealized);
//
//	}
//
//	@Override
//	protected void build() {
//		createPopup();
//		orderPanel = new OrderPanel(apps, form, hSetting);
//		orderPanel.build();
//		orderPanel.comboClient.setEnabled(false);
//		((FilterColumn) orderPanel.filterOrder.getFilteredData("account"))
//				.setField("");
//		orderPanel.filterOrder.fireFilterChanged();
//
//		tab = new JTabbedPane();
//		pfFilter = new FilterPortfolio(form);
//		((FilterColumn) pfFilter.getFilteredData("account")).setField("");
//		comboClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
//				.getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
//		// fieldName = new JText(true);
//		fieldName = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
//				.getStore(TradingStore.DATA_ACCOUNT), Account.C_NAME, true);
//		fieldOffice = new JText(false);
//		fieldSales = new JText(false);
//		fieldCategory = new JText(false);
//		fieldPhone = new JText(false);
//		fieldHP = new JText(false);
//		fieldEmail = new JText(false);
//
//		fieldSingleID = new JText(false);
//		fieldVirtualAcc = new JText(false);
//		fieldIA = new JText(false);
//
//		fieldCreditLimit = new JNumber(Double.class, 0, 0, false, false);
//		fieldCurrentRatio = new JNumber(Double.class, 0, 2, false, false);
//		fieldTradingLimit = new JNumber(Double.class, 0, 0, false, false);
//		fieldDeposit = new JNumber(Double.class, 0, 0, false, false);
//		fieldWithdraw = new JNumber(Double.class, 0, 0, false, false);
//		fieldBid = new JNumber(Double.class, 0, 0, false, false);
//		fieldStockValue = new JNumber(Double.class, 0, 0, false, false);
//		fieldBuy = new JNumber(Double.class, 0, 0, false, false);
//		fieldOffer = new JNumber(Double.class, 0, 0, false, false);
//		fieldMarketValue = new JNumber(Double.class, 0, 0, false, false);
//		fieldSell = new JNumber(Double.class, 0, 0, false, false);
//		fieldLQValue = new JNumber(Double.class, 0, 0, false, false);
//		fieldNetAC = new JNumber(Double.class, 0, 0, false, false);
//		fieldTopUp = new JNumber(Double.class, 0, 0, false, false);
//		fieldForceSell = new JNumber(Double.class, 0, 0, false, false);
//		fieldUnreliazedValue = new JNumber(Double.class, 0, 0, false, false);
//		btnRefresh = new JButton("Refresh");
//		btnDuoDate = new JButton("Duedate");
//		btnRefresh.setMnemonic('F');
//
//		fieldBid.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//			}
//		});
//
//		fieldBid.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mousePressed(MouseEvent e) {
//				tab.setSelectedIndex(2);
//			}
//		});
//		fieldOffer.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mousePressed(MouseEvent e) {
//				tab.setSelectedIndex(2);
//			}
//		});
//		fieldMarketValue.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mousePressed(MouseEvent e) {
//				tab.setSelectedIndex(1);
//			}
//		});
//
//		fieldStockValue.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mousePressed(MouseEvent e) {
//				tab.setSelectedIndex(1);
//			}
//		});
//
//		fieldLQValue.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mousePressed(MouseEvent e) {
//				tab.setSelectedIndex(1);
//			}
//		});
//		fieldDeposit.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mousePressed(MouseEvent e) {
//				btnRefresh.requestFocus();
//				comboClient.getTextEditor().requestFocus();
//				SwingUtilities.invokeLater(new Runnable() {
//					@Override
//					public void run() {
//						HashMap param = new HashMap();
//						param.put("CLIENT", client);
//						apps.getUI().showUI(TradingUI.UI_CASHCOLL, param);
//					}
//				});
//			}
//		});
//
//		/*fieldBuy.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mousePressed(MouseEvent e) {
//				//btnRefresh.requestFocus();
//				//comboClient.getTextEditor().requestFocus();
//				SwingUtilities.invokeLater(new Runnable() {
//					@Override
//					public void run() {
//						HashMap param = new HashMap();
//						param.put("CLIENT", client);
//						param.put("BUYSELL", "B");
//						apps.getUI().showUI(TradingUI.UI_BUYSELLDETAIL, param);
//					}
//				});
//			}
//		});
//
//		fieldSell.addMouseListener(new MouseAdapter() {
//			@Override
//			public void mousePressed(MouseEvent e) {
//				//btnRefresh.requestFocus();
//				//comboClient.getTextEditor().requestFocus();
//				SwingUtilities.invokeLater(new Runnable() {
//					@Override
//					public void run() {
//						HashMap param = new HashMap();
//						param.put("CLIENT", client);
//						param.put("BUYSELL", "S");
//						apps.getUI().showUI(TradingUI.UI_BUYSELLDETAIL, param);
//					}
//				});
//			}
//		});*/
//		//Tambahan Due date
//		btnDuoDate.addActionListener(new ActionListener() {
//
//			@Override
//			public void actionPerformed(ActionEvent arg0) {
//				SwingUtilities.invokeLater(new Runnable() {
//					@Override
//					public void run() {
//						HashMap param = new HashMap();
//						//System.out.println(">>>>"+client);
//						param.put("CLIENT", client);
//						apps.getUI().showUI(TradingUI.UI_DUEDATE,param);
//					}
//				});
//
//			}
//		})	;
//
//		registerEvent(comboClient.getTextEditor());
//		registerEventCombo(comboClient);
//
//		registerEvent(fieldName.getTextEditor());
//		registerEventCombo(fieldName);
//
//		registerEvent(btnRefresh);
//
//		((IEQTradeApp) apps).getTradingEngine()
//				.getStore(TradingStore.DATA_ACCOUNT)
//				.addTableModelListener(new TableModelListener() {
//					@Override
//					public void tableChanged(TableModelEvent e) {
//						fillInfo();
//					}
//				});
//
//
//
//		comboClient.getTextEditor().addFocusListener(new FocusAdapter() {
//
//			@Override
//			public void focusGained(FocusEvent e) {
//				comboClient.showPopup();
//			}
//
//			public void focusLost(FocusEvent e) {
//				String newclient = comboClient.getText().trim().toUpperCase();
//				// if (!newclient.equals(client)) {
//				client = newclient;
//				((FilterColumn) pfFilter.getFilteredData("account"))
//						.setField(comboClient.getText().trim().toUpperCase()
//								.equals("") ? "" : comboClient.getText().trim()
//								.toUpperCase());
//				pfFilter.fireFilterChanged();
//				orderPanel.comboClient.getTextEditor().setText(
//						comboClient.getText().trim().toUpperCase());
//				((FilterColumn) orderPanel.filterOrder
//						.getFilteredData("account")).setField(comboClient
//						.getText().trim().equals("") ? "" : comboClient
//						.getText().trim().toUpperCase());
//				orderPanel.filterOrder.fireFilterChanged();
//				fillInfo();
//				updatePortfolio();
//				// }
//			}
//		});
//
//		fieldName.getTextEditor().addFocusListener(new FocusAdapter() {
//
//			@Override
//			public void focusGained(FocusEvent e) {
//				fieldName.showPopup();
//			}
//
//			@Override
//			public void focusLost(FocusEvent e) {
//				String newclient = comboClient.getText().trim().toUpperCase();
//				// if (!newclient.equals(client)) {
//				client = newclient;
//				((FilterColumn) pfFilter.getFilteredData("account"))
//						.setField(comboClient.getText().trim().toUpperCase()
//								.equals("") ? "" : comboClient.getText().trim()
//								.toUpperCase());
//				pfFilter.fireFilterChanged();
//				orderPanel.comboClient.getTextEditor().setText(
//						comboClient.getText().trim().toUpperCase());
//				((FilterColumn) orderPanel.filterOrder
//						.getFilteredData("account")).setField(comboClient
//						.getText().trim().equals("") ? "" : comboClient
//						.getText().trim().toUpperCase());
//				orderPanel.filterOrder.fireFilterChanged();
//				fillInfo();
//			}
//
//		});
//
//		btnRefresh.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				if (!comboClient.getText().trim().equals("")) {
//
//					/*if (isSuperUser()) {
//						((IEQTradeApp) apps).getTradingEngine().refreshData(
//								comboClient.getText().trim().toUpperCase());
//					} else {*/
//					setState(false);
//						((IEQTradeApp) apps).getTradingEngine().refreshAccount(
//								comboClient.getText().trim().equals("") ? "%"
//										: comboClient.getText().trim()
//												.toUpperCase());
//						((IEQTradeApp) apps)
//								.getTradingEngine()
//								.refreshPortfolio(
//										comboClient.getText().trim().equals("") ? "%"
//												: comboClient.getText().trim()
//														.toUpperCase(), "%");
//					updatePortfolio();
//					//}
//				}
//			}
//		});
//
//		table = createTable(
//				((IEQTradeApp) apps).getTradingEngine().getStore(
//						TradingStore.DATA_PORTFOLIO), pfFilter,
//				(Hashtable) hSetting.get("table"));
//		table.getTable().add(popupMenu);
//		table.getTable().addMouseListener(new MyMouseAdapter());
//		table.addMouseListener(new MyCustomMouseAdapter());
//		table.setColumnHide(PortfolioDef.columnhidedetail);
//		table.getTable().getModel()
//		.addTableModelListener(new RowChanged());
//		registerEvent(table);
//		registerEvent(table.getTable());
//		//count
//		tStockV= new JNumber(Double.class, 0, 0, false, false);
//		tUnrealized = new JNumber(Double.class, 0, 0, false, false);
//		tMarketV = new JNumber(Double.class, 0, 0, false, false);
//
//		JPanel infoPanel = new JPanel();
//		FormLayout infLayout = new FormLayout(
//				"pref,3dlu,110px,10dlu,pref,3dlu,110px, 10dlu, pref, 3dlu, 110px, 10dlu, pref","pref");
//		PanelBuilder infBuilder = new PanelBuilder(infLayout, infoPanel);
//		infBuilder.setBorder(new EmptyBorder(4, 4, 4, 4));
//		CellConstraints cc = new CellConstraints();
//		infBuilder.add(new JLabel("Stock Val"), cc.xy(1, 1));
//		infBuilder.add(tStockV, cc.xy(3, 1));
//		infBuilder.add(new JLabel("Market Val"), cc.xy(5, 1));
//		infBuilder.add(tMarketV, cc.xy(7, 1));
//		infBuilder.add(new JLabel("Unrealized"), cc.xy(9, 1));
//		infBuilder.add(tUnrealized, cc.xy(11, 1));
//		JScrollPane b = new JScrollPane(infoPanel);
//		b.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
//		b.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
//
//		JSkinPnl stockList = new JSkinPnl();
//		stockList.add(b,BorderLayout.SOUTH);
//		stockList.add(table, BorderLayout.CENTER);
//
//		JPanel editorPanel = new JPanel();
//		FormLayout layout = new FormLayout(
//				"pref,2dlu,80px,2dlu,pref,2dlu,130px, 2dlu, pref, 2dlu, 130px , 2dlu, pref, 2dlu, 130px",
//				"pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref");
//		PanelBuilder builder = new PanelBuilder(layout, editorPanel);
//		builder.setBorder(new EmptyBorder(4, 4, 4, 4));
//		//CellConstraints cc = new CellConstraints();
//		builder.addSeparator("Client Info", cc.xyw(1, 1, 11));
//		builder.add(btnRefresh, cc.xy(15, 1));
//		builder.add(btnDuoDate,cc.xyw(12, 1,2));
//		builder.add(new JTextLabel("Client", 'C', comboClient.getTextEditor()),
//				cc.xy(1, 3));
//		builder.add(comboClient, cc.xy(3, 3));
//		builder.add(fieldName, cc.xyw(5, 3, 3));
//		builder.add(new JLabel("Office"), cc.xy(9, 3));
//		builder.add(fieldOffice, cc.xy(11, 3));
//		builder.add(new JLabel("Sales"), cc.xy(13, 3));
//		builder.add(fieldSales, cc.xy(15, 3));
//		builder.add(new JLabel("Category"), cc.xy(1, 5));
//		builder.add(fieldCategory, cc.xy(3, 5));
//		builder.add(new JLabel("Phone"), cc.xy(5, 5));
//		builder.add(fieldPhone, cc.xy(7, 5));
//		builder.add(new JLabel("HP"), cc.xy(9, 5));
//		builder.add(fieldHP, cc.xy(11, 5));
//		builder.add(new JLabel("Email"), cc.xy(13, 5));
//		builder.add(fieldEmail, cc.xy(15, 5));
//
//		builder.add(new JLabel("Single ID"), cc.xy(5, 7));
//		builder.add(fieldSingleID, cc.xy(7, 7));
//		builder.add(new JLabel("Virtual A/C"), cc.xy(9, 7));
//		builder.add(fieldVirtualAcc, cc.xy(11, 7));
//		builder.add(new JLabel("Investor A/C"), cc.xy(13, 7));
//		builder.add(fieldIA, cc.xy(15, 7));
//
//
//		builder.addSeparator("Credit Info", cc.xyw(1, 9, 15));
//		builder.add(new JLabel("Credit Limit"), cc.xy(5, 11));
//		builder.add(fieldCreditLimit, cc.xy(7, 11));
//		builder.add(new JLabel("Current Ratio"), cc.xy(9, 11));
//		builder.add(fieldCurrentRatio, cc.xy(11, 11));
//		builder.add(new JLabel("Trading Limit"), cc.xy(13, 11));
//		builder.add(fieldTradingLimit, cc.xy(15, 11));
//		builder.addSeparator("Limit Info", cc.xyw(1, 13, 15));
//		builder.add(new JLabel("Deposit"), cc.xy(5, 15));
//		builder.add(fieldDeposit, cc.xy(7, 15));
//		builder.add(new JLabel("Bid"), cc.xy(9, 15));
//		builder.add(fieldBid, cc.xy(11, 15));
//		builder.add(new JLabel("Stock Value"), cc.xy(13, 15));
//		builder.add(fieldStockValue, cc.xy(15, 15));
//
//		builder.add(new JLabel("Cash Withdraw"), cc.xy(5, 17));
//		builder.add(fieldWithdraw, cc.xy(7, 17));
//		builder.add(new JLabel("Offer"), cc.xy(9, 17));
//		builder.add(fieldOffer, cc.xy(11, 17));
//
//		builder.add(new JLabel("Net A/C"), cc.xy(9, 19));
//		builder.add(fieldNetAC, cc.xy(11, 19));
//
//		builder.add(new JLabel("Market Value"), cc.xy(13, 17));
//		builder.add(fieldMarketValue, cc.xy(15, 17));
//
//		builder.add(new JLabel("Buy"), cc.xy(5, 19));
//		builder.add(fieldBuy, cc.xy(7, 19));
//		builder.add(new JLabel("LQ Value"), cc.xy(13, 19));
//		builder.add(fieldLQValue, cc.xy(15, 19));
//		builder.add(new JLabel("Sell"), cc.xy(5, 21));
//		builder.add(fieldSell, cc.xy(7, 21));
//
//		builder.add(new JLabel("Unrealized Value"), cc.xy(13, 21));
//		builder.add(fieldUnreliazedValue, cc.xy(15, 21));
//
//		builder.addSeparator("", cc.xyw(1, 23, 15));
//		builder.add(new JLabel("Top Up"), cc.xy(13, 25));
//		builder.add(fieldTopUp, cc.xy(15, 25));
//		builder.add(new JLabel("Force Sell"), cc.xy(13, 27));
//		builder.add(fieldForceSell, cc.xy(15, 27));
//
//		pnlContent = new JSkinPnl(new BorderLayout());
//		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
//		pnlContent.setOpaque(true);
//		tab.addTab("Portfolio", editorPanel);
//		tab.addTab("Stock List", stockList);
//		tab.addTab("Order", orderPanel);
//		pnlContent.add(tab, BorderLayout.CENTER);
//		refresh();
//
//		initData();
//
//	}
//
//	protected void updatePortfolio() {
//		//log.info("updat_portfolio_stock");
//		((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
//				.get(FeedParser.PARSER_STOCKSUMMARY).removeListener(this);
//
//		String newclient = comboClient.getText();
//		if (newclient != null && !newclient.isEmpty()) {
//			newclient = newclient.trim().toUpperCase();
//			vports.clear();
//			vports = getPortfolioByClient(newclient);
//			for (Portfolio pp : vports) {
//				StockSummary ss = (StockSummary) ((IEQTradeApp) apps)
//						.getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY)
//						.getDataByKey(new Object[] { pp.getStock(), "RG" });
//				pp.setLastPrice(ss.getClosing());
//			}
//
//			((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
//					.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
//		}
//		//log.info("updat_portfolio_stock_done");
//
//	}
//
//	private Vector<Portfolio> getPortfolioByClient(String newclient) {
//		int i = ((IEQTradeApp) apps).getTradingEngine()
//				.getStore(TradingStore.DATA_PORTFOLIO).getRowCount();
//		Vector<Portfolio> vports = new Vector<Portfolio>();
//		for (int x = 0; x < i; x++) {
//			Portfolio p = (Portfolio) ((IEQTradeApp) apps).getTradingEngine()
//					.getStore(TradingStore.DATA_PORTFOLIO).getDataByIndex(x);
//			if (p.getTradingId().equals(newclient)) {
//				vports.add(p);
//			}
//
//		}
//		return vports;
//	}
//
//	private void initData() {
//		// TODO Auto-generated method stub
//		if (comboClient.getModel() != null
//				&& comboClient.getModel().getSize() > 0) {
//			comboClient.setSelectedIndex(0);
//			String newclient = comboClient.getText().trim().toUpperCase();
//			// if (!newclient.equals(client)) {
//			client = newclient;
//			((FilterColumn) pfFilter.getFilteredData("account"))
//					.setField(comboClient.getText().trim().toUpperCase()
//							.equals("") ? "" : comboClient.getText().trim()
//							.toUpperCase());
//			pfFilter.fireFilterChanged();
//			orderPanel.comboClient.getTextEditor().setText(
//					comboClient.getText().trim().toUpperCase());
//			((FilterColumn) orderPanel.filterOrder.getFilteredData("account"))
//					.setField(comboClient.getText().trim().equals("") ? ""
//							: comboClient.getText().trim().toUpperCase());
//			orderPanel.filterOrder.fireFilterChanged();
//			fillInfo();
//			// }
//
//		}
//	}
//
//	public void loadSetting() {
//		hSetting = (Hashtable) TradingSetting
//				.getLayout((String) getProperty(C_PROPERTY_NAME));
//		if (hSetting == null) {
//			hSetting = new Hashtable();
//			hSetting.put("table", PortfolioDef.getTableDetailDef());
//			hSetting.put("tableOrder", OrderDef.getTableDef());
//		}
//		if (hSetting.get("size") == null)
//			hSetting.put("size", new Rectangle(20, 20, 850, 350));
//	}
//
//	public void saveSetting() {
//		hSetting.put(
//				"size",
//				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
//						.getHeight()));
//		hSetting.put("table", table.getTableProperties());
//		hSetting.put("tableOrder", orderPanel.tableOrder.getTableProperties());
//		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
//				hSetting);
//	}
//
//	public void refresh() {
//		table.getViewport().setBackground(
//				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
//		table.setNewFont(TradingSetting.getFont());
//		orderPanel.refresh();
//	}
//	protected class RowChanged implements TableModelListener {
//
//		public void tableChanged(final TableModelEvent e) {
//			double count = 0, tlot = 0, tvalue = 0,tmarket = 0;
//			int size = table.getTable().getRowCount();
//
//			for (int i = 0; i < size; i++) {
//				Portfolio order = (Portfolio) table.getDataByIndex(i);
//				count++;
//				tlot = tlot + order.getStockVal().doubleValue();
//				tvalue = tvalue + order.getPLValue().doubleValue();
//				tmarket = tmarket+ order.getValueLastPrice();
//
//			}
//			tStockV.setValue(new Double(tlot));
//			tUnrealized.setValue(new Double(tvalue));
//			tMarketV.setValue(new Double(tmarket));
//			//penambahan dr rmi
//			fieldStockValue.setValue(new Double(tlot));
//			fieldUnreliazedValue.setValue(new Double(tvalue));
//			fieldMarketValue.setValue(new Double(tmarket));
//
//			if (e.getType() == TableModelEvent.INSERT) {
//				Rectangle r = table.getTable().getCellRect(size - 1, 0,
//						true);
//				table.scrollRectToVisible(r);
//			}
//
//		}
//	}
//	private void registerEventCombo(JDropDown comp) {
//		comp.getEditor().getEditorComponent()
//				.setFocusTraversalKeysEnabled(false);
//		InputMap inputMap = comp.getTextEditor().getInputMap(
//				JComponent.WHEN_FOCUSED);
//		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");
//
//		comp.getTextEditor().getActionMap()
//				.put("tabAction", new AbstractAction("tabAction") {
//					private static final long serialVersionUID = 1L;
//
//					@Override
//					public void actionPerformed(ActionEvent e) {
//
//						if (e.getSource().equals(comboClient.getTextEditor())) {
//							comboClient.setSelectedIndex(comboClient
//									.getSelectedIndex());
//							comboClient.hidePopup();
//							btnRefresh.requestFocus();
//						} else if (e.getSource().equals(
//								fieldName.getTextEditor())) {
//							fieldName.setSelectedIndex(fieldName
//									.getSelectedIndex());
//							comboClient.setSelectedIndex(fieldName
//									.getSelectedIndex());
//							fieldName.hidePopup();
//							btnRefresh.requestFocus();
//						}
//					}
//
//				});
//	}
//
//	void registerEvent(JComponent comp) {
//		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
//		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
//				"escapeAction");
//		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
//				"enterAction");
//		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "clientAction");
//		comp.getActionMap().put("clientAction",
//				new AbstractAction("clientAction") {
//					private static final long serialVersionUID = 1L;
//
//					public void actionPerformed(ActionEvent evt) {
//						comboClient.getTextEditor().requestFocus();
//					}
//				});
//		comp.getActionMap().put("escapeAction",
//				new AbstractAction("escapeAction") {
//					private static final long serialVersionUID = 1L;
//
//					public void actionPerformed(ActionEvent evt) {
//						form.setVisible(false);
//					}
//				});
//		comp.getActionMap().put("enterAction",
//				new AbstractAction("enterAction") {
//					private static final long serialVersionUID = 1L;
//
//					public void actionPerformed(ActionEvent evt) {
//						if (evt.getSource() instanceof JButton) {
//							((JButton) evt.getSource()).doClick();
//						} else if (evt.getSource().equals(
//								comboClient.getTextEditor())) {
//							comboClient.setSelectedIndex(comboClient
//									.getSelectedIndex());
//							comboClient.hidePopup();
//							btnRefresh.requestFocus();
//						} else if (evt.getSource().equals(
//								fieldName.getTextEditor())) {
//							fieldName.setSelectedIndex(fieldName
//									.getSelectedIndex());
//							fieldName.hidePopup();
//							comboClient.setSelectedIndex(fieldName
//									.getSelectedIndex());
//							btnRefresh.requestFocus();
//						} else {
//							if (evt.getSource() instanceof JTextField) {
//								try {
//									((JNumber) evt.getSource()).commitEdit();
//								} catch (Exception ex) {
//								}
//								;
//							}
//						}
//					}
//				});
//	}
//
//	private boolean isSuperUser() {
//		// return true;
//		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
//				.getTradingEngine()
//				.getStore(TradingStore.DATA_USERPROFILE)
//				.getDataByField(new Object[] { "40" },
//						new int[] { UserProfile.C_MENUID });
//		return sales != null ? sales.getProfileId().toUpperCase()
//				.equals("SUPERUSER") : false;
//		// ret
//	}
//	public void setState(boolean state) {
//		btnRefresh.setEnabled(state);
//	}
//	public void successRefresh() {
//
//
//			SwingUtilities.invokeLater(new Runnable() {
//
//				@Override
//				public void run() {
//					setState(true);
//				}
//			});
//	}
//	public void success() {
//		if (orderPanel != null)
//			orderPanel.success();
//	}
//
//	@Override
//	public void newMessage(Model model) {
//		StockSummary ss = (StockSummary) model;
//		String newclient = comboClient.getText();
//		if (newclient != null && !newclient.isEmpty()) {
//			newclient = newclient.trim().toUpperCase();
//			Portfolio pp = getTempPortfolioByStock(ss.getCode());
//			if (pp != null) {
//				//log.info("update portfolio_" + pp.getStock() + ":"+ ss.getClosing());
//				pp.setLastPrice(ss.getClosing());
//
//				((IEQTradeApp) apps).getTradingEngine()
//						.getStore(TradingStore.DATA_PORTFOLIO).refresh();
//			}
//
//		}
//	}
//
//	public Portfolio getTempPortfolioByStock(String stock) {
//		for (Portfolio pp : vports) {
//			if (pp.getStock().equals(stock))
//				return pp;
//		}
//
//		return null;
//	}
//
//}
