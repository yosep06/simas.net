package eqtrade.trading.ui;

import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;
import eqtrade.trading.model.Order;

public class FilterGTC<GTCModel> extends FilterBase{
	protected HashMap mapFilter = new HashMap();

	public FilterGTC(Component comp) {
		super(comp, "filter");
		mapFilter.put("account", new FilterColumn("account",String.class,null, FilterColumn.C_EQUAL));
		mapFilter.put("stock", new FilterColumn("stock", String.class, null,
				FilterColumn.C_EQUAL));
		mapFilter.put("bos", new FilterColumn("bos", String.class, null,
				FilterColumn.C_EQUAL));
		mapFilter.put("status", new FilterColumn("status", Vector.class, null,
				FilterColumn.C_MEMBEROF));
		/*mapFilter.put("tradeno", new FilterColumn("tradeno", String.class,
				new String("0"), FilterColumn.C_EQUAL));*/
		mapFilter.put("taker", new FilterColumn("taker", String.class,
				new String("0"), FilterColumn.C_EQUAL));
		mapFilter.put("user", new FilterColumn("user", String.class,
				new String(""), FilterColumn.C_EQUAL));
		/*mapFilter.put("floor", new FilterColumn("floor", String.class,
				new String("0"), FilterColumn.C_EQUAL));*/
		mapFilter.put("price", new FilterColumn("price", Double.class, null,
				FilterColumn.C_EQUAL));
		/*mapFilter.put("marketid", new FilterColumn("marketid", String.class,
				null, FilterColumn.C_EQUAL));*/
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try	{
			Column val =(Column) model.getValueAt(row, 0);
			Order dat = (Order) val.getSource();
			//System.out.println("trading"+dat.getTradingId()+"stock"+(((FilterColumn) mapFilter.get("account")).getField()));
				if 	/*(((FilterColumn) mapFilter.get("entrytime")).compare(dat
					.getEntryTime())
					&&(((FilterColumn) mapFilter.get("orderid")).compare(dat
					.getId())*/
					(((FilterColumn) mapFilter.get("account")).compare(dat
					.getTradingId())
					/*&&(((FilterColumn) mapFilter.get("boardid")).compare(dat
					.getBoard())*/
					&& ((FilterColumn) mapFilter.get("stock")).compare(dat
							.getStock())
					&& ((FilterColumn) mapFilter.get("bos")).compare(dat
							.getBOS())
					&& ((FilterColumn) mapFilter.get("status")).compare(dat
							.getStatus())
					/*&& ((FilterColumn) mapFilter.get("lot")).compare(dat
							.getLot())*/
					/*&& ((FilterColumn) mapFilter.get("volume")).compare(dat
							.getVolume())*/
					&& ((FilterColumn) mapFilter.get("price")).compare(dat
							.getPrice())
					/*&& ((FilterColumn) mapFilter.get("marketid")).compare(dat
							.getMarketOrderId())
					&& ((FilterColumn) mapFilter.get("validuntil")).compare(dat
							.getcValiduntil())
					&& ((FilterColumn) mapFilter.get("entryby")).compare(dat
									.getEntryBy())*/
					/*&& ((FilterColumn) mapFilter.get("floor")).compare(dat
							.getIsFloor())*/
					&& filterTaker(((FilterColumn) mapFilter.get("taker"))
							.getField().toString(),
							((FilterColumn) mapFilter.get("user")).getField()
									.toString(), dat)) {
	
								avail = true;
		}
		return avail;
	}catch (Exception e)	{
		return false;
	}
}
	
	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public int showDialog() {
		return super.showDialog();
	}
	
	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
		
	}
	boolean filterTaker(String type, String user, Order ord) {
		boolean result = true;
		if (type.equals("0")) { // my order only
			result = ord.getEntryBy().equals(user)
					|| ord.getAmendBy().equals(user)
					|| ord.getWithdrawBy().equals(user);
		} else if (type.equals("1")) { // taker order only
			result = !ord.getEntryBy().equals(user)
					|| (!ord.getAmendBy().equals("") && !ord.getAmendBy()
							.equals(user))
					|| (!ord.getWithdrawBy().equals("") && !ord.getWithdrawBy()
							.equals(user));
		} // all order
		return result;
	}

	/*boolean isTrade(Order order) {
		boolean result = false;
		if ((!order.getTradeNo().equals("0"))) {
			result = true;
		}
		return result;
	}*/

}
