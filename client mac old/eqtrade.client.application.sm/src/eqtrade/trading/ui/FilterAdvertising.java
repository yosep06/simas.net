package eqtrade.trading.ui;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.trading.model.NegDealList;

public class FilterAdvertising extends FilterBase {
	public FilterAdvertising(Component parent) {
		super(parent, "filter");
		mapFilter.put("advertising", new FilterColumn("advertising",
				String.class, null, FilterColumn.C_EQUAL));
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	public void clearFilter() {
		((FilterColumn) mapFilter.get("advertising")).setField(null);
		fireFilterChanged();
	}

	public boolean isFiltered() {
		return (!(((FilterColumn) mapFilter.get("advertising")).getField() != null));
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			NegDealList dat = (NegDealList) val.getSource();
			if (!((FilterColumn) mapFilter.get("advertising")).compare(dat
					.getIsAdvertising())) {
				avail = true;
			}
			return avail;
		} catch (Exception ec) {
			ec.printStackTrace();
			return false;
		}
	}
}