package eqtrade.trading.ui.core;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.Utils;
import eqtrade.trading.app.TradingUI;

public class UILogon extends UI {
	private JLabel lblUserid;
	private JLabel lblPassword;
	private JLabel lblStatus;
	private JTextField fieldUserid;
	private JPasswordField fieldPassword;
	private JButton btnOK;
	private JButton btnCancel;
	private JLabel image;

	public UILogon(String app) {
		super("next-G (trading application)", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_LOGON);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldUserid.requestFocus();
			}
		});
	}

	@Override
	protected void build() {
		image = new JLabel(
				new ImageIcon(EQTrade.class.getResource("logon.png")),
				SwingConstants.LEFT);

		lblUserid = new JLabel("user");
		lblPassword = new JLabel("PIN");
		lblStatus = new JLabel("please enter correct user & PIN");
		fieldUserid = new JTextField(4);
		fieldPassword = new JPasswordField(10);
		btnOK = new JButton("OK");
		btnCancel = new JButton("Cancel");

		JPanel panel = new JPanel();
		FormLayout layout = new FormLayout(
				"pref, 2dlu, 100px, 2dlu, pref, 2dlu, 150px, 2dlu, pref, 2dlu, pref, pref:grow",
				"0, 0, pref, 4dlu, pref");
		PanelBuilder builder = new PanelBuilder(layout, panel);
		panel.setBorder(new EmptyBorder(15, 45, 15, 15));
		CellConstraints cc = new CellConstraints();
		builder.add(lblStatus, cc.xywh(1, 3, 11, 1));
		builder.add(lblUserid, cc.xy(1, 5));
		builder.add(fieldUserid, cc.xy(3, 5));
		builder.add(lblPassword, cc.xy(5, 5));
		builder.add(fieldPassword, cc.xy(7, 5));
		builder.add(btnOK, cc.xy(9, 5));
		builder.add(btnCancel, cc.xy(11, 5));

		JPanel panel2 = new JPanel();
		FormLayout layout2 = new FormLayout("550px", "pref, 2dlu, pref");
		PanelBuilder builder2 = new PanelBuilder(layout2, panel2);
		panel2.setBorder(new EmptyBorder(3, 3, 3, 3));
		builder2.add(image, cc.xy(1, 1));
		builder2.add(panel, cc.xy(1, 3));

		btnOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doLogin();
			}
		});

		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(panel2, BorderLayout.CENTER);
		registerEvent(fieldUserid);
		registerEvent(fieldPassword);
		registerEvent(btnOK);
		registerEvent(btnCancel);

	}

	private void doLogin() {
		if (fieldUserid.getText().trim().equals("")) {
			lblStatus.setText("please enter valid user");
			fieldUserid.requestFocus();
		} else if ((new String(fieldPassword.getPassword()).trim().equals(""))) {
			lblStatus.setText("please enter valid password");
			fieldPassword.requestFocus();
		} else {
			setEnableInfo(false);
			lblStatus.setText("connecting.....");
			String strPwd = Utils
					.getMD5(new String(fieldPassword.getPassword()));
			((IEQTradeApp) apps).getTradingEngine().login(
					fieldUserid.getText().toUpperCase(), strPwd);
		}
	}

	private void setEnableInfo(boolean b) {
		fieldUserid.setEnabled(b);
		fieldPassword.setEnabled(b);
		btnOK.setEnabled(b);
	}

	@Override
	protected void initUI() {
		super.initUI();
		form.setResizable(false);
		form.getBtnClose().setVisible(false);
		form.getBtnMin().setVisible(false);
	}

	@Override
	public void show() {
		focus();
		Utils.showToCenter(form);
		super.show();
	}

	@Override
	public void loadSetting() {
		// hSetting =
		// (Hashtable)CorePlugin.getPreferences().getSetting((String)getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 550, 380));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		// CorePlugin.getPreferences().setSetting((String)getProperty(C_PROPERTY_NAME),
		// hSetting);
	}

	@Override
	public void refresh() {
	}

	public void failed(String reason) {
		setStatus(reason);
		setEnableInfo(true);
		fieldUserid.selectAll();
		fieldUserid.requestFocus();
	}

	public void setStatus(String status) {
		lblStatus.setText(status);
	}

	public void success() {
		saveSetting();
		close();
	}

	private void registerEvent(JComponent comp) {
		InputMap inputMap = comp
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		comp.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						btnCancel.doClick();
					}
				});
		comp.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton)
							((JButton) evt.getSource()).doClick();
						else
							btnOK.doClick();
					}
				});
	}

}
