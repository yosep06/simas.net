package eqtrade.trading.ui.core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.ITradingEngine;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.TradingEngine;
import eqtrade.trading.engine.TradingEventDispatcher;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.UserProfile;

public final class UIRefresh extends UI {
	private JButton btnCancel;
	private JButton btnOK;
	// private JLabel lblInfo;
	private JLabel lblStatus;
	private JSkinDlg frame;
	private JCheckBox checkMaster;
	private JCheckBox checkAccount;
	private JCheckBox checkOrder;
	private JCheckBox checkPF;
	// private JTextField fieldClient;
	private JLabel lblClient;
	private JDropDown comboClient;
	private JButton btnAdd, btnDelete;
	private static JList listClient;
	private JPanel pnlSuperuserClient = null;

	public UIRefresh(String app) {
		super("Trading Refresh", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_REFRESH);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				btnOK.requestFocus();
			}
		});
	}

	private void setState(boolean state) {
		checkMaster.setEnabled(state);
		checkAccount.setEnabled(state);
		checkOrder.setEnabled(state);
		checkPF.setEnabled(state);
		btnOK.setEnabled(state);
		btnCancel.setEnabled(state);
		// fieldClient.setEnabled(state);
	}

	@Override
	public void refresh() {
		if (frame != null) {
			frame.updateUI();
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}

	@Override
	protected void build() {
		// fieldClient = new JTextField();
		comboClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		btnAdd = new JButton("Add");
		btnDelete = new JButton("Delete");
		lblClient = new JLabel("Client");
		listClient = new JList(new DefaultListModel());
		listClient.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		// listClient.setFont(new Font(Font., Font.PLAIN, 12));

		JScrollPane grScrPane = new JScrollPane(listClient,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		checkMaster = new JCheckBox("Master data");
		checkAccount = new JCheckBox("Account");
		checkPF = new JCheckBox("Portfolio");
		checkOrder = new JCheckBox("Order & Trade");
		checkMaster.setSelected(false);
		checkAccount.setSelected(false);
		checkOrder.setSelected(false);
		checkPF.setSelected(false);
		checkMaster.setOpaque(false);
		checkAccount.setOpaque(false);
		checkOrder.setOpaque(false);
		checkPF.setOpaque(false);

		checkOrder.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setStatusfilterClient(checkOrder.isSelected());

			}
		});

		// lblInfo = new JLabel("please wait...");
		lblStatus = new JLabel(" ");
		btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				/*
				 * if (isSuperUser()) { if
				 * (fieldClient.getText().trim().equals("")) {
				 * Utils.showMessage("please enter valid client code", frame); }
				 * else { setState(false); ((IEQTradeApp)
				 * apps).getTradingEngine().refreshData(
				 * fieldClient.getText().trim().toUpperCase()); } } else { if
				 * (!checkMaster.isSelected() && !checkAccount.isSelected() &&
				 * !checkOrder.isSelected() && !checkPF.isSelected()) {
				 * Utils.showMessage(
				 * "please select one or more data to refresh..", frame); } else
				 * { setState(false); ((IEQTradeApp)
				 * apps).getTradingEngine().refreshData(
				 * checkMaster.isSelected(), checkAccount.isSelected(),
				 * checkPF.isSelected(), checkOrder.isSelected()); } }
				 */

				if (!checkMaster.isSelected() && !checkAccount.isSelected()
						&& !checkOrder.isSelected() && !checkPF.isSelected()) {
					Utils.showMessage(
							"please select one or more data to refresh..",
							frame);
				} else {
					boolean isordercheck = checkOrder.isSelected();

					if (isSuperUser()) {
						// if (isordercheck) {

						// }

						Object[] objs = (Object[]) ((DefaultListModel) listClient
								.getModel()).toArray();
						StringBuffer sbf = new StringBuffer();
						for (int i = 0; i < objs.length; i++) {
							String[] sp = objs[i].toString().split("-");
							String tradingid = sp[0].trim();
							Account acc = getAccountId(tradingid);
							try {
								((ITradingEngine) ((IEQTradeApp) apps)
										.getTradingEngine())
										.getConnection()
										.loadOrder(
												TradingEventDispatcher.EVENT_REFRESHSTATUS,
												acc.getTradingId(), "%", "%", false);
								((ITradingEngine) ((IEQTradeApp) apps)
										.getTradingEngine()).getConnection()
										.addFilterByClient(acc.getTradingId());
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							// .refreshOrder(acc.getTradingId(), "%");
							sbf.append(acc.getAccId()).append("#");
						}

						((ITradingEngine) ((IEQTradeApp) apps)
								.getTradingEngine()).getConnection()
								.saveFilterByClient(sbf.toString());

						isordercheck = false;
					}

					setState(false);

					((IEQTradeApp) apps).getTradingEngine().refreshData(
							checkMaster.isSelected(),
							checkAccount.isSelected(), false,
							checkPF.isSelected(), isordercheck);

				}
			}
		});
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});

		btnDelete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String sdata = (String) listClient.getSelectedValue();
				if (sdata != null && !sdata.isEmpty()) {
					String tradingid = sdata.split("-")[0].trim();
					Account acc = getAccountId(tradingid);

					((DefaultListModel) listClient.getModel())
							.removeElement(sdata);

					(((TradingEngine) ((IEQTradeApp) apps).getTradingEngine()))
							.getConnection().deleteFilterByClient(
									acc.getAccId());

				}
			}
		});

		btnAdd.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent evt) {
				String tradingid = (String) comboClient.getText().toUpperCase()
						.trim();
				Account acc = getAccountId(tradingid);

				if (tradingid != null && !tradingid.isEmpty() && acc != null) {

					if (((DefaultListModel) listClient.getModel()).size() <= 20) {

						String sdata = tradingid + " - " + acc.getName();
						if (!((DefaultListModel) listClient.getModel())
								.contains(sdata)) {
							((DefaultListModel) listClient.getModel())
									.addElement(sdata);

							(((TradingEngine) ((IEQTradeApp) apps)
									.getTradingEngine())).getConnection()
									.addFilterByClient(acc.getAccId());

						}
					} else {
						Utils.showMessage("Maximum add client", null);
					}
				}
			}
		});

		pnlContent = new JSkinPnl();
		FormLayout layoutContent = new FormLayout(
				"pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,fill:pref:grow,2dlu",
				"pref, 2dlu, pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref");
		CellConstraints cc = new CellConstraints();
		PanelBuilder pnlBuilder = new PanelBuilder(layoutContent, pnlContent);
		pnlBuilder.setBorder(new EmptyBorder(4, 4, 4, 4));
		pnlBuilder.add(checkMaster, cc.xywh(1, 1, 5, 1));
		pnlBuilder.add(checkAccount, cc.xywh(1, 3, 5, 1));
		pnlBuilder.add(checkPF, cc.xywh(1, 5, 5, 1));
		pnlBuilder.add(checkOrder, cc.xywh(1, 7, 5, 1));

		pnlSuperuserClient = new JPanel();
		pnlSuperuserClient.setOpaque(false);

		FormLayout layoutContent2 = new FormLayout(
				"pref,2dlu,75px,2dlu,pref,2dlu,pref,2dlu,pref",
				"2dlu,pref, 2dlu,pref,150px");
		// CellConstraints cc2 = new CellConstraints();
		PanelBuilder pnlBuilder2 = new PanelBuilder(layoutContent2,
				pnlSuperuserClient);
		pnlBuilder2.setBorder(new EmptyBorder(0, 0, 0, 0));
		pnlBuilder2.add(lblClient, cc.xy(1, 2));
		pnlBuilder2.add(comboClient, cc.xy(3, 2));
		pnlBuilder2.add(btnAdd, cc.xy(5, 2));
		pnlBuilder2.add(btnDelete, cc.xy(7, 2));
		pnlBuilder2.add(grScrPane, cc.xywh(1, 4, 9, 2));
		// pnlBuilder2.add(new EmptyBorder(1, 1,1, 1, 1),cc.xy(9, 9));

		pnlBuilder.add(pnlSuperuserClient, cc.xyw(1, 9, 9));
		pnlBuilder.add(lblStatus, cc.xywh(1, 11, 5, 1));
		pnlBuilder.add(btnOK, cc.xy(1, 13));
		pnlBuilder.add(btnCancel, cc.xy(3, 13));

		setStatusfilterClient(checkOrder.isSelected());

		loadData();

		/*
		 * comboClient.getTextEditor().addFocusListener(new FocusAdapter() {
		 * 
		 * @Override public void focusGained(FocusEvent e) {
		 * comboClient.showPopup(); }
		 * 
		 * 
		 * 
		 * 
		 * });
		 */

		comboClient.getTextEditor().addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent arg0) {
				comboClient.showPopup();
			}

		});

		// registerEvent(comboClient.getTextEditor());
		registerEventCombo(comboClient);
	}

	private void registerEventCombo(JDropDown comp) {
		comp.getEditor().getEditorComponent()
				.setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(
				JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap()
				.put("enterAction", new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(comboClient.getTextEditor())) {
							comboClient.setSelectedIndex(comboClient
									.getSelectedIndex());
							comboClient.hidePopup();

						}

					}

				});

		comp.getTextEditor().getActionMap()
				.put("tabAction", new AbstractAction("tabAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(comboClient.getTextEditor())) {
							comboClient.setSelectedIndex(comboClient
									.getSelectedIndex());
							comboClient.hidePopup();
						}
					}

				});

	}

	private void setStatusfilterClient(boolean b) {
		pnlSuperuserClient.setEnabled(b);
		lblClient.setEnabled(b);
		comboClient.setEnabled(b);
		btnAdd.setEnabled(b);
		btnDelete.setEnabled(b);
		listClient.setEnabled(b);

	}

	private void loadData() {
		if (isSuperUser()) {
			/*
			 * String sfile = "refresh-superuser-" + (((TradingEngine)
			 * ((IEQTradeApp) apps).getTradingEngine()))
			 * .getConnection().getUserId() + ".dat"; Object[] obj = (Object[])
			 * Utils.readFile("data/config/" + sfile); ((DefaultListModel)
			 * listClient.getModel()).clear(); if (obj != null) { for (int i =
			 * 0; i < obj.length; i++) ((DefaultListModel)
			 * listClient.getModel()) .addElement(obj[i].toString()); }
			 */

			String sfilter = (((TradingEngine) ((IEQTradeApp) apps)
					.getTradingEngine())).getConnection().loadFilterByClient();

			//log.info("filter " + sfilter);

			if (sfilter != null && !sfilter.isEmpty()) {

				String[] sp = sfilter.split("#");
				((DefaultListModel) listClient.getModel()).clear();

				for (String ss : sp) {

					Account acc = getAccountIdByAccid(ss);

					((DefaultListModel) listClient.getModel()).addElement(acc
							.getTradingId() + " - " + acc.getName());
				}
			}

		}
	}

	Account getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { alias },
						new int[] { Account.C_TRADINGID });
		return (acc != null ? acc : null);
	}

	Account getAccountIdByAccid(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { alias },
						new int[] { Account.C_ACCID });
		return (acc != null ? acc : null);
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg("Refresh data...");
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.setAlwaysOnTop(true);
		frame.setResizable(false);
		frame.setContent(pnlContent);
		frame.pack();
	}

	public void failed(String reason) {
		setStatus(reason);
		setState(true);
	}

	public void success() {
		close();
	}

	public void setStatus(final String status) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (lblStatus != null)
					lblStatus.setText(status);
			}
		});
	}

	@Override
	public void saveSetting() {
		// hSetting.put("size", new Rectangle(frame.getX(), frame.getY(),
		// frame.getWidth(), frame.getHeight()));
		// config.getPreferences().setSetting((String)getProperty(C_PROPERTY_NAME),
		// hSetting);
		// log.info("save " + isSuperUser());
		/*
		 * if (isSuperUser()) { String sfile = "data/config/refresh-superuser-"
		 * + (((TradingEngine) ((IEQTradeApp) apps).getTradingEngine()))
		 * .getConnection().getUserId() + ".dat"; Object[] objs = (Object[])
		 * ((DefaultListModel) listClient .getModel()).toArray();
		 * Utils.writeFile(sfile, objs); }
		 */

	}

	@Override
	public void loadSetting() {

	}

	private boolean isSuperUser() {
		// return true;
		if (((IEQTradeApp) apps) != null) {
			UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_USERPROFILE)
					.getDataByField(new Object[] { "40" },
							new int[] { UserProfile.C_MENUID });
			return sales != null ? sales.getProfileId().toUpperCase()
					.equals("SUPERUSER") : false;
		}
		return false;
		// return false;
	}

	@Override
	public void show() {
		Utils.showToCenter(frame);
		boolean superuser = isSuperUser();
		// superuser = true;
		/*
		 * checkMaster.setVisible(!superuser);
		 * checkAccount.setVisible(!superuser);
		 * checkOrder.setVisible(!superuser); checkPF.setVisible(!superuser);
		 * fieldClient.setVisible(superuser); lblClient.setVisible(superuser);
		 */
		checkMaster.setVisible(true);
		checkAccount.setVisible(true);
		checkOrder.setVisible(true);
		checkPF.setVisible(true);
		// fieldClient.setVisible(false);
		// lblClient.setVisible(false);
		if (!superuser) {
			pnlSuperuserClient.setVisible(false);
		}

		frame.setVisible(true);

	}

	@Override
	public void show(Object param) {
		show();
	}

	@Override
	public void hide() {
		saveSetting();
		frame.setVisible(false);
	}

	@Override
	public void close() {
		saveSetting();
		if (frame != null)
			frame.dispose();
		frame = null;
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}
}
