package eqtrade.trading.ui.core;

import java.io.*;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.UserProfile;

public final class UIColour extends UI {
	private JSkinDlg frame;
	private JButton btnOK;
	private JButton btnCancel;
	private JButton btnDeleteSession;
	private JButton btnDeleteStatic;
	
	
	private JText tBack = new JText(false);
	private JText tBack2 = new JText(false);
	private JText tNormal = new JText(false);
	private JText tUp = new JText(false);
	private JText tDown = new JText(false);
	private JText tFlat = new JText(false);
	private JText tBuy = new JText(false);
	private JText tSell = new JText(false);
	private JText tOther = new JText(false);

	private JDropDown cbFont = new JDropDown();
	private JDropDown cbStyle = new JDropDown();
	private JNumber textSize = new JNumber(Integer.class, 0, 0, true);

	private JCheckBox checkPopup = new JCheckBox(
			"always show popup order status");
	private JCheckBox soundcheck = new JCheckBox(
			"alert sound when order match");
	private JCheckBox checkAnnouncement = new JCheckBox(
	"always show announcement");

	private JDialog dialog = null;
	private ActionListener okListener = null;
	private JColorChooser colorChooser = new JColorChooser();
	private int numChooser = 1;
	private static final String FILE_DIR = "data\\session";
	private static final String FILE_TEXT_EXT = ".cache";

	public UIColour(String app) {
		super("Change Trading Colour", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_COLOUR);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			}
		});
	}

	@Override
	protected void build() {
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		String[] strFontList = ge.getAvailableFontFamilyNames();
		
		for (int i = 0; i < strFontList.length; i++) {
			cbFont.addItem((strFontList[i]).toLowerCase());
		}
		cbStyle.addItem(new String("Plain"));
		cbStyle.addItem(new String("Bold"));
		cbStyle.addItem(new String("Italic"));

		textSize.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				textSize.selectAll();
			}

			@Override
			public void focusLost(FocusEvent e) {
				try {
					textSize.commitEdit();
					if (((Integer) textSize.getValue()).intValue() <= 0) {
						textSize.setValue(new Integer(12));
					}
				} catch (Exception ex) {
					textSize.setValue(new Integer(12));
				}
			}
		});

		textSize.setHorizontalAlignment(SwingConstants.RIGHT);
		
		btnDeleteStatic = new JButton("Delete Static");
		btnOK = new JButton("OK");
		btnOK.setMnemonic('O');
		btnOK.addActionListener(new java.awt.event.ActionListener() { 
			@Override
			public void actionPerformed(ActionEvent e) {
				btnOKAction();
			}
		});
		btnCancel = new JButton("Cancel");
		btnCancel.setMnemonic('C');
		btnCancel.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				hide();
			}
		});
		btnDeleteSession = new JButton("Delete Session");
		btnDeleteSession.addActionListener(new java.awt.event.ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				
				File file = new File("data\\session");
				File file1 = new File("data\\static");
				
				deleteCache(file);
				deleteCache(file1);
				JOptionPane.showMessageDialog(null, "Delete Session & Static Berhasil", "Delete Session", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		registerEvent(btnOK);
		registerEvent(btnCancel);

		JSkinPnl pnlEntry = new JSkinPnl();
		FormLayout layoutEntry = new FormLayout(
				"pref, 2dlu, 100px,2dlu,pref, 2dlu, 100px,2dlu,pref, 2dlu, 100px",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref,2dlu,pref,20dlu");
		PanelBuilder pnlBuilder = new PanelBuilder(layoutEntry, pnlEntry);
		pnlBuilder.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		pnlBuilder.add(new JLabel("Back 1"), cc.xy(1, 1));
		pnlBuilder.add(tBack, cc.xy(3, 1));
		pnlBuilder.add(new JLabel("Back 2"), cc.xy(5, 1));
		pnlBuilder.add(tBack2, cc.xy(7, 1));
		pnlBuilder.add(new JLabel("Normal"), cc.xy(9, 1));
		pnlBuilder.add(tNormal, cc.xy(11, 1));
		pnlBuilder.add(new JLabel("Up"), cc.xy(1, 3));
		pnlBuilder.add(tUp, cc.xy(3, 3));
		pnlBuilder.add(new JLabel("Down"), cc.xy(5, 3));
		pnlBuilder.add(tDown, cc.xy(7, 3));
		pnlBuilder.add(new JLabel("Flat"), cc.xy(9, 3));
		pnlBuilder.add(tFlat, cc.xy(11, 3));
		pnlBuilder.add(new JLabel("Buy"), cc.xy(1, 5));
		pnlBuilder.add(tBuy, cc.xy(3, 5));
		pnlBuilder.add(new JLabel("Sell"), cc.xy(5, 5));
		pnlBuilder.add(tSell, cc.xy(7, 5));
		pnlBuilder.add(new JLabel("Other"), cc.xy(9, 5));
		pnlBuilder.add(tOther, cc.xy(11, 5));
		pnlBuilder.add(new JLabel("Font"), cc.xy(1, 7));
		pnlBuilder.add(cbFont, cc.xy(3, 7));
		pnlBuilder.add(new JLabel("Style"), cc.xy(5, 7));
		pnlBuilder.add(cbStyle, cc.xy(7, 7));
		pnlBuilder.add(new JLabel("Size"), cc.xy(9, 7));
		pnlBuilder.add(textSize, cc.xy(11, 7));
		checkPopup.setOpaque(false);
		pnlBuilder.add(checkPopup, cc.xyw(1, 11, 7));
		soundcheck.setOpaque(false);
		pnlBuilder.add(soundcheck, cc.xyw(1, 13, 7));
		checkAnnouncement.setOpaque(false);
		pnlBuilder.add(checkAnnouncement, cc.xyw(1, 15, 7));
		//pnlBuilder.add(btnDeleteSession,cc.xyw(1, 16, 3));

		pnlContent = new JSkinPnl();
		pnlContent.add(pnlEntry, BorderLayout.CENTER);
		pnlContent.add(buildButton(), BorderLayout.SOUTH);
		createEvent();
	}

	private void createMouseListener(final JComponent comp, final int no) {
		comp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (arg0.getClickCount() == 2 && arg0.getButton() == 1) {
					colorChooser.setColor(comp.getBackground());
					numChooser = no;
					dialog.setLocationRelativeTo(frame);
					dialog.setVisible(true);
				}
			}
		});
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.setResizable(false);
		frame.setContent(pnlContent);
		frame.pack();
		frame.setLocation(((Rectangle) hSetting.get("size")).x,
				((Rectangle) hSetting.get("size")).y);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
	}

	private void btnOKAction() {
		save();
		apps.getUI().refresh();
		hide();
	}
	public static void deleteCache(File folder) {
	    File[] files = folder.listFiles();
	    if(files!=null) { //some JVMs return null for empty dirs
	        for(File f: files) {
//	           System.out.println ("-- "+f.getName() );
	           //if(f.getName().endsWith(".cache") ){
	           f.delete();
	           
	           //}
	        }
	    }
	    
	}
	/*public class GenericExtFilter implements FilenameFilter{
		private String ext;
		public GenericExtFilter(String ext){
			this.ext=ext;
		}
		public boolean accept (File dir, String name){
			return (name.endsWith(ext));
		}
	}*/
	private JSkinPnl buildButton() {
		JSkinPnl pnlButton = new JSkinPnl();
		FormLayout layoutButton = new FormLayout(
				"pref:grow, 2dlu, pref, 2dlu, pref, 2dlu, pref", "pref");
		PanelBuilder builderBtn = new PanelBuilder(layoutButton, pnlButton);
		builderBtn.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		builderBtn.add(this.btnOK, cc.xy(5, 1));
		builderBtn.add(this.btnCancel, cc.xy(7, 1));
		builderBtn.add(this.btnDeleteSession, cc.xy(3,1));
		return pnlButton;
	}

	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}

	@Override
	public void hide() {
		saveSetting();
		frame.setVisible(false);
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("size", new Rectangle(20, 20, 600, 400));
		}
	}

	@Override
	public void refresh() {
		if (frame != null) {
			frame.updateUI();
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}

	private void registerEvent(JComponent comm) {
		InputMap inputMap = comm
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		comm.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						btnCancel.doClick();
					}
				});
		comm.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton)
							((JButton) evt.getSource()).doClick();
						else
							btnOK.doClick();
					}
				});
	}

	@Override
	public void saveSetting() {
		hSetting.put("size",
				new Rectangle(frame.getX(), frame.getY(), frame.getWidth(),
						frame.getHeight()));
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	public void setState(boolean state) {
		btnOK.setEnabled(state);
		btnCancel.setEnabled(state);
	}

	@Override
	public void show() {
		load();
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}

	@Override
	public void show(Object param) {
		show();
	}

	private void load() {
		tBack.setBackground(TradingSetting
				.getColor(TradingSetting.C_BACKGROUND));
		tBack2.setBackground(TradingSetting
				.getColor(TradingSetting.C_BACKGROUNDEVEN));
		tNormal.setBackground(TradingSetting
				.getColor(TradingSetting.C_FOREGROUND));
		tUp.setBackground(TradingSetting.getColor(TradingSetting.C_PLUS));
		tDown.setBackground(TradingSetting.getColor(TradingSetting.C_MINUS));
		tFlat.setBackground(TradingSetting.getColor(TradingSetting.C_ZERO));
		tBuy.setBackground(TradingSetting.getColor(TradingSetting.C_BUY));
		tSell.setBackground(TradingSetting.getColor(TradingSetting.C_SELL));
		tOther.setBackground(TradingSetting.getColor(TradingSetting.C_OTHER));
		Font font = TradingSetting.getFont();
		cbFont.setSelectedItem(new String(font.getName().toLowerCase()));
		cbStyle.setSelectedIndex(font.getStyle());
		textSize.setValue(new Integer(font.getSize()));
		checkPopup.setSelected(TradingSetting.getPopup());
		soundcheck.setSelected(TradingSetting.getPlaySound());
		checkAnnouncement.setSelected(TradingSetting.getcAnnouncement());
		
		UserProfile userprof = (UserProfile) ((IEQTradeApp) apps).getTradingEngine().getStore(
				TradingStore.DATA_USERPROFILE).getDataByField(
				new Object[] { "40" }, new int[] { UserProfile.C_MENUID });
		if (userprof.getProfileId().toUpperCase().equals("CUSTOMER")) {
			checkPopup.setEnabled(false);
		}
		else
		{	
			checkPopup.setEnabled(true);
		}
		
	}

	private void save() {
		TradingSetting.putColor(TradingSetting.C_BACKGROUND,
				tBack.getBackground());
		TradingSetting.putColor(TradingSetting.C_BACKGROUNDEVEN,
				tBack2.getBackground());
		TradingSetting.putColor(TradingSetting.C_FOREGROUND,
				tNormal.getBackground());
		TradingSetting.putColor(TradingSetting.C_PLUS, tUp.getBackground());
		TradingSetting.putColor(TradingSetting.C_MINUS, tDown.getBackground());
		TradingSetting.putColor(TradingSetting.C_ZERO, tFlat.getBackground());
		TradingSetting.putColor(TradingSetting.C_BUY, tBuy.getBackground());
		TradingSetting.putColor(TradingSetting.C_SELL, tSell.getBackground());
		TradingSetting.putColor(TradingSetting.C_OTHER, tOther.getBackground());
		String name = (String) cbFont.getSelectedItem();
		int style = cbStyle.getSelectedIndex();
		int size = ((Integer) textSize.getValue()).intValue();
		TradingSetting.putFont(new Font(name, style, size));
		TradingSetting.setPopup(checkPopup.isSelected());
		TradingSetting.setPlaySound(soundcheck.isSelected());
		TradingSetting.setcAnnouncement(checkAnnouncement.isSelected());
	}

	private void createEvent() {
		okListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				switch (numChooser) {
				case 1:
					tBack.setBackground(colorChooser.getColor());
					break;
				case 2:
					tBack2.setBackground(colorChooser.getColor());
					break;
				case 3:
					tNormal.setBackground(colorChooser.getColor());
					break;
				case 4:
					tUp.setBackground(colorChooser.getColor());
					break;
				case 5:
					tDown.setBackground(colorChooser.getColor());
					break;
				case 6:
					tFlat.setBackground(colorChooser.getColor());
					break;
				case 7:
					tBuy.setBackground(colorChooser.getColor());
					break;
				case 8:
					tSell.setBackground(colorChooser.getColor());
					break;
				case 9:
					tOther.setBackground(colorChooser.getColor());
					break;
				default:
					break;
				}
			}
		};
		createMouseListener(tBack, 1);
		createMouseListener(tBack2, 2);
		createMouseListener(tNormal, 3);
		createMouseListener(tUp, 4);
		createMouseListener(tDown, 5);
		createMouseListener(tFlat, 6);
		createMouseListener(tBuy, 7);
		createMouseListener(tSell, 8);
		createMouseListener(tOther, 9);
		dialog = JColorChooser.createDialog(frame, "Pick a Colour", true,
				colorChooser, okListener, null);
		dialog.setModal(true);
	}
}
