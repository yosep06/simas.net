package eqtrade.trading.ui.core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedApplication;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.Utils;

public final class UIReconnect extends UI {
	private JButton btnCancel;
	private JLabel lblInfo;
	private JLabel lblStatus;
	private int count;
	private JSkinDlg frame;

	public UIReconnect(String app) {
		super("Trading Reconnect", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_RECONNECT);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				btnCancel.requestFocus();
			}
		});
	}

	@Override
	public void refresh() {
		if (frame != null) {
			frame.updateUI();
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}

	@Override
	protected void build() {
		count = 1;
		lblInfo = new JLabel("Please wait");
		lblStatus = new JLabel(" ");
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				((TradingApplication) apps).close();
				close();
			}
		});

		pnlContent = new JSkinPnl();
		FormLayout layoutContent = new FormLayout("200px,2dlu,pref, 4dlu",
				"pref, 2dlu, pref");
		CellConstraints cc = new CellConstraints();
		PanelBuilder pnlBuilder = new PanelBuilder(layoutContent, pnlContent);
		pnlContent.setBorder(new EmptyBorder(4, 4, 4, 4));
		pnlBuilder.add(lblInfo, cc.xywh(1, 1, 3, 1));
		pnlBuilder.add(lblStatus, cc.xy(1, 3));
		pnlBuilder.add(btnCancel, cc.xy(3, 3));
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg("Reconnecting...");
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.setAlwaysOnTop(true);
		frame.setResizable(false);
		frame.setContent(pnlContent);
		frame.pack();
	}

	private void doConnect() {
		count++;
		setStatus("Trading reconnecting (" + count + ").. ");
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
				} catch (Exception ex) {
				}
				((IEQTradeApp) apps).getTradingEngine().reconnect(count);
			}
		}).start();
	}

	public void failed(String reason) {
		setStatus(reason);
		doConnect();
	}

	public void success() {
		close();
	}

	public void setStatus(final String status) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				lblStatus.setText(status);
			}
		});
	}

	@Override
	public void saveSetting() {
		// hSetting.put("size", new Rectangle(frame.getX(), frame.getY(),
		// frame.getWidth(), frame.getHeight()));
		// config.getPreferences().setSetting((String)getProperty(C_PROPERTY_NAME),
		// hSetting);
	}

	@Override
	public void loadSetting() {
		// hSetting =
		// (Hashtable)config.getPreferences().getSetting((String)getProperty(C_PROPERTY_NAME));
		// if (hSetting == null) {
		// hSetting = new Hashtable();
		// hSetting.put("size", new Rectangle(20,20,600,400));
		// }
	}

	@Override
	public void show() {
		count = 1;
		Utils.showToCenter(frame);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				doConnect();
			}
		});
		frame.setVisible(true);
	}

	@Override
	public void show(Object param) {
		show();
	}

	@Override
	public void hide() {
		saveSetting();
		frame.setVisible(false);
	}

	@Override
	public void close() {
		saveSetting();
		if (frame != null)
			frame.dispose();
		frame = null;
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}
}
