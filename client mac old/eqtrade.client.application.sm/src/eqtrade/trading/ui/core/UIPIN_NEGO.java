package eqtrade.trading.ui.core;

import java.awt.BorderLayout;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.application.UIMaster;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.ui.UIAmendOrder;
import eqtrade.trading.ui.UIEntryCrossing;
import eqtrade.trading.ui.UIEntryNegdeal;

public final class UIPIN_NEGO extends UI {
	private JSkinDlg frame;
	private JLabel lblPIN;
	private JPasswordField fieldPIN;
	private JButton btnOK;
	private JButton btnCancel;
	private JLabel lblNEGO;
	private JLabel lblResult;
	private JLabel lblStatus;
	private JLabel lblClient;
	private JLabel lblStock;
	private JLabel lblPrice;
	private JLabel lblVolume;
	String pForm;

	public UIPIN_NEGO(String app) {
		super("Trading PIN NEGO", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_PIN_NEGO);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldPIN.requestFocus();
			}
		});
	}

	@Override
	protected void build() {
		fieldPIN = new JPasswordField();
		btnOK = new JButton("OK");
		btnOK.setMnemonic('O');
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnOKAction();
			}
		});
		btnCancel = new JButton("Cancel");
		btnCancel.setMnemonic('C');
		btnCancel.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//((UIEntryNegdeal) apps.getUI().getForm(TradingUI.UI_ENTRYNEGDEAL)).nconfirm = 1;
				//((UIEntryCrossing) apps.getUI().getForm(TradingUI.UI_ENTRYCROSSING)).nconfirm = 1;
				//((UIAmendOrder) apps.getUI().getForm(TradingUI.UI_AMENDORDER)).nconfirm = 1;
				close();
				Global = 0;
			}
		});
		lblNEGO = new JLabel ("are you sure want to send this order?\n");
		lblResult = new JLabel ("");
		lblClient = new JLabel ("");
		lblStock = new JLabel ("");
		lblPrice = new JLabel ("");
		lblVolume = new JLabel ("");
		lblPIN = new JLabel("Enter your Authorization PIN Nego");
		lblPIN.setDisplayedMnemonic('l');
		//lblData.setDisplayedMnemonic('1');
		lblPIN.setLabelFor(fieldPIN);
		registerEvent(fieldPIN);
		registerEvent(btnOK);
		registerEvent(btnCancel);

		JSkinPnl pnlEntry = new JSkinPnl();
		FormLayout layoutEntry = new FormLayout("pref, 2dlu, 25px, pref, 2dlu, pref, 2dlu",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu");
		PanelBuilder pnlBuilder = new PanelBuilder(layoutEntry, pnlEntry);
		pnlBuilder.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		pnlBuilder.add(this.lblNEGO, cc.xy (1,3));
		pnlBuilder.add(this.lblResult, cc.xy (1,5));
		pnlBuilder.add(this.lblClient, cc.xy (1,7));
		pnlBuilder.add(this.lblStock, cc.xy (1,9));
		pnlBuilder.add(this.lblPrice, cc.xy(1,11));
		pnlBuilder.add(this.lblVolume, cc.xy (1,13));
		pnlBuilder.add(this.lblPIN, cc.xy(1,15));
		pnlBuilder.add(this.fieldPIN, cc.xy(1,17));

		pnlContent = new JSkinPnl();
		pnlContent.add(pnlEntry, BorderLayout.CENTER);
		pnlContent.add(buildButton(), BorderLayout.SOUTH);
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.setResizable(false);
		frame.getBtnClose().setVisible(false);
		frame.getBtnMin().setVisible(false);
		frame.setContent(pnlContent);
		frame.pack();
		frame.setLocation(((Rectangle) hSetting.get("size")).x,
				((Rectangle) hSetting.get("size")).y);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
		frame.setAlwaysOnTop(true);		
	}

	private void btnOKAction() {
		//String strPIN = Utils.getMD5(new String(fieldPIN.getPassword()));
		if (new String(fieldPIN.getPassword()).trim().equals("")) {
			JOptionPane.showMessageDialog(frame, "PIN Nego cannot be empty",
					"alert", JOptionPane.ERROR_MESSAGE);
			focus();
			return;
		} else {
			log.info("FIELDPINNNNNNNNNNNNNNN "+new String(fieldPIN.getPassword()));
			((IEQTradeApp) apps).getTradingEngine().checkPIN_NEGO (new String(fieldPIN.getPassword()),pForm);
			setState(false);
			lblPIN.setText("Enter your Authorization PIN Nego");
			//form.setVisible(false);
			System.out.println("FAILEDDD");
		}
	}

	private JSkinPnl buildButton() {
		JSkinPnl pnlButton = new JSkinPnl();
		FormLayout layoutButton = new FormLayout(
				"pref, 2dlu, pref, 2dlu, pref", "pref");
		PanelBuilder builderBtn = new PanelBuilder(layoutButton, pnlButton);
		builderBtn.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		
		builderBtn.add(this.btnOK, cc.xy(1, 1));
		builderBtn.add(this.btnCancel, cc.xy(3, 1));
		return pnlButton;
	}
	
	/*public void doLoginNego() {
		if ((new String(fieldPIN.getPassword()).trim().equals(""))) {
			lblStatus.setText("please enter valid password");
			fieldPIN.requestFocus();
		} else {
			setEnableInfo(false);
			lblStatus.setText("connecting (nego).....");
			String strPwd = Utils
					.getMD5(new String(fieldPIN.getPassword()));
			((IEQTradeApp) apps).getTradingEngine().loginnego, strPwd);
		}
	}*/

	/*private void setEnableInfo(boolean b) {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public void close() {
		saveSetting();
		lblPIN.setText("Enter your Authorization PIN Nego");
		frame.dispose();
		frame = null;
	}
	
	int Global = 0;
	public void failed(String reason) {
		JOptionPane.showMessageDialog(frame, reason, "alert",
				JOptionPane.ERROR_MESSAGE);
		setState(true);
		Global ++;
		if (Global >3){
			hide();
		}else if(Global < 4){
		lblPIN.setText("Enter your Authorization PIN Nego  ("+Global+"/3)");
		}
		else if(Global <4 && Global == 0){
			lblPIN.setText("Enter your Authorization PIN Nego ");
		}
	}
	
	@Override
	public void hide() {
		saveSetting();
		frame.setVisible(false);
		Global = 0;
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("size", new Rectangle(20, 20, 600, 400));
		}
	}

	@Override
	public void refresh() {
		if (frame != null) {
			frame.updateUI();
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}

	private void registerEvent(JComponent comm) {
		InputMap inputMap = comm
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		comm.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						btnCancel.doClick();
					}
				});
		comm.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton)
							((JButton) evt.getSource()).doClick();
						else
							btnOK.doClick();
					}
				});
	}

	@Override
	public void saveSetting() {
		hSetting.put("size",
				new Rectangle(frame.getX(), frame.getY(), frame.getWidth(),
						frame.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	public void setState(boolean state) {
		fieldPIN.setEnabled(state);
		fieldPIN.setText("");
		btnOK.setEnabled(state);
		btnCancel.setEnabled(state);
	}

	public void setStatus(String status) {
		Utils.showMessage(status, frame);
	}

	@Override
	public void show() {
		/*
		 * focus(); Utils.showToCenter(frame); frame.setVisible(true);
		 */
		focus();
		// Utils.showToCenter(frame);
		//frame.setLocation(MouseInfo.getPointerInfo().getLocation().x, MouseInfo
			//	.getPointerInfo().getLocation().y);
		Point p = ((UIMaster)apps.getUIMain()).getLocationLbl();
		
		frame.setLocation(p.x,p.y + 97);
		frame.setVisible(true);		
	}

	@Override
	public void show(Object param) {
		//if()
		HashMap p =  (HashMap) param;
		lblResult.setText((String) p.get("Result"));
		lblClient.setText((String) p.get("Client"));
		lblStock.setText((String) p.get("Stock"));
		lblPrice.setText((String) p.get("Price"));
		lblVolume.setText((String) p.get("Volume"));
		pForm = (String)p.get("form");
		show();
	}

	public void success() {
		((TradingApplication) apps).applySecurity();
		close();
	}

	public void setWait(boolean b) {
		// TODO Auto-generated method stub

	}
}
