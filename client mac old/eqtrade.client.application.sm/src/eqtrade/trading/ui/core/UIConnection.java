package eqtrade.trading.ui.core;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.EQTrade;
import eqtrade.feed.core.Utils;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;

public final class UIConnection extends UI {
	private JSkinDlg frame;
	//private JLabel labelConn1;
	//private JLabel labelConn2;
	private JLabel labelConn3;
	//private JTextField fieldConn1;
	//private JTextField fieldConn2;
	private JTextField fieldConn3;
	private JButton btnOK;
	private JButton btnCancel;
	private JList listTConn;
	private DefaultListModel lModel;
	private JButton btnUp;
	private JButton btnDn;
	private JTextField fieldAdd;
	private JButton btnAdd;
	private JButton btnDel;

	public UIConnection(String app) {
		super("Change Trading Connection", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_CONNECTION);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				//fieldConn1.requestFocus();
				fieldAdd.requestFocus();
			}
		});
	}

	@Override
	protected void build() {
		/*
		fieldConn1 = new JTextField();
		fieldConn1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				fieldConn1.selectAll();
			}
		});
		fieldConn2 = new JTextField();
		fieldConn2.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				fieldConn2.selectAll();
			}
		});
		*/
		fieldConn3 = new JTextField();
		fieldConn3.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				fieldConn3.selectAll();
			}
		});
		btnOK = new JButton("OK");
		btnOK.setMnemonic('O');
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnOKAction();
			}
		});
		btnCancel = new JButton("Cancel");
		btnCancel.setMnemonic('C');
		btnCancel.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		/*
		labelConn1 = new JLabel("Server 1");
		labelConn1.setDisplayedMnemonic('1');
		labelConn1.setLabelFor(fieldConn1);
		labelConn2 = new JLabel("Server 2");
		labelConn2.setDisplayedMnemonic('2');
		labelConn2.setLabelFor(fieldConn2);
		*/
		labelConn3 = new JLabel("Forex");
		labelConn3.setDisplayedMnemonic('F');
		labelConn3.setLabelFor(fieldConn3);

		fieldAdd = new JTextField();
		fieldAdd.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				fieldAdd.selectAll();
			}
		});
		btnAdd = new JButton("Add");
		btnAdd.setMnemonic('A');
		btnAdd.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnAddAction();
			}
		});
		btnDel = new JButton("Del");
		btnDel.setMnemonic('l');
		btnDel.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnDelAction();
			}
		});
		
		lModel = new DefaultListModel();
		listTConn = new JList(lModel);
		btnUp = new JButton("Up");
		btnUp.setMnemonic('U');
		btnUp.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnUpAction();
			}
		});
		btnDn = new JButton("Down");
		btnDn.setMnemonic('D');
		btnDn.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnDnAction();
			}
		});
		
		//registerEvent(fieldConn1);
		//registerEvent(fieldConn2);
		registerEvent(fieldConn3);
		registerEvent(btnOK);
		registerEvent(btnCancel);
		registerEvent(listTConn);
		registerEvent(btnUp);
		registerEvent(btnDn);
		registerEvent(btnAdd);
		registerEvent(btnDel);
		registerEvent(fieldAdd);

		JSkinPnl pnlEntry = new JSkinPnl();
		//FormLayout layoutEntry = new FormLayout("pref, 2dlu, 200px",
		//		"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		FormLayout layoutEntry = new FormLayout("pref, 2dlu, 200px",
				"pref, 2dlu, pref");
		PanelBuilder pnlBuilder = new PanelBuilder(layoutEntry, pnlEntry);
		pnlBuilder.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		pnlBuilder.addSeparator("Enter new connection", cc.xywh(1, 1, 3, 1));
		//pnlBuilder.add(this.labelConn1, cc.xy(1, 3));
		//pnlBuilder.add(this.fieldConn1, cc.xy(3, 3));
		//pnlBuilder.add(this.labelConn2, cc.xy(1, 5));
		//pnlBuilder.add(this.fieldConn2, cc.xy(3, 5));
		//pnlBuilder.add(this.labelConn3, cc.xy(1, 7));
		//pnlBuilder.add(this.fieldConn3, cc.xy(3, 7));
		pnlBuilder.add(this.labelConn3, cc.xy(1, 3));
		pnlBuilder.add(this.fieldConn3, cc.xy(3, 3));

		JSkinPnl pnlUpdown = new JSkinPnl();
		FormLayout layoutUpdown = new FormLayout("200px, 2dlu, pref",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		PanelBuilder pbList = new PanelBuilder(layoutUpdown, pnlUpdown);
		pbList.setDefaultDialogBorder();
		CellConstraints ce = new CellConstraints();
		pbList.addSeparator("Setup trading connection", ce.xywh(1, 1, 3, 1));
		pbList.add(this.fieldAdd, ce.xy(1, 3));
		pbList.add(this.btnAdd, ce.xy(3, 3));
		pbList.add(this.listTConn, ce.xywh(1, 5, 1, 5));
		pbList.add(this.btnUp, ce.xy(3, 5));
		pbList.add(this.btnDn, ce.xy(3, 7));
		pbList.add(this.btnDel, ce.xy(3, 9));
		
		//this.fieldConn1.setEnabled(false);
		//this.fieldConn2.setEnabled(false);
		this.fieldConn3.setEnabled(false);

		pnlContent = new JSkinPnl();
		pnlContent.add(pnlUpdown, BorderLayout.CENTER);
		//pnlContent.add(pnlEntry, BorderLayout.CENTER);
		pnlContent.add(buildButton(), BorderLayout.SOUTH);
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.setResizable(false);
		frame.setContent(pnlContent);
		frame.pack();
		frame.setLocation(((Rectangle) hSetting.get("size")).x,
				((Rectangle) hSetting.get("size")).y);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
	}

	private void btnOKAction() {
		save();
		close();
		/*
		if (fieldConn1.getText().trim().length() > 0
				&& fieldConn2.getText().trim().length() > 0) {
			save();
			close();
		} else {
			JOptionPane.showMessageDialog(frame,
					"please enter valid connection", "alert",
					JOptionPane.ERROR_MESSAGE);
			focus();
		}
		*/
	}

	private void btnUpAction() {
		if (!listTConn.isSelectionEmpty()) {
			int dipilih = listTConn.getSelectedIndex();
			if (dipilih>0) {
				int iatas = dipilih--;
				String satas = (String) lModel.elementAt(iatas);
				String sbawah = (String) lModel.elementAt(dipilih);
				lModel.set(iatas, sbawah);
				lModel.set(dipilih, satas);
				listTConn.setSelectedValue(satas, true);
			}
		}
	}
	
	private void btnDnAction() {
		if (!listTConn.isSelectionEmpty()) {
			int dipilih = listTConn.getSelectedIndex();
			if (dipilih<lModel.getSize()-1) {
				int ibawah = dipilih++;
				String satas = (String) lModel.elementAt(dipilih);
				String sbawah = (String) lModel.elementAt(ibawah);
				lModel.set(ibawah, satas);
				lModel.set(dipilih, sbawah);
				listTConn.setSelectedValue(sbawah, true);
			}
		}
	}

	private void btnAddAction() {
		if (fieldAdd.getText().trim().length() > 0) {
			int gd = lModel.getSize();
			lModel.add(gd, fieldAdd.getText().trim());
			fieldAdd.setText("");
			fieldAdd.requestFocus();
		}
	}
	
	private void btnDelAction() {
		if (!listTConn.isSelectionEmpty()) {
			int dipilih = listTConn.getSelectedIndex();
			lModel.remove(dipilih);
		}
	}

	private JSkinPnl buildButton() {
		JSkinPnl pnlButton = new JSkinPnl();
		FormLayout layoutButton = new FormLayout(
				"pref:grow, 2dlu, pref, 2dlu, pref", "pref");
		PanelBuilder builderBtn = new PanelBuilder(layoutButton, pnlButton);
		builderBtn.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		builderBtn.add(this.btnOK, cc.xy(3, 1));
		builderBtn.add(this.btnCancel, cc.xy(5, 1));
		return pnlButton;
	}

	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}

	@Override
	public void hide() {
		saveSetting();
		frame.setVisible(false);
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("size", new Rectangle(20, 20, 600, 400));
		}
	}

	@Override
	public void refresh() {
		if (frame != null) {
			frame.updateUI();
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}

	private void registerEvent(JComponent comm) {
		InputMap inputMap = comm
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		comm.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						btnCancel.doClick();
					}
				});
		comm.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton)
							((JButton) evt.getSource()).doClick();
						else
							btnOK.doClick();
					}
				});
	}

	@Override
	public void saveSetting() {
		hSetting.put("size",
				new Rectangle(frame.getX(), frame.getY(), frame.getWidth(),
						frame.getHeight()));
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	public void setState(boolean state) {
		//fieldConn1.setEnabled(state);
		//fieldConn2.setEnabled(state);
		fieldConn3.setEnabled(state);
		btnOK.setEnabled(state);
		btnCancel.setEnabled(state);
		listTConn.setEnabled(state);
		btnUp.setEnabled(state);
		btnDn.setEnabled(state);
		fieldAdd.setEnabled(state);
		btnAdd.setEnabled(state);
		btnDel.setEnabled(state);
	}

	@Override
	public void show() {
		load();
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}

	@Override
	public void show(Object param) {
		show();
	}

	private void load() {
		try {
			Object o = com.vollux.framework.Utils
					.readFile("data/config/trading.dat");

			if (o != null) {
				Vector v = (Vector) o;
				int mode=0;
				for (int i=0; i<v.size(); i++) {
					try {
						if (mode==0) lModel.add(i, (String) v.elementAt(i));
						if (mode==1) fieldConn3.setText((String) v.elementAt(i));
					} catch (Exception ex) {
						mode++;
					}
				}
			}
			/*
			if (o != null) {
				Vector v = (Vector) o;
				fieldConn1.setText((String) v.elementAt(0));
				fieldConn2.setText((String) v.elementAt(1));
				if (v.size() > 3) {
					fieldConn3.setText((String) v.elementAt(3));
				}
			}
			*/
		} catch (Exception ex) {

		}
	}

	private void save() {
		/*
		Vector v = new Vector(3);
		v.addElement(fieldConn1.getText());
		v.addElement(fieldConn2.getText());
		*/
		Vector v = new Vector(lModel.getSize());
		for (int i=0; i<lModel.getSize(); i++) {
			//log.info("lmodel "+lModel.elementAt(i).toString());
			v.addElement((String) lModel.elementAt(i));
		}
		//v.addElement(new Integer(1000));
		//v.addElement(fieldConn3.getText());
		com.vollux.framework.Utils.writeFile("data/config/trading.dat", v);
	}
}
