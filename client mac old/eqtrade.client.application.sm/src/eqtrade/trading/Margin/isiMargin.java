package eqtrade.trading.Margin;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class isiMargin  implements Printable {
	private java.util.Date currDate = new java.util.Date ();
	private SimpleDateFormat sdf = new SimpleDateFormat ("EEE, dd MMMM yyyy,  HH:mm:ss", Locale.getDefault());
	private String d = sdf.format (currDate) ;
	int[] pageBreaks ;
	@Override
	public int print(Graphics grapihcs, PageFormat pageFormat, int pageIndex )
			throws PrinterException {
		
		
		try{
			pageFormat.setOrientation(PageFormat.LANDSCAPE);
			Graphics2D g= (Graphics2D) grapihcs;
			Graphics2D gg= (Graphics2D) grapihcs;
			gg.setFont(new Font("Tahoma", Font.BOLD, 9));
			g.setFont(new Font("Tahoma", Font.PLAIN,8));
			g.setColor(Color.black);
//baru
	        Font font = new Font("Tahoma", Font.ITALIC,8);
	        java.awt.FontMetrics metrics = g.getFontMetrics(font);
	        
	        int lineHeight = metrics.getHeight();
	        System.out.println("lineheight "+lineHeight+" imageheight "+pageFormat.getImageableHeight());
			if (pageBreaks == null) {
	        //    initTextLines();
	            int linesPerPage = (int)((pageFormat.getImageableHeight()-350)/lineHeight);
	            int numBreaks = (PrintMargin.getString().length-1)/linesPerPage;
	            System.out.println("numbreak:"+numBreaks+" lineperPage:"+linesPerPage);
	            pageBreaks = new int[numBreaks];
	            for (int b=0; b<numBreaks; b++) {
	                pageBreaks[b] = (b+1)*linesPerPage; 
	            }
	        }

	        if (pageIndex > pageBreaks.length) {
	            return NO_SUCH_PAGE;
	        }
			int i1,x,y,z;
			
			g.drawString("Margin Monitoring",30,50);

			g.drawLine(20,62,780,62);
			//g.drawLine(20, 70, 20, 165);
			//g.drawLine(20, 70, 20, 165);
			gg.drawString("NO. ",25,71);
			gg.drawString("ACC ID. ",40,71);
			gg.drawString("NAMA NASABAH.",90,71);
			gg.drawString("CURRENT RATIO.",210,71);
			gg.drawString("CREDIT LIMIT.",280,71);
			gg.drawString("NET AC.",350,71);
			gg.drawString("LQ VALUE.",430,71);
			gg.drawString("MARKET VALUE.",505,71);
			gg.drawString("CURRENT TL.",570,71);
			gg.drawString("TOP UP.", 640,71);
			gg.drawString("FORCE SELL.",710,71);
			gg.drawLine(20,75,780,75);
			x = 20;
			y = 85;
			i1= 1;
			z = 90;
			
			int[] xB = {40,90,240,280,350,430,505,575,640,710};
			int  i = PrintMargin.getString().length;
			System.out.println ("Page Index "+pageIndex+"- Page Break "+pageBreaks.length);
			System.out.println("length = "+i);

	        int start = (pageIndex == 0) ? 0 : pageBreaks[pageIndex-1];
	        int end   = (pageIndex == pageBreaks.length)
	                         ? PrintMargin.getString().length : pageBreaks[pageIndex];
			 for(int  a = start;a<end;a++){

				 g.drawString(""+(a+1)+".",25,y); 
				 for(int c=0;c<10;c++){
					 g.drawString(PrintMargin.getString()[a][c],xB[c],y) ;
						System.out.println(PrintMargin.getString()[a][c]);
						
				 }
				 g.drawLine(x,z,780,z);
				 	z +=14;
					y += 14;
					i1++;
					
			/////////
			 }
			g.drawLine(x,y+2,780,y+1);
			//g.drawLine(x,y,780,y+9);
			g.drawLine(780, 62, 780, y+1);
			g.drawLine(20, 62, 20, y+2);
			if(pageIndex==pageBreaks.length){
				g.drawString("Jumlah Credit Limit :     "+PrintMargin.getJmlhCL(), 195, y-1);
			}
			g.drawString("Page "+(((Integer)pageIndex)+1)+" of "+(((Integer)pageBreaks.length)+1), 710, y+14);
			
			g.drawString("Tanggal Cetak : "+d,20, y+14);			
			return PAGE_EXISTS;
			}catch (Exception e) {
		}
		return PAGE_EXISTS;
	}
	
}
