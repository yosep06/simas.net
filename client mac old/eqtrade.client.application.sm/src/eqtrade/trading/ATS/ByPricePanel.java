package eqtrade.trading.ATS;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Hashtable;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.block.EmptyBlock;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.sun.istack.internal.Builder;
import com.vollux.framework.IApp;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinForm;
import com.vollux.ui.JText;
import com.vollux.ui.JTextLabel;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.StockSummary;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.model.ScheduleDef;
import feed.admin.Utils;
public class ByPricePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static JRadioButton radioNone;
	static JRadioButton radioRemaining;
	static JRadioButton radioCumulative;
	public static JPanel pricePanel;
	
	static JNumber fieldStopPrice;
	static JNumber fieldProfitPrice;
	JNumber fieldDefault;
	static JNumber fieldRemainingLot;
	static JNumber fieldCumulative;
	
	JNumber fieldCompare;
	static JTextField fieldSchedule;
	
	static JRadioButton checkStopLost;
	static JRadioButton checkProfitTaking;
	static JCheckBox checkSchedule;
	private IApp apps;
	private JSkinForm form;
	private Hashtable hSetting;
	static JTextLabel lblprofit;
	static JTextLabel lblstop;
	public ByPricePanel(IApp apps, JSkinForm form, Hashtable hSetting){
		super(new BorderLayout());
		this.apps = apps;
		this.form = form;
		this.hSetting = hSetting;
	}
	
	protected void build(){
		radioNone 			= new JRadioButton("None");									radioNone.setOpaque(false);
		radioRemaining 		= new JRadioButton("Remaining Bid Lot At Price");			radioRemaining.setOpaque(false);
		radioCumulative 	= new JRadioButton("Cumulative Done(Since Order Given)");	radioCumulative.setOpaque(false);
		fieldStopPrice 		= new JNumber(Double.class,0,0,false,true);
		fieldDefault 		= new JNumber(Double.class,0,0,false,false);
		fieldRemainingLot 	= new JNumber(Double.class,0,0,false,true);
		fieldCumulative 	= new JNumber(Double.class,0,0,false,true);
		fieldProfitPrice	= new JNumber(Double.class,0,0,false,true);
		fieldSchedule		= new JTextField();
		
		checkStopLost 		= new JRadioButton("Compare Last Price <=");		checkStopLost.setOpaque(false);
		checkProfitTaking 	= new JRadioButton("Compare Last Price >=");		checkProfitTaking.setOpaque(false);
		checkSchedule 		= new JCheckBox("Schedule Label");					checkSchedule.setOpaque(false);
		lblprofit			= new JTextLabel("Last Price >=");
		lblstop				= new JTextLabel("Last Price <=");
		
		ButtonGroup group = new ButtonGroup();
		group.add(radioCumulative);
		group.add(radioNone);
		group.add(radioRemaining);
		
		fieldStopPrice.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				fieldDefault.setText(fieldStopPrice.getText());				
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub				
			}
		});
		fieldSchedule.setEnabled(false);
		checkSchedule.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(checkSchedule.isSelected())
					fieldSchedule.setEnabled(true);
				else fieldSchedule.setEnabled(false);
			}
		});
		//ats limit price 10022016
		fieldProfitPrice.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
//				if (!((UINewSchedule)apps.getUI().getForm(TradingUI.UI_NEWSCHEDULE)).validPrice((Double)fieldProfitPrice.getValue())) {
//					fieldProfitPrice.setValue(null);
//					fieldProfitPrice.requestFocus();
//				};
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		fieldStopPrice.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				// TODO Auto-generated method stub
//				if (!((UINewSchedule)apps.getUI().getForm(TradingUI.UI_NEWSCHEDULE)).validPrice((Double)fieldStopPrice.getValue())) {
//					fieldStopPrice.setValue(null);
//					fieldStopPrice.requestFocus();
//					fieldDefault.setValue(null);
//				}
				
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		ButtonGroup check = new ButtonGroup();
		check.add(checkProfitTaking);
		check.add(checkStopLost);
		
		checkProfitTaking.addActionListener(new Selected());
		checkStopLost.addActionListener(new Selected());
		radioNone.addActionListener(new Selected());
		radioCumulative.addActionListener(new Selected());
		radioRemaining.addActionListener(new Selected());
		
		
		checkStopLost.setSelected(true);
		radioNone.setSelected(true);
		fieldCumulative.setEnabled(false);
		fieldRemainingLot.setEnabled(false);
		actStopLost(true);
		actProfit(false);		
		
		pricePanel = new JPanel();
		pricePanel.setBackground(Color.red.darker());
		FormLayout pricelayout = new FormLayout(
				"20px,80px,50px,30px,50px,20px,50px,80px,20px,130px,100px",
				//"pref,2dlu,20px,2dlu,pref,2dlu,50px, 2dlu, pref, 2dlu, 50px , 2dlu, pref, 2dlu, 20px, pref, 2dlu, 50px, pref, 2dlu, 80px",
				"pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref");
		PanelBuilder priceBuilder = new PanelBuilder(pricelayout,pricePanel);
		CellConstraints cc = new CellConstraints();
		priceBuilder.setBorder(new EmptyBorder(4,4,4,4));
		priceBuilder.add(checkStopLost,cc.xyw(1, 1,3));
		priceBuilder.addSeparator("",cc.xyw(5,1,7));
		priceBuilder.add(lblstop,cc.xyw(2, 3,3));
		priceBuilder.add(fieldStopPrice,cc.xyw(3, 3,2));
		priceBuilder.add(new JTextLabel("  IDR"),cc.xy(5, 3));
		priceBuilder.add(radioNone,cc.xy(2, 5));
		priceBuilder.add(radioRemaining,cc.xyw(2, 7,3));
		priceBuilder.add(fieldDefault,cc.xy(5, 7));
		priceBuilder.add(new JTextLabel("<="),cc.xy(6,7));
		priceBuilder.add(fieldRemainingLot,cc.xy(7, 7));
		priceBuilder.add(new JTextLabel("Lot"),cc.xy(8, 7));
		priceBuilder.add(radioCumulative,cc.xyw(2, 9,4));
		priceBuilder.add(new JTextLabel(">="),cc.xy(6, 9));
		priceBuilder.add(fieldCumulative,cc.xy(7, 9));
		priceBuilder.add(new JTextLabel("Lot"),cc.xy(8, 9));
		priceBuilder.addSeparator("",cc.xyw(5,11,7));
		priceBuilder.add(checkProfitTaking,cc.xyw(1, 11,3));
		priceBuilder.add(lblprofit,cc.xyw(2, 13,3));
		priceBuilder.add(fieldProfitPrice,cc.xyw(3, 13,2));
		priceBuilder.add(new JTextLabel("  IDR"),cc.xy(5, 13));
		priceBuilder.add(checkSchedule,cc.xyw(1, 17,2));
		priceBuilder.addSeparator("",cc.xyw(1,15,8));
		priceBuilder.add(fieldSchedule,cc.xy(3, 17));	
		this.add(pricePanel);				
	}
	
	public String send(){
		String send = "" ;
		if(checkStopLost.isSelected()){
			if (radioNone.isSelected()) {
				send="STOP PRICE <= "+fieldStopPrice.getText();	
			}else if (radioRemaining.isSelected()) {
				send="Remaining "+fieldDefault.getText()+"<="+fieldRemainingLot.getText();
			}else if (radioCumulative.isSelected()) {
				send="cumulative >="+fieldCumulative.getText();
			}
		}
		if (checkProfitTaking.isSelected()) {
			send=send+" PROFIT >="+fieldProfitPrice.getText();
		}		
		return send;
	}
	
	public void clean(){
		fieldStopPrice.setValue(null);
		fieldDefault.setValue(null);
		fieldRemainingLot.setValue(null);
		fieldCumulative.setValue(null);
		fieldProfitPrice.setValue(null);
		fieldSchedule.setText("");
	}
	static void actStopLost(boolean ac){
		fieldStopPrice.setEnabled(ac);
		fieldRemainingLot.setEnabled(ac);
		fieldCumulative.setEnabled(ac);
		radioNone.setEnabled(ac);
		radioCumulative.setEnabled(ac);
		radioRemaining.setEnabled(ac);
		
	}
	static void actProfit(boolean ac){
		fieldProfitPrice.setEnabled(ac);
	}
	static String check(){
		String error = null ;
		if(checkStopLost.isSelected()){
			if(fieldStopPrice.getText().isEmpty())
				error = "Please enter Compare last price";
			if(radioRemaining.isSelected()){
				if(fieldRemainingLot.getText().isEmpty())
					error = "Please enter Remaining Lot";
			}
			if(radioCumulative.isSelected()){
				if(fieldCumulative.getText().isEmpty())
					error = "Please enter Cumulative Lot";
			}
		}
		if(checkProfitTaking.isSelected()){
			if(fieldProfitPrice.getText().isEmpty())
				error = "Please enter Profit Last Price";
		}
		if(checkSchedule.isSelected()){
			if(fieldSchedule.getText().isEmpty())
				error = "Please enter Schedule";
		}
		//System.out.println(error);
		return error;
		
	}
	class Selected implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent a) {
			// TODO Auto-generated method stub
			if(a.getSource() == checkProfitTaking){
				actProfit(true);
				actStopLost(false);
			} else if(a.getSource() == checkStopLost){
				actProfit(false);
				actStopLost(true);
			} else if(a.getSource() == radioNone){
				fieldCumulative.setEnabled(false);
				fieldRemainingLot.setEnabled(false);
			} else if(a.getSource() == radioCumulative){
				fieldCumulative.setEnabled(true);
				fieldRemainingLot.setEnabled(false);
			}else if(a.getSource() == radioRemaining){
				fieldCumulative.setEnabled(false);
				fieldRemainingLot.setEnabled(true);
			}
		}
		
	}
}
