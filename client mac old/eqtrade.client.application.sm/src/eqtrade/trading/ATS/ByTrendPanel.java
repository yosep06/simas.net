package eqtrade.trading.ATS;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.rmi.UnexpectedException;
import java.util.Hashtable;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.IApp;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinForm;
import com.vollux.ui.JText;
import com.vollux.ui.JTextLabel;
import eqtrade.trading.ATS.UINewSchedule;
import eqtrade.trading.app.TradingUI;
public class ByTrendPanel extends JPanel{
	
	public static JRadioButton radioUpTrend;
	public static JRadioButton radioDownTrend;
	public static JPanel trendPanel;
	static JNumber fieldUpCompare ;
	static JNumber fieldDownCompare;
	static JNumber fieldReverseTop;
	static JNumber fieldReverseButton;
	static JTextField fieldSchedule;
	static JCheckBox checkSchedule;
	private IApp apps;
	private JSkinForm form;
	private Hashtable hSetting;
	
	public ByTrendPanel(IApp apps, JSkinForm form, Hashtable hSetting){
		super(new BorderLayout());
		this.apps = apps;
		this.form = form;
		this.hSetting = hSetting;
	}
	
	protected void build(){
		radioUpTrend = new JRadioButton("Up Trend Reversal"); 			radioUpTrend.setOpaque(false);
		radioDownTrend = new JRadioButton("Down Trend Reversal");		radioDownTrend.setOpaque(false);
		fieldUpCompare = new JNumber(Double.class, 0,0, false,true);
		fieldReverseTop =new JNumber(Double.class, 0,0, false,true);
		fieldReverseButton =new JNumber(Double.class, 0,0, false,true);
		fieldDownCompare =new JNumber(Double.class, 0,0, false,true);
		checkSchedule = new JCheckBox("Schedule Label = ");				checkSchedule.setOpaque(false);
		fieldSchedule = new JTextField();
		ButtonGroup group = new ButtonGroup();
		group.add(radioDownTrend);
		group.add(radioUpTrend);
		
		trendPanel = new JPanel();
		trendPanel.setBackground(Color.red.darker());
		FormLayout trendlayout = new FormLayout(
				"20px,120px,20px,80px,80px,130px,100px",
				"pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref");
		PanelBuilder trendBuilder = new PanelBuilder(trendlayout,trendPanel);
		CellConstraints cc = new CellConstraints();
		trendBuilder.setBorder(new EmptyBorder(3,3,3,3));
		trendBuilder.addSeparator("",cc.xyw(3, 1,5));
		trendBuilder.add(radioUpTrend,cc.xyw(1, 1,2));
		trendBuilder.add(new JTextLabel("Compare Last Price >= "),cc.xyw(2, 3,2));
		trendBuilder.add(fieldUpCompare,cc.xy(4, 3));
		trendBuilder.add(new JTextLabel("IDR"),cc.xy(5, 3));
		trendBuilder.add(new JTextLabel("Reversal From TOP  >= "),cc.xyw(2, 5,2));
		trendBuilder.add(fieldReverseTop,cc.xy(4, 5));
		trendBuilder.add(new JTextLabel("IDR"),cc.xy(5,5));
		trendBuilder.add(radioDownTrend,cc.xyw(1, 7,2));
		trendBuilder.addSeparator("",cc.xyw(3,7,5));
		trendBuilder.add(new JTextLabel("Compare Last Price <= "),cc.xyw(2, 9,2));
		trendBuilder.add(fieldDownCompare,cc.xy(4, 9));
		trendBuilder.add(new JTextLabel("IDR"),cc.xy(5,9));
		trendBuilder.add(new JTextLabel("Reversal From BOTTOM >= "),cc.xyw(2,11,2));
		trendBuilder.add(fieldReverseButton,cc.xy(4, 11));
		trendBuilder.add(new JTextLabel("IDR"),cc.xy(5,11));
		trendBuilder.add(checkSchedule,cc.xyw(1, 13,2));
		trendBuilder.add(fieldSchedule,cc.xyw(3, 13,2));
		
		radioDownTrend.setSelected(true);

		fieldSchedule.setEnabled(false);
		checkSchedule.setSelected(false);
		AcUp(false);
		radioUpTrend.addActionListener(/*new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				AcUp(radioUpTrend.isSelected());
				AcDown(radioDownTrend.isSelected());
				tabs();
			}
		}*/ new MyAction());
		radioDownTrend.addActionListener(/*new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				AcUp(radioUpTrend.isSelected());
				AcDown(radioDownTrend.isSelected());
				tabs();
				
			}
		}*/new MyAction());
		checkSchedule.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(checkSchedule.isSelected())	fieldSchedule.setEnabled(true);
				else fieldSchedule.setEnabled(false);
			}
		});
		// ats limit price 10022016
		fieldUpCompare.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
//				if (!((UINewSchedule)apps.getUI().getForm(TradingUI.UI_NEWSCHEDULE)).validPrice((Double)fieldUpCompare.getValue())) {
//					fieldUpCompare.setValue(null);
//					fieldUpCompare.requestFocus();
//				}
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		fieldDownCompare.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
//				if (!((UINewSchedule)apps.getUI().getForm(TradingUI.UI_NEWSCHEDULE)).validPrice((Double)fieldDownCompare.getValue())) {
//					fieldDownCompare.setValue(null);
//					fieldDownCompare.requestFocus();
//				}	
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				
			}
		});
		
		this.add(trendPanel);
		
	}
	
	static void AcUp(boolean ak){
			fieldUpCompare.setEnabled(ak);
			fieldReverseTop.setEnabled(ak);
	}
	static void AcDown(boolean ad){
			fieldDownCompare.setEnabled(ad);
			fieldReverseButton.setEnabled(ad);		
	}
	void tabs(){
		if (radioDownTrend.isSelected()) {
			UINewSchedule.tabSellBuy.setSelectedIndex(0);
			radioDownTrend.setSelected(true);
		} else if(radioUpTrend.isSelected()){
			UINewSchedule.tabSellBuy.setSelectedIndex(1);
		}
	}
	public void clean(){
		fieldDownCompare.setValue(null);
		fieldReverseButton.setValue(null);
		fieldReverseTop.setValue(null);
		fieldUpCompare.setValue(null);
		fieldSchedule.setText(null);
		
	}
	
	public static String check(){
		String error =null;
		if(radioDownTrend.isSelected()){
			if(fieldDownCompare.getText().isEmpty()){
				error = "Please enter Down Compare";
			}
			if(fieldReverseButton.getText().isEmpty()){
				error = "Please enter Reverse Buttom";
			}
		}else if(radioUpTrend.isSelected()){
			if(fieldReverseTop.getText().isEmpty()){
				error = "Please enter Reverse Top";
			}
			if(fieldUpCompare.getText().isEmpty()){
				error = "Please enter Up Compare";
			}
		}
		if(checkSchedule.isSelected()){
			if(fieldSchedule.getText().isEmpty()){
			error = "Please enter Schedule";
			}		
		}
		return error;
	}
	
	
	public class MyAction implements ActionListener{
		  public void actionPerformed(ActionEvent e){
			  if(e.getSource()  == radioDownTrend){
				  AcUp(radioUpTrend.isSelected());
					AcDown(radioDownTrend.isSelected());
					tabs();
			  }else if(e.getSource()== radioUpTrend){
				  AcUp(radioUpTrend.isSelected());
					AcDown(radioDownTrend.isSelected());
					tabs();check();
			  }
			  }
		}
}
