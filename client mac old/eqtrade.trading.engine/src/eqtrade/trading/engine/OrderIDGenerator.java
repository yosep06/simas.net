package eqtrade.trading.engine;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import eqtrade.trading.core.Utils;


final public class OrderIDGenerator {
    private static int counter=0;
    private static int counterAts = 0;
    private static int counterGTC = 0;//*gtc
//    private final static SimpleDateFormat formatdate = new SimpleDateFormat("ddHHmmss");
    private final static SimpleDateFormat formatdate2 = new SimpleDateFormat("yyyyMMdd");
    private static long diff = 0;
    private final static String filename = "data/trading/sequence.dat";
    private static String sessionid="";
    private static String orisessionid = "";
    private static boolean shouldchgpwd =false;
    private static String terminal = "";
    private static String broker = "DH";
    private static String limitOrder = "7";
    private final static SimpleDateFormat dateFormat3 = new SimpleDateFormat("dd");
    
    static { 
    	load();
		try {
			terminal = InetAddress.getLocalHost().getHostAddress();			
		} catch (Exception ex){}
    }
    
    public static String getBrokerCode(){
    	return broker;
    }
    
    public static String getLimitOrder(){
    	return limitOrder;
    }
    
    public static String getTerminal(){
    	return terminal;
    }
    
    public static void setShouldchgpwd(boolean value){
    	shouldchgpwd = value;
    }
    
    public static boolean isShouldchgpwd(){
    	return shouldchgpwd;
    }
        
    public static void setSessionid(String id){
    	orisessionid = id;
        sessionid = addLeading(id+"", 6)+"#";
    }
    
    public static String getOriSessionId(){
    	return orisessionid;
    }

    public synchronized static String gen(String name){
        String unique = sessionid;
        if (counter == 9999999) counter = 0;
        counter++;
        //int jml = 20 - (name.length()+7);
        int jml = 8;
        save();
        //return name.toUpperCase() +unique+addLeading(counter+"",jml);
        return unique+addLeading(counter+"",jml);
    }
//*gtc
    public synchronized static String genGTC(String name){
        String unique = sessionid;
        String tgl = dateFormat3.format(getTime());
        if (counterAts == 999) counterAts = 0;
        counterAts++;
        //int jml = 20 - (name.length()+7);
        int jml = 6;
        save();
        //return name.toUpperCase() +unique+addLeading(counter+"",jml);
        return unique+tgl+addLeading(counterAts+"",jml);

    }
    public synchronized static String genAts(String name){
        String unique = sessionid;
        String tgl = dateFormat3.format(getTime());
        if (counterAts == 999) counterAts = 0;
        counterAts++;
        int jml = 6; //- (name.length()+9);
        save();//tgl(2)counter(3);
        return unique+tgl+addLeading(counterAts+"",jml);
    }
    private static String addLeading(String from, int len){
        if (from.length() < len) {
            int k = len - from.length();
            for (int i = 0; i<k;i++) from = "0".concat(from);
        }
        return from;
    }
    
    public static void setTime(Date serverTime){
        diff = serverTime.getTime() - (new Date()).getTime();
        System.out.println("new lag time: "+diff);
    }
    
    public static Date getTime(){
        return new Date((new Date()).getTime() + diff);
    }
    
    private static void save(){
        Vector v = new Vector(2);
        v.addElement(formatdate2.format(new Date()));
        v.addElement(new Integer(counter));
        Utils.writeFile(filename, v);
    }
    
    public static void setCounter(int i){
    	counter = i;
    }
    
    public static void addCounter(){
    	counter++;
    }
    
    public static void resetCounter(){
    	counter=0;
    }
    //*gtc
    public static void setCounterGTC(int i){
    	counterGTC = i;
    }
    
    public static void addCounterGTC(){
    	counterGTC++;
    }
    
    public static void resetCounterGTC(){
    	counterGTC=0;
    }
    
    public static int getCurrent(){
    	return counter;
    }

    private static void load(){
        Object o = Utils.readFile(filename);
        if (o != null){
            Vector v = (Vector)o;
            System.out.println("reading file seq: " +v);
            if (v.elementAt(0).toString().equals(formatdate2.format(new Date()))){
                counter = ((Integer)v.elementAt(1)).intValue();
            } else {
                resetCounter();
            }
        }
    }
    public static int getCounterAts() {
		return counterAts;
	}

	public static void setCounterAts(int i) {
		counterAts = i;
	}
}
