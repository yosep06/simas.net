package eqtrade.trading.engine;

import java.util.HashMap;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import quickfix.Message;
import quickfix.field.MsgType;
import eqtrade.trading.core.Event;
import eqtrade.trading.core.ITradingEngine;
import eqtrade.trading.core.Utils;

public class ThreadIn extends Thread {
	private final Log log = LogFactory.getLog(getClass());
	private ITradingEngine engine;
	private volatile Vector vin = new Vector(20, 10);

	private volatile boolean bstop = false;

	public ThreadIn (ITradingEngine engine) {
		super();
		

		this.engine = engine;
	}
	
	public void setStop(){
		bstop = true;
        synchronized(vin){
            vin.notify();
        }
	}
	
    public  void addIn(Vector vins){
        synchronized(vin){
	        vin.addAll(vins);
            vin.notify();
        }
    }

	private Vector popInMsg() {
		synchronized (vin) {
			if (vin.size() > 0) {
				String e = (String) vin.remove(0);
				Vector vtemp = new Vector(1);
				vtemp.addElement(e);
				return vtemp;
			} else {
				return null;
			}
		}
	}

	/*public void run() {
		Vector vtemp;
		while (!bstop) {
			try {
				vtemp = popInMsg();
				if (vtemp != null) {
					for (int i = 0; i < vtemp.size(); i++) {
						String tmp = (String) vtemp.elementAt(i);
						Message message = new Message(tmp);
	                    HashMap param = new HashMap();
	                    param.put(TradingEventDispatcher.PARAM_FIX, message);
	                    Event evt = new Event(message.getHeader().getField(new MsgType()).getValue(), param);
	                    //evt.setImmediately(true);
	                    engine.getEventDispatcher().pushEvent(evt);
						Thread.yield();
	                    //this.sleep(100);
					}
				} else {
					synchronized (vin) {
						try {
							vin.wait();
						} catch (Exception ex) {
						}
					}
				}
			} catch (Exception ex) {
				log.error(Utils.logException(ex));
			}
		}
	}*/
	
	
	
	
	
	  @Override
	public void run() {
	        Vector vtemp;
	        while (!bstop) {
	            try {
	                vtemp = popInMsg();
	                if (vtemp != null) {
	                    for (int i = 0; i < vtemp.size(); i++) {
	                        String tmp = (String) vtemp.elementAt(i);
	                        Message message = new Message(tmp);
	                        HashMap param = new HashMap();
	                        System.out.println(message.getHeader().getField(new MsgType()).getValue()+"header");
	                        param.put(TradingEventDispatcher.PARAM_FIX, message);
	                        Event evt = new Event(message.getHeader().getField(new MsgType()).getValue(), param);
	                        evt.setImmediately(true);
	                        engine.getEventDispatcher().pushEvent(evt);
	                        //Thread.yield();
	                        Thread.sleep(5);
	                    }
	                } else {
	                    synchronized (vin) {
	                        try {
	                            vin.wait();
	                        } catch (Exception ex) {
	                        }
	                    }
	                }
	            } catch (Exception ex) {
	               // log.error(Utils.logException(ex));
	            }
	        }
	    }
	
}
