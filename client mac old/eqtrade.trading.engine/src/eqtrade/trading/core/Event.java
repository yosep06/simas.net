package eqtrade.trading.core;

import java.util.HashMap;

public class Event {
	private String name;
	private HashMap param;
	private boolean needACK = true;
	private boolean immediately = false;  
	
	public Event(String name){
		this.name=name;
		this.param = new HashMap();
	}	
	
	public Event(String name, boolean immediately){
		this(name);
		this.immediately = immediately;
	}
	
	
	public Event(String name, HashMap param){
		this.name = name;
		this.param = param;
	}
	
	public Event(String name, HashMap param, boolean immediately){
		this(name, param);
		this.immediately = immediately;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public HashMap getParam() {
		return param;
	}

	public Event setParam(HashMap param) {
		this.param = param;
		return this;
	}

	public boolean isNeedACK() {
		return needACK;
	}

	public void setNeedACK(boolean needACK) {
		this.needACK = needACK;
	}
	
	public boolean isImmediately() {
		return immediately;
	}

	public void setImmediately(boolean immediately) {
		this.immediately = immediately;
	}	
	
	public Object getField(String key){
		return param.get(key);
	}
	
	public Event setField(String key, Object value){
		param.put(key, value);		
		return this;
	}	
}
