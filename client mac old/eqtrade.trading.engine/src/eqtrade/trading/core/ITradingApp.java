package eqtrade.trading.core;

import java.util.Vector;

import com.vollux.idata.GridModel;

import eqtrade.trading.model.Notif;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderMatrix;
import eqtrade.trading.model.Schedule;

public interface ITradingApp {
	public void start();
	public void addListener(ITradingListener listener);
	public void removeListener(ITradingListener listener);	
	public void login(String userid, String passwd);	
	public void checkPIN(String userid, String pin);
	public void checkPIN_NEGO(String pin, String form);
	public void checkPINSLS(String userid, String pin);
	public void changePasswd(String oldPasswd, String newPasswd);
	public void changePIN(String oldPIN, String newPIN);
	public void reconnect(int counter);
	public void createOrder(Order order);
	public void createNotif(Notif notif);
	public void deleteOrder(Order order);
	public void sendOrder(Order order);
	public void withdrawOrder(Order order);
	public void amendOrder(Order oldorder, Order neworder);
	public GridModel getStore(Integer id);
	public void logout();
	public void stop();
	public String getUserId();
	public String getPassword();
	public String getPIN();
	public void refreshData(boolean master, boolean account, boolean margin, boolean pf, boolean order);
	public void refreshData(String tradingid);
	public void refreshOrder(String param1, String param2,String param3);
	public void refreshTrade(String orderid);
	public void refreshPortfolio(String param1, String param2);
	public void refreshAccount(String param);
	public void refreshAts(String param);
	public void refreshBrowseAts(String param);
	public void refreshCashColl(String param);
	public void refreshBuySellDetail(String param1, String param2);
	public void refreshDueDate(String param1);
	public void refreshOrderMatrix(String param);
	public void refreshOrderMatrixList(String param,String param2);
	public void refreshNotif(String param);
	public String splitOrder(String param);
	public void createOrderMulti(Vector order);
	public void withdrawOrderMulti(Vector order);
	public void createSchedule(Schedule schedule);//schedule
	public void withdrawAts(Schedule schedule);//withdrawa ats
	
	//public void createOrderMatrix(OrderMatrix oMatrix, Vector param);
	public void createOrderMatrix(String label, OrderMatrixData[] data, boolean isbasket, String basketsendtime);
	public void deleteOrderMatrix(Vector param);
	public void deleteOrderMatrixByLabel(String userid, String label);
	public void deleteOrderMatrixDetil(Vector param);
	//*gtc 24112014
	public void GTC (Order gtc);
	public void createGTCMulti (Vector order);
	public void withdrawGtc (Order gtc);
	public void AmmendGtc (Order oldorder, Order neworder);
	public void refreshGTC (String param, String param2);
	
	ITradingConnection getConnection();
}
