package eqtrade.trading.core;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import quickfix.field.ReqType;

import com.Ostermiller.util.Base64;
import com.Ostermiller.util.MD5;

import eqtrade.trading.engine.socket.TradingSocketConnection;

public final class Utils extends Object {
	public static final SimpleDateFormat formatDate = new SimpleDateFormat(
			"yyMMdd");

	public static Vector parser(String s, String s1) {
		Vector vector = new Vector(10, 5);
		StringBuffer stringbuffer = new StringBuffer(100);
		StringTokenizer stringtokenizer;
		if (!s1.equals(" ")) {
			stringbuffer.append(s + " ");
			int i = 0;
			do {
				i = stringbuffer.toString().indexOf(s1, i);
				if (i != -1) {
					stringbuffer.insert(i, " ");
					i += 2;
				}
			} while (i != -1);
			String s3 = stringbuffer.toString();
			stringtokenizer = new StringTokenizer(s3, s1);
		} else {
			stringbuffer.append(s);
			String s4 = stringbuffer.toString();
			stringtokenizer = new StringTokenizer(s4);
		}
		String s2;
		for (; stringtokenizer.hasMoreElements(); vector.addElement(s2.trim()))
			s2 = new String((String) stringtokenizer.nextElement());

		return vector;
	}

	public static void parser(String strInput, String strDelimeter,
			Vector vecInput) {

		StringBuffer strTemp3;
		int nIndeks;
		String strTemp2, strTemp4;
		java.util.StringTokenizer st;

		if (!strDelimeter.equals(" ")) {
			strTemp3 = new StringBuffer(strInput + " ");
			nIndeks = 0;
			do {
				nIndeks = strTemp3.toString().indexOf(strDelimeter, nIndeks);
				if (nIndeks != -1) {
					strTemp3.insert(nIndeks, " ");
					nIndeks += 2;
				}
			} while (nIndeks != -1);
			strTemp4 = strTemp3.toString();
			st = new java.util.StringTokenizer(strTemp4, strDelimeter);
		} else {
			strTemp3 = new StringBuffer(strInput);
			strTemp4 = strTemp3.toString();
			st = new java.util.StringTokenizer(strTemp4);
		}

		while (st.hasMoreElements()) {
			strTemp2 = new String((String) st.nextElement());
			vecInput.addElement(strTemp2.trim());
		}

	}

	public static double strToDouble(String strInput, double defaultValue) {
		double nReturn = 0;

		nReturn = strToDouble(strInput, defaultValue, true);

		return nReturn;
	}

	public static double strToDouble(String strInput, double defaultValue,
			boolean bFormat) {
		double nReturn = 0;
		Number nNumber = null;
		NumberFormat nf = NumberFormat.getInstance();
		ParsePosition ps;

		nReturn = defaultValue;
		try {
			strInput = strInput.trim();
			if (bFormat) {
				if (strInput.length() > 0) {
					ps = new ParsePosition(0);
					nNumber = nf.parse(strInput, ps);
					if (ps.getIndex() == strInput.length())
						nReturn = nNumber.doubleValue();
				}
			}
		} catch (Exception ex) {
			// ex.printStackTrace();
		}

		return nReturn;
	}

	public static boolean saveToFile(String strInput, String strFileName) {
		boolean bResult = false;

		try {
			DataOutputStream out2 = new DataOutputStream(
					new BufferedOutputStream(new FileOutputStream(strFileName)));
			try {
				out2.writeBytes(strInput);
				out2.close();
				bResult = true;
			} catch (IOException ex) {
			}
			DataInputStream in5 = new DataInputStream(new BufferedInputStream(
					new FileInputStream(strFileName)));
			try {
				System.out.println(in5.readByte());
			} catch (IOException ex) {
			}
		} catch (Exception ex) {
		}
		return bResult;
	}

	public static String readFromFile(String strFileName) {
		byte[] strByte = new byte[2];
		StringBuffer strTempBuff = new StringBuffer();

		if (strFileName != null) {
			if (strFileName.length() > 0) {
				try {
					DataInputStream in = new DataInputStream(
							new BufferedInputStream(new FileInputStream(
									strFileName)));

					while (in.read(strByte, 0, 2) != -1) {
						strTempBuff.append(new String(strByte));
						// strResult += new String(strByte);
					}

					in.close();
				} catch (IOException ex) {
					// ex.printStackTrace();
				}
			}
		}

		return strTempBuff.toString();
	}

	public synchronized static void hideMessage(){
		if (dialog != null) {
			dialog.dispose();
		}
	}
	
	public synchronized static void showMessage(final String strInput,
			final Component myParent) {
		try {
			
		
		if (time != null) time.cancel();
		time = new Timer();
		task = new RequestTime(); 
		time.schedule(task, 10000);
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {		
				JButton btnOK = new JButton("OK");
				options = new Object[] { btnOK };
				System.out.println((dialog != null)+" showmessage");
				if (dialog != null)
					dialog.dispose();
		
				JOptionPane jop = new JOptionPane(strInput,
						JOptionPane.INFORMATION_MESSAGE, JOptionPane.PLAIN_MESSAGE,
						null, options, options[0]);
				dialog = jop.createDialog(myParent, "Information");
		
				btnOK.addMouseListener(new java.awt.event.MouseAdapter() {
					@Override
					public void mouseClicked(java.awt.event.MouseEvent e) {
						time.cancel();
						dialog.dispose();
					}
				});
				btnOK.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent ae) {
						time.cancel();
						dialog.dispose();
					}
		
				});
				dialog.setVisible(true);
			}
		});
		} catch (Exception e) {e.printStackTrace();
		}
	}

	public static void showToCenter(Component myFrame) {
		java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit()
				.getScreenSize();
		java.awt.Dimension frameSize = myFrame.getSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		myFrame.setLocation((screenSize.width - frameSize.width) / 2,
				(screenSize.height - frameSize.height) / 2);
		// myFrame.setVisible(true);
	}

	public static Point getCenter(Dimension frameSize) {
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit()
				.getScreenSize();
		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}
		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}
		return new Point((screenSize.width - frameSize.width) / 2,
				(screenSize.height - frameSize.height) / 2);
	}

	public static java.util.Date genServerTime(long diffTime) {
		java.util.Date myDate = new java.util.Date();

		myDate = new java.util.Date(myDate.getTime() + diffTime);

		return myDate;
	}

	public static String logException(Exception ex) {
		StringBuffer strresult = new StringBuffer("");

		Object[] objarr = ex.getStackTrace();
		strresult.append("[Start:Error : ").append("\n");
		strresult.append(ex.toString()).append("\n");
		for (int i = 0; i < objarr.length; i++) {
			strresult.append("---->").append(objarr[i].toString()).append("\n");
		}
		strresult.append("End:Error]");

		return strresult.toString();
	}

	public static final int C_CLOSING_DLG = -1;

	public static final int C_YES_DLG = 0;

	public static final int C_NO_DLG = 1;

	static JDialog dialog = null;

	static JOptionPane pane = null;

	static Object[] options = null;

	public static final int DEFAULT_BUTTON_YES = 0;

	public static final int DEFAULT_BUTTON_NO = 1;
	
	static TimerTask task ;
	
	static Timer time ;

	public static int showConfirmDialog(Component parent, String strTitle,
			String strInput, int nDefault) {

//		if (time != null) time.cancel();
//		time = new Timer();
//		time.schedule(task, 3000);
//		try {
			
		
		int nreturn = -1;
		JButton Yes = new JButton("Yes");
		JButton No = new JButton("No");

		Yes.setMnemonic('Y');
		No.setMnemonic('N');

		No.addKeyListener(new KeyHandlerAdapter());
		Yes.addKeyListener(new KeyHandlerAdapter());

		options = new Object[] { Yes, No };
		pane = new JOptionPane(strInput, JOptionPane.QUESTION_MESSAGE,
				JOptionPane.PLAIN_MESSAGE, null, options, options[nDefault]);
		dialog = pane.createDialog(parent, strTitle);

		Set set = pane
				.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS);
		KeyStroke forward = KeyStroke.getKeyStroke("RIGHT");
		set = new HashSet(set);
		set.add(forward);
		pane.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
				set);
		set = pane
				.getFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS);
		KeyStroke backward = KeyStroke.getKeyStroke("LEFT");
		set = new HashSet(set);
		set.add(backward);
		pane.setFocusTraversalKeys(
				KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, set);

		Yes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pane.setValue(options[0]);
				dialog.dispose();
			}
		});

		No.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pane.setValue(options[1]);
				dialog.dispose();
			}
		});

		dialog.setVisible(true);
		Object value = pane.getValue();

		if (value == null)
			nreturn = C_CLOSING_DLG;
		else if (value.equals(options[0]))
			nreturn = C_YES_DLG;
		else if (value.equals(options[1]))
			nreturn = C_NO_DLG;
		
		return nreturn;
//		} catch (Exception e) {e.printStackTrace(); return C_NO_DLG;
//		}
	}

	public static Object readFile(String pname) {
		Object objresult = null;

		try {
			File file = new File(pname);
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fis);
			objresult = in.readObject();
			in.close();
		} catch (Exception ex) {
			objresult = null;
		}

		return objresult;
	}

	public static boolean writeFile(String pname, Object objin) {
		boolean bresult = false;

		try {
			if (objin != null) {
				FileOutputStream fos = new FileOutputStream(pname);
				ObjectOutput out = new ObjectOutputStream(fos);
				out.writeObject(objin);
				out.close();
				bresult = true;
			}
		} catch (Exception ex) {
			// ex.printStackTrace();
		}

		return bresult;
	}

	public static String getMD5(String strinput) {
		String strresult = "";

		try {

			strresult = MD5.getHashString(strinput.toLowerCase());

		} catch (Exception ex) {
			// ex.printStackTrace();
		}

		return strresult;
	}

	public static byte[] compressBase64(String strinput) {
		byte[] bresult = null;

		try {

			byte[] buffer2 = Base64.decode(strinput.getBytes());
			byte[] bufferdecompressed = Utils.decompress(buffer2);

			bresult = bufferdecompressed;

		} catch (Exception ex) {

			// ex.printStackTrace();

		}

		return bresult;
	}

	/*
	 * method ini digunakan untuk decompressed data dengan input : byte array
	 * (compressed) output : byte array (uncompressed) created by epd ,
	 * Thursday, July 03, 2003
	 */

	public static byte[] decompress(byte[] input) {
		byte[] result = null;
		Inflater decompressor = new Inflater();
		try {

			decompressor.setInput(input);

			ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);

			byte[] buf = new byte[1024];
			int count;
			while (!decompressor.finished()) {
				count = decompressor.inflate(buf);
				bos.write(buf, 0, count);
			}
			bos.close();

			result = bos.toByteArray();
		} catch (Exception ex) {
			// ex.printStackTrace();
		} finally {
			decompressor.end();
		}

		return result;
	}

	public static Object readBinary(byte[] binput) {
		Object objresult = null;

		try {
			ObjectInputStream in = new ObjectInputStream(
					new ByteArrayInputStream(binput));
			objresult = in.readObject();
			in.close();
		} catch (Exception ex) {
			// ex.printStackTrace();
		}

		return objresult;
	}

	public static Object decode64FromFile(String sinput) throws Exception {

		return Utils.readBinary(Base64.decode(sinput.getBytes()));

	}

	public static byte[] compress(byte[] input) {
		byte[] result = null;
		Deflater compressor = new Deflater();

		try {
			compressor.setLevel(Deflater.BEST_COMPRESSION);

			compressor.setInput(input);
			compressor.finish();

			ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);

			byte[] buf = new byte[1024];
			int count;
			while (!compressor.finished()) {
				count = compressor.deflate(buf);
				bos.write(buf, 0, count);
			}
			bos.close();

			result = bos.toByteArray();
		} catch (Exception ex) {
			// ex.printStackTrace();
		} finally {
			compressor.end();
		}

		return result;
	}

	public static byte[] objectToByte(Object objin) {
		byte[] bresult = null;

		try {
			if (objin != null) {
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				ObjectOutput out = new ObjectOutputStream(bos);

				out.writeObject(objin);
				out.close();

				bresult = bos.toByteArray();
			}
		} catch (Exception ex) {
			// ex.printStackTrace();
		}

		return bresult;
	}

	public static byte[] objectToCompressedByte(Object objin) {
		byte[] bresult = null;

		try {
			if (objin != null) {
				byte[] btemp = Utils.objectToByte(objin);
				bresult = Utils.compress(btemp);
			}
		} catch (Exception ex) {
			// ex.printStackTrace();
		}

		return bresult;
	}

	public static Object compressedByteToObject(byte[] binput) {
		Object objresult = null;

		try {
			if (binput != null) {
				// System.out.println(" comppress byte : " + binput.length);
				byte[] btemp = Utils.decompress(binput);
				// System.out.println(" decompprese byte : " + btemp.length);
				objresult = Utils.readBinary(btemp);
			}
		} catch (Exception ex) {
			// ex.printStackTrace();
		}

		return objresult;
	}

	public static void deleteFile(String sdatapath, String[] exclude) {
		try {
			boolean isexclude = false;
			File filedata = new File(sdatapath);
			if (filedata.isDirectory()) {
				String[] files = filedata.list();
				for (int i = 0; i < files.length; i++) {
					isexclude = false;
					for (int j = 0; j < exclude.length; j++) {
						if (files[i].toLowerCase().startsWith(
								exclude[j].toLowerCase())) {
							isexclude = true;
							break;
						}
					}
					if (!isexclude) {
						File filetemp = new File(sdatapath + "/" + files[i]);
						if (!filetemp.isDirectory())
							filetemp.delete();
					}
				}
			}
		} catch (Exception ex) {
			// ex.printStackTrace();
		}
	}

	private static Random gen = new Random();

	public static int randMax(int maxval) {
		return gen.nextInt(maxval) + 1;
	}

	public static int[] random(int jmlorder, int jmlsplit, boolean random) {
		int[] hasilAll = new int[jmlsplit];
		int hasil = 0;
		int x = 1;
		int jml = 0;
		int maxnumber;
		while (x <= jmlsplit) {
			if (x == jmlsplit) {
				hasil = jmlorder - jml;
			} else {
				if (x == 1) {
					maxnumber = Math.round(jmlorder / jmlsplit);
				} else {
					maxnumber = Math.round((jmlorder - jml)
							/ (jmlsplit - x + 1));
				}
				hasil = (random ? randMax(maxnumber) : maxnumber);
				jml = jml + hasil;
			}
			hasilAll[x - 1] = hasil;
			x++;
		}
		return hasilAll;
	}
}

class KeyHandlerAdapter extends KeyAdapter {
	@Override
	public void keyPressed(KeyEvent ke) {
		int key = ke.getKeyCode();
		if (key == KeyEvent.VK_ENTER) {
			Object ob = ke.getSource();
			((JButton) ob).doClick();
		}
		if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_LEFT) {
			ke.setKeyCode(KeyEvent.VK_TAB);
		}
	}
}
class RequestTime extends TimerTask{
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			if (Utils.dialog!=null) {
				Utils.dialog.dispose();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
