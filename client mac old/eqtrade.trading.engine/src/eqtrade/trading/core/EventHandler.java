package eqtrade.trading.core;


public abstract class EventHandler {
    public abstract void execute(Event param);
}
