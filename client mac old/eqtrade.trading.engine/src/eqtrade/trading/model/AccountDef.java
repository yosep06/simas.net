package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class AccountDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader);
        htable.put("alignment", defaultHeaderAlignment);
        htable.put("width", defaultColumnWidth);
        htable.put("order", defaultColumnOrder);
        htable.put("sorted", columnsort);        
        htable.put("sortcolumn",new Integer(Account.C_ACCID));
        htable.put("sortascending", new Boolean(true));
        htable.put("hide", columhide);
        return htable;
	}	
	
	public static int[] columhide = new int[]{
		Account.C_MYACC
	};
	
    public static String[] dataHeader = new String[]{
    	"Trading Id", "Branch", "Sales", "Cust Type", "Credit Limit", "Current Ratio", "Current TL", "Deposit", "Buy", "Sell",
    	"NetAC", "Bid", "Offer", "Stock Value", "Market Value", "LQ Value", "TopUp", "Force Sell", "MyACC", "Acc Id",
    	"Name", "Cust Id", "Status", "Is Corp", "Inv Type", "Phone", "Handphone", "Email", "Sub Account", 
    	"ComplainceId", "Address", "Acc Type", "Cash Withdraw",
    	"SingleID", "Virtual Account", "Investor Account"
    };

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			100,100,100,100,100,100,100,100,100,100,
            100,100,100,100,100,100,100,100,100,100,
            100,100,100,100,100,100,100,100,100,100,100,100 ,100,
            100,100,100
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6,7,8,9,10,
            11,12,13,14,15,16,17,18,19,20,
            21,22,23,24,25,26,27,28,29,30,31,32,
            33,34,35
	};	

	public static boolean[] columnsort = new boolean[]{
			true, true, true, true, true,true, true,true,true,true,
            true, true, true, true, true,true, true,true,true,true,
            true, true, true, true, true,true, true,true,true,true,
            true, true, true,
            true, true, true
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(Account.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row acc) {
		List vec = new Vector(Account.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Account.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new AccountField(acc, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static AccRender render = new AccRender();
	
    static class AccRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new AccountRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((AccountRender)renderer).setValue(value);
			return renderer;
		}
	}
}
