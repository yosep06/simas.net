package eqtrade.trading.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.trading.core.TradingSetting;

public class NotifRender extends DefaultTableCellRenderer{
	private static final long serialVersionUID = -2072791002514934859L;
	private static NumberFormat formater = new DecimalFormat("#,##0");
	private String strFieldName = new String();
	public static SimpleDateFormat formattime = new SimpleDateFormat("HH:mm:ss");
	private static Color newBack;
	private static Color newFore;
	private static Notif ord;
	
	public Component getTableCellRendererComponent(JTable table, Object value, 
		boolean isSelected,boolean hasFocus, int row, int column){
		strFieldName = table.getColumnName(column);
	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
	 	try{
	 		strFieldName = table.getColumnName(column);
			newBack =  (column % 2) == 0 ? TradingSetting.getColor(TradingSetting.C_BACKGROUND) : TradingSetting.getColor(TradingSetting.C_BACKGROUNDEVEN);
			if(value instanceof MutableIData){
				
			}
	 	}catch (Exception e) {
			// TODO: handle exception
		}
	 	
	 	return component;
	}
}
