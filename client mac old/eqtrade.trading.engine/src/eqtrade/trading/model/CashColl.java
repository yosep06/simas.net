package eqtrade.trading.model;

import eqtrade.trading.core.Utils;

public final class CashColl extends Model {
    private static final long serialVersionUID = 309177931626178221L;
    public static final int	C_TRADINGID = 0;
    public static final int   C_ACCID = 1;
	public static final int	C_CASHID = 2;
    public static final int   C_CASHNAME = 3;
	public static final int	C_BEGVALUE = 4;
	public static final int   C_VALUE = 5;
	public static final int 	CIDN_NUMBEROFFIELDS = 6;

	public CashColl(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public CashColl(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 4: case 5:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}	

    public Double  getValue(){
        return ((Double)getData(C_VALUE));
    }
    public void setValue(Double sinput){
        setData(sinput, C_VALUE);
    }    
	
	
	public String getTradingID(){
		return (String) getData(C_TRADINGID);
	}
	public void setTradingID(String sinput){
		setData(sinput, C_TRADINGID);
	}
	public String getAccID(){
		return (String) getData(C_ACCID);
	}
	public void setAccID(String sinput){
		setData(sinput, C_ACCID);
	}	
	public String getCashName(){
		return (String) getData(C_CASHNAME);
	}
	public void setCashName(String sinput){
		setData(sinput, C_CASHNAME);
	}    
    public String getCashID(){
        return (String) getData(C_CASHID);
    }
    public void setCashID(String sinput){
        setData(sinput, C_CASHID);
    }
    public Double getBegValue(){
        return (Double) getData(C_BEGVALUE);
    }
    public void setBegValue(Double sinput){
        setData(sinput, C_BEGVALUE);
    }
}