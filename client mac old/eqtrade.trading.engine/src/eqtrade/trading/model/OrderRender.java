package eqtrade.trading.model;

import java.awt.Color;
import java.awt.Component;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.trading.core.SelectedBorder;
import eqtrade.trading.core.TradingSetting;

public class OrderRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     private String strFieldName = new String("");
	 public static NumberFormat formatter = new DecimalFormat("#,##0  ");
	 public static SimpleDateFormat formattime = new SimpleDateFormat("HH:mm:ss");
	 public static SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd");
     private static SelectedBorder border = new SelectedBorder();
	 private static Order ord;
     private static Color newBack;
     private static Color newFore;

	 public static HashMap hashStatus = new HashMap();
	 public static HashMap hashType = new HashMap();
     public static HashMap hashSide = new HashMap();
     public static HashMap hashAccType = new HashMap();
	 static {
        hashStatus.put("SE", "Sending entry...");
        hashStatus.put("ST", "Sending temp...");
        hashStatus.put("SD", "Sending delete...");
        hashStatus.put("SW", "Sending withdraw...");
        hashStatus.put("SA", "Sending amend...");
        hashStatus.put("SES", "Sending entry (on server)...");
        hashStatus.put("STS", "Sending temp (on server)...");
        hashStatus.put("SDS", "Sending delete (on server)...");
        hashStatus.put("SWS", "Sending withdraw (on server)...");
        hashStatus.put("SAS", "Sending amend (on server)...");
        hashStatus.put("F", "Failed");
        hashStatus.put("O|P", "All Open");
        hashStatus.put("0", "Open");
        hashStatus.put("SE|ST|SD|SW|SA|SES|STS|SDS|SWS|SAS", "All Sending");
        hashStatus.put("E|ET|EW|EA|ED", "All Request");
        hashStatus.put("S", "Split");
		hashType.put("0", "Day");
        hashType.put("S", "Session");
        hashType.put("G", "GTC");//*gtc 24112014
        hashSide.put("1", "Buy");
        hashSide.put("2", "Sell");
        //Tambahan untuk ORI 2.0 kvn 20111130 - remark on
        hashSide.put("5", "Sell");
        hashSide.put("M", "Buy");
        //End 

	 }

	 @Override
	public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
		 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
		 	try  {
			        strFieldName = table.getColumnName(column);
					newBack =  (column % 2) == 0 ? TradingSetting.getColor(TradingSetting.C_BACKGROUND) : TradingSetting.getColor(TradingSetting.C_BACKGROUNDEVEN);
		
				 	if (value instanceof MutableIData){
				  		ord = (Order)((MutableIData)value).getSource();
				  		//Tambahan untuk ORI 2.0 kvn 20111130 - Remark on
				  		//if (ord.getBOS().equals(Order.C_BUY)){
				  		if (ord.getBOS().equals(Order.C_BUY) || ord.getBOS().equals("M")){ 			
				  		//End Remark	
				            newFore = TradingSetting.getColor(TradingSetting.C_BUY);   
				  		} else {
				            newFore = TradingSetting.getColor(TradingSetting.C_SELL);   
				  		} 
				  		if (ord.getStatus().equals("R") || ord.getStatus().equals("W") || ord.getStatus().equals("A") || 
				  			ord.getStatus().equals("F") || ord.getStatus().equals("D")){
				  			newFore = newFore.darker();
				  		} else if (ord.getStatus().equals("SES") || ord.getStatus().equals("STS") || ord.getStatus().equals("SWS") || ord.getStatus().equals("SAS") || ord.getStatus().equals("SDS")){
				  			newFore = TradingSetting.getColor((TradingSetting.C_ZERO)).darker();
				  		} else if (ord.getStatus().equals("SE") || ord.getStatus().equals("ST") || ord.getStatus().equals("SW") || ord.getStatus().equals("SA") || ord.getStatus().equals("SD")){
				  			newFore = TradingSetting.getColor((TradingSetting.C_ZERO));				  		
				  		} else if (ord.getStatus().equals("F")){
				  			newFore = TradingSetting.getColor(TradingSetting.C_OTHER);   
				  		}
				 	} 
				 	
				 	if  (isSelected) {
			            ((JLabel)component).setBorder(border);
			            newBack = Color.yellow;
			        }
			        component.setBackground(newBack);
			        component.setForeground(newFore);          
		 	} catch (Exception ex){
		 		ex.printStackTrace();
		 	}
		 	return component;
    }
	
	 @Override
	public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
	             Order o = (Order)args.getSource();
	             String strTemp;
                 setHorizontalAlignment((dat instanceof Double)? SwingConstants.RIGHT : SwingConstants.LEFT);
	             	if (dat instanceof Double) {
	                    if (strFieldName.equals(OrderDef.dataHeader[Order.C_MYACC])){
	                    	setText(((Double)dat).doubleValue()==1?" Yes":" No");
	                    } else {
			    				double d = Math.floor(((Double)dat).doubleValue());
			    				double s = (((Double)dat).doubleValue()) - d;
			    				if (s > 0){
				    				formatter.setMaximumFractionDigits(2);
				    				formatter.setMinimumFractionDigits(2);
			    				} else {
				    				formatter.setMaximumFractionDigits(0);
				    				formatter.setMinimumFractionDigits(0);	    					
			    				}
								setText(formatter.format(dat));					
	                    }
		    		} else if (dat instanceof Date){
		    			if (strFieldName.equals(OrderDef.dataHeader[Order.C_ORDERDATE])){
		    				setText(formatdate.format((Date)dat));
		    			} else {
		    				setText(formattime.format((Date)dat));
		    			}
		    		} else if (dat instanceof Timestamp){
		    			setText(formattime.format(new Date(((Timestamp)dat).getTime())));
		    		} else if (strFieldName.equals(OrderDef.dataHeader[Order.C_STATUSID])){
		    			if (dat!=null && o!=null){
			    			if (dat.equals("o") && o.getIsBasketOrder().equals("1")) {
			    				strTemp = "o";
			    			} else {
			    				strTemp = (String)hashStatus.get(dat);
			    			}
			    			if (strTemp == null) strTemp = dat+" Unknown";
			    			setText(strTemp);
		    			} else {
		    				strTemp = (String)hashStatus.get(dat);
			    			if (strTemp == null) strTemp = dat+" Unknown";
			    			setText(strTemp);
		    			}
		    		} else if (strFieldName.equals(OrderDef.dataHeader[Order.C_ORDERTYPE])){
		    			strTemp = (String)hashType.get(dat);
		    			if (strTemp == null) strTemp = dat+" Uknown";
		    			setText(strTemp);
		    		} else if (strFieldName.equals(OrderDef.dataHeader[Order.C_ORDTYPE])){
		    				if (dat != null) {
			    				if (dat.equals("7") || dat.equals("E")) setText("Limited Order");
				    			else setText(dat+" unknown");
		    				} else {
		    					setText("");
		    				}
		    		} else if (strFieldName.equals(OrderDef.dataHeader[Order.C_ISADVERTISING]) ||
		    				strFieldName.equals(OrderDef.dataHeader[Order.C_ISBASKETORDER]) ||
		    				strFieldName.equals(OrderDef.dataHeader[Order.C_ISOVERLIMIT]) ||
		    				strFieldName.equals(OrderDef.dataHeader[Order.C_ISFLOOR]) ||
		    				strFieldName.equals(OrderDef.dataHeader[Order.C_ISSHORTSELL])){
		    				if (dat == null){
		    					setText("");
		    				} else {
		    					setText(dat.equals("1") ? "Yes" : "No");
		    				}	             	
                    } else if (strFieldName.equals(OrderDef.dataHeader[Order.C_BUYORSELL])){
                        strTemp = (String)hashSide.get(dat);
                        if (strTemp == null) strTemp = dat+" Unknown";
                        setText(strTemp);
		            } else if (dat == null){
		                setText("");
		            }else {
		                setText(" "+dat.toString());
		            }
	         }
	     } catch (Exception e){
	     }
	 }
}
