package eqtrade.trading.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import eqtrade.trading.core.Utils;

public class Schedule extends Model{

	private static final long serialVersionUID = 2104383868242771292L;
	
	public static final int C_ATSID = 0;
	public static final int C_ATSDATE = 1;
	public static final int C_CLIENTID = 2;
	public static final int C_BUYSELL = 3;
	public static final int C_SECID = 4;
	public static final int C_PIRCE = 5;
	public static final int C_LOT = 6;
	public static final int C_VOLUME = 7;
	public static final int C_LABEL = 8;
	public static final int C_VALIDITY =9;
	public static final int C_VALIDUNTIL = 10;
	public static final int C_ATSSTATUS =11;
	public static final int C_ORDERID = 12;
	public static final int C_ORDERSTATUS = 13;
	public static final int C_CONDITIONTYPE =14;
	public static final int C_CONDITIONMETHOD =15;
	public static final int C_COMPARELASTPRICE = 16;
	public static final int C_REMAINBIDLOT =17;
	public static final int C_CUMULATIVVEDONE = 18;
	public static final int C_CUMULATIVECALC = 19;
	public static final int C_REVERSALLIMIT = 20;
	public static final int C_BD_FILTER=21;
	public static final int C_BD_REFCODE=22;
	public static final int C_BD_REFOPT = 23;
	public static final int C_BD_DONETYPE = 24;
	public static final int C_BD_DONESPEC = 25;	
	public static final int C_ENTRYBUY=26;
	public static final int C_DESCRIPTION =27;
	public static final int C_ENTRYTIME=28;
	public static final int C_ENTRYTERMINAL =29;
	public static final int C_EXECBY=30;
	public static final int C_EXECTIME=31;
	public static final int C_WITHDRAWBY=32;
	public static final int C_WITHDRAWTIME =33;
	public static final int C_WITHDRAWTERMINAL=34;
	public static final int C_FLAG1=35;
	public static final int C_INVTYPE = 36;
	public static final int C_COMPLAINCEID = 37;
	
	public static final int 	CIDN_NUMBEROFFIELDS = 38;

	public static final String C_BUY = "1";
    public static final String C_SELL = "2";
    
    public static final String C_ENTRY = "o";
    public static final String C_WITHDRAW = "w";
    public static final String C_OPEN = "O";
    public static final String C_WITHDRAW_S = "W";
    
    //executed withdraw description (popup)
    
    public static final String C_REQUEST_ENTRY = "RE";
    public static final String C_REQUEST_WITHDRAW = "RW";
    public static final String C_ENTRY_SUCCESS = "ES";
    public static final String C_WITHDRAW_SUCCESS = "WS";
    public static final String C_EXECUTED = "EX";
    public static final String C_WITHDRAW_FAILED = "WF";
    public static final String C_ENTRY_FAILED = "EF";
    
	
	public Schedule() {
		super(CIDN_NUMBEROFFIELDS);
		
	}
	public final static SimpleDateFormat formatdate = new SimpleDateFormat("HHmmss");

	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 6: case 7: case 8: case 17: case 18: case 19: case 20: case 21: case 26:case 9:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;	
			case 1: case 3: case 4: case 5:  case 10: case 12: case 13: case 14: case 15: case 16: case 22: case 23:
			case 24: case 25: case 27: case 28: case 30: case 31: case 32: case 33 :case 34 :case 35 :
				if(!vdata.elementAt(i).equals("-"))
					try{
						vdata.setElementAt(formatdate.parse((String)vdata.elementAt(i)), i);
					}catch (Exception e) {
						vdata.setElementAt(null, i);
					}
					else {
						vdata.setElementAt(null, i);
					}
				break;
			default:
				break;
			}
		}
		
	}

	public String getcAtsid() {
		return (String) getData(C_ATSID);
	}

	public void setcAtsid(String cAtsid) {
		setData(cAtsid, C_ATSID);
	}

	public Date getcAtsdate() {
		return (Date) getData(C_ATSDATE);
	}

	public void setcAtsdate(Date cAtsdate) {
		setData(cAtsdate, C_ATSDATE);
	}

	public String getcClientid() {
		return (String)getData(C_CLIENTID);
	}

	public void setcClientid(String cClientid) {
		setData(cClientid, C_CLIENTID);
	}

	public String getcBuysell() {
		return (String) getData(C_BUYSELL);
	}

	public void setcBuysell(String cBuysell) {
		setData(cBuysell, C_BUYSELL);
	}

	public String getcSecid() {
		return (String) getData(C_SECID);
	}

	public void setcSecid(String cSecid) {
		setData(cSecid, C_SECID);
	}

	public Double getcPirce() {
		return (Double) getData(C_PIRCE);
	}

	public void setcPirce(Double cPirce) {
		setData(cPirce, C_PIRCE);
	}

	public Double getcLot() {
		return (Double) getData(C_LOT);
	}

	public void setcLot(Double cLot) {
		setData(cLot, C_LOT);
	}

	public String getcDescription() {
		return (String) getData(C_DESCRIPTION); 
	}

	public void setcDescription(String cDescription) {
		setData(cDescription, C_DESCRIPTION);
	}

	public Double getcVolume() {
		return (Double) getData(C_VOLUME);
	}

	public void setcVolume(Double cVolume) {
		setData(cVolume, C_VOLUME);
	}

	public String getcLabel() {
		return (String) getData(C_LABEL);
	}

	public void setcLabel(String cLabel) {
		setData(cLabel, C_LABEL);
	}

	public String getcValidity() {
		return (String) getData(C_VALIDITY);
	}

	public void setcValidity(String cValidity) {
		setData(cValidity, C_VALIDITY);
	}

	public Date getcValiduntil() {
		return (Date) getData(C_VALIDUNTIL);
	}

	public void setcValiduntil(Date cValiduntil) {
		setData(cValiduntil, C_VALIDUNTIL);
	}

	public String getcAtsstatus() {
		return (String) getData(C_ATSSTATUS);
	}

	public void setcAtsstatus(String cAtsstatus) {
		setData(cAtsstatus, C_ATSSTATUS);
	}

	public String getcOrderid() {
		return (String) getData(C_ORDERID);
	}

	public void setcOrderid(String cOrderid) {
		setData(cOrderid, C_ORDERID);
	}

	public String getcOrderstatus() {
		return (String) getData(C_ORDERSTATUS);
	}

	public void setcOrderstatus(String cOrderstatus) {
		setData(cOrderstatus, C_ORDERSTATUS);
	}

	public String getcConditiontype() {
		return (String) getData(C_CONDITIONTYPE);
	}

	public void setcConditiontype(String cConditiontype) {
		setData(cConditiontype, C_CONDITIONTYPE);
	}

	public String getcConditionmethod() {
		return (String) getData(C_CONDITIONMETHOD);
	}

	public void setcConditionmethod(String cConditionmethod) {
		setData(cConditionmethod, C_CONDITIONMETHOD);
	}

	public Double getcComparelastprice() {
		return (Double) getData(C_COMPARELASTPRICE);
	}

	public void setcComparelastprice(Double cComparelastprice) {
		setData(cComparelastprice, C_COMPARELASTPRICE);
	}

	public Double getcRemainbidlot() {
		return (Double) getData(C_REMAINBIDLOT);
	}

	public void setcRemainbidlot(Double cRemainbidlot) {
		setData(cRemainbidlot, C_REMAINBIDLOT);
	}

	public Double getcCumulativvedone() {
		return (Double) getData(C_CUMULATIVVEDONE);
	}

	public void setcCumulativvedone(Double cCumulativvedone) {
		setData(cCumulativvedone, C_CUMULATIVVEDONE);
	}

	
	
	public Double getcCumulativecalc() {
		return (Double) getData(C_CUMULATIVECALC);
	}

	public void setcCumulativecalc(Double cCumulativecalc) {
		setData(cCumulativecalc, C_CUMULATIVECALC);
	}

	public Double getcReversallimit() {
		return (Double) getData(C_REVERSALLIMIT);
		}

	public void setcReversallimit(Double cReversallimit) {
		setData(cReversallimit, C_REVERSALLIMIT);
	}

	public String getcBdFilter() {
		return (String) getData(C_BD_FILTER);
	}

	public void setcBdFilter(String cBdFilter) {
		setData(cBdFilter, C_BD_FILTER);
	}

	public String getcBdRefcode() {
		return (String) getData(C_BD_REFCODE);
	}

	public void setcBdRefcode(String cBdRefcode) {
		setData(cBdRefcode, C_BD_REFCODE);
	}

	public String getcBdRefopt() {
		return (String) getData(C_BD_REFOPT);
	}

	public void setcBdRefopt(String cBdRefopt) {
		setData(cBdRefopt, C_BD_REFOPT);
	}

	

	public String getcBdDonetype() {
		return (String) getData(C_BD_DONETYPE);
	}

	public void setcBdDonetype(String cBdDonetype) {
		setData(cBdDonetype, C_BD_DONETYPE);
	}

	public Double getcBdDonespec() {
		return (Double) getData(C_BD_DONESPEC);
	}

	public void setcBdDonespec(Double cBdDonespec) {
		setData(cBdDonespec, C_BD_DONESPEC);
	}

	public String getcEntrybuy() {
		return (String) getData(C_ENTRYBUY);
	}

	public void setcEntrybuy(String cEntrybuy) {
		setData(cEntrybuy, C_ENTRYBUY);
	}

	public Date getcEntrytime() {
		return (Date) getData(C_ENTRYTIME);
	}

	public void setcEntrytime(Date cEntrytime) {
		setData(cEntrytime, C_ENTRYTIME);
	}

	public String getcEntryterminal() {
		return (String) getData(C_ENTRYTERMINAL);
	}

	public void setcEntryterminal(String cEntryterminal) {
		setData(cEntryterminal, C_ENTRYTERMINAL);
	}

	public String getcExecby() {
		return (String) getData(C_EXECBY);
	}

	public void setcExecby(String cExecby) {
		setData(cExecby, C_EXECBY) ;
	}

	public Date getcExectime() {
		return (Date) getData(C_EXECTIME);
	}

	public void setcExectime(Date date) {
	   setData( date, C_EXECTIME)	;
	}

	public String getcWithdrawby() {
		return (String) getData(C_WITHDRAWBY);
	}

	public void setcWithdrawby(String cWithdrawby) {
		setData( cWithdrawby, C_WITHDRAWBY);
	}

	public Date getcWithdrawtime() {
		return (Date)getData(C_WITHDRAWTIME);
	}

	public void setcWithdrawtime(Date cWithdrawtime) {
		setData(cWithdrawtime,C_WITHDRAWTIME );
	}

	public String getcWithdrawterminal() {
		return (String)getData(C_WITHDRAWTERMINAL);
	}

	public void setcWithdrawterminal(String cWithdrawterminal) {
		setData( cWithdrawterminal,C_WITHDRAWTERMINAL);
	}

	public Double getcFlag1() {
		return (Double) getData(C_FLAG1);
	}

	public void setcFlag1(Double cFlag1) {
		setData( cFlag1,C_FLAG1 );
	}

	
 	public String getcInvtype() {
		return (String) getData(C_INVTYPE);
	}

	public  void setcInvtype(String cInvtype) {
		setData( cInvtype,C_INVTYPE );		
	}
	
	public String getComplianceId() {
		return (String) getData(C_COMPLAINCEID);
	}

	public  void setComplianceId(String cInvtype) {
		setData( cInvtype,C_COMPLAINCEID );		
	}
}
