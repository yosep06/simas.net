package eqtrade.trading.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.trading.core.SelectedBorder;
import eqtrade.trading.core.TradingSetting;

public class PortfolioRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     private String strFieldName = new String("");
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	 private static NumberFormat formatter2 = new DecimalFormat("#,##0.00  ");
    private static Color newBack;
    private static Color newFore;
    private static SelectedBorder border = new SelectedBorder();
	private static Portfolio pf;
	 
	 @Override
	public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        strFieldName = table.getColumnName(column);
		newBack =  (column % 2) == 0 ? TradingSetting.getColor(TradingSetting.C_BACKGROUND) : TradingSetting.getColor(TradingSetting.C_BACKGROUNDEVEN);

	 	if (value instanceof MutableIData){
	  		pf = (Portfolio)((MutableIData)value).getSource();
	  		if (pf.getPLValue().doubleValue()>0){
	            newFore = TradingSetting.getColor(TradingSetting.C_PLUS);   
	  		} else if (pf.getPLValue().doubleValue()<0){
	            newFore = TradingSetting.getColor(TradingSetting.C_MINUS);   
	  		} else {
	  			newFore = TradingSetting.getColor(TradingSetting.C_ZERO);
	  		}
	 	} 
	 	
	 	if  (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);          
	 	return component;
    }
	
	 @Override
	public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? SwingConstants.RIGHT : SwingConstants.LEFT);
	             if (dat instanceof Double) {
            	 	 if (strFieldName.equals(PortfolioDef.dataHeader[Portfolio.C_LASTPRICE]) ||
                         strFieldName.equals(PortfolioDef.dataHeader[Portfolio.C_AVGPRICE])  ){
            	 		 	setText(formatter2.format(dat));
            	 	 } else if (strFieldName.equals(PortfolioDef.dataHeader[Portfolio.C_MYACC])){
            	 		 	setText(((Double)dat).doubleValue()==1?"Yes":"No");
	            	 } else if (strFieldName.equals(PortfolioDef.dataHeader[Portfolio.C_PLPERCENT])) {
	            		Double dob = (Double) dat;
	            		 	if(dob.isNaN() ){
	            		 		setText("0");
	            		 	}else if ( dob.isInfinite()) {
	            		 		setText("-");
							} else{
	            		 		setText(formatter2.format(dat));
	            		 	}
	            		 	
	            	 } else {
	            		 	setText(formatter.format(dat));
	            	 }
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
