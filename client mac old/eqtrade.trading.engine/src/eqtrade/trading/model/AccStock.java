package eqtrade.trading.model;


public final class AccStock extends Model {
    private static final long serialVersionUID = 309177931626178221L;
    public static final int    C_ACCTYPE = 0;
    public static final int	C_STOCK = 1;
    public static final int    C_HAIRCUT = 2;
	public static final int 	CIDN_NUMBEROFFIELDS = 3;

	public AccStock(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public AccStock(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
	}	

	public String getStock(){
		return (String) getData(C_STOCK);
	}
	public void setStock(String sinput){
		setData(sinput, C_STOCK);
	}
	public String getAccType(){
		return (String) getData(C_ACCTYPE);
	}
	public void setAccType(String sinput){
		setData(sinput, C_ACCTYPE);
	}	
	public String getHaircut(){
		return (String) getData(C_HAIRCUT);
	}
	public void setHaircut(String sinput){
		setData(sinput, C_HAIRCUT);
	}    
}