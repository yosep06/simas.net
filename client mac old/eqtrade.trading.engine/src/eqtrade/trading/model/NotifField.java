package eqtrade.trading.model;

import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class NotifField extends Column implements MutableIDisplayIData{

	public NotifField(Row source, int idx) throws Exception {
		super(source, idx);
		// TODO Auto-generated constructor stub
	}

	@Override
	public ImmutableIDisplay getImmutableIDisplay() {
		// TODO Auto-generated method stub
		return NotifDef.render;
	}

	@Override
	public MutableIDisplay getMutableIDisplay() {
		// TODO Auto-generated method stub
		return NotifDef.render;
	}

}
