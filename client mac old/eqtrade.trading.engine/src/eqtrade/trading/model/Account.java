package eqtrade.trading.model;

import eqtrade.trading.core.Utils;

public final class Account extends Model {
    private static final long serialVersionUID = -7326173869763611211L;
    public static final int  C_TRADINGID = 0;
    public static final int  C_BRANCHID = 1;
    public static final int  C_SALESID = 2;
    public static final int  C_CUSTTYPE = 3;
    public static final int  C_CREDITLIMIT = 4;
    public static final int  C_CURRENTRATIO = 5;
    public static final int  C_CURRTL = 6;
    public static final int  C_DEPOSIT = 7;
    public static final int  C_BUY = 8;
    public static final int  C_SELL = 9;
    public static final int  C_NETAC = 10;
    public static final int  C_BID = 11;
    public static final int  C_OFFER = 12;
    public static final int  C_STOCKVAL = 13;
    public static final int  C_MARKETVAL = 14;
    public static final int  C_LQVALUE = 15;
    public static final int  C_TOPUP = 16;
    public static final int  C_FORCESELL = 17;
    public static final int  C_MYACC = 18;
    public static final int  C_ACCID = 19;
	public static final int  C_NAME = 20;
	public static final int  C_CUSTID = 21;
    public static final int  C_STATUS = 22;
    public static final int	C_ISCORPORATE = 23;
    public static final int  C_INVTYPE = 24;
    public static final int  C_PHONE = 25;
    public static final int  C_HANDPHONE = 26;
    public static final int  C_EMAIL = 27;
    public static final int  C_SUBACCOUNT = 28;
    public static final int	C_COMPLIANCEID = 29;
    public static final int  C_ADDRESS = 30;    
    public static final int  C_ACCTYPE = 31;
    public static final int  C_WITHDRAW = 32;
    public static final int	C_SINGLEID = 33;
    public static final int C_VIRTUALACCOUNT = 34;
    public static final int C_INVESTORACCOUNT = 35;
    public static final int C_ATS = 37;
    public static final int C_EXPKTP = 38;//yosep expktp
	public static final int CIDN_NUMBEROFFIELDS = 39;
 

	public Account(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Account(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 5: case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13: case 14: case 15: case 16: case 17: case 32:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}

	public String getAccId(){
		return (String) getData(C_ACCID);
	}
	public void setAccId(String sinput){
		setData(sinput, C_ACCID);
	}
	public String getBranchId(){
		return (String) getData(C_BRANCHID);
	}
	public void setBranchId(String sinput){
		setData(sinput, C_BRANCHID);
	}
	public String getComlianceId(){
		return (String) getData(C_COMPLIANCEID);
	}
	public void setComplianceId(String sinput){
		setData(sinput, C_COMPLIANCEID);
	}
	public String getSalesId(){
		return (String) getData(C_SALESID);
	}
	public void setSalesId(String sinput){
		setData(sinput, C_SALESID);
	}
	
    public String getAddress(){
        return (String) getData(C_ADDRESS);
    }
    public void setAddress(String sinput){
        setData(sinput, C_ADDRESS);
    }
    
    public String getAccType(){
        return (String) getData(C_ACCTYPE);
    }
    public void setAccType(String sinput){
        setData(sinput, C_ACCTYPE);
    }
    public String getCustId(){
        return (String) getData(C_CUSTID);
    }
    public void setCustId(String sinput){
        setData(sinput, C_CUSTID);
    }
    public String getIsCorporate(){
        return (String) getData(C_ISCORPORATE);
    }
    public void setIsCorporate(String sinput){
        setData(sinput, C_ISCORPORATE);
    }    
    public String getName(){
        return (String) getData(C_NAME);
    }
    public void setName(String sinput){
        setData(sinput, C_NAME);
    }
    public Double getCreditLimit(){
        return (Double) getData(C_CREDITLIMIT);
    }
    public void setCreditLimit(Double sinput){
        setData(sinput, C_CREDITLIMIT);
    }
    public Double getCurrentRatio(){
        return (Double) getData(C_CURRENTRATIO);
    }
    public void setCurrentRatio(Double sinput){
        setData(sinput, C_CURRENTRATIO);
    }
    public Double getLQValue(){
        return (Double) getData(C_LQVALUE);
    }
    public void setLQValue(Double sinput){
        setData(sinput, C_LQVALUE);
    }
    public Double getDeposit(){
        return (Double) getData(C_DEPOSIT);
    }
    public void setDeposit(Double sinput){
        setData(sinput, C_DEPOSIT);
    }
    public Double getBuy(){
        return (Double) getData(C_BUY);
    }
    public void setBuy(Double sinput){
        setData(sinput, C_BUY);
    }
    public Double getSell(){
        return (Double) getData(C_SELL);
    }
    public void setSell(Double sinput){
        setData(sinput, C_SELL);
    }
    public Double getNetAC(){
        return (Double) getData(C_NETAC);
    }
    public void setNetAC(Double sinput){
        setData(sinput, C_NETAC);
    }
    public Double getBid(){
        return (Double) getData(C_BID);
    }
    public void setBid(Double sinput){
        setData(sinput, C_BID);
    }
    public Double getOffer(){
        return (Double) getData(C_OFFER);
    }
    public void setOffer(Double sinput){
        setData(sinput, C_OFFER);
    }
    public Double getStockVal(){
        return (Double) getData(C_STOCKVAL);
    }
    public void setStockVal(Double sinput){
        setData(sinput, C_STOCKVAL);
    }
    public Double getMarketVal(){
        return (Double) getData(C_MARKETVAL);
    }
    public void setMarketVal(Double sinput){
        setData(sinput, C_MARKETVAL);
    }
    public Double getCurrTL(){
        return (Double) getData(C_CURRTL);
    }
    public void setCurrTL(Double sinput){
        setData(sinput, C_CURRTL);
    }
    public String getStatus(){
        return (String) getData(C_STATUS);
    }
    public void setStatus(String sinput){
        setData(sinput, C_STATUS);
    }
    public String getInvType(){
        return (String) getData(C_INVTYPE);
    }
    public void setInvType(String sinput){
        setData(sinput, C_INVTYPE);
    }
    public String getCustType(){
        return (String) getData(C_CUSTTYPE);
    }
    public void setCustType(String sinput){
        setData(sinput, C_CUSTTYPE);
    }
    public String getTradingId(){
        return (String) getData(C_TRADINGID);
    }
    public void setTradingId(String  sinput){
        setData(sinput, C_TRADINGID);
    }
    public String getSubAccount(){
        return (String) getData(C_SUBACCOUNT);
    }
    public void setSubAccount(String sinput){
        setData(sinput, C_SUBACCOUNT);
    }
    public String getPhone(){
        return (String) getData(C_PHONE);
    }
    public void setPhone(String sinput){
        setData(sinput, C_PHONE);
    }    
    public String getEmail(){
        return (String) getData(C_EMAIL);
    }
    public void setEmail(String sinput){
        setData(sinput, C_EMAIL);
    }    
    public String getHandphone(){
        return (String) getData(C_HANDPHONE);
    }
    public void setHandphone(String sinput){
        setData(sinput, C_HANDPHONE);
    }    
    public Double getTopup(){
        return (Double) getData(C_TOPUP);
    }
    public void setTopup(Double sinput){
        setData(sinput, C_TOPUP);
    }
    public Double getForceSell(){
        return (Double) getData(C_FORCESELL);
    }
    public void setForceSell(Double sinput){
        setData(sinput, C_FORCESELL);
    }
    public boolean isMyAccount(){
        return ((Double)getData(C_MYACC)).doubleValue() == 1;
    }
    public void setMyAccount(boolean sinput){
        setData(new Double(sinput?"1":"0"), C_MYACC);
    }    
    public Double getWithdraw(){
        return (Double) getData(C_WITHDRAW);
    }
    public void setWithdraw(Double sinput){
        setData(sinput, C_WITHDRAW);
    }
    public String getSingleID() {
    	return (String) getData(C_SINGLEID);
    }
    public void setSingleID(String sinput) {
    	setData(sinput, C_SINGLEID);
    }
    public String getVirtualAccount() {
    	return (String) getData(C_VIRTUALACCOUNT);
    }
    public void setVirtualAccount(String sinput) {
    	setData(sinput, C_VIRTUALACCOUNT);
    }
    public String getInvestorAccount() {
    	return (String) getData(C_INVESTORACCOUNT);
    }
    public void setInvestorAccount(String sinput) {
    	setData(sinput, C_INVESTORACCOUNT);
    }
	public String getAts() {
		return (String)getData(C_ATS);
	}
	public void setAts(String sinput) {
		setData(sinput , C_ATS);
	}
	
	//yosep expktp
	public void setExpktp(String sinput) {
		setData(sinput , C_EXPKTP);
	}
	public String getcExpktp() {
		return (String)getData(C_EXPKTP);
	}
}