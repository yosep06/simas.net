package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class NegDealListDef {
	public static Hashtable getTableDef() {
		Hashtable htable = new Hashtable();
		htable.put("header", dataHeader);
		htable.put("alignment", defaultHeaderAlignment);
		htable.put("width", defaultColumnWidth);
		htable.put("order", defaultColumnOrder);
		htable.put("sorted", columnsort);
		htable.put("sortcolumn", new Integer(0));
		htable.put("sortascending", new Boolean(true));
		return htable;
	}

	/*public static Hashtable getTableAdverDef() {
		Hashtable htable = new Hashtable();
		htable.put("header", dataHeader);
		htable.put("alignment", defaultHeaderAlignment);
		htable.put("width", defaultColumnWidth2);
		htable.put("order", defaultColumnOrder2);
		htable.put("sorted", columnsort);
		htable.put("sortcolumn", new Integer(0));
		htable.put("sortascending", new Boolean(true));
		return htable;
	}*/

	public static String[] dataHeader = new String[] { "MarketOrderID",
			"MarketREF", "Advertisiment", "OriginalOrderID", "UserID",
			"BrokerID", "OrderTime", "Status", "InvType", "SecID", "BoardID",
			"SecurityID", "B/S", "Volume", "Price", "Balance", "OrdType",
			"OrderExpire", "TradeTime", "Note", "ContraBrokerID",
			"ContraUserID", "ComplianceID", "Amount" };

	 public static int[] defaultHeaderAlignment = new int[]{
			SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,SwingConstants.CENTER
	    };
	
	/*public static int[] defaultHeaderAlignment = new int[] {
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER, 
			SwingConstants.CENTER,SwingConstants.CENTER,
			SwingConstants.CENTER,SwingConstants.CENTER,
			SwingConstants.CENTER,SwingConstants.CENTER };*/

	/*public static int[] defaultColumnWidth = new int[] { 100, 100, 100, 100,
			100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
			100, 100, 00, 00, 00, 00, 00, 00, 00, 00, 00 };*/
	 
	 public  static int[] defaultColumnWidth = new int[]{
			100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100
	};

	public static int[] defaultColumnWidth2 = new int[] { 100, 100, 100, 100,
			100, 100, 100, 100, 100, 100, 100, 00, 00, 00, 00, 00, 00, 00, 00,
			00, 00, 00, 00, 00, 00, 00, 00, 00 };

	
	  public static int[] defaultColumnOrder = new int[]{
	  0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 };
	 

	/*public static int[] defaultColumnOrder2 = new int[] {
			NegDealList.C_CONTRABROKERID, NegDealList.C_CONTRAUSERID,
			NegDealList.C_ORDERDATE, NegDealList.C_ORIGINALORDERID,
			NegDealList.C_MARKETORDERID, NegDealList.C_BUYSELL,
			NegDealList.C_SECID, NegDealList.C_PRICE,
			NegDealList.C_VOLUME,
			NegDealList.C_AMOUNT,
			NegDealList.C_STATUSID,

			// hide
			NegDealList.C_BRDID, NegDealList.C_NOTE, NegDealList.C_MATCHTIME,
			NegDealList.C_DONELOT, NegDealList.C_BALANCE,
			NegDealList.C_AMENDTIME, NegDealList.C_WITHDRAWTIME,
			NegDealList.C_ORDERTYPE, 1, 2, 4, 5, 8, 11, 16, 18, 22 };*/

	/*public static int[] defaultColumnOrder = new int[] {
			NegDealList.C_CONTRAUSERID, NegDealList.C_ORDERDATE,
			NegDealList.C_ORIGINALORDERID, NegDealList.C_MARKETORDERID,
			NegDealList.C_BRDID, NegDealList.C_CONTRABROKERID,
			NegDealList.C_BUYSELL, NegDealList.C_SECID, NegDealList.C_PRICE,
			NegDealList.C_VOLUME, NegDealList.C_AMOUNT, NegDealList.C_STATUSID,
			NegDealList.C_NOTE, NegDealList.C_MATCHTIME, NegDealList.C_DONELOT,
			NegDealList.C_BALANCE, NegDealList.C_AMENDTIME,
			NegDealList.C_WITHDRAWTIME, NegDealList.C_ORDERTYPE,
			// hide
			1, 2, 4, 5, 8, 11, 16, 18, 22 };*/

	public static boolean[] columnsort = new boolean[] { true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true };

	public static Vector getHeader() {
		Vector header = new Vector(NegDealList.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}

	public static List createTableRow(Row row) {
		List vec = new Vector(NegDealList.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < NegDealList.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new NegDealListField(row, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}

	public final static NDLRender render = new NDLRender();

	static class NDLRender extends MutableIDisplayAdapter {
		DefaultTableCellRenderer renderer = new NegDealListRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			renderer.getTableCellRendererComponent(table, value, isSelected,
					hasFocus, row, column);
			((NegDealListRender) renderer).setValue(value);
			return renderer;
		}
	}
}
