package eqtrade.trading.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import eqtrade.trading.core.Utils;


public final class NegDealList extends Model {
    private static final long serialVersionUID = 309177931626178221L;
    public static final int    C_MARKETORDERID = 0;
    public static final int	C_MARKETREF = 1;
    public static final int	C_ISADVERTISING = 2;
    public static final int	C_ORIGINALORDERID = 3;
    public static final int	C_USERID = 4;
    public static final int	C_BROKERID = 5;
    public static final int	C_ORDERDATE = 6;
    public static final int	C_STATUSID = 7;
    public static final int	C_INVTYPE = 8;
    public static final int	C_SECID = 9;
    public static final int	C_BRDID = 10;
    public static final int	C_SECURITYID = 11;
    public static final int	C_BUYSELL = 12;
    public static final int    C_VOLUME = 13;
    public static final int	C_PRICE = 14;
    public static final int    C_BALANCE = 15;
    public static final int    C_ORDTYPE = 16;
    public static final int    C_ORDERTYPE = 17;
    public static final int    C_TRADEDATE = 18;
    public static final int    C_NOTE = 19;
    public static final int    C_CONTRABROKERID = 20;
    public static final int    C_CONTRAUSERID = 21;
    public static final int    C_COMPLIANCEID = 22;
    public static final int    C_AMOUNT = 23;   
	public static final int 	CIDN_NUMBEROFFIELDS = 24;
    public final static SimpleDateFormat formatdate = new SimpleDateFormat("HHmmss");

	public NegDealList(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public NegDealList(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 13: case 14: case 15: case 24: 
			vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
		 	case 6: case 18: 
		 		if (!vdata.elementAt(i).equals(""))
                    try {
                        vdata.setElementAt(formatdate.parse((String)vdata.elementAt(i)), i);
                    } catch (Exception ex){vdata.setElementAt(null,i);}
                else 
                    vdata.setElementAt(null, i);
				break;
			default:
				break;
			}
		}		
	}	

	public String getMarketRef(){
		return (String) getData(C_MARKETREF);
	}
	public void setMarketRef(String sinput){
		setData(sinput, C_MARKETREF);
	}
	public String getMarketOrderID(){
		return (String) getData(C_MARKETORDERID);
	}
	public void setMarketOrderID(String sinput){
		setData(sinput, C_MARKETORDERID);
	}	
	public String getIsAdvertising(){
		return (String) getData(C_ISADVERTISING);
	}
	public void setIsAdvertising(String sinput){
		setData(sinput, C_ISADVERTISING);
	}	
	public String getOriginalOrderID(){
		return (String) getData(C_ORIGINALORDERID);
	}
	public void setOriginalOrderID(String sinput){
		setData(sinput, C_ORIGINALORDERID);
	}	
	public String getUserID(){
		return (String) getData(C_USERID);
	}
	public void setUserID(String sinput){
		setData(sinput, C_USERID);
	}	
	public String getBrokerID(){
		return (String) getData(C_BROKERID);
	}
	public void setBrokerID(String sinput){
		setData(sinput, C_BROKERID);
	}	
	public Date getOrderDate(){
		return (Date) getData(C_ORDERDATE);
	}
	public void setOrderDate(Date sinput){
		setData(sinput, C_ORDERDATE);
	}	
	public String getStatusID(){
		return (String) getData(C_STATUSID);
	}
	public void setStatusID(String sinput){
		setData(sinput, C_STATUSID);
	}	
	public String getInvType(){
		return (String) getData(C_INVTYPE);
	}
	public void setInvType(String sinput){
		setData(sinput, C_INVTYPE);
	}	
	public String getSecID(){
		return (String) getData(C_SECID);
	}
	public void setSecID(String sinput){
		setData(sinput, C_SECID);
	}	
	public String getBrdID(){
		return (String) getData(C_BRDID);
	}
	public void setBrdID(String sinput){
		setData(sinput, C_BRDID);
	}	
	public String getSecurityID(){
		return (String) getData(C_SECURITYID);
	}
	public void setSecurityID(String sinput){
		setData(sinput, C_SECURITYID);
	}	
	public String getBuySell(){
		return (String) getData(C_BUYSELL);
	}
	public void setBuySell(String sinput){
		setData(sinput, C_BUYSELL);
	}		
	public Double getVolume(){
		return (Double) getData(C_VOLUME);
	}
	public void setVolume(Double sinput){
		setData(sinput, C_VOLUME);
	}    
	public Double getPrice(){
		return (Double) getData(C_PRICE);
	}
	public void setPrice(Double sinput){
		setData(sinput, C_PRICE);
	}    
	public Double getBalance(){
		return (Double) getData(C_BALANCE);
	}
	public void setBalance(Double sinput){
		setData(sinput, C_BALANCE);
	}    
	public String getOrdType(){
		return (String) getData(C_ORDTYPE);
	}
	public void setOrdType(String sinput){
		setData(sinput, C_ORDERTYPE);
	}		
	public String getOrderType(){
		return (String) getData(C_ORDERDATE);
	}
	public void setOrderType(String sinput){
		setData(sinput, C_ORDERTYPE);
	}		
	public Date getTradeDate(){
		return (Date) getData(C_TRADEDATE);
	}
	public void setTradeDate(Date sinput){
		setData(sinput, C_TRADEDATE);
	}		
	public String getNote(){
		return (String) getData(C_NOTE);
	}
	public void setNote(String sinput){
		setData(sinput, C_NOTE);
	}		
	public String getContraBrokerID(){
		return (String) getData(C_CONTRABROKERID);
	}
	public void setContraBrokerID(String sinput){
		setData(sinput, C_CONTRABROKERID);
	}		
	public String getContraUserID(){
		return (String) getData(C_CONTRAUSERID);
	}
	public void setContraUserID(String sinput){
		setData(sinput, C_CONTRAUSERID);
	}		
	public String getComplianceID(){
		return (String) getData(C_COMPLIANCEID);
	}
	public void setComplianceID(String sinput){
		setData(sinput, C_COMPLIANCEID);
	}		
	
	
	
}