package eqtrade.trading.model;

import eqtrade.trading.core.Utils;

public final class Portfolio extends Model {
    private static final long serialVersionUID = 309177931626178221L;
    public static final int  C_TRADINGID = 0;
	public static final int	C_STOCK = 1;
	public static final int	C_BEGVOLUME = 2;
	public static final int	C_AVAILABLEVOLUME = 3;
	public static final int	C_STOCKVAL = 4;
    public static final int   C_AVGPRICE = 5;
	public static final int	C_CLOSINGPRICE = 6;
    public static final int   C_MYACC = 7;
    public static final int	C_ACCOUNTID = 8;
    public static final int	C_CURRVOLUME = 9;
	public static final int	C_LASTPRICE = 10;
    public static final int  C_LOTSIZE = 11;
    public static final int  C_VALUELASTPRICE = 12;
    public static final int  C_BEGLOT = 13;
    public static final int	C_AVAILABLELOT=14;
    public static final int	C_CURRLOT = 15;
    public static final int	C_PL = 16;
    public static final int	C_PLPERCENT = 17;
	public static final int 	CIDN_NUMBEROFFIELDS = 18;
    
	public Portfolio(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Portfolio(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 2: case 3: case 4: case 5: case 6: case 9: case 10: case 11: case 12: case 13: case 14: case 15: case 16: case 17:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
		calculate();
	}
		
	public void calculate(){
        calculatePrice();
        calculateLot();
	}
    
    private void calculatePrice(){
    	//double lastPrice =0.00;
        //if (getLastPrice().doubleValue()>0.00) {
    	double lastPrice = getLastPrice().doubleValue();
        double lotSize = getLotSize().doubleValue();
        //} else {
        //	lastPrice = getClosingPrice().doubleValue();
        //};
    	setStockVal(new Double(getAvailableVolume().doubleValue()*getAvgPrice().doubleValue()));
        setValueLastPrice(new Double(getAvailableVolume().doubleValue()*lastPrice));
        setPLValue(new Double(getValueLastPrice().doubleValue() - getStockVal().doubleValue()));
        setPLPercent(new Double(getPLValue().doubleValue() * 100 / getStockVal().doubleValue()));    
//        setPLValue(new Double( (getLastPrice().doubleValue()-getAvgPrice().doubleValue())*getCurrVolume().doubleValue()) );
//        setPLPercent(new Double( (getLastPrice().doubleValue()*100/getAvgPrice().doubleValue())));
    }
    
    private void calculateLot(){
        double lotSize = getLotSize().doubleValue();
        setBegLot(new Double(Math.floor(getBegVolume().doubleValue() / lotSize)));
        setCurrLot(new Double(Math.floor(getCurrVolume().doubleValue() / lotSize)));
        setAvailableLot(new Double(Math.floor(getAvailableVolume().doubleValue() / lotSize)));
    }
    
    public boolean isMyAccount(){
        return ((Double)getData(C_MYACC)).doubleValue() == 1;
    }
    public void setMyAccount(boolean sinput){
        setData(new Double(sinput?"1":"0"), C_MYACC);
    }    
	public String getAccountId(){
		return (String) getData(C_ACCOUNTID);
	}
	public void setAccountId(String sinput){
		setData(sinput, C_ACCOUNTID);
	}
	public String getTradingId(){
		return (String) getData(C_TRADINGID);
	}
	public void setTradingId(String sinput){
		setData(sinput, C_TRADINGID);
	}	
	public String getStock(){
		return (String) getData(C_STOCK);
	}
	public void setStock(String sinput){
		setData(sinput, C_STOCK);
	}
    
    public Double getBegVolume(){
        return (Double) getData(C_BEGVOLUME);
    }
    public void setBegVolume(Double sinput){
        setData(sinput, C_BEGVOLUME);
    }

    public Double getAvailableVolume(){
        return (Double) getData(C_AVAILABLEVOLUME);
    }
    public void setAvailableVolume(Double sinput){
        setData(sinput, C_AVAILABLEVOLUME);
        calculate();
    }

    public Double getCurrVolume(){
        return (Double) getData(C_CURRVOLUME);
    }
    public void setCurrVolume(Double sinput){
        setData(sinput, C_CURRVOLUME);
        calculate();
    }

    public Double getStockVal(){
		return (Double) getData(C_STOCKVAL);
	}
	public void setStockVal(Double sinput){
		setData(sinput, C_STOCKVAL);
	}

	public Double getClosingPrice(){
			
		return (Double) getData(C_CLOSINGPRICE);
	}
	public void setClosingPrice(Double sinput){
		setData(sinput, C_CLOSINGPRICE);
		calculate();
	}
	
	public Double getLastPrice(){
		
		if ((Double) getData(C_LASTPRICE)>0.00) { 
			return (Double) getData(C_LASTPRICE);
		} else {
			return (Double) getData(C_CLOSINGPRICE);
		}
		
	}
	public void setLastPrice(Double sinput){
		if (sinput>0.00) {
			setData(sinput, C_LASTPRICE);
		} else {
			setData((Double) getData(C_CLOSINGPRICE), C_LASTPRICE);
		}
		calculate();
	}
    public Double getBegLot(){
        return (Double) getData(C_BEGLOT);
    }
    public void setBegLot(Double sinput){
        setData(sinput, C_BEGLOT);
    }
    public Double getAvailbaleLot(){
        return (Double) getData(C_AVAILABLELOT);
    }
    public void setAvailableLot(Double sinput){
        setData(sinput, C_AVAILABLELOT);
    }
    public Double getCurrLot(){
        return (Double) getData(C_CURRLOT);
    }
    public void setCurrLot(Double sinput){
        setData(sinput, C_CURRLOT);
    }
    public Double getPLPercent(){
        return (Double) getData(C_PLPERCENT);
    }
    public void setPLPercent(Double sinput){
        setData(sinput, C_PLPERCENT);
    }
    public Double getPLValue(){
        return (Double) getData(C_PL);
    }
    public void setPLValue(Double sinput){
        setData(sinput, C_PL);
    }
    public Double getValueLastPrice(){
        return (Double) getData(C_VALUELASTPRICE);
    }
    public void setValueLastPrice(Double sinput){
        setData(sinput, C_VALUELASTPRICE);
    }
    public Double getAvgPrice(){
        return (Double) getData(C_AVGPRICE);
    }
    public void setAvgPrice(Double sinput){
        setData(sinput, C_AVGPRICE);
    }
    
    public Double getLotSize(){
        return (Double) getData(C_LOTSIZE);
    }
    public void setLotSize(Double sinput){
        setData(sinput, C_LOTSIZE);
        calculate();
    }
}