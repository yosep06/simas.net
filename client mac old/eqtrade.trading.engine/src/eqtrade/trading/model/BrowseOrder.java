package eqtrade.trading.model;

public class BrowseOrder extends Model {

	public static final int C_ORDERID = 0;
	public static final int C_SECID = 1;
	public static final int C_BUYORSELL = 2;
	public static final int C_PRICE = 3;
	public static final int C_VOLUME = 4;
	public static final int C_ATSSTATUS = 5;
	public static final int C_LABEL = 6;
	public static final int C_CLIENTID = 7;
	
	public static final int CIDN_NUMBEROFFIELDS = 8;
	
	public BrowseOrder(){
		super(CIDN_NUMBEROFFIELDS);
	}
	public BrowseOrder(String msg){
		super(CIDN_NUMBEROFFIELDS,msg);
	}
	@Override
	protected void convertType() {
		// TODO Auto-generated method stub
		
	}

	public  String getOrderid() {
		return (String) getData(C_ORDERID);
	}

	public  void setOrderid(String cOrderid) {
		setData(cOrderid,C_ORDERID);
	}

	public  String getSecid() {
		return (String) getData(C_SECID);
	}

	public  void setSecid(String cSecid) {
		setData(cSecid,C_SECID);
	}

	public String getBuyorsell() {
		return (String) getData(C_BUYORSELL);
	}

	public  void setBuyorsell(String cBuyorsell) {
		setData(cBuyorsell,C_BUYORSELL);
	}

	public Double getPrice() {
		return (Double) getData(C_PRICE);
	}

	public  void setPrice(Double cPrice) {
		setData(cPrice,C_PRICE) ;
	}

	public Double getVolume() {
		return (Double) getData(C_VOLUME);
	}

	public  void setVolume(Double cVolume) {
		setData(cVolume,C_VOLUME);
	}

	public String getAtsstatus() {
		return (String) getData(C_ATSSTATUS);
	}

	public  void setAtsstatus(String cAtsstatus) {
		setData(cAtsstatus,C_ATSSTATUS);
	}

	public  String getLabel() {
		return (String)getData(C_LABEL);
	}

	public  void setLabel(String cLabel) {
		setData(cLabel,C_LABEL);
	}
	public String getClientid() {
		return (String)getData(C_CLIENTID) ;
	}
	public void setClientid(String Clientid) {
		setData(Clientid, C_CLIENTID);
	}
}
