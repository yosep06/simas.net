package eqtrade.feed.engine.socket.blockrequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eqtrade.feed.core.IFeedEngine;

public class LoginRequest extends BaseBlock {
	public LoginRequest(String _type, IFeedEngine _engine) {
		super(_type, _engine);
	}



	private Log log = LogFactory.getLog(getClass());

	

	@Override
	protected Object parsing(Object dt) {
		
		String st = (String) dt;
		//log.info("parsing receive msg "+st);
		return Long.parseLong(st);
	}

}
