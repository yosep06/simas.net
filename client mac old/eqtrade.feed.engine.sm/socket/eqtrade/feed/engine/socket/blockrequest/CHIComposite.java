package eqtrade.feed.engine.socket.blockrequest;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.engine.socket.BlockRequestSynchronized;

public class CHIComposite extends BaseBlock {

	
	public CHIComposite(String _type, IFeedEngine _engine) {
		super(_type, _engine);
	}

	@Override
	protected Object parsing(Object dt) {
		String msg = (String) dt;
		return (String) msg.replace(BlockRequestSynchronized.REQUEST_HISTORY
				+ "|", "");
	}

}
