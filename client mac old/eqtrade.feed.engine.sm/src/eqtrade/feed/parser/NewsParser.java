package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import javax.swing.SwingUtilities;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.News;
import eqtrade.feed.model.NewsDef;
import eqtrade.feed.model.Subscriber;


public final class NewsParser  extends Parser {

    public NewsParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}

    
    //NW|ID|SUBJECT|TITLE
	public void process(final Vector vdata){
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (vdata.size() > 0){
		    		News news = null;
					Vector vRow;
					//log.info("Parser News : "+vdata.size());
		    		for (int i = 0; i<vdata.size(); i++){
		    			String source = (String) vdata.elementAt(i);
		    			news = new News(source);
		    			vRow = new Vector(1);
		                vRow.addElement(NewsDef.createTableRow(news));
		                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
		                ((Subscriber)hashSubscriber.get(news.getType())).setSeqno((int)news.getSeqno());
		                // System.err.println("news : "+vRow);              	
		    		}
		    		refreshListener(news); 
		        }
			}
		});        		
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_NEWS;
    }
    
    protected List createTableRow(Row dat){
    	return NewsDef.createTableRow(dat);
    }
}