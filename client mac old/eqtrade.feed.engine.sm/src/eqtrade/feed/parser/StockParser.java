package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockDef;
import eqtrade.feed.model.StockRender;
import eqtrade.feed.model.Subscriber;


public final class StockParser  extends Parser {

    public StockParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
        loadModel("3");
	}
    
    //header|type|seqno|code|name|status|stocktype|sector|ipo|base|listed|tradable|lotsize
	public void process(Vector vdata){
        if (vdata.size() > 0){
    		Stock stock;
			Vector vRow;
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			//log.info("stock : " + source);
    			stock = new Stock(source);
    			//Remark for New ORI -on remark
    			//log.info("stock : " + StockRender.getSector(stock.getCode()));
    			
    			//if (StockRender.getSector(stock.getCode()) == null){
    	               StockRender.stockSector.put(stock.getCode(), stock.getRemarks().charAt(3)+"");
    			//}
    			//end of remark
    			vRow = new Vector(1);
                vRow.addElement(createTableRow(stock));
                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                ((Subscriber)hashSubscriber.get(stock.getType())).setSeqno((int)stock.getSeqno());
                refreshListener(stock);
    		}
        }		
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_STOCK;
    }
    
    protected List createTableRow(Row dat){
    	return StockDef.createTableRow(dat);
    }
    
    protected String genFilename(String msg){
    	return "data/static/"+msg+".cache";
    }
    
    //always request from zero
    protected int loadModel(String msg){
    	super.loadModel(msg);
    	return 0;
    }
}