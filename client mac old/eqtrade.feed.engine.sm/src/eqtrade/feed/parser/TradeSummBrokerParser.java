package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Broker;
import eqtrade.feed.model.Subscriber;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;


public final class TradeSummBrokerParser  extends Parser {

    public TradeSummBrokerParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}
    
    protected String getBrokerName(String code){
        Broker s = ((Broker)engine.getDataStore().get(FeedStore.DATA_BROKER).getDataByKey(new Object[]{code}));
        return (s!=null) ? s.getName() : "";
    }    
    
	public void process(Vector vdata){
        if (vdata.size() > 0){
    		TradeSummary trade;
			Vector vRow;
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			trade = new TradeSummary(source);
    			trade.setBrokername(getBrokerName(trade.getBroker()));
    			vRow = new Vector(1);
                vRow.addElement(createTableRow(trade));
                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                ((Subscriber)hashSubscriber.get(trade.getType())).setSeqno((int)trade.getSeqno());
                refreshListener(trade);
    		}
        }		
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_TRADESUMBROKER;
    }
    
    protected List createTableRow(Row dat){
    	return TradeSummaryDef.createTableRow(dat);
    }
}