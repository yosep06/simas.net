package eqtrade.feed.parser;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import com.vollux.idata.Column;
import com.vollux.idata.Row;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Market;
import eqtrade.feed.model.MarketDef;
import eqtrade.feed.model.Sector;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockRender;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;
import eqtrade.feed.model.Subscriber;
import eqtrade.feed.model.Summary;
import eqtrade.feed.model.SummaryDef;


public final class StockSummParser  extends Parser {
    public StockSummParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
    }
    
    public void process(Vector vdata){
    	for (int i=0; i<vdata.size(); i++){
    		String data = (String)vdata.elementAt(i);
    		process(data);
    	}
    }
    
	private void process(String source) {
        if (source != null) {
            StockSummary ss;
			Vector vRow;
            ss = new StockSummary(source);
            ((Subscriber)hashSubscriber.get(ss.getType())).setSeqno((int)ss.getSeqno());
                ss.setName(getStockName(ss.getCode()));
                ss.setSector(getSectorName(getSectorbyStock(ss.getCode())));
                int ssidx = engine.getDataStore().get(getModelID()).getIndexByKey(new Object[]{ss.getCode(), ss.getBoard()});
                if (ssidx>-1){
                    StockSummary ssold = (StockSummary)engine.getDataStore().get(getModelID()).getDataByIndex(ssidx);
                    genTotalMarket(ssold, ss);
                    genBoardMarket(ssold, ss);
                    gendtopgainer(ssold, ss);
                    ssold.fromVector(ss.getData());
                } else {
                    genTotalMarket(null, ss);
                    genBoardMarket(null, ss);
                    vRow = new Vector(1);
                    vRow.addElement(StockSummaryDef.createTableRow(ss));
                    engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                }
            refreshListener(ss);
            genSummary();
            engine.getDataStore().get(getModelID()).refresh();
            engine.getDataStore().get(FeedStore.DATA_MARKET).refresh();
            engine.getDataStore().get(FeedStore.DATA_TOPGAINER).refresh();
        }
	}
	
	protected void genSummary(){
        int i = engine.getDataStore().get(getModelID()).getRowCount();
        StockSummary stock;
        double adv = 0, dec = 0, unchg = 0, untrade=0;
        for (int j=0; j<i;j++){
            stock = (StockSummary)engine.getDataStore().get(getModelID()).getDataByIndex(j);
            if (stock.getBoard().equals("RG")){
                if (stock.getChange().doubleValue() > 0) {
                    adv++;
                } else if (stock.getChange().doubleValue() < 0) {
                    dec++;
                } //else if (stock.getTradedFrequency().doubleValue()>=0 && stock.getChange().doubleValue()==0)
                else if (stock.getChange().doubleValue() == 0 && stock.getTradedFrequency().doubleValue() > 0) {
                	//log.info("SIZE : "+stock.getChange());
                //	log.info("UNCHANGE_stock : "+stock.getCode());
                	unchg++;
                } else if (stock.getTradedFrequency().doubleValue()==0) {
                    untrade++;
                }
            }
        }
        //log.info("UNCHANGE_tot : "+unchg);
        Summary summ = new Summary();
        summ.setActiontype("SUMMARY");
        summ.setInfo1(new Double(adv));
        summ.setInfo2(new Double(dec));
        summ.setInfo3(new Double(unchg));
        summ.setInfo4(new Double(untrade));
        Vector vrow = new Vector(1);
        vrow.addElement(SummaryDef.createTableRow(summ));
        engine.getDataStore().get(FeedStore.DATA_MARKETSUMM).addRow(vrow, false, false);
    }
    
    protected void genTotalMarket(StockSummary oldStock, StockSummary newStock){
        Market market;        
        int idx = engine.getDataStore().get(FeedStore.DATA_MARKET).getIndexByKey(new Object[]{"TOTAL"});
        if (idx > -1){
            market = (Market)engine.getDataStore().get(FeedStore.DATA_MARKET).getDataByIndex(idx);
            if (oldStock == null){
                market.setValue(new Double(market.getValue().doubleValue() + newStock.getTradedValue().doubleValue()));
                market.setVolume(new Double(market.getVolume().doubleValue() + newStock.getTradedVolume().doubleValue()));
                market.setFreq(new Double(market.getFreq().doubleValue() + newStock.getTradedFrequency().doubleValue()));                
            } else {
                market.setValue(new Double(market.getValue().doubleValue() - oldStock.getTradedValue().doubleValue() + newStock.getTradedValue().doubleValue()));
                market.setVolume(new Double(market.getVolume().doubleValue() - oldStock.getTradedVolume().doubleValue() + newStock.getTradedVolume().doubleValue()));
                market.setFreq(new Double(market.getFreq().doubleValue() - oldStock.getTradedFrequency().doubleValue() + newStock.getTradedFrequency().doubleValue()));                
            }
        } else {
            market = new Market();
            market.setBoard("TOTAL");
            market.setValue(new Double(newStock.getTradedValue().doubleValue()));
            market.setVolume(new Double(newStock.getTradedVolume().doubleValue()));
            market.setFreq(new Double(newStock.getTradedFrequency().doubleValue()));
           Vector vRow = new Vector(1);
            vRow.addElement(MarketDef.createTableRow(market));
            engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vRow, false, false);
        }
    }
	
    protected void genBoardMarket(StockSummary oldStock, StockSummary newStock){
        Market market;        
        int idx = engine.getDataStore().get(FeedStore.DATA_MARKET).getIndexByKey(new Object[]{newStock.getBoard()});
        if (idx > -1){
            market = (Market)engine.getDataStore().get(FeedStore.DATA_MARKET).getDataByIndex(idx);
            if (oldStock == null){
                market.setValue(new Double(market.getValue().doubleValue() + newStock.getTradedValue().doubleValue()));
                market.setVolume(new Double(market.getVolume().doubleValue() + newStock.getTradedVolume().doubleValue()));
                market.setFreq(new Double(market.getFreq().doubleValue() + newStock.getTradedFrequency().doubleValue()));                
            } else {
                market.setValue(new Double(market.getValue().doubleValue() - oldStock.getTradedValue().doubleValue() + newStock.getTradedValue().doubleValue()));
                market.setVolume(new Double(market.getVolume().doubleValue() - oldStock.getTradedVolume().doubleValue() + newStock.getTradedVolume().doubleValue()));
                market.setFreq(new Double(market.getFreq().doubleValue() - oldStock.getTradedFrequency().doubleValue() + newStock.getTradedFrequency().doubleValue()));                
            }
            engine.getDataStore().get(FeedStore.DATA_MARKET).refresh(idx);
        } else {
            market = new Market();
            market.setBoard(newStock.getBoard());
            market.setValue(new Double(newStock.getTradedValue().doubleValue()));
            market.setVolume(new Double(newStock.getTradedVolume().doubleValue()));
            market.setFreq(new Double(newStock.getTradedFrequency().doubleValue()));
           Vector vRow = new Vector(1);
            vRow.addElement(MarketDef.createTableRow(market));
            engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vRow, false, false);
        }
    }
    
    protected void gendtopgainer(StockSummary oldStock, StockSummary newStock){
    	StockSummary  tp ;
    	 int idx = engine.getDataStore().get(FeedStore.DATA_TOPGAINER).getIndexByKey(new Object[]{newStock.getCode(),newStock.getBoard()});
         if (idx > -1) {
			tp = (StockSummary) engine.getDataStore().get(FeedStore.DATA_TOPGAINER).getDataByIndex(idx);
			if (oldStock == null) {
				tp.setTradedFrequency(new Double(tp.getTradedFrequency().doubleValue()+newStock.getTradedFrequency().doubleValue()));
				tp.setTradedVolume(new Double(tp.getTradedVolume().doubleValue() + newStock.getTradedVolume().doubleValue()));
				tp.setTradedValue(new Double(tp.getTradedValue().doubleValue() + newStock.getTradedValue().doubleValue()));
				
			} else {
				tp.setTradedFrequency(new Double(tp.getTradedFrequency().doubleValue()-oldStock.getTradedFrequency().doubleValue()+newStock.getTradedFrequency().doubleValue()));
				tp.setTradedVolume(new Double(tp.getTradedVolume().doubleValue() - oldStock.getTradedVolume().doubleValue()+ newStock.getTradedVolume().doubleValue()));
				tp.setTradedValue(new Double(tp.getTradedValue().doubleValue() -oldStock.getTradedValue().doubleValue() + newStock.getTradedValue().doubleValue()));
			}
//			engine.getDataStore().get(FeedStore.DATA_TOPGAINER).refresh(idx);
		}
    }
    
    protected Integer getModelID(){
        return FeedStore.DATA_STOCKSUMMARY;
    }
    
    protected List createTableRow(Row dat){
    	return StockSummaryDef.createTableRow(dat);
    }
    
    protected void saveModel(Subscriber r){
        String filename = "data/session/"+FeedSetting.session+"-"+"5S"+".cache";
        Vector vdata = new Vector(50,10);
        if (getModelID() != null){
            Vector vtemp = engine.getDataStore().get(FeedStore.DATA_MARKET).getDataVector();
            for (int i=0; i<vtemp.size();i++){
                vdata.addElement(((Column)((Vector)vtemp.elementAt(i)).elementAt(0)).getSource());

            }
            HashMap map = new HashMap(2);
            map.put("seq", new Integer(r.getSeqno()));
            map.put("msg", "5S");
            map.put("data", vdata);
            Utils.writeFile(filename, Utils.objectToCompressedByte(map));
        }
        super.saveModel(r);
    }
    
    protected int loadModel(String msg){
        try {
            String  filename = "data/session/"+FeedSetting.session+"-"+"5S"+".cache";
            Object o = Utils.readFile(filename);
            if (o != null){
                byte[] byt = (byte[])o;            	
                HashMap map =  (HashMap)Utils.compressedByteToObject(byt);
                Vector vdata = (Vector)map.get("data");
                for (int i=0; i<vdata.size();i++){
                    Vector vrow = new Vector(1);
                    vrow.addElement(MarketDef.createTableRow((Row)vdata.elementAt(i)));
                    engine.getDataStore().get(FeedStore.DATA_MARKET).addRow(vrow, false, false);

                }
            } 
        } catch (Exception ex){
        }
        
        int seq=0;
        try {
            String  filename = genFilename(msg); 
            Object o = Utils.readFile(filename);
            if (o != null){
            	byte[] byt = (byte[])o;            	
                HashMap map =  (HashMap)Utils.compressedByteToObject(byt); // (HashMap)o;
                Vector vdata = (Vector)map.get("data");
                Vector vrow = new Vector(100,5);
                for (int i=0; i<vdata.size();i++){
                    Row r = (Row)vdata.elementAt(i);
    	            if (StockRender.getSector(((StockSummary)r).getCode()) == null){
    	            	StockRender.stockSector.put(((StockSummary)r).getCode(), ((StockSummary)r).getRemarks().charAt(3)+"");
    	            }
                    vrow.addElement(createTableRow(r));
                }
                if (getModelID() != null && vrow.size()>0) {
                	engine.getDataStore().get(getModelID()).addRow(vrow, false, false);
                	engine.getDataStore().get(getModelID()).refresh();
                }
                seq = ((Integer)map.get("seq")).intValue();
            } else {
                seq= 0;            
            }
        } catch (Exception ex){
            seq=0;
        }
        genSummary();
        return seq;
    }

    protected String getStockName(String code){
        Stock s = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new String[]{code}));
        return (s!=null) ? s.getName() : "";
    }

    protected String getSectorbyStock(String code) {
    	Stock stocksector = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new String[]{code}));
    	return (stocksector != null) ? stocksector.getSector() : "";
    }
    
    protected String getSectorName(String code) {
    	Sector sector = ((Sector)engine.getDataStore().get(FeedStore.DATA_SECTORNAME).getDataByKey(new String[]{code}));
    	return (sector != null) ? sector.getCIDX_SECTORNAME() : "";
    }
}