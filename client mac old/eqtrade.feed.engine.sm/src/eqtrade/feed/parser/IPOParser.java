package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import javax.swing.SwingUtilities;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.IPO;
import eqtrade.feed.model.IPODef;
import eqtrade.feed.model.Subscriber;


public final class IPOParser  extends Parser {

    public IPOParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}

    
    //actiontype|stock|amount|ratio1|ratio2|cumdate|extdate|recordingdate|distdate
	public void process(final Vector vdata){
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				 if (vdata.size() > 0){
			    		IPO ipo;
						Vector vRow;
			    		for (int i = 0; i<vdata.size(); i++){
			    			String source = (String) vdata.elementAt(i);
			    			ipo = new IPO(source);
			    			vRow = new Vector(1);
			                vRow.addElement(IPODef.createTableRow(ipo));
			                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
			                ((Subscriber)hashSubscriber.get(ipo.getType())).setSeqno((int)ipo.getSeqno());
			                refreshListener(ipo);
			    		}
			        }
				}
		});       	
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_IPO;
    }
    
    protected List createTableRow(Row dat){
    	return IPODef.createTableRow(dat);
    }
}