package eqtrade.feed.parser; 

import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vollux.idata.Column;
import com.vollux.idata.Row;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedEventDispatcher;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.RunningTrade;
import eqtrade.feed.model.RunningTradeDef;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.Subscriber;

public final class TradeHistoryMParser  extends Parser {
	
	private ThreadMHistory thread = null;
	int ProsOrLoad = 0;
	public TradeHistoryMParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
        
        if (thread != null) {
			thread.stop();
			thread = null;
		}
        thread  = new ThreadMHistory(this);
        thread.start();
	}
    
    public Subscriber genRequest(String msg){
    	ProsOrLoad =1;
        int seq = loadModel(msg);
        //System.out.println("Subscriber : "+msg);
        return new Subscriber(msg, seq);         
    }
    
    public void process(Vector vdata){
    	if (vdata.size()>0){
    		ProsOrLoad = 0;
//			RunningTrade trade = new RunningTrade((String)vdata.elementAt(0));
    		thread.addMsg(vdata);
//			if(((Subscriber)hashSubscriber.get(trade.getType()+"|"+trade.getStock())) == null) {
//				thread.notifVmsg();
//			}
//    		System.out.println("proces");
//    		for (int i = 0; i<vdata.size(); i++){
//    			String data = (String)vdata.elementAt(i);
//    			process(data);
////    			System.out.println("DATA : "+data);
//    		}
    	}
    }
	int j=0;

	void process(String vdata) {
        if (vdata != null) {
			RunningTrade trade = new RunningTrade(vdata);
			if (trade.getBoard().equals("RG")||(trade.getBoard().equals("TN"))||(trade.getBoard().equals("NG"))) {
				//yosep 04122014 chg nego timeTrade
				if (trade.getBoard().equalsIgnoreCase("NG")) {
					StockSummary st = (StockSummary)engine.getDataStore().get(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new String[]{trade.getStock(),"RG"});
				trade.setPrevious(st.getPrevious());
				}
				
				trade.calculate();				
				if(((Subscriber)hashSubscriber.get(trade.getType()+"|"+trade.getStock())) == null) {
					hashSubscriber.put(trade.getType() + "|"+ trade.getStock(), new Subscriber(trade.getType() + "|"+ trade.getStock(),0));
				}
				((Subscriber)hashSubscriber.get(trade.getType()+"|"+trade.getStock())).setSeqno((int)trade.getSeqno());
				Vector vRow = new Vector(1);
	            vRow.addElement(createTableRow(trade));
	            engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
//            	System.out.println(engine.getDataStore().get(getModelID()).getDataVector().size()+ " addModel " );
//	            System.out.println("VROW process : "+trade.getStock()+ " -- "+trade.getLast());
	            refreshListener(trade);
        	}
		}
	}
	
	protected int loadModel(String msg) {j=0;
		/*try {
			String filename = genFilename(msg);
			Object o = Utils.readFile(filename);
			if (o != null) {
				byte[] byt = (byte[]) o;
				HashMap map = (HashMap) Utils.compressedByteToObject(byt); // (HashMap)o;
				Vector vdata = (Vector) map.get("data");
				//Vector vrow = new Vector(100, 5);.
				System.out.println("loadmodel ");
//				for (int i = 0; i < vdata.size(); i++) {
				thread.addMsg(vdata);
//					Vector vrow = new Vector();
//					vrow.addElement(createTableRow((Row) vdata.elementAt(i)));
//					engine.getDataStore().get(getModelID())
//					.addRow(vrow, false, false);
//				}
				engine.getDataStore().get(getModelID()).refresh();

				int seq = ((Integer) map.get("seq")).intValue();
				return seq;
			} else {
				return 0;
			}
		} catch (Exception ex) {
			// log.error(Utils.logException(ex));
			return 0;
		}*/
		return 0;
	}
	
    protected String genFilename(String msg){
    	String[] s1 = msg.split("\\|");
    	//System.out.println("genFileName : "+s1);
    	return "data/session/"+FeedSetting.session+"-"+s1[0]+s1[1]+".cache";
    }

    protected Integer getModelID(){
        return FeedStore.DATA_TRADEHISTORYM;
    }
    
    protected List createTableRow(Row dat){
    	return RunningTradeDef.createTableRow(dat);
    }
    
    protected boolean removeAfterUnsubscribe(){
    	return true;
    }
    
    protected Vector getDataVector(Subscriber r){
    	Vector vtemp = new Vector(10);
    	RunningTrade price;
        Vector vAll = engine.getDataStore().get(getModelID()).getDataVector();
        for (int i = 0; i < vAll.size();i++){
            price = (RunningTrade)((Column)((Vector)vAll.elementAt(i)).elementAt(0)).getSource();
            if (r.getKey().endsWith(price.getStock())){
            	vtemp.addElement(vAll.elementAt(i));
            }            
        }
        //System.out.println("VTEMP : "+vtemp.toString());
        return vtemp;
    }
    
    public void removeModel(Subscriber r){
    	try {System.out.println("removemodel");
	        int counter=0, rowCount= engine.getDataStore().get(getModelID()).getDataVector().size();
	        for (int i=0; i<rowCount;i++){
	            RunningTrade price = (RunningTrade)(((Column)((Vector)engine.getDataStore().get(getModelID()).getDataVector().elementAt(counter)).elementAt(0)).getSource());
	            if (r.getKey().endsWith(price.getStock())){
	            	engine.getDataStore().get(getModelID()).getDataVector().remove(counter);
//	            	System.out.println(engine.getDataStore().get(getModelID()).getDataVector().size()+ " removemodel " );
	            } else counter++;
	        }
	        engine.getDataStore().get(getModelID()).refresh();
//	        System.out.println("removemodel2"); setseqno 0
	        hashSubscriber.remove(r.getKey());
    	} catch (Exception ex){ 
    		return;
    	}
    }
}

class ThreadMHistory extends Thread{
	
	private Vector vmsg = new Vector();
	private TradeHistoryMParser thp;
	private Log log = LogFactory.getLog(getClass());
	public int count = 0;
	
	public ThreadMHistory(TradeHistoryMParser thp){
		this.thp = thp;
		vmsg.clear();
	}
	
	public void addMsg(Vector msg){
		synchronized (vmsg) {
			vmsg.addAll(msg);
			vmsg.notify();
		}
	}
	
	public void addModelMsg(Vector msg ) throws InterruptedException{
		synchronized (vmsg) {
			vmsg.addAll(msg);
			vmsg.wait(1000);
		}
	}
	
	@Override
	public void run() {
		while (true) {
			if (vmsg.size() == 0) {
				synchronized (vmsg) {
					try {
						HashMap param = new HashMap();
						param.put(FeedEventDispatcher.PARAM_HISTORY, true);
						thp.engine.getEventDispatcher().pushEvent(FeedEventDispatcher.EVENT_SETTIMETRADE, param);
				
						vmsg.wait();
					} catch (InterruptedException e) {
						// TODO: handle exception
					}
				}
			} else {
				while (vmsg.size() > 0) {
					if (vmsg.elementAt(0) instanceof String) {
						try {
							
						String data = (String)vmsg.remove(0);
						thp.process(data);
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else {
//						try {
						Vector vrow = new Vector();
						vrow.addElement(thp.createTableRow((Row) vmsg.remove(0)));
						thp.engine.getDataStore().get(thp.getModelID())
						.addRow(vrow, false, false);
//			            System.out.println("VROW process : ");
						/*	
						} catch (ConcurrentModificationException e) {
							// TODO: handle exception
							System.out.println("ee ");
						} catch (IllegalArgumentException e){
							System.out.println("bb");
						} catch (Exception e) {
							e.printStackTrace();
						}*/
					}
					// yosep fixingTimetrade
	    			/*if (vmsg.size()<10) {
	    				HashMap param = new HashMap();
	    				param.put(FeedEventDispatcher.PARAM_HISTORY, true);
						thp.engine.getEventDispatcher().pushEvent(FeedEventDispatcher.EVENT_SETTIMETRADE, param);
					}else{
	    				HashMap param = new HashMap();
	    				param.put(FeedEventDispatcher.PARAM_HISTORY, false);
						thp.engine.getEventDispatcher().pushEvent(FeedEventDispatcher.EVENT_SETTIMETRADE, param);
					}*/
				}
			}
		}
	}
	
	public void notifVmsg(){
		synchronized (vmsg) {
			vmsg.notify();
		}
	}
	public void waitVmsg() throws InterruptedException{
		synchronized (vmsg) {
			vmsg.wait();
		}
	}
	public Vector getVmsg(){
		return vmsg;
	}
	
}