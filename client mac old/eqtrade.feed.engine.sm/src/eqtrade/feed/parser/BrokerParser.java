package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Broker;
import eqtrade.feed.model.BrokerDef;
import eqtrade.feed.model.Subscriber;


public final class BrokerParser  extends Parser {

    public BrokerParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
        loadModel("4");
	}
    
    //BD|AD|KAPITA SEKURINDO|0
	public void process(Vector vdata){
        if (vdata.size() > 0){
    		Broker broker;
			Vector vRow;
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			broker = new Broker(source);
    			vRow = new Vector(1);
                vRow.addElement(createTableRow(broker));
                engine.getDataStore().get(FeedStore.DATA_BROKER).addRow(vRow, false, false);
                ((Subscriber)hashSubscriber.get(broker.getType())).setSeqno((int)broker.getSeqno());
                refreshListener(broker);
    		}
        }
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_BROKER;
    }
    
    protected List createTableRow(Row dat){
    	return BrokerDef.createTableRow(dat);
    }
    
    protected String genFilename(String msg){
    	return "data/static/"+msg+".cache";
    }
    
//  always request from zero
    protected int loadModel(String msg){
    	super.loadModel(msg);
    	return 0;
    }

}