package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Currency;
import eqtrade.feed.model.CurrencyDef;
import eqtrade.feed.model.Subscriber;


public final class CurrencyParser  extends Parser {

    public CurrencyParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}
    
    //code|value|change
	public void process(Vector vdata){
        if (vdata.size() > 0){
    		Currency			currency;
			Vector vRow;
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			currency = new Currency(source);
    			vRow = new Vector(1);
                vRow.addElement(createTableRow(currency));
                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                ((Subscriber)hashSubscriber.get(currency.getType())).setSeqno((int)currency.getSeqno());
                refreshListener(currency);
    		}
        }		
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_CURRENCY;
    }
    
    protected List createTableRow(Row dat){
    	return CurrencyDef.createTableRow(dat);
    }
}