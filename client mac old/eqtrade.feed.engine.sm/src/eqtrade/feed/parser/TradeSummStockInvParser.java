package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Broker;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.Subscriber;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;


public final class TradeSummStockInvParser  extends Parser {

	private ThreadTrade thread = null;
	
    public TradeSummStockInvParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
        
        if (thread != null) {
			thread.stop();
			thread = null;
		}
        
        thread = new ThreadTrade(this);
        thread.start();
	}
    
    protected String getStockName(String code){
        Stock s = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new Object[]{code}));
        return (s!=null) ? s.getName() : "";
    }
    protected String getBrokerName(String code){
        Broker s = ((Broker)engine.getDataStore().get(FeedStore.DATA_BROKER).getDataByKey(new Object[]{code}));
        return (s!=null) ? s.getName() : "";
    }
    double f=0, d =0;

	public void process(Vector vdata){
       /* if (vdata.size() > 0){
    		TradeSummary trade,tradeinv;
			Vector vRow;
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			trade = new TradeSummary(source);
    			int idx = engine.getDataStore().get(FeedStore.DATA_TRADESUMMARKETINV).getIndexByKey(new Object[]{trade.getStock(),"D"});
    			if (idx > -1) {
					tradeinv = (TradeSummary) engine.getDataStore().get(FeedStore.DATA_TRADESUMMARKETINV).getDataByIndex(idx);
					tradeinv.setBuyvol(new Double(tradeinv.getBuyvol().doubleValue() + trade.getBuyvol().doubleValue()));
	                tradeinv.setBuyval(new Double(tradeinv.getBuyval().doubleValue() + trade.getBuyval().doubleValue()));
	                tradeinv.setBuyfreq(new Double(tradeinv.getBuyfreq().doubleValue() + trade.getBuyfreq().doubleValue()));                
	                tradeinv.setSellvol(new Double(tradeinv.getSellvol().doubleValue() + trade.getSellvol().doubleValue()));
	                tradeinv.setSellval(new Double(tradeinv.getSellval().doubleValue() + trade.getSellval().doubleValue()));
	                tradeinv.setSellfreq(new Double(tradeinv.getSellfreq().doubleValue() + trade.getSellfreq().doubleValue()));    
	                tradeinv.calculate();
				} else {
					trade.setStockname(getStockName(trade.getStock()));
	    			vRow = new Vector(1);
	                vRow.addElement(createTableRow(trade));
	                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
	                ((Subscriber)hashSubscriber.get(trade.getType())).setSeqno((int)trade.getSeqno());
	                refreshListener(trade);
				}
                engine.getDataStore().get(getModelID()).refresh();
    			
    		}
        }		*/
		if (vdata.size() > 0) {
			thread.addMsg(vdata);		
		}
		
	}
	
	public void process(String vdata){
		TradeSummary trade,tradeinv;
		Vector vRow;
		
		trade = new TradeSummary(vdata);
			trade.calculate();
		if (trade.getBoard().equalsIgnoreCase("RG")) {
			int idx = engine.getDataStore().get(FeedStore.DATA_TRADESUMMARY)
					.getIndexByKey(new Object[]{trade.getBroker(),trade.getStock(), trade.getBoard(), trade.getInvestor()});
			if (idx >-1) {
				TradeSummary old = (TradeSummary)engine.getDataStore().get(FeedStore.DATA_TRADESUMMARY).getDataByIndex(idx);
				genTradeSummMarketInv(old,trade);
				old.fromVector(trade.getData());
				engine.getDataStore().get(FeedStore.DATA_TRADESUMMARY).refresh(idx);
			} else {
				genTradeSummMarketInv(null,trade);
				 vRow = new Vector(1);
                 vRow.addElement(TradeSummaryDef.createTableRow(trade));
                 engine.getDataStore().get(FeedStore.DATA_TRADESUMMARY).addRow(vRow, false, false);
			}
              refreshListener(trade);
                ((Subscriber)hashSubscriber.get(trade.getType())).setSeqno((int)trade.getSeqno());
 
		}
	
		
	}
	
	void genTradeSummMarketInv(TradeSummary old,TradeSummary newtrade){
		TradeSummary tradeinv;
		
		int idx = engine.getDataStore().get(FeedStore.DATA_TRADESUMMARKETINV).getIndexByKey(new Object[]{newtrade.getStock(),newtrade.getInvestor()});
		if (idx > -1) {
			tradeinv = (TradeSummary) engine.getDataStore().get(FeedStore.DATA_TRADESUMMARKETINV).getDataByIndex(idx);
			if (old == null) {
				tradeinv.setBuyvol(new Double(tradeinv.getBuyvol().doubleValue() + newtrade.getBuyvol().doubleValue()));
	            tradeinv.setBuyval(new Double(tradeinv.getBuyval().doubleValue() + newtrade.getBuyval().doubleValue()));
	            tradeinv.setBuyfreq(new Double(tradeinv.getBuyfreq().doubleValue() + newtrade.getBuyfreq().doubleValue()));                
	            tradeinv.setSellvol(new Double(tradeinv.getSellvol().doubleValue() + newtrade.getSellvol().doubleValue()));
	            tradeinv.setSellval(new Double(tradeinv.getSellval().doubleValue() + newtrade.getSellval().doubleValue()));
	            tradeinv.setSellfreq(new Double(tradeinv.getSellfreq().doubleValue() + newtrade.getSellfreq().doubleValue()));    
	            tradeinv.calculate();
			} else {
				tradeinv.setBuyvol(new Double(tradeinv.getBuyvol().doubleValue() - old.getBuyvol().doubleValue() +newtrade.getBuyvol().doubleValue()));
	            tradeinv.setBuyval(new Double(tradeinv.getBuyval().doubleValue() - old.getBuyval().doubleValue() + newtrade.getBuyval().doubleValue()));
	            tradeinv.setBuyfreq(new Double(tradeinv.getBuyfreq().doubleValue() - old.getBuyfreq().doubleValue() + newtrade.getBuyfreq().doubleValue()));                
	            tradeinv.setSellvol(new Double(tradeinv.getSellvol().doubleValue() - old.getSellvol().doubleValue() + newtrade.getSellvol().doubleValue()));
	            tradeinv.setSellval(new Double(tradeinv.getSellval().doubleValue() - old.getSellval().doubleValue() + newtrade.getSellval().doubleValue()));
	            tradeinv.setSellfreq(new Double(tradeinv.getSellfreq().doubleValue() - old.getSellfreq().doubleValue() + newtrade.getSellfreq().doubleValue()));    
	            tradeinv.calculate();
			}			
			 try {
				 tradeinv.setBuyavg(new Double(tradeinv.getBuyval().doubleValue() / tradeinv.getBuyvol().doubleValue()));
	            } catch (Exception ex){
	            	tradeinv.setBuyavg(new Double(0));
	            }
	            try {
	            	tradeinv.setSellavg(new Double(tradeinv.getSellval().doubleValue() / tradeinv.getSellvol().doubleValue()));
	            } catch (Exception ex){
	            	tradeinv.setSellavg(new Double(0));
	            }
			engine.getDataStore().get(getModelID()).refresh(idx);
		} else {
			tradeinv = new TradeSummary();
			tradeinv.setStockname(getStockName(newtrade.getStock()));
			tradeinv.fromVector(newtrade.getData());
            tradeinv.calculate();
            tradeinv.setBrokername(getBrokerName(tradeinv.getBroker()));
            tradeinv.setStockname(getStockName(tradeinv.getStock()));
            tradeinv.setBroker("ALL");
			Vector vRow = new Vector(1);
            vRow.addElement(createTableRow(tradeinv));
            engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
            refreshListener(tradeinv);
		}
	

	}
    
    protected Integer getModelID(){
//        return FeedStore.DATA_TRADESUMSTOCKINV;
    	return FeedStore.DATA_TRADESUMMARKETINV;
    }
    
    protected List createTableRow(Row dat){
    	return TradeSummaryDef.createTableRow(dat);
    }
    
    class ThreadTrade extends Thread{

    	private Vector vmsg = new Vector();
    	private TradeSummStockInvParser tsp;
    	private Log log = LogFactory.getLog(getClass());
    	public int count = 0;
    	
    	public ThreadTrade (TradeSummStockInvParser tsp){
    		this.tsp = tsp;
    		vmsg.clear();
    	}
    	
    	public void addMsg(Vector msg){
    		synchronized (vmsg) {
    			vmsg.addAll(msg);
    			vmsg.notify();
    		}
    	}
    	
    	@Override
    	public void run() {
    		// TODO Auto-generated method stub
    		while (true) {
    			if (vmsg.size() == 0) {
    				synchronized (vmsg) {
							try {
							vmsg.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} else {
					while (vmsg.size() > 0 ) {
						if (vmsg.elementAt(0) instanceof String) {
							try {
								String data = (String) vmsg.remove(0);
								tsp.process(data);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						else {
							Vector vrow = new Vector();
							vrow.addElement(tsp.createTableRow((Row) vmsg.remove(0)));
							tsp.engine.getDataStore().get(tsp.getModelID())
							.addRow(vrow, false, false);
						}
					}
				}
    			
			}
    	}
    }
    
    
}