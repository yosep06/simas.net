package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import javax.swing.SwingUtilities;

import com.vollux.idata.Column;
import com.vollux.idata.Row;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Quote;
import eqtrade.feed.model.QuoteDef;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.Subscriber;

public final class QuoteParser extends Parser {
	public QuoteParser(IFeedEngine engine) {
		super(engine);
		setName(this.getClass().getName());
		setPriority(NORM_PRIORITY + 2);
	}

	protected String genFilename(String msg) {
		String[] s1 = msg.split("\\|");
		String[] s2 = s1[1].split("\\#");
		return "data/session/" + FeedSetting.session + "-" + s1[0] + s2[0]
				+ s2[1] + ".cache";
	}

	public void process(Vector vdata) {
		if (vdata.size() > 0) {
			for (int i = 0; i < vdata.size(); i++) {
				String data = (String) vdata.elementAt(i);
				// System.out.println("Test data Best Qoute: "+data);
				process(data);
			}
		}
	}

	// header|type|seqno|time|stock|board|prev<B[]O[]L[]
	private void process(String vdata) {
		if (vdata != null) {
			Vector vRow, vparse, vheader;
			String stock, board, sdata, spprice, sbid, soffer, slast;
			Vector vbid, voff;
			int nstart, nend;
			Quote oLast, oq;
			vparse = Utils.parser(vdata, "<");
			vheader = Utils.parser(vparse.elementAt(0).toString(), "|");
			stock = vheader.elementAt(4).toString();
			board = vheader.elementAt(5).toString();
			spprice = vheader.elementAt(6).toString();
			sdata = vparse.elementAt(1).toString();
			/*
			 * try { StockSummary stock2 =
			 * (StockSummary)engine.getDataStore().get
			 * (FeedStore.DATA_STOCKSUMMARY).getDataByKey(new Object[]{stock,
			 * "RG"});
			 * 
			 * System.out.println(stock2.getAvg()+" pppp "); } catch (Exception
			 * e) { e.printStackTrace(); }
			 */
			nstart = sdata.indexOf("[") + 1;
			nend = sdata.indexOf("]", nstart);
			sbid = sdata.substring(nstart, nend);
			vbid = getRow(stock, sbid, spprice, "B", board);

			nstart = sdata.indexOf("[", nend) + 1;
			nend = sdata.indexOf("]", nstart);
			soffer = sdata.substring(nstart, nend);
			voff = getRow(stock, soffer, spprice, "O", board);

			nstart = sdata.indexOf("[", nend) + 1;
			nend = sdata.indexOf("]", nstart);
			slast = sdata.substring(nstart, nend);
			oLast = new Quote();
			if (!slast.equals("")) {
				String[] str = slast.split("\\|");
				oLast.setPrice(new Double(Double.parseDouble(str[0])));
				oLast.setLot(new Double(Double.parseDouble(str[1])));
			}
			oLast.setActiontype(stock);
			oLast.setBoard(board);
			oLast.setType("L");
			oLast.setPrev(new Double(Utils.strToDouble(spprice, 0)));
			oLast.setNumber(new Double(11));
			for (int j = 0; j < voff.size(); j++) {
				oq = (Quote) voff.elementAt(j);
				oq.setNumber(new Double(10 - j));
				// vRow = new Vector(1);
				// vRow.addElement(QuoteDef.createTableRow(oq));
				final Vector vr = new Vector(1);
				vr.addElement(QuoteDef.createTableRow(oq));
				// engine.getDataStore().get(getModelID()).addRow(vRow, false,
				// false);
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						// process(vdata);
						engine.getDataStore().get(getModelID())
								.addRow(vr, false, false);

					}
				});
			}

			// vRow = new Vector(1);
			// vRow.addElement(QuoteDef.createTableRow(oLast));
			// engine.getDataStore().get(getModelID()).addRow(vRow, false,
			// false);
			final Vector vr = new Vector(1);
			vr.addElement(QuoteDef.createTableRow(oLast));

			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					engine.getDataStore().get(getModelID())
							.addRow(vr, false, false);

				}
			});

			for (int j = 0; j < vbid.size(); j++) {
				oq = (Quote) vbid.elementAt(j);
				oq.setNumber(new Double(j + 12));
				//vRow = new Vector(1);
				//vRow.addElement(QuoteDef.createTableRow(oq));
				final Vector vr2 = new Vector(1);
				vr2.addElement(QuoteDef.createTableRow(oq));
				

				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						// engine.getDataStore().get(getModelID()).addRow(vr,
						// false, false);
						engine.getDataStore().get(getModelID())
								.addRow(vr2, false, false);

					}
				});

			}
		}
	}

	// get top 5 , if data available less then 5 add empty row
	private Vector getRow(String stock, String msg, String pprice, String type,
			String board) {
		Vector vresult = new Vector(11);
		Quote doq;
		String stemp;
		String[] sdata;
		double total = 0;

		Vector vparse = Utils.parser(msg, ">");
		for (int i = 0; i < vparse.size(); i++) {
			stemp = (String) vparse.elementAt(i);
			if (stemp.trim().length() > 0) {
				doq = new Quote();
				sdata = stemp.split("\\|");
				doq.setPrice(new Double(Double.parseDouble(sdata[0])));
				doq.setLot(new Double(Double.parseDouble(sdata[1])));
				doq.setFreq(new Double(Double.parseDouble(sdata[2])));
				doq.setActiontype(stock);
				doq.setBoard(board);
				doq.setPrev(new Double(Utils.strToDouble(pprice, 0)));
				doq.setSide(type);
				total = total + doq.getLot().doubleValue();
				vresult.addElement(doq);
			}
			if (vresult.size() == 10)
				break;
		}
		// less then 5 then add empty row
		int diff = 10 - vresult.size();
		for (int i = 0; i < diff; i++) {
			doq = new Quote();
			doq.setActiontype(stock);
			doq.setBoard(board);
			doq.setPrev(new Double(Utils.strToDouble(pprice, 0)));
			doq.setSide(type);
			vresult.addElement(doq);
		}
		Quote d = new Quote();
		d.setActiontype(stock);
		d.setBoard(board);
		d.setPrev(new Double(Utils.strToDouble(pprice, 0)));
		d.setPrice(new Double(0));
		d.setLot(new Double(total));
		d.setSide(type);
		vresult.addElement(d);
		return vresult;
	}

	protected Integer getModelID() {
		return FeedStore.DATA_QUOTE;
	}

	protected List createTableRow(Row dat) {
		return QuoteDef.createTableRow(dat);
	}

	protected boolean removeAfterUnsubscribe() {
		return true;
	}

	protected Vector getDataVector(Subscriber r) {
		Vector vtemp = new Vector(10);
		Quote order;
		Vector vAll = engine.getDataStore().get(getModelID()).getDataVector();
		for (int i = 0; i < vAll.size(); i++) {
			order = (Quote) ((Column) ((Vector) vAll.elementAt(i)).elementAt(0))
					.getSource();
			if (r.getKey().endsWith(order.getCode() + "#" + order.getBoard())) {
				vtemp.addElement(vAll.elementAt(i));
			}
		}
		return vtemp;
	}

	public void removeModel(Subscriber r) {
		int rowCount = engine.getDataStore().get(getModelID()).getRowCount();
		Quote order;
		int counter = 0;
		for (int i = 0; i < rowCount; i++) {
			order = (Quote) engine.getDataStore().get(getModelID())
					.getDataByIndex(counter);
			if (r.getKey().endsWith(order.getCode() + "#" + order.getBoard())) {
				engine.getDataStore().get(getModelID()).deleteRow(counter);
			} else
				counter++;
		}
	}

	@Override
	public void doProcess(Vector vdata) {
		process(vdata);
	}

}