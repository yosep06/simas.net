package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Indices;
import eqtrade.feed.model.IndicesDef;
import eqtrade.feed.model.Subscriber;


public final class IndicesParser  extends Parser {

    public IndicesParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}
    
    //code|basevalue|marketvalue|index|open|high|low|previous
	public void process(Vector vdata){
        if (vdata.size() > 0){
    		Indices			indices;
			Vector vRow;
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			indices = new Indices(source);
    			vRow = new Vector(1);
                vRow.addElement(createTableRow(indices));
                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                ((Subscriber)hashSubscriber.get(indices.getType())).setSeqno((int)indices.getSeqno());
                refreshListener(indices);
    		}
        }		
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_INDICES;
    }
    
    protected List createTableRow(Row dat){
    	return IndicesDef.createTableRow(dat);
    }
}