package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import javax.swing.SwingUtilities;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Rups;
import eqtrade.feed.model.RupsDef;
import eqtrade.feed.model.Subscriber;


public final class RupsParser  extends Parser {

    public RupsParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}
    
    //code|value|change
	public void process(final Vector vdata){
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (vdata.size() > 0){
		    		Rups			rups;
					Vector vRow;
		    		for (int i = 0; i<vdata.size(); i++){
		    			String source = (String) vdata.elementAt(i);
		    			rups = new Rups(source);
		    			vRow = new Vector(1);
		                vRow.addElement(createTableRow(rups));
		                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
		                ((Subscriber)hashSubscriber.get(rups.getType())).setSeqno((int)rups.getSeqno());
		                refreshListener(rups);
		    		}
		        }
			}
		});        		
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_RUPS;
    }
    
    protected List createTableRow(Row dat){
    	return RupsDef.createTableRow(dat);
    }
}