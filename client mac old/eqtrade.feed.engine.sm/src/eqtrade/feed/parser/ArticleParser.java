package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Article;
import eqtrade.feed.model.ArticleDef;
import eqtrade.feed.model.Subscriber;

public final class ArticleParser extends Parser {
	
	private ThreadUpdate thread = null;
	public ArticleParser(IFeedEngine engine) {
		super(engine);
		setName(this.getClass().getName());
		
		if(thread != null) {
			thread.stop();
			thread = null;
		}
		
		thread = new ThreadUpdate(this);
		thread.start();		
	}

	public void process(final Vector vdata) {			
		thread.addMsg(vdata);
	}	

	private synchronized  void doproses() {
		
	}
	
	protected Integer getModelID() {
		return FeedStore.DATA_GLOBALNEWS;
	}

	protected List createTableRow(Row dat) {
		return ArticleDef.createTableRow(dat);
	}
}

class ThreadUpdate extends Thread {

	private Vector<String> vmsg = new Vector<String>();
	private ArticleParser ap;
	private Log log = LogFactory.getLog(getClass());
	public int counter = 0;
	
	public ThreadUpdate(ArticleParser ap){
		this.ap = ap;
		vmsg.clear();
	}

//	public void addMsg(String msg) {
//		synchronized (this) {
//			vmsg.add(msg);
//			vmsg.notify();
//		}
//	}
	
	public void addMsg(Vector msg) {			
		synchronized (vmsg) {			
			vmsg.addAll(msg);
			vmsg.notify();
		}		
	}

	@Override
	public void run() {
		while (true) {
			if (vmsg.size() == 0) {
				synchronized (vmsg) {
					try {
						vmsg.wait();
					} catch (InterruptedException e) {
						//e.printStackTrace();
					}
				}
			} else {		
				while (vmsg.size() > 0) {
					
					//for(int i = 0; i < vmsg.size(); i++) {
					String source = (String) vmsg.remove(0);
					Article art = new Article(source);
					Vector vRow = new Vector();
					vRow.addElement(ap.createTableRow(art));
					ap.engine.getDataStore().get(ap.getModelID()).addRow(vRow, false, false);
					//((Subscriber) ap.hashSubscriber.get(art.getType())).setSeqno((int) art.getSeqno());
					//ap.refreshListener(art);
				}
			
			}
		}
	}
	
}