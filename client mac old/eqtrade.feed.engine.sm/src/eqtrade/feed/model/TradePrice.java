package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public final class TradePrice extends Model {
	private static final long serialVersionUID = -9155545002187918243L;
    public static final int	CIDX_CODE = 3;
    public static final int CIDX_BOARD = 4;
    public static final int CIDX_PRICE = 5;
    public static final int CIDX_LOT = 6;
    public static final int CIDX_FREQ = 7;
    public static final int CIDN_NUMBEROFFIELDS = 8;

	public TradePrice(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public TradePrice(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 5: case 6: case 7:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}
    
    public String getBoard(){
        return (String) getData(CIDX_BOARD);
    }
    public void setBoard(String sinput){
        setData(sinput, CIDX_BOARD);
    }
    
	public String getStock(){
		return (String) getData(CIDX_CODE);
	}
	
	public void setStock(String stock){
		setData(stock, CIDX_CODE);
	}
	
	public void setActiontype(String sinput){
		setData(sinput, CIDX_CODE);
	}
	
	public Double getPrice(){
		return (Double) getData(CIDX_PRICE);
	}
	public void setPrice(Double sinput){
		setData(sinput, CIDX_PRICE);
	}

	public Double getLot(){
		return (Double) getData(CIDX_LOT);
	}
	public void setLot(Double sinput){
		setData(sinput, CIDX_LOT);
	}
	
	public Double getFreq(){
		return (Double)getData(CIDX_FREQ);
	}
	
	public void setFreq(Double sinput){
		setData(sinput, CIDX_FREQ);
	}
}