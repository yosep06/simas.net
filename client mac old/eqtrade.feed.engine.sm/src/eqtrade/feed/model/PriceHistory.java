package eqtrade.feed.model;

import java.io.DataOutput;

import com.sun.xml.internal.ws.Closeable;

import eqtrade.feed.core.Utils;

public final class PriceHistory extends Model {
	private static final long serialVersionUID = 1L;
	
	public static final int	CIDX_DATE = 0;
    public static final int CIDX_OPEN = 1;
    public static final int	CIDX_HIGH = 2;
    public static final int	CIDX_LOW = 3;
    public static final int CIDX_CLOSE = 4;
    public static final int CIDX_PREV = 5;
    public static final int	CIDX_VOLUME = 6;
    public static final int	CIDX_FREQ = 7;
    public static final int CIDN_NUMBEROFFIELDS =8;

    public Double getOpen() {
		return (Double) getData(CIDX_OPEN);
	}
	public void setOpen(Double sinput) {
		setData(sinput, CIDX_OPEN);
	}
	public Double getHigh() {
		return (Double) getData(CIDX_HIGH);
	}
	public void setHigh(Double sinput) {
		setData(sinput, CIDX_HIGH);
	}
	public Double  getLow() {
		return (Double) getData(CIDX_LOW);
	}
	public void setLow(Double sinput) {
		setData(sinput, CIDX_LOW);
	}
	public Double getClose() {
		return (Double) getData(CIDX_CLOSE);
	}
	public void setClose(Double sinput) {
		setData(sinput, CIDX_CLOSE);
	}
	public Double getPrev() {
		return (Double) getData(CIDX_PREV);
	}
	public void setPrev(Double sinput) {
		setData(sinput, CIDX_PREV);
	}
	public Double getVolume() {
		return (Double) getData(CIDX_VOLUME);
	}
	public void setVolume(Double sinput) {
		setData(sinput, CIDX_VOLUME);
	}
	public Double getFreq() {
		return (Double) getData(CIDX_FREQ);
	}
	public void setFreq(Double sinput) {
		setData(sinput, CIDX_FREQ);
	}
    public PriceHistory() {
		super(CIDN_NUMBEROFFIELDS);		
	}
	public PriceHistory(String smsg) {
		super(CIDN_NUMBEROFFIELDS, smsg);		
	}
	
	
	@Override	
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 1: case 2: case 3:case 4: case 5: case 6: case 7:  case 8:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;			
			default:
				break;
			}
		}
	}
	
	public String getDate() {
		return (String) getData(CIDX_DATE);		
	}
	public void setDate(String sinput){
		setData(sinput, CIDX_DATE);
	}
}
