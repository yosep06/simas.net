package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public final class Quote extends Model {
	private static final long serialVersionUID = -9155545002187918243L;
    public static final int		CIDX_STOCK = 0;
    public static final int		CIDX_BOARD = 1;
    public static final int		CIDX_SIDE = 2;
    public static final int		CIDX_NUMBER = 3;
    public static final int        CIDX_PRICE = 4;
    public static final int        CIDX_LOT = 5;
    public static final int		CIDX_FREQ = 6;
    public static final int		CIDX_PREV = 7;
    public static final int 		CIDN_NUMBEROFFIELDS = 8;
    
    public String toString(){
    	return getCode()+"|"+getBoard()+"|"+getSide()+"|"+getNumber()+"|"+getPrice()+"|"+getLot()+"|"+getPrev();
    }

	public Quote(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Quote(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 3: case 4: case 5: case 6: case 7:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}
    
	public String getCode(){
		return (String) getData(CIDX_STOCK);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_STOCK);
	}
	
	public String getBoard(){
		return (String) getData(CIDX_BOARD);
	}
	public void setBoard(String sinput){
		setData(sinput, CIDX_BOARD);
	}
	
	public String getSide(){
		return (String) getData(CIDX_SIDE);
	}
	public void setSide(String sinput){
		setData(sinput, CIDX_SIDE);
	}
	
	public Double getPrice(){
		return (Double) getData(CIDX_PRICE);
	}
	public void setPrice(Double sinput){
		setData(sinput, CIDX_PRICE);
	}

	public Double getLot(){
		return (Double) getData(CIDX_LOT);
	}
	public void setLot(Double sinput){
		setData(sinput, CIDX_LOT);
	}

	public Double getFreq(){
		return (Double) getData(CIDX_FREQ);
	}
	public void setFreq(Double sinput){
		setData(sinput, CIDX_FREQ);
	}

	public Double getPrev(){
		return (Double) getData(CIDX_PREV);
	}
	public void setPrev(Double sinput){
		setData(sinput, CIDX_PREV);
	}
	
	public Double getNumber(){
		return (Double) getData(CIDX_NUMBER);
	}
	public void setNumber(Double sinput){
		setData(sinput, CIDX_NUMBER);
	}
}