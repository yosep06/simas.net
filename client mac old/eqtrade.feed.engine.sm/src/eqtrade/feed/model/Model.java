package eqtrade.feed.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.Utils;

public  abstract class Model extends Row implements Serializable {
	private static final long serialVersionUID = 1L;
	protected Vector 	vdata;
	protected int			nsize;
	public static final int		CIDX_HEADER = 0;
	public static final int		CIDX_TYPE= 1;
	public static final int		CIDX_SEQNO = 2;

	
	public Model(int nsizepar){
		this.nsize = nsizepar;
		this.vdata = new Vector(nsize, 10);
		this.initData();
		convertType();
	}
	
	public String getHeader(){
		return (String) getData(CIDX_HEADER);
	}
	public void setHeader(String sinput){
		setData(sinput, CIDX_HEADER);
	}
	
	public String getType(){
		return (String) getData(CIDX_TYPE);
	}
	public void setType(String sinput){
		setData(sinput, CIDX_TYPE);
	}
	
	public double getSeqno(){
		return Double.parseDouble((String)getData(CIDX_SEQNO));
		//System.out.println((Double)getData(CIDX_SEQNO));
		//return Double.parseDouble((String)getData(CIDX_SEQNO));
		
		//return (Double)getData(CIDX_SEQNO);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_SEQNO);
	}

	public Model(int nsizepar, String smsg){
		this.nsize = nsizepar;
		if (smsg.trim().length() > 0){
			vdata = Utils.parser(smsg, "|");
			int diff = nsize - vdata.size();
			for (int i=0;i<diff;i++){
				vdata.addElement(new String(""));
			}
		} else {
			this.vdata = new Vector(nsize, 10);
			this.initData();
		}
		convertType();
	}
	
	protected abstract void convertType();

	protected void initData(){
		vdata.addAll(Collections.nCopies(nsize, ""));
	}

	public void fromString(String smsg){
		Vector vtemp = Utils.parser(smsg, "|");
		Collections.copy(vdata, vtemp);
		if (vdata.size()<nsize){
			vdata.addElement(new String(""));
		}
		convertType();
	}

	public void fromVector(Vector vmsg){
		Collections.copy(vdata, vmsg);
	}

	public Vector getData(){
		return (Vector) vdata.clone();
	}

	public int getColumnCount() {
		return nsize;
	}

	public Object getData(int arg0){
			return this.vdata.elementAt(arg0);
	}

	public void setData(Object arg0, int arg1){
		this.vdata.setElementAt(arg0, arg1);
	}
}