package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class CurrencyDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(News.CIDX_ID));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}	
    public static String[] dataHeader = new String[]{
		 "header", "type", "seqno", "Currency","Time", "Value", "+ / -","High","Low"};

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER,SwingConstants.CENTER,SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			0,0,0,100,100, 100,100,100,100
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6,7,8
	};	

	public static boolean[] columnsort = new boolean[]{
			false, false, false, true, false, false,false,false,false
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(Currency.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row curr) {
		List vec = new Vector(Currency.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Currency.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new CurrencyField(curr, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static CurrRender render = new CurrRender();	
    static class CurrRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new CurrencyRender();
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((CurrencyRender)renderer).setValue(value);
			return renderer;
		}
	}
}
