package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class RupsDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(Rups.CIDX_STOCK));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}	
    public static String[] dataHeader = new String[]{
		 "header", "type", "seqno", "Stock", "Date", "Time", "Description"};

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			0,0,0,50,50, 50,100
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6
	};	

	public static boolean[] columnsort = new boolean[]{
			true, true, true,true, true, true,true
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(Rups.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row rups) {
		List vec = new Vector(Rups.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Rups.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new RupsField(rups, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static RpsRender render = new RpsRender();	
    static class RpsRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new RupsRender();
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((RupsRender)renderer).setValue(value);
			return renderer;
		}
	}
}
