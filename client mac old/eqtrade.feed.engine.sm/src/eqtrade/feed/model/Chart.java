package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public final class Chart extends Model {
	private static final long serialVersionUID = 7891111031929287100L;
    public static final int		CIDX_STOCK = 0;
    public static final int		CIDX_DATE = 1;
    public static final int		CIDX_TIME = 2;
    public static final int		CIDX_HIGHPRICE = 3;
    public static final int 		CIDX_LOWPRICE = 4;
    public static final int		CIDX_CURRENTPRICE = 5;
    public static final int		CIDX_OPENPRICE = 6;
    public static final int		CIDX_VOLUME = 7;
    public static final int		CIDN_NUMBEROFFIELDS = 8;
    
    public String toString(){
    	return getStock()+"|"+getDate()+"|"+getTime()+"|"+getHighPrice().doubleValue()+"|"+
    	getLowPrice().doubleValue()+"|"+getCurrentPrice().doubleValue()+"|"+getOpenPrice().doubleValue()+"|"+getVolume().doubleValue();
    }

	public Chart(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Chart(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 3: case 4: case 5: case 6: case 7:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}
    
	public String getStock(){
		return (String) getData(CIDX_STOCK);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_STOCK);
	}
	public String getDate(){
		return (String) getData(CIDX_DATE);
	}
	public void setDate(String sinput){
		setData(sinput, CIDX_DATE);
	}
	public String getTime(){
		return (String) getData(CIDX_TIME);
	}
	public void setTime(String sinput){
		setData(sinput, CIDX_TIME);
	}
	public Double getHighPrice(){
		return (Double) getData(CIDX_HIGHPRICE);
	}
	public void setHighPrice(Double sinput){
		setData(sinput, CIDX_HIGHPRICE);
	}

	public Double getLowPrice(){
		return (Double) getData(CIDX_LOWPRICE);
	}
	public void setLowPrice(Double sinput){
		setData(sinput, CIDX_LOWPRICE);
	}

	public Double getCurrentPrice(){
		return (Double) getData(CIDX_CURRENTPRICE);
	}
	public void setCurrentPrice(Double sinput){
		setData(sinput, CIDX_CURRENTPRICE);
	}

	public Double getOpenPrice(){
		return (Double) getData(CIDX_OPENPRICE);
	}
	public void setOpenPrice(Double sinput){
		setData(sinput, CIDX_OPENPRICE);
	}

	public Double getVolume(){
		return (Double) getData(CIDX_VOLUME);
	}
	public void setVolume(Double sinput){
		setData(sinput, CIDX_VOLUME);
	}
}
