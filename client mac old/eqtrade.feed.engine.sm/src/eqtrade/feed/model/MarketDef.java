package eqtrade.feed.model;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class MarketDef {
    public final static HashMap boardMap = new HashMap(5);    
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(0));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}
    static {
        boardMap.put("RG", "Regular");
        //boardMap.put("TS", "Crossing");
        boardMap.put("TN", "Cash Market");
        boardMap.put("NG", "Negotiated");
        boardMap.put("TOTAL", "IDX");
        boardMap.put("SPACE", "");
        boardMap.put("FOREIGNBUY","Foreign Buy");
        boardMap.put("FOREIGNSELL","Foreign Sell");
        boardMap.put("FOREIGN%", "Foreign %");
        boardMap.put("FOREIGNNET", "Foreign Net");
    }

    public static String[] dataHeader = new String[]{
		"Market", "Value", "Volume", "Freq"};

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			150,150,100,100
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3
	};	

	public static boolean[] columnsort = new boolean[]{
			false, false, false, false
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(Market.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row market) {
		List vec = new Vector(Market.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Market.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new MarketField(market, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static MRender render = new MRender();
	
    static class MRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new MarketRender();

		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((MarketRender)renderer).setValue(value);
			return renderer;
		}
	}
}
