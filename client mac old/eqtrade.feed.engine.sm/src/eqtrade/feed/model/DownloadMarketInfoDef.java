package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

import eqtrade.feed.model.DownloadMarketInfoDef.TRRender;

public final class DownloadMarketInfoDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(100));
        htable.put("sortascending", new Boolean(false));
        return htable;
	}
	
	public static String[] dataHeader = new String[]{
	"Date","FileName","Download"};

	public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER		
	};

	public  static int[] defaultColumnWidth = new int[]{
			25,25,25
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2
	};	

	public static boolean[] columnsort = new boolean[]{
			false, false, false
	};
	
	 public static Vector getHeader() {
			Vector header = new Vector(DownloadMarketInfo.CIDN_NUMBEROFFIELDS);
			for (int i = 0; i < dataHeader.length; i++) {
				header.addElement(dataHeader[i]);
			}
			return header;
		}
		
	   public static List createTableRow(Row downloadmarketinfo) {
			List vec = new Vector(DownloadMarketInfo.CIDN_NUMBEROFFIELDS);
			for (int i = 0; i < DownloadMarketInfo.CIDN_NUMBEROFFIELDS; i++) {
				
				try {
					vec.add(i, new DownloadMarketInfoField(downloadmarketinfo, i));
				} catch (Exception e) {
				}
			}
			return vec;
		}
		
		public final static TRRender render = new TRRender();
		
	   static class TRRender extends MutableIDisplayAdapter{
			DefaultTableCellRenderer renderer = new DownloadMarketInfoRender();

			public TableCellRenderer getTableCellRenderer(JTable tabelDownloadMarketInfo, Object value, boolean isSelected, boolean hasFocus, int row, int column){
				renderer.getTableCellRendererComponent(tabelDownloadMarketInfo, value,isSelected, hasFocus, row, column);
				((DownloadMarketInfoRender)renderer).setValue(value);
				return renderer;
			}
		}

}
