package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public final class GlobalIndices extends Model {
	private static final long serialVersionUID = 7911973356856075647L;
	public static final int		CIDX_CODE = 3;
	public static final int		CIDX_LAST = 4;
	public static final int 	CIDX_PREV = 5;
	public static final int		CIDX_HIGH = 6 ;
	public static final int		CIDX_LOW = 7;
	public static final int		CIDX_OPEN =8;
	public static final int 	CIDX_CHANGE = 9;
	public static final int		CIDX_PERCENT = 10;
	public static final int		CIDX_URUTAN = 11;
	public static final int 	CIDN_NUMBEROFFIELDS = 12;

	public GlobalIndices(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public GlobalIndices(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}
	
	public String getCode(){
		return (String) getData(CIDX_CODE);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_CODE);
	}

	public Double getLast(){
		return (Double) getData(CIDX_LAST);
	}
	public void setLast(Double sinput){
		setData(sinput, CIDX_LAST);
	}
	public Double getPrev(){
		return (Double) getData(CIDX_PREV);
	}
	public void setPrev(Double sinput){
		setData(sinput, CIDX_PREV);
	}
	
	public Double getChange(){
		return (Double) getData(CIDX_CHANGE);
	}
	public void setChange(Double sinput){
		setData(sinput, CIDX_CHANGE);
	}

	public Double getPercent(){
		return (Double) getData(CIDX_PERCENT);
	}
	public void setPercent(Double sinput){
		setData(sinput, CIDX_PERCENT);
	}
	
	public Double getUrutan(){
		return (Double) getData(CIDX_URUTAN);
	}
	public void setUrutan(Double sinput){
		setData(sinput, CIDX_URUTAN);
	}
	public Double getHigh(){
		return (Double) getData(CIDX_HIGH);
	}
	public void setHigh(Double sinput){
		setData(sinput, CIDX_HIGH);
	}
	public Double getLow(){
		return (Double) getData(CIDX_LOW);
	}
	public void setLow(Double sinput){
		setData(sinput, CIDX_LOW);
	}
	public Double getOpen(){
		return (Double) getData(CIDX_OPEN);
	}
	public void setOpen(Double sinput){
		setData(sinput, CIDX_OPEN);
	}
}