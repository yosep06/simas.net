package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public final class Summary extends Model {
	private static final long serialVersionUID = 7891111031929287100L;
    public static final int		CIDX_CODE = 0;
    public static final int		CIDX_INFO1 = 1;
    public static final int 		CIDX_INFO2 = 2;
    public static final int		CIDX_INFO3 = 3;
    public static final int		CIDX_INFO4 = 4;
    public static final int 		CIDX_INFO5 = 5;
    public static final int 		CIDX_INFO6 = 6;
    public static final int 		CIDX_INFO7 = 7;
    public static final int 		CIDX_INFO8 = 8;
    public static final int 		CIDX_INFO9 = 9;
    public static final int 		CIDX_INFO10 = 10;
    public static final int		CIDN_NUMBEROFFIELDS = 11;

	public Summary(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Summary(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 1: case 2: case 3: case 4: case 5:case 6: case 7: case 8: case 9: case 10:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}
    
	public String getCode(){
		return (String) getData(CIDX_CODE);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_CODE);
	}

	public Double getInfo1(){
		return (Double) getData(CIDX_INFO1);
	}
	public void setInfo1(Double sinput){
		setData(sinput, CIDX_INFO1);
	}

	public Double getInfo2(){
		return (Double) getData(CIDX_INFO2);
	}
	public void setInfo2(Double sinput){
		setData(sinput, CIDX_INFO2);
	}

	public Double getInfo3(){
		return (Double) getData(CIDX_INFO3);
	}
	public void setInfo3(Double sinput){
		setData(sinput, CIDX_INFO3);
	}
	
	public Double getInfo4(){
		return (Double) getData(CIDX_INFO4);
	}
	public void setInfo4(Double sinput){
		setData(sinput, CIDX_INFO4);
	}

	public Double getInfo5(){
		return (Double) getData(CIDX_INFO5);
	}
	public void setInfo5(Double sinput){
		setData(sinput, CIDX_INFO5);
	}
	public Double getInfo6(){
		return (Double) getData(CIDX_INFO6);
	}
	public void setInfo6(Double sinput){
		setData(sinput, CIDX_INFO6);
	}
	public Double getInfo7(){
		return (Double) getData(CIDX_INFO7);
	}
	public void setInfo7(Double sinput){
		setData(sinput, CIDX_INFO7);
	}
	public Double getInfo8(){
		return (Double) getData(CIDX_INFO8);
	}
	public void setInfo8(Double sinput){
		setData(sinput, CIDX_INFO8);
	}
	public Double getInfo9(){
		return (Double) getData(CIDX_INFO9);
	}
	public void setInfo9(Double sinput){
		setData(sinput, CIDX_INFO9);
	}
	public Double getInfo10(){
		return (Double) getData(CIDX_INFO10);
	}
	public void setInfo10(Double sinput){
		setData(sinput, CIDX_INFO10);
	}

}
