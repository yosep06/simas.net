package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.sun.org.apache.xml.internal.serializer.utils.Utils;
import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class GlobalIndicesRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     private String strFieldName = new String("");
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	 private static NumberFormat formatter2 = new DecimalFormat("#,##0.000  ");
     private static Color newBack;
     private static Color newFore;
     private static SelectedBorder border = new SelectedBorder();
	 private static  Icon upArrow = new ImageIcon(Utils.class.getResource("/eqtrade/feed/images/uparrow.gif"));
	 private static  Icon downArrow = new ImageIcon(Utils.class.getResource("/eqtrade/feed/images/downarrow.gif"));
	 private static  Icon noArrow = new ImageIcon(Utils.class.getResource("/eqtrade/feed/images/noarrow.gif"));
     


	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){

	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        strFieldName = table.getColumnName(column);
        if (value instanceof MutableIData) {
    		//newBack =  (((row+1) % 6) == 0 || ((row+2) % 6 == 0) || ((row+3) % 6 == 0)) ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
			newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
            newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
            GlobalIndices in = (GlobalIndices)((MutableIData)value).getSource();
	       	 if (
	        		 strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_LAST]) ||
	        		 strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_CHANGE]) ||
	        		 strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_PERCENT]) ||
	        		 strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_HIGH]) ||
	        		 strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_LOW]) ||
	        		 strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_OPEN]))
	        		 {
		            if (in.getChange().doubleValue()>0) {
		                newFore = FeedSetting.getColor(FeedSetting.C_PLUS);   
		            } else if (in.getChange().doubleValue()<0) {
		                newFore = FeedSetting.getColor(FeedSetting.C_MINUS);   
		            } else {
		                newFore = FeedSetting.getColor(FeedSetting.C_ZERO);                    
		            }
	       	 } else if (strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_PREV])){
	       		 newFore = FeedSetting.getColor(FeedSetting.C_ZERO);
	       	 } else {
	       		 newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
	       	 }

        }
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);           
        
	 	return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
	             if (dat instanceof Double) {
		            	 if (strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_LAST]) ||
		            		 strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_PREV]) ||
		            		 strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_PERCENT])){
		            		 setIcon(null);
		            		 setText(formatter2.format(dat));
		            	 } else if (strFieldName.equals(GlobalIndicesDef.dataHeader[GlobalIndices.CIDX_CHANGE])){
			            	 double val = ((Double)dat).doubleValue();
		            		 setIcon(val>0 ? upArrow : (val<0? downArrow : noArrow));
		            		 setIconTextGap(2);
		            		 setHorizontalTextPosition(LEFT);
		            		 setText(formatter2.format(dat));
		            	 } else {
		            		 setIcon(null);
		            		 setText(formatter.format(dat));
		            	 }
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	 setIcon(null);
	            	 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
