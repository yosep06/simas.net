package eqtrade.feed.model;

import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class SummaryField extends Column implements MutableIDisplayIData {
    public SummaryField(Row source, int idx) throws Exception{
        super(source, idx);
    }
    
    public MutableIDisplay getMutableIDisplay() {
		return SummaryDef.render;
	}

	public ImmutableIDisplay getImmutableIDisplay() {
		return SummaryDef.render;
	}
}
