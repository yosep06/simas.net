package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

import eqtrade.feed.model.GlobalIndicesDef.GINRender;

public final class CommodityDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(Commodity.CIDX_NAME));
        htable.put("sortascending", new Boolean(true)); 
        return htable; 
	}
    public static String[] dataHeader = new String[]{
    	//code|basevalue|marketvalue|index|open|high|low|previous
		"header", "type", "seqno", "Name","Detail","Market", "Last", "Close","High","Low","Open", "+ / -", "%"};

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			0,0,0,100,100,100,100,100,100,100,100,100,100
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6,7,8,9,10,11,12
	};	

	public static boolean[] columnsort = new boolean[]{
			false, false, false, true, true,false,false,false,false,false,false,false,false
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(Commodity.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row indices) {
		List vec = new Vector(Commodity.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Commodity.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new CommodityField(indices, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static ComRender render = new ComRender();
	
    static class ComRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new CommodityRender();

		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((CommodityRender)renderer).setValue(value);
			return renderer;
		}
	}
}
