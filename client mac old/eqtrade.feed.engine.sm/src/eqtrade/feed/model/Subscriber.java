package eqtrade.feed.model;

import java.util.Hashtable;

public class Subscriber extends Hashtable {
    private static final long serialVersionUID = 122181263966203689L;
    
    public Subscriber(){
        super(4,1);
    }
    
    public Subscriber(String key, int seqno){
        this(key, seqno, 0);
    }
    
    public Subscriber(String key, int seqno, int max){
        this();
        put("key", key);
        put("seqno", new Integer(seqno));
        put("max", new Integer(max));
        put("reset", new Boolean(false));
        put("listener", new Integer(0));
    }
    
    public String getRequest(){
    	return getKey()+"|"+getSeqno();
    }
    public void setSeqno(int seqno){
        put("seqno", new Integer(seqno));
    }
    
    public int getSeqno(){
        return ((Integer)get("seqno")).intValue();
    }
    
    public int getMax(){
        return ((Integer)get("max")).intValue();        
    }
    
    public String getKey(){
        return (String)get("key");
    }
    
    public void setReset(boolean reset){
        put("reset", new Boolean(reset));
    }
    
    public int addSubscriber(){
        Integer i = (Integer)get("listener");
        put("listener", new Integer(i.intValue()+1));
        return i.intValue() + 1;
    }
    
    public int getSubscriber(){
        Integer i = (Integer)get("listener");
        return i.intValue();
    }
    
    public int removeSubsriber(){
        Integer i = (Integer)get("listener");
        if (i.intValue()-1 <0) 
            put("listener", new Integer(0));       
        else
        	put("listener", new Integer(i.intValue()-1));       
        return i.intValue() - 1;
    }
    
    public boolean hasSubscriber(){
        return ((Integer)get("listener")).intValue() > 0;
    }
    
    public boolean hasMax(){
        return ((Integer)get("max")).intValue() > 0;
    }
    
    public void reset(){
        setSeqno(0);
//        if (((Boolean)get("reset")).booleanValue()){
//        }
    }
}
