package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.sun.org.apache.xml.internal.serializer.utils.Utils;
import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class CurrencyRender extends DefaultTableCellRenderer{
	private Log log=LogFactory.getLog(getClass());
	private static final long serialVersionUID = 213203526059607433L;
    private String strFieldName = new String("");
    
    private static NumberFormat formatter = new DecimalFormat("#,##0.0000");
    
    private static Color newBack;
    private static Color newFore;
    private static SelectedBorder border = new SelectedBorder();
	 private static  Icon upArrow = new ImageIcon(Utils.class.getResource("/eqtrade/feed/images/uparrow.gif"));
	 private static  Icon downArrow = new ImageIcon(Utils.class.getResource("/eqtrade/feed/images/downarrow.gif"));
	 private static  Icon noArrow = new ImageIcon(Utils.class.getResource("/eqtrade/feed/images/noarrow.gif"));

	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
        Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        strFieldName = table.getColumnName(column);
        if (value instanceof MutableIData) {
			newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
            newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
            Currency in = (Currency)((MutableIData)value).getSource();
	       	 if (strFieldName.equals(CurrencyDef.dataHeader[Currency.CIDX_VALUE]) ||
	        		 strFieldName.equals(CurrencyDef.dataHeader[Currency.CIDX_CHANGE])||
	        		 strFieldName.equals(CurrencyDef.dataHeader[Currency.CIDX_HIGH]) ||
	        		 strFieldName.equals(CurrencyDef.dataHeader[Currency.CIDX_LOW])
	       	 		){
		            if (in.getChange().doubleValue()>0) {
		                newFore = FeedSetting.getColor(FeedSetting.C_PLUS);   
		            } else if (in.getChange().doubleValue()<0) {
		                newFore = FeedSetting.getColor(FeedSetting.C_MINUS);   
		            } else {
		                newFore = FeedSetting.getColor(FeedSetting.C_ZERO);                    
		            }
	       	 } else {
	       		 newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
	       	 }

        }
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);  
	 	return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
	             //log.info((String)dat);
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
	             if (dat instanceof Double) {
            		 if (strFieldName.equals(CurrencyDef.dataHeader[Currency.CIDX_CHANGE])){
		            	 double val = ((Double)dat).doubleValue();
	            		 setIcon(val>0 ? upArrow : (val<0? downArrow : noArrow));
	            		 setIconTextGap(2);
	            		 setHorizontalTextPosition(LEFT);
	            	 } 	  else {
	            		 setIcon(null);
	            	 }
            		 
            		// if (Math.floor(Double.parseDouble((String)dat))!=Double.parseDouble((String)dat)){
            		 setText(formatter.format(dat));
            		 //} else {
            		//	 setText((String)dat);
            		 //}	 
            			 
	            	 //setText( (((Double)dat).doubleValue() == 0) ? "" : formatter.format(dat));
	             
	             
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	setIcon(null);
	            	setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
