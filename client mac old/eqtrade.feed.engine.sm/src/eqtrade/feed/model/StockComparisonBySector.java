package eqtrade.feed.model;



import eqtrade.feed.core.Utils;

public final class StockComparisonBySector extends Model{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7039472284777030369L;
	public static final int CIDX_STOCK = 3;
	public static final int CIDX_DER 	=4;
	public static final int CIDX_ROA	=5;
	public static final int CIDX_ROE	=6;
	public static final int CIDX_NPM	=7;
	public static final int CIDX_OPM	=8;
	public static final int CIDX_EPS	=9;
	public static final int CIDX_BV	=10;
	public static final int CIDX_PER	=13;
	public static final int CIDX_SECTOR = 11;
	/*public static final int CIDX_LASTUPDATE=11;*/
	public static final int CIDX_CLOSE=12;
	public static final int CIDN_NUMBEROFFIELDS = 14;
	
	public StockComparisonBySector(){
		super(CIDN_NUMBEROFFIELDS);
	}
	public StockComparisonBySector(String smsg) {
		super(CIDN_NUMBEROFFIELDS,smsg);
	}

	@Override
	protected void convertType() {
		for(int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i){
			case 4: case 5: case 6: case 7: case 8: case 9: case 10:  case 11:case 12:  case 13:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;
				default:
					break;
			}
		}
	}
	public String getStock() {
		return (String) getData(CIDX_STOCK);
	}
	public void seStock(String sinput) {
		//System.out.println("Stock Comparison ________"+sinput);
		setData(sinput,CIDX_STOCK );
	}
	public Double getDer() {
		return (Double) getData(CIDX_DER);
	}
	public void setDer(Double sinput) {
		setData(sinput,CIDX_DER);
	}
	public Double getRoa() {
		return (Double) getData(CIDX_ROA);
	}
	public void setRoa(Double sinput) {
		setData(sinput,CIDX_ROA);
	}
	public Double getRoe() {
		return (Double) getData(CIDX_ROE);
	}
	public void setRoe(Double sinput) {
		setData(sinput,CIDX_ROE);
	}
	public Double getNpm() {
		return (Double) getData(CIDX_NPM);
	}
	public void setNpm(Double sinput) {
		setData(sinput,CIDX_NPM);
	}
	public Double getOpm() {
		return (Double) getData(CIDX_OPM);
	}
	public void setOpm(Double sinput) {
		setData(sinput,CIDX_OPM);
	}
	public Double getEps() {
		return (Double) getData(CIDX_EPS);
	}
	public void setEps(String sinput) {
		setData(sinput,CIDX_EPS);
	}
	public Double getBv() {
		return (Double) getData(CIDX_BV);
	}
	public void setBv(Double sinput) {
		setData(sinput,CIDX_BV);
	}
	public Double getPer() {
		return (Double) getData(CIDX_PER);
	}
	public void setPer(Double sinput) {
		setData(sinput,CIDX_PER);
	}
	public Double getSector(){
		return  (Double)getData(CIDX_SECTOR);
	}
	public void setSector(Double sinput){
		setData(sinput, CIDX_SECTOR);
	}
	/*public String getLastUpdate(){
		return (String) getData(CIDX_LASTUPDATE);
	}
	public void setLastUpdate(String sinput){
		setData(sinput, CIDX_LASTUPDATE);
	}*/
	public Double getClose(){
		return (Double) getData(CIDX_CLOSE);
	}
	public void setClose(Double sinput){
		setData(sinput, CIDX_CLOSE);
	}
	
	
}
