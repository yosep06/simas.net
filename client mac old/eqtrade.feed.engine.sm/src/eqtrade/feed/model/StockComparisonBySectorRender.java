package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.SelectedBorder;
import eqtrade.feed.engine.FeedStore;

public class StockComparisonBySectorRender extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 441449167070233418L;
	
	private static NumberFormat formatter = new DecimalFormat();
	private static NumberFormat format= new DecimalFormat("###0");
	private String strFieldName = new String("");
	private static Color newBack;
	private static Color newFore;
	protected IFeedEngine engine;
	private static SelectedBorder border = new SelectedBorder();
	public static HashMap stockSector = new HashMap(200,5);
	
	 public static Color getSectorColor(String Stock){
		 	//stockSector.put("BTEK", "2");
		 	String sector = StockRender.getSector(Stock);
		 	sector = (sector==null)? "" : sector;
		 	Color s = FeedSetting.getColor(FeedSetting.C_FOREGROUND);  
		 	if (sector.equals("1")){
		 		s = FeedSetting.getColor(FeedSetting.C_AGRICULTURE);      
		 	} else if (sector.equals("2")){
		 		s = FeedSetting.getColor(FeedSetting.C_MINING);      
		 	} else if (sector.equals("3")){
		 		s = FeedSetting.getColor(FeedSetting.C_BASICINDUSTRY);      
		 	} else if (sector.equals("4")){
		 		s = FeedSetting.getColor(FeedSetting.C_MISCELLANEOUS);      
		 	} else if (sector.equals("5")){
		 		s = FeedSetting.getColor(FeedSetting.C_CONSUMER);      
		 	} else if (sector.equals("6")){
		 		s = FeedSetting.getColor(FeedSetting.C_PROPERTY);      
		 	} else if (sector.equals("7")){
		 		s = FeedSetting.getColor(FeedSetting.C_INFRASTRUCTURE);      
		 	} else if (sector.equals("8")){
		 		s = FeedSetting.getColor(FeedSetting.C_FINANCE);      
		 	} else if (sector.equals("9")){
		 		s = FeedSetting.getColor(FeedSetting.C_TRADE);      
		 	} 
		 	//System.out.println("keluar color "+Stock+" "+sector+" "+stockSector.toString());
	    	 return s;
	     }
	 
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column){
		 Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
//       strFieldName = table.getColumnName(column);
		 StockComparisonBySector s =null;
		 if (value instanceof MutableIData) {
     		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
	       		s = (StockComparisonBySector)((MutableIData)value).getSource(); 
	       		newFore = getSectorColor(s.getStock()); 
         }           
		 
		 //System.out.println("masuk color "+s.getStock()+" "+newFore);
		
		/* if (value instanceof MutableIData) {	        
	        	Component component = (StockComparisonBySectorRender)((MutableIData)value).getSource();
	    		newBack =  (column % 2) == 0 ? FeedSetting.getSectorColor(FeedStore.DATA_SECTOR) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
	    		 if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_PREVIOUS])){
	    			 newFore = FeedSetting.getColor(FeedSetting.C_ZERO);*/
		
		/*Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
        newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND); 
        //newColor = FeedSetting.getColor(FeedSetting.C_BACKGROUND);
*/      
		if(isSelected){
        	((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);
		return component;
	}
	public void setValue(Object value){
		try{
			if(value instanceof MutableIData){
				MutableIData args = (MutableIData)value;
				Object dat = args.getData();
				setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT:JLabel.LEFT);
				if(dat instanceof Double){
						setText((((Double)dat).doubleValue()==0)?"":formatter.format(dat));
				}else if(dat ==null){
					setText("");
				}else{
					if(strFieldName.equals(StockComparisonBySectorDef.dataHeader[StockComparisonBySector.CIDX_STOCK])){
						setHorizontalTextPosition(JLabel.LEFT);
					}
					setText(""+dat.toString());
				}
			}
		}catch (Exception e) {
		}
	}
	
	public static void setSector(String Stock, String Sector){
   	 stockSector.put(Stock, Sector);
   	 //getSectorColor(stock);
  }
	
	/*protected String getSectorbyStock(String code){
        //Stock s = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new String[]{code}));
        Stock stocksector = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new String[]{code}));        
        return (stocksector != null) ? stocksector.getSector() : "";
    }*/
	
	public static String getSector(String Stock){
   	 return (String)stockSector.get(Stock);
    }
}

//package eqtrade.feed.model;
//
//import java.awt.Color;
//import java.awt.Component;
//import java.text.DecimalFormat;
//import java.text.NumberFormat;
//import java.util.HashMap;
//
//import javax.swing.JLabel;
//import javax.swing.JTable;
//import javax.swing.table.DefaultTableCellRenderer;
//
//import com.vollux.idata.indirection.MutableIData;
//
//import eqtrade.feed.core.FeedSetting;
//import eqtrade.feed.core.IFeedEngine;
//import eqtrade.feed.core.SelectedBorder;
//import eqtrade.feed.engine.FeedStore;
//
//public class StockComparisonBySectorRender extends DefaultTableCellRenderer {
//	private static final long serialVersionUID = 441449167070233418L;
//	
//	private static NumberFormat formatter = new DecimalFormat();
//	private static NumberFormat format= new DecimalFormat("###0");
//	private String strFieldName = new String("");
//	private static Color newBack;
//	private static Color newFore;
//	protected IFeedEngine engine;
//	private static SelectedBorder border = new SelectedBorder();
//	public static HashMap stockSector = new HashMap(200,5);
//	
//	 public static Color getSectorColor(String Stock){
//		 	//stockSector.put("BTEK", "2");
//		 	String sector = StockRender.getSector(Stock);
//		 	sector = (sector==null)? "" : sector;
//		 	Color s = FeedSetting.getColor(FeedSetting.C_FOREGROUND);  
//		 	if (sector.equals("1")){
//		 		s = FeedSetting.getColor(FeedSetting.C_AGRICULTURE);      
//		 	} else if (sector.equals("2")){
//		 		s = FeedSetting.getColor(FeedSetting.C_MINING);      
//		 	} else if (sector.equals("3")){
//		 		s = FeedSetting.getColor(FeedSetting.C_BASICINDUSTRY);      
//		 	} else if (sector.equals("4")){
//		 		s = FeedSetting.getColor(FeedSetting.C_MISCELLANEOUS);      
//		 	} else if (sector.equals("5")){
//		 		s = FeedSetting.getColor(FeedSetting.C_CONSUMER);      
//		 	} else if (sector.equals("6")){
//		 		s = FeedSetting.getColor(FeedSetting.C_PROPERTY);      
//		 	} else if (sector.equals("7")){
//		 		s = FeedSetting.getColor(FeedSetting.C_INFRASTRUCTURE);      
//		 	} else if (sector.equals("8")){
//		 		s = FeedSetting.getColor(FeedSetting.C_FINANCE);      
//		 	} else if (sector.equals("9")){
//		 		s = FeedSetting.getColor(FeedSetting.C_TRADE);      
//		 	} 
//		 	//System.out.println("keluar color "+Stock+" "+sector+" "+stockSector.toString());
//	    	 return s;
//	     }
//	 
//	public Component getTableCellRendererComponent(JTable table, Object value,
//			boolean isSelected, boolean hasFocus, int row, int column){
//		 Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
////       strFieldName = table.getColumnName(column);
//		 StockComparisonBySector s =null;
//		 if (value instanceof MutableIData) {
//     		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
//	       		s = (StockComparisonBySector)((MutableIData)value).getSource(); 
//	       		newFore = getSectorColor(s.getStock()); 
//         }           
//		 
//		 //System.out.println("masuk color "+s.getStock()+" "+newFore);
//		
//		/* if (value instanceof MutableIData) {	        
//	        	Component component = (StockComparisonBySectorRender)((MutableIData)value).getSource();
//	    		newBack =  (column % 2) == 0 ? FeedSetting.getSectorColor(FeedStore.DATA_SECTOR) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
//	    		 if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_PREVIOUS])){
//	    			 newFore = FeedSetting.getColor(FeedSetting.C_ZERO);*/
//		
//		/*Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
//        newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND); 
//        //newColor = FeedSetting.getColor(FeedSetting.C_BACKGROUND);
//*/      
//		if(isSelected){
//        	((JLabel)component).setBorder(border);
//        }
//        component.setBackground(newBack);
//        component.setForeground(newFore);
//		return component;
//	}
//	public void setValue(Object value){
//		try{
//			if(value instanceof MutableIData){
//				MutableIData args = (MutableIData)value;
//				Object dat = args.getData();
//				setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT:JLabel.LEFT);
//				if(dat instanceof Double){
//						setText((((Double)dat).doubleValue()==0)?"":formatter.format(dat));
//				}else if(dat ==null){
//					setText("");
//				}else{
//					if(strFieldName.equals(StockComparisonBySectorDef.dataHeader[StockComparisonBySector.CIDX_STOCK])){
//						setHorizontalTextPosition(JLabel.LEFT);
//					}
//					setText(""+dat.toString());
//				}
//			}
//		}catch (Exception e) {
//		}
//	}
//	
//	public static void setSector(String Stock, String Sector){
//   	 stockSector.put(Stock, Sector);
//   	 //getSectorColor(stock);
//  }
//	
//	/*protected String getSectorbyStock(String code){
//        //Stock s = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new String[]{code}));
//        Stock stocksector = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new String[]{code}));        
//        return (stocksector != null) ? stocksector.getSector() : "";
//    }*/
//	
//	public static String getSector(String Stock){
//   	 return (String)stockSector.get(Stock);
//    }
//}
