package eqtrade.feed.model;

public class DownloadMarketInfo extends Model {
	private static final long serialVersionUID = 1L;
	
	public static final int	CIDX_DATE = 0;
	public static final int	CIDX_FILENAME = 1;
	public static final int	CIDX_DOWNLOAD = 2;
	public static final int CIDN_NUMBEROFFIELDS = 3;
	
	public String getDate() {
		return (String) getData(CIDX_DATE);		
	}
	public void setDate(String sinput){
		setData(sinput, CIDX_DATE);
	}
	
	public String getFileName() {
		return (String) getData(CIDX_FILENAME);		
	}
	public void setFileName(String sinput){
		setData(sinput, CIDX_FILENAME);
	}
	
	public String getDownload() {
		return (String) getData(CIDX_DOWNLOAD);		
	}
	public void setDownload(String sinput){
		setData(sinput, CIDX_DOWNLOAD);
	}
	

	public DownloadMarketInfo() {
		super(CIDN_NUMBEROFFIELDS);
	}
	
	public DownloadMarketInfo(String smsg) {
		super(CIDN_NUMBEROFFIELDS, smsg);		
	}
	
	@Override
	protected void convertType() {
		
	}

}
