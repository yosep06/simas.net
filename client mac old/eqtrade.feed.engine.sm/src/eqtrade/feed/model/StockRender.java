package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class StockRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
//     private String strFieldName = new String("");
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
     private static Color newBack;
     private static Color newFore;
     private static SelectedBorder border = new SelectedBorder();
     public static HashMap stockSector = new HashMap(200,5);
     
     public static Color getSectorColor(String stock){
	 	String sector = getSector(stock);
	 	sector = (sector==null)? "" : sector;
	 	Color c = FeedSetting.getColor(FeedSetting.C_FOREGROUND);      
	 	if (sector.equals("1")){
	 		c = FeedSetting.getColor(FeedSetting.C_AGRICULTURE);      
	 	} else if (sector.equals("2")){
	 		c = FeedSetting.getColor(FeedSetting.C_MINING);      
	 	} else if (sector.equals("3")){
	 		c = FeedSetting.getColor(FeedSetting.C_BASICINDUSTRY);      
	 	} else if (sector.equals("4")){
	 		c = FeedSetting.getColor(FeedSetting.C_MISCELLANEOUS);      
	 	} else if (sector.equals("5")){
	 		c = FeedSetting.getColor(FeedSetting.C_CONSUMER);      
	 	} else if (sector.equals("6")){
	 		c = FeedSetting.getColor(FeedSetting.C_PROPERTY);      
	 	} else if (sector.equals("7")){
	 		c = FeedSetting.getColor(FeedSetting.C_INFRASTRUCTURE);      
	 	} else if (sector.equals("8")){
	 		c = FeedSetting.getColor(FeedSetting.C_FINANCE);      
	 	} else if (sector.equals("9")){
	 		c = FeedSetting.getColor(FeedSetting.C_TRADE);      
	 	} 
    	 return c;
     }
     
     public static void setSector(String stock, String sector){
    	 stockSector.put(stock, sector);
     }
     
     public static String getSector(String stock){
    	 return (String)stockSector.get(stock);
     }

	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
            Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
//            strFieldName = table.getColumnName(column);
            if (value instanceof MutableIData) {
        		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
	       		Stock s = (Stock)((MutableIData)value).getSource(); 
	       		newFore = getSectorColor(s.getCode()); 
            }            
            if (isSelected) {
                ((JLabel)component).setBorder(border);
            }
            component.setBackground(newBack);
            component.setForeground(newFore);   
            return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
	             if (dat instanceof Double) {
	                 setText(formatter.format(dat));
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
