package eqtrade.feed.model;


public final class Rups extends Model {
	private static final long serialVersionUID = 1681132210428041847L;
	public static final int  CIDX_STOCK = 3;
	public static final int	CIDX_DATE = 4; 
    public static final int	CIDX_TIME = 5;
    public static final int  CIDX_DESCRIPTION = 6;
    public static final int 	CIDN_NUMBEROFFIELDS = 7;

	public Rups(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Rups(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
	}
	public String getDescription(){
        return (String) getData(CIDX_DESCRIPTION);
    }
    public void setDescription(String sinput){
        setData(sinput, CIDX_DESCRIPTION);
    }
    
    public String getDate(){
        return (String) getData(CIDX_DATE);
    }
    public void setDate(String sinput){
        setData(sinput, CIDX_DATE);
    }
    
    public String getTime(){
		return (String) getData(CIDX_TIME);
	}
	public void setTime(String sinput){
		setData(sinput, CIDX_TIME);
	}
	
	public String getStock(){
		return (String) getData(CIDX_STOCK);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_STOCK);
	}
}