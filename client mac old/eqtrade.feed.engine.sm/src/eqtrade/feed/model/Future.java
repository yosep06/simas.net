package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public final class Future extends Model {
	private static final long serialVersionUID = 1681132210428041847L;
	public static final int CIDX_NAME = 3;
	public static final int CIDX_DETAIL = 4;
	public static final int CIDX_MARKET = 5;
	public static final int	CIDX_LAST = 6;
	public static final int	CIDX_CLOSE = 7; 
    public static final int	CIDX_HIGH = 8;
    public static final int	CIDX_LOW = 9; 
    public static final int	CIDX_OPEN = 10;
    public static final int	CIDX_CHANGE = 11;
    public static final int	CIDX_PERSEN = 12;
    public static final int CIDN_NUMBEROFFIELDS = 13;
    
	public Future(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Future(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 6:case 7: case 8: case 9: case 10:case 11:case 12:  
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;	
			
			default:
				break;
			}
		}
	}
    
    public Double getLast(){
        return (Double) getData(CIDX_LAST);
    }
    public void setLast(Double sinput){
        setData(sinput, CIDX_LAST);
    }
    
    public Double getChange(){
		return (Double) getData(CIDX_CHANGE);
	}
	public void setChange(String sinput){
		setData(sinput, CIDX_CHANGE);
	}
	public String getDetail(){
		return (String) getData(CIDX_DETAIL);
	}
	public void setDetail(String sinput){
		setData(sinput, CIDX_DETAIL);
	}
	public String getMarket(){
		return (String) getData(CIDX_MARKET);
	}
	public void setMarket(String sinput){
		setData(sinput, CIDX_MARKET);
	}
	public String getName(){
		return (String) getData(CIDX_NAME);
	}
	public void setName(String sinput){
		setData(sinput, CIDX_NAME);
	}
	 public Double getHigh(){
			return (Double) getData(CIDX_HIGH);
	}
	public void setHigh(String sinput){
			setData(sinput, CIDX_HIGH);
	}
	public Double getLow(){
		return (Double) getData(CIDX_LOW);
	}
	public void setLow(String sinput){
		setData(sinput, CIDX_LOW);
	}
	public String getOpen(){
		return (String) getData(CIDX_OPEN);
	}
	public void setOpen(String sinput){
		setData(sinput, CIDX_OPEN);
	}
	public String getClose(){
		return (String) getData(CIDX_CLOSE);
	}
	public void setClose(String sinput){
		setData(sinput, CIDX_CLOSE);
	}
	public String getPersen(){
		return (String) getData(CIDX_PERSEN);
	}
	public void setPersen(String sinput){
		setData(sinput, CIDX_PERSEN);
	}
}
