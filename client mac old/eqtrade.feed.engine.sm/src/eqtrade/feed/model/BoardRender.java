package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class BoardRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     //private String strFieldName = new String("");
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
     private static Color newBack;
     private static Color newFore;
     private static SelectedBorder border = new SelectedBorder();

	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){

	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        //strFieldName = table.getColumnName(column);
		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
        newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }        
        component.setBackground(newBack);
        component.setForeground(newFore);   
	 	return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
	             if (dat instanceof Double) {
	                 setText(formatter.format(dat));
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
