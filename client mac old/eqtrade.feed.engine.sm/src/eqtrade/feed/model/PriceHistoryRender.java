package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class PriceHistoryRender extends DefaultTableCellRenderer{
	private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	private static NumberFormat formatter3= new DecimalFormat("#,##0.00");
	private String strFieldName = new String("");
	private int nrow = -1;
	private static Color newBack;
	private static Color newFore;
	private static SelectedBorder border = new SelectedBorder();
	
	public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column){
		Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		strFieldName = table.getColumnName(column);
	 	nrow = row;
	 	
	 	//table.getCellEditor(1, 2);
//	 	System.out.println(table.getCellEditor(1, 2).toString());
	 	
	 	 if (value instanceof MutableIData) {	        
	        	PriceHistory ss = (PriceHistory)((MutableIData)value).getSource();
	    		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
	    		
	    		if (strFieldName.equals(PriceHistoryDef.dataHeader[PriceHistory.CIDX_LOW]) || strFieldName.equals(PriceHistoryDef.dataHeader[PriceHistory.CIDX_CLOSE]) || strFieldName.equals(PriceHistoryDef.dataHeader[PriceHistory.CIDX_HIGH]) || strFieldName.equals(PriceHistoryDef.dataHeader[PriceHistory.CIDX_OPEN])) {	    			
	    			newFore = ((Double)((MutableIData)value).getData()).doubleValue() > ss.getPrev().doubleValue() ? FeedSetting.getColor(FeedSetting.C_PLUS) : 
	    			 		 (((Double)((MutableIData)value).getData()).doubleValue() < ss.getPrev().doubleValue() ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
	    			if(ss.getOpen().doubleValue() == 0) newFore = FeedSetting.getColor(FeedSetting.C_ZERO);
	    		} else if (strFieldName.equals(PriceHistoryDef.dataHeader[PriceHistory.CIDX_DATE]) || strFieldName.equals(PriceHistoryDef.dataHeader[PriceHistory.CIDX_VOLUME]) || strFieldName.equals(PriceHistoryDef.dataHeader[PriceHistory.CIDX_FREQ])) {
	    			 newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);
	    		 } else {
	    			 newFore = StockRender.getSectorColor(ss.getDate());    			 
	    		 }
	        }
	 	
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }        
        
        component.setBackground(newBack);
        component.setForeground(newFore);   	 	
	 	return component;

	}
	
	public void setValue(Object value) {
		try {
			if (value instanceof MutableIData) {
				MutableIData args = (MutableIData)value;
				Object dat = args.getData();
				setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT:JLabel.LEFT);
				if (dat instanceof Double) {
					//setText((((Double)dat).doubleValue()==0)?"":formatter.format(dat));
					 setIcon(null);
	            	 double val = ((Double)dat).doubleValue();
	            	 if (Math.abs(val)>1000000000) {
		                 setText(formatter3.format(val / 1000000000)+"B  ");	            		 
	            		 
	            	 } else if (Math.abs(val)>1000000) {
		                 setText(formatter3.format(val / 1000000)+"M  ");	            		 
	            	 } else {
		                 setText(formatter.format(dat));	            		 	            		 
	            	 }
				} else if (dat ==null) {
					setText("0");
				} else {
					setText(""+dat.toString());
				}
			}
		} catch (Exception e) {
			return;
		}
	}
}
