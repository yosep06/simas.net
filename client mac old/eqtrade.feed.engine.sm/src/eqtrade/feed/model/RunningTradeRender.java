package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class RunningTradeRender extends DefaultTableCellRenderer{
	
	private static final long serialVersionUID = 213203526059607433L;
	private String strFieldName = new String("");
	private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	private static NumberFormat formatter2 = new DecimalFormat("#,##0.00  ");
	private static Color newBack;
	private static Color newFore;
	private static SelectedBorder border = new SelectedBorder();
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HHmmss");
	private static SimpleDateFormat timeNewFormat = new SimpleDateFormat(" HH:mm:ss");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	//private static final int lotsize = 500;

	 
	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);	 	
	 	strFieldName = table.getColumnName(column);
 	    if (value instanceof MutableIData) {
	        RunningTrade rt = (RunningTrade)((MutableIData)value).getSource();

	 	    if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_BUYER]) || strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_SELLER])){
                newFore = BrokerRender.getColor(((MutableIData)value).getData().toString());
            } else if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_BUY]) || strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_SELL])){
                if ("F".equals(((MutableIData)value).getData())) {
                    newFore = FeedSetting.getColor(FeedSetting.C_FOREIGN);   
                } else {
                    newFore = FeedSetting.getColor(FeedSetting.C_DOMESTIC);   
                }
            } else if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_STOCK]) ){
            	newFore = StockRender.getSectorColor(rt.getStock());
            } else if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_TXTIME]) || strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_BOARD])){
            	newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
            } else {
	 	    	if (rt.getChange().doubleValue()>0) {
	 	    		newFore = FeedSetting.getColor(FeedSetting.C_PLUS);   
	 	    	} else if (rt.getChange().doubleValue()<0) {
	 	    		newFore = FeedSetting.getColor(FeedSetting.C_MINUS);   
	 	    	} else {
	 	    		newFore = FeedSetting.getColor(FeedSetting.C_ZERO);   	 	    		
	 	    	}
	 	    }
	        if (rt.getBuyer().equals(FeedSetting.getSetting(FeedSetting.C_BROKER1)) || rt.getSeller().equals(FeedSetting.getSetting(FeedSetting.C_BROKER1))) {
	        	newBack = FeedSetting.getColor(FeedSetting.C_BROKERCOLOR1);   
	        } else if (rt.getBuyer().equals(rt.getSeller()) && !rt.getBuyer().equals("")){
	        	newBack = FeedSetting.getColor(FeedSetting.C_TS);   
 	    		newFore = FeedSetting.getColor(FeedSetting.C_TS).darker().darker().darker();   	 	    		
	        } else if (rt.getBuyer().equals(FeedSetting.getSetting(FeedSetting.C_BROKER2)) || rt.getSeller().equals(FeedSetting.getSetting(FeedSetting.C_BROKER2))) {
	        	newBack = FeedSetting.getColor(FeedSetting.C_BROKERCOLOR2);   
	        } else if (rt.getBuyer().equals(FeedSetting.getSetting(FeedSetting.C_BROKER3)) || rt.getSeller().equals(FeedSetting.getSetting(FeedSetting.C_BROKER3))) {
	        	newBack = FeedSetting.getColor(FeedSetting.C_BROKERCOLOR3);   
	        } else {
	    		//newBack =  (((row+1) % 4) == 0 || ((row+2) % 4 == 0)) ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
	    		//newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
	        	boolean hit = rt.getLast().doubleValue() == rt.getBestOffer().doubleValue();
	        	if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_BUYER]) || strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_BUY])){
		    		newBack =  hit ? FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN) : FeedSetting.getColor(FeedSetting.C_BACKGROUND);
	        	} else if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_SELLER]) || strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_SELL])){
		    		newBack =  hit ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);	        		
	        	} else {
	        		newBack =  FeedSetting.getColor(FeedSetting.C_BACKGROUND);
	        	}
	        }
 	    }
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
    	component.setBackground(newBack);
    	component.setForeground(newFore);    		
	 	return component;
    }

	 public Component getTableCellRendererComponentSimple(JTable table, Object value,
			 	boolean isSelected, boolean hasFocus, int row, int column){
		 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);	 	
		 	strFieldName = table.getColumnName(column);
	 	    if (value instanceof MutableIData) {
		        RunningTrade rt = (RunningTrade)((MutableIData)value).getSource();
		        newBack = FeedSetting.getColor(FeedSetting.C_BACKGROUND);

		 	    if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_BUYER]) || strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_SELLER])){
	                newFore = BrokerRender.getColor(((MutableIData)value).getData().toString());
	            } else if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_BUY]) || strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_SELL])){
	                if ("F".equals(((MutableIData)value).getData())) {
	                    newFore = FeedSetting.getColor(FeedSetting.C_FOREIGN);   
	                } else {
	                    newFore = FeedSetting.getColor(FeedSetting.C_DOMESTIC);   
	                }
	            } else if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_TXTIME])){
	            	newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
	            } else {
		 	    	if (rt.getChange().doubleValue()>0) {
		 	    		newFore = FeedSetting.getColor(FeedSetting.C_PLUS);   
		 	    	} else if (rt.getChange().doubleValue()<0) {
		 	    		newFore = FeedSetting.getColor(FeedSetting.C_MINUS);   
		 	    	} else {
		 	    		newFore = FeedSetting.getColor(FeedSetting.C_ZERO);   	 	    		
		 	    	}
		 	    }
	 	    }

	 	    if (isSelected) {
	            ((JLabel)component).setBorder(border);
	        }
	    	component.setBackground(newBack);
	    	component.setForeground(newFore);    		
		 	return component;
	    }
	 
	 public void setValue(Object value){
	     try {
	    	 if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             if (!((RunningTrade)args.getSource()).getTxTime().equals("")){
		             Object dat = args.getData();
                     setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
		             if (dat instanceof Double) {
		            	 if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_PERCENT])){
		            		 setText(formatter2.format(dat));
		            	 } else if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_CHANGE])){           		 
		            		 setText((((Double)dat).doubleValue() > 0 ? "+" : "")+formatter.format(dat));	            		 
		            	 } else if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_LOT])){   
		            		 //System.out.println("lot runtrade : "+Double.parseDouble(FeedSetting.getLot()));
		            		 setText(formatter.format(((Double)dat).doubleValue() / Double.parseDouble(FeedSetting.getLot())));           		 
		            	 } else {
		            		 setText(formatter.format(dat));
		            	 }
		             } else if (dat == null){
		                 setText(" ");
		             } else {
		            	 if (strFieldName.equals(RunningTradeDef.dataHeader[RunningTrade.CIDX_TXTIME])){
		            		 setText(timeNewFormat.format(timeFormat.parse(dateFormat.format(new Date())+" "+dat.toString())));
		            	 } else {
		            		 setText(" "+dat.toString());
		            	 }
		             }
	             } else {
	            	 setText(" ");
	             }
	    	 }
	     } catch (Exception e){
	     }
	 }
}
