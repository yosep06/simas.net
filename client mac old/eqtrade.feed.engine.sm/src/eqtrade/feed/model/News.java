package eqtrade.feed.model;


public final class News extends Model {
	private static final long serialVersionUID = 1681132210428041847L;
//	no|subject|title|content
	public static final int  CIDX_ID = 3;
    public static final int	CIDX_SUBJECT = 4;
    public static final int  CIDX_TITLE = 5;
    public static final int  CIDX_CONTENT = 6;
    public static final int 	CIDN_NUMBEROFFIELDS = 7;

	public News(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public News(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
	}
    
    public String getTitle(){
        return (String) getData(CIDX_TITLE);
    }
    public void setTitle(String sinput){
        setData(sinput, CIDX_TITLE);
    }
    
	public String getSubject(){
		return (String) getData(CIDX_SUBJECT);
	}
	public void setSubject(String sinput){
		setData(sinput, CIDX_SUBJECT);
	}
	
	public String getID(){
		return (String) getData(CIDX_ID);
	}
	public void setID(String sinput){
		setData(sinput, CIDX_ID);
	}

	public String getContent(){
		return (String) getData(CIDX_CONTENT);
	}
	public void setContent(String sinput){
		setData(sinput, CIDX_CONTENT);
	}
}