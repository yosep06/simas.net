package eqtrade.feed.model;

import java.math.BigDecimal;
import java.util.Vector;

import eqtrade.feed.core.Utils;


public final class RunningTrade extends Model {
	private static final long serialVersionUID = 1712405571062973537L;
//	time|stock|board|price|lot|buyer|buyertype|seller|sellertype|
//	bestbid|bestbidlot|bestoffer|bestofferlot|tradeno|previous|chg|%

	public static final int	CIDX_TXTIME = 3;
	public static final int	CIDX_STOCK = 4;
	public static final int	CIDX_BOARD = 5;
	public static final int	CIDX_LAST = 6;
	public static final int	CIDX_LOT = 7;
	public static final int	CIDX_BUYER = 8;
	public static final int	CIDX_BUY = 9;
	public static final int	CIDX_SELLER = 10;
	public static final int	CIDX_SELL = 11;
	public final static int CIDX_BESTBID = 12;
	public final static int CIDX_BESTBIDLOT = 13;
	public final static int CIDX_BESTOFFER = 14;
	public final static int CIDX_BESTOFFERLOT = 15;
	public final static int	CIDX_TRADENO = 16;
	public final static int CIDX_PREVIOUS = 17;
	public static final int	CIDX_CHANGE = 18;
	public final static int CIDX_PERCENT = 19;
	public static final int	CIDN_NUMBEROFFIELDS = 20;
	
	public RunningTrade(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public RunningTrade(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	protected void convertType() {
		String tempstr ="";
		
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			/*case 2:
				System.out.println("Coba ah");	
				System.out.println((String)vdata.elementAt(i));
				tempstr = (String)vdata.elementAt(i);
				if (tempstr.length()>0) {
					vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				} else
				{
					System.out.println("0");
					vdata.setElementAt(new Double(Utils.strToDouble("0", 0)), i);
				}
				break;*/
			case 6: case 7: case 12: case 13: case 14: case 15: case 17: case 18: case 19:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
		calculate();
	}

	public String getTxTime(){
		return (String) getData(CIDX_TXTIME);
	}
	public void setTxTime(String sinput){
		setData(sinput, CIDX_TXTIME);
	}

	public String getStock(){
		return (String) getData(CIDX_STOCK);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_STOCK);
	}

	public String getTradeno(){
		return (String) getData(CIDX_TRADENO);
	}
	public void setTradeno(String sinput){
		setData(sinput, CIDX_TRADENO);
	}

	public Double getLast(){
		return (Double) getData(CIDX_LAST);
	}
	public void setLast(Double sinput){
		setData(sinput, CIDX_LAST);
	}

	public Double getPrevious(){
		return (Double) getData(CIDX_PREVIOUS);
	}
	public void setPrevious(Double sinput){
		setData(sinput, CIDX_PREVIOUS);
	}

	public Double getLot(){
		return (Double) getData(CIDX_LOT);
	}
	public void setLot(Double sinput){
		setData(sinput, CIDX_LOT);
	}

	public String getBuy(){
		return (String) getData(CIDX_BUY);
	}
	public void setBuy(String sinput){
		setData(sinput, CIDX_BUY);
	}

	public String getBuyer(){
		return (String) getData(CIDX_BUYER);
	}
	public void setBuyer(String sinput){
		setData(sinput, CIDX_BUYER);
	}

	public String getSeller(){
		return (String) getData(CIDX_SELLER);
	}
	public void setSeller(String sinput){
		setData(sinput, CIDX_SELLER);
	}

	public Double getChange(){
		return (Double) getData(CIDX_CHANGE);
	}
	public void setChange(Double sinput){
		setData(sinput, CIDX_CHANGE);
	}

	public String getSell(){
		return (String) getData(CIDX_SELL);
	}
	public void setSell(String sinput){
		setData(sinput, CIDX_SELL);
	}

	public String getBoard(){
		return (String) getData(CIDX_BOARD);
	}
	public void setBoard(String sinput){
		setData(sinput, CIDX_BOARD);
	}
	
	public Double getBestBid(){
		return (Double) getData(CIDX_BESTBID);
	}
	public void setBestBid(Double sinput){
		setData(sinput, CIDX_BESTBID);
	}
	
	public Double getBestBidLot(){
		return (Double) getData(CIDX_BESTBIDLOT);
	}
	public void setBestBidLot(Double sinput){
		setData(sinput, CIDX_BESTBIDLOT);
	}
	
	public Double getBestOffer(){
		return (Double) getData(CIDX_BESTOFFER);
	}
	public void setBestOffer(Double sinput){
		setData(sinput, CIDX_BESTOFFER);
	}
	
	public Double getBestOfferLot(){
		return (Double) getData(CIDX_BESTOFFERLOT);
	}
	public void setBestOfferLot(Double sinput){
		setData(sinput, CIDX_BESTOFFERLOT);
	}

	public Double getPercent(){
		return (Double) getData(CIDX_PERCENT);
	}
	public void setPercent(Double sinput){
		setData(sinput, CIDX_PERCENT);
	}

	public void calculate(){
		double npercent = 0;
		double nlast = getLast().doubleValue();
		double nprev = getPrevious().doubleValue();
		double nchange = nprev == 0 ? 0 : nlast - nprev;
		if (nlast != 0){
			if (nprev != 0){
				npercent = (nchange * 100 ) / nprev;
				BigDecimal bd = new BigDecimal(npercent);
				bd = bd.setScale(2, BigDecimal.ROUND_DOWN);
				npercent = bd.doubleValue();
			} 
		} 
		setPercent(new Double(npercent));
		setChange(new Double(nchange));
	}


	public void fromVector(Vector vmsg){
		super.fromVector(vmsg);
		calculate();
	}
}
