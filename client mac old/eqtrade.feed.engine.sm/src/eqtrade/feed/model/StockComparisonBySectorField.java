package eqtrade.feed.model;



import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class StockComparisonBySectorField extends Column implements MutableIDisplayIData{
	public StockComparisonBySectorField (Row source, int idx) throws Exception{
		super(source, idx);
	}

	@Override
	public ImmutableIDisplay getImmutableIDisplay() {
		// TODO Auto-generated method stub
		return StockComparisonBySectorDef.render;
	}

	@Override
	public MutableIDisplay getMutableIDisplay() {
		// TODO Auto-generated method stub
		return StockComparisonBySectorDef.render;
	}

}
