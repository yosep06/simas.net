package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;
import eqtrade.feed.core.Utils;

public class BrokerRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     private String strFieldName = new String("");
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
     private static Color newBack;
     private static Color newFore;
     private static SelectedBorder border = new SelectedBorder();
     private static Vector vGov = new Vector(10,5);
     private static Vector vForeign = new Vector(20,5);
     private static Vector vLocal = new Vector(50,5);
     
     public static void setBroker(String source){
    	 try {
		    	 String temp[] = source.split("\\|");
		    	 vGov = Utils.parser(temp[0], ",");
		    	 vForeign = Utils.parser(temp[1], ",");
		    	 vLocal =Utils.parser(temp[2], ",");
    	 } catch (Exception ex){}
     }
     
     public static Color getColor(String code){
    	 if (vGov.contains(code)){
    		 return FeedSetting.getColor(FeedSetting.C_BGOV);
    	 } else if (vForeign.contains(code)){
    		 return FeedSetting.getColor(FeedSetting.C_BFOREIGN);
    	 } else {
    		 return FeedSetting.getColor(FeedSetting.C_BLOCAL);
    	 }
     }

	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){

	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        strFieldName = table.getColumnName(column);
        if (value instanceof MutableIData) {
	        Broker rt = (Broker)((MutableIData)value).getSource();

    		//newBack =  (((row+1) % 4) == 0 || ((row+2) % 4 == 0)) ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
    		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
            if (strFieldName.equals(BrokerDef.dataHeader[Broker.CIDX_CODE]) ||
            	 strFieldName.equals(BrokerDef.dataHeader[Broker.CIDX_NAME])){
            	newFore = getColor(rt.getCode());
            } else {
            	newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
            }
        }
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);   
	 	return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
	             if (dat instanceof Double) {
	                 setText(formatter.format(dat));
	             } else if (dat == null){
	                 setText("");
                 } else if (strFieldName.equals(BrokerDef.dataHeader[Broker.CIDX_STATUS])){
                    setText(dat.equals("0") ? " Active" : " Suspended");
	             } else {
	            	 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
