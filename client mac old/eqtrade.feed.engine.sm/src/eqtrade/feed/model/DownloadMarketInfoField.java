package eqtrade.feed.model;

import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class DownloadMarketInfoField extends Column implements MutableIDisplayIData { 
	
	public DownloadMarketInfoField(Row source, int idx) throws Exception {
		super(source, idx);		
	}

	@Override
	public ImmutableIDisplay getImmutableIDisplay() {
		return DownloadMarketInfoDef.render;
	}

	@Override
	public MutableIDisplay getMutableIDisplay() {
		 return DownloadMarketInfoDef.render;
	}

}
