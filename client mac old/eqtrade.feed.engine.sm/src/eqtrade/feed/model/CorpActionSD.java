package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public class CorpActionSD extends Model {
	private static final long serialVersionUID = 1L;
	public static final int	CIDX_ACTIONTYPE = 3;
	public static final int	CIDX_STOCK = 4;
	public static final int	CIDX_AMOUNT = 5;
	public static final int CIDX_RATIO1 = 6;
	public static final int CIDX_RATIO2 = 7;
	public static final int CIDX_CUMDATE = 8;
	public static final int CIDX_EXPDATE = 9;
	public static final int CIDX_RECORDDATE = 10;
	public static final int CIDX_DISTDATE = 11;
	public static final int CIDX_TRANSACTIONDATE = 12;
	public static final int CIDX_LASTTRANSACTIONDATE = 13;
	public static final int CIDN_NUMBEROFFIELDS = 14;
	
	public CorpActionSD() {
		super(CIDN_NUMBEROFFIELDS);		
	}
	
	public CorpActionSD(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 5:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}		
	}
	
	public String getRatio1(){
		return (String) getData(CIDX_RATIO1);
	}
	public void setRatio1(String sinput){
		setData(sinput, CIDX_RATIO1);
	}
	
	public String getRatio2(){
		return (String) getData(CIDX_RATIO2);
	}
	public void setRatio2(String sinput){
		setData(sinput, CIDX_RATIO2);
	}

	public String getCumdate(){
		return (String) getData(CIDX_CUMDATE);
	}
	public void setCumdate(String sinput){
		setData(sinput, CIDX_CUMDATE);
	}

	public String getExpdate(){
		return (String) getData(CIDX_EXPDATE);
	}
	public void setExpdate(String sinput){
		setData(sinput, CIDX_EXPDATE);
	}
	
	public String getRecorddate(){
		return (String) getData(CIDX_RECORDDATE);
	}
	public void setRecorddate(String sinput){
		setData(sinput, CIDX_RECORDDATE);
	}

	public String getDistdate(){
		return (String) getData(CIDX_DISTDATE);
	}
	public void setDistdate(String sinput){
		setData(sinput, CIDX_DISTDATE);
	}
	public String getTransactiondate(){
		return (String) getData(CIDX_TRANSACTIONDATE);
	}
	public void setTransactiondate(String sinput){
		setData(sinput, CIDX_TRANSACTIONDATE);
	}
	public String getLasttransactiondate(){
		return (String) getData(CIDX_LASTTRANSACTIONDATE);
	}
	public void setLasttransactiondate(String sinput){
		setData(sinput, CIDX_LASTTRANSACTIONDATE);
	}
	public String getActiontype(){
		return (String) getData(CIDX_ACTIONTYPE);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_ACTIONTYPE);
	}

	public String getStock(){
		return (String) getData(CIDX_STOCK);
	}
	public void setStock(String sinput){
		setData(sinput, CIDX_STOCK);
	}
	
	public Double getAmount(){
		return (Double) getData(CIDX_AMOUNT);
	}
	public void setAmount(Double sinput){
		setData(sinput, CIDX_AMOUNT);
	}
}
