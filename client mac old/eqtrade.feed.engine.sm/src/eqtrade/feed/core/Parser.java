package eqtrade.feed.core;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vollux.idata.Column;
import com.vollux.idata.Row;

import eqtrade.feed.model.Model;
import eqtrade.feed.model.Subscriber;


public abstract class Parser extends Thread {
    protected final Log log = LogFactory.getLog(getClass());
    protected volatile boolean isrun = true;
    protected volatile Vector vmsg = new Vector(100, 10);
    public Hashtable hashSubscriber = new Hashtable(10,2);
    public IFeedEngine engine;
    protected volatile Vector vlistener = new Vector(10,1);
	public String oldstock,newstock;
	public Parser(IFeedEngine engine){
		this.engine = engine;
    }
	
	public void addListener(IMessage listener){
		if (!vlistener.contains(listener)) 
			vlistener.addElement(listener);
	}
	
	public void removeListener(IMessage listener){
		vlistener.remove(listener);
	}
	
	protected void refreshListener(Model model){
		try {
			for (int i =0; i<vlistener.size();i++){
				IMessage listener = (IMessage)vlistener.elementAt(i);
				listener.newMessage(model);
			}
		} catch (Exception ex){
			return;
		}
	}
	
	public void disconnected(){
		
	}
    
    public Subscriber genRequest(String msg){
        int seq = loadModel(msg);
        return new Subscriber(msg, seq); 
    }
    
    public void subscribe(final String msg){
	        Subscriber r = (Subscriber)hashSubscriber.get(msg);
        	//log.info("subscribe to : "+msg+" "+r);

	        if (r == null){
	            r = genRequest(msg);
	            r.addSubscriber();
	            hashSubscriber.put(msg, r);
	            sendRequest(r);
	        } else {
	            r.addSubscriber();
	            if (r.getSubscriber() == 1) {
	            	if (removeAfterUnsubscribe()) r.setSeqno(loadModel(msg));
	            	sendRequest(r);
	            }
	        }    			
    }
    
    protected void sendRequest(Subscriber r){
        if (r.hasSubscriber()){
            //log.info("subscribe to server: "+r.getKey()+" start with no: "+r.getSeqno());
            engine.getConnection().subscribe(r.getRequest());
        }
    }
    
    public void unsubscribe(String msg){
	        final Subscriber r = (Subscriber)hashSubscriber.get(msg);
            //log.info("unsubscribe : "+msg);
	        if (r != null && r.getSubscriber()>0){
	            if (r.removeSubsriber() == 0) {
		        	synchronized(vmsg){
		                log.info("unsubscribe from server: "+msg);
		                engine.getConnection().unsubscribe(msg);
		                if (removeAfterUnsubscribe()) {
		                	saveModel(r);
			                removeModel(r);
		                }
		        	}
		        }
	        }
    }
    
    protected boolean removeAfterUnsubscribe(){
    	return false;
    }
    
    public void removeModel(Subscriber r){
        //should be implemented by subclass
        //if after unregister the data should be remove from memory
        //usually used by engine with very large data , ex: TradeQuote, OrderDetail and StockDepth
    }
    
    public void resubscribe(){
        //log.info("resubscribe all request");
        for(Iterator i = hashSubscriber.keySet().iterator();i.hasNext();){
            String key = (String)i.next();
            Subscriber r = (Subscriber)hashSubscriber.get(key);
            sendRequest(r);
        }
    }

    public void onMessage(String mdt){
		vmsg.addElement(mdt);
		synchronized(vmsg){
			vmsg.notify();
		}
	}


	public void setStop(){
        for(Iterator i = hashSubscriber.keySet().iterator();i.hasNext();){
            String key = (String)i.next();
            Subscriber r = (Subscriber)hashSubscriber.get(key);
            if (removeAfterUnsubscribe()) {
            	if (r.getSubscriber() != 0) {
            		saveModel(r);
            	}
            } else {
            	saveModel(r);            	
            }
        }
		isrun = false;
		synchronized(vmsg){
			vmsg.notify();
		}
	}
    
    public void setStart(){
        if (!isAlive()) start();
    }


	public void run(){
		Vector	vtemp;
		while(isrun){
			try {
				if (vmsg.size() > 0){
					vtemp = (Vector)vmsg.clone();
					vmsg.removeAll(vtemp);
					doProcess(vtemp);
					sleep(1);
				} else {
					synchronized(vmsg){
						vmsg.wait();
					}
				}
			}
			catch (Exception ex){
				//System.out.println(ex.getMessage());
				//ex.printStackTrace();
				return;
			}
		}
		try {
			join();
		} catch (Exception ex){
			return;
		}
		System.gc();
	}
    
    protected Integer getModelID(){
        return null;
    }
    
    protected Vector getDataVector(Subscriber r){
        return engine.getDataStore().get(getModelID()).getDataVector();
    }
    
    protected String genFilename(String msg) {
    	return "data/session/"+FeedSetting.session+"-"+msg+".cache";
    }
    
    protected void saveModel(Subscriber r) {
        String filename = genFilename(r.getKey()); 
        Vector vdata = new Vector(50,10);
        if (getModelID() != null) {
            Vector vtemp = getDataVector(r);
            for (int i=0; i<vtemp.size();i++){
                vdata.addElement(((Column)((Vector)vtemp.elementAt(i)).elementAt(0)).getSource());
            }
            HashMap map = new HashMap(2);
            map.put("seq", new Integer(r.getSeqno()));
            map.put("msg", r.getKey());
            map.put("data", vdata);
            Utils.writeFile(filename, Utils.objectToCompressedByte(map));            
            
           // System.out.println("SUBSCRIBER R PARDER : "+r);
            //System.out.println("VTEMP PARSER : "+vtemp.toString());
        }
        //System.out.println("MODEL ID : "+getModelID().toString());
        //System.out.println("FILE NAME : "+filename);
        //System.out.println("VDATA PARSER : "+vdata.toString());
    }
    
    protected List createTableRow(Row dat) {
    	return null;
    }
    
    protected int loadModel(String msg){
        try {
            String  filename = genFilename(msg); 
            Object o = Utils.readFile(filename);
            if (o != null){
            	byte[] byt = (byte[])o;            	
                HashMap map =  (HashMap)Utils.compressedByteToObject(byt); // (HashMap)o;
                Vector vdata = (Vector)map.get("data");
                Vector vrow = new Vector(100,5);
                for (int i=0; i<vdata.size();i++){
                    vrow.addElement(createTableRow((Row)vdata.elementAt(i)));
                }
                if (getModelID() != null && vrow.size()>0) {
                	engine.getDataStore().get(getModelID()).addRow(vrow, false, false);
                	engine.getDataStore().get(getModelID()).refresh();
                }
                int seq = ((Integer)map.get("seq")).intValue();
                //System.out.println("VROW LOADMODEL : "+ vrow.toString());
                return  seq;
                
            } else {
                return 0;            
            }
           
        } catch (Exception ex){
            //log.error(Utils.logException(ex));
            return 0;
        }
    }
    
    public void doProcess(final Vector vdata){
        SwingUtilities.invokeLater(new Runnable(){
            public void run() {            		
                process(vdata);

            }
        });
    }
    
    
	public abstract void process(Vector vdata);
}
