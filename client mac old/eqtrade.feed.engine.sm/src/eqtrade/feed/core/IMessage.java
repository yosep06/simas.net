package eqtrade.feed.core;

import eqtrade.feed.model.Model;

public interface IMessage {
	public void newMessage(Model model);
}
