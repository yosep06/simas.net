package eqtrade.feed.core;

import java.util.HashMap;

public abstract class Event {
    public abstract void execute(HashMap param);
}
     