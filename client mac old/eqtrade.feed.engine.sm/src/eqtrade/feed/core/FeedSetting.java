package eqtrade.feed.core;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;

import com.eqtrade.FileConfig;

public final class FeedSetting {
    private static Hashtable hlayout;
    private static Hashtable hcolor;
    public static String C_CURRENT_LAYOUT ;
    public static Hashtable hconfig;
    public static String session;
    private static String def = "current.dat";
    private static Properties proplot;
	public static String C_LOTSIZE = "lotsize";
	private static String getLot;
	private static Double Lot;
	protected static FileConfig config;
	
    static {
        session =Utils.formatDate.format(new Date());
        hlayout = new Hashtable();
        hconfig = new Hashtable();
        defaultColor();
        C_CURRENT_LAYOUT = loadCurrent();
        
        /*try {
			File file = new File("data/config/config-lot.properties");
			if(!file.exists()){
				file.createNewFile();
				
				proplot = new Properties();
				proplot.load(new FileInputStream(file));
				if(proplot.isEmpty()){
					proplot.put(C_LOTSIZE, "100");
					
					proplot.store(new FileOutputStream(file), "");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
    }
    
    public static final String C_BACKGROUND = "color.background";
    public static final String C_BACKGROUNDEVEN = "color.background.even";
    public static final String C_FOREGROUND = "color.foreground";
    public static final String C_PLUS = "color.plus";
    public static final String C_MINUS = "color.minus";
    public static final String C_ZERO = "color.zero";
    public static final String C_TS = "color.ts";
    public static final String C_BROKER1 = "name.broker1";
    public static final String C_BROKERCOLOR1 = "color.broker1";
    public static final String C_BROKER2 = "name.broker2";
    public static final String C_BROKERCOLOR2 = "color.broker2";
    public static final String C_BROKER3 = "name.broker3";
    public static final String C_BROKERCOLOR3 = "color.broker3";
    public static final String C_FOREIGN = "color.foreign";
    public static final String C_DOMESTIC = "color.domestic";
    public static final String C_FONT = "default.font";
    
    public static final String C_AGRICULTURE = "color.agri";
    public static final String C_MINING = "color.mining";
    public static final String C_BASICINDUSTRY = "color.basic.industry";
    public static final String C_MISCELLANEOUS = "color.miscelaneous";
    public static final String C_CONSUMER = "color.consumer";
    public static final String C_PROPERTY = "color.property";
    public static final String C_INFRASTRUCTURE = "color.infrastructure";
    public static final String C_FINANCE = "color.finance";
    public static final String C_TRADE = "color.trade";
    
    public static final String C_BGOV = "color.bgov";
    public static final String C_BFOREIGN = "color.bforeign";
    public static final String C_BLOCAL = "color.blocal";
    
    public static void defaultColor(){
    	hcolor = new Hashtable();
    	hcolor.put(C_AGRICULTURE, Color.green.darker());
    	hcolor.put(C_MINING, Color.green.brighter());
    	hcolor.put(C_BASICINDUSTRY, Color.red.darker());
    	hcolor.put(C_MISCELLANEOUS, Color.red.brighter());
    	hcolor.put(C_CONSUMER, new Color(230,120,10));
    	hcolor.put(C_PROPERTY, Color.white);
    	hcolor.put(C_INFRASTRUCTURE, Color.blue);
    	hcolor.put(C_FINANCE, Color.cyan);
    	hcolor.put(C_TRADE, Color.yellow);
    	hcolor.put(C_BACKGROUND, new Color(11,16,16));
    	hcolor.put(C_BACKGROUNDEVEN, new Color(17,25,25));
    	hcolor.put(C_FOREGROUND, Color.white.darker());
    	hcolor.put(C_PLUS, Color.green);
    	hcolor.put(C_MINUS, Color.red);
    	hcolor.put(C_ZERO, Color.yellow);
    	hcolor.put(C_TS, Color.yellow);
    	hcolor.put(C_BROKER1, "");
    	hcolor.put(C_BROKERCOLOR1, Color.darkGray);
    	hcolor.put(C_BROKER2, "");
    	hcolor.put(C_BROKERCOLOR2, Color.darkGray);
    	hcolor.put(C_BROKER3, "");
    	hcolor.put(C_BROKERCOLOR3, Color.darkGray);
//    	hcolor.put(C_FOREIGN,new Color(255,0,204) );
    	hcolor.put(C_FOREIGN,Color.yellow );
    	hcolor.put(C_DOMESTIC, new Color(0,204,255));
    	hcolor.put(C_FONT, new Font("dialog", Font.BOLD, 11));
    	hcolor.put(C_BGOV, Color.green);
    	hcolor.put(C_BFOREIGN, Color.red);
    	hcolor.put(C_BLOCAL, Color.magenta.darker());
    }
    
    
    public static String getLot() {
    	try {
    		config = new FileConfig("data/config/config-lot.properties");
    		getLot = config.getProperty("lotsize");
    		Lot = Double.parseDouble(getLot);
    		//System.out.println("lot :"+getLot);
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    	return getLot;
    }
    
    /*public static String getLot(String key){
    	return (String)proplot.get(key);
    }*/
    
    private static String loadCurrent() {
        Object obj = Utils.readFile("data/layout/"+def);
        return (obj!=null) ? obj.toString() : "_default";
    }
    
    public static void saveLayout(String name){
    	C_CURRENT_LAYOUT = name;
    	save();
    }
    
    public static  void save(){         
    	hconfig.put("COLOR", hcolor);
    	hconfig.put("LAYOUT", hlayout);
        Utils.writeFile("data/layout/"+C_CURRENT_LAYOUT+".scr",hconfig);               
        Utils.writeFile("data/layout/"+def, C_CURRENT_LAYOUT);
    }
    
    public static void loadLayout(String name){
    	C_CURRENT_LAYOUT = name;
    	load();
    }
    
    public static void load(){
    	Object obj = Utils.readFile("data/layout/"+C_CURRENT_LAYOUT+".scr");
    	if (obj == null){
    		C_CURRENT_LAYOUT = "_default";
    		obj = Utils.readFile("data/layout/_default.scr");
    	}
    	
    	if (obj!=null){
    		hconfig = (Hashtable)obj;
    		hcolor = (Hashtable)hconfig.get("COLOR");
    		if (hcolor == null) defaultColor();
    		hlayout = (Hashtable)hconfig.get("LAYOUT");
    		if (hlayout == null) hlayout =new Hashtable();
    	}
    }
    
    public static void putLayout(String key, Object object){
        hlayout.put(key, object);
    }
    
    public static void removeLayout(String key){
    	hlayout.remove(key);
    }
    
    public static Object getLayout(String key){
        return hlayout.get(key);
    }
    
    public static void putColor(String key, Color object){
        hcolor.put(key, object);
    }
    
    public static Object  getSetting(String key){
        return hcolor.get(key);
    }
    
    public static void putSetting(String key, Object object){
        hcolor.put(key, object);
    }
    
    public static Color  getColor(String key){
        return (Color)hcolor.get(key);
    }

    public static void putFont(Font f){
    	hcolor.put(C_FONT, f);    	
    }
    
    public static Font getFont(){
    	return (Font)hcolor.get(C_FONT);
    }

    public static Hashtable getLayout(){
        return hlayout;
    }
    
    public static Hashtable getColor(){
    	return hcolor;
    }
}
