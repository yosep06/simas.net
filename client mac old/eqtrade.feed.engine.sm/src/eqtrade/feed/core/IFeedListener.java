package eqtrade.feed.core;

public interface IFeedListener {
	void loginOK();
	void loginFailed(String msg);
	void loginStatus(String msg);
	void changePwdOK();
	void changePwdFailed(String msg);
	void disconnected();
	void reconnectStatus(String msg);
	void reconnectFailed(String msg);
	void reconnectOK();
	void killed();
	/*//yosep enforcementpass
	void forgotPwd();
	void changePwd(String isExpired);*/
	//yosep other device
	void falseloginStatus(String msg);
	void finisTimeTrade(boolean st );// yosep fixingTimetrade
}
