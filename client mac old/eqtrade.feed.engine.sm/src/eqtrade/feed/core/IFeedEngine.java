package eqtrade.feed.core;

import java.util.Vector;

import eqtrade.feed.engine.FeedConnection;
import eqtrade.feed.engine.FeedEventDispatcher;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;

public  interface IFeedEngine {
	IFeedConnection getConnection();	
	FeedStore getDataStore();	
	FeedEventDispatcher getEventDispatcher();
	FeedParser getParser();
	Vector getEventListener();
	public IFeedConnectionQueue getConnectionQueue();
}
