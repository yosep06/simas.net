package eqtrade.feed.core;

import com.vollux.idata.GridModel;

public interface IFeedApp {
	public void start();
	public void addListener(IFeedListener listener);
	public void removeListener(IFeedListener listener);	
	public void login(String userid, String passwd);	
	public void changePasswd(String oldPasswd, String newPasswd);
	public void reconnect(int counter);
	public void subscribe(String code, String message);
	public void unsubscribe(String code, String message);
	public GridModel getStore(Integer id);
	public void logout();
	public void stop();
	public String getUserId();
	public String getPassword();
	public IFeedEngine getEngine();
	// yosep other device #start
	public void login(String userid, String passwd,int count);	
	public void logout(String userid, String passwd,int count);
	// yosep other device #end
}
