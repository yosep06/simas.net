package eqtrade.feed.core;

public interface IFeedConnectionQueue {
	public void subscribe(String sheader);

	public void unsubscribe(String sheader);

	public void start() throws Exception;
	
	public void stop() throws Exception;

	public void reconnect(final int count);

}
