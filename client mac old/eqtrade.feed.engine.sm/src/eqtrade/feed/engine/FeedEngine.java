package eqtrade.feed.engine;

import java.util.HashMap;
import java.util.Vector;

import com.vollux.idata.GridModel;

import eqtrade.feed.core.IFeedApp;
import eqtrade.feed.core.IFeedConnection;
import eqtrade.feed.core.IFeedConnectionQueue;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.IFeedListener;
import eqtrade.feed.engine.socket.FeedConnectionQueue;
import eqtrade.feed.engine.socket.FeedConnectionSocket;

public class FeedEngine implements IFeedEngine, IFeedApp{
	private IFeedConnection comm = null;
	private IFeedConnectionQueue connQueue = null;
	private FeedStore store = null;
	private FeedEventDispatcher event = null;
	private FeedParser parser = null;
	private Vector vlistener = null;
	
	public FeedEngine(){		
	}
	
	public void start(){
		vlistener = new Vector(5,1);
		store = new FeedStore();
		//comm = new FeedConnection(this);
		comm = new FeedConnectionSocket(this);
		connQueue = new FeedConnectionQueue(this);
		event = new FeedEventDispatcher(this);
//		parser = new FeedParser(this);
		parser = new FeedParserMultithread(this);
	}
	
	public IFeedEngine getEngine(){
		return (IFeedEngine)this;
	}
	
	public void addListener(IFeedListener listener){
		if (!vlistener.contains(listener)){
			vlistener.add(listener);
		}
	}
	public IFeedConnectionQueue getConnectionQueue() {
		return connQueue;
	}
	
	public void removeListener(IFeedListener listener){
		vlistener.remove(listener);
	}
	
	public Vector getEventListener(){
		return vlistener;
	}
	
	public IFeedConnection getConnection(){
		return comm;
	}
	
	public FeedStore getDataStore(){
		return store;
	}
	
	public FeedEventDispatcher getEventDispatcher(){
		return event;
	}
	
	public FeedParser getParser(){
		return parser;
	}
	//---------------------------------------------------------------------------------------------------------
	public void login(String userid, String passwd){
        HashMap param = new HashMap(1);
        param.put(FeedEventDispatcher.PARAM_USERID, userid);
        param.put(FeedEventDispatcher.PARAM_PASSWORD, passwd);
        event.pushEvent(FeedEventDispatcher.EVENT_LOGIN, param);		
	}
	
	public void changePasswd(String oldPasswd, String newPasswd){
        HashMap param = new HashMap(1);
        param.put(FeedEventDispatcher.PARAM_OLDPASSWORD, oldPasswd);
        param.put(FeedEventDispatcher.PARAM_PASSWORD, newPasswd);
        event.pushEvent(FeedEventDispatcher.EVENT_CHGPASSWD, param);				
	}
	
	public void reconnect(int counter){
        HashMap param = new HashMap(1);
        param.put(FeedEventDispatcher.PARAM_COUNTER, new Integer(counter));
        event.pushEvent(FeedEventDispatcher.EVENT_RECONNECT, param);				
	}
	
	public void subscribe(String code, String message){
        HashMap param = new HashMap(1);
        param.put(FeedEventDispatcher.PARAM_CODE, code);
        param.put(FeedEventDispatcher.PARAM_MESSAGE, message);
        event.pushEvent(FeedEventDispatcher.EVENT_SUBSCRIBE, param);						
	}
	
	public void unsubscribe(String code, String message){
        HashMap param = new HashMap(1);
        param.put(FeedEventDispatcher.PARAM_CODE, code);
        param.put(FeedEventDispatcher.PARAM_MESSAGE, message);
        event.pushEvent(FeedEventDispatcher.EVENT_UNSUBSCRIBE, param);								
	}
	
	public void logout(){
        event.pushEvent(FeedEventDispatcher.EVENT_LOGOUT, null);				
	}
	
	public void stop(){
		parser.stop();
	}
	public GridModel getStore(Integer id){
		return store.get(id);
	}

	public String getPassword() {
		return comm.getPassword();
	}

	public String getUserId() {
		return comm.getUserId();
	}

	// yosep other device #start
	public boolean getIsLogin(){
		return comm.getFeedLogin();
	}
	
	public int getcountLogin(){
		return comm.getCountFeedLogin();
	}
	
	@Override
	public void login(String userid, String passwd, int count) {
        HashMap param = new HashMap(1);
        param.put(FeedEventDispatcher.PARAM_USERID, userid);
        param.put(FeedEventDispatcher.PARAM_PASSWORD, passwd);
        param.put(FeedEventDispatcher.PARAM_COUNTER, count);
        event.pushEvent(FeedEventDispatcher.EVENT_LOGIN, param);	
		
	}

	@Override
	public void logout(String userid, String passwd,int count) {
		HashMap param = new HashMap(1);
		param.put(FeedEventDispatcher.PARAM_COUNTER, count);
		param.put(FeedEventDispatcher.PARAM_USERID, userid);
		param.put(FeedEventDispatcher.PARAM_PASSWORD, passwd);
        event.pushEvent(FeedEventDispatcher.EVENT_LOGOUT, param);			
	}
	// yosep other device #end
}
