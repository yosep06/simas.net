package eqtrade.feed.engine;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import com.vollux.idata.GridModel;

import eqtrade.feed.core.GridModelext;
import eqtrade.feed.model.Article;
import eqtrade.feed.model.ArticleDef;
import eqtrade.feed.model.Board;
import eqtrade.feed.model.BoardDef;
import eqtrade.feed.model.Broker;
import eqtrade.feed.model.BrokerDef;
import eqtrade.feed.model.Chart;
import eqtrade.feed.model.ChartDef;
import eqtrade.feed.model.Commodity;
import eqtrade.feed.model.CommodityDef;
import eqtrade.feed.model.CorpAction;
import eqtrade.feed.model.CorpActionDef;
import eqtrade.feed.model.CorpActionSD;
import eqtrade.feed.model.CorpActionSDDef;
import eqtrade.feed.model.Currency;
import eqtrade.feed.model.CurrencyDef;
import eqtrade.feed.model.Future;
import eqtrade.feed.model.FutureDef;
import eqtrade.feed.model.GlobalIndices;
import eqtrade.feed.model.GlobalIndicesDef;
import eqtrade.feed.model.IPO;
import eqtrade.feed.model.IPODef;
import eqtrade.feed.model.Indices;
import eqtrade.feed.model.IndicesDef;
import eqtrade.feed.model.Market;
import eqtrade.feed.model.MarketDef;
import eqtrade.feed.model.News;
import eqtrade.feed.model.NewsDef;
import eqtrade.feed.model.Queue;
import eqtrade.feed.model.QueueDef;
import eqtrade.feed.model.Quote;
import eqtrade.feed.model.QuoteDef;
import eqtrade.feed.model.RunningTrade;
import eqtrade.feed.model.RunningTradeDef;
import eqtrade.feed.model.Rups;
import eqtrade.feed.model.RupsDef;
import eqtrade.feed.model.Sector;
import eqtrade.feed.model.SectorDef;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockComparisonBySector;
import eqtrade.feed.model.StockComparisonBySectorDef;
import eqtrade.feed.model.StockDef;
import eqtrade.feed.model.StockNews;
import eqtrade.feed.model.StockNewsDef;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;
import eqtrade.feed.model.Summary;
import eqtrade.feed.model.SummaryDef;
import eqtrade.feed.model.TradePrice;
import eqtrade.feed.model.TradePriceDef;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;

public class FeedStore {
	public final static Integer DATA_BOARD = new Integer(0);
	public final static Integer DATA_ALERT = new Integer(1);
	public final static Integer DATA_CHART = new Integer(2);
	public final static Integer DATA_QUOTE = new Integer(3);			
	public final static Integer DATA_TRADE = new Integer(4);
	public final static Integer DATA_RUNNINGTRADE = new Integer(5);
	public final static Integer DATA_STOCK = new Integer(6);
	public final static Integer DATA_BROKER = new Integer(7);
	public final static Integer DATA_STOCKSUMMARY = new Integer(8);
	public final static Integer DATA_MARKET = new Integer(9);
	public final static Integer DATA_MARKETSUMM = new Integer(10);
	public final static Integer DATA_INDICES = new Integer(11);
	public final static Integer DATA_NEWS = new Integer(12);
	public final static Integer DATA_CORPACTION = new Integer(13);
	public final static Integer DATA_TRADEPRICE = new Integer(14);
	public final static Integer DATA_TRADEHISTORY = new Integer(15);	
	public final static Integer DATA_TRADESUMMARY = new Integer(16);
	public final static Integer DATA_TRADESUMBROKER = new Integer(17);
	public final static Integer DATA_TRADESUMBS = new Integer(18);
	public final static Integer DATA_TRADESUMSTOCKINV = new Integer(19);
	public final static Integer DATA_TRADESUMINV = new Integer(20);
	public final static Integer DATA_INVESTOR = new Integer(21);
	public final static Integer DATA_GLOBALNEWS = new Integer(22);
	public final static Integer DATA_CURRENCY = new Integer(23);
	public final static Integer DATA_GLOBALINDICES = new Integer(24);
	public final static Integer DATA_TRADESUMSB = new Integer(25);
	public final static Integer DATA_RUPS = new Integer(26);
	public final static Integer DATA_IPO = new Integer(27);
	//public final static Integer DATA_CORPACTION2 = new Integer(2);//update-280814
	public final static Integer DATA_COMMODITY = new Integer(28);
	public final static Integer DATA_FUTURE = new Integer(29);
	public final static Integer DATA_QUEUE = new Integer(30);
	public final static Integer DATA_STOCKNEWS = new Integer(31);
	public final static Integer DATA_STOCKCOMPARISONBYSECTOR = new Integer(32);
	public final static Integer DATA_SECTOR = new Integer(33);
	//public final static Integer DATA_SECTORNAME = new Integer(38);//update-280814
	public final static Integer DATA_QUOTE_QUEUE = new Integer(34);
	public final static Integer DATA_QUEUE_TYPE = new Integer(35);
	public final static Integer DATA_STOCKSUMMARY_HISTORY = new Integer(36); //tambahan table untuk histori
	public final static Integer DATA_TRADESUMSB_SELL = new Integer(37);
	//public final static Integer DATA_CORPACTIONSD = new Integer(13); // untuk CORPACTION di STOCKDETAIL By Iben 04092013
	//public final static Integer DATA_CORPACTIONSD2 = new Integer(2);
	public final static Integer DATA_SECTORNAME = new Integer(38);//update-280814
	public final static Integer DATA_CORPACTION2 = new Integer(39);//update-280814
	public final static Integer DATA_CORPACTIONSD = new Integer(40); // untuk CORPACTION di STOCKDETAIL By Iben 04092013
	public final static Integer DATA_CORPACTIONSD2 = new Integer(41);//update-280814
	public final static Integer DATA_TOPGAINER = new Integer(42); // yosep topgainer
	public final static Integer DATA_PERIODE = new Integer(43);// yosep topgainer
	public final static Integer DATA_TRADESUMMARKETINV = new Integer(44);//yosep invbymarket
	public final static Integer DATA_TRADEHISTORYM = new Integer(45);	
	
    private GridModel boardModel = new GridModel(BoardDef.getHeader(), false);
    private GridModel alertModel = new GridModel(BoardDef.getHeader(), false);
    private GridModel chartModel = new GridModel(ChartDef.getHeader(), false);
    private GridModel quoteModel = new GridModel(QuoteDef.getHeader(), false);
    private GridModel runningTradeModel = new GridModel(RunningTradeDef.getHeader(), false);
    private GridModel stockModel = new GridModel(StockDef.getHeader(), false);
    private GridModel brokerModel = new GridModel(BrokerDef.getHeader(), false);
    private GridModel stockSummModel = new GridModel(StockSummaryDef.getHeader(), false);
    private GridModel stockSummModelHistori = new GridModel(StockSummaryDef.getHeader(), false);
    private GridModel marketModel = new GridModel(MarketDef.getHeader(), false);
    private GridModel marketSummModel = new GridModel(SummaryDef.getHeader(), false);
    private GridModel indicesModel = new GridModel(IndicesDef.getHeader(), false);
    private GridModel newsModel = new GridModel(NewsDef.getHeader(), false);
    private GridModel corpActionModel = new GridModel(CorpActionDef.getHeader(), false);
    private GridModel corpActionModel2 = new GridModel(CorpActionDef.getHeader(), false);
    private GridModel tradePriceModel = new GridModel(TradePriceDef.getHeader(), false);
    private GridModelext tradeHistoryModel = new GridModelext(RunningTradeDef.getHeader(), false);//new GridModel(RunningTradeDef.getHeader(), false);
    private GridModelext tradeHistoryMModel = new GridModelext(RunningTradeDef.getHeader(), false);//new GridModel(RunningTradeDef.getHeader(), false);
    private GridModel tradeSummModel = new GridModel(TradeSummaryDef.getHeader(), false);
    private GridModel tradeSummBrokerModel = new GridModel(TradeSummaryDef.getHeader(), false);
    private GridModel tradeSummBSModel = new GridModel(TradeSummaryDef.getHeader(), false);
    private GridModel tradeSummSBModel = new GridModel(TradeSummaryDef.getHeader(), false);//tradeSummSBModelsell
    private GridModel tradeSummSBModelsell = new GridModel(TradeSummaryDef.getHeader(), false);//tradeSummSBModelsell
    private GridModel tradeSummStockInvModel = new GridModel(TradeSummaryDef.getHeader(), false);
    private GridModel tradeSummInvModel = new GridModel(TradeSummaryDef.getHeader(), false);
    private GridModel investorModel = new GridModel(BoardDef.getHeader(), false);
    private GridModel globalNewsModel = new GridModel(ArticleDef.getHeader(), false);
    private GridModel currencyModel = new GridModel(CurrencyDef.getHeader(), false);
    private GridModel globalIndicesModel = new GridModel(GlobalIndicesDef.getHeader(), false);
    private GridModel rupsModel = new GridModel(RupsDef.getHeader(), false);
    private GridModel ipoModel = new GridModel(IPODef.getHeader(), false);
    private GridModel commodityModel = new GridModel(CommodityDef.getHeader(), false);
    private GridModel futureModel = new GridModel(FutureDef.getHeader(), false);
    //private GridModel queueModel = new GridModel(QueueDef.getHeader(), false);
    private GridModel stockNewsModel = new GridModel(StockNewsDef.getHeader(), false);
    private GridModel stockComparisonBySectorModel = new GridModel(StockComparisonBySectorDef.getHeader(),false);
    private GridModel sectorModel = new GridModel(SectorDef.getHeader(), false);
    private GridModel sectorNameModel = new GridModel(SectorDef.getHeader(), false);
	private GridModel queueModel = new GridModel(QueueDef.getHeader(), false);//add order tracking
	private GridModel queueType = new GridModel(BoardDef.getHeader(), false);//add order tracking
	private GridModel corpActionSDModel = new GridModel(CorpActionSDDef.getHeader(), false); //untuk CORPACTION di STOCKDETAIL By Iben 04092013
	private GridModel corpActionSDModel2 = new GridModel(CorpActionSDDef.getHeader(), false);
	private GridModel topGainer = new GridModel(StockSummaryDef.getHeader(), false);// yosep topgainer
	private GridModel periode = new GridModel(BoardDef.getHeader(), false);// yosep topgainer
	private GridModel invbymarket = new GridModel(TradeSummaryDef.getHeader(), false);// yosep invbymarket
	
    private HashMap map = new HashMap(25,5);
	
	public FeedStore(){
		init();
	}
	
	private void init(){
        boardModel.setKey(new int[]{Board.CIDX_CODE});
        investorModel.setKey(new int[]{Board.CIDX_CODE});
        alertModel.setKey(new int[]{Board.CIDX_CODE});
        chartModel.setKey(new int[]{Chart.CIDX_STOCK, Chart.CIDX_DATE, Chart.CIDX_TIME});
        quoteModel.setKey(new int[]{Quote.CIDX_STOCK, Quote.CIDX_BOARD, Quote.CIDX_SIDE, Quote.CIDX_NUMBER});
        stockModel.setKey(new int[]{Stock.CIDX_CODE});
        brokerModel.setKey(new int[]{Broker.CIDX_CODE});
        stockSummModel.setKey(new int[]{StockSummary.CIDX_CODE, StockSummary.CIDX_BOARD});
        stockSummModelHistori.setKey(new int [] {StockSummary.CIDX_CODE});
        marketModel.setKey(new int[]{Market.CIDX_BOARD});
        marketSummModel.setKey(new int[]{Summary.CIDX_CODE});
        indicesModel.setKey(new int[]{Indices.CIDX_CODE});
        newsModel.setKey(new int[]{News.CIDX_ID});
        corpActionModel.setKey(new int[]{CorpAction.CIDX_ACTIONTYPE, CorpAction.CIDX_STOCK, CorpAction.CIDX_CUMDATE});
        corpActionModel2.setKey(new int[]{CorpAction.CIDX_ACTIONTYPE, CorpAction.CIDX_STOCK, CorpAction.CIDX_CUMDATE});
        tradePriceModel.setKey(new int[]{TradePrice.CIDX_CODE, TradePrice.CIDX_BOARD, TradePrice.CIDX_PRICE});        
        tradeSummModel.setKey(new int[]{TradeSummary.CIDX_BROKER, TradeSummary.CIDX_STOCK, TradeSummary.CIDX_BOARD, TradeSummary.CIDX_INVESTOR});
        tradeSummBrokerModel.setKey(new int[]{TradeSummary.CIDX_BROKER});
        tradeSummBSModel.setKey(new int[]{TradeSummary.CIDX_BROKER, TradeSummary.CIDX_STOCK});
        tradeSummSBModel.setKey(new int[]{TradeSummary.CIDX_STOCK, TradeSummary.CIDX_BROKER});
        tradeSummSBModelsell.setKey(new int[]{TradeSummary.CIDX_STOCK, TradeSummary.CIDX_BROKER});
        tradeSummStockInvModel.setKey(new int[]{TradeSummary.CIDX_STOCK, TradeSummary.CIDX_INVESTOR});
        tradeSummInvModel.setKey(new int[]{TradeSummary.CIDX_INVESTOR});
        globalNewsModel.setKey(new int[]{Article.CIDX_ID, Article.CIDX_DATE});
        currencyModel.setKey(new int[]{Currency.CIDX_CODE});
        globalIndicesModel.setKey(new int[]{GlobalIndices.CIDX_CODE});
        rupsModel.setKey(new int[]{Rups.CIDX_STOCK, Rups.CIDX_DATE});
        ipoModel.setKey(new int[]{IPO.CIDX_NAME});
        commodityModel.setKey(new int[]{Commodity.CIDX_NAME});
        futureModel.setKey(new int[]{Future.CIDX_NAME});
        //queueModel.setKey(new int[] {Queue.CIDX_STOCK,  Queue.CIDX_NUMBER});
        stockNewsModel.setKey(new int[]{StockNews.CIDX_NEWSID});//Stock News
        stockComparisonBySectorModel.setKey(new int[]{StockComparisonBySector.CIDX_STOCK});//Comparison By Sector
        sectorModel.setKey(new int[]{Sector.CIDX_SECTORNAME});
        sectorNameModel.setKey(new int[]{Sector.CIDX_SECTORCODE});
    	queueModel.setKey(new int[] { Queue.CIDX_ORDERID });//add order tracking
    	queueType.setKey(new int[] { Board.CIDX_CODE });//add order tracking    	
    	tradeHistoryModel.setKey(new int[]{RunningTrade.CIDX_TRADENO}); // untuk time & trade double
    	tradeHistoryMModel.setKey(new int[]{RunningTrade.CIDX_TRADENO}); // untuk time & trade double
    	corpActionSDModel.setKey(new int[]{CorpActionSD.CIDX_ACTIONTYPE, CorpActionSD.CIDX_STOCK, CorpActionSD.CIDX_CUMDATE}); //untuk CORPACTION di STOCKDETAIL By Iben 04092013
    	corpActionSDModel2.setKey(new int[]{CorpActionSD.CIDX_ACTIONTYPE, CorpAction.CIDX_STOCK, CorpAction.CIDX_CUMDATE});
    	topGainer.setKey(new int[]{StockSummary.CIDX_CODE, StockSummary.CIDX_BOARD});
        periode.setKey(new int[]{Board.CIDX_CODE});
        invbymarket.setKey(new int[]{TradeSummary.CIDX_STOCK, TradeSummary.CIDX_INVESTOR});
        
        map.put(DATA_BOARD, boardModel);
        map.put(DATA_INVESTOR, investorModel);
        map.put(DATA_ALERT, alertModel);
        map.put(DATA_CHART, chartModel);
        map.put(DATA_QUOTE, quoteModel);
        map.put(DATA_RUNNINGTRADE, runningTradeModel);
        map.put(DATA_STOCK, stockModel);
        map.put(DATA_BROKER, brokerModel);
        map.put(DATA_STOCKSUMMARY, stockSummModel);
        
        map.put(DATA_STOCKSUMMARY_HISTORY, stockSummModelHistori);
        map.put(DATA_MARKET, marketModel);
        map.put(DATA_MARKETSUMM, marketSummModel);
        map.put(DATA_INDICES, indicesModel);
		map.put(DATA_NEWS, newsModel);
        map.put(DATA_CORPACTION, corpActionModel);
        map.put(DATA_CORPACTION2, corpActionModel2);
        map.put(DATA_TRADEPRICE, tradePriceModel);
		map.put(DATA_TRADEHISTORY, tradeHistoryModel);
		map.put(DATA_TRADEHISTORYM, tradeHistoryMModel);
		map.put(DATA_TRADESUMMARY, tradeSummModel);
		map.put(DATA_TRADESUMBROKER, tradeSummBrokerModel);
		map.put(DATA_TRADESUMBS, tradeSummBSModel);
		map.put(DATA_TRADESUMSB, tradeSummSBModel);
		map.put(DATA_TRADESUMSB_SELL, tradeSummSBModelsell);
		map.put(DATA_TRADESUMSTOCKINV, tradeSummStockInvModel);
		map.put(DATA_TRADESUMINV, tradeSummInvModel);
		map.put(DATA_GLOBALNEWS, globalNewsModel);
		map.put(DATA_CURRENCY, currencyModel);
		map.put(DATA_GLOBALINDICES, globalIndicesModel);
		map.put(DATA_RUPS, rupsModel);
		map.put(DATA_IPO, ipoModel);
		map.put(DATA_COMMODITY, commodityModel);
		map.put(DATA_FUTURE, futureModel);
		map.put(DATA_STOCKNEWS, stockNewsModel);
		map.put(DATA_STOCKCOMPARISONBYSECTOR, stockComparisonBySectorModel);
		map.put(DATA_SECTOR, sectorModel);
		map.put(DATA_SECTORNAME, sectorNameModel);
		map.put(DATA_QUOTE_QUEUE, queueModel);//add order tracking
		map.put(DATA_QUEUE_TYPE, queueType);//add order tracking
		map.put(DATA_CORPACTIONSD, corpActionSDModel); //untuk CORPACTION di STOCKDETAIL By Iben 04092013
		map.put(DATA_CORPACTIONSD2, corpActionSDModel2);
		map.put(DATA_TOPGAINER, topGainer); // yosep topgainer
		map.put(DATA_PERIODE, periode);// yosep topgainer
		map.put(DATA_TRADESUMMARKETINV, invbymarket);//yosep invbymarket
		//map.put(DATA_QUEUE, queueModel);
        initBoard();
        initInvestor();
        initMarket();  
        initCorp();
        initQueueType();//add order tracking
        initPeriode();// yosep topgainer
    }
	
	
	//add order tracking
	protected void initQueueType() {
		Vector vrow = new Vector(5); 
		Board b = new Board();
		b.setActiontype("Bid");
		b.setName("BID");
		vrow.addElement(BoardDef.createTableRow(b));
		b = new Board();
		b.setActiontype("Offer");
		b.setName("OFFER");
		vrow.addElement(BoardDef.createTableRow(b));
		queueType.addRow(vrow, false, false);
	}
	private void initCorp() {
		Vector vrow = new Vector(5);
		CorpAction ca1 = new CorpAction();
		ca1.setActiontype("Stock Split");
		//ca1.setAmount((Double)0);
		 ca1.setHeader("Stock Split");
		        vrow.addElement(CorpActionDef.createTableRow(ca1));

		CorpAction ca2 = new CorpAction();
		ca2.setActiontype("Stock Bonus");
		ca2.setHeader("Stock Bonus");
		        vrow.addElement(CorpActionDef.createTableRow(ca2));
		        
        CorpAction ca3 = new CorpAction();
		ca3.setActiontype("Right Issue");
		ca3.setHeader("Right Issue");
		 vrow.addElement(CorpActionDef.createTableRow(ca3));
		        corpActionModel2.addRow(vrow, false, false); 
		
	}

	protected void initBoard(){
		Vector vrow = new Vector(5);
        Board b = new Board();
        b.setActiontype("All");
        b.setName("ALL");
        vrow.addElement(BoardDef.createTableRow(b));
        b = new Board();
        b.setActiontype("Regular");
        b.setName("RG");
        vrow.addElement(BoardDef.createTableRow(b));
        b = new Board();
        b.setActiontype("Cash Market");
        b.setName("TN");
        vrow.addElement(BoardDef.createTableRow(b));
        b = new Board();
        b.setActiontype("Negotiated");
        b.setName("NG");
        vrow.addElement(BoardDef.createTableRow(b));
        boardModel.addRow(vrow, false, false);		
	}
	
	protected void initInvestor(){
		Vector vrow = new Vector(5);
        Board b = new Board();
        b.setActiontype("All");
        b.setName("ALL");
        vrow.addElement(BoardDef.createTableRow(b));
        b = new Board();
        b.setActiontype("Domestic");
        b.setName("D");
        vrow.addElement(BoardDef.createTableRow(b));
        b = new Board();
        b.setActiontype("Foreign");
        b.setName("F");
        vrow.addElement(BoardDef.createTableRow(b));
        investorModel.addRow(vrow, false, false);		
	}
	// yosep topgainer
	protected void initPeriode(){
		Vector vrow = new Vector();
		Board b = new Board();
		b.setActiontype("Daily");
		b.setName("DAY");
		vrow.addElement(BoardDef.createTableRow(b));
		b = new Board();
		b.setActiontype("Weekly");
		b.setName("WEEK");
		vrow.addElement(BoardDef.createTableRow(b));
		b = new Board();
		b.setActiontype("Monthly");
		b.setName("MONTH");
		vrow.addElement(BoardDef.createTableRow(b));
		b = new Board();
		b.setActiontype("Semi Annualy");
		b.setName("ANNUALY");
		vrow.addElement(BoardDef.createTableRow(b));
		b = new Board();
		b.setActiontype("Year To Date");
		b.setName("YEAR");
		vrow.addElement(BoardDef.createTableRow(b));
		periode.addRow(vrow, false, false);
	}
	
	protected void initMarket(){
		Vector vRow = new Vector(5);
		Market m;
		for (Iterator i = MarketDef.boardMap.keySet().iterator(); i.hasNext();){
			m = new Market();
			m.setBoard((String)i.next());
			vRow.addElement(MarketDef.createTableRow(m));
		}
		marketModel.addRow(vRow, false, false);
	}

	public GridModel get(Integer key){
		return (GridModel)map.get(key);
	}
}
