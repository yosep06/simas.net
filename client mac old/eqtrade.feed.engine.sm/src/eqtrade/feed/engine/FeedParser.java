package eqtrade.feed.engine;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.core.Utils;
import eqtrade.feed.model.Sector;
import eqtrade.feed.parser.ActionParser;
import eqtrade.feed.parser.ArticleParser;
import eqtrade.feed.parser.BrokerParser;
import eqtrade.feed.parser.CommodityParser;
import eqtrade.feed.parser.CurrencyParser;
import eqtrade.feed.parser.FutureParser;
import eqtrade.feed.parser.GlobalIdxParser;
import eqtrade.feed.parser.IPOParser;
import eqtrade.feed.parser.IndicesParser;
import eqtrade.feed.parser.NewsParser;
//import eqtrade.feed.parser.QueueParser;
import eqtrade.feed.parser.OrderParser;
import eqtrade.feed.parser.QuoteParser;
import eqtrade.feed.parser.RupsParser;
import eqtrade.feed.parser.SectorParser;
import eqtrade.feed.parser.StockComparisonBySectorParser;
import eqtrade.feed.parser.StockNewsParser;
import eqtrade.feed.parser.StockParser;
import eqtrade.feed.parser.StockSummParser;
import eqtrade.feed.parser.TopGainerParser;
import eqtrade.feed.parser.TradeHistoryMParser;
import eqtrade.feed.parser.TradeHistoryParser;
import eqtrade.feed.parser.TradeParser;
import eqtrade.feed.parser.TradePriceParser;
import eqtrade.feed.parser.TradeSummBSParser;
import eqtrade.feed.parser.TradeSummBrokerParser;
import eqtrade.feed.parser.TradeSummInvParser;
import eqtrade.feed.parser.TradeSummSBParser;
import eqtrade.feed.parser.TradeSummStockInvParser;

public class FeedParser {
    public final static String PARSER_QUOTE = new String("1");
    public final static String PARSER_RUNNINGTRADE = new String("2");
    public final static String PARSER_STOCK = new String("3");
    public final static String PARSER_BROKER = new String("4");
    public final static String PARSER_STOCKSUMMARY = new String("5");
    public final static String PARSER_INDICES = new String("6");
    public final static String PARSER_NEWS = new String("9");
    public final static String PARSER_CORPACTION = new String("A");    
    public final static String PARSER_TRADEPRICE = new String("TP");
    //public final static String PARSER_TRADESUMMARY = new String("TS");
    public final static String PARSER_TRADEHISTORYM = new String("MTH");
    public final static String PARSER_TRADEHISTORY = new String("TH");
    public final static String PARSER_ALERT = new String("AL");
    public final static String PARSER_CURRENCY = new String("CR");
    public final static String PARSER_GLOBALIDX = new String("GI");
    public final static String PARSER_ARTICLE = new String("NW");    
    public final static String PARSER_COMMODITY = new String("COM");
    public final static String PARSER_FUTURE = new String("FTR");
    public final static String PARSER_TRADESUMMBROKER = new String("TSBB");    
    public final static String PARSER_TRADESUMMBS = new String("TSBS");    
    public final static String PARSER_TRADESUMMSB = new String("TSSB");    
    public final static String PARSER_TRADESUMMSTOCKINV = new String("TSSI");    
    public final static String PARSER_TRADESUMMINV = new String("TSI");  
    public final static String PARSER_STOCKNEWS = new String("SN");
    public final static String PARSER_COMPARISONBYSECTOR = new String("CBS");
    public final static String PARSER_SECTOR = new String("SC");
    
    public final static String PARSER_RUPS = new String("RPS");
    public final static String PARSER_IPO = new String("IPO");
    //add order tracking
    public final static String PARSER_QUEUE_BID = new String("10");
	public final static String PARSER_QUEUE_OFFER = new String("11");
	public final static String PARSER_QUEUE_BID_DELETE = new String("12");
	public final static String PARSER_QUEUE_OFFER_DELETE = new String("13");
	public final static String PARSER_QUEUE = new String("queue"); 
	public final static String PARSER_CORPACTIONSD = new String("A"); 
	public final static String PARSER_TOPGAINER = new String("TG");//yosep topgainer
    private final Log log = LogFactory.getLog(getClass());

    
    protected IFeedEngine engine;
    protected HashMap map = new HashMap(10);
	
	public FeedParser(IFeedEngine engine){
		this.engine = engine;
		init();
		start();
	}
	
	public void addParser(String key, Parser parser){
		map.put(key, parser);
		parser.start();
	}
	
	protected void init(){
        map.put(PARSER_QUOTE, new QuoteParser(engine));
        map.put(PARSER_RUNNINGTRADE, new TradeParser(engine));
        map.put(PARSER_STOCK, new StockParser(engine));
        map.put(PARSER_BROKER, new BrokerParser(engine));
        map.put(PARSER_STOCKSUMMARY, new StockSummParser(engine));
        map.put(PARSER_INDICES, new IndicesParser(engine));
        map.put(PARSER_NEWS, new NewsParser(engine));
        map.put(PARSER_CORPACTION, new ActionParser(engine));
        map.put(PARSER_TRADEPRICE, new TradePriceParser(engine));
        //map.put(PARSER_TRADESUMMARY, new TradeSummParser(engine));
        map.put(PARSER_TRADEHISTORY, new TradeHistoryParser(engine));
        map.put(PARSER_TRADEHISTORYM, new TradeHistoryMParser(engine));
        map.put(PARSER_CURRENCY, new CurrencyParser(engine));
        map.put(PARSER_GLOBALIDX, new GlobalIdxParser(engine));
        map.put(PARSER_ARTICLE, new ArticleParser(engine));
        map.put(PARSER_TRADESUMMBROKER, new TradeSummBrokerParser(engine));
        map.put(PARSER_TRADESUMMBS, new TradeSummBSParser(engine));
        map.put(PARSER_TRADESUMMSB, new TradeSummSBParser(engine));
        map.put(PARSER_TRADESUMMSTOCKINV, new TradeSummStockInvParser(engine));
        map.put(PARSER_TRADESUMMINV, new TradeSummInvParser(engine));
        map.put(PARSER_RUPS, new RupsParser(engine));
        map.put(PARSER_IPO, new IPOParser(engine));
        map.put(PARSER_COMMODITY, new CommodityParser(engine));
        map.put(PARSER_FUTURE, new FutureParser(engine));
        map.put(PARSER_STOCKNEWS, new StockNewsParser(engine));
        map.put(PARSER_COMPARISONBYSECTOR, new StockComparisonBySectorParser(engine));
        map.put(PARSER_SECTOR, new SectorParser(engine));
        map.put(PARSER_TOPGAINER, new TopGainerParser(engine));// yosep topgainer
       
        try {
			map.put(PARSER_QUEUE, new OrderParser(engine));
		} catch (Exception e) {			
			e.printStackTrace();
		}//OrderTracking
	}
	
	public void parse(String msg){
        Vector vheader = Utils.parser(msg, "|");
        for (Iterator i = map.keySet().iterator(); i.hasNext();){
            String key = (String)i.next();
            if (key.startsWith(vheader.elementAt(1).toString())) ((Parser)map.get(key)).onMessage(msg);
            else if  (key.equals((vheader.elementAt(0).toString()))){ 
            	//log.info("feedparser" + msg + ": "+ vheader.toString());
            	((Parser)map.get(key)).onMessage(msg);}
        }
//      //if (header.equals("RT")) ((ThreadAlert)map.get(PARSER_ALERT)).onMessage(mdt);
        if(vheader.elementAt(0).equals("queue")){
        	((Parser)map.get(PARSER_QUEUE)).onMessage(msg);
        }
    }

    public Parser get(String code){
        return (Parser)map.get(code);
    }
    
    public void subscribe(String key, String msg){
        get(key).subscribe(msg);
      //  log.info("subscribe "+key);
    }
    
    public void unsubscribe(String key, String msg){
        get(key).unsubscribe(msg);
    }
    
    public void resubscribe(){
        for (Iterator i = map.keySet().iterator(); i.hasNext();){
            String key = (String)i.next();
            ((Parser)map.get(key)).resubscribe();
        }
    }
    
    public void start(){
        for (Iterator i = map.keySet().iterator(); i.hasNext();){
            String key = (String)i.next();
            ((Parser)map.get(key)).setStart();
        }        
    }
    
    public void stop(){
        for (Iterator i = map.keySet().iterator(); i.hasNext();){
            String key = (String)i.next();            
            ((Parser)map.get(key)).setStop();
        }          
    }
}
