package eqtrade.feed.engine;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.Timer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedConnection;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Utils;
import feed.gateway.broadcaster.Gateway;

public class FeedConnection implements IFeedConnection {
	protected final Log log = LogFactory.getLog(getClass());
	private volatile Gateway gateway = null;
	private long sesid = 0;
	private String uid = "", pwd, svr;
	private String ssvr;
	private Thread inQueue;
	private Thread outQueue;
	private int ninterval, totalServer;
	private Timer refreshTimer = null;
	private volatile Vector vin = new Vector(20, 10);
	private volatile boolean bstop = false;
	private volatile Vector vou = new Vector(10, 5);
	private final static String C_GETNEW = "GET";
	private final static String C_SUBSCRIBE = "SUB";
	private final static String CMD_UNSUBSCRIBE = "UNSUB";
	private long diff = 0;
	private Timer timerRefresh = null;

	private HashMap hashIP = new HashMap();
	private IFeedEngine engine;

	public FeedConnection(IFeedEngine engine) {
		log.info("feed updated2");
		this.engine = engine;
		init();
	}

	public String getHistory(String param) throws Exception {
		try {
			String result = null;
			byte[] bpar = (gateway != null) ? gateway.getHistory((int) sesid,
					param) : null;
			if (bpar != null) {
				result = (String) new String(Utils.decompress(bpar));
			}
			return result;
		} catch (RemoteException ex) {
			ex.printStackTrace();
			throw new Exception("failed");
		}
	}
	
	/*public String getQueue(String param) throws Exception {
		log.info("FeedConn -- getQueuue "+ param);
		//System.out.println("FeedConn -- getQueuue= "+ param);
		try {
			String result = null;
			byte[] bpar = (gateway != null) ? gateway.getQueue((int) sesid,
					param) : null;
			
			if (bpar != null) {
				result = (String) new String(Utils.decompress(bpar));
			}
			return result;
		} catch (RemoteException ex) {
			ex.printStackTrace();
			throw new Exception("failed");
		}
	}*/
	
	public byte[] getFile(String param) throws Exception {
		try {
			return (gateway != null) ? gateway.getHistory((int) sesid, param)
					: null;
		} catch (RemoteException ex) {
			ex.printStackTrace();
			throw new Exception("failed");
		}
	}

	private long getServerTime() {
		try {
			long result = 0;
			result = (gateway != null) ? gateway.getTime() : new Date()
					.getTime();
			return result;
		} catch (RemoteException ex) {
			return new Date().getTime();
		}

	}

	private void init() {
		loadSetting();
		inQueue = new Thread(createInQueue());
		outQueue = new Thread(createOutQueue());
		refreshTimer = new Timer(ninterval, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				(new Thread(new Runnable() {
					public void run() {
						addOut(C_GETNEW);
					}
				})).start();
				//addOut(C_GETNEW);

			}
		});
		refreshTimer.setInitialDelay(ninterval);
		refreshTimer.stop();
		timerRefresh = new Timer(3 * 60 * 1000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				(new Thread(new Runnable() {
					public void run() {
						diff = new Date().getTime() - getServerTime();
					}
				})).start();
				//diff = new Date().getTime() - getServerTime();
			}
		});
		timerRefresh.setInitialDelay(3 * 60 * 1000);
		timerRefresh.stop();
	}

	public long getTime() {
		return new Date().getTime() - diff;
	}

	public void setStop() {
		refreshTimer.stop();
		timerRefresh.stop();
		bstop = true;
		synchronized (vin) {
			vin.notify();
		}
		synchronized (vou) {
			vou.notify();
		}
	}
	
	
	 private Runnable createInQueue(){
		 	log.info("createInQueue --FeedConnection on engine");
	        return new Runnable(){
	            public void run(){
	                Vector      vtemp;
	                while(!bstop){
	                    try {
	                        if (vin.size() > 0){
	                            vtemp = (Vector) vin.clone();
	                            vin.removeAll(vtemp);
	                            for (int i = 0; i < vtemp.size(); i++){
//	                                HashMap param = new HashMap(1);
//	                                param.put(FeedEventDispatcher.PARAM_MESSAGE, vtemp.elementAt(i));
//	                                engine.getEventDispatcher().pushEvent(FeedEventDispatcher.EVENT_ONDATA, param);
	                                   engine.getParser().parse((String)vtemp.elementAt(i));
	                            }
	                        } else {
	                                synchronized(vin){vin.wait();}
	                        }
	                    } catch (Exception ex){}
	                }
	                System.gc();
	            }
	       };
	    }
	

/*	private Runnable createInQueue() {
		return new Runnable() {
			public void run() {
				Vector vtemp;
				while (!bstop) {
					try {
						if (vin.size() > 0) {
							vtemp = (Vector) vin.clone();
							vin.removeAll(vtemp);
							for (int i = 0; i < vtemp.size(); i++) {
								HashMap param = new HashMap(1);
								param.put(FeedEventDispatcher.PARAM_MESSAGE,
										vtemp.elementAt(i));
								engine.getEventDispatcher()
										.pushEvent(
												FeedEventDispatcher.EVENT_ONDATA,
												param);
							}
						} else {
							synchronized (vin) {
								vin.wait();
							}
						}
					} catch (Exception ex) {
					}
				}
				System.gc();
			}
		};
	}*/

	private Runnable createOutQueue() {
		return new Runnable() {
			public void run() {
				Vector vtemp;
				while (!bstop) {
					try {
						if (vou.size() > 0) {
							vtemp = (Vector) vou.clone();
							vou.removeAll(vtemp);
							processOut(vtemp);
						} else {
							synchronized (vou) {
								vou.wait();
							}
						}
					} catch (Exception ex) {
					}
				}
				System.gc();
			}
		};
	}

	protected void addIn(String vins) {
		vin.addElement(vins);
		synchronized (vin) {
			vin.notify();
		}
	}

	private void addOut(String smsg) {
		vou.addElement(smsg);
		synchronized (vou) {
			vou.notify();
		}
	}

	private void loadSetting() {
		Object o = Utils.readFile("data/config/feed.dat");
		Vector v = null;
		if (o == null) {
			v = new Vector(3);
			v.addElement("172.21.1.101:5555/feed");
			v.addElement("172.21.1.101:5555/feed");
			//v.addElement("127.0.0.1:5555/feed");
			//v.addElement("127.0.0.1:5555/feed");
			v.addElement(new Integer(600));
			Utils.writeFile("data/config/feed.dat", v);
		} else {
			v = (Vector) o;
		}
		//ninterval = ((Integer) v.elementAt(2)).intValue();
		int x = v.size();
		ninterval = ((Integer) v.elementAt(x-1)).intValue();
		totalServer = x-1;
		hashIP.clear();
		for (int i = 1; i <= totalServer; i++) {
			ssvr = (String) v.elementAt(i - 1);
			hashIP.put(new String(i + ""), new String("rmi://" + ssvr));
		}
		// try {
		// sip = InetAddress.getLocalHost().getHostAddress();
		// } catch (Exception ex){}
		log.info("available server test: " + totalServer);
		log.info("available server test: " + hashIP.toString());
	}

	Thread threadLogin = null;

	public void logon(final String suid, final String spwd) {
		if (threadLogin != null) {
			threadLogin.stop();
			threadLogin = null;
		}
		threadLogin = new Thread(new Runnable() {
			public void run() {
				doLogin(suid, spwd);
			}
		});
		threadLogin.start();
	}

	public void doLogin(String suid, String spwd) {
		loadSetting();
		boolean bresult = false;
		int i = 1, curr = 1, next = 2, loginattempt = 10, logcoba=1;;
		String url;
		while (logcoba <=loginattempt) {
			i =1;
			curr=1;
			next=2;
			for (; i <= (totalServer * 1); i++) {
				try {
					if (i == next) {
						curr++;
						next = next + 1;
					}
					url = (String) hashIP.get(new String(curr + ""));
					log.info("login to : " + url + " user name: " + suid);
					gateway = (Gateway) Naming.lookup(url);
					bresult = gateway != null;
					if (bresult) {
						try {
							sesid = gateway.login(suid, spwd);
							bresult = sesid > 0;
							if (bresult) {
								String session = gateway.getDate();
								if (session != null) {
									logcoba =loginattempt+1;
									FeedSetting.session = new String(session);
									diff = new Date().getTime() - getServerTime();
									startServices();
									uid = suid;
									pwd = spwd;
									svr = url;
									log.info("login OK");
									engine.getEventDispatcher()
											.pushEvent(
													FeedEventDispatcher.EVENT_LOGINOK,
													null);
									break;
								} else {
									log.warn("Login Failed, retrying(" + i
											+ ")....");
									HashMap param = new HashMap(1);
									param.put(FeedEventDispatcher.PARAM_MESSAGE,
											"Register Failed, retrying(" + i
													+ ")....");
									engine.getEventDispatcher().pushEvent(
											FeedEventDispatcher.EVENT_LOGINSTATUS,
											param);
								}
							} else {
								log.warn("Invalid userid/password....");
								HashMap param = new HashMap(1);
								param.put(FeedEventDispatcher.PARAM_MESSAGE,
										"Invalid userid/password");
								engine.getEventDispatcher().pushEvent(
										FeedEventDispatcher.EVENT_LOGINFAILED,
										param);
								break;
							}
						} catch (Exception exp) {
							log.warn("Failed, " + exp.getMessage());
							HashMap param = new HashMap(1);
							//param.put(FeedEventDispatcher.PARAM_MESSAGE,
							//		"Failed, retrying(" + i + ")....");
							param.put(FeedEventDispatcher.PARAM_MESSAGE, "connecting.....");
							engine.getEventDispatcher().pushEvent(
									FeedEventDispatcher.EVENT_LOGINSTATUS, param);
							try {
								Thread.sleep(500);
							} catch (Exception d) {
							}
							;
							bresult = false;
							log.error(Utils.logException(exp));
							exp.printStackTrace();
							// break;
						}
					} else {
						log.warn("Connection Failed, retrying(" + i + ")....");
						HashMap param = new HashMap(1);
						//param.put(FeedEventDispatcher.PARAM_MESSAGE,
						//		"Connection Failed, retrying(" + i + ")....");
						param.put(FeedEventDispatcher.PARAM_MESSAGE, "connecting.....");
						engine.getEventDispatcher().pushEvent(
								FeedEventDispatcher.EVENT_LOGINSTATUS, param);
					}
				} catch (Exception ex) {
					log.warn("Failed, retrying(" + i + ")....");
					HashMap param = new HashMap(1);
					//param.put(FeedEventDispatcher.PARAM_MESSAGE,
					//		"Failed, retrying(" + i + ")....");
					param.put(FeedEventDispatcher.PARAM_MESSAGE, "connecting.....");
					engine.getEventDispatcher().pushEvent(
							FeedEventDispatcher.EVENT_LOGINSTATUS, param);
					try {
						Thread.sleep(500);
					} catch (Exception exp) {
					}
					;
					bresult = false;
					log.error(Utils.logException(ex));
					ex.printStackTrace();
				}
			}
			logcoba=logcoba+1;
		}	
		if (!bresult && i > totalServer * 1) {
			HashMap param = new HashMap(1);
			param.put(FeedEventDispatcher.PARAM_MESSAGE, "Connection Failed, Please Try Again ..");
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_LOGINFAILED, param);
		}
	}

	private void startServices() {
		if (!this.inQueue.isAlive())
			inQueue.start();
		if (!this.outQueue.isAlive())
			outQueue.start();
		refreshTimer.start();
		timerRefresh.start();
	}

	public void changePassword(String soldpwd, String snewpwd) {
		try {
			if (gateway != null) {
				log.info("request change password");
				if (soldpwd.equals(pwd)) {
					if (gateway.chgPassword((int) sesid, uid, soldpwd, snewpwd)) {
						pwd = snewpwd;
						engine.getEventDispatcher().pushEvent(
								FeedEventDispatcher.EVENT_CHGPASSWDOK, null);
					} else {
						HashMap param = new HashMap(1);
						param.put(FeedEventDispatcher.PARAM_MESSAGE,
								"Failed, try again..");
						engine.getEventDispatcher().pushEvent(
								FeedEventDispatcher.EVENT_CHGPASSWDFAILED,
								param);
					}
				} else {
					HashMap param = new HashMap(1);
					param.put(FeedEventDispatcher.PARAM_MESSAGE,
							"old password is not valid");
					engine.getEventDispatcher().pushEvent(
							FeedEventDispatcher.EVENT_CHGPASSWDFAILED, param);
				}
			} else {
				HashMap param = new HashMap(1);
				param.put(FeedEventDispatcher.PARAM_MESSAGE,
						"Failed, not connected");
				engine.getEventDispatcher().pushEvent(
						FeedEventDispatcher.EVENT_CHGPASSWDFAILED, param);
			}
		} catch (Exception ex) {
			HashMap param = new HashMap(1);
			param.put(FeedEventDispatcher.PARAM_MESSAGE,
					"Failed, connection drop");
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_CHGPASSWDFAILED, param);
			if (!ex.getMessage().endsWith("killed"))
				reconnect();
			else
				killed();
		}
	}

	private void killed() {
		engine.getEventDispatcher().pushEvent(FeedEventDispatcher.EVENT_KILLED,
				null);
	}

	private void reconnect() {
		refreshTimer.stop();
		timerRefresh.stop();
		if (sesid != -1)
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_DISCONNECT, null);
	}

	public boolean logout() {
		refreshTimer.stop();
		timerRefresh.stop();
		boolean bresult = true;
		try {
			uid = "";
			vou.clear();
			threadLogin.stop();
			threadLogin = null;
			gateway.logout((int) sesid);
			sesid = -1;
			this.gateway = null;
			log.info("request logout");
		} catch (Exception ex) {
			bresult = false;
		}

		return bresult;
	}

	public void reconnect(final int count) {
		new Thread(new Runnable() {
			public void run() {
				doReconnect(count);
			}
		}).start();
	}

	private int flip = 1, nextflip = 1, cnt = 0;

	public void doReconnect(int counter) {
		boolean bresult = false;
		try {
			cnt++;
			HashMap param = new HashMap(1);
			param.put(FeedEventDispatcher.PARAM_MESSAGE,
					"trying reconnecting...");
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_RECONNECTSTATUS, param);
			if (cnt == nextflip) {
				flip++;
				nextflip = nextflip + 1;
			}
			if (cnt > totalServer * 1) {
				flip = 1;
				nextflip = 1;
				cnt = 0;
			}
			String url = (String) hashIP.get(new String(flip + ""));
			log.info("reconnect to: " + url + " userid: " + uid
					+ " flip number: " + flip);
			gateway = (Gateway) Naming.lookup(url);
			bresult = gateway != null;
			if (bresult) {
				sesid = gateway.login(uid, pwd);
				bresult = sesid > 0;
				if (bresult) {
					String session = gateway.getDate();
					if (session != null) {
						FeedSetting.session = new String(session);
						svr = url;
						startServices();
						log.info("ok, connection ready");
						engine.getEventDispatcher().pushEvent(
								FeedEventDispatcher.EVENT_RECONNECTOK, null);
					} else {
						log.warn("error while get session from server");
						HashMap param2 = new HashMap(1);
						param2.put(FeedEventDispatcher.PARAM_MESSAGE,
								"Failed on register to server, try again");
						engine.getEventDispatcher().pushEvent(
								FeedEventDispatcher.EVENT_RECONNECTSTATUS,
								param2);
					}
				} else {
					log.warn("reconnecting failed");
					HashMap param2 = new HashMap(1);
					param2.put(FeedEventDispatcher.PARAM_MESSAGE,
							"reconnecting failed or server not ready");
					engine.getEventDispatcher().pushEvent(
							FeedEventDispatcher.EVENT_RECONNECTSTATUS, param2);
				}
			}
		} catch (Exception ex) {
			log.warn("reconnecting failed");
			HashMap param2 = new HashMap(1);
			param2.put(FeedEventDispatcher.PARAM_MESSAGE,
					"reconnecting failed or server not ready");
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_RECONNECTFAILED, param2);
			log.error(Utils.logException(ex));
			ex.printStackTrace();
		}
	}

	private synchronized void processOut(Vector vdatamsg) {
		byte[] bpar = null;
		try {
			String scmd, stemp;
			Vector vtempdata = new Vector(10, 5);
			for (int i = 0; i < vdatamsg.size(); i++) {
				stemp = vdatamsg.elementAt(i).toString();
				if (!vtempdata.contains(stemp)) {
					vtempdata.addElement(stemp);
				}
			}

			for (int i = 0; i < vtempdata.size(); i++) {
				scmd = vtempdata.elementAt(i).toString();
				if (sesid > -1) {
					if (scmd.equals(C_GETNEW)) {
						bpar = (gateway != null) ? gateway
								.getMessages((int) sesid) : null;
						if (bpar != null) {
							String vintmp = (String) new String(
									Utils.decompress(bpar));
							String as[] = vintmp.split("\n", -2);
							int aslength = as.length - 1;
							for (int j = 0; j < aslength; j++) {
								String s1 = as[j];
								addIn(s1);
							}
						}
					} else if (scmd.startsWith(C_SUBSCRIBE)) {
						String[] msg = scmd.split("\\*");
						if (gateway != null)
							gateway.subscribe((int) sesid, msg[1]);
					} else if (scmd.startsWith(CMD_UNSUBSCRIBE)) {
						String[] msg = scmd.split("\\*");
						if (gateway != null)
							gateway.unsubscribe((int) sesid, msg[1]);
					}
				}
			}
		} catch (RemoteException ex) {
			log.error(Utils.logException(ex));
			ex.printStackTrace();
			if (!ex.getMessage().endsWith("killed")) {
				reconnect();
			} else {
				if (timerRefresh.isRunning())
					killed();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(Utils.logException(ex));
		}
	}

	public void subscribe(String sheader) {
		StringBuffer sb = new StringBuffer(100);
		sb.append(C_SUBSCRIBE);
		sb.append("*");
		sb.append(sheader);
		addOut(sb.toString());
	}

	public void unsubscribe(String msgtype) {
		StringBuffer sb = new StringBuffer(100);
		sb.append(CMD_UNSUBSCRIBE);
		sb.append("*");
		sb.append(msgtype);
		addOut(sb.toString());
	}

	public String getUserId() {
		return uid;
	}

	public String getPassword() {
		return pwd;
	}

	public String getUrl() {
		return svr;
	}

	@Override
	public String getDate() throws RemoteException {
		// TODO Auto-generated method stub
		return gateway.getDate();
	}

	@Override
	public Long getSessionid() {
		// TODO Auto-generated method stub
		return sesid;
	}

	@Override
	public void send(String msg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean getFeedLogin() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getCountFeedLogin() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setCountFeedLogin(int a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setuserpass(String username, String password) {
		// TODO Auto-generated method stub
		
	}
}
