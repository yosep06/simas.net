package eqtrade.feed.core;

import java.util.Vector;

import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.idata.Row;

public class GridModelext extends GridModel{

	protected DefaultTableModel realTableModel;
	public Vector realData = new Vector(100,50);
	protected Vector headerData;
	protected int[] editableColumn ;
	protected int[] keys;
	protected boolean editable;
	
	public GridModelext(Vector headerData, boolean editable){
		super(headerData, editable);
		this.headerData = headerData;
		this.editable = editable;
		init();
	}
	
	public GridModelext(Vector headerData, Vector data, boolean editable)
	  { super(headerData, data, editable);
	    this.realData = data;
	    this.headerData = headerData;
	    this.editable = editable;
	    init();
	  }
	
	public void refresh() {
	   try {
	     realTableModel.fireTableDataChanged();
	   } catch (Exception localException) {	    }
	}

	public void refresh(int idx) {
	    this.realTableModel.fireTableChanged(
	    new TableModelEvent(this.realTableModel, idx, idx));
	}

	  
	private void init() {
		 realTableModel = new DefaultTableModel(this.realData, this.headerData);
		 editableColumn = new int[0];
	}
	private void addRow(final Vector rowData) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				add(rowData);
			}
		});
	    
	}
	private void add(Vector rowData){
		Vector temp = new Vector();
	    temp.addAll(rowData);
	    realTableModel.getDataVector().addAll(temp);
	    realTableModel.newRowsAdded(
	      new TableModelEvent(this, 
	      getRowCount() - 1, getRowCount() - 1, 
	      -1, 1));
	}
	
	private void insertRow(Vector rowData) {
	    Vector temp = new Vector();
	    temp.addAll(rowData);
	    realTableModel.getDataVector().addAll(0, temp);
	    refresh();
	}
	
	public void deleteRow(int idx) {
		realTableModel.removeRow(idx);
	}
	
  	public Vector getDataVector() {
	    return realTableModel.getDataVector();
  	}	  
  	
	@Override
	public void addTableModelListener(TableModelListener l) {
	    this.realTableModel.addTableModelListener(l);
		
	}

	@Override
	public Class getColumnClass(int columnIndex) {
	    return realTableModel.getColumnClass(columnIndex);
	}

	@Override
	public int getColumnCount() {
	    return this.realTableModel.getColumnCount();
	}

	@Override
	public String getColumnName(int columnIndex) {
	    return this.realTableModel.getColumnName(columnIndex);
	}

	@Override
	public int getRowCount() {
	    return this.realTableModel.getRowCount();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
	    return this.realTableModel.getValueAt(rowIndex, columnIndex);
	}

	@Override
	public boolean isCellEditable(int arg0, int arg1) {
	    return this.editable;
	}

	@Override
	public void removeTableModelListener(TableModelListener l) {
	    this.realTableModel.removeTableModelListener(l);
		
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	    this.realTableModel.setValueAt(aValue, rowIndex, columnIndex);
		
	}

	  public Object getDataByKey(Object[] param) {
	    int i = getIndexByKey(param);
	    return i >= 0 ? getDataByIndex(i) : null;
	  }

	  public Object getDataByField(Object[] param, int[] field) {
	    int i = getIndexByField(param, field);
	    return i >= 0 ? getDataByIndex(i) : null;
	  }

	  public Object getDataByIndex(int idx) {
	    return ((Column)this.realTableModel.getValueAt(idx, 0))
	      .getSource();
	  }

	  public void setKey(int[] keys) {
	    this.keys = new int[keys.length];
	    for (int i = 0; i < keys.length; i++)
	      this.keys[i] = keys[i];
	  }

	  public void addRow(Vector data, boolean allowDuplicate, boolean onFirstRow)
	  {
	    Row temp = null; Row temp2 = null;
	    boolean match = false;
	    int i = 0;
	    if (!allowDuplicate) {
	      if (this.keys != null)
	      {
	        for (i = 0; i < this.realTableModel.getRowCount(); i++) {
	          temp = (Row)((Column)this.realTableModel
	            .getValueAt(i, 0)).getSource();
	          temp2 = (Row)
	            ((Column)((Vector)data
	            .elementAt(0)).elementAt(0)).getSource();
	          for (int j = 0; j < this.keys.length; j++) {
	            Object obj1 = null;
	            Object obj2 = null;
	            try {
	              obj1 = temp.getData(this.keys[j]);
	              obj2 = temp2.getData(this.keys[j]);
	            }
	            catch (Exception localException)
	            {
	            }
	            if (obj1.toString().equals(obj2.toString()))
	              match = true;
	            else
	              match = false;
	            if (!match)
	              break;
	          }
	          if (match)
	            break;
	        }
	        if (match) {
	          for (int k = 0; k < this.realTableModel.getColumnCount(); k++)
	            try {
	              temp.setData(temp2.getData(k), k);
	            }
	            catch (Exception localException1) {
	            }
	          refresh(i);
	        }
	        else if (!onFirstRow) {
	          addRow(data);
	        } else {
	          insertRow(data);
	        }
	      }
	      else if (!onFirstRow) {
	        addRow(data);
	      } else {
	        insertRow(data);
	      }
	    }
	    else if (!onFirstRow)
	      addRow(data);
	    else
	      insertRow(data);
	  }

	  public int getIndexByKey(Object[] param)
	  {
	    Row temp = null;
	    boolean match = false;
	    int i = 0;
	    if (this.keys != null)
	    {
	      for (i = 0; i < this.realTableModel.getRowCount(); i++) {
	        temp = (Row)((Column)this.realTableModel.getValueAt(i, 
	          0)).getSource();
	        for (int j = 0; j < this.keys.length; j++) {
	          Object obj1 = null;
	          Object obj2 = null;
	          try {
	            obj1 = temp.getData(this.keys[j]);
	            obj2 = param[j];
	          } catch (Exception localException) {
	          }
	          if (obj1.toString().equals(obj2.toString()))
	            match = true;
	          else
	            match = false;
	          if (!match)
	            break;
	        }
	        if (match)
	          break;
	      }
	      if (match) {
	        return i;
	      }
	      return -1;
	    }
	    return -1;
	  }

	  public int getIndexByField(Object[] param, int[] field)
	  {
	    Row temp = null;
	    boolean match = false;
	    int i = 0;
	    if (field != null) {
	      for (i = 0; i < this.realTableModel.getRowCount(); i++) {
	        temp = (Row)((Column)this.realTableModel.getValueAt(i, 
	          0)).getSource();
	        for (int j = 0; j < field.length; j++) {
	          Object obj1 = null;
	          Object obj2 = null;
	          try {
	            obj1 = temp.getData(field[j]);
	            obj2 = param[j];
	          } catch (Exception localException) {
	          }
	          if (obj1.toString().equals(obj2.toString()))
	            match = true;
	          else
	            match = false;
	          if (!match)
	            break;
	        }
	        if (match)
	          break;
	      }
	      if (match) {
	        return i;
	      }
	      return -1;
	    }
	    return -1;
	  }
}
