package eqtrade.feed.core;

import java.util.Vector;

import javax.swing.event.TableModelEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.idata.Row;

public class EQGridModel extends GridModel {
	private Logger log = LoggerFactory.getLogger(getClass());

	public EQGridModel(Vector headerData, boolean editable) {
		super(headerData, editable);
	}

	public boolean addRow(Row row,Vector vRow, int idx) {
		try {
			Row temp = (Row) ((Column) this.realTableModel.getValueAt(idx, 0))
					.getSource();
			for (int k = 0; k < realTableModel.getColumnCount(); k++){
				try {
					temp.setData(row.getData(k), k);
				} catch (Exception localException1) {
				}
			}
			
			refresh(idx);
			
		} catch (Exception ex) {
			//ex.printStackTrace();
			//Vector v = new Vector();
			//v.addElement(vRow);
			//addRow(vRow, false, false);
			/*
			log.info(Utils.logException(ex));
			log.info("row count "+this.realTableModel.getRowCount());
			for(int i=0;i<this.realTableModel.getRowCount();i++){
				Row temp = (Row) ((Column) this.realTableModel.getValueAt(i, 0))
						.getSource();
				try {
					log.info("cek row "+temp.getData(3)+" "+i+" "+row.getData(3));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.info(Utils.logException(e));
					
				}

				
			}*/
			
			try {
			Vector temp = new Vector();
		    temp.addAll(vRow);
			
			realTableModel.getDataVector().addAll(temp);
		    realTableModel.newRowsAdded(new TableModelEvent(this, 
		      getRowCount() - 1, getRowCount() - 1, 
		      -1, 1));
			}catch(Exception ex2){
				log.info(Utils.logException(ex2));

			}
			return false;
		}

		return true;
	}

}
