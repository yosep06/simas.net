package eqtrade.feed.core;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import java.security.spec.KeySpec;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEParameterSpec;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import com.eqtrade.FileConfig;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.UnsupportedEncodingException;
import java.io.IOException;

public class Encryption {  
	private static String _version;
    private Cipher dcipher;
    
    protected Encryption(SecretKey key, String algorithm) {
        try {           
            dcipher = Cipher.getInstance(algorithm);           
            dcipher.init(Cipher.DECRYPT_MODE, key);
        } catch (NoSuchPaddingException e) {
            System.out.println("EXCEPTION: NoSuchPaddingException");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("EXCEPTION: NoSuchAlgorithmException");
        } catch (InvalidKeyException e) {
            System.out.println("EXCEPTION: InvalidKeyException");
        }
    }
   
    protected Encryption(String passPhrase) {
        byte[] salt = {(byte)0xA9, (byte)0x9B, (byte)0xC8, (byte)0x32, (byte)0x56, (byte)0x34, (byte)0xE3, (byte)0x03};        
        int iterationCount = 19;
        
        try {
            KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
            
            dcipher = Cipher.getInstance(key.getAlgorithm());            
            AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);            
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("EXCEPTION: InvalidAlgorithmParameterException");
        } catch (InvalidKeySpecException e) {
            System.out.println("EXCEPTION: InvalidKeySpecException");
        } catch (NoSuchPaddingException e) {
            System.out.println("EXCEPTION: NoSuchPaddingException");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("EXCEPTION: NoSuchAlgorithmException");
        } catch (InvalidKeyException e) {
            System.out.println("EXCEPTION: InvalidKeyException");
        }
    }

    public String decrypt(String str) {
        try {
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);           
            byte[] utf8 = dcipher.doFinal(dec);            
            return new String(utf8, "UTF8");
        } catch (BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {
        } catch (IOException e) {
        }
        return null;
    }

    public static void doDecrypt() {
        
    } 
        
    public static String getVersion() {
    	try {     
    		FileConfig config = new FileConfig("data/config/version.dat");
            String passPhrase   = "s1N4rM@$v2";           
            Encryption desEncrypter = new Encryption(passPhrase);           

            _version = desEncrypter.decrypt(config.getProperty("version"));
            System.out.println("hasil : "+_version);
           
        } catch (Exception e) {
        	System.err.println(e.getMessage());
        }
    	return _version;
    }
    
}