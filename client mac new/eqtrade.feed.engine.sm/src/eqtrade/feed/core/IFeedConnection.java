package eqtrade.feed.core;

import java.rmi.RemoteException;

public interface IFeedConnection {
	public String getHistory(String param) throws Exception;

	public byte[] getFile(String param) throws Exception;

	public void subscribe(String sheader);

	public void unsubscribe(String sheader);

	public void logon(String username, String password);
	public void reconnect(final int count);
	
	public void changePassword(String oldpassword,String newpwd);
	
	public boolean logout();
	
	public String getDate() throws Exception ;
	
	public String getUserId();
	
	public long getTime();
	
	public String getPassword();
	
	public Long getSessionid();
	
	public void send(String msg);
	
	// yosep other device
	public boolean getFeedLogin();
	
	public int getCountFeedLogin();
	
	public void setCountFeedLogin(int a );
	
	public void setuserpass(String username, String password);
		// yosep other device
}
