package eqtrade.feed.engine;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.core.Utils;

public class FeedParserMultithread extends FeedParser {

	private final Log log = LogFactory.getLog(getClass());
	private ThreadPoolExecutor executorThread;
	private Hashtable<String, ParserThread> hprocess = new Hashtable<String, FeedParserMultithread.ParserThread>();
	private int waitstate = 50;

	public FeedParserMultithread(IFeedEngine engine) {
		super(engine);
		int coreThread = Runtime.getRuntime().availableProcessors();
		executorThread = new ThreadPoolExecutor(
				coreThread,
				coreThread * 3,
				5,
				TimeUnit.MILLISECONDS,
				(BlockingQueue<Runnable>) new java.util.concurrent.LinkedBlockingQueue<Runnable>());
		executorThread.prestartAllCoreThreads();
		
	}

	@Override
	public void parse(String message) {
		Vector vheader = Utils.parser(message, "|");
		Parser parser = null;
		String key = null;
		for (Iterator i = map.keySet().iterator(); i.hasNext();) {
			key = (String) i.next();
			if (key.startsWith(vheader.elementAt(1).toString())) {
				parser = ((Parser) map.get(key));
				break;
			} else if (key.equals((vheader.elementAt(0).toString()))) {
				parser = ((Parser) map.get(key));
				break;
			}
		}
		if (vheader.elementAt(0).equals("queue")) {
			parser = ((Parser) map.get(PARSER_QUEUE));
		}

		if (parser != null && key != null) {
			ParserThread processQuote = hprocess.get(key);

			if (processQuote != null && processQuote.isSuccess(message)) {
			} else {
				ParserThread processQuote2 = new ParserThread(key, parser,
						this.waitstate, (FeedParserMultithread) this);
				processQuote2.addProcess(message);
				hprocess.put(key, processQuote2);
				executorThread.execute(processQuote2);
			}
		}

	}

	public void closeThread(ParserThread thread) {
		boolean isremove = hprocess.remove(thread.getKey()) != null;

	}

	@Override
	public void start() {
		
	}

	public class ParserThread extends Thread {
		private Parser parser;
		private Vector<String> queueProcess = new Vector<String>();
		private FeedParserMultithread pool;
		private int waitingstate = 0;
		private String key;

		public ParserThread(String key, Parser parser, int waitingstate,
				FeedParserMultithread pool) {
			this.key = key;
			this.parser = parser;
			this.pool = pool;
			this.waitingstate = waitingstate;
		}

		public String getKey() {
			return key;
		}

		@Override
		public void run() {
			Vector vtemp;
			while (!queueProcess.isEmpty()) {

				// String sdata = queueProcess.get(0);
				vtemp = (Vector) queueProcess.clone();
				
				process(vtemp);
				synchronized (queueProcess) {
					if (queueProcess.size() == 1) {

						try {
							queueProcess.wait(waitingstate);
						} catch (InterruptedException e) {
							e.printStackTrace();
							log.error("error message " + e.getMessage());
						}
					}

					//queueProcess.remove(0);
					queueProcess.removeAll(vtemp);
				}

			}
			pool.closeThread(this);
			if (!queueProcess.isEmpty()) {
				log.error("ada sisa di queueprocess " + queueProcess.size());
//				System.out.println("ada sisa di queueprocess "
//						+ queueProcess.size());
			}
		}

		private void process(Vector vmsg) {
			this.parser.doProcess(vmsg);
		}

		public boolean isSuccess(String sdata) {
			synchronized (queueProcess) {
				if (!queueProcess.isEmpty()) {
					queueProcess.add(sdata);
					queueProcess.notifyAll();
					return true;
				} else
					return false;
			}
		}

		public void addProcess(String data) {
			synchronized (queueProcess) {

				queueProcess.add(data);
				queueProcess.notify();
			}
		}

	}

}
