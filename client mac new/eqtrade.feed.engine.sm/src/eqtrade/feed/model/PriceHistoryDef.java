package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class PriceHistoryDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(100));
        htable.put("sortascending", new Boolean(false));
        return htable;
	}
	public static String[] dataHeader = new String[]{
		"Date","Open","High","Low","Close","Prev","Volume","Freq"};

   public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER
   };

	public  static int[] defaultColumnWidth = new int[]{
			50,50,50,50,50,50,50,50
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6,7
	};	

	public static boolean[] columnsort = new boolean[]{
			false, false, false,false, false, false,false,false
	};
	
   public static Vector getHeader() {
		Vector header = new Vector(PriceHistory.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
   public static List createTableRow(Row pricehistory) {
		List vec = new Vector(PriceHistory.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < PriceHistory.CIDN_NUMBEROFFIELDS; i++) {
			
			try {
				vec.add(i, new PriceHistoryField(pricehistory, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static TRRender render = new TRRender();
	
   static class TRRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new PriceHistoryRender();

		public TableCellRenderer getTableCellRenderer(JTable tabelPriceHistory, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(tabelPriceHistory, value,isSelected, hasFocus, row, column);
			((PriceHistoryRender)renderer).setValue(value);
			return renderer;
		}
	}
}
