package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.sun.org.apache.xml.internal.serializer.utils.Utils;
import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class StockSummaryRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
	 int    nrow = -1;
     private String strFieldName = new String("");
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	 private static NumberFormat formatter2= new DecimalFormat("#,##0.00  ");
	 private static NumberFormat formatter3= new DecimalFormat("#,##0.00");
     private static Color newBack;
     private static Color newFore;
     private static SelectedBorder border = new SelectedBorder();
	 private static  Icon upArrow = new ImageIcon(Utils.class.getResource("/eqtrade/feed/images/uparrow.gif"));
	 private static  Icon downArrow = new ImageIcon(Utils.class.getResource("/eqtrade/feed/images/downarrow.gif"));
	 private static  Icon noArrow = new ImageIcon(Utils.class.getResource("/eqtrade/feed/images/noarrow.gif"));

	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){

	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
	 	strFieldName = table.getColumnName(column);
	 	nrow = row;
        if (value instanceof MutableIData) {	        
        	StockSummary ss = (StockSummary)((MutableIData)value).getSource();
    		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
    		 if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_PREVIOUS])){
    			 newFore = FeedSetting.getColor(FeedSetting.C_ZERO);
    		 } else if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_HIGHEST])){
        			 newFore = ss.getHighest().doubleValue() > ss.getClosing().doubleValue() ? FeedSetting.getColor(FeedSetting.C_PLUS) : 
        				 					(ss.getHighest().doubleValue() < ss.getClosing().doubleValue() ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
    		 } else if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_BESTBIDVOLUME])){
    			 newFore = ss.getBestBid().doubleValue() > ss.getClosing().doubleValue() ? FeedSetting.getColor(FeedSetting.C_PLUS) : 
    				 					(ss.getBestBid().doubleValue() < ss.getClosing().doubleValue() ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
    		 } else if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_BESTOFFERVOLUME])){
    			 newFore = ss.getBestOffer().doubleValue() > ss.getClosing().doubleValue() ? FeedSetting.getColor(FeedSetting.C_PLUS) : 
    				 					(ss.getBestOffer().doubleValue() < ss.getClosing().doubleValue() ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
    		 } else if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_CHANGE]) ||
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_PERCENT])){
    			 newFore = ((Double)((MutableIData)value).getData()).doubleValue() > 0 ? FeedSetting.getColor(FeedSetting.C_PLUS) : 
    				 (((Double)((MutableIData)value).getData()).doubleValue() < 0 ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
    		 } else if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_LOWEST]) ||
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_CLOSING]) || 
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_OPENING]) ||
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_BESTBID]) ||
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_BESTBIDVOLUME]) ||
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_BESTOFFER]) ||
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_BESTOFFERVOLUME]) ||
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_AVG])){
    			 	 newFore = ((Double)((MutableIData)value).getData()).doubleValue() > ss.getPrevious().doubleValue() ? FeedSetting.getColor(FeedSetting.C_PLUS) : 
    			 		 (((Double)((MutableIData)value).getData()).doubleValue() < ss.getPrevious().doubleValue() ? FeedSetting.getColor(FeedSetting.C_MINUS) : FeedSetting.getColor(FeedSetting.C_ZERO));
    		 } else if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_TRADEDFREQUENCY]) ||
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_TRADEDLOT]) || 
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_TRADEDVALUE]) || 
    				 strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_TRADEDVOLUME])){
        			 newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);
    		 } else {
    			 newFore = StockRender.getSectorColor(ss.getCode());    			 
    		 }
        }
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);   	 	
	 	return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
            	 if (dat instanceof Double) {
            		 if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_PERCENT])){
		            	 double val = ((Double)dat).doubleValue();
	            		 setText(formatter2.format(dat));
	            		 setIcon(val>0 ? upArrow : (val<0? downArrow : noArrow));
	            		 setIconTextGap(2);
	            		 setHorizontalTextPosition(LEFT);
	            	 } else {
	            		 setIcon(null);
		            	 double val = ((Double)dat).doubleValue();
		            	 if (Math.abs(val)>1000000000){
			                 setText(formatter3.format(val / 1000000000)+"B  ");	            		 
		            		 
		            	 } else if (Math.abs(val)>1000000){
			                 setText(formatter3.format(val / 1000000)+"M  ");	            		 
		            	 } else {
			                 setText(formatter.format(dat));	            		 	            		 
		            	 }
	            	 }
	             } else if (dat == null){
	                 setText("");
            		 setIcon(null);
	             } else {
            		 setIcon(null);
	            	 if (strFieldName.equals(StockSummaryDef.dataHeader[StockSummary.CIDX_SEQNO])){
	                     setHorizontalAlignment(JLabel.RIGHT);
	            		 setText(formatter.format(nrow+1));	            	 
	            	 } else  
	            		 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
