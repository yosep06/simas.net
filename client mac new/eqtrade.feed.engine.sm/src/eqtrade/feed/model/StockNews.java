package eqtrade.feed.model;

public final class StockNews extends Model {
	private static final long serialVersionUID = 1681132210428041847L;
	public static int CIDX_NEWSID = 3;
	public static int CIDX_NEWDATE = 4;
	public static int CIDX_NEWSSTOCK = 5;
	public static int CIDX_NEWSTITLE =6;
	public static int CIDX_CONTENT=7;
	public static final int 	CIDN_NUMBEROFFIELDS = 8;
	
	public StockNews() {
		super(CIDN_NUMBEROFFIELDS);
		
	}
	public StockNews(String smsg) {
		super(CIDN_NUMBEROFFIELDS, smsg);
		
	}

	@Override
	protected void convertType() {
		// TODO Auto-generated method stub
	}

	public String getNewsid() {
		return (String) getData(CIDX_NEWSID);
	}

	public void setNewsid(String sinput) {
		setData(sinput,CIDX_NEWSID);
	}

	public String getNewsdate() {
		return (String)getData(CIDX_NEWDATE);
	}

	public void setNewsdate(String sinput) {
		setData(sinput,CIDX_NEWDATE);
	}

	public String getNewsstock() {
		return (String) getData(CIDX_NEWSSTOCK);
	}

	public void setNewsstock(String sinput) {
		setData(sinput,CIDX_NEWSSTOCK);
	}

	public String getNewstitle() {
		return (String)getData(CIDX_NEWSTITLE);
	}

	public void setNewstitle(String sinput) {
		setData(sinput, CIDX_NEWSTITLE);
	}
	public String getContent() {
		return (String)getData(CIDX_CONTENT);
	}
	public void setContent(String sinput) {
		setData(sinput,CIDX_CONTENT);
	
	}
	
}
