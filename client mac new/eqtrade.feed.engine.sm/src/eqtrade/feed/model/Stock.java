package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public final class Stock extends Model implements Comparable {
	private static final long serialVersionUID = 9034606696294087003L;
	public static final int	CIDX_CODE = 3;
	public static final int	CIDX_NAME = 4;
	public static final int	CIDX_STATUS = 5;
	public static final int	CIDX_STOCKTYPE = 6;

	public static final int	CIDX_SECTOR = 7;
	public static final int	CIDX_IPOPRICE = 8;
	public static final int	CIDX_BASEPRICE = 9;
	public static final int	CIDX_LISTEDSHARES = 10;

	public static final int CIDX_TRADELISTEDSHARES = 11;
	public static final int CIDX_SHARESPERLOT = 12;
	public static final int CIDN_NUMBEROFFIELDS = 14;
	public static final int CIDX_REMARKS = 13;

	public Stock(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Stock(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 8: case 9: case 10: case 11: case 12:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}
	//remark for ORI
	public String getRemarks(){
		 return (String) getData(CIDX_REMARKS);
	 }
	
	public void setRemarks(String remarks){
		 setData(remarks, CIDX_REMARKS); 
	}
	//End of Remark

	public String getCode(){
		return (String) getData(CIDX_CODE);
	}
	
	public void setActiontype(String sinput){
		setData(sinput, CIDX_CODE);
	}

	public String getName(){
		return (String) getData(CIDX_NAME);
	}
	
	public void setName(String sinput){
		setData(sinput, CIDX_NAME);
	}

	public String getStatus(){
		return (String) getData(CIDX_STATUS);
	}
	public void setStatus(String sinput){
		setData(sinput, CIDX_STATUS);
	}

	public String getStockType(){
		return (String) getData(CIDX_STOCKTYPE);
	}
	public void setStockType(String sinput){
		setData(sinput, CIDX_STOCKTYPE);
	}

	public String getSector(){
		return (String) getData(CIDX_SECTOR);
	}
	public void setSector(String sinput){
		setData(sinput, CIDX_SECTOR);
	}

	public Double getIpoPrice(){
		return (Double) getData(CIDX_IPOPRICE);
	}
	public void setIpoPrice(Double sinput){
		setData(sinput, CIDX_IPOPRICE);
	}

	public Double getBasePrice(){
		return (Double) getData(CIDX_BASEPRICE);
	}
	public void setBasePrice(Double sinput){
		setData(sinput, CIDX_BASEPRICE);
	}

	public String getListedShares(){
		return (String) getData(CIDX_LISTEDSHARES);
	}
	public void setListedShares(String sinput){
		setData(sinput, CIDX_LISTEDSHARES);
	}

	public String getTradeListedShares(){
		return (String) getData(CIDX_TRADELISTEDSHARES);
	}
	public void setTradeListedShares(String sinput){
		setData(sinput, CIDX_LISTEDSHARES);
	}

	public Double getSharesPerLot(){
		return (Double) getData(CIDX_SHARESPERLOT);
	}
	public void setSharesPerLot(Double sinput){
		setData(sinput, CIDX_SHARESPERLOT);
	}

	public int compareTo(Object otemp){
		Stock dsdtmp = (Stock) otemp;
		String scode = dsdtmp.getCode();
		return getCode().compareTo(scode);
	}

}