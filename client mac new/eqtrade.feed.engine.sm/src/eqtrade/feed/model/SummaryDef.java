package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class SummaryDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(-1));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}
    public static String[] dataHeader = new String[]{
		"code", "info1", "info2", "info3", "info4", "info5", "info6", "info7", "info8", "info9", "info10s"};

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			50,100,100,100,100,100,100,100,100,100,100
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6,7,8,9,10
	};	

	public static boolean[] columnsort = new boolean[]{
			false, false, false, false, false, false, false, false, false, false, false
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(Summary.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row market) {
		List vec = new Vector(Summary.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Summary.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new SummaryField(market, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static MSummRender render = new MSummRender();
	
    static class MSummRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new SummaryRender();

		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((SummaryRender)renderer).setValue(value);
			return renderer;
		}
	}
}
