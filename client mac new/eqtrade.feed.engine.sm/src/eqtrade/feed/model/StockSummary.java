package eqtrade.feed.model;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;

public final class StockSummary extends Model  {
//	stock|board|remark|prev|high|low|close|change|tradevol|traveval|tradefreq|index
//	|foregin|open|bestbid|bestbidvol|bestoffer|bestoffervol

	private static final long serialVersionUID = 7521643761609146891L;
	public static final int		CIDX_CODE = 3;
	public static final int 	CIDX_BOARD = 4;
	public static final int 	CIDX_REMARKS = 5;
	public static final int 	CIDX_PREVIOUS = 6;

	public static final int		CIDX_HIGHEST = 7;
	public static final int 	CIDX_LOWEST = 8;
	public static final int		CIDX_CLOSING = 9;
	public static final int		CIDX_CHANGE = 10;

	public static final int		CIDX_TRADEDVOLUME = 11;
	public static final int		CIDX_TRADEDVALUE = 12;
	public static final int		CIDX_TRADEDFREQUENCY = 13;
	public static final int		CIDX_INDIVIDUALINDEX = 14;

	public static final int		CIDX_FOREIGNERS = 15;
	public static final int		CIDX_OPENING = 16;
	public static final int		CIDX_BESTBID = 17;
	public static final int		CIDX_BESTBIDVOLUME = 18;

	public static final int 	CIDX_BESTOFFER = 19;
	public static final int		CIDX_BESTOFFERVOLUME = 20;
	public static final int 	CIDX_PERCENT = 21;
	public static final int		CIDX_AVG = 22;
	public static final int		CIDX_NAME = 23;
	public static final int		CIDX_TRADEDLOT = 24;
	public static final int		CIDX_SECTOR = 25;
	public static final int		CIDN_NUMBEROFFIELDS = 26;

	public StockSummary(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public StockSummary(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			 case 6: case 7: case 8: case 9: case 10: case 11: case 12: case 13: case 14: case 15: case 16:
			case 17: case 18: case 19: case 20: case 21: case 22:  case 24:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
		calculate();
		
	    //tambahan ngubah dari vol ke lot
		//by Kvn & Rymnz 20100830
        setBestBidVolume(new Double(getBestBidVolume().doubleValue() / Double.parseDouble(FeedSetting.getLot())));
        setBestOfferVolume(new Double(getBestOfferVolume().doubleValue() / Double.parseDouble(FeedSetting.getLot())));

	}
    
    private void calculate(){
        setData(getClosing(), CIDX_CLOSING);
        
        double nlast = getClosing().doubleValue();
        double    nprev = getPrevious().doubleValue();
        double nchange = nlast - nprev;
        if (nlast==0) {
            setChange(new Double(0));
            setPercent(new Double(0));
        } else {
            setChange(new Double(nchange));
            if  (nprev != 0){
                double  persen = (nchange * 100) / nprev;
                setPercent(new Double(persen));
            } else {
                setPercent(new Double(0));
            }
        }
        
        double avg = getTradedVolume().doubleValue() == 0 ? 0 : getTradedValue().doubleValue() / getTradedVolume().doubleValue();
        setAvg(new Double(avg));
        setTradedLot( new Double(getTradedVolume().doubleValue() / Double.parseDouble(FeedSetting.getLot())));
   }


	public String getCode(){
		return (String) getData(CIDX_CODE);
	}
	public void setCode(String sinput){
		setData(sinput, CIDX_CODE);
	}

	public String getName(){
		return (String) getData(CIDX_NAME);
	}
	public void setName(String sinput){
		setData(sinput, CIDX_NAME);
	}

	public String getBoard(){
		return (String) getData(CIDX_BOARD);
	}
	public void setBoard(String sinput){
		setData(sinput, CIDX_BOARD);
	}

	public String getRemarks(){
		return (String) getData(CIDX_REMARKS);
	}
	public void setRemarks(String sinput){
		setData(sinput, CIDX_REMARKS);
	}

	public Double getPrevious(){
		return (Double) getData(CIDX_PREVIOUS);
	}
	public void setPrevious(Double sinput){
		setData(sinput, CIDX_PREVIOUS);
	}

	public Double getPercent(){
		return (Double) getData(CIDX_PERCENT);
	}
	public void setPercent(Double sinput){
		setData(sinput, CIDX_PERCENT);
	}

	public Double getHighest(){
		return (Double) getData(CIDX_HIGHEST);
	}
	public void setHighest(Double sinput){
		setData(sinput, CIDX_HIGHEST);
	}

	public Double getLowest(){
		return (Double) getData(CIDX_LOWEST);
	}
	public void setLowest(Double sinput){
		setData(sinput, CIDX_LOWEST);
	}

	public Double getClosing(){
		return (Double) getData(CIDX_CLOSING);
	}
	public void setClosing(Double sinput){
		setData(sinput, CIDX_CLOSING);
		calculate();
	}

	public Double getChange(){
		return (Double) getData(CIDX_CHANGE);
	}
	public void setChange(Double sinput){
		setData(sinput, CIDX_CHANGE);
	}

	public Double getTradedVolume(){
		return (Double) getData(CIDX_TRADEDVOLUME);
	}
	public void setTradedVolume(Double sinput){
		setData(sinput, CIDX_TRADEDVOLUME);
	}

	public Double getTradedLot(){
		return (Double) getData(CIDX_TRADEDLOT);
	}
	public void setTradedLot(Double sinput){
		setData(sinput, CIDX_TRADEDLOT);
	}

	public Double getTradedValue(){
		return (Double) getData(CIDX_TRADEDVALUE);
	}
	public void setTradedValue(Double sinput){
		setData(sinput, CIDX_TRADEDVALUE);
	}

	public Double getTradedFrequency(){
		return (Double) getData(CIDX_TRADEDFREQUENCY);
	}
	public void setTradedFrequency(Double sinput){
		setData(sinput, CIDX_TRADEDFREQUENCY);
	}

	public Double getIndividualIndex(){
		return (Double) getData(CIDX_INDIVIDUALINDEX);
	}
	public void setIndividualIndex(Double sinput){
		setData(sinput, CIDX_INDIVIDUALINDEX);
	}

	public Double getForeigners(){
		return (Double) getData(CIDX_FOREIGNERS);
	}
	public void setForeigners(Double sinput){
		setData(sinput, CIDX_FOREIGNERS);
	}

	public Double getOpening(){
		return (Double) getData(CIDX_OPENING);
	}
	public void setOpening(Double sinput){
		setData(sinput, CIDX_OPENING);
	}

	public Double getBestBid(){
		return (Double) getData(CIDX_BESTBID);
	}
	public void setBestBid(Double sinput){
		setData(sinput, CIDX_BESTBID);
	}

	public Double getBestBidVolume(){
		return (Double) getData(CIDX_BESTBIDVOLUME);
	}
	
	public void setBestBidVolume(Double sinput){
		setData(sinput, CIDX_BESTBIDVOLUME);
	}

	public Double getBestOffer(){
		return (Double) getData(CIDX_BESTOFFER);
	}
	public void setBestOffer(Double sinput){
		setData(sinput, CIDX_BESTOFFER);
	}

	public Double getBestOfferVolume(){
		return (Double) getData(CIDX_BESTOFFERVOLUME);
	}
	
	public void setBestOfferVolume(Double sinput){
		setData(sinput, CIDX_BESTOFFERVOLUME);
	}
	
	public Double getAvg(){
		return (Double) getData(CIDX_AVG);
	}
	public void setAvg(Double sinput){
		setData(sinput, CIDX_AVG);
	}
	
	public String getSector() {
		return (String) getData(CIDX_SECTOR);
	}
	
	public void setSector(String sinput) {
		setData(sinput,CIDX_SECTOR);
	}

	public int compareTo(Object otemp){
		StockSummary dtstmp = (StockSummary) otemp;
		String scode = dtstmp.getCode();
		String sboard = dtstmp.getBoard();
		String skeytmp = scode + "-" + sboard;
		String skey = getCode() + "-" + getBoard();
		return skey.compareTo(skeytmp);
	}
}
