package eqtrade.feed.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class QuoteDef {
	public static Hashtable getTableVerDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeaderVer.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidthVer.clone());
        htable.put("order", defaultColumnOrderVer.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(Quote.CIDX_NUMBER));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}
	public static Hashtable getTableBidDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeaderBid);
        htable.put("alignment", defaultHeaderAlignment);
        htable.put("width", defaultColumnWidthBid);
        htable.put("order", defaultColumnOrderBid);
        htable.put("sorted", columnsort);        
        htable.put("sortcolumn",new Integer(Quote.CIDX_NUMBER));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}
	public static Hashtable getTableOffDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeaderOff);
        htable.put("alignment", defaultHeaderAlignment);
        htable.put("width", defaultColumnWidthOff);
        htable.put("order", defaultColumnOrderOff);
        htable.put("sorted", columnsort);        
        htable.put("sortcolumn",new Integer(Quote.CIDX_NUMBER));
        htable.put("sortascending", new Boolean(false));
        return htable;
	}
	//stock|board|side|number|price|lot|freq|prev
    public static String[] dataHeaderVer = new String[]{"Stock", "Board", "Side", "Number", "Off/Bid", "Lot", "#", "Prev"};
    public static String[] dataHeaderOff = new String[]{"Stock", "Board", "Side", "Number", "Offer", "Lot", "#O", "Prev"};
    public static String[] dataHeaderBid = new String[]{"Stock", "Board", "Side", "Number", "Bid", "Lot", "#B", "Prev"};

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER,SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidthVer = new int[]{0,0,0,0,37,32,25,0};
	public  static int[] defaultColumnOrderVer = new int[]{0,1,2,3,4,5,6,7};	

	public  static int[] defaultColumnWidthOff = new int[]{0,0,0,0,37,32,25,0};
	public  static int[] defaultColumnOrderOff = new int[]{0,1,2,3,4,5,6,7};	

	public  static int[] defaultColumnWidthBid = new int[]{25,32,37,0,0,0,0,0};
	public  static int[] defaultColumnOrderBid = new int[]{6,5,4,0,1,2,3,7};	

	public static boolean[] columnsort = new boolean[]{
			false,false,false,false,false,false, false, false
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(Quote.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeaderBid.length; i++) {
			header.addElement(dataHeaderBid[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row quote) {
		List vec = new Vector(Quote.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Quote.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new QuoteField(quote, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static OQRender render = new OQRender();
	
    static class OQRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new QuoteRender();

		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((QuoteRender)renderer).setValue(value);
			return renderer;
		}
	}
}
