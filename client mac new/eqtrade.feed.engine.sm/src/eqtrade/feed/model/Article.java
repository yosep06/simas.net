package eqtrade.feed.model;


public final class Article extends Model {
	private static final long serialVersionUID = 1681132210428041847L;
	public static final int  CIDX_ID = 3;
	//public static final int CIDX_SOURCE=3;
	public static final int	CIDX_DATE = 4; 
    public static final int  CIDX_TITLE = 5;
    public static final int	CIDX_SUBJECT = 6;
    public static final int  CIDX_CONTENT = 7;
    //public static final int CIDX_LINK = 8;
    public static final int 	CIDN_NUMBEROFFIELDS = 8;

	public Article(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Article(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}

	protected void convertType() {
	}
    
    public String getTitle(){
        return (String) getData(CIDX_TITLE);
    }
    public void setTitle(String sinput){
        setData(sinput, CIDX_TITLE);
    }
    
    public String getDate(){
        return (String) getData(CIDX_DATE);
    }
    public void setDate(String sinput){
        setData(sinput, CIDX_DATE);
    }
    
   
    
    public String getId(){
		return (String) getData(CIDX_ID);
	}
	public void setId(String sinput){
		setData(sinput, CIDX_ID);
	}
	
	public String getSubject(){
		return (String) getData(CIDX_SUBJECT);
	}
	public void setSubject(String sinput){
		setData(sinput, CIDX_SUBJECT);
	}

	public String getContent(){
		return (String) getData(CIDX_CONTENT);
	}
	public void setContent(String sinput){
		setData(sinput, CIDX_CONTENT);
	}
}