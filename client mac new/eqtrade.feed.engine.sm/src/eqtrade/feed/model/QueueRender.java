package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;


public class QueueRender extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 213203526059607433L;
	private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	private String strFieldName = new String("");

	private static Color newBack;
	private static Color newFore;
	private boolean highlight = false;
	private static SelectedBorder border = new SelectedBorder();
	
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		Component component = super.getTableCellRendererComponent(table, value,
				isSelected, hasFocus, row, column);
		strFieldName = table.getColumnName(column);
//		try {
			newBack = (column % 2 )==0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND):FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
			if(value instanceof MutableIData){
				MutableIData arg = (MutableIData)value;
				Queue qu = (Queue)arg.getSource();
				String cp = qu.getState();
				/*if(strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_STOCK]) || 
						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_BOARD])||
						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_LOT])||
						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_ID])||
						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_ORDERID])||
						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_STATE])||
						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_TYPE])){
					newFore= FeedSetting.getColor(FeedSetting.C_FOREGROUND);
				}else{*/
					if(cp.equals("kuning")){
					newFore = FeedSetting.getColor(FeedSetting.C_ZERO);
					//System.out.println("Pilih="+qu.getState());
					}else if (cp.equals("merah")){
						newFore = FeedSetting.getColor(FeedSetting.C_MINUS);
						//System.out.println("Pilih="+qu.getState());
					}else if(cp.equals("hijau")){
						newFore = FeedSetting.getColor(FeedSetting.C_PLUS);
						//System.out.println("Pilih="+qu.getState());
					}else{
						newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);
					}
				}

			if(isSelected){
				((JLabel)component).setBorder(border);
			}

// mark the row in Best Quote Queue compared to Order List
			if(Queue.getMarketOrderIDList().size() > 0){
				for (int counter = 0; counter < Queue.getMarketOrderIDList().size(); counter++){
					if (table.getValueAt(row, 6).toString().equals("000"+Queue.getMarketOrderIDList().get(counter))){
							component.setBackground(new Color(255,255,0));
							component.setForeground(new Color(86,86,0));
							counter = Queue.getMarketOrderIDList().size();
					}else{
							component.setBackground(newBack);
							component.setForeground(newFore);
						}
					}
			}else{
				component.setBackground(newBack);
				component.setForeground(newFore);
			}
//////////////////

//		} catch (Exception e) {
////			System.out.println(e.getMessage());
//			throw new IllegalArgumentException();
//		}
		return component;
	}

	public void setValue(Object value) {
		try {
			if (value instanceof MutableIData) {
				MutableIData args = (MutableIData) value;
				Object dat = args.getData();
				setHorizontalAlignment((dat instanceof Double) ? JLabel.RIGHT
						: JLabel.LEFT);
				if (dat instanceof Double) {
					setText((((Double) dat).doubleValue() == 0) ? ""
							: formatter.format(dat));
				} else if (dat == null) {
					setText("");
				} else {
					setText(" " + dat.toString());
				}
			}
		} catch (Exception e) {
		}
	}
}


//package eqtrade.feed.model;
//
//import java.awt.Color;
//import java.awt.Component;
//import java.text.DecimalFormat;
//import java.text.NumberFormat;
//
//import javax.swing.JLabel;
//import javax.swing.JTable;
//import javax.swing.table.DefaultTableCellRenderer;
//
//import com.vollux.idata.indirection.MutableIData;
//
//import eqtrade.feed.core.FeedSetting;
//import eqtrade.feed.core.SelectedBorder;
//
//public class QueueRender extends DefaultTableCellRenderer {
//	private static final long serialVersionUID = 213203526059607433L;
//	private static NumberFormat formatter = new DecimalFormat("#,##0  ");
//	private String strFieldName = new String("");
//
//	private static Color newBack;
//	private static Color newFore;
//	private boolean highlight = false;
//	private static SelectedBorder border = new SelectedBorder();
//
//	public Component getTableCellRendererComponent(JTable table, Object value,
//			boolean isSelected, boolean hasFocus, int row, int column) {
//		Component component = super.getTableCellRendererComponent(table, value,
//				isSelected, hasFocus, row, column);
//		strFieldName = table.getColumnName(column);
//
//		try {
//			newBack = (column % 2 )==0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND):FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
//			if(value instanceof MutableIData){
//				MutableIData arg = (MutableIData)value;
//				Queue qu = (Queue)arg.getSource();
//				String cp = qu.getState(); 
//				/*if(strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_STOCK]) || 
//						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_BOARD])||
//						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_LOT])||
//						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_ID])||
//						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_ORDERID])||
//						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_STATE])||
//						strFieldName.equals(QueueDef.dataHeaderBid[Queue.CIDX_TYPE])){
//					newFore= FeedSetting.getColor(FeedSetting.C_FOREGROUND);
//				}else{*/
//					if(cp.equals("kuning")){
//						newFore = FeedSetting.getColor(FeedSetting.C_ZERO);
//					}else if (cp.equals("merah")){
//						newFore = FeedSetting.getColor(FeedSetting.C_MINUS);
//					}else if(cp.equals("hijau")){
//						newFore = FeedSetting.getColor(FeedSetting.C_PLUS);
//					}else{
//						newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);
//					}
//				}
//			if(isSelected){
//				((JLabel)component).setBorder(border);
//			}
//
//// avaldano : Color the row
//			for (int counter = 0; counter < Queue.getMarketOrderIDList().size(); counter++){
//				if (table.getValueAt(row, 6).toString().equals("000"+Queue.getMarketOrderIDList().get(counter))){
//					Color flagBack	= new Color(255,255,0);
//					component.setBackground(flagBack);
//					counter = Queue.getMarketOrderIDList().size();
//				}
//				else{
//					component.setBackground(newBack);
//				}
//				component.setForeground(newFore);
//			}
///////////////////		
//		} catch (Exception e) {
//			System.out.println("Salah");
//		}
//		return component;
//	}
//
//	public void setValue(Object value) {
//		try {
//			if (value instanceof MutableIData) {
//				MutableIData args = (MutableIData) value;
//				Object dat = args.getData();
//				setHorizontalAlignment((dat instanceof Double) ? JLabel.RIGHT
//						: JLabel.LEFT);
//				if (dat instanceof Double) {
//					setText((((Double) dat).doubleValue() == 0) ? ""
//							: formatter.format(dat));
//				} else if (dat == null) {
//					setText("");
//				} else {
//					setText(" " + dat.toString());
//				}
//			}
//		} catch (Exception e) {
//		}
//	}
//}
