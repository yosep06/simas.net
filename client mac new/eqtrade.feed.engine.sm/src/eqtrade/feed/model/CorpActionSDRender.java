package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class CorpActionSDRender  extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
     private static Color newBack;
     private static Color newFore;
     private static SelectedBorder border = new SelectedBorder();
     private String strFieldName = new String("");


	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
		strFieldName = table.getColumnName(column);
	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        if (value instanceof MutableIData) {	        
    		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
    		 if (strFieldName.equals(CorpActionSDDef.dataHeader[CorpActionSD.CIDX_STOCK])){
    			newFore = StockRender.getSectorColor(((MutableIData)value).getData().toString()); 
    		 } else {
    			 newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);           	
    		 }
        }
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);   
	 	return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
	             Calendar cal = Calendar.getInstance();
                 
                 String DATE_FORMAT_NOW = "yyyy-MM-dd";
                 Log log = LogFactory.getLog(getClass());
                 
                 Calendar current = Calendar.getInstance();
                 SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                 sdf.format(current.getTime());
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
	             if (dat instanceof Double) {
	                 setText(dat.toString());
	             }  else {
	            	 if(strFieldName.equals(CorpActionDef.dataHeader[CorpAction.CIDX_STOCK])){
	            		 CorpAction corp=(CorpAction)args.getSource();
		            	 String tgl=corp.getCumdate();
		            	 Calendar now = Calendar.getInstance();
			             now.set(Calendar.MONTH, now.get(Calendar.MONTH) + 1);
		            	 if(!tgl.equals("null") && !tgl.equals("0")) {
		            		 Date dt = new Date();
		            		 cal.set(Calendar.YEAR, Integer.parseInt(tgl.substring(0,4)));
			            	 cal.set(Calendar.MONTH, Integer.parseInt(tgl.substring(5,7)));
			            	 cal.set(Calendar.DATE,Integer.parseInt(tgl.substring(8,10)));
			            	 
			            	 if(cal.before(now)) {
			            		 setText(" "+dat.toString()+"*");
		            		  } else {
		            			  setText(" "+dat.toString());
		            		  }
		            	 } else {
		            		 setText(" "+dat.toString());
		            	 }
		                 String val = ((String)dat).toString();
	            	 } else if(strFieldName.equals(CorpActionDef.dataHeader[CorpAction.CIDX_AMOUNT])) {
	            		 BigDecimal bd = new BigDecimal(dat.toString());
	            		 DecimalFormat df = new DecimalFormat("0.####");
	            		 setText(" " + df.format(bd));
	            	 } else {
	            		 setText(" "+dat.toString());
	            	 }
	             }
	         }
	     } catch (Exception e){
	    	 e.printStackTrace();
	     }
	 }
}
