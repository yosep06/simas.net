package eqtrade.feed.model;

public final class Board extends Model {
	private static final long serialVersionUID = 5417016306669964378L;
	public static final int		CIDX_CODE = 0;
	public static final int		CIDX_NAME = 1;
	public static final int 	CIDN_NUMBEROFFIELDS = 2;

	public Board (){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Board (String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	protected void convertType() {
	}

	public String getCode(){
		return (String) getData(CIDX_CODE);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_CODE);
	}

	public String getName(){
		return (String) getData(CIDX_NAME);
	}
	public void setName(String sinput){
		setData(sinput, CIDX_NAME);
	}
}