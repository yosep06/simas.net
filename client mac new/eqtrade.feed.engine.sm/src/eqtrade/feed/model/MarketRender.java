package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class MarketRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     //private String strFieldName = new String("");
     private static NumberFormat formatter = new DecimalFormat("#,##0  ");
     private static NumberFormat formatter2 = new DecimalFormat("#,##0.00");
     private static Color newBack;
     private static Color newFore;
     private static SelectedBorder border = new SelectedBorder();


	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){

            Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
            //strFieldName = table.getColumnName(column);
    		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
	        if (row==3){
                newFore = FeedSetting.getColor(FeedSetting.C_ZERO);   
	            ((JLabel)component).setBorder(border);	        	 
	        } else if (row == 7){
	            if (value instanceof MutableIData) {
	            	if (((MutableIData)value).getData() instanceof Double){
		                double val = ((Double)((MutableIData)value).getData()).doubleValue();
			            if (val>0) {
			                newFore = FeedSetting.getColor(FeedSetting.C_PLUS);   
			            } else if (val<0) {
			                newFore = FeedSetting.getColor(FeedSetting.C_MINUS);   
			            } else {
			                newFore = FeedSetting.getColor(FeedSetting.C_ZERO);                    
			            }
	            	}
	            }
	        } else {
	            newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   	        	
	        }
	        if (isSelected) {
	            ((JLabel)component).setBorder(null);
	        }
	        
            component.setBackground(newBack);
            component.setForeground(newFore);   
            return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 Market m = (Market)((MutableIData)value).getSource();
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
	             if (dat instanceof Double) {
	            	 if ((m.getBoard().equals("SPACE"))){
	            		 setText("");
	            	 } else {
			                 double val = ((Double)dat).doubleValue();
			            	 if (Math.abs(val)>1000000000){
				                 setText(formatter2.format(val / 1000000000)+"B  ");	            		 			            		 
			            	 } else if (Math.abs(val)>1000000){
				                 setText(formatter2.format(val / 1000000)+"M  ");	            		 
			            	 } else {
				                 setText(formatter.format(dat));	            		 	            		 
			            	 }
	            	 }
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	 setText(" "+MarketDef.boardMap.get(dat.toString()));
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
