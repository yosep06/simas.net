package eqtrade.feed.model;

import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class TradeSummaryField extends Column implements MutableIDisplayIData {
    public TradeSummaryField(Row source, int idx) throws Exception{
        super(source, idx);
    }
    
    public MutableIDisplay getMutableIDisplay() {
		return TradeSummaryDef.render;
	}

	public ImmutableIDisplay getImmutableIDisplay() {
		return TradeSummaryDef.render;
	}
}
