package eqtrade.feed.model;

import eqtrade.feed.core.Utils;

public final class Indices extends Model implements Comparable{
//	code|basevalue|marketvalue|index|open|high|low|previous
	private static final long serialVersionUID = 7911973356856075647L;
	public static final int		CIDX_CODE = 3;
	public static final int		CIDX_BASEVALUE = 4;
	public static final int		CIDX_MARKETVALUE = 5;
	public static final int		CIDX_LAST = 6;
	public static final int		CIDX_OPEN = 7;
	public static final int		CIDX_HIGH = 8;
	public static final int		CIDX_LOW = 9;
	public static final int 		CIDX_PREV = 10;
	public static final int		CIDX_TRANSTIME = 11;
	public static final int 		CIDX_CHANGE = 12;
	public static final int		CIDX_PERCENT = 13;
	public static final int 		CIDN_NUMBEROFFIELDS = 15;
	

	public Indices(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Indices(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 4: case 5: case 6: case 7: case 8:
			case 9: case 10: case 12: case 13: case 14:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
		calculate();
	}
	
	private void calculate(){
        double nLast = getLast().doubleValue();
        double nPrev = getPrev().doubleValue();
        nPrev  = nPrev==0 ? nLast : nPrev;
        double change = nLast - nPrev;
        setChange(new Double(change));
        setPercent(new Double(((nLast - nPrev) / nPrev)*100));
	}
	
	public String getCode(){
		return (String) getData(CIDX_CODE);
	}
	public void setActiontype(String sinput){
		setData(sinput, CIDX_CODE);
	}

	public String getTranstime(){
		return (String) getData(CIDX_TRANSTIME);
	}
	public void setTranstime(String sinput){
		setData(sinput, CIDX_TRANSTIME);
	}

	public Double getBaseValue(){
		return (Double) getData(CIDX_BASEVALUE);
	}
	public void setBaseValue(Double sinput){
		setData(sinput, CIDX_BASEVALUE);
	}

	public Double getMarketValue(){
		return (Double) getData(CIDX_MARKETVALUE);
	}
	public void setMarketValue(Double sinput){
		setData(sinput, CIDX_MARKETVALUE);
	}

	public Double getLast(){
		return (Double) getData(CIDX_LAST);
	}
	public void setLast(Double sinput){
		setData(sinput, CIDX_LAST);
		calculate();
	}
	public Double getOpen(){
		return (Double) getData(CIDX_OPEN);
	}
	public void setOpen(Double sinput){
		setData(sinput, CIDX_OPEN);
	}
	public Double getHigh(){
		return (Double) getData(CIDX_HIGH);
	}
	public void setHigh(Double sinput){
		setData(sinput, CIDX_HIGH);
	}
	public Double getLow(){
		return (Double) getData(CIDX_LOW);
	}
	public void setLow(Double sinput){
		setData(sinput, CIDX_LOW);
	}

	public Double getPrev(){
		return (Double) getData(CIDX_PREV);
	}
	public void setPrev(Double sinput){
		setData(sinput, CIDX_PREV);
		calculate();
	}
	
	public Double getChange(){
		return (Double) getData(CIDX_CHANGE);
	}
	public void setChange(Double sinput){
		setData(sinput, CIDX_CHANGE);
	}

	public Double getPercent(){
		return (Double) getData(CIDX_PERCENT);
	}
	public void setPercent(Double sinput){
		setData(sinput, CIDX_PERCENT);
	}

	public int compareTo(Object otemp){
		Indices ditemp = (Indices) otemp;
		String stemp = ditemp.getCode();
		String ssrc = getCode();

		if (stemp.equalsIgnoreCase("composite")) stemp = "0" + stemp;
		if (stemp.equalsIgnoreCase("lq45")) stemp = "1" + stemp;

		if (ssrc.equalsIgnoreCase("composite")) ssrc = "0" + ssrc;
		if (ssrc.equalsIgnoreCase("lq45")) ssrc = "1" + ssrc;

		return ssrc.compareTo(stemp);
	}	
}