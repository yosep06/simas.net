package eqtrade.feed.model;

import javax.swing.DefaultComboBoxModel;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import com.vollux.idata.Column;

public class ComboModel extends DefaultComboBoxModel implements TableModelListener{
	private static final long serialVersionUID = -50571507940903154L;
	private Object selectedItem;
	private TableModel model;
	private int fieldNo;

	public ComboModel(TableModel model, int fieldNo) {
		this.model = model;
		this.fieldNo = fieldNo;
		
	}

	public void tableChanged(TableModelEvent e){
		fireContentsChanged(this, -1, -1);
	}


	public Object getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(Object newValue) {
		selectedItem = newValue;
		fireContentsChanged(this, -1, -1);
	}

	public int getSize() {
		return model.getRowCount();
	}

	public Object getElementAt(int i) {
		try {
			Column f = (Column) model.getValueAt(i, fieldNo);
			return f.getData();
		} catch (Exception ex){
			return null;
		}
	}
}
