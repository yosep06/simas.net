package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class TradeSummaryRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     private String strFieldName = new String("");
     private int nrow;
	 private static NumberFormat formatter = new DecimalFormat("#,##0");
	 private static NumberFormat formatter2 = new DecimalFormat("#,##0.00");
     private static Color newBack;
     private static Color newFore;
     private static SelectedBorder border = new SelectedBorder();

	 public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){

	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        strFieldName = table.getColumnName(column);
        nrow = row;
        if (value instanceof MutableIData) {	        
    		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
		 	 TradeSummary ts = (TradeSummary)((MutableIData)value).getSource();
    		 if (strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_STOCK]) ||
    				 strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_STOCKNAME])){
    			newFore = StockRender.getSectorColor(ts.getStock()); 
    		 } else if (strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_BROKER])||
    				 strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_BROKERNAME])){
    			 	newFore = BrokerRender.getColor(ts.getBroker());
    		 } else if (strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_INVESTOR])){
    			 	 newFore = ts.getInvestor().equals("F") ? FeedSetting.getColor(FeedSetting.C_FOREIGN) : FeedSetting.getColor(FeedSetting.C_DOMESTIC);
    		 } else if (strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_BUYAVG]) ||
    				 strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_BUYVOL]) ||
    				 strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_BUYVAL]) ||
    				 strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_BUYFREQ]) ||
    				 strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_BUYLOT])){
    			 	newFore = FeedSetting.getColor(FeedSetting.C_PLUS);
    		 } else if (strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_SELLAVG]) ||
    				 strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_SELLVOL]) ||
    				 strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_SELLVAL]) ||
    				 strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_SELLFREQ]) ||
    				 strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_SELLLOT])){
    			 	newFore = FeedSetting.getColor(FeedSetting.C_MINUS);    			 	
    		 } else if (strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_VALNET])) {
    			 newFore =  (ts.getBuyval().doubleValue() > ts.getSellval().doubleValue()) ? FeedSetting.getColor(FeedSetting.C_PLUS) : FeedSetting.getColor(FeedSetting.C_MINUS);
    		 } else {
    			 newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);           	
    		 }
        }       
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);   
	 	return component;
    }
	
	 public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT : JLabel.LEFT);
	             if (dat instanceof Double) {
	            	 double val = ((Double)dat).doubleValue();
	            	 if (Math.abs(val)>1000000000){
		                 setText(formatter2.format(val / 1000000000)+"B  ");	            		 
	            		 
	            	 } else if (Math.abs(val)>1000000){
		                 setText(formatter2.format(val / 1000000)+"M  ");	            		 
	            	 } else {
		                 setText(formatter.format(dat)+"  ");	            		 	            		 
	            	 }
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	 if (strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_INVESTOR])){
	            		 setText((dat.toString().equals("F") ? " Foreign" : " Domestic"));
	            	 } else {
		            	 if (strFieldName.equals(TradeSummaryDef.dataHeader[TradeSummary.CIDX_SEQNO])){
		                     setHorizontalAlignment(JLabel.RIGHT);
		            		 setText(formatter.format(nrow+1));	            	 
		            	 } else  
		            		 setText(" "+dat.toString());
	            	 }
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
