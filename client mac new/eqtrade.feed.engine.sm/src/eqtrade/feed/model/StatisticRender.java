package eqtrade.feed.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.SelectedBorder;

public class StatisticRender extends DefaultTableCellRenderer {
	private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	private static NumberFormat formatter3= new DecimalFormat("#,##0.00");
	private String strFieldName = new String("");
	int    nrow = -1;
	private static Color newBack;
	private static Color newFore;
	private static SelectedBorder border = new SelectedBorder();
	
	public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column){
		
		Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
        
		newBack =  (column % 2) == 0 ? FeedSetting.getColor(FeedSetting.C_BACKGROUND) : FeedSetting.getColor(FeedSetting.C_BACKGROUNDEVEN);
        newFore = FeedSetting.getColor(FeedSetting.C_FOREGROUND);   
        if (isSelected) {
            ((JLabel)component).setBorder(border);
        }
        component.setBackground(newBack);
        component.setForeground(newFore);  
	 	return component;
}
	
	public void setValue(Object value){
		try{
			/*if(value instanceof MutableIData){
				MutableIData args = (MutableIData)value;
				Object dat = args.getData();
				setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT:JLabel.LEFT);
				if(dat instanceof Double){
					setText((((Double)dat).doubleValue()==0)?"":formatter.format(dat));
					 setIcon(null);
	            	 double val = ((Double)dat).doubleValue();
	            	 if (Math.abs(val)>1000000000){
		                 setText(formatter3.format(val / 1000000000)+"B  ");	            		 
	            		 
	            	 } else if (Math.abs(val)>1000000){
		                 setText(formatter3.format(val / 1000000)+"M  ");	            		 
	            	 } else {
		                 setText(formatter.format(dat));	            		 	            		 
	            	 }
				}else if(dat ==null){
					setText("0");
				}else{
					setText(""+dat.toString());
				}
			}*/
			if (value instanceof MutableIData) {
				MutableIData args = (MutableIData)value;
				Object dat = args.getData();
				setHorizontalAlignment((dat instanceof Double)? JLabel.RIGHT:JLabel.LEFT);				
				if (dat instanceof Double) {					
					 setIcon(null);
	            	 double val = ((Double)dat).doubleValue();
	            	 if (Math.abs(val)>1000000000) {
		                 setText(formatter3.format(val / 1000000000)+"B  ");	            		 
	            		 
	            	 } else if (Math.abs(val)>1000000) {
		                 setText(formatter3.format(val / 1000000)+"M  ");	            		 
	            	 } else {
		                 setText(formatter.format(dat));	            		 	            		 
	            	 }
				} else if(dat == null) {
					setText("0");
				} else {
					setText(""+dat.toString());
				}
			}
		}catch (Exception e) {
			return;
		}
	}

}
