package eqtrade.feed.parser;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sun.security.action.GetBooleanAction;

import com.vollux.idata.Column;
import com.vollux.idata.Row;

import eqtrade.feed.core.EQGridModel;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Market;
import eqtrade.feed.model.MarketDef;
import eqtrade.feed.model.Sector;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockRender;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;
import eqtrade.feed.model.Subscriber;
import eqtrade.feed.model.Summary;
import eqtrade.feed.model.SummaryDef;

public final class StockSummParser extends Parser {

	private Hashtable<String, StockSummary> hdataStocksummary = new Hashtable<String, StockSummary>();
	// private Hashtable<String, Integer> hdataStocksummaryCounter = new
	// Hashtable<String, Integer>();
	private Vector<String> vsscounter = new Vector<String>();
	private Vector<String> vtemp = new Vector<String>();
	private Vector<Double> vtempseqno = new Vector<Double>();

	private Logger log = LoggerFactory.getLogger(getClass());

	private Hashtable<String, Object> hdataMarket = new Hashtable<String, Object>();
	private boolean isready = false;

	public StockSummParser(IFeedEngine engine) {
		super(engine);
		setName(this.getClass().getName());
	}

	public void process(Vector vdata) {
		log.info("vdata stock summ "+vdata.size());
		for (int i = 0; i < vdata.size(); i++) {
			String data = (String) vdata.elementAt(i);

			final StockSummary ss = new StockSummary(data);

			Subscriber before = ((Subscriber) hashSubscriber.get(ss.getType()));

			log.info("stocksumm " + ss.getSeqno() + " before "
					+ before.getSeqno());
			((Subscriber) hashSubscriber.get(ss.getType())).setSeqno((int) ss
					.getSeqno());

			if (StockRender.getSector(ss.getCode()) == null) {
				StockRender.stockSector.put(ss.getCode(), ss.getRemarks()
						.charAt(3) + "");
			}

			SwingUtilities.invokeLater(new Runnable() {

				public void run() {
					Vector vRow;
					ss.setName(getStockName(ss.getCode()));
					int ssidx = engine
							.getDataStore()
							.get(getModelID())
							.getIndexByKey(
									new Object[] { ss.getCode(), ss.getBoard() });
					if (ssidx > -1) {
						StockSummary ssold = (StockSummary) engine
								.getDataStore().get(getModelID())
								.getDataByIndex(ssidx);
						genTotalMarket(ssold, ss);
						genBoardMarket(ssold, ss);
						ssold.fromVector(ss.getData());
					} else {
						genTotalMarket(null, ss);
						genBoardMarket(null, ss);
						vRow = new Vector(1);
						vRow.addElement(StockSummaryDef.createTableRow(ss));
						engine.getDataStore().get(getModelID())
								.addRow(vRow, false, false);
					}
					refreshListener(ss);
					// }
					genSummary();
					engine.getDataStore().get(getModelID()).refresh();
					engine.getDataStore().get(FeedStore.DATA_MARKET).refresh();
				}
			});

		}
	}

	@Override
	public void doProcess(Vector vdata) { //
		// super.doProcess(vdata);
		// process(vdata);

		// log.info("data stock summ "+vdata.size());
		processTunning(vdata);
	}

	public void processTunning(Vector vdata) {

		// final Vector<StockSummary> vss = new Vector<StockSummary>();
		// final Vector vel = new Vector();

		try {
			long start = System.nanoTime();
			for (int i = 0; i < vdata.size(); i++) {
				String data = (String) vdata.elementAt(i);
				String[] sp = data.split("\\|");
				final StockSummary ss = new StockSummary(data);

				((Subscriber) hashSubscriber.get(ss.getType()))
						.setSeqno((int) ss.getSeqno());

				/*if (sp.length == 22) {
					ss.setStatus(sp[21]);
				}*/

				//if(ss.getCode().equals("BBCA") || ss.getCode().equals("AALI"))
				//log.info("staus " +ss.getCode()+" "+ss.getBoard()+" "+ ss.getStatus() + " " + ss.getSeqno()+" "+ss.getClosing()+" "+ss.getHighest()+" "+isready+" "+vtempseqno.contains(ss.getSeqno()));
				
				/*if (ss.getStatus().isEmpty() && !isready) {
					vtemp.add(data);
				} else if (!vtempseqno.contains(ss.getSeqno())) {
					if (ss.getStatus().equals("finish")) {
						isready = true;
						vdata.addAll(vtemp);
						vtemp.clear();
					}*/
					
//					ss.setName(getStockNameReal(ss.getCode()));
					ss.setName(getStockName(ss.getCode()));
	                ss.setSector(getSectorName(getSectorbyStock(ss.getCode())));
					String key = ss.getCode() + "#" + ss.getBoard();

					StockSummary ssold = hdataStocksummary.get(key);
					if (ssold == null) {
						// hdataStocksummaryCounter.put(key, value)
						// vsscounter.add(ss);
						int idx2 = vsscounter.indexOf(ss.getCode()+" "+ss.getBoard());
						if (idx2 <= -1) {
							vsscounter.add(ss.getCode()+" "+ss.getBoard()); //log.info("stock summ "+ss.getCode()+" "+ss.getClosing()+" "+ss.getBoard()+" "+idx2);						
						}
					}
					
					
					//if(ss.getCode().equals("SOCI")){
					//}

					final int idx = vsscounter.indexOf(ss.getCode()+" "+ss.getBoard());

					genTotalMarketTunning(ssold, ss);
					genBoardMarketTunning(ssold, ss);
					gendtopgainer(ssold, ss);

					hdataStocksummary.put(key, ss);

					SwingUtilities.invokeLater(new Runnable() {

						public void run() {
							refreshListener(ss);
						}
					});

					final Vector vRow = new Vector(1);

					vRow.addElement(StockSummaryDef.createTableRow(ss));
					SwingUtilities.invokeLater(new Runnable() {

						public void run() {
							((EQGridModel) engine.getDataStore().get(
									getModelID())).addRow(ss, vRow, idx); //log.info("stock summ "+ss.getCode()+" "+ss.getClosing()+" "+idx);
						}

					});
					vtempseqno.add(ss.getSeqno());
				}

			long end = System.nanoTime() - start;

			/*log.info("proc_time_lv_"
					+ (TimeUnit
							.MILLISECONDS.convert(end,
							TimeUnit.NANOSECONDS))+" "+vdata.size());*/
				// vss.add(ss);
				// vel.add(vRow);

//			}

			/*
			 * SwingUtilities.invokeLater(new Runnable() {
			 * 
			 * public void run() { // engine.getDataStore().get(getModelID()) //
			 * .addRow(vRow, false, false); for (int i = 0; i < vss.size(); i++)
			 * {
			 * 
			 * StockSummary ss = vss.get(i);
			 * 
			 * int idx = vsscounter.indexOf(ss);
			 * 
			 * ((EQGridModel) engine.getDataStore().get(getModelID()))
			 * .addRow(ss, (Vector) vel.get(i), idx); } } });
			 */

			final Vector vRowm = new Vector(1);
			vRowm.addElement(MarketDef.createTableRow((Market) hdataMarket
					.get("TOTAL")));

			SwingUtilities.invokeLater(new Runnable() {

				public void run() {
					engine.getDataStore().get(FeedStore.DATA_MARKET)
							.addRow(vRowm, false, false);
				}
			});

			genSummaryTunning();
			//genSummary();
			SwingUtilities.invokeLater(new Runnable() {

				public void run() {
					engine.getDataStore().get(getModelID()).refresh();
					engine.getDataStore().get(FeedStore.DATA_MARKET).refresh();

				}
			});
		} catch (Exception ex) {
			log.error(Utils.logException(ex));
		}

	}

	@Override
	public void resubscribe() {
		isready = false;
		vtemp.clear();
		vtempseqno.clear();
		super.resubscribe();
	}

	private void process(String source) {
		if (source != null) {
			StockSummary ss;
			Vector vRow;
			// log.info("stock summ parser " + source);
			ss = new StockSummary(source);
			((Subscriber) hashSubscriber.get(ss.getType())).setSeqno((int) ss
					.getSeqno());
			if (StockRender.getSector(ss.getCode()) == null) {
				StockRender.stockSector.put(ss.getCode(), ss.getRemarks()
						.charAt(3) + "");
			}
			// if (ss.getTradedFrequency().doubleValue()>0) {
			ss.setName(getStockName(ss.getCode()));
			int ssidx = engine
					.getDataStore()
					.get(getModelID())
					.getIndexByKey(new Object[] { ss.getCode(), ss.getBoard() });
			if (ssidx > -1) {
				StockSummary ssold = (StockSummary) engine.getDataStore()
						.get(getModelID()).getDataByIndex(ssidx);
				genTotalMarket(ssold, ss);
				genBoardMarket(ssold, ss);
				ssold.fromVector(ss.getData());
			} else {
				genTotalMarket(null, ss);
				genBoardMarket(null, ss);
				vRow = new Vector(1);
				vRow.addElement(StockSummaryDef.createTableRow(ss));
				engine.getDataStore().get(getModelID())
						.addRow(vRow, false, false);
			}
			refreshListener(ss);
			// }
			genSummary();
			engine.getDataStore().get(getModelID()).refresh();
			engine.getDataStore().get(FeedStore.DATA_MARKET).refresh();
		}
	}

	protected void genSummary() {
		SwingUtilities.invokeLater(new Runnable() {
			
			public void run() {
				int i = engine.getDataStore().get(getModelID()).getRowCount();
				StockSummary stock;
				double adv = 0, dec = 0, unchg = 0, untrade = 0;
				for (int j = 0; j < i; j++) {
					stock = (StockSummary) engine.getDataStore().get(getModelID())
							.getDataByIndex(j);
					if (stock.getBoard().equals("RG")) {
						if (stock.getChange().doubleValue() > 0) {
							adv++;
						} else if (stock.getChange().doubleValue() < 0) {
							dec++;
						} else if (stock.getTradedFrequency().doubleValue() >= 0
								&& stock.getChange().doubleValue() == 0) {
							unchg++;
						} else if (stock.getTradedFrequency().doubleValue() == 0) {
							untrade++;
						}
					
						
					}
					
					
				
					
					
					
				}
				Summary summ = new Summary();
				summ.setActiontype("SUMMARY");
				summ.setInfo1(new Double(adv));
				summ.setInfo2(new Double(dec));
				summ.setInfo3(new Double(unchg));
				summ.setInfo4(new Double(untrade));
				Vector vrow = new Vector(1);
				vrow.addElement(SummaryDef.createTableRow(summ));
				engine.getDataStore().get(FeedStore.DATA_MARKETSUMM)
						.addRow(vrow, false, false);
	
			}
		});
	}

	private void genSummaryTunning() {
		double adv = 0, dec = 0, unchg = 0, untrade = 0;

		for (Iterator i = hdataStocksummary.keySet().iterator(); i.hasNext();) {
			String key = (String) i.next();
			StockSummary stock = hdataStocksummary.get(key);
			if (stock.getBoard().equals("RG")) {
				if (stock.getChange().doubleValue() > 0) {
					adv++;
				} else if (stock.getChange().doubleValue() < 0) {
					dec++;
				} else if (stock.getChange().doubleValue() == 0 && stock.getTradedFrequency().doubleValue() >0) {
					unchg++;
				} /*else if (stock.getTradedFrequency().doubleValue() == 0) {
					untrade++;
				}*/
				
				

			}
			
			if(stock.getBoard().equals("RG"))
				if(stock.getTradedFrequency().doubleValue() == 0){
					untrade++;
					//log.info("untrade "+stock.getCode()+" "+stock.getBoard()+" "+stock.getTradedFrequency());
				}
			
		}

		Summary summ = new Summary();
		summ.setActiontype("SUMMARY");
		summ.setInfo1(new Double(adv));
		summ.setInfo2(new Double(dec));
		summ.setInfo3(new Double(unchg));
		summ.setInfo4(new Double(untrade));
		final Vector vrow = new Vector(1);
		vrow.addElement(SummaryDef.createTableRow(summ));

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				engine.getDataStore().get(FeedStore.DATA_MARKETSUMM)
						.addRow(vrow, false, false);
			}
		});

	}

	protected void genTotalMarket(StockSummary oldStock, StockSummary newStock) {
		Market market;
		int idx = engine.getDataStore().get(FeedStore.DATA_MARKET)
				.getIndexByKey(new Object[] { "TOTAL" });
		if (idx > -1) {
			market = (Market) engine.getDataStore().get(FeedStore.DATA_MARKET)
					.getDataByIndex(idx);
			if (oldStock == null) {
				market.setValue(new Double(market.getValue().doubleValue()
						+ newStock.getTradedValue().doubleValue()));
				market.setVolume(new Double(market.getVolume().doubleValue()
						+ newStock.getTradedVolume().doubleValue()));
				market.setFreq(new Double(market.getFreq().doubleValue()
						+ newStock.getTradedFrequency().doubleValue()));
			} else {
				market.setValue(new Double(market.getValue().doubleValue()
						- oldStock.getTradedValue().doubleValue()
						+ newStock.getTradedValue().doubleValue()));
				market.setVolume(new Double(market.getVolume().doubleValue()
						- oldStock.getTradedVolume().doubleValue()
						+ newStock.getTradedVolume().doubleValue()));
				market.setFreq(new Double(market.getFreq().doubleValue()
						- oldStock.getTradedFrequency().doubleValue()
						+ newStock.getTradedFrequency().doubleValue()));
			}
		} else {
			market = new Market();
			market.setBoard("TOTAL");
			market.setValue(new Double(newStock.getTradedValue().doubleValue()));
			market.setVolume(new Double(newStock.getTradedVolume()
					.doubleValue()));
			market.setFreq(new Double(newStock.getTradedFrequency()
					.doubleValue()));
			Vector vRow = new Vector(1);
			vRow.addElement(MarketDef.createTableRow(market));
			engine.getDataStore().get(FeedStore.DATA_MARKET)
					.addRow(vRow, false, false);
		}
	}

	private void genTotalMarketTunning(StockSummary oldStock,
			StockSummary newStock) {
		Market market = (Market) hdataMarket.get("TOTAL");
		if (market == null) {
			market = new Market();
			market.setBoard("TOTAL");
			market.setValue(new Double(newStock.getTradedValue().doubleValue()));
			market.setVolume(new Double(newStock.getTradedVolume()
					.doubleValue()));
			market.setFreq(new Double(newStock.getTradedFrequency()
					.doubleValue()));
			hdataMarket.put("TOTAL", market);
		} else {
			if (oldStock == null) {
				market.setValue(new Double(market.getValue().doubleValue()
						+ newStock.getTradedValue().doubleValue()));
				market.setVolume(new Double(market.getVolume().doubleValue()
						+ newStock.getTradedVolume().doubleValue()));
				market.setFreq(new Double(market.getFreq().doubleValue()
						+ newStock.getTradedFrequency().doubleValue()));
			} else {
				market.setValue(new Double(market.getValue().doubleValue()
						- oldStock.getTradedValue().doubleValue()
						+ newStock.getTradedValue().doubleValue()));
				market.setVolume(new Double(market.getVolume().doubleValue()
						- oldStock.getTradedVolume().doubleValue()
						+ newStock.getTradedVolume().doubleValue()));
				market.setFreq(new Double(market.getFreq().doubleValue()
						- oldStock.getTradedFrequency().doubleValue()
						+ newStock.getTradedFrequency().doubleValue()));
			}
		}

	}

	protected void genBoardMarket(StockSummary oldStock, StockSummary newStock) {
		Market market;
		int idx = engine.getDataStore().get(FeedStore.DATA_MARKET)
				.getIndexByKey(new Object[] { newStock.getBoard() });
		if (idx > -1) {
			market = (Market) engine.getDataStore().get(FeedStore.DATA_MARKET)
					.getDataByIndex(idx);
			if (oldStock == null) {
				market.setValue(new Double(market.getValue().doubleValue()
						+ newStock.getTradedValue().doubleValue()));
				market.setVolume(new Double(market.getVolume().doubleValue()
						+ newStock.getTradedVolume().doubleValue()));
				market.setFreq(new Double(market.getFreq().doubleValue()
						+ newStock.getTradedFrequency().doubleValue()));
			} else {
				market.setValue(new Double(market.getValue().doubleValue()
						- oldStock.getTradedValue().doubleValue()
						+ newStock.getTradedValue().doubleValue()));
				market.setVolume(new Double(market.getVolume().doubleValue()
						- oldStock.getTradedVolume().doubleValue()
						+ newStock.getTradedVolume().doubleValue()));
				market.setFreq(new Double(market.getFreq().doubleValue()
						- oldStock.getTradedFrequency().doubleValue()
						+ newStock.getTradedFrequency().doubleValue()));
			}
			engine.getDataStore().get(FeedStore.DATA_MARKET).refresh(idx);
		} else {
			market = new Market();
			market.setBoard(newStock.getBoard());
			market.setValue(new Double(newStock.getTradedValue().doubleValue()));
			market.setVolume(new Double(newStock.getTradedVolume()
					.doubleValue()));
			market.setFreq(new Double(newStock.getTradedFrequency()
					.doubleValue()));
			Vector vRow = new Vector(1);
			vRow.addElement(MarketDef.createTableRow(market));
			engine.getDataStore().get(FeedStore.DATA_MARKET)
					.addRow(vRow, false, false);
		}
	}

	protected void genBoardMarketTunning(StockSummary oldStock,
			StockSummary newStock) {
		Market market = (Market) hdataMarket.get(newStock.getBoard());
		if (market == null) {
			market = new Market();
			market.setBoard(newStock.getBoard());
			market.setValue(new Double(newStock.getTradedValue().doubleValue()));
			market.setVolume(new Double(newStock.getTradedVolume()
					.doubleValue()));
			market.setFreq(new Double(newStock.getTradedFrequency()
					.doubleValue()));

			hdataMarket.put(newStock.getBoard(), market);

		} else {
			if (oldStock == null) {
				market.setValue(new Double(market.getValue().doubleValue()
						+ newStock.getTradedValue().doubleValue()));
				market.setVolume(new Double(market.getVolume().doubleValue()
						+ newStock.getTradedVolume().doubleValue()));
				market.setFreq(new Double(market.getFreq().doubleValue()
						+ newStock.getTradedFrequency().doubleValue()));
			} else {
				market.setValue(new Double(market.getValue().doubleValue()
						- oldStock.getTradedValue().doubleValue()
						+ newStock.getTradedValue().doubleValue()));
				market.setVolume(new Double(market.getVolume().doubleValue()
						- oldStock.getTradedVolume().doubleValue()
						+ newStock.getTradedVolume().doubleValue()));
				market.setFreq(new Double(market.getFreq().doubleValue()
						- oldStock.getTradedFrequency().doubleValue()
						+ newStock.getTradedFrequency().doubleValue()));
			}
		}

		final Vector vRow = new Vector(1);
		vRow.addElement(MarketDef.createTableRow(market));
		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				engine.getDataStore().get(FeedStore.DATA_MARKET)
						.addRow(vRow, false, false);

			}
		});

	}
	protected void gendtopgainer(StockSummary oldStock, StockSummary newStock){
    	StockSummary  tp ;
    	 int idx = engine.getDataStore().get(FeedStore.DATA_TOPGAINER).getIndexByKey(new Object[]{newStock.getCode(),newStock.getBoard()});
         if (idx > -1) {
			tp = (StockSummary) engine.getDataStore().get(FeedStore.DATA_TOPGAINER).getDataByIndex(idx);
			if (oldStock == null) {
				tp.setTradedFrequency(new Double(tp.getTradedFrequency().doubleValue()+newStock.getTradedFrequency().doubleValue()));
				tp.setTradedVolume(new Double(tp.getTradedVolume().doubleValue() + newStock.getTradedVolume().doubleValue()));
				tp.setTradedValue(new Double(tp.getTradedValue().doubleValue() + newStock.getTradedValue().doubleValue()));
				
			} else {
				tp.setTradedFrequency(new Double(tp.getTradedFrequency().doubleValue()-oldStock.getTradedFrequency().doubleValue()+newStock.getTradedFrequency().doubleValue()));
				tp.setTradedVolume(new Double(tp.getTradedVolume().doubleValue() - oldStock.getTradedVolume().doubleValue()+ newStock.getTradedVolume().doubleValue()));
				tp.setTradedValue(new Double(tp.getTradedValue().doubleValue() -oldStock.getTradedValue().doubleValue() + newStock.getTradedValue().doubleValue()));
			}
//			engine.getDataStore().get(FeedStore.DATA_TOPGAINER).refresh(idx);
		} else {
//	        Vector vrow = new Vector(1);
//	        vrow.addElement(StockSummaryDef.createTableRow(newStock));
//			engine.getDataStore().get(FeedStore.DATA_TOPGAINER).addRow(vrow, false, false);
		}
    }
	protected Integer getModelID() {
		return FeedStore.DATA_STOCKSUMMARY;
	}

	protected List createTableRow(Row dat) {
		return StockSummaryDef.createTableRow(dat);
	}

	protected void saveModel(Subscriber r) {
		String filename = "data/session/" + FeedSetting.session + "-" + "5S"
				+ ".cache";
		log.info("save model " + filename);
		Vector vdata = new Vector(50, 10);
		if (getModelID() != null) {
			Vector vtemp = engine.getDataStore().get(FeedStore.DATA_MARKET)
					.getDataVector();
			for (int i = 0; i < vtemp.size(); i++) {
				vdata.addElement(((Column) ((Vector) vtemp.elementAt(i))
						.elementAt(0)).getSource());

			}
			HashMap map = new HashMap(2);
			map.put("seq", new Integer(r.getSeqno()));
			map.put("msg", "5S");
			map.put("data", vdata);
			Utils.writeFile(filename, Utils.objectToCompressedByte(map));
		}
		super.saveModel(r);
	}

	protected int loadModel(String msg) {
		
		
		for (Iterator i = MarketDef.boardMap.keySet().iterator(); i.hasNext();) {
			Market m = new Market();
			m.setBoard((String) i.next());
			hdataMarket.put(m.getBoard(), m);
			
		}
		
		try {
			String filename = "data/session/" + FeedSetting.session + "-"
					+ "5S" + ".cache";
			Object o = Utils.readFile(filename);
			if (o != null) {
				byte[] byt = (byte[]) o;
				HashMap map = (HashMap) Utils.compressedByteToObject(byt);
				Vector vdata = (Vector) map.get("data");
				for (int i = 0; i < vdata.size(); i++) {
					Row r = (Row) vdata.elementAt(i);
					Vector vrow = new Vector(1);
					vrow.addElement(MarketDef.createTableRow(r));
					engine.getDataStore().get(FeedStore.DATA_MARKET)
							.addRow(vrow, false, false);
					Market m = (Market) r;
					hdataMarket.put(m.getBoard(), m);

				}
			}
		} catch (Exception ex) {
		}

		int seq = 0;
		try {
			String filename = genFilename(msg);
			Object o = Utils.readFile(filename);
			if (o != null) {
				byte[] byt = (byte[]) o;
				HashMap map = (HashMap) Utils.compressedByteToObject(byt); // (HashMap)o;
				Vector vdata = (Vector) map.get("data");
				
				for (int i = 0; i < vdata.size(); i++) {
					Row r = (Row) vdata.elementAt(i);
					if (StockRender.getSector(((StockSummary) r).getCode()) == null) {
						StockRender.stockSector.put(
								((StockSummary) r).getCode(),
								((StockSummary) r).getRemarks().charAt(3) + "");
					}
					Vector vrow = new Vector(100, 5);
					vrow.addElement(createTableRow(r));

					StockSummary ss = (StockSummary) r;

					String key = ss.getCode() + "#" + ss.getBoard();
					hdataStocksummary.put(key, ss);
					
					if (getModelID() != null) {
						engine.getDataStore().get(getModelID())
								.addRow(vrow, false, false);
						
						int idx2 = vsscounter.indexOf(ss.getCode()+" "+ss.getBoard());
						if (idx2 <= -1) {
							vsscounter.add(ss.getCode()+" "+ss.getBoard());
						}
						
						idx2 = vsscounter.indexOf(ss.getCode()+" "+ss.getBoard());
						
						((EQGridModel) engine.getDataStore().get(
								getModelID())).addRow(ss, vrow, idx2);
						
					}

				}
				
				engine.getDataStore().get(getModelID()).refresh();
				
				seq = ((Integer) map.get("seq")).intValue();
			} else {
				seq = 0;
			}
		} catch (Exception ex) {
			seq = 0;
			ex.printStackTrace();
		}
		genSummaryTunning();
		return seq;
	}

	protected String getStockName(String code) {
		Stock s = ((Stock) engine.getDataStore().get(FeedStore.DATA_STOCK)
				.getDataByKey(new String[] { code }));
		return (s != null) ? s.getName() : "";
	}

	/*protected String getStockNameReal(String code) {
		Stock s = (Stock) engine.getDataStore()
				.getReal(FeedStore.DATA_STOCK_REAL).get(code);
		return (s != null) ? s.getName() : "";
	}*/
	protected String getSectorbyStock(String code) {
    	Stock stocksector = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new String[]{code}));
    	return (stocksector != null) ? stocksector.getSector() : "";
    }
    
    protected String getSectorName(String code) {
    	Sector sector = ((Sector)engine.getDataStore().get(FeedStore.DATA_SECTORNAME).getDataByKey(new String[]{code}));
    	return (sector != null) ? sector.getCIDX_SECTORNAME() : "";
    }
}