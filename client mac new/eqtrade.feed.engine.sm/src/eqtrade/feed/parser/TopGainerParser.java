package eqtrade.feed.parser;

import java.util.Vector;

import com.vollux.idata.Column;
import com.vollux.idata.GridModel;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.RunningTrade;
import eqtrade.feed.model.Sector;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;
import eqtrade.feed.model.Subscriber;

public class TopGainerParser extends Parser{

	public TopGainerParser(IFeedEngine engine) {
		super(engine);
		setName(this.getClass().getName());
	}

	@Override
	public void process(Vector vdata) {
		for (int i = 0; i < vdata.size(); i++) {
			String data = (String)vdata.elementAt(i);
			process(data);
		}
		
	}

	private void process(String source) {
		 if (source != null) {
            StockSummary ss;
			Vector vRow;
            ss = new StockSummary(source);
		//di tambah hari ini 
	        ss.setName(getStockName(ss.getCode()));
	        ss.setSector(getSectorName(getSectorbyStock(ss.getCode())));
            int ssidx = engine.getDataStore().get(getModelID()).getIndexByKey(new Object[]{ss.getCode(), ss.getBoard()});
            StockSummary ssnew = (StockSummary) engine.getDataStore().get(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new Object[]{ss.getCode(), ss.getBoard()});
            if (ssidx>-1){
        	 	StockSummary ssold = (StockSummary)engine.getDataStore().get(getModelID()).getDataByIndex(ssidx);
        	 	// yang ada di table StockSummary ditambah hari ini
        	 	ss.setTradedFrequency(new Double(ss.getTradedFrequency().doubleValue()+ssnew.getTradedFrequency().doubleValue()));
        	 	ss.setTradedVolume(new Double(ss.getTradedVolume().doubleValue() + ssnew.getTradedVolume().doubleValue()));
				ss.setTradedValue(new Double(ss.getTradedValue().doubleValue() + ssnew.getTradedValue().doubleValue()));
        	 	ss.setClosing(ssnew.getClosing());
//        	 	ss.setTradedFrequency(ssnew.getTradedFrequency()+ss.getTradedFrequency());
                ssold.fromVector(ss.getData());
	        } else if(ssnew != null){ 
	        	ss.setTradedVolume(new Double(ss.getTradedVolume().doubleValue() + ssnew.getTradedVolume().doubleValue()));
				ss.setTradedValue(new Double(ss.getTradedValue().doubleValue() + ssnew.getTradedValue().doubleValue()));
        	 	ss.setClosing(ssnew.getClosing());
        	 	ss.setTradedFrequency(ssnew.getTradedFrequency()+ss.getTradedFrequency());
        	    vRow = new Vector(1);
			    vRow.addElement(StockSummaryDef.createTableRow(ss));
			    engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
		 	} else {
	        	// yang ga ada di table StockSummary langsung masuk table
//	        	 	ss.setTradedFrequency(new Double(ss.getTradedFrequency().doubleValue()+ssnew.getTradedFrequency().doubleValue()));
	        	 	ss.setTradedVolume(new Double(ss.getTradedVolume().doubleValue() ));
					ss.setTradedValue(new Double(ss.getTradedValue().doubleValue() ));
	        	 	ss.setClosing(ss.getClosing());
	        	 	ss.setTradedFrequency(ss.getTradedFrequency());
	        	   vRow = new Vector(1);
				   vRow.addElement(StockSummaryDef.createTableRow(ss));
				   engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
	         }
		 }
		 engine.getDataStore().get(getModelID()).refresh();
	}
	
	protected boolean removeAfterUnsubscribe(){
    	return false;
    }
	 
	public void removeModel(Subscriber r){
    	engine.getDataStore().get(getModelID()).getDataVector().clear();
    	engine.getDataStore().get(getModelID()).refresh();
    }
	
    protected Integer getModelID(){
        return FeedStore.DATA_TOPGAINER;
    }
    
    protected String getStockName(String code){
        Stock s = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new String[]{code}));
        return (s!=null) ? s.getName() : "";
    }

    protected String getSectorbyStock(String code) {
    	Stock stocksector = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new String[]{code}));
    	return (stocksector != null) ? stocksector.getSector() : "";
    }
    
    protected String getSectorName(String code) {
    	Sector sector = ((Sector)engine.getDataStore().get(FeedStore.DATA_SECTORNAME).getDataByKey(new String[]{code}));
    	return (sector != null) ? sector.getCIDX_SECTORNAME() : "";
    }
}
