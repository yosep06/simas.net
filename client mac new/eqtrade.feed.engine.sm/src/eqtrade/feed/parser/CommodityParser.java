package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Commodity;
import eqtrade.feed.model.CommodityDef;
import eqtrade.feed.model.IPO;
import eqtrade.feed.model.IPODef;
import eqtrade.feed.model.Subscriber;

public final class CommodityParser extends Parser {
	 public CommodityParser(IFeedEngine engine){
			super(engine);
	        setName(this.getClass().getName());
		}

	    
	    //actiontype|stock|amount|ratio1|ratio2|cumdate|extdate|recordingdate|distdate
		public void process(Vector vdata){
	        if (vdata.size() > 0){
	    		Commodity commodity;
				Vector vRow;
	    		for (int i = 0; i<vdata.size(); i++){
	    			String source = (String) vdata.elementAt(i);
	    			commodity = new Commodity(source);
	    			vRow = new Vector(1); 
	                vRow.addElement(CommodityDef.createTableRow(commodity));
	                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
	                ((Subscriber)hashSubscriber.get(commodity.getType())).setSeqno((int)commodity.getSeqno());
	                refreshListener(commodity);
	                log.info("source "+source);
	    		}
	    		
	        }		
		}
	    
	    protected Integer getModelID(){
	        return FeedStore.DATA_COMMODITY;
	    }
	    
	    protected List createTableRow(Row dat){
	    	return CommodityDef.createTableRow(dat);
	    }
}
