package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Commodity;
import eqtrade.feed.model.CommodityDef;
import eqtrade.feed.model.Future;
import eqtrade.feed.model.FutureDef;
import eqtrade.feed.model.IPODef;
import eqtrade.feed.model.Subscriber;

public final class FutureParser extends Parser {
	 public FutureParser(IFeedEngine engine){
			super(engine);
	        setName(this.getClass().getName());
		}

	    
	    //actiontype|stock|amount|ratio1|ratio2|cumdate|extdate|recordingdate|distdate
		public void process(Vector vdata){
	        if (vdata.size() > 0){
	    		Future future;
				Vector vRow;
	    		for (int i = 0; i<vdata.size(); i++){
	    			String source = (String) vdata.elementAt(i);
	    			future = new Future(source);
	    			vRow = new Vector(1);
	                vRow.addElement(FutureDef.createTableRow(future));
	                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
	                ((Subscriber)hashSubscriber.get(future.getType())).setSeqno((int)future.getSeqno());
	                refreshListener(future);
	                log.info("source "+source);
	    		}
	    		
	        }		
		}
	    
	    protected Integer getModelID(){
	        return FeedStore.DATA_FUTURE;
	    }
	    
	    protected List createTableRow(Row dat){
	    	return FutureDef.createTableRow(dat);
	    }

}
