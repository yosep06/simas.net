package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import javax.swing.SwingUtilities;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.CorpAction;
import eqtrade.feed.model.CorpActionDef;
import eqtrade.feed.model.Subscriber;


public final class ActionParser  extends Parser {

    public ActionParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}

    
    //actiontype|stock|amount|ratio1|ratio2|cumdate|extdate|recordingdate|distdate
	public void process(final Vector vdata){
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (vdata.size() > 0){
		    		CorpAction action;
					Vector vRow;
		    		for (int i = 0; i<vdata.size(); i++){
		    			String source = (String) vdata.elementAt(i);
		    			action = new CorpAction(source);
		    			vRow = new Vector(1);
		                vRow.addElement(CorpActionDef.createTableRow(action));
		                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
		                ((Subscriber)hashSubscriber.get(action.getType())).setSeqno((int)action.getSeqno());
		                refreshListener(action);
		    		}
		        }
			}
		});        		
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_CORPACTION;
    }
    
    protected List createTableRow(Row dat){
    	return CorpActionDef.createTableRow(dat);
    }
}