package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Column;
import com.vollux.idata.Row;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Broker;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.Subscriber;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;


public final class TradeSummSBParser  extends Parser {

    public TradeSummSBParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}
    
    protected String genFilename(String msg){
    	String[] s1 = msg.split("\\|");
   	return "data/session/"+FeedSetting.session+"-"+s1[0]+s1[1]+".cache";
    }
    
    protected String getStockName(String code){
        Stock s = ((Stock)engine.getDataStore().get(FeedStore.DATA_STOCK).getDataByKey(new Object[]{code}));
        return (s!=null) ? s.getName() : "";
    }
    
    protected String getBrokerName(String code){
        Broker s = ((Broker)engine.getDataStore().get(FeedStore.DATA_BROKER).getDataByKey(new Object[]{code}));
        return (s!=null) ? s.getName() : "";
    }

	public void process(Vector vdata){
        if (vdata.size() > 0){
    		TradeSummary trade;
			Vector vRow;
    		for (int i = 0; i<vdata.size(); i++){
    			String source = (String) vdata.elementAt(i);
    			trade = new TradeSummary(source);
    			trade.setBrokername(getBrokerName(trade.getBroker()));
    			trade.setStockname(getStockName(trade.getStock()));
    			vRow = new Vector(1);
                vRow.addElement(createTableRow(trade));
                engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                ((Subscriber)hashSubscriber.get(trade.getType()+"|"+trade.getStock())).setSeqno((int)trade.getSeqno());
                refreshListener(trade);
    		}
        }		
	}
    
    protected Integer getModelID(){
        return FeedStore.DATA_TRADESUMSB;
    }
    
    protected List createTableRow(Row dat){
    	return TradeSummaryDef.createTableRow(dat);
    }
    
    protected boolean removeAfterUnsubscribe(){
    	return true;
    }
    
    protected Vector getDataVector(Subscriber r){
    	Vector vtemp = new Vector(10);
        TradeSummary trade;
        Vector vAll = engine.getDataStore().get(getModelID()).getDataVector();
        for (int i = 0; i < vAll.size();i++){
            trade = (TradeSummary)((Column)((Vector)vAll.elementAt(i)).elementAt(0)).getSource();
            if (r.getKey().endsWith(trade.getStock())){
            	vtemp.addElement(vAll.elementAt(i));
            } 
        }        
        return vtemp;
    }
    
    public void removeModel(Subscriber r){
        int rowCount = engine.getDataStore().get(getModelID()).getRowCount();
        TradeSummary trade;
        int counter =0;
        for (int i = 0; i < rowCount;i++){
            trade = (TradeSummary)engine.getDataStore().get(getModelID()).getDataByIndex(counter);
            if (r.getKey().endsWith(trade.getStock())){
            	engine.getDataStore().get(getModelID()).deleteRow(counter);
            } else counter++;
        }
    }  
}