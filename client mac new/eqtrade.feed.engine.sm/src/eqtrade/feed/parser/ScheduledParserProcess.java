package eqtrade.feed.parser;

import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.org.apache.xalan.internal.xsltc.compiler.Parser;

public class ScheduledParserProcess extends Thread {

	private int timer;
	private Vector vtemp = new Vector();
	private boolean isterminated = false;
	private IScheduledParser parser;
	private Logger log = LoggerFactory.getLogger(getClass());

	public ScheduledParserProcess(int timer, IScheduledParser parser) {
		super();
		this.timer = timer;
		this.parser = parser;
		start();
	}

	@Override
	public void run() {

		while (!isterminated) {

			synchronized (this) {
				try {
					this.wait(timer);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			Vector vclone = new Vector();
			synchronized (vtemp) {
				vclone.addAll(vtemp);
				vtemp.clear();
			}

			try {
				//log.info("bulk tv " + vclone.size());
				parser.processBulk(vclone);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}

	}

	public void setstop() {
		synchronized (this) {
			isterminated = true;
			this.notify();
		}
	}

	public void addProcess(Vector v) {
		synchronized (vtemp) {
			vtemp.addAll(v);
			vtemp.notifyAll();
		}
	}

}
