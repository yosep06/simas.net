package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import javax.swing.SwingUtilities;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.News;
import eqtrade.feed.model.NewsDef;
import eqtrade.feed.model.StockNews;
import eqtrade.feed.model.StockNewsDef;
import eqtrade.feed.model.Subscriber;

public final class StockNewsParser extends Parser {
	private ThreadStockNews thread;
	public StockNewsParser(IFeedEngine engine) {
		super(engine);
		setName(this.getClass().getName());
		thread = new ThreadStockNews(this);
		thread.start();
	}	
	
	public void process(final Vector vdata) {		
		thread.addMsg(vdata);
	}
	
	protected Integer getModelID(){
        return FeedStore.DATA_STOCKNEWS;
    }
    
    protected List createTableRow(Row dat){
    	return StockNewsDef.createTableRow(dat);
    }

	
}

class ThreadStockNews extends Thread {
	private Vector<String> vmsg = new Vector<String>();
	private StockNewsParser snp;
	private Log log = LogFactory.getLog(getClass());
	
	public ThreadStockNews (StockNewsParser snp) {
		this.snp = snp;
	}
	
//	public void addMsg(String msg) {
//		synchronized (vmsg) {
//			vmsg.clear();
//			vmsg.add(msg);
//			vmsg.notify();
//		}
//	}
	
	public void addMsg(Vector msg) {
		synchronized (vmsg) {
			vmsg.clear();
			vmsg.addAll(msg);
			vmsg.notify();
		}
	}
	
	@Override
	public void run() {
		while (true) {
			if (vmsg.size() == 0) {
				synchronized (vmsg) {
					try {
						vmsg.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} else {		
				while (vmsg.size() > 0) {
					String source = (String) vmsg.remove(0);
					StockNews sn = new StockNews(source);
					Vector vRow = new Vector();
					vRow.add(snp.createTableRow(sn));
					snp.engine.getDataStore().get(snp.getModelID()).addRow(vRow, false, false);
					((Subscriber)snp.hashSubscriber.get(sn.getType())).setSeqno((int) sn.getSeqno());
				}
			}
		}
	}
}