package eqtrade.feed.parser;

import java.util.List;
import java.util.Vector;

import com.vollux.idata.Row;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Chart;
import eqtrade.feed.model.ChartDef;
import eqtrade.feed.model.Subscriber;


public final class ChartParser  extends Parser {
    public ChartParser(IFeedEngine engine){
		super(engine);
        setName(this.getClass().getName());
	}

	//CS-TLKM|070321|093022|5000|5400|5300
    public void process(Vector vdata){
        if (vdata.size() > 0){
            Chart chart, oldchart;
            Vector vRow;
            int idx;
            for (int i = 0; i<vdata.size(); i++){
                String mdt = (String) vdata.elementAt(i);
                chart = new Chart(mdt);
                idx = engine.getDataStore().get(getModelID()).getIndexByKey(new Object[]{chart.getStock(), chart.getDate(), chart.getTime()});
                if (idx <0) {
	                    vRow = new Vector(1);
	                    vRow.addElement(createTableRow(chart));
	                    engine.getDataStore().get(getModelID()).addRow(vRow, false, false);
                } else {
                	oldchart = (Chart)engine.getDataStore().get(getModelID()).getDataByIndex(idx);
                	oldchart.fromVector(chart.getData());
                	engine.getDataStore().get(getModelID()).refresh(idx);
                }
                refreshListener(chart);
            }
        }
	}    
    
    protected Integer getModelID(){
        return FeedStore.DATA_CHART;
    }
    
    protected List createTableRow(Row dat){
        return ChartDef.createTableRow(dat);
    }
    
    protected boolean removeAfterUnsubscribe(){
    	return true;
    }
    
    public void removeModel(Subscriber r){
    	engine.getDataStore().get(getModelID()).getDataVector().clear();
    	engine.getDataStore().get(getModelID()).refresh();
    }    
    
    protected void saveModel(Subscriber r){
    	//didisable karena data selalu diambil dari server (ada history data)
    }
    
    protected int loadModel(String msg){
    	super.loadModel(msg);
    	return 0;
    }
}