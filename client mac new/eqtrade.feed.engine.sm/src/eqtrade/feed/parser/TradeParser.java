package eqtrade.feed.parser;

import java.util.HashMap;
import java.util.Vector;

import javax.swing.SwingUtilities;

import com.vollux.idata.Row;

import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.core.Parser;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.RunningTrade;
import eqtrade.feed.model.RunningTradeDef;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.Subscriber;

public final class TradeParser  extends Parser implements IScheduledParser {
	private int MAXROW = 50;
	
	//YOSEP 191114 CLIENT BERAT
	//private Logger log = LoggerFactory.getLogger(getClass());
	private Vector<RunningTrade> vtemp = new Vector<RunningTrade>();
	private ScheduledParserProcess scheduleProcess;
	private int MAX_ROWLIVETRADE = 100;
	
    public TradeParser(IFeedEngine engine){
		super(engine);
		setPriority(NORM_PRIORITY+1);
        setName(this.getClass().getName());
		Vector vrow = new Vector(MAXROW);
		for (int i =0; i<MAXROW; i++){
			vrow.addElement(RunningTradeDef.createTableRow(new RunningTrade()));
		}
		engine.getDataStore().get(getModelID()).addRow(vrow, false, false);
		//YOSEP 191114 CLIENT BERAT
		scheduleProcess = new ScheduledParserProcess(500, this);
	}
    
    public Subscriber genRequest(String msg){
        int seq = loadModel(msg);
        return new Subscriber(msg, seq); 
    }
    //YOSEP 191114 CLIENT BERAT
    public void process(Vector vdata){
    	if (vdata.size()>0){
    		// TODO Auto-generated method stub
    					/*
    					 * for (int i = 0; i < vdata.size(); i++) { String data = (String)
    					 * vdata.elementAt(i); process(data); }
    					 */

    					scheduleProcess.addProcess(vdata);
    					;
    		
    	}
    }

	private void process(String vdata){
        if (vdata!=null){
			RunningTrade trade = new RunningTrade(vdata);
			//yosep 04122014 chg nego timeTrade
			if (trade.getBoard().equalsIgnoreCase("NG")) {
				StockSummary st = (StockSummary)engine.getDataStore().get(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new String[]{trade.getStock(),"RG"});
			trade.setPrevious(st.getPrevious());
			}
			
			trade.calculate();
            ((Subscriber)hashSubscriber.get(trade.getType())).setSeqno((int)trade.getSeqno());
            refreshListener(trade);
    		int to = MAXROW-1;
    		int from = MAXROW- 2;
    		for (int j=0; j<MAXROW - 1;j++){
    			RunningTrade rtfrom = (RunningTrade)engine.getDataStore().get(getModelID()).getDataByIndex(from);
    			RunningTrade rtto = (RunningTrade)engine.getDataStore().get(getModelID()).getDataByIndex(to);
    			rtto.fromVector(rtfrom.getData());
    			from--;
    			to--;
    		}
			RunningTrade rt = (RunningTrade)engine.getDataStore().get(getModelID()).getDataByIndex(0);
			rt.fromVector(trade.getData());
    		engine.getDataStore().get(getModelID()).refresh();
        }
	}

    protected Integer getModelID(){
        return FeedStore.DATA_RUNNINGTRADE;
    }
    
    protected int loadModel(String msg){
        try {
            String  filename = "data/session/"+FeedSetting.session+"-"+msg+".cache";
            //log.info("load model: "+msg+ " from: "+filename);
            Object o = Utils.readFile(filename);
            if (o != null){
            	byte[] byt = (byte[])o;            	
            	HashMap map =  (HashMap)Utils.compressedByteToObject(byt);
                Vector vdata = (Vector)map.get("data");
                if (vdata.size() !=0) {
                	engine.getDataStore().get(getModelID()).getDataVector().clear();
                	engine.getDataStore().get(getModelID()).refresh();
                }
                for (int i=0; i<vdata.size();i++){
                    Vector vrow = new Vector(1);
                    vrow.addElement(RunningTradeDef.createTableRow((Row)vdata.elementAt(i)));
                    engine.getDataStore().get(getModelID()).addRow(vrow, false, false);
                }
                int seq = ((Integer)map.get("seq")).intValue();
                return  (seq - MAXROW) < 0 ? 0 : (seq - MAXROW);
            } else {
                return 0;            
            }
        } catch (Exception ex){
            return 0;
        }
    }

	@Override
	public void processBulk(final Vector vdata) {
		// TODO Auto-generated method stub
		//YOSEP 191114 CLIENT BERAT
		if (vdata.size() > 0) {

			// TODO Auto-generated method stub

			while (vdata.size() > MAX_ROWLIVETRADE) {
				vdata.remove(0);
			}

			SwingUtilities.invokeLater(new Runnable() {

				public void run() {
					// TODO Auto-generated method stub
					long start = System.nanoTime();
					for (int i = 0; i < vdata.size(); i++) {
						String data = (String) vdata.elementAt(i);
						process(data);
					}

					long end = System.nanoTime() - start;

					/*log.info("proc_time_lv_"
							+ (TimeUnit.MILLISECONDS.convert(end,
									TimeUnit.NANOSECONDS))+" "+vdata.size());*/

				}
			});

		}
	}
}