package eqtrade.feed.engine.socket;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.Timer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eqtrade.ClientSocket;
import com.eqtrade.Receiver;
import com.eqtrade.SocketFactory;
import com.eqtrade.SocketInterface;

import eqtrade.feed.core.IFeedConnectionQueue;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedEngine;
import eqtrade.feed.engine.FeedEventDispatcher;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.parser.OrderParser;

public class FeedConnectionQueue implements Receiver, IFeedConnectionQueue {

	private SocketInterface socketQueue = null;
	private FeedEngine engine;
	private Logger log = LoggerFactory.getLogger(getClass());
	private int ninterval, totalServer;
	private String svr;
	private HashMap hashIP = new HashMap();
	private boolean isConnected = false;
	private Timer timerRefresh = null;


	public FeedConnectionQueue(FeedEngine engine) {
		this.engine = engine;
		init();
		
	}

	private void init() {
		timerRefresh = new Timer(1 * 60 * 1000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				heartbeat();
			}
		});
		timerRefresh.setInitialDelay(1 * 20 * 1000);
		timerRefresh.stop();
	}

	protected void heartbeat() {
		log.info("send heartbeat");
		socketQueue.sendMessage("heartbeat");
	}

	public void loadSettings() throws Exception {
		log.info("load setting qeueue");

		if (socketQueue == null)
			socketQueue = SocketFactory.createSocket(
					"data/config/config-ordertracking.ini", this);

		Object o = Utils.readFile("data/config/feed-queue.dat");
		Vector v = null;
		if (o == null) {
			v = new Vector(3);
			v.addElement("10.21.1.37:8000");
			v.addElement("10.21.1.37:8000");
			Utils.writeFile("data/config/feed-queue.dat", v);
		} else {
			v = (Vector) o;
		}
		// ninterval = ((Integer) v.elementAt(2)).intValue();
		int x = v.size();
		totalServer = x;
		hashIP.clear();
		for (int i = 0; i < totalServer; i++) {
			String ssvr = (String) v.elementAt(i);
			hashIP.put(new String(i + ""), new String(ssvr));
		}
		// try {
		// sip = InetAddress.getLocalHost().getHostAddress();
		// } catch (Exception ex){}
		log.info("available server test: " + totalServer);
		log.info("available server test: " + hashIP.toString());

	}

	public boolean searchingServer() throws Exception {
		boolean bresult = false;
		int i = 1, curr = 0, next = 2, loginattempt = 10, logcoba = 1;

		String url;
		loadSettings();
		while (logcoba <= loginattempt) {
			for (; i <= (totalServer * 1); i++) {
				if (i == next) {
					curr++;
					next = next + 1;
				}

				url = (String) hashIP.get(new String(curr + ""));
				log.info("login to : " + url);
				String[] urlport = url.split("\\:");

				isConnected = connect(urlport[0], new Integer(urlport[1]));

				log.info("isconnected " + isConnected);

				if (isConnected) {
					svr = url;
					logcoba = logcoba + loginattempt + 1;
					
					break;
				} else {
					log.warn("trying to reconnect (" + i + ")");
				}

			}
			logcoba = logcoba + 1;

		}

		if (!bresult && i > totalServer * 1) {
			HashMap param = new HashMap(1);
			socketQueue.closeSocket();
		}

		return bresult;
	}

	public void start() throws Exception {
		searchingServer();
	}

	public boolean connect(String host, int port) {
		if (!socketQueue.isConnected()) {
			boolean isstart = socketQueue.start(host, port);

			return isstart;
		}
		return true;

	}

	public void start(String ip, int port) {
		socketQueue.start(ip, port);
	}

	@Override
	public void connected(ClientSocket arg0) {
		log.info("connected "+isConnected);
		if(isConnected){
			if (!isReconnect)
			timerRefresh.start();
			
				
		}
	}

	@Override
	public void disconnect(ClientSocket arg0) {
		log.info("disconnected socket " + arg0.isConnected());
		if (engine.getConnection().getSessionid() > 0) {
			try {
				timerRefresh.stop();
				start();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void receive(ClientSocket arg0, byte[] arg1) {
		String msg = new String(arg1);

		if (msg.startsWith("queue-receiver")) {
			((OrderParser) engine.getParser().get(FeedParser.PARSER_QUEUE))
					.receive(msg);
		}

	}

	public void subscribe(String sheader) {
		socketQueue.sendMessage(sheader);
	}

	public void unsubscribe(String sheader) {
		socketQueue.sendMessage(sheader);
	}

	@Override
	public void stop() throws Exception {
		if (socketQueue != null && socketQueue.isConnected())
			socketQueue.closeSocket();
	}

	@Override
	public void reconnect(final int count) {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {
			public void run() {
				doReconnect(count);
			}
		}).start();
	}
	
	private int flip = 1, nextflip = 1, cnt = 0;
	private boolean isReconnect = false;

	protected void doReconnect(int count) {
		boolean bresult = false;
		isReconnect = true;
		try {
			cnt++;
			HashMap param = new HashMap(1);
			param.put(FeedEventDispatcher.PARAM_MESSAGE,
					"trying reconnecting...");
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_RECONNECTSTATUS, param);
			if (cnt == nextflip) {
				flip++;
				nextflip = nextflip + 1;
			}
			if (cnt >= totalServer * 1) {
				flip = 1;
				nextflip = 1;
				cnt = 0;
			}
			String url = (String) hashIP.get(new String(flip + ""));
			log.info("reconnect to: " + url + " flip number: " + flip + " " + totalServer);
			String[] urlport = url.split("\\:");
			// for (; !bresult;) {
			bresult = connect(urlport[0], new Integer(urlport[1]));
			log.info("doReconnect:" + url + " " + bresult);

			if (bresult) {
				svr = url;
				log.info("reconnect socket connected");
			} else {
				log.warn("reconnecting failed");
				HashMap param2 = new HashMap(1);
				param2.put(FeedEventDispatcher.PARAM_MESSAGE,
						"reconnecting failed or server not ready");
				engine.getEventDispatcher().pushEvent(
						FeedEventDispatcher.EVENT_RECONNECTSTATUS, param2);
			} /*
			 * else { log.warn("reconnecting failed"); HashMap param2 = new
			 * HashMap(1); param2.put(FeedEventDispatcher.PARAM_MESSAGE,
			 * "reconnecting failed or server not ready");
			 * engine.getEventDispatcher().pushEvent(
			 * FeedEventDispatcher.EVENT_RECONNECTFAILED, param2); }
			 */
		} catch (Exception ex) {
			log.warn("reconnecting failed");
			HashMap param2 = new HashMap(1);
			param2.put(FeedEventDispatcher.PARAM_MESSAGE,
					"reconnecting failed or server not ready");
			engine.getEventDispatcher().pushEvent(
					FeedEventDispatcher.EVENT_RECONNECTFAILED, param2);
			log.error(Utils.logException(ex));
			ex.printStackTrace();
		}
	}

	@Override
	public void receive(ClientSocket arg0, Object arg1) {
		// TODO Auto-generated method stub
		
	}
}
