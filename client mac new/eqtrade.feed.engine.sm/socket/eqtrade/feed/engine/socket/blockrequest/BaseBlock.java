package eqtrade.feed.engine.socket.blockrequest;

import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eqtrade.feed.core.IFeedEngine;
import eqtrade.feed.engine.socket.FeedConnectionSocket;

public class BaseBlock {
	protected IFeedEngine engine;
	protected Vector vreceive = new Vector();
	protected Log log = LogFactory.getLog(getClass());
	private String type;

	// private Queue queue = new LinkedBlockingQueue(10);

	public BaseBlock(String _type, IFeedEngine _engine) {
		this.engine = _engine;
		this.type = _type;
	}

	public Object sendAndReceive(String msg) {
		((FeedConnectionSocket) engine.getConnection()).send(msg);
		Object receive = "";
//		log.info("send and receive " + msg);
		synchronized (vreceive) {
			try {
				vreceive.wait();
				if(vreceive.size()>0){
				Object dt = (Object) vreceive.remove(0);
				receive = parsing(dt);
				}
			} catch (Exception e) {
				e.printStackTrace();
				log.error(e.getMessage());
			}
		}
		log.info("send and receive finished " + msg + " " + receive);

		return receive;
	}

	public Object sendAndReceive() throws Exception {
		return sendAndReceive(type + "*"
				+ engine.getConnection().getSessionid());
	}

	protected Object parsing(Object dt) {
		return dt;
	}

	public void addReceive(Object msg) {
		// log.info("receive " + msg);

		synchronized (vreceive) {
			vreceive.add(msg);
			vreceive.notify();
		}
	}

	public Object sendAndReceiveWithParam(String... param) throws Exception {
		String ss = type + "*" + engine.getConnection().getSessionid() + "*";
		for (int i = 0; i < param.length; i++) {
			ss += param[i];
			if (i < (param.length - 1))
				ss += "*";
		}

		return sendAndReceive(ss);

	}
	
	public Object sendAndReceiveWithParam(String param) throws Exception {
		String ss = type + "*" + engine.getConnection().getSessionid() + "*"+param;

		return sendAndReceive(ss);

	}
	
	
	
	public void clear() {
		synchronized (vreceive) {
			vreceive.clear();
			vreceive.notify();
		}
	}

}
