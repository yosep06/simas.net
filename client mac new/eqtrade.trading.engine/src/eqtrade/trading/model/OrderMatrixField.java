package eqtrade.trading.model;

import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class OrderMatrixField extends Column implements MutableIDisplayIData{
	public OrderMatrixField(Row source, int idx)throws Exception{
		super(source, idx); 
	} 

	@Override
	public ImmutableIDisplay getImmutableIDisplay() {
		return OrderMatrixDef.render;
	}

	@Override
	public MutableIDisplay getMutableIDisplay() {
		return OrderMatrixDef.render;
	}
}
