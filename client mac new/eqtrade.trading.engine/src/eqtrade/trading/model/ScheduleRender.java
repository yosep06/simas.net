
package eqtrade.trading.model;

import java.awt.Color;
import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.trading.core.SelectedBorder;
import eqtrade.trading.core.TradingSetting;

public class ScheduleRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     private String strFieldName = new String("");
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	 public static SimpleDateFormat formattime = new SimpleDateFormat("HH:mm:ss");
	 public static SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd");
	 public static SimpleDateFormat formatdatetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
     private static Color newBack;
     private static Color newFore;
     private static Schedule sch;
     private static SelectedBorder border = new SelectedBorder();
     public static HashMap hashStatus = new HashMap();
     public static HashMap hashAccType = new HashMap();
     public static HashMap hashInvType = new HashMap();
     public static HashMap hashCustType = new HashMap();

	 @Override
	public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
	 	try {
			strFieldName = table.getColumnName(column); 
			newBack =  (column % 2) == 0 ? TradingSetting.getColor(TradingSetting.C_BACKGROUND) : TradingSetting.getColor(TradingSetting.C_BACKGROUNDEVEN);
	        if(value instanceof MutableIData){
	        	sch = (Schedule) ((MutableIData)value).getSource();
	        	if(sch.getcBuysell().equals("0")){
	        		newFore = TradingSetting.getColor(TradingSetting.C_BUY);
	        	}else{
	        		newFore = TradingSetting.getColor(TradingSetting.C_SELL);
	        	}
	        	if(sch.getcAtsstatus().equals("W")){
	        		newFore = newFore.darker();
	        	}
	        }
	        if (isSelected) {
	            ((JLabel)component).setBorder(border);
	        }
	        component.setBackground(newBack);
	        component.setForeground(newFore);  
	 	
		} catch (Exception e) {
			// TODO: handle exception
		}
        return component;
    }
	
	 @Override
	public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 String strTemp = null;
                 setHorizontalAlignment((dat instanceof Double)? SwingConstants.RIGHT : SwingConstants.LEFT);
	             if (dat instanceof Double) {
	            		 if (strFieldName.equals(ScheduleDef.dataHeader[Schedule.C_CLIENTID])){
	            			 setText(((Double)dat).doubleValue()==1? "Yes":"No");
	            		 } else {
	            			 setText(formatter.format(dat));
	            		 }
                 } else if (dat instanceof Date){
		    			if (strFieldName.equals(ScheduleDef.dataHeader[Schedule.C_ENTRYTIME])){
		    				setText(formattime.format((Date)dat));
		    			}else if (strFieldName.equals(ScheduleDef.dataHeader[Schedule.C_ATSDATE])){
		    				setText(formatdate.format((Date)dat));
		    			} else {
		    				setText(formatdatetime.format((Date)dat));
		    			}
                 } else if (strFieldName.equals(ScheduleDef.dataHeader[Schedule.C_BUYSELL])){
                     String buysell = (String)dat;
                     if (buysell.equalsIgnoreCase("0")) strTemp = "BUY";
                     else strTemp = "SELL";
                     setText(strTemp);
                 } else if (strFieldName.equals(ScheduleDef.dataHeader[Schedule.C_VALIDITY])){
                     String valid = (String)dat;
                     if (valid.equalsIgnoreCase("S")) strTemp = "Session";
                     else if (valid.equalsIgnoreCase("D")) strTemp = "DAY";
                     else if(valid.equalsIgnoreCase("GTC")) strTemp = "GTC"; 
                     setText(strTemp);
                 } else if (dat == null){
	                 setText("");
	             } else {
	            	 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
