package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

import eqtrade.trading.model.NegDealListDef.NDLRender;

public final class NotifDef {
	public static Hashtable getTableDef(){
		Hashtable htable = new Hashtable();
		htable.put("header", dataHeader);
		htable.put("alignment", defaultHeaderAlignment);
		htable.put("width", defaultColumnWidth);
		htable.put("order", defaultColumnOrder);
		htable.put("sorted", columnsort);
		htable.put("sortcolumn", new Integer(0));
		htable.put("sortascending", new Boolean(true));
		
		return htable;
		
	}
	public static int[] columhide = new int[]{
		};
	
    public static String[] dataHeader = new String[]{
    	"2","2","2","2","2","2","2","2","2","2","2",
    	};
        
    public static int[] defaultHeaderAlignment = new int[]{
		};

	public  static int[] defaultColumnWidth = new int[]{
		};

	public  static int[] defaultColumnOrder = new int[]{
		};	

	public static boolean[] columnsort = new boolean[]{
		};
	public static Vector getHeader(){
		Vector header = new Vector(Notif.CIDN_NUMBEROFFIELDS);
		for(int i=0; i<dataHeader.length; i++){
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	public static List createTableRow(Row row){
		List vec = new Vector(Notif.CIDN_NUMBEROFFIELDS);
		for(int i=0; i<Notif.CIDN_NUMBEROFFIELDS; i++){
			
			try{
				vec.add(i, new NotifField(row,i));
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		return vec;
	}
	public final static NDLRender render = new NDLRender();
	static class NDLRender extends MutableIDisplayAdapter {
		DefaultTableCellRenderer renderer = new NotifRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			renderer.getTableCellRendererComponent(table, value, isSelected,
					hasFocus, row, column);
			//((NotificationRender) renderer).setValue(value);
			return renderer;
		}
	}

}
