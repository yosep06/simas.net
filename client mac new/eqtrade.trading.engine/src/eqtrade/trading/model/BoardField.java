package eqtrade.trading.model;

import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class BoardField extends Column implements MutableIDisplayIData {
    public BoardField(Row source, int idx) throws Exception{
        super(source, idx);
    }
    
    @Override
	public MutableIDisplay getMutableIDisplay() {
		return BoardDef.render;
	}

	@Override
	public ImmutableIDisplay getImmutableIDisplay() {
		return BoardDef.render;
	}
}
