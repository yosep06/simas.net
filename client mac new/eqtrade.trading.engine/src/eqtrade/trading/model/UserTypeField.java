package eqtrade.trading.model;

import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class UserTypeField extends Column implements MutableIDisplayIData {
    public UserTypeField(Row source, int idx) throws Exception{
        super(source, idx);
    }
    
    @Override
	public MutableIDisplay getMutableIDisplay() {
		return UserTypeDef.render;
	}

	@Override
	public ImmutableIDisplay getImmutableIDisplay() {
		return UserTypeDef.render;
	}
}
