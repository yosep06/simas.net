package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class ScheduleDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader.clone());
        htable.put("alignment", defaultHeaderAlignment.clone());
        htable.put("width", defaultColumnWidth.clone());
        htable.put("order", defaultColumnOrder.clone());
        htable.put("sorted", columnsort.clone());        
        htable.put("sortcolumn",new Integer(Schedule.C_ENTRYTIME));
        htable.put("sortascending", new Boolean(true));
        htable.put("hide", columhide.clone());
        return htable;
	}	
	
	public static int[] columhide = new int[]{
		Schedule.C_VOLUME,Schedule.C_CONDITIONTYPE,Schedule.C_CONDITIONMETHOD, Schedule.C_COMPARELASTPRICE, Schedule.C_REMAINBIDLOT, Schedule.C_CUMULATIVVEDONE,
		Schedule.C_CUMULATIVECALC, Schedule.C_REVERSALLIMIT, Schedule.C_BD_FILTER, Schedule.C_BD_REFCODE,
		Schedule.C_BD_REFOPT,Schedule.C_BD_DONETYPE,Schedule.C_BD_DONESPEC, Schedule.C_FLAG1, Schedule.C_INVTYPE,
		Schedule.C_ORDERID, Schedule.C_ORDERSTATUS, Schedule.C_COMPLAINCEID
		
	};
	
    public static String[] dataHeader = new String[]{
    	"ATS ID","ATS Date","Client ID", "B/S","Stock","Price","Lot","Volume","Label","Validity","Valid Until","ATS Status","Order ID",
    	"Order Status","Condition Type","Condition Method","Compare Last Price","Remain Bid Lot","Cumulative Done","Cumulative calc",
    	"Reversal Limit","Filter","Ref Code","Refopt","Done Type","Done Spec","Entry By","Description","Entry Time","Entry Terminal","Exec By",
    	"Exec Time","Withdraw By","Withdraw Time","Withdraw Terminal","Flag 1","INV Type","Complaince Id"
    };
        
    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
        SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER
        
       
    };

	public  static int[] defaultColumnWidth = new int[]{
			100,100,100,100,100,100,100,100,100,100,
			100,100,100,100,100,100,100,100,100,100,
			100,100,100,100,100,100,100,100,100,100,
			100,100,100,100,100,100,100,100

	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,
			21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37
	};	

	public static boolean[] columnsort = new boolean[]{
		true, true, true, true, true, true, true, true,true,true,
		true, true, true, true, true, true, true, true,true,true,
		true, true, true, true, true, true, true, true,true,true,
		true, true, true, true, true, true, true, true
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(Schedule.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row acc) {
		List vec = new Vector(Schedule.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Schedule.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new ScheduleField(acc, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static AccRender render = new AccRender();
	
    static class AccRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new ScheduleRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((ScheduleRender)renderer).setValue(value);
			return renderer;
		}
	}
}
