package eqtrade.trading.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;


public final class Order extends Model {
	private static final long serialVersionUID = 1L;
	public static final int	C_ORDERID = 0;
    public static final int	C_ORDERDATE = 1;
    public static final int	C_XCHID = 2;
    public static final int	C_BRDID = 3;
    public static final int	C_BROKERID = 4;
    public static final int	C_BUYORSELL = 5;
    public static final int	C_ORDTYPE = 6;
    public static final int	C_ORDERTYPE = 7;
    public static final int	C_SECID = 8;
    public static final int	C_ISADVERTISING = 9;
    public static final int	C_TRADINGID = 10;
    public static final int	C_INVTYPE = 11;
    public static final int	C_CURID = 12;
    public static final int	C_CONTRABROKERID = 13;
    public static final int	C_CONTRAUSERID = 14;
    public static final int	C_PRICE = 15;
    public static final int	C_LOT = 16;
    public static final int	C_VOLUME = 17;
    public static final int	C_ISBASKETORDER = 18;
    public static final int	C_BASKETSENDTIME = 19;
    public static final int	C_STATUSID = 20;
    public static final int	C_TLBEFORE = 21;
    public static final int	C_AVPORTBEFORE = 22;
    public static final int	C_RATIOBEFORE = 23;
    public static final int	C_ISOVERLIMIT = 24;
    public static final int	C_ISSHORTSELL = 25;
    public static final int	C_MARKETORDERID = 26;
    public static final int	C_DONEVOLUME = 27;
    public static final int	C_PREVDONEVOLUME = 28;
    public static final int	C_PREVORDERID = 29;
    public static final int	C_NEXTORDERID = 30;
    public static final int	C_PREVMARKETORDERID = 31;
    public static final int	C_NEXTMARKETORDERID = 32;
    public static final int	C_ENTRYRTTIME = 33;
    public static final int	C_ENTRYRTBY = 34;
    public static final int	C_ENTRYRTTERMINAL = 35;
    public static final int	C_REJECTTIME = 36;
    public static final int	C_OPENTIME = 37;
    public static final int	C_OPENSENDTIME = 38;
    public static final int	C_AMENDTIME = 39;
    public static final int	C_AMENDBY = 40;
    public static final int	C_AMENDTERMINAL = 41;
    public static final int	C_AMENDSENDTIME = 42;
    public static final int	C_WITHDRAWTIME = 43;
    public static final int	C_WITHDRAWBY = 44;
    public static final int	C_WITHDRAWTERMINAL = 45;
    public static final int	C_WITHDRAWSENDTIME = 46;
    public static final int	C_SPLITTO = 47;
    public static final int	C_SPLITBY = 48;
    public static final int	C_SPLITTERMINAL = 49;
    public static final int	C_MATCHCOUNTER = 50;
    public static final int	C_MATCHTIME = 51;
    public static final int	C_LQVALUEBEFORE = 52;
    public static final int	C_COUNTER = 53;
    public static final int	C_TOTALCOUNTERSPLIT = 54;
    public static final int	C_NOTE = 55;
    public static final int	C_COMPLAINCEID = 56;
    public static final int	C_UPDATEDATE = 57;
    public static final int	C_USERID = 58;
    public static final int	C_UPDATEBY = 59;
    public static final int	C_TERMINALID = 60;
    public static final int	C_ISFLOOR = 61;
    public static final int	C_MYACC = 62;
    public static final int	C_BALANCEVOLUME = 63;
    public static final int	C_BALANCELOT = 64;
    public static final int	C_DONELOT = 65;
    public static final int	C_TRADENO = 66;
    public static final int	C_MARKETREF = 67;
    public static final int	C_NEGDEALREF = 68;
    public static final int	C_ACCID = 69;
    
    public static final int	C_ORDERIDCONTRA = 70;
    public static final int	C_ACCIDCONTRA = 71;
    public static final int	C_INVTYPECONTRA = 72;
    public static final int	C_TRADINGIDCONTRA = 73;
    //Tambahan untuk Order Amount
    public static final int	C_ORDERAMOUNT = 74;
    public static final int C_VALIDUNTIL = 75;//*gtc 24112014
    public static final int C_LASTUPDATE = 76;//*gtc 24112014
    public static final int CIDN_NUMBEROFFIELDS = 77;//*gtc 24112014
    //--Akhir Tambahan --
 	
 	public static final String C_BUY = "1";
    public static final String C_SELL = "2";
    
    public static final String C_DAYORDER = "0";
    public static final String C_SESSIONORDER = "S";
    
    
    public static final String C_ENTRY_TEMPORARY = "ET";
    public static final String C_TEMPORARY = "T";
    public static final String C_ENTRY = "o";
    public static final String C_OPEN = "O";
    public static final String C_PARTIAL_MATCH = "P";
    public static final String C_FULL_MATCH = "M";
    public static final String C_DELETE_REQUEST = "RD";
    public static final String C_DELETE = "D";
    public static final String C_WITHDRAW = "W";
    public static final String C_AMEND = "A";            
    public static final String C_REQUEST_WITHDRAW = "w";
    public static final String C_REJECTED = "8";
    public static final String C_REJECTED2 = "R";    	
    public static final String C_REQUEST_AMEND = "a";
    public static final String C_SENDING_ENTRY = "SE";
    public static final String C_SENDING_TEMPORARY = "ST";
    public static final String C_SENDING_DELETE = "SD";
    public static final String C_SENDING_WITHDRAW = "SW";
    public static final String C_SENDING_AMEND = "SA";
    public static final String C_SENDING_ENTRYSERVER = "SES";
    public static final String C_SENDING_TEMPORARYSERVER = "STS";
    public static final String C_SENDING_DELETESERVER = "SDS";
    public static final String C_SENDING_WITHDRAWSERVER = "SWS";
    public static final String C_SENDING_AMENDSERVER = "SAS";
    public static final String C_FAILED = "F";
    public static final String C_SPLIT = "S";
    
    public final static SimpleDateFormat formatdate = new SimpleDateFormat("HHmmss");

    
    public void calculate(){
    	
    	if(!getOrderIDContra().isEmpty() || getBoard().equalsIgnoreCase("NG")){
    		setLot(getLot() / Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE)));
    	}
    	
    	double lotsize = getVolume().doubleValue() / getLot().doubleValue();
    	setBalVol(new Double(getVolume().doubleValue() - getDoneVol().doubleValue()));
    	setBalLot(new Double(getBalVol().doubleValue() / lotsize));
    	setDoneLot(new Double(getDoneVol().doubleValue() / lotsize));
    	
    	
        buildAmount();
    }

	public void buildAmount() {
		// TODO Auto-generated method stub
		double OrderAmount = getVolume().doubleValue() == 0 ? 0 : getVolume().doubleValue() * getPrice().doubleValue();
        setOrderAmount(new Double(OrderAmount));
	}

	public Order(){
		super(CIDN_NUMBEROFFIELDS);
	}
	public Order(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 15: case 16: case 17: case 21: case 22: case 23: case 27: case 28: case 47: case 50: case 52: case 53: case 54:
			case 63: case 64: case 65: case 66: case 74: 	
			vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
		 	case 1: case 19: case 33: case 36: case 37: case 38: case 39: case 42: case 43: case 46: case 51: case 57:
		 		if (!vdata.elementAt(i).equals(""))
                    try {
                        vdata.setElementAt(formatdate.parse((String)vdata.elementAt(i)), i);
                    } catch (Exception ex){vdata.setElementAt(null,i);}
                else 
                    vdata.setElementAt(null, i);
				break;
			default:
				break;
			}
		}
		
		//Tambahan untuk Menghitung Order Amount
		//double OrderAmount = getVolume().doubleValue() == 0 ? 0 : getVolume().doubleValue() * getPrice().doubleValue();
        //setOrderAmount(new Double(OrderAmount));
		//--Akhir penambahan
	}
	
    public void updateVolume(double newVolume){
        if (getData(C_VOLUME) != null){
            double lotsize = ((Double)getData(C_VOLUME)).doubleValue() / ((Double)getData(C_LOT)).doubleValue(); 
            double diff = newVolume - ((Double)getData(C_VOLUME)).doubleValue();
            double newbalance = ((Double)getData(C_BALANCEVOLUME)).doubleValue() + diff;
            setData(new Double(newbalance), C_BALANCEVOLUME);
            setData(new Double(newVolume / lotsize), C_LOT);
            setData(new Double(newVolume), C_VOLUME);
            setData(new Double(newbalance / lotsize), C_BALANCELOT);
        } else {
            setData(new Double(newVolume), C_VOLUME);
        }
        
        buildAmount();
    }  
    
    public char getHandInst(){
    	if (getIsAdvertising().equals("0")){
    		return getBoard().equals("NG") ? '3' : '1';
    	} else {
    		return '2';
    	}
    }
    
    public String getId(){
		return (String) getData(C_ORDERID);
	}
	public void setId(String sinput){
		setData(sinput, C_ORDERID);
	}
    public String getAccId(){
		return (String) getData(C_ACCID);
	}
	public void setAccId(String sinput){
		setData(sinput, C_ACCID);
	}
	public String getTradingId(){
		return (String) getData(C_TRADINGID);
	}
	public void setTradingId(String sinput){
		setData(sinput, C_TRADINGID);
	}
    public String getStock(){
		return (String) getData(C_SECID);
	}
	public void setStock(String sinput){
		setData(sinput, C_SECID);
	}
    public String getBoard(){
		return (String) getData(C_BRDID);
	}
	public void setBoard(String sinput){
		setData(sinput, C_BRDID);
	}
    public String getMarketref(){
		return (String) getData(C_MARKETREF);
	}
	public void setMarketRef(String sinput){
		setData(sinput, C_MARKETREF);
	}
    public String getNegDealRef(){
		return (String) getData(C_NEGDEALREF);
	}
	public void setNegDealRef(String sinput){
		setData(sinput, C_NEGDEALREF);
	}
    public String getStatus(){
		return (String) getData(C_STATUSID);
	}
	public void setStatus(String sinput){
		setData(sinput, C_STATUSID);
	}
    public String getExchange(){
		return (String) getData(C_XCHID);
	}
	public void setExchange(String sinput){
		setData(sinput, C_XCHID);
	}
    public String getMarketOrderId(){
		return (String) getData(C_MARKETORDERID);
	}
	public void setMarketOrderId(String sinput){
		setData(sinput, C_MARKETORDERID);
	}
    public Double getPrice(){
		return (Double) getData(C_PRICE);
	}
	public void setPrice(Double sinput){
		setData(sinput, C_PRICE);
	}
	
	//Tambahan Untuk Order Amount
	public Double getOrderAmount(){
		return (Double) getData(C_ORDERAMOUNT);
	}
	
	public void setOrderAmount(Double sinput){
		setData(sinput, C_ORDERAMOUNT);
	}
	//-- Akhir Penambahan --
	
    public String getBOS(){
		return (String) getData(C_BUYORSELL);
	}
	public void setBOS(String sinput){
		setData(sinput, C_BUYORSELL);
	}
    public String getAmendTerminal(){
		return (String) getData(C_AMENDTERMINAL);
	}
	public void setAmendTerminal(String sinput){
		setData(sinput, C_AMENDTERMINAL);
	}
    public String getIsAdvertising(){
		return (String) getData(C_ISADVERTISING);
	}
	public void setIsAdvertising(String sinput){
		setData(sinput, C_ISADVERTISING);
	}
    public Double getVolume(){
		return (Double) getData(C_VOLUME);
	}
	public void setVolume(Double sinput){
		setData(sinput, C_VOLUME);
	}
    public Double getLot(){
		return (Double) getData(C_LOT);
	}
	public void setLot(Double sinput){
		setData(sinput, C_LOT);
	}
    public Double getBalVol(){
		return (Double) getData(C_BALANCEVOLUME);
	}
	public void setBalVol(Double sinput){
		setData(sinput, C_BALANCEVOLUME);
	}
    public Double getBalLot(){
		return (Double) getData(C_BALANCELOT);
	}
	public void setBalLot(Double sinput){
		setData(sinput, C_BALANCELOT);
	}
    public String getEntryBy(){
		return (String) getData(C_ENTRYRTBY);
	}
	public void setEntryBy(String sinput){
		setData(sinput, C_ENTRYRTBY);
	}
    public String getEntryTerminal(){
		return (String) getData(C_ENTRYRTTERMINAL);
	}
	public void setEntryTerminal(String sinput){
		setData(sinput, C_ENTRYRTTERMINAL);
	}
    public Date getEntryTime(){
		return (Date) getData(C_ENTRYRTTIME);
	}
	public void setEntryTime(Date sinput){
		setData(sinput, C_ENTRYRTTIME);
	}
    public Date getOpenTime(){
		return (Date) getData(C_OPENTIME);
	}
	public void setOpenTime(Date sinput){
		setData(sinput, C_OPENTIME);
	}
    public Date getOpenSendTime(){
		return (Date) getData(C_OPENSENDTIME);
	}
	public void setOpenSendTime(Date sinput){
		setData(sinput, C_OPENSENDTIME);
	}
    public String getWithdrawBy(){
		return (String) getData(C_WITHDRAWBY);
	}
	public void setWithdrawBy(String sinput){
		setData(sinput, C_WITHDRAWBY);
	}
    public Date getWithdrawTime(){
		return (Date) getData(C_WITHDRAWTIME);
	}
	public void setWithdrawTime(Date sinput){
		setData(sinput, C_WITHDRAWTIME);
	}
    public Date getAmendSendTime(){
		return (Date) getData(C_AMENDSENDTIME);
	}
	public void setAmendSendTime(Date sinput){
		setData(sinput, C_AMENDSENDTIME);
	}
    public Date getWithdrawSendTime(){
		return (Date) getData(C_WITHDRAWSENDTIME);
	}
	public void setWithdrawSendTime(Date sinput){
		setData(sinput, C_WITHDRAWSENDTIME);
	}
    public String getAmendBy(){
		return (String) getData(C_AMENDBY);
	}
	public void setAmendBy(String sinput){
		setData(sinput, C_AMENDBY);
	}
    public String getWithdrawTerminal(){
		return (String) getData(C_WITHDRAWTERMINAL);
	}
	public void setWithdrawTerminal(String sinput){
		setData(sinput, C_WITHDRAWTERMINAL);
	}
    public Date getAmendTime(){
		return (Date) getData(C_AMENDTIME);
	}
	public void setAmendTime(Date sinput){
		setData(sinput, C_AMENDTIME);
	}
    public Date getRejectTime(){
		return (Date) getData(C_REJECTTIME);
	}
	public void setRejectTime(Date sinput){
		setData(sinput, C_REJECTTIME);
	}
    public String getNote(){
		return (String) getData(C_NOTE);
	}
	public void setNote(String sinput){
		setData(sinput, C_NOTE);
	}
    public String getOrdType(){
		return (String) getData(C_ORDTYPE);
	}
	public void setOrdType(String sinput){
		setData(sinput, C_ORDTYPE);
	}
    public String getPrevOrderId(){
		return (String) getData(C_PREVORDERID);
	}
	public void setPrevOrderId(String sinput){
		setData(sinput, C_PREVORDERID);
	}
    public String getNextOrderId(){
		return (String) getData(C_NEXTORDERID);
	}
	public void setNextOrderId(String sinput){
		setData(sinput, C_NEXTORDERID);
	}
    public String getPrevMarketOrderId(){
		return (String) getData(C_PREVMARKETORDERID);
	}
	public void setPrevMarketOrderId(String sinput){
		setData(sinput, C_PREVMARKETORDERID);
	}
    public String getNextMarketOrderId(){
		return (String) getData(C_NEXTMARKETORDERID);
	}
	public void setNextMarketOrderId(String sinput){
		setData(sinput, C_NEXTMARKETORDERID);
	}
	
    public String getIsOverLimit(){
		return (String) getData(C_ISOVERLIMIT);
	}
	public void setIsOverLimit(String sinput){
		setData(sinput, C_ISOVERLIMIT);
	}
    public String getSplitBy(){
		return (String) getData(C_SPLITBY);
	}
	public void setSplitBy(String sinput){
		setData(sinput, C_SPLITBY);
	}
    public String getSplitTerminal(){
		return (String) getData(C_SPLITTERMINAL);
	}
	public void setSplitTerminal(String sinput){
		setData(sinput, C_SPLITTERMINAL);
	}
    public String getIsShortSell(){
		return (String) getData(C_ISSHORTSELL);
	}
	public void setIsShortSell(String sinput){
		setData(sinput, C_ISSHORTSELL);
	}	
    public Date getMatchTime(){
		return (Date) getData(C_MATCHTIME);
	}
	public void setMatchTime(Date sinput){
		setData(sinput, C_MATCHTIME);
	}
    public Date getBasketTime(){
		return (Date) getData(C_BASKETSENDTIME);
	}
	public void setBasketTime(Date sinput){
		setData(sinput, C_BASKETSENDTIME);
	}
    public Double getTLBefore(){
		return (Double) getData(C_TLBEFORE);
	}
	public void setTLBefore(Double sinput){
		setData(sinput, C_TLBEFORE);
	}
    public Double getMatchCounter(){
		return (Double) getData(C_MATCHCOUNTER);
	}
	public void setMatchCounter(Double sinput){
		setData(sinput, C_MATCHCOUNTER);
	}
    public Double getTotalCounterSplit(){
		return (Double) getData(C_TOTALCOUNTERSPLIT);
	}
	public void setTotalcounterSplit(Double sinput){
		setData(sinput, C_TOTALCOUNTERSPLIT);
	}
    public Double getLQValueBefore(){
		return (Double) getData(C_LQVALUEBEFORE);
	}
	public void setLQValueBefore(Double sinput){
		setData(sinput, C_LQVALUEBEFORE);
	}
    public Double getCounter(){
		return (Double) getData(C_COUNTER);
	}
	public void setCounter(Double sinput){
		setData(sinput, C_COUNTER);
	}
    public Double getSplitTo(){
		return (Double) getData(C_SPLITTO);
	}
	public void setSplitTo(Double sinput){
		setData(sinput, C_SPLITTO);
	}
    public Double getPrevDoneVolume(){
		return (Double) getData(C_PREVDONEVOLUME);
	}
	public void setPrevDoneVolume(Double sinput){
		setData(sinput, C_PREVDONEVOLUME);
	}
    public Double getPortBefore(){
		return (Double) getData(C_AVPORTBEFORE);
	}
	public void setPortBefore(Double sinput){
		setData(sinput, C_AVPORTBEFORE);
	}	
    public Double getRatioBefore(){
		return (Double) getData(C_RATIOBEFORE);
	}
	public void setRatioBefore(Double sinput){
		setData(sinput, C_RATIOBEFORE);
	}	
    public Date getOrderDate(){
		return (Date) getData(C_ORDERDATE);
	}
	public void setOrderDate(Date sinput){
		setData(sinput, C_ORDERDATE);
	}	
    public String getUpdateDate(){
		return (String) getData(C_UPDATEDATE);
	}
	public void setUpdateDate(Date sinput){
		setData(sinput, C_UPDATEDATE);
	}	
    public String getInvType(){
		return (String) getData(C_INVTYPE);
	}
	public void setInvType(String sinput){
		setData(sinput, C_INVTYPE);
	}	
    public String getComplianceId(){
		return (String) getData(C_COMPLAINCEID);
	}
	public void setComplianceId(String sinput){
		setData(sinput, C_COMPLAINCEID);
	}	
    public String getUser(){
		return (String) getData(C_USERID);
	}
	public void setUser(String sinput){
		setData(sinput, C_USERID);
	}	
    public String getUpdateBy(){
		return (String) getData(C_UPDATEBY);
	}
	public void setUpdateBy(String sinput){
		setData(sinput, C_UPDATEBY);
	}	
    public String getTerminalId(){
		return (String) getData(C_TERMINALID);
	}
	public void setTerminalId(String sinput){
		setData(sinput, C_TERMINALID);
	}	
    public String getIsFloor(){
		return (String) getData(C_ISFLOOR);
	}
	public void setIsFloor(String sinput){
		setData(sinput, C_ISFLOOR);
	}	
	
    public String getOrderType(){
		return (String) getData(C_ORDERTYPE);
	}
	public void setOrderType(String sinput){
		setData(sinput, C_ORDERTYPE);
	}	
    public String getTradeNo(){
		return (String) getData(C_TRADENO);
	}
	public void setTradeNo(String sinput){
		setData(sinput, C_TRADENO);
	}	
    public String getBroker(){
		return (String) getData(C_BROKERID);
	}
	public void setBroker(String sinput){
		setData(sinput, C_BROKERID);
	}	
    public String getContraBroker(){
		return (String) getData(C_CONTRABROKERID);
	}
	public void setContraBroker(String sinput){
		setData(sinput, C_CONTRABROKERID);
	}	
    public String getContraUser(){
		return (String) getData(C_CONTRAUSERID);
	}
	public void setContraUser(String sinput){
		setData(sinput, C_CONTRAUSERID);
	}	
    public String getCurrency(){
		return (String) getData(C_CURID);
	}
	public void setCurrency(String sinput){
		setData(sinput, C_CURID);
	}	
    public String getOrderIDContra(){
		return (String) getData(C_ORDERIDCONTRA);
	}
	public void setOrderIDContra(String sinput){
		setData(sinput, C_ORDERIDCONTRA);
	}	
    public String getAccIDContra(){
		return (String) getData(C_ACCIDCONTRA);
	}
	public void setAccIDContra(String sinput){
		setData(sinput, C_ACCIDCONTRA);
	}	
    public String getInvTypeContra(){
		return (String) getData(C_INVTYPECONTRA);
	}
	public void setInvTypeContra(String sinput){
		setData(sinput, C_INVTYPECONTRA);
	}	
    public String getTradingIDContra(){
		return (String) getData(C_TRADINGIDCONTRA);
	}
	public void setTradingIDContra(String sinput){
		setData(sinput, C_TRADINGIDCONTRA);
	}		
    public String getIsBasketOrder(){
		return (String) getData(C_ISBASKETORDER);
	}
	public void setIsBasketOrder(String sinput){
		setData(sinput, C_ISBASKETORDER);
	}	
	
    public Double getDoneLot(){
		return (Double) getData(C_DONELOT);
	}
	public void setDoneLot(Double sinput){
		setData(sinput, C_DONELOT);
	}		
    public Double getDoneVol(){
		return (Double) getData(C_DONEVOLUME);
	}
	public void setDoneVol(Double sinput){
		setData(sinput, C_DONEVOLUME);
	}		
    public boolean isMyAccount(){
        return ((Double)getData(C_MYACC)).doubleValue() == 1;
    }
    public void setMyAccount(boolean sinput){
        setData(new Double(sinput?"1":"0"), C_MYACC);
    }    
    //gtc 24112014
    public Date getcValiduntil() {
		return (Date)getData(C_VALIDUNTIL);
	}
	public void setValiduntil (Date sinput)	{
		setData (sinput , C_VALIDUNTIL);
	}

	public Date getLastUpdate(){
		return (Date)getData(C_LASTUPDATE);
	}
	public void setLastUpdate(Date sinput){
		setData(sinput, C_LASTUPDATE);
	}
	
    @Override
	public String toString(){
    	return getId()+" "+getTradingId()+" "+getStock()+" "+getPrice().doubleValue()+" "+getLot()+ " "+getTradeNo().toString();
    }
    
    
}