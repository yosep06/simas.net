package eqtrade.trading.model;

import java.awt.Component;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

public class UserTypeRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	 
	 @Override
	public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
	 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
	 	return component;
    }
	
	 @Override
	public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? SwingConstants.RIGHT : SwingConstants.LEFT);
	             if (dat instanceof Double) {
	            		 setText(formatter.format(dat));
	             } else if (dat == null){
	                 setText("");
	             } else {
	            	 setText(" "+dat.toString());
	             }
	         }
	     } catch (Exception e){
	     }
	 }
}
