package eqtrade.trading.model;

import com.vollux.idata.Column;
import com.vollux.idata.Row;
import com.vollux.idata.indirection.ImmutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplay;
import com.vollux.idata.indirection.MutableIDisplayIData;

public class DueDateField extends Column implements MutableIDisplayIData{
	 public DueDateField(Row source, int idx) throws Exception{
	        super(source, idx);
	    }
	    
	    @Override
		public MutableIDisplay getMutableIDisplay() {
			return DueDateDef.render;
		}

		@Override
		public ImmutableIDisplay getImmutableIDisplay() {
			return DueDateDef.render;
		}
}
