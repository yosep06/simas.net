package eqtrade.trading.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.Vector;


import com.vollux.idata.Row;

import eqtrade.trading.core.Utils;

public  abstract class Model extends Row implements Serializable {
	private static final long serialVersionUID = 1L;
	protected Vector 	vdata;
	protected int			nsize;
	
	public Model(int nsizepar){
		this.nsize = nsizepar;
		this.vdata = new Vector(nsize, 10);
		this.initData();
		convertType();
	}

	public Model(int nsizepar, String smsg){
		this.nsize = nsizepar;
		if (smsg.trim().length() > 0){
			vdata = Utils.parser(smsg, "|");
			int diff = nsize - vdata.size();
			for (int i=0;i<diff;i++){
				vdata.addElement(new String(""));
			}
		} else {
			this.vdata = new Vector(nsize, 10);
			this.initData();
		}
		convertType();
	}
	
	protected abstract void convertType();

	protected void initData(){
		vdata.addAll(Collections.nCopies(nsize, ""));
	}

	public void fromString(String smsg){
		Vector vtemp = Utils.parser(smsg, "|");
		Collections.copy(vdata, vtemp);
		if (vdata.size()<nsize){
			vdata.addElement(new String(""));
		}
		convertType();
	}

	public void fromVector(Vector vmsg){
		Collections.copy(vdata, vmsg);
	}

	public Vector getData(){
		return (Vector) vdata.clone();
	}

	@Override
	public int getColumnCount() {
		return nsize;
	}

	@Override
	public Object getData(int arg0){
		return this.vdata.elementAt(arg0);
	}

	@Override
	public void setData(Object arg0, int arg1){
		this.vdata.setElementAt(arg0, arg1);
	}
}