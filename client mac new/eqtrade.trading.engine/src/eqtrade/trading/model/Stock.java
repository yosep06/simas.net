package eqtrade.trading.model;

import eqtrade.trading.core.Utils;

public final class Stock extends Model {
    private static final long serialVersionUID = 309177931626178221L;
    public static final int	C_ID = 0;
    public static final int   C_NAME = 1;
	public static final int	C_LOTSIZE = 2;
    public static final int   C_CLOSINGPRICE = 3;
	public static final int	C_LASTPRICE = 4;
	public static final int   C_PREOPENING = 5;
	public static final int 	CIDN_NUMBEROFFIELDS = 6;

	public Stock(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Stock(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 2: case 3: case 4:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;				
			default:
				break;
			}
		}
	}	

    public boolean isPreopening(){
        return ((Double)getData(C_PREOPENING)).doubleValue() == 1;
    }
    public void setPreopening(boolean sinput){
        setData(new Double(sinput?"1":"0"), C_PREOPENING);
    }    
	
	
	public String getSecurities(){
		return (String) getData(C_ID);
	}
	public void setSecurities(String sinput){
		setData(sinput, C_ID);
	}
	public String getName(){
		return (String) getData(C_NAME);
	}
	public void setName(String sinput){
		setData(sinput, C_NAME);
	}	
	public Double getClosingPrice(){
		return (Double) getData(C_CLOSINGPRICE);
	}
	public void setClosingPrice(Double sinput){
		setData(sinput, C_CLOSINGPRICE);
	}    
    public Double getLotSize(){
        return (Double) getData(C_LOTSIZE);
    }
    public void setLotSize(Double sinput){
        setData(sinput, C_LOTSIZE);
    }
    public Double getLastPrice(){
        return (Double) getData(C_LASTPRICE);
    }
    public void setLastPrice(Double sinput){
        setData(sinput, C_LASTPRICE);
    }
}