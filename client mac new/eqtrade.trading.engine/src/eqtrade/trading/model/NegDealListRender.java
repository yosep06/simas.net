package eqtrade.trading.model;

import java.awt.Color;
import java.awt.Component;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.trading.core.SelectedBorder;
import eqtrade.trading.core.TradingSetting;

public class NegDealListRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
	 private static NumberFormat formatter = new DecimalFormat("#,##0  ");
     private String strFieldName = new String("");

	 public static SimpleDateFormat formattime = new SimpleDateFormat("HH:mm:ss");
     private static Color newBack;
     private static Color newFore;
     private static SelectedBorder border = new SelectedBorder();
	 private static NegDealList ord;
	 @Override
	public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
	        strFieldName = table.getColumnName(column);
		 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
	        
		 	try  {
		        strFieldName = table.getColumnName(column);
				newBack =  (column % 2) == 0 ? TradingSetting.getColor(TradingSetting.C_BACKGROUND) : TradingSetting.getColor(TradingSetting.C_BACKGROUNDEVEN);
	
			 	if (value instanceof MutableIData){
			  		ord = (NegDealList)((MutableIData)value).getSource();
			  		if (ord.getBuySell().equals(Order.C_BUY)){
			            newFore = TradingSetting.getColor(TradingSetting.C_BUY);   
			  		} else {
			            newFore = TradingSetting.getColor(TradingSetting.C_SELL);   
			  		} 
			  		if (ord.getStatusID().equals("R") || ord.getStatusID().equals("W") || ord.getStatusID().equals("A") || 
			  			ord.getStatusID().equals("F") || ord.getStatusID().equals("D")){
			  			newFore = newFore.darker();
			  		} else if (ord.getStatusID().equals("SES") || ord.getStatusID().equals("STS") || ord.getStatusID().equals("SWS") || ord.getStatusID().equals("SAS") || ord.getStatusID().equals("SDS")){
			  			newFore = TradingSetting.getColor((TradingSetting.C_ZERO)).darker();
			  		} else if (ord.getStatusID().equals("SE") || ord.getStatusID().equals("ST") || ord.getStatusID().equals("SW") || ord.getStatusID().equals("SA") || ord.getStatusID().equals("SD")){
			  			newFore = TradingSetting.getColor((TradingSetting.C_ZERO));				  		
			  		} else if (ord.getStatusID().equals("F")){
			  			newFore = TradingSetting.getColor(TradingSetting.C_OTHER);   
			  		}
			 	} 
			 	
			 	if  (isSelected) {
		            ((JLabel)component).setBorder(border);
		        }
		        component.setBackground(newBack);
		        component.setForeground(newFore);          
	 	} catch (Exception ex){
	 		ex.printStackTrace();
	 	}
		 	return component;
    }
	
	 @Override
	public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	        	 
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
	             NegDealList o = (NegDealList)args.getSource();
	             String strTemp;
                 setHorizontalAlignment((dat instanceof Double)? SwingConstants.RIGHT : SwingConstants.LEFT);
	             	if (dat instanceof Double) {
			    				double d = Math.floor(((Double)dat).doubleValue());
			    				double s = (((Double)dat).doubleValue()) - d;
			    				if (s > 0){
				    				formatter.setMaximumFractionDigits(2);
				    				formatter.setMinimumFractionDigits(2);
			    				} else {
				    				formatter.setMaximumFractionDigits(0);
				    				formatter.setMinimumFractionDigits(0);	    					
			    				}
								setText(formatter.format(dat));					
		    		} else if (dat instanceof Date){
	    				setText(formattime.format((Date)dat));
		    		} else if (dat instanceof Timestamp){
		    			setText(formattime.format(new Date(((Timestamp)dat).getTime())));
		    		} else if (strFieldName.equals(NegDealListDef.dataHeader[NegDealList.C_STATUSID])){
		    			if (dat!=null && o!=null){
		    				strTemp = (String)OrderRender.hashStatus.get(dat);
			    			if (strTemp == null) strTemp = dat+" ";
			    			setText(strTemp);
		    			} else {
		    				strTemp = (String)OrderRender.hashStatus.get(dat);
			    			if (strTemp == null) strTemp = dat+" ";
			    			setText(strTemp);
		    			}
		    		} else if (strFieldName.equals(NegDealListDef.dataHeader[NegDealList.C_ORDERTYPE])){
		    			strTemp = (String)OrderRender.hashType.get(dat);
		    			if (strTemp == null) strTemp = dat+" ";
		    			setText(strTemp);
		    		} else if (strFieldName.equals(NegDealListDef.dataHeader[NegDealList.C_ORDTYPE])){
		    				if (dat != null) {
			    				if (dat.equals("7") || dat.equals("E")) setText("Limited Order");
			    				else if (dat.equals("1")) setText("Market Order");
				    			else setText(dat+" ");
		    				} else {
		    					setText("");
		    				}
		    		} else if (strFieldName.equals(NegDealListDef.dataHeader[NegDealList.C_ISADVERTISING])){
		    				if (dat == null){
		    					setText("");
		    				} else {
		    					setText(dat.equals("1") ? "Yes" : "No");
		    				}
                    } else if (strFieldName.equals(NegDealListDef.dataHeader[NegDealList.C_BUYSELL])){
                        strTemp = (String)OrderRender.hashSide.get(dat);
                        if (strTemp == null) strTemp = dat+" Unknown";
                        setText(strTemp);
		            } else if (dat == null){
		                setText("");
		            } else {
		                setText(" "+dat.toString());
		            }
	         }
	     } catch (Exception e){
	    	 e.printStackTrace();
	     }
	 }
}
