package eqtrade.trading.model;


public final class UserType extends Model {
    private static final long serialVersionUID = 309177931626178221L;
    public static final int	C_ID = 0;
    public static final int  	C_NAME = 1;
	public static final int 	CIDN_NUMBEROFFIELDS = 2;

	public UserType(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public UserType(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
	}	

	public String getId(){
		return (String) getData(C_ID);
	}
	public void setId(String sinput){
		setData(sinput, C_ID);
	}
	public String getName(){
		return (String) getData(C_NAME);
	}
	public void setName(String sinput){
		setData(sinput, C_NAME);
	}	  
}