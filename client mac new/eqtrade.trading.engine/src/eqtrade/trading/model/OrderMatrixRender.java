package eqtrade.trading.model;

import java.awt.Color;
import java.awt.Component;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import com.vollux.idata.indirection.MutableIData;

import eqtrade.trading.core.SelectedBorder;
import eqtrade.trading.core.TradingSetting;

public class OrderMatrixRender extends DefaultTableCellRenderer{
	 private static final long serialVersionUID = 213203526059607433L;
     private String strFieldName = new String("");
	 public static NumberFormat formatter = new DecimalFormat("#,##0  ");
	 public static SimpleDateFormat formattime = new SimpleDateFormat("HH:mm:ss / yyyy-MM-dd");
	 public static SimpleDateFormat formatdate = new SimpleDateFormat("HH:mm:ss / yyyy-MM-dd");
     private static SelectedBorder border = new SelectedBorder();
     private static Color newBack;
     private static Color newFore;


	 @Override
	public Component getTableCellRendererComponent(JTable table, Object value,
	 	boolean isSelected, boolean hasFocus, int row, int column){
		 	Component component = super.getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
		 	try  {
			        strFieldName = table.getColumnName(column);
					newBack =  (column % 2) == 0 ? TradingSetting.getColor(TradingSetting.C_BACKGROUND) : TradingSetting.getColor(TradingSetting.C_BACKGROUNDEVEN);
					newFore = TradingSetting.getColor(TradingSetting.C_FOREGROUND);   
				 	if  (isSelected) {
			            ((JLabel)component).setBorder(border);
			        }
			        component.setBackground(newBack);
			        component.setForeground(newFore);          
		 	} catch (Exception ex){
		 		ex.printStackTrace();
		 	}
		 	return component;
    }
	
	 @Override
	public void setValue(Object value){
	     try {
	         if (value instanceof MutableIData) {
	             MutableIData args = (MutableIData)value;
	             Object dat = args.getData();
                 setHorizontalAlignment((dat instanceof Double)? SwingConstants.RIGHT : SwingConstants.LEFT);
	             	if (dat instanceof Double) {
	             		 setText(formatter.format(dat));
		    		} else if (dat instanceof Date){
		    				setText(formatdate.format((Date)dat));
		    		} else if (dat instanceof Timestamp){
		    			setText(formattime.format(new Date(((Timestamp)dat).getTime())));
		    		} else if (dat == null){
		                setText(" ");
		            } else {
		                setText(" "+dat.toString());
		            }
	         }
	     } catch (Exception e){
	     }
	 }
}
