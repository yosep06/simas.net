package eqtrade.trading.model;


public final class UserProfile extends Model {
    private static final long serialVersionUID = 309177931626178221L;
    public static final int	C_USERID = 0;
    public static final int  C_PROFILEID = 1;
    public static final int  C_MENUID = 2;
    public static final int  C_ISALLOWED = 3;
    public static final int  C_USERTYPE = 4; // yosep expktp
	public static final int 	CIDN_NUMBEROFFIELDS = 5;

	public UserProfile(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public UserProfile(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
//		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
//			switch (i) {
//			case 2: case 3: case 4:
//				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
//				break;				
//			default:
//				break;
//			}
//		}
	}	

	public String getUserId(){
		return (String) getData(C_USERID);
	}
	public void setUserId(String sinput){
		setData(sinput, C_USERID);
	}
	public String getProfileId(){
		return (String) getData(C_PROFILEID);
	}
	public void setProfileId(String sinput){
		setData(sinput, C_PROFILEID);
	}	
	public String getMenuId(){
		return (String) getData(C_MENUID);
	}
	public void setMenuId(String sinput){
		setData(sinput, C_MENUID);
	}    
    public boolean isAllowed(){
        return ((String)getData(C_ISALLOWED)).equals("1");
    }
	public String getIsAllowed(){
		return (String) getData(C_ISALLOWED);
	}
	public void setIsAllowed(String sinput){
		setData(sinput, C_ISALLOWED);
	}
//yosep expktp
	public String getUsertype() {
		return (String) getData(C_USERTYPE);
	}    
	
	public void setUsertype(String sinput){
		setData(sinput, C_USERTYPE);
	}
}