package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class BuySellDetailDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader);
        htable.put("alignment", defaultHeaderAlignment);
        htable.put("width", defaultColumnWidth);
        htable.put("order", defaultColumnOrder);
        htable.put("sorted", columnsort);        
        htable.put("sortcolumn",new Integer(3));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}	
	
    public static final int	C_TRADINGID = 0;
    public static final int C_ACCID = 1;
	public static final int	C_BUYORSELL = 2;
	public static final int C_TSPAN = 3;
	public static final int C_SPANDATE = 4;
	public static final int C_VALUE = 5;

	public static String[] dataHeader = new String[]{
    	"ClientID", "AccID", "BuyOrSell", "Tx", "Date", "Value"
    };

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.RIGHT   };

	public  static int[] defaultColumnWidth = new int[]{
			0,0,0,20,50,80
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5
	};	

	public static boolean[] columnsort = new boolean[]{
			false, false, false, true, false, false
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(BuySellDetail.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row bsdet) {
		List vec = new Vector(BuySellDetail.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < BuySellDetail.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new BuySellDetailField(bsdet, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static BSDRender render = new BSDRender();
	
    static class BSDRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new BuySellDetailRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((BuySellDetailRender)renderer).setValue(value);
			return renderer;
		}
	}
}
