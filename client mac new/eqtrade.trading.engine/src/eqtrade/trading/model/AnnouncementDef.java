package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class AnnouncementDef {
	public static Hashtable getTableDef(){
		Hashtable htable = new Hashtable();
		htable.put("header", dataHeader);
        htable.put("alignment", defaultHeaderAlignment);
        htable.put("width", defaultColumnWidth);
        htable.put("order", defaultColumnOrder);
        htable.put("sorted", columnsort);        
        htable.put("sortcolumn",new Integer(Announcement.C_ID));
        //htable.put("sortdescending", new Boolean(true));
        htable.put("sortascending", new Boolean(false));
        htable.put("hide", columhide);
		return htable;
	}
	public static final int C_ID =0;
	public static final int C_Date=1;
	public static final int C_TIME=2;
	public static final int C_TITLE=3;
	public static final int C_CONTENT=4;
	public static final int C_ENTRYTIME=5;
	public static final int C_ENTRYBY=6;
	public static final int C_STATUS=7;
	public static final int C_STATUSTIME=8;
	public static final int C_STATUSBY=9;

	
	public static int[] columhide = new int[]{
		
	};
	
    public static String[] dataHeader = new String[]{
    	"ID","Date","Time","Title","Content","Entry Time","Entry By","Status","Status Time","Status By"
    };

    public static int[] defaultHeaderAlignment = new int[]{
    	SwingConstants.CENTER,SwingConstants.CENTER,SwingConstants.CENTER,SwingConstants.CENTER,SwingConstants.CENTER,
    	SwingConstants.CENTER,SwingConstants.CENTER,SwingConstants.CENTER,SwingConstants.CENTER,SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			0,100,100,380,0,0,0,0,0,0
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3,4,5,6,7,8,9
	};	

	public static boolean[] columnsort = new boolean[]{
		true,true,true,true,true,true,true,true,true,true
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(Announcement.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row acc) {
		List vec = new Vector(Announcement.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Announcement.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new AnnouncementField(acc, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static AnnRender render = new AnnRender();
	
    static class AnnRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new AnnouncementRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((AnnouncementRender)renderer).setValue(value);
			return renderer;
		}
	}

}
