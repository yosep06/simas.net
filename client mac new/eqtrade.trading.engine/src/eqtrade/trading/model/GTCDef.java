package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class GTCDef {
	public static Hashtable getTableDef() {
		Hashtable htable = new Hashtable();
		htable.put("header", dataHeader.clone());
		htable.put("alignment", defaultHeaderAlignment.clone());
		htable.put("width", defaultColumnWidth.clone());
		htable.put("order", defaultColumnOrder.clone());
		htable.put("sorted", columnsort.clone());
		htable.put("sortcolumn", new Integer(33));
		htable.put("sortascending", new Boolean(true));
		htable.put("hide", columnhide.clone());
		return htable;
	}


	public static String[] dataHeader = new String[] { "OrderID", "OrderDate",
			"MarketID", "BoardID", "BrokerID", "BuySell", "OrdType",
			"OrderExpire", "StockID", "Advertisement", "ClientID",
			"InvestorType", "CurrencyID", "CounterPartBrokerID",
			"CounterparUserID", "Price", "Lot", "Volume", "BasketOrder",
			"BasketSendTime", "OrderStatus", "TradeLimit", "StockBalance",
			"CurrentRatio", "OverLimit", "Short", "MarketOrderID",
			"DoneVolume", "PrevDoneVolume", "PrevOrderID", "NextOrderID",
			"PrevMarketOrderID", "NextMarketOrderID", "EntryTime", "EntryBy",
			"EntryTerminal", "RejectTime", "OpenTime", "OpenSendTime",
			"AmendTime", "AmendBy", "AmendTerminal", "AmendSendTime",
			"WithdrawTime", "WithdrawBy", "WithdrawTerminal",
			"WithdrawSendTime", "SplitTo", "SplitBy", "SplitTerminal",
			"MatchCounter", "MatchTime", "Liquidity", "Counter",
			"TotalCounterSplit", "Note", "ComplainceID", "LastUpdate",
			"UserID", "SysUserID", "TerminalID", "Floor", "MyACC", "BalVolume",
			"BalLot", "DoneLot", "MarketTradeId", "MarketRef", "NegDealRef",
			"AccID", "OrderIDContra", "AccIDContra", "InvTypeContra",
			"ClientIDContra", "Amount", "ValidUntil", "Reconcile Time" };

	public static int[] defaultHeaderAlignment = new int[] {
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER, SwingConstants.CENTER,
			SwingConstants.CENTER			};

	public static int[] defaultColumnWidth = new int[] { 100, 100, 100, 100,
			100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100,
			100,
			100,
			100,
			// hide
			100, 100, 100, 100, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
			00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
			00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00, 00,
			00, 00, 00, 00, 00, 00, 00, 00

	};

	public static int[] defaultColumnOrder = new int[] {

		 Order.C_TRADINGID,Order.C_ENTRYRTTIME, Order.C_ORDERID, Order.C_BRDID, Order.C_BUYORSELL,
			Order.C_SECID, Order.C_LOT, Order.C_PRICE, Order.C_ORDERAMOUNT,
			Order.C_VALIDUNTIL, Order.C_ENTRYRTBY, Order.C_STATUSID,			 
			Order.C_BALANCELOT, Order.C_DONELOT, Order.C_VOLUME,
			Order.C_BALANCEVOLUME, Order.C_DONEVOLUME,Order.C_WITHDRAWTIME,
			Order.C_WITHDRAWTERMINAL,Order.C_WITHDRAWBY,Order.C_ENTRYRTTERMINAL,
			Order.C_LASTUPDATE,
			Order.C_NEXTORDERID,Order.C_MARKETORDERID,
			 Order.C_ORDTYPE, Order.C_TLBEFORE,
			Order.C_ORDERDATE, Order.C_XCHID, Order.C_BROKERID,
			Order.C_ORDERTYPE, Order.C_ISADVERTISING, Order.C_INVTYPE,
			Order.C_CURID, Order.C_CONTRABROKERID, Order.C_CONTRAUSERID,
			Order.C_ISBASKETORDER, Order.C_BASKETSENDTIME,Order.C_AMENDTIME,
			Order.C_AVPORTBEFORE, Order.C_RATIOBEFORE, Order.C_ISOVERLIMIT,
			Order.C_ISSHORTSELL,  Order.C_PREVDONEVOLUME,
			Order.C_PREVORDERID, Order.C_NOTE, Order.C_MATCHTIME,
			Order.C_PREVMARKETORDERID, Order.C_NEXTMARKETORDERID,
			 Order.C_REJECTTIME, Order.C_OPENTIME,
			Order.C_OPENSENDTIME, Order.C_AMENDBY, Order.C_AMENDTERMINAL,
			Order.C_AMENDSENDTIME, 
			 Order.C_WITHDRAWSENDTIME,
			Order.C_SPLITTO, Order.C_SPLITBY, Order.C_SPLITTERMINAL,
			Order.C_MATCHCOUNTER, Order.C_LQVALUEBEFORE, Order.C_COUNTER,
			Order.C_TOTALCOUNTERSPLIT, Order.C_COMPLAINCEID,
			Order.C_UPDATEDATE, Order.C_USERID, Order.C_UPDATEBY,
			Order.C_TERMINALID, Order.C_ISFLOOR, Order.C_MYACC,
			 Order.C_TRADENO, Order.C_MARKETREF,
			Order.C_NEGDEALREF, Order.C_ACCID, Order.C_ORDERIDCONTRA,
			Order.C_ACCIDCONTRA, Order.C_INVTYPECONTRA,
			Order.C_TRADINGIDCONTRA };

	
	public static boolean[] columnsort = new boolean[] { true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true, true, true, true,
			true, true, true, true, true, true, true, true };

	public static int[] columnhide = new int[] { 
		 Order.C_ORDTYPE,
		 Order.C_NEXTORDERID,Order.C_MARKETORDERID,
		 Order.C_TLBEFORE,
		Order.C_ORDERDATE, Order.C_XCHID, Order.C_BROKERID,
		Order.C_ORDERTYPE, Order.C_ISADVERTISING, Order.C_INVTYPE,
		Order.C_CURID, Order.C_CONTRABROKERID, Order.C_CONTRAUSERID,
		Order.C_ISBASKETORDER, Order.C_BASKETSENDTIME,Order.C_AMENDTIME,
		Order.C_AVPORTBEFORE, Order.C_RATIOBEFORE, Order.C_ISOVERLIMIT,
		Order.C_ISSHORTSELL,  Order.C_PREVDONEVOLUME,
		Order.C_PREVORDERID, Order.C_NOTE, Order.C_MATCHTIME,
		Order.C_PREVMARKETORDERID, Order.C_NEXTMARKETORDERID,
		 Order.C_REJECTTIME, Order.C_OPENTIME,
		Order.C_OPENSENDTIME, Order.C_AMENDBY, Order.C_AMENDTERMINAL,
		Order.C_AMENDSENDTIME, 
		 Order.C_WITHDRAWSENDTIME,
		Order.C_SPLITTO, Order.C_SPLITBY, Order.C_SPLITTERMINAL,
		Order.C_MATCHCOUNTER, Order.C_LQVALUEBEFORE, Order.C_COUNTER,
		Order.C_TOTALCOUNTERSPLIT, Order.C_COMPLAINCEID,
		Order.C_UPDATEDATE, Order.C_USERID, Order.C_UPDATEBY,
		Order.C_TERMINALID, Order.C_ISFLOOR, Order.C_MYACC,
		 Order.C_TRADENO, Order.C_MARKETREF,
		Order.C_NEGDEALREF, Order.C_ACCID, Order.C_ORDERIDCONTRA,
		Order.C_ACCIDCONTRA, Order.C_INVTYPECONTRA,
		Order.C_TRADINGIDCONTRA
		 /*Order.C_MYACC, 
																 * Order.C_DONELOT
																 * ,
																 
	Order.C_TRADENO, Order.C_MARKETREF, Order.C_ACCID, Order.C_ACCIDCONTRA,
			Order.C_ORDERID, Order.C_MARKETORDERID, Order.C_DONELOT,
			Order.C_BALANCELOT, Order.C_AMENDTIME, Order.C_WITHDRAWTIME,
			Order.C_ORDTYPE, Order.C_TLBEFORE, Order.C_ORDERDATE,
			Order.C_XCHID, Order.C_BROKERID, Order.C_ORDERTYPE,
			Order.C_ISADVERTISING, Order.C_INVTYPE, Order.C_CURID,
			Order.C_CONTRABROKERID, Order.C_CONTRAUSERID, Order.C_VOLUME,
			Order.C_ISBASKETORDER, Order.C_BASKETSENDTIME,
			Order.C_AVPORTBEFORE, Order.C_RATIOBEFORE, Order.C_ISOVERLIMIT,
			Order.C_ISSHORTSELL, Order.C_DONEVOLUME, Order.C_PREVDONEVOLUME,
			Order.C_PREVORDERID, Order.C_NEXTORDERID,
			Order.C_PREVMARKETORDERID, Order.C_NEXTMARKETORDERID,
			Order.C_ENTRYRTTERMINAL, Order.C_REJECTTIME, Order.C_OPENTIME,
			Order.C_OPENSENDTIME, Order.C_AMENDBY, Order.C_AMENDTERMINAL,
			Order.C_AMENDSENDTIME, Order.C_WITHDRAWBY,
			Order.C_WITHDRAWTERMINAL, Order.C_WITHDRAWSENDTIME,
			Order.C_SPLITTO, Order.C_SPLITBY, Order.C_SPLITTERMINAL,
			Order.C_MATCHCOUNTER, Order.C_LQVALUEBEFORE, Order.C_COUNTER,
			Order.C_TOTALCOUNTERSPLIT, Order.C_COMPLAINCEID,
			Order.C_UPDATEDATE, Order.C_USERID, Order.C_UPDATEBY,
			Order.C_TERMINALID, Order.C_ISFLOOR, Order.C_MYACC,
			Order.C_BALANCEVOLUME, Order.C_TRADENO, Order.C_MARKETREF,
			Order.C_NEGDEALREF, Order.C_ACCID, Order.C_ORDERIDCONTRA,
			Order.C_ACCIDCONTRA, Order.C_INVTYPECONTRA, Order.C_TRADINGIDCONTRA*/ };

	public static Vector getHeader() {
		Vector header = new Vector(Order.CIDN_NUMBEROFFIELDS);

		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}

	public static List createTableRow(Row pf) {
		List vec = new Vector(Order.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < Order.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new GTCField(pf, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}

	public final static ABCRender render = new ABCRender();

	static class ABCRender extends MutableIDisplayAdapter {
		DefaultTableCellRenderer renderer = new GTCRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			renderer.getTableCellRendererComponent(table, value, isSelected,
					hasFocus, row, column);
			((GTCRender) renderer).setValue(value);

			return renderer;
		}
	}

}
