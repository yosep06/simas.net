package eqtrade.trading.model;

import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.vollux.idata.Row;
import com.vollux.idata.indirection.MutableIDisplayAdapter;

public final class UserProfileDef {
	public static Hashtable getTableDef(){
        Hashtable htable = new Hashtable();
        htable.put("header", dataHeader);
        htable.put("alignment", defaultHeaderAlignment);
        htable.put("width", defaultColumnWidth);
        htable.put("order", defaultColumnOrder);
        htable.put("sorted", columnsort);        
        htable.put("sortcolumn",new Integer(0));
        htable.put("sortascending", new Boolean(true));
        return htable;
	}	
    public static String[] dataHeader = new String[]{
    	"userId", "ProfileId", "MenuId", "Allowed"
    };

    public static int[] defaultHeaderAlignment = new int[]{
		SwingConstants.CENTER, SwingConstants.CENTER, SwingConstants.CENTER,
		SwingConstants.CENTER
    };

	public  static int[] defaultColumnWidth = new int[]{
			100,100,100,100
	};

	public  static int[] defaultColumnOrder = new int[]{
			0,1,2,3
	};	

	public static boolean[] columnsort = new boolean[]{
			true, true, true,true
	};
	
    public static Vector getHeader() {
		Vector header = new Vector(UserProfile.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < dataHeader.length; i++) {
			header.addElement(dataHeader[i]);
		}
		return header;
	}
	
    public static List createTableRow(Row row) {
		List vec = new Vector(UserProfile.CIDN_NUMBEROFFIELDS);
		for (int i = 0; i < UserProfile.CIDN_NUMBEROFFIELDS; i++) {
			try {
				vec.add(i, new UserProfileField(row, i));
			} catch (Exception e) {
			}
		}
		return vec;
	}
	
	public final static UserRender render = new UserRender();
	
    static class UserRender extends MutableIDisplayAdapter{
		DefaultTableCellRenderer renderer = new UserProfileRender();

		@Override
		public TableCellRenderer getTableCellRenderer(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column){
			renderer.getTableCellRendererComponent(table, value,isSelected, hasFocus, row, column);
			((UserProfileRender)renderer).setValue(value);
			return renderer;
		}
	}
}
