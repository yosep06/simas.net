package eqtrade.trading.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class Exchange extends Model {
    private static final long serialVersionUID = 309177931626178221L;
    public final static SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");

    public static final int	C_ID = 0;
    public static final int  	C_NAME = 1;
    public static final int  	C_PREOPTIME = 2;
	public static final int	C_SESS1TIME = 3;
	public static final int	C_SESS2TIME = 4;
	public static final int   C_SESS2FRIDAYTIME = 5;
	public static final int 	CIDN_NUMBEROFFIELDS = 6;

	public Exchange(){
		super(CIDN_NUMBEROFFIELDS);
	}

	public Exchange(String smsg){
		super(CIDN_NUMBEROFFIELDS, smsg);
	}
	
	@Override
	protected void convertType() {
		for (int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch (i) {
			case 2 :case 3: case 4: case 5:
                try {
                    vdata.setElementAt(formatTime.parse((String)vdata.elementAt(i)), i);
                } catch (Exception ex){vdata.setElementAt(null,i);}
				break;				
			default:
				break;
			}
		}
	}	

	public String getId(){
		return (String) getData(C_ID);
	}
	public void setId(String sinput){
		setData(sinput, C_ID);
	}
	public String getName(){
		return (String) getData(C_NAME);
	}
	public void setName(String sinput){
		setData(sinput, C_NAME);
	}	
	public Date getPreopTime(){
		return (Date) getData(C_PREOPTIME);
	}
	public void setPreopTime(Date sinput){
		setData(sinput, C_PREOPTIME);
	}    
    public Date getSess1Time(){
        return (Date) getData(C_SESS1TIME);
    }
    public void setSess1Time(Date sinput){
        setData(sinput, C_SESS1TIME);
    }
    public Date getSess2Time(){
        return (Date) getData(C_SESS2TIME);
    }
    public void setSess2Time(Date sinput){
        setData(sinput, C_SESS2TIME);
    }
    public Date getSess2FridayTime(){
        return (Date) getData(C_SESS2FRIDAYTIME);
    }
    public void setSess2FridayTime(Date sinput){
        setData(sinput, C_SESS2FRIDAYTIME);
    }
    
}