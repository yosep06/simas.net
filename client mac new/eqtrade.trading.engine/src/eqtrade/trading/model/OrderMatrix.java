package eqtrade.trading.model;


import java.util.Date;

import javax.print.DocFlavor.STRING;


import eqtrade.trading.core.Utils;

public class OrderMatrix extends Model{

	private static final long serialVersionUID = 1L;
	public static int C_LABEL = 0;
	public static int C_BUYORSELL = 1;
	public static int C_SECID = 2;
	public static int C_PRICE = 3;
	public static int C_LOT = 4;
	public static int C_AMOUNT = 5;
	public static int C_CLIENT = 6;
	public static int C_TBUYAMOUNT = 7;
	public static int C_TSELLAMOUNT = 8;
	public static int C_LASTEDIT = 9;
	public static int C_VOLUME = 10 ;
	public static int C_USERID	= 11;	
	public static int C_ORDERMATRIXID = 12;
	public static int C_ISBASKET = 13;
	public static int C_BASKETTIME = 14;
	
    public static final int CIDN_NUMBEROFFIELDS = 15;
	
	public OrderMatrix() {
		super(CIDN_NUMBEROFFIELDS);
	}
	
	public OrderMatrix(String msg){
		super(CIDN_NUMBEROFFIELDS,msg);
	}

	@Override
	protected void convertType() {
		for(int i=0; i<CIDN_NUMBEROFFIELDS; i++){
			switch(i){
			case 3:case 4:case 5:case 7:case 8:case 10:
				vdata.setElementAt(new Double(Utils.strToDouble((String)vdata.elementAt(i), 0)), i);
				break;
			
			}
		}
	}
	public void buildAmount() {
		double OrderAmount = getVolume().doubleValue() == 0 ? 0 : getVolume().doubleValue() * getPrice().doubleValue();
        setAmount(new Double(OrderAmount));
	}

	public String getLabel() {
		return (String)getData(C_LABEL);
	}
	
	public void setLabel(String sinput) {
		setData(sinput,C_LABEL);
	}
	
	public String getBuyorsell() {
		return (String)getData(C_BUYORSELL);
	}
	
	public void setBuyorsell(String sinput) {
		setData(sinput,C_BUYORSELL) ;
	}

	public String getSecid() {
		return (String)getData(C_SECID);
	}
	
	public void setSecid(String sinput) {
		setData(sinput,C_SECID );
	}
	
	public Double getPrice() {
		return (Double)getData(C_PRICE);
	}

	public void setPrice(Double sinput) {
		setData(sinput, C_PRICE) ;
	}
	
	public Double getLot() {
		return (Double)getData(C_LOT);
	}

	public void setLot(Double sinput) {
		setData(sinput,C_LOT );
	}
	
	public Double getAmount() {
		return (Double)getData(C_AMOUNT);
	}

	public void setAmount(Double sinput) {
		setData(sinput, C_AMOUNT );
	}
	
	public String getClient() {
		return (String)getData(C_CLIENT);
	}

	public void setClient(String sinput) {
		setData(sinput, C_CLIENT);
	}
	
	public Double getTbuyamount() {
		return (Double)getData(C_TBUYAMOUNT);
	}

	public void setTbuyamount(Double sinput) {
		setData(sinput, C_TBUYAMOUNT );
	}
	
	public Double getTsellamount() {
		return (Double)getData(C_TSELLAMOUNT);
	}
	
	public void setTsellamount(Double sinput) {
		setData(sinput,C_TSELLAMOUNT );
	}

	public Date getLastedit() {
		return (Date)getData(C_LASTEDIT);
	}

	public void setLastedit(Date sinput) {
		setData(sinput,C_LASTEDIT );
	}
	
	public Double getVolume(){
		return (Double) getData(C_VOLUME);
	}
	
	public void setVolume(Double sinput){
		setData(sinput, C_VOLUME);
	}
	
	public String getUserid() {
		return (String)getData(C_USERID);
	}

	public void setUserid(String sinput) {
		setData(sinput, C_USERID) ;
	}

	public String getOrdermatrixid() {
		return (String)getData(C_ORDERMATRIXID);
	}

	public void setOrdermatrixid(String sinput) {
		setData(sinput, C_ORDERMATRIXID);
	}

	public String getIsBasket() {
		return (String)getData(C_ISBASKET);
	}

	public void setIsBasket(String sinput) {
		setData(sinput, C_ISBASKET);
	}

	public String getBasketTime() {
		return  (String)getData(C_BASKETTIME);
	}

	public  void setBasketTime(String sinput) {
		setData(sinput, C_BASKETTIME);
	}

}
