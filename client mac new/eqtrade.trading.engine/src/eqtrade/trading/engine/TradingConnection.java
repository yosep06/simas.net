package eqtrade.trading.engine;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.Timer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import thst.gtw.rmi.ThstGtwInterface;
import eqtrade.trading.core.Event;
import eqtrade.trading.core.ITradingConnection;
import eqtrade.trading.core.ITradingEngine;
import eqtrade.trading.core.Utils;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderMatrix;
import eqtrade.trading.model.UserProfile;

public class TradingConnection implements ITradingConnection {
	public final static String COMM_TRADING = "trading";
	private final Log log = LogFactory.getLog(getClass());
	public volatile ThstGtwInterface rmi = null;
	public volatile long sesid = 0;
	private String uid, pwd, pin;
	private String ssvr;
	private int ninterval, totalServer;
	private Timer refreshTimer = null;
	private final static String CMD_REQUEST = "RQT";
	private ThreadIn inThread;
	private ThreadOut outThread;
	private boolean process = true;
	private ITradingEngine engine;
	private HashMap hashIP = new HashMap();

	public TradingConnection(ITradingEngine engine) {
		this.engine = engine;
		init();
	}

	public String getPassword() {
		return pwd;
	}

	public String getUserId() {
		return uid;
	}

	public String getPIN() {
		return pin;
	}

	public String getIPServer() {
		return "";
	}

	public void init() {
		loadSetting();
		inThread = new ThreadIn(engine);
		outThread = new ThreadOut(engine);
		refreshTimer = new Timer(ninterval, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Event evt = new Event(CMD_REQUEST, null);
				// processHeartbeat(evt);
				// log.info("execute action listeners");
				(new Thread(new Runnable() {
					@Override
					public void run() {
						Event evt = new Event(CMD_REQUEST, null);
						processHeartbeat(evt);
					}
				})).start();
			}
		});
		refreshTimer.setInitialDelay(ninterval);
		refreshTimer.stop();
	}

	public void setStop() {
		//log.info("heartbeat timer stopped, cause : setStop called by application");
		refreshTimer.stop();
		inThread.setStop();
		outThread.setStop();
	}

	private void loadSetting() {
		Object o = Utils.readFile("data/config/trading.dat");
		Vector v = null;

		if (o == null) {
			v = new Vector(3);
			// v.addElement("172.17.0.60:7899/Gateway");
			// v.addElement("trading.simasnet.com:7899/Gateway");
			v.addElement("172.21.1.101:7899/Gateway");
			v.addElement("172.21.1.101:7899/Gateway");
			v.addElement(new Integer(600));
			v.add("http://forex.simasnet.com");
			Utils.writeFile("data/config/trading.dat", v);
		} else {
			v = (Vector) o;
		}
		int zz = v.size();
		// ninterval = ((Integer) v.elementAt(2)).intValue();
		ninterval = ((Integer) v.elementAt(zz - 2)).intValue();
		// ninterval = 300;
		ninterval = 100;
		totalServer = zz - 2;
		hashIP.clear();
		for (int i = 1; i <= totalServer; i++) {
			ssvr = (String) v.elementAt(i - 1);
			hashIP.put(new String(i + ""), new String("rmi://" + ssvr));
		}
		// try {
		// sip = InetAddress.getLocalHost().getHostAddress();
		// } catch (Exception ex){}
		//log.info("available server: " + totalServer);
		//log.info("available server: " + hashIP.toString());
	}

	public void loadMaster(String eventStatus) throws Exception {
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAACCOUNTTYPE, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(5%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getAccType(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAINVTYPE, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(15%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getInvType(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAUSERTYPE, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(25%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getUserType(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATACUSTTYPE, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(35%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getCustType(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAEXCHANGE, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(45%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getExchange(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATABOARD, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(55%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getBoard(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATABROKER, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(60%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getBroker(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAACCSTATUS, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(65%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getStatusAcc(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAORDERSTATUS, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(75%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getStatusOrder(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAACCSTOCK, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(85%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getAccTypeSec(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATASTOCK, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(95%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getSecurities(sesid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAUSERPROFILE, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(97%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getUserProfile(sesid, uid)));

		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATANEGDEALLIST, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(98%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getNegDeal(sesid)));
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAANNOUNCEMENT, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading master(99%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getAnnouncement(sesid)));
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATABACKNOTIF, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loding master(100%)...")
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getBackNotif(sesid)));
	}

	public void loadOrder(String eventStatus, String param1, String param2,String param3,
			boolean clear) throws Exception {
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAORDER, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading order...")
						.setField(TradingEventDispatcher.PARAM_CLEAR,
								new Boolean(clear))
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getOrder(sesid, param1, param2)));
	}

	public void loadTrade(String eventStatus, String param1) throws Exception {
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATATRADE, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading trade...")
						.setField(TradingEventDispatcher.PARAM_CLEAR,
								new Boolean(true))
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getMatchOrder(sesid, param1)));
	}

	public void loadAccount(String eventStatus, String param1, boolean clear)
			throws Exception {
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAACCOUNT, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading account...")
						.setField(TradingEventDispatcher.PARAM_CLEAR,
								new Boolean(clear))
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getAccount(sesid, param1)));
	}

	/*
	 * public void loadMargin(String eventStatus, String param1, boolean clear)
	 * throws Exception { engine.getEventDispatcher().pushEvent( new
	 * Event(TradingEventDispatcher.EVENT_DATAMARGIN, true)
	 * .setField(TradingEventDispatcher.PARAM_CODE, eventStatus)
	 * .setField(TradingEventDispatcher.PARAM_MESSAGE, "loading margin...")
	 * .setField(TradingEventDispatcher.PARAM_CLEAR, new Boolean(clear))
	 * .setField(TradingEventDispatcher.PARAM_DATA, rmi.getAccount(sesid,
	 * param1))); }
	 */
	
	  public void loadSchedule(String eventStatus,String param1,String param2,
	  boolean clear) throws Exception { engine.getEventDispatcher().pushEvent(
	  new Event(TradingEventDispatcher.EVENT_DATASCHEDULE,true)
	  .setField(TradingEventDispatcher.PARAM_CODE, eventStatus)
	  .setField(TradingEventDispatcher.PARAM_MESSAGE, "loading Schedule....")
	  .setField(TradingEventDispatcher.PARAM_CLEAR, new Boolean(clear))
	  .setField(TradingEventDispatcher.PARAM_DATA, rmi.getAts(sesid,param1,param2)));//rmi.get seharusna getschedule 
	  } 
	  
	  public void loadBrowseOrder(String eventStatus, String param1, boolean clear)
	  throws Exception{ 
	  engine.getEventDispatcher().pushEvent( new
	  Event(TradingEventDispatcher.EVENT_DATABROWSEORDER,true)
	  .setField(TradingEventDispatcher.PARAM_CODE, eventStatus)
	  .setField(TradingEventDispatcher.PARAM_MESSAGE, "loading browser....")
	  .setField(TradingEventDispatcher.PARAM_CLEAR, new Boolean(clear))
	  .setField(TradingEventDispatcher.PARAM_DATA, rmi.getAtsBrowse(sesid))); }
	 

	public void loadPortfolio(String eventStatus, String param1, String param2,
			boolean clear) throws Exception {
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATAPORTFOLIO, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading portfolio...")
						.setField(TradingEventDispatcher.PARAM_CLEAR,
								new Boolean(clear))
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getPortfolio(sesid, param1, param2)));
	}

	public void loadCashColl(String eventStatus, String param1, boolean clear)
			throws Exception {
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATACASHCOLL, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading cash collateral...")
						.setField(TradingEventDispatcher.PARAM_CLEAR,
								new Boolean(clear))
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getCashColl(sesid, param1)));
	}

	public void loadBuySellDetail(String eventStatus, String param1,
			String param2, boolean clear) throws Exception {
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATABUYSELLDETAIL, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading buy/sell detail...")
						.setField(TradingEventDispatcher.PARAM_CLEAR,
								new Boolean(clear))
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getBuySellDetail(sesid, param1, param2)));
	}

	public void loadDueDate(String eventStatus, String param1, boolean clear)
			throws Exception {
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATADUEDATE, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading DueDate ...")
						.setField(TradingEventDispatcher.PARAM_CLEAR,
								new Boolean(clear))
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getDueDate(sesid, param1)));
	}

	public void loadNotif(String eventStatus, String param1, boolean clear)
			throws Exception {
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DATABACKNOTIF, true)
						.setField(TradingEventDispatcher.PARAM_CODE,
								eventStatus)
						.setField(TradingEventDispatcher.PARAM_MESSAGE,
								"loading Notif ...")
						.setField(TradingEventDispatcher.PARAM_CLEAR,
								new Boolean(clear))
						.setField(TradingEventDispatcher.PARAM_DATA,
								rmi.getBackNotif(sesid)));
	}

	public void refreshData(final boolean master, final boolean account,
			final boolean margin, final boolean pf, final boolean order) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (master)
						loadMaster(TradingEventDispatcher.EVENT_REFRESHSTATUS);
					if (account)
						loadAccount(TradingEventDispatcher.EVENT_REFRESHSTATUS,
								"%", true);					
					if (pf)
						loadPortfolio(
								TradingEventDispatcher.EVENT_REFRESHSTATUS,
								"%", "%", true);
					if (order) {
						loadOrder(TradingEventDispatcher.EVENT_REFRESHSTATUS,
								"%", "%", "%", true);
						loadTrade(TradingEventDispatcher.EVENT_REFRESHSTATUS,
								"%");
					}
					engine.getEventDispatcher().pushEvent(
							new Event(TradingEventDispatcher.EVENT_REFRESHOK));
				} catch (Exception ex) {
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_REFRESHFAILED)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"refresh failed, please try again"));
				}
			}
		}).start();
	}

	public void refreshData(final String tradingid) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					loadAccount(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							tradingid, false);
					// loadSchedule(TradingEventDispatcher.EVENT_REFRESHSTATUS,
					// tradingid, "%", false);
					loadPortfolio(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							tradingid, "%", false);
					loadOrder(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							tradingid, "%","%", false);
					loadTrade(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							tradingid);
					// loadBrowseOrder(TradingEventDispatcher.EVENT_REFRESHSTATUS
					// , tradingid, false);
					engine.getEventDispatcher().pushEvent(
							new Event(TradingEventDispatcher.EVENT_REFRESHOK));
				} catch (Exception ex) {
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_REFRESHFAILED)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"refresh failed, please try again"));
				}
			}
		}).start();
	}

	public void refreshData(final String tradingid, final boolean isclear) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					loadAccount(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							tradingid, isclear);
					// loadSchedule(TradingEventDispatcher.EVENT_REFRESHSTATUS
					// , tradingid, "%", isclear);
					loadPortfolio(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							tradingid, "%", isclear);
					loadOrder(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							tradingid, "%","%", isclear);
					loadTrade(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							tradingid);
					engine.getEventDispatcher().pushEvent(
							new Event(TradingEventDispatcher.EVENT_REFRESHOK));
				} catch (Exception ex) {
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_REFRESHFAILED)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"refresh failed, please try again"));
				}
			}
		}).start();
	}

	public void refreshOrder(final String param1, final String param2,final String param3) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					loadOrder(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							param1, param2,param3, false);
					engine.getEventDispatcher().pushEvent(
							new Event(TradingEventDispatcher.EVENT_REFRESHOK));
				} catch (Exception ex) {
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_REFRESHFAILED)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"refresh failed, please try again"));
				}
			}
		}).start();
	}

	public void refreshTrade(final String param1) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					loadTrade(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							param1);
					engine.getEventDispatcher().pushEvent(
							new Event(TradingEventDispatcher.EVENT_REFRESHOK));
				} catch (Exception ex) {
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_REFRESHFAILED)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"refresh failed, please try again"));
				}
			}
		}).start();
	}

	public void refreshPortfolio(final String param1, final String param2) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					loadPortfolio(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							param1, param2, false);
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_REFRESHPORTFOLIO));
				} catch (Exception ex) {
				}
			}
		}).start();
	}

	public void refreshAccount(final String param) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					loadAccount(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							param, false);
				} catch (Exception ex) {
				}
			}
		}).start();
	}

	public void refreshAts(final String param) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					// loadSchedule(TradingEventDispatcher.EVENT_REFRESHSTATUS,
					// param,"%", false);
				} catch (Exception ex) {

				}
			}
		}).start();
	}

	public void refreshBrowseAts(final String param) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					// loadBrowseOrder(TradingEventDispatcher.EVENT_REFRESHSTATUS,
					// param, false);
				} catch (Exception ex) {

				}
			}
		}).start();
	}

	public void refreshCashColl(final String param) {
		new Thread(new Runnable() {
			public void run() {
				try {
					loadCashColl(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							param, false);
				} catch (Exception ex) {
				}
			}
		}).start();
	}

	public void refreshBuySellDetail(final String param1, final String param2) {
		new Thread(new Runnable() {
			public void run() {
				try {
					loadBuySellDetail(
							TradingEventDispatcher.EVENT_REFRESHSTATUS, param1,
							param2, false);
				} catch (Exception ex) {
				}
			}
		}).start();
	}

	public void refreshDueDate(final String param1) {
		new Thread(new Runnable() {
			public void run() {
				try {
					loadDueDate(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							param1, false);
				} catch (Exception ex) {
					System.out.println("------" + ex);
				}
			}
		}).start();
	}

	public void refreshNotif(final String param) {
		new Thread(new Runnable() {
			public void run() {
				try {
					loadNotif(TradingEventDispatcher.EVENT_REFRESHSTATUS,
							param, false);
				} catch (Exception ex) {
					System.out.println("+++++" + ex);
				}
			}
		}).start();
	}

	public String splitOrder(String param) {
		JSONObject result = new JSONObject();
		result.put("psukses", "0");
		result.put("desc", "not implemented by server");
		result.put("pacc", new JSONArray());
		result.put("porder", new JSONArray());
		result.put("pmatch", new JSONArray());
		try {
			byte[] hasil = rmi.entrySplitDone(sesid,
					Utils.compress(param.getBytes()));
//			System.out.println(new String(Utils.decompress(hasil)));
			result = (JSONObject) ((JSONArray) JSONValue.parse(new String(Utils
					.decompress(hasil)))).get(0);
			((TradingEventDispatcher) engine.getEventDispatcher())
					.doSplit(result);
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return result.toString();
	}

	public void getServertime() throws Exception {
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_TIMESERVER).setField(
						TradingEventDispatcher.PARAM_TIMESERVER,
						new Long(rmi.getServerTime(sesid))));
	}

	public void logon(final String suid, final String spwd) {
		new Thread(new Runnable() {
			public void run() {
				loadSetting();
				process = true;
				processLogin(suid, spwd);
			}
		}).start();
	}

	public void processLogin(String suid, String spwd) {
		boolean bresult = false;
		int i = 1;
		String url;
		for (; i <= totalServer && process; i++) {
			try {
				url = (String) hashIP.get(i + "");
				//log.info("request login to : " + url + "user name: " + suid);
				rmi = (ThstGtwInterface) Naming.lookup(url);
				bresult = rmi != null;
				if (bresult) {
					try {
						sesid = rmi.login(suid, spwd);
						// sesid = 100;
						bresult = sesid > 0;
						if (bresult) {
							uid = suid;
							pwd = spwd;
							//log.info("request register OK, with session id: "	+ sesid);
							loadMaster(TradingEventDispatcher.EVENT_LOGINSTATUS);

							UserProfile profile = (UserProfile) engine
									.getDataStore()
									.get(TradingStore.DATA_USERPROFILE)
									.getDataByField(new Object[] { "40" },
											new int[] { UserProfile.C_MENUID });
							// if (profile != null) {
							// log.info("user login with profile : "
							// + profile.getProfileId());
							// if (!profile.getProfileId().toUpperCase()
							// .equals("SUPERUSER")) {
							loadAccount(
									TradingEventDispatcher.EVENT_LOGINSTATUS,
									"%", true);
							// loadSchedule(
							// TradingEventDispatcher.EVENT_LOGINSTATUS,
							// "%", "%", true);
							// loadBrowseOrder(
							// TradingEventDispatcher.EVENT_LOGINSTATUS,
							// "%", true);
							loadPortfolio(
									TradingEventDispatcher.EVENT_LOGINSTATUS,
									"%", "%", true);

							if (!profile.getProfileId().toUpperCase()
									.equals("SUPERUSER")) {
								loadOrder(
										TradingEventDispatcher.EVENT_LOGINSTATUS,
										"%", "%","%", true);
								loadTrade(
										TradingEventDispatcher.EVENT_LOGINSTATUS,
										"%");

							} else {
								//log.info("user is SUPERUSER load data trading will be off");
							}
							// }

							getServertime();
							startServices();
							engine.getEventDispatcher()
									.pushEvent(
											new Event(
													TradingEventDispatcher.EVENT_NEWSESSIONID)
													.setField(
															TradingEventDispatcher.PARAM_SESSIONID,
															new Long(sesid)));
							engine.getEventDispatcher()
									.pushEvent(
											new Event(
													TradingEventDispatcher.EVENT_LOGINOK));
							bresult = true;
							break;
						} else {
							engine.getEventDispatcher()
									.pushEvent(
											new Event(
													TradingEventDispatcher.EVENT_LOGINFAILED)
													.setField(
															TradingEventDispatcher.PARAM_MESSAGE,
															"Invalid user/password for trading gateway"));
							break;
						}
					} catch (Exception exp) {
						log.warn("Failed, " + exp.getMessage());
						engine.getEventDispatcher()
								.pushEvent(
										new Event(
												TradingEventDispatcher.EVENT_LOGINSTATUS)
												.setField(
														TradingEventDispatcher.PARAM_MESSAGE,
														"Trading Connection Failed, retrying("
																+ i + ")...."));
						try {
							Thread.sleep(500);
						} catch (Exception a) {
						}
						;
						bresult = false;
						//log.error(Utils.logException(exp));
					}
				} else {
					log.warn("Connection Failed, retrying(" + i + ")....");
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_LOGINSTATUS)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"Trading Connection Failed, retrying("
															+ i + ")...."));
				}
			} catch (Exception ex) {
				log.warn("Connection Failed, retrying(" + i + ")....");
				engine.getEventDispatcher().pushEvent(
						new Event(TradingEventDispatcher.EVENT_LOGINSTATUS)
								.setField(TradingEventDispatcher.PARAM_MESSAGE,
										"Trading Connection Failed, retrying("
												+ i + ")...."));
				try {
					Thread.sleep(500);
				} catch (Exception exp) {
				}
				;
				bresult = false;
				//log.error(Utils.logException(ex));
			}
		}
		if (!bresult && i > totalServer) {
			engine.getEventDispatcher()
					.pushEvent(
							new Event(TradingEventDispatcher.EVENT_LOGINFAILED)
									.setField(
											TradingEventDispatcher.PARAM_MESSAGE,
											"Failed, please check your trading connection"));
		}
	}

	public void startServices() {
		if (!this.inThread.isAlive())
			inThread.start();
		if (!this.outThread.isAlive())
			outThread.start();

		//log.info("heartbeat timer started, cause : startServices called by application");
		refreshTimer.start();
	}

	public void changePassword(final String soldpwd, final String snewpwd) {
		new Thread(new Runnable() {
			public void run() {
				try {
					boolean success = rmi.chgPwd(sesid, uid, snewpwd);
					if (success) {
						engine.getEventDispatcher()
								.pushEvent(
										new Event(
												TradingEventDispatcher.EVENT_CHGPASSWDOK));
					} else {
						engine.getEventDispatcher()
								.pushEvent(
										new Event(
												TradingEventDispatcher.EVENT_CHGPASSWDFAILED)
												.setField(
														TradingEventDispatcher.PARAM_MESSAGE,
														"change password failed, please try again."));
					}
				} catch (Exception ex) {
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_CHGPASSWDFAILED)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"change password failed, please check your connection"));
				}
			}
		}).start();
	}

	public void changePIN(final String soldpin, final String snewpin) {
		new Thread(new Runnable() {
			public void run() {
				try {
					boolean success = rmi.changepin(sesid, uid, snewpin);
					if (success) {
						engine.getEventDispatcher()
								.pushEvent(
										new Event(
												TradingEventDispatcher.EVENT_CHGPINOK));
						pin = snewpin;
					} else {
						engine.getEventDispatcher()
								.pushEvent(
										new Event(
												TradingEventDispatcher.EVENT_CHGPINFAILED)
												.setField(
														TradingEventDispatcher.PARAM_MESSAGE,
														"change PIN failed, please try again."));
					}
				} catch (Exception ex) {
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_CHGPINFAILED)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"change PIN failed, please check your connection"));
				}
			}
		}).start();
	}

	public void checkPIN(final String puserid, final String pPIN) {
		new Thread(new Runnable() {
			public void run() {
				try {
					boolean success = rmi.checkpin(sesid, uid, pPIN);
					if (success) {
						engine.getEventDispatcher()
								.pushEvent(
										new Event(
												TradingEventDispatcher.EVENT_CHECKPINOK));
						pin = pPIN;
					} else {
						engine.getEventDispatcher()
								.pushEvent(
										new Event(
												TradingEventDispatcher.EVENT_CHECKPINFAILED)
												.setField(
														TradingEventDispatcher.PARAM_MESSAGE,
														"invalid PIN, please try again."));
					}
				} catch (Exception ex) {
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_CHECKPINFAILED)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"logon trading failed, please check your connection"));
				}
			}
		}).start();
	}

	public void checkPINSLS(final String puserid, final String pPIN) {
		new Thread(new Runnable() {
			public void run() {
				try {
					boolean success = rmi.checkpin(sesid, uid, pPIN);
					if (success) {
						engine.getEventDispatcher()
								.pushEvent(
										new Event(
												TradingEventDispatcher.EVENT_CHECKPINOK));
						pin = pPIN;
					}
				} catch (Exception ex) {
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_CHECKPINFAILED)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"logon trading failed, please check your connection"));
				}
			}
		}).start();
	}

	public void reconnect() {
		sesid = -1;
		//log.info("heartbeat timer stopped, cause : reconnect called by application");
		refreshTimer.stop();
		engine.getEventDispatcher().pushEvent(
				new Event(TradingEventDispatcher.EVENT_DISCONNECT));
	}

	public boolean logout() {
		//log.info("heartbeat timer stopped, cause : logout called by application");
		refreshTimer.stop();
		process = false;
		boolean bresult = true;
		try {
			rmi.logout(sesid);
		} catch (Exception ex) {

		}
		try {
			//log.info("request logout");
			if (bresult) {
				sesid = -1;
				outThread.doClear();
				this.rmi = null;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			bresult = false;
		}
		return bresult;
	}

	public void reconnect(final int count) {
		new Thread(new Runnable() {
			public void run() {
				doReconnect(count);
			}
		}).start();
	}

	private int flip = 1, nextflip = 1, cnt = 0;

	public void doReconnect(int counter) {
		boolean bresult = false;
		try {
			cnt++;
			engine.getEventDispatcher().pushEvent(
					new Event(TradingEventDispatcher.EVENT_RECONNECTSTATUS)
							.setField(TradingEventDispatcher.PARAM_MESSAGE,
									"trying reconnecting..."));
			if (cnt == nextflip) {
				flip++;
				nextflip = nextflip + 1;
			}
			if (cnt > totalServer * 1) {
				flip = 1;
				nextflip = 1;
				cnt = 0;
			}
			String url = (String) hashIP.get(new String(flip + ""));
			rmi = (ThstGtwInterface) Naming.lookup(url);
			bresult = rmi != null;
			if (bresult) {
				sesid = rmi.login(uid, pwd);
				bresult = sesid > 0;
				if (bresult) {
					startServices();
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_NEWSESSIONID)
											.setField(
													TradingEventDispatcher.PARAM_SESSIONID,
													new Long(sesid)));
					//log.info("ok, we are back to live with new sessionid: "+ sesid);
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_RECONNECTOK));
				} else {
					//log.warn("reconnecting failed");
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_RECONNECTSTATUS)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"reconnecting failed or server not ready"));
				}
			}
		} catch (Exception ex) {
			log.warn("reconnecting failed");
			engine.getEventDispatcher().pushEvent(
					new Event(TradingEventDispatcher.EVENT_RECONNECTFAILED)
							.setField(TradingEventDispatcher.PARAM_MESSAGE,
									"reconnecting failed or server not ready"));
			//log.error(Utils.logException(ex));
			ex.printStackTrace();
		}
	}

	private void processHeartbeat(Event evt) {
		byte[] bpar = null;
		try {
			if (sesid > -1) {
				if (evt.getName().equals(CMD_REQUEST)) {
					refreshTimer.stop();
					// log.info("process getMsg");
					bpar = rmi.getCurrentMsg(sesid);
					// log.info("process getMsg finished "+bpar);
					if (bpar != null) {
						String temp = new String(Utils.decompress(bpar));
						JSONArray array = (JSONArray) JSONValue.parse(temp);
						Vector vintmp = new Vector(array.size());
						for (int i = 0; i < array.size(); i++) {
							//log.info("server say: " + array.get(i).toString());
							vintmp.addElement(array.get(i));
						}
						inThread.addIn(vintmp);
					}

					if (process)
						refreshTimer.start();
				}
			}
		} catch (RemoteException ex) {
			//log.error(Utils.logException(ex));
			// tambahan apakah exceptionnya karena kill by server atau harus
			// reconnect
			// reconnect();

			if (ex.getCause().toString().toLowerCase().contains("kill")) {
				logout();
				engine.getEventDispatcher()
						.pushEvent(
								new Event(TradingEventDispatcher.EVENT_KILL)
										.setField(
												TradingEventDispatcher.PARAM_MESSAGE,
												"Force logout because you have been login at another computer or bad connection"));
			} else {
				reconnect();
			}
		} catch (Exception ex) {
			//log.error(Utils.logException(ex));
			reconnect();
			/*
			 * logout(); engine.getEventDispatcher() .pushEvent( new
			 * Event(TradingEventDispatcher.EVENT_KILL) .setField(
			 * TradingEventDispatcher.PARAM_MESSAGE,
			 * "Force logout because you have been login at another computer or bad connection"
			 * ));
			 */
		}
	}

	public boolean sendEvent(Event event) {
		outThread.addOut(event);
		return true;
	}

	public boolean isConnected() {
		return true;
	}

	public long getSessid() {
		return sesid;
	}

	@Override
	public long getSessionid() {
		// TODO Auto-generated method stub
		return this.sesid;
	}

	@Override
	public void refreshData(final boolean master, final boolean account,
			final boolean pf, final boolean order) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					if (master)
						loadMaster(TradingEventDispatcher.EVENT_REFRESHSTATUS);
					if (account)
						loadAccount(TradingEventDispatcher.EVENT_REFRESHSTATUS,
								"%", true);
					if (pf)
						loadPortfolio(
								TradingEventDispatcher.EVENT_REFRESHSTATUS,
								"%", "%", true);
					if (order) {
						loadOrder(TradingEventDispatcher.EVENT_REFRESHSTATUS,
								"%", "%","%", true);
						loadTrade(TradingEventDispatcher.EVENT_REFRESHSTATUS,
								"%");
					}
					engine.getEventDispatcher().pushEvent(
							new Event(TradingEventDispatcher.EVENT_REFRESHOK));
				} catch (Exception ex) {
					engine.getEventDispatcher()
							.pushEvent(
									new Event(
											TradingEventDispatcher.EVENT_REFRESHFAILED)
											.setField(
													TradingEventDispatcher.PARAM_MESSAGE,
													"refresh failed, please try again"));
				}
			}
		}).start();
	}

	@Override
	public boolean entryWithdraw(Object data) throws Exception {
		// TODO Auto-generated method stub
		return rmi.entryWithdraw(getSessid(), (byte[]) data);
	}

	@Override
	public boolean entryAmend(Object data) throws Exception {
		// TODO Auto-generated method stub
		return rmi.entryAmend(getSessid(), (byte[]) data);
	}

	@Override
	public boolean createOrder(Object data) throws Exception {
		// TODO Auto-generated method stub
		return rmi.createOrder(getSessid(), (byte[]) data);
	}

	@Override
	public boolean createAts(Object data) throws Exception {
		// TODO Auto-generated method stub
		return rmi.createAts(getSessid(), (byte[]) data);
	}

	@Override
	public boolean atsWithdraw(Object data) throws Exception {
		// TODO Auto-generated method stub
		return rmi.atsWithdraw(getSessid(), (byte[]) data);
	}

	@Override
	public boolean notification(Object data) throws Exception {
		// TODO Auto-generated method stub
		return rmi.notification(getSessid(), (byte[]) data);
	}

	@Override
	public void addFilterByClient(String filter) {

	}

	@Override
	public void deleteFilterByClient(String filter) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setFilter(boolean isFilter) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addFilterByOrder(String filter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteFilterByOrder(String filter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveFilterByClient(String filter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String loadFilterByClient() {
		// TODO Auto-generated method stub
		return null;
	}

		@Override
	public void refreshOrderMatrix(String param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void refreshOrderMatrixList(String param,String param2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadOrderMatrix(String eventStatus, String param1, boolean clear)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadOrderMatrixList(String eventStatus, String param1,String param2,
			boolean clear) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean createOrderMatrix(Object data) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteOrderMatrix(Object data) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteOrderMatrixDetil(Object data) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addFilterByStock(String filter) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteFilterByStock(String filter) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void checkPIN_NEGO(String pPIN, String form) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadGTC(String eventStatus, String param1, String param2,
			boolean clear) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void refreshGTC(String param, String param2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean createGtc(Object data) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void gtc(Order param) throws Exception {
		// TODO Auto-generated method stub
		
	}
}