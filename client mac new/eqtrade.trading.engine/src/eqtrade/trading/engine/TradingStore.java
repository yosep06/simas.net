package eqtrade.trading.engine;

import java.util.HashMap;
import java.util.Vector;

import com.vollux.demo.Mapping;
import com.vollux.demo.MappingDef;
import com.vollux.idata.GridModel;

import eqtrade.trading.model.AccStatus;
import eqtrade.trading.model.AccStatusDef;
import eqtrade.trading.model.AccStock;
import eqtrade.trading.model.AccStockDef;
import eqtrade.trading.model.AccType;
import eqtrade.trading.model.AccTypeDef;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.AccountDef;
import eqtrade.trading.model.Announcement;
import eqtrade.trading.model.AnnouncementDef;
import eqtrade.trading.model.Board;
import eqtrade.trading.model.BoardDef;
import eqtrade.trading.model.Broker;
import eqtrade.trading.model.BrokerDef;
import eqtrade.trading.model.BrowseOrder;
import eqtrade.trading.model.BrowseOrderDef;
import eqtrade.trading.model.BuySellDetail;
import eqtrade.trading.model.BuySellDetailDef;
import eqtrade.trading.model.CashColl;
import eqtrade.trading.model.CashCollDef;
import eqtrade.trading.model.CustType;
import eqtrade.trading.model.CustTypeDef;
import eqtrade.trading.model.DueDate;
import eqtrade.trading.model.DueDateDef;
import eqtrade.trading.model.Exchange;
import eqtrade.trading.model.ExchangeDef;
import eqtrade.trading.model.GTCDef;
import eqtrade.trading.model.InvType;
import eqtrade.trading.model.InvTypeDef;
import eqtrade.trading.model.Margin;
import eqtrade.trading.model.MarginDef;
import eqtrade.trading.model.NegDealList;
import eqtrade.trading.model.NegDealListDef;
import eqtrade.trading.model.Notif;
import eqtrade.trading.model.NotifDef;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.OrderMatrix;
import eqtrade.trading.model.OrderMatrixDef;
import eqtrade.trading.model.OrderStatus;
import eqtrade.trading.model.OrderStatusDef;
import eqtrade.trading.model.Portfolio;
import eqtrade.trading.model.PortfolioDef;
import eqtrade.trading.model.Schedule;
import eqtrade.trading.model.ScheduleDef;
import eqtrade.trading.model.Stock;
import eqtrade.trading.model.StockDef;
import eqtrade.trading.model.UserProfile;
import eqtrade.trading.model.UserProfileDef;
import eqtrade.trading.model.UserType;
import eqtrade.trading.model.UserTypeDef;

public class TradingStore {
	public final static Integer DATA_ACCOUNT = new Integer(0);
	
	public final static Integer DATA_PORTFOLIO = new Integer(1);	
	public final static Integer DATA_ORDER = new Integer(2);
	public final static Integer DATA_STOCK= new Integer(3);
	public final static Integer DATA_STOCKACCOUNTTYPE = new Integer(5);
	public final static Integer DATA_ACCOUNTTYPE = new Integer(6);
	public final static Integer DATA_USERTYPE = new Integer(7);
	public final static Integer DATA_ORDERTYPE = new Integer(8);
	public final static Integer DATA_INVTYPE = new Integer(9);
	public final static Integer DATA_EXCHANGE = new Integer(10);
	public final static Integer DATA_BOARD = new Integer(10);
    public final static Integer DATA_METHOD = new Integer(11);
    public final static Integer DATA_SIDE = new Integer(12);
	public final static Integer DATA_USERPROFILE = new Integer(13);
    public final static Integer DATA_ORDERSTATUS = new Integer(14);
    public final static Integer DATA_TAKER  = new Integer(15);
    public final static Integer DATA_CUSTTYPE = new Integer(16);
    public final static Integer DATA_ACCSTATUS = new Integer(17);
    public final static Integer DATA_ACCSTOCK = new Integer(18);
    public final static Integer DATA_TIMER = new Integer(19);
    public final static Integer DATA_TRADINGACC = new Integer(20);
    public final static Integer DATA_BOARDFILTER =new Integer(21);
    public final static Integer DATA_FLOORFILTER = new Integer(22);
    public final static Integer DATA_NEGDEALLIST = new Integer(23);
    public final static Integer DATA_BROKER = new Integer(24);
    public final static Integer DATA_CASHCOLL = new Integer(25);
    public final static Integer DATA_TRADESUMM = new Integer(26);
	public final static Integer DATA_MARGIN = new Integer(27);//margin
	public final static Integer DATA_BUYSELLDETAIL = new Integer(28);
	public final static Integer DATA_DUEDATE = new Integer(29);
	public final static Integer DATA_SCHEDULELIST = new Integer(30);//Schedule
	public final static Integer DATA_BROWSEORDER = new Integer(31);//Schedule
	public final static Integer DATA_ANNOUNCEMENT = new Integer(32); 
	public final static Integer DATA_NOTIFICATION = new Integer(33);
	public final static Integer DATA_ORDERMATRIX = new Integer(33);
	public final static Integer DATA_ORDERMATRIXLIST = new Integer(34);
	public final static Integer DATA_GTC = new Integer(35);//*gtc 24112014
	public final static Integer DATA_GTCTYPE = new Integer(36);//*gtc 24112014
	
    private GridModel accountModel = new GridModel(AccountDef.getHeader(), false);
    private GridModel marginModel = new GridModel(MarginDef.getHeader(), false);//margin
    private GridModel scheduleListModel = new GridModel(ScheduleDef.getHeader(), false);//Schedule
    private GridModel browseorderModel = new GridModel(BrowseOrderDef.getHeader(), false);
    private GridModel portfolioModel = new GridModel(PortfolioDef.getHeader(), false);
    private GridModel orderModel = new GridModel(OrderDef.getHeader(), false);
    private GridModel stockModel = new GridModel(StockDef.getHeader(), false);
    private GridModel stockAccTypeModel = new GridModel(AccStockDef.getHeader(), false);
    private GridModel accountTypeModel = new GridModel(AccTypeDef.getHeader(), false);
    private GridModel userTypeModel = new GridModel(UserTypeDef.getHeader(), false);
    private GridModel orderTypeModel = new GridModel(MappingDef.getHeader(), false);
    private GridModel invTypeModel = new GridModel(InvTypeDef.getHeader(), false);
    private GridModel exchangeModel = new GridModel(ExchangeDef.getHeader(), false);
    private GridModel boardModel = new GridModel(BoardDef.getHeader(), false);
    private GridModel sideModel = new GridModel(MappingDef.getHeader(), false);
    private GridModel userProfileModel = new GridModel(UserProfileDef.getHeader(), false);
    private GridModel orderStatusModel = new GridModel(OrderStatusDef.getHeader(), false);
    private GridModel takerModel = new GridModel(MappingDef.getHeader(), false);
    private GridModel custTypeModel = new GridModel(CustTypeDef.getHeader(), false);
    private GridModel accStatusModel = new GridModel(AccStatusDef.getHeader(), false);
    private GridModel accStockModel = new GridModel(AccStatusDef.getHeader(), false);
    private GridModel timerModel = new GridModel(MappingDef.getHeader(), false);
    private GridModel tradingAccModel = new GridModel(MappingDef.getHeader(), false);
    private GridModel boardFilterModel = new GridModel(MappingDef.getHeader(), false);
    private GridModel floorFilterModel = new GridModel(MappingDef.getHeader(), false);
    private GridModel negDealListModel = new GridModel(NegDealListDef.getHeader(), false);
    private GridModel brokerModel = new GridModel(BrokerDef.getHeader(), false);
    private GridModel cashCollModel = new GridModel(CashCollDef.getHeader(), false);
    private GridModel tradeModel = new GridModel(OrderDef.getHeader(), false);
    private GridModel buysellDetailModel = new GridModel(BuySellDetailDef.getHeader(), false);
    private GridModel duedateModel = new GridModel(DueDateDef.getHeader(), false);
    private GridModel announcement = new GridModel(AnnouncementDef.getHeader(), false);
    private GridModel notification = new GridModel(NotifDef.getHeader(), false);
    private GridModel orderMatrix = new GridModel(OrderMatrixDef.getHeader(), false);
    private GridModel orderMatrixlist = new GridModel(OrderMatrixDef.getHeader(), false);
    
    private GridModel GTCModel = new GridModel (GTCDef.getHeader(), false);//*gtc 24112014
    private GridModel gtcTypeModel = new GridModel(MappingDef.getHeader(), false);//*gtc 24112014
    private HashMap map = new HashMap(33,5);
	
	public TradingStore(){
		init();
	}
	
	private void init(){
        accountModel.setKey(new int[]{Account.C_ACCID});
        marginModel.setKey(new int[]{Margin.C_ACCID});//margin
        scheduleListModel.setKey(new int[]{Schedule.C_ATSID});//Schedule
        browseorderModel.setKey(new int[]{BrowseOrder.C_ORDERID});//Schedule
        portfolioModel.setKey(new int[]{Portfolio.C_TRADINGID, Portfolio.C_STOCK});
        orderModel.setKey(new int[]{Order.C_ORDERID, Order.C_TRADENO});
        stockModel.setKey(new int[]{Stock.C_ID});
        stockAccTypeModel.setKey(new int[]{AccStock.C_ACCTYPE, AccStock.C_STOCK});
        accountTypeModel.setKey(new int[]{AccType.C_ID});
        userTypeModel.setKey(new int[]{UserType.C_ID});
        orderTypeModel.setKey(new int[]{Mapping.CIDX_CODE});
        invTypeModel.setKey(new int[]{InvType.C_ID});
        exchangeModel.setKey(new int[]{Exchange.C_ID});
        boardModel.setKey(new int[]{Board.C_ID});
        sideModel.setKey(new int[]{Mapping.CIDX_CODE});
        userProfileModel.setKey(new int[]{UserProfile.C_USERID, UserProfile.C_MENUID});
        orderStatusModel.setKey(new int[]{OrderStatus.C_ID});
        takerModel.setKey(new int[]{Mapping.CIDX_CODE});
        custTypeModel.setKey(new int[]{CustType.C_ID});
        accStatusModel.setKey(new int[]{AccStatus.C_ID});
        accStockModel.setKey(new int[]{AccStock.C_ACCTYPE, AccStock.C_STOCK});
        timerModel.setKey(new int[]{Mapping.CIDX_CODE});
        tradingAccModel.setKey(new int[]{Mapping.CIDX_CODE});
        boardFilterModel.setKey(new int[]{Mapping.CIDX_CODE});
        floorFilterModel.setKey(new int[]{Mapping.CIDX_CODE});
        negDealListModel.setKey(new int[]{NegDealList.C_MARKETORDERID});
        brokerModel.setKey(new int[]{Broker.C_ID});
        cashCollModel.setKey(new int[]{CashColl.C_TRADINGID, CashColl.C_CASHID});
        tradeModel.setKey(new int[]{Order.C_ORDERID, Order.C_PRICE});
        buysellDetailModel.setKey(new int[] {BuySellDetail.C_TRADINGID, BuySellDetail.C_ACCID, BuySellDetail.C_BUYORSELL, BuySellDetail.C_TSPAN});
        duedateModel.setKey(new int[] {DueDate.C_TRADINGID, DueDate.C_ACCID,DueDate.C_TSPAN });
        announcement.setKey(new int[]{Announcement.C_ID});
        orderMatrix.setKey(new int[] {OrderMatrix.C_LABEL});
        orderMatrixlist.setKey(new int[] {OrderMatrix.C_LABEL});
        GTCModel.setKey(new int[]{Order.C_ORDERID});//*gtc 24112014
        gtcTypeModel.setKey(new int[]{Mapping.CIDX_CODE});//*gtc 24112014
           
        notification.setKey(new int[]{Notif.C_USERID});
         
        map.put(DATA_ACCOUNT, accountModel);
        map.put(DATA_MARGIN, marginModel);
        map.put(DATA_SCHEDULELIST, scheduleListModel);//Schedule
        map.put(DATA_BROWSEORDER, browseorderModel);//Schedule
        map.put(DATA_PORTFOLIO, portfolioModel);
        map.put(DATA_ORDER, orderModel);
        map.put(DATA_STOCK, stockModel);
        map.put(DATA_STOCKACCOUNTTYPE, stockAccTypeModel);
        map.put(DATA_ACCOUNTTYPE, accountTypeModel);
        map.put(DATA_USERTYPE, userTypeModel);
        map.put(DATA_ORDERTYPE, orderTypeModel);
        map.put(DATA_INVTYPE, invTypeModel);
        map.put(DATA_EXCHANGE, exchangeModel);
        map.put(DATA_BOARD, boardModel);
        map.put(DATA_SIDE, sideModel);
        map.put(DATA_USERPROFILE, userProfileModel);
        map.put(DATA_ORDERSTATUS, orderStatusModel);
        map.put(DATA_TAKER, takerModel);
        map.put(DATA_CUSTTYPE, custTypeModel);
        map.put(DATA_ACCSTATUS, accStatusModel);
        map.put(DATA_ACCSTOCK, accStockModel);
        map.put(DATA_TIMER , timerModel);
        map.put(DATA_TRADINGACC, tradingAccModel);
        map.put(DATA_BOARDFILTER, boardFilterModel);
        map.put(DATA_FLOORFILTER, floorFilterModel);
        map.put(DATA_NEGDEALLIST, negDealListModel);
        map.put(DATA_BROKER, brokerModel);
        map.put(DATA_CASHCOLL, cashCollModel);
        map.put(DATA_TRADESUMM, tradeModel);
        map.put(DATA_BUYSELLDETAIL, buysellDetailModel);
        map.put(DATA_DUEDATE, duedateModel);
        map.put(DATA_ORDERMATRIX, orderMatrix);
        map.put(DATA_ORDERMATRIXLIST, orderMatrixlist);
        map.put(DATA_ANNOUNCEMENT, announcement); 
        map.put(DATA_NOTIFICATION, notification);
        map.put(DATA_GTC, GTCModel);//*gtc 24112014
        map.put(DATA_GTCTYPE, gtcTypeModel);//*gtc 24112014
//      
//        initBoard();
//        initExchange();
//        initInvType();
        initSide();
        initTaker();
        initOrderStatus();
        initOrderType();
        initTimer();
        initTradingAcc();
        initBoardFilter();
        initFloorFilter();
        initGtcType();//*gtc 24112014
	}
	
	public void clear(){
	       accountModel.getDataVector().clear();
	       marginModel.getDataVector().clear();//margin
	        portfolioModel.getDataVector().clear();
	        orderModel.getDataVector().clear();
	        stockModel.getDataVector().clear();
	        stockAccTypeModel.getDataVector().clear();
	        userProfileModel.getDataVector().clear();
	        accStockModel.getDataVector().clear();
	        negDealListModel.getDataVector().clear();
	        cashCollModel.getDataVector().clear();
	        tradeModel.getDataVector().clear();
	        buysellDetailModel.getDataVector().clear();
	        duedateModel.getDataVector().clear();
	        scheduleListModel.getDataVector().clear();//Schedule
	        browseorderModel.getDataVector().clear();
	        announcement.getDataVector().clear();
	        orderMatrix.getDataVector().clear();
	        orderMatrixlist.getDataVector().clear();
	        notification.getDataVector().clear();
	        GTCModel.getDataVector().clear();//*gtc 24112014
		        
	        scheduleListModel.refresh();
	        browseorderModel.refresh();
	        accountModel.refresh();
	        marginModel.refresh();//margin
	        portfolioModel.refresh();
	        orderModel.refresh();
	        stockModel.refresh();
	        stockAccTypeModel.refresh();
	        userProfileModel.refresh();
	        accStockModel.refresh();
	        negDealListModel.refresh();
	        cashCollModel.refresh();
	        tradeModel.refresh();
	        buysellDetailModel.refresh();
	        duedateModel.refresh();
	        tradeModel.refresh();
	        announcement.refresh();
	        orderMatrix.refresh();
	        orderMatrixlist.refresh();
	        notification.refresh();
	        GTCModel.refresh();//*gtc 24112014
	}
	
    private void initSide(){
        String[] bos = new String[]{"All", "Buy", "Sell"};
        String[] bosDesc = new String[]{"0", "1", "2"};
        Mapping m;
        Vector vRow = new Vector(10);
        for (int i=0; i<bos.length;i++){
            m = new Mapping();
            m.setCode(bos[i]);
            m.setName(bosDesc[i]);
            m.setDesc(bosDesc[i]);
            vRow.addElement(MappingDef.createTableRow(m));
        }
        sideModel.addRow(vRow, false, false);        
    }
    
    private void initTradingAcc(){
        String[] ai = new String[]{"Indonesia", "Asing"};
        String[] aiDesc = new String[]{"I", "A"};
        Mapping m;
        Vector vRow = new Vector(10);
        for (int i=0; i<ai.length;i++){
            m = new Mapping();
            m.setCode(ai[i]);
            m.setName(aiDesc[i]);
            m.setDesc(aiDesc[i]);
            vRow.addElement(MappingDef.createTableRow(m));
        }
        tradingAccModel.addRow(vRow, false, false);        
    }

    private void initTimer(){
        String[] bos = new String[]{"Now", "Custom"};
        String[] bosDesc = new String[]{"0", "09:15:00", "09:30:00", "13:30:00", "-1"};
        Mapping m;
        Vector vRow = new Vector(10);
        for (int i=0; i<bos.length;i++){
            m = new Mapping();
            m.setCode(bos[i]);
            m.setName(bosDesc[i]);
            m.setDesc(bosDesc[i]);
            vRow.addElement(MappingDef.createTableRow(m));
        }
        timerModel.addRow(vRow, false, false);        
    }

    private void initTaker(){
        String[] bos = new String[]{"my order", "taker order","all order"};
        String[] bosDesc = new String[]{"1", "2","3"};
        Mapping m;
        Vector vRow = new Vector(10);
        for (int i=0; i<bos.length;i++){
            m = new Mapping();
            m.setCode(bos[i]);
            m.setName(bosDesc[i]);
            m.setDesc(bosDesc[i]);
            vRow.addElement(MappingDef.createTableRow(m));
        }
        takerModel.addRow(vRow, false, false);        
    }
    
    private void initOrderStatus(){
        String[] bos = new String[]{"All","O|P", "ST|SW|SA|SD|SE|STS|SWS|SAS|SDS|SES","E|EW|EA|ET|ED"};
        String[] bosDesc = new String[]{"All", "Open", "Sending", "Request"};
        OrderStatus m;
        Vector vRow = new Vector(10);
        for (int i=0; i<bos.length;i++){
            m = new OrderStatus();
            m.setId(bos[i]);
            m.setName(bosDesc[i]);
            vRow.addElement(OrderStatusDef.createTableRow(m));
        }
        orderStatusModel.addRow(vRow, false, false);        
    }

    private void initBoardFilter(){
        String[] ord = new String[]{"All", "RG|TN", "NG"};
        String[] ordDesc = new String[]{"All", "Regular", "Non-Regular"};
        String[] map = new String[]{"All", "RG|TN","NG"};
        Mapping m;
        Vector vRow = new Vector(10);
        for (int i=0; i<ord.length;i++){
            m = new Mapping();
            m.setCode(ord[i]);
            m.setName(ordDesc[i]);
            m.setDesc(map[i]);
            vRow.addElement(MappingDef.createTableRow(m));
        }
        boardFilterModel.addRow(vRow, false, false);        
    }
  
    private void initFloorFilter(){
        String[] ord = new String[]{"All", "0", "1"};
        String[] ordDesc = new String[]{"All", "Remote", "Floor"};
        String[] map = new String[]{"All", "0","1"};
        Mapping m;
        Vector vRow = new Vector(10);
        for (int i=0; i<ord.length;i++){
            m = new Mapping();
            m.setCode(ord[i]);
            m.setName(ordDesc[i]);
            m.setDesc(map[i]);
            vRow.addElement(MappingDef.createTableRow(m));
        }
        floorFilterModel.addRow(vRow, false, false);        
    }

    private void initOrderType(){
        String[] ord = new String[]{"Day", "Session"};
        String[] ordDesc = new String[]{"Day Order", "Session Order"};
        String[] map = new String[]{"0","S"};
        Mapping m;
        Vector vRow = new Vector(10);
        for (int i=0; i<ord.length;i++){
            m = new Mapping();
            m.setCode(ord[i]);
            m.setName(ordDesc[i]);
            m.setDesc(map[i]);
            vRow.addElement(MappingDef.createTableRow(m));
        }
        //*gtc 24112014
        orderTypeModel.addRow(vRow, false, false);   //*gtc     
    }
    
    private void initGtcType(){//*gtc
        String[] ord = new String[]{"Day", "Session","GTC"};
        String[] ordDesc = new String[]{"Day Order", "Session Order","G"};
        String[] map = new String[]{"0","S","G"};
        Mapping m;//*gtc
        Vector vRow = new Vector(10);
        for (int i=0; i<ord.length;i++){
            m = new Mapping();
            m.setCode(ord[i]);
            m.setName(ordDesc[i]);
            m.setDesc(map[i]);
            vRow.addElement(MappingDef.createTableRow(m));
        }//*gtc
        gtcTypeModel.addRow(vRow, false, false);  
    }
    
    public void registerModel(Integer key, GridModel model){
    	map.put(key, model);
    }
	
	public GridModel get(Integer key){
		return (GridModel)map.get(key);
	}
}
