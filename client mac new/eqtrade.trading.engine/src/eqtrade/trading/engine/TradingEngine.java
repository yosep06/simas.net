package eqtrade.trading.engine;

import java.util.Vector;

import com.vollux.idata.GridModel;

import eqtrade.trading.core.Event;
import eqtrade.trading.core.ITradingApp;
import eqtrade.trading.core.ITradingConnection;
import eqtrade.trading.core.ITradingEngine;
import eqtrade.trading.core.ITradingListener;
import eqtrade.trading.core.OrderMatrixData;
import eqtrade.trading.engine.socket.TradingSocketConnection;
import eqtrade.trading.model.Notif;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderMatrix;
import eqtrade.trading.model.Schedule;

public class TradingEngine implements ITradingEngine, ITradingApp {
	private ITradingConnection comm = null;
	private TradingStore store = null;
	private TradingEventDispatcher event = null;
	private Vector vlistener = null;

	public TradingEngine() {
	}

	@Override
	public void start() {
		vlistener = new Vector(5, 1);
		store = new TradingStore();
		//comm = new TradingConnection(this);
		comm = new TradingSocketConnection(this);
		event = new TradingEventDispatcher(this);
	}

	@Override
	public void addListener(ITradingListener listener) {
		if (!vlistener.contains(listener)) {
			vlistener.add(listener);
		}
	}

	@Override
	public void removeListener(ITradingListener listener) {
		vlistener.remove(listener);
	}

	@Override
	public Vector getEventListener() {
		return vlistener;
	}

	@Override
	public ITradingConnection getConnection() {
		return comm;
	}

	@Override
	public TradingStore getDataStore() {
		return store;
	}

	@Override
	public TradingEventDispatcher getEventDispatcher() {
		return event;
	}

	// ---------------------------------------------------------------------------------------------------------
	@Override
	public void login(String userid, String passwd) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_LOGIN).setField(
				TradingEventDispatcher.PARAM_USERID, userid).setField(
				TradingEventDispatcher.PARAM_PASSWORD, passwd));
	}

	@Override
	public void checkPIN(String userid, String pin) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_CHECKPIN)
				.setField(TradingEventDispatcher.PARAM_USERID, userid)
				.setField(TradingEventDispatcher.PARAM_PIN, pin));
	}

	@Override
	public void checkPINSLS(String userid, String pin) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_CHECKPINSLS)
				.setField(TradingEventDispatcher.PARAM_USERID, userid)
				.setField(TradingEventDispatcher.PARAM_PIN, pin));
	}

	@Override
	public void changePasswd(String oldPasswd, String newPasswd) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_CHGPASSWD)
				.setField(TradingEventDispatcher.PARAM_OLDPASSWORD, oldPasswd)
				.setField(TradingEventDispatcher.PARAM_PASSWORD, newPasswd));
	}

	@Override
	public void changePIN(String oldPIN, String newPIN) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_CHGPIN)
				.setField(TradingEventDispatcher.PARAM_OLDPIN, oldPIN)
				.setField(TradingEventDispatcher.PARAM_PIN, newPIN));
	}

	@Override
	public void reconnect(int counter) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_RECONNECT)
				.setField(TradingEventDispatcher.PARAM_COUNTER, new Integer(
						counter)));
	}

	@Override
	public void logout() {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_LOGOUT));
	}

	@Override
	public void refreshData(boolean master, boolean account, boolean margin,
			boolean pf, boolean order) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH)
				.setField("MASTER", new Boolean(master))
				.setField("ACCOUNT", new Boolean(account))
				.setField("MARGIN", new Boolean(margin))
				.setField("PF", new Boolean(pf))
				.setField("ORDER", new Boolean(order)));
	}

	@Override
	public void refreshData(String tradingid) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH_BYID)
				.setField("TRADINGID", tradingid));
	}

	@Override
	public void refreshOrder(String param1, String param2, String param3) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH_ORDER)
				.setField(TradingEventDispatcher.PARAM_ACCOUNTID, param1)
				.setField(TradingEventDispatcher.PARAM_ORDERID, param2)
				.setField(TradingEventDispatcher.PARAM_STOCK, param3));
	}

	@Override
	public void refreshTrade(String orderid) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH_TRADE)
				.setField(TradingEventDispatcher.PARAM_ORDERID, orderid));
	}

	@Override
	public void refreshPortfolio(String param1, String param2) {
		event.pushEvent(new Event(
				TradingEventDispatcher.EVENT_REFRESH_PORTFOLIO).setField(
				TradingEventDispatcher.PARAM_ACCOUNTID, param1).setField(
				TradingEventDispatcher.PARAM_STOCK, param2));
	}

	@Override
	public void refreshAccount(String param) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH_ACCOUNT)
				.setField(TradingEventDispatcher.PARAM_ACCOUNTID, param));
	}

	@Override
	public void refreshCashColl(String param) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH_CASHCOLL)
				.setField(TradingEventDispatcher.PARAM_TRADINGID, param));
	}

	@Override
	public void refreshBuySellDetail(String param1, String param2) {
		event.pushEvent(new Event(
				TradingEventDispatcher.EVENT_REFRESH_BUYSELLDETAIL).setField(
				TradingEventDispatcher.PARAM_TRADINGID, param1).setField(
				TradingEventDispatcher.PARAM_ACCOUNTID, param2));
		// .setField(TradingEventDispatcher.PARAM_BUYORSELL, param3));
	}

	public void refreshDueDate(String param1) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH_DUEDATE)
				.setField(TradingEventDispatcher.PARAM_TRADINGID, param1));
		// .setField(TradingEventDispatcher.PARAM_BUYORSELL, param3));
	}

	@Override
	public void refreshNotif(String param) {
		event.pushEvent(new Event(
				TradingEventDispatcher.EVENT_REFRESH_BACKNOTIF).setField(
				TradingEventDispatcher.PARAM_TRADINGID, param));

	}

	@Override
	public void refreshAts(String param1) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH_ATS)
				.setField(TradingEventDispatcher.PARAM_ACCOUNTID, param1));
	}

	@Override
	public void refreshBrowseAts(String param) {
		event.pushEvent(new Event(
				TradingEventDispatcher.EVENT_REFRESH_BROWSEATS).setField(
				TradingEventDispatcher.PARAM_ACCOUNTID, param));
	}

	@Override
	public void stop() {
	}

	@Override
	public GridModel getStore(Integer id) {
		return store.get(id);
	}

	@Override
	public String getPassword() {
		return comm.getPassword();
	}

	@Override
	public String getPIN() {
		return comm.getPIN();
	}

	@Override
	public String getUserId() {
		return comm.getUserId();
	}

	public String getIPServer() {
		return comm.getIPServer();
	}

	@Override
	public void createOrder(final Order order) {
		// event.pushEvent(new Event(TradingEventDispatcher.EVENT_CREATEORDER)
		// .setField(TradingEventDispatcher.PARAM_ORDER, order)
		// .setField(TradingEventDispatcher.PARAM_OLDSTATUS, order.getStatus())
		// );
		Event e = new Event(TradingEventDispatcher.EVENT_CREATEORDER).setField(
				TradingEventDispatcher.PARAM_ORDER, order).setField(
				TradingEventDispatcher.PARAM_OLDSTATUS, order.getStatus());
//		System.out.println("status " + order.getStatus());
//		System.out.println("status " + order.toString() + " -- "
//				+ e.getParam().get(TradingEventDispatcher.PARAM_ORDER));
		event.getEventHandler(TradingEventDispatcher.EVENT_CREATEORDER)
				.execute(e);

	}

	@Override
	public void createNotif(Notif notif) {
		Event e = new Event(TradingEventDispatcher.EVENT_NOTIFICATION)
				.setField(TradingEventDispatcher.PARAM_NOTIFICAION, notif);
		event.getEventHandler(TradingEventDispatcher.EVENT_NOTIFICATION)
				.execute(e);
	}

	@Override
	public void deleteOrder(Order order) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_DELETEORDER)
				.setField(TradingEventDispatcher.PARAM_ORDER, order).setField(
						TradingEventDispatcher.PARAM_OLDSTATUS,
						order.getStatus()));
	}

	@Override
	public void sendOrder(Order order) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_SENDORDER)
				.setField(TradingEventDispatcher.PARAM_ORDER, order).setField(
						TradingEventDispatcher.PARAM_OLDSTATUS,
						order.getStatus()));
	}

	@Override
	public void withdrawOrder(Order order) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REQUESTWITHDRAW)
				.setField(TradingEventDispatcher.PARAM_ORDER, order).setField(
						TradingEventDispatcher.PARAM_OLDSTATUS,
						order.getStatus()));
	}

	@Override
	public void amendOrder(Order oldorder, Order neworder) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REQUESTAMEND)
				.setField(TradingEventDispatcher.PARAM_OLDORDER, oldorder)
				.setField(TradingEventDispatcher.PARAM_NEWORDER, neworder)
				.setField(TradingEventDispatcher.PARAM_OLDSTATUS,
						oldorder.getStatus()));
	}

	@Override
	public String splitOrder(String param) {
		return getConnection().splitOrder(param);
	}

	@Override
	public void createOrderMulti(Vector order) {
		Event e = new Event(TradingEventDispatcher.EVENT_CREATEORDERMULTI)
				.setField(TradingEventDispatcher.PARAM_ORDER, order).setField(
						TradingEventDispatcher.PARAM_OLDSTATUS, Order.C_ENTRY);
		event.getEventHandler(TradingEventDispatcher.EVENT_CREATEORDERMULTI)
				.execute(e);

	}

	@Override
	public void withdrawOrderMulti(Vector order) {
		event.pushEvent(new Event(
				TradingEventDispatcher.EVENT_REQUESTWITHDRAW_MULTI).setField(
				TradingEventDispatcher.PARAM_ORDER, order).setField(
				TradingEventDispatcher.PARAM_OLDSTATUS, Order.C_OPEN));
	}

	@Override
	public void createSchedule(Schedule schedule) {
		Event e = new Event(TradingEventDispatcher.EVENT_CREATESCHEDULE)
				.setField(TradingEventDispatcher.PARAM_SCHEDULE, schedule)
				.setField(TradingEventDispatcher.PARAM_OLDSCHEDULE,
						schedule.getcOrderstatus());//
		event.getEventHandler(TradingEventDispatcher.EVENT_CREATESCHEDULE)
				.execute(e);
	}

	@Override
	public void withdrawAts(Schedule schedule) {
		Event e = new Event(TradingEventDispatcher.EVENT_WITHDRAWATS).setField(
				TradingEventDispatcher.PARAM_SCHEDULE, schedule).setField(
				TradingEventDispatcher.PARAM_OLDSCHEDULE,
				schedule.getcOrderstatus());
		event.getEventHandler(TradingEventDispatcher.EVENT_WITHDRAWATS).execute(e);
	}

	@Override
	public void createOrderMatrix(String label, OrderMatrixData[] data, boolean isbasket, String basketsendtime) {
		Event e = new Event(TradingEventDispatcher.EVENT_CREATEORDERMATRIX)
						.setField(TradingEventDispatcher.PARAM_ORDERMATRIXLABEL, label)
						.setField(TradingEventDispatcher.PARAM_DATA, data)
						.setField("ISBASKET", new Boolean(isbasket))
						.setField(TradingEventDispatcher.PARAM_BASKETSENDTIME, basketsendtime);
		event.getEventHandler(TradingEventDispatcher.EVENT_CREATEORDERMATRIX).execute(e);
	}

	@Override
	public void refreshOrderMatrix(String param) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH_LOADORDERMATRIX).setField(TradingEventDispatcher.PARAM_TRADINGID, param));
	}

	@Override
	public void refreshOrderMatrixList(String param, String param2) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH_LOADORDERMATRIXLIST)
		.setField(TradingEventDispatcher.PARAM_TRADINGID, param)
		.setField(TradingEventDispatcher.PARAM_USERID, param2));
	}

	@Override
	public void deleteOrderMatrix(Vector param) {
		Event e = new Event(TradingEventDispatcher.EVENT_DELETEORDERMATRIX)
			.setField(TradingEventDispatcher.PARAM_DATA, param);
		event.getEventHandler(TradingEventDispatcher.EVENT_DELETEORDERMATRIX).execute(e);
		
	}

	@Override
	public void deleteOrderMatrixDetil(Vector param) {
		Event e = new Event(TradingEventDispatcher.EVENT_DELETEORDERMATRIXDETIL)
		.setField(TradingEventDispatcher.PARAM_DATA, param);
	event.getEventHandler(TradingEventDispatcher.EVENT_DELETEORDERMATRIXDETIL).execute(e);
			
	}

	@Override
	public void deleteOrderMatrixByLabel(String userid, String label) {
		Event e = new Event(TradingEventDispatcher.EVENT_DELETEORDERMATRIXBYLABEL).setField(TradingEventDispatcher.PARAM_USERID, userid).setField(TradingEventDispatcher.PARAM_ORDERMATRIXLABEL, label);
		event.getEventHandler(TradingEventDispatcher.EVENT_DELETEORDERMATRIXBYLABEL).execute(e);
	}
	
	@Override
	public void checkPIN_NEGO(String pin, String form) {
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_CHECKPIN_NEGO)
				.setField(TradingEventDispatcher.PARAM_PIN, pin)
				.setField(TradingEventDispatcher.PARAM_FORM, form));
	}// *gtc 24112014
	@Override
	public void GTC(Order gtc) {
		// TODO Auto-generated method stub
		Event a = new Event(TradingEventDispatcher.EVENT_CREATEGTC).setField(
				TradingEventDispatcher.PARAM_GTC,gtc)
				.setField(TradingEventDispatcher.PARAM_OLDSCHEDULE, gtc.getStatus());//
		event.getEventHandler(TradingEventDispatcher.EVENT_CREATEGTC).
			execute(a);	

	}

	@Override
	public void createGTCMulti(Vector order) {
		// TODO Auto-generated method stub
		Event t = new Event(TradingEventDispatcher.EVENT_CREATEGTCMULTI)
				.setField(TradingEventDispatcher.PARAM_GTC,order);
				//.setField(TradingEventDispatcher.PARAM_OLDSTATUS, Order.C_ENTRY);
		event.getEventHandler(TradingEventDispatcher.EVENT_CREATEGTCMULTI).
			execute(t);	

	}

	@Override
	public void withdrawGtc(Order gtc) {
		// TODO Auto-generated method stub
		event.pushEvent(new Event(
				TradingEventDispatcher.EVENT_WITHDRAWGTC).setField(
				TradingEventDispatcher.PARAM_GTC, gtc).setField(
				TradingEventDispatcher.PARAM_OLDSTATUS, Order.C_OPEN));

	}

	@Override
	public void AmmendGtc(Order oldorder, Order neworder) {
		// TODO Auto-generated method stub
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_AMMENDGTC)
		.setField(TradingEventDispatcher.PARAM_OLDORDER, oldorder)
		.setField(TradingEventDispatcher.PARAM_NEWORDER, neworder)
		.setField(TradingEventDispatcher.PARAM_OLDSTATUS, Order.C_AMEND));

	}

	@Override
	public void refreshGTC(String param, String param2) {
		// TODO Auto-generated method stub
		
		event.pushEvent(new Event(TradingEventDispatcher.EVENT_REFRESH_GTC)
		.setField(TradingEventDispatcher.PARAM_ACCOUNTID, param)
		.setField(TradingEventDispatcher.PARAM_ORDERID, param2));
	}
}
