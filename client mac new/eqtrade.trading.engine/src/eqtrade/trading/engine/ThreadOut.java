package eqtrade.trading.engine;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import quickfix.Message;
import eqtrade.trading.core.Event;
import eqtrade.trading.core.ITradingEngine;
import eqtrade.trading.core.Utils;

public class ThreadOut extends Thread {
	private final Log log = LogFactory.getLog(getClass());
	protected ITradingEngine engine;
	private volatile boolean bstop = false;
	private volatile Vector vou = new Vector(10, 5);
	protected final static String CMD_REQUEST = "RQT";

	public ThreadOut(ITradingEngine engine) {
		super();
		this.engine = engine;
	}

	public void doClear() {
		vou.clear();
	}

	public void setStop() {
		bstop = true;
		synchronized (vou) {
			vou.notify();
		}
	}

	public void addOut(Event smsg) {
		synchronized (vou) {
			vou.addElement(smsg);
			vou.notify();
		}
	}

	public Vector popOutMsg() {
		synchronized (vou) {
			if (vou.size() > 0) {
				Event e = (Event) vou.remove(0);
				Vector vtemp = new Vector(1);
				vtemp.addElement(e);
				return vtemp;
			} else {
				return null;
			}
		}
	}

	@Override
	public void run() {
		Vector vtemp;
		while (!bstop) {
			try {
				vtemp = popOutMsg();
				if (vtemp != null) {
					processOut(vtemp);
					Thread.yield();
					// try {Thread.sleep(100);} catch (Exception ex){}
					// this.sleep(100);
				} else {
					synchronized (vou) {
						try {
							vou.wait();
						} catch (Exception ex) {
						}
					}
				}
			} catch (Exception ex) {
				log.error(Utils.logException(ex));
			}
		}
	}

	/*
	 * private void processOut(Vector vdatamsg){ boolean success = false; int i
	 * = 0; try { for (; i < vdatamsg.size(); i++){ Event evt =
	 * (Event)vdatamsg.elementAt(i); if (engine.getConnection().sesid > -1){
	 * String event = evt.getName(); log.info("send to server: "+event+" ("
	 * +engine
	 * .getConnection().sesid+")  "+evt.getField(TradingEventDispatcher.PARAM_FIX
	 * )); if (event.equals(TradingEventDispatcher.EVENT_DELETEORDER)){ //Order
	 * ord = (Order)evt.getField(TradingEventDispatcher.PARAM_ORDER); //success
	 * = engine.getConnection().rmi.deleteTemp(engine.getConnection().sesid,
	 * Utils.compress(ord.getId().getBytes())); } else if
	 * (event.equals(TradingEventDispatcher.EVENT_REQUESTWITHDRAW)){ success =
	 * engine.getConnection().rmi.entryWithdraw(engine.getConnection().sesid,
	 * Utils
	 * .compress(((Message)evt.getField(TradingEventDispatcher.PARAM_FIX)).toString
	 * ().getBytes())); } else if
	 * (event.equals(TradingEventDispatcher.EVENT_REQUESTAMEND)){ success =
	 * engine.getConnection().rmi.entryAmend(engine.getConnection().sesid,
	 * Utils.
	 * compress(((Message)evt.getField(TradingEventDispatcher.PARAM_FIX)).toString
	 * ().getBytes())); } else if
	 * (event.equals(TradingEventDispatcher.EVENT_CREATEORDER)){ success =
	 * engine.getConnection().rmi.createOrder(engine.getConnection().sesid,
	 * Utils
	 * .compress(((Message)evt.getField(TradingEventDispatcher.PARAM_FIX)).toString
	 * ().getBytes())); } else { //success =
	 * engine.getConnection().rmi.send_fixmsg(engine.getConnection().sesid,
	 * Utils
	 * .compress(((Message)evt.getField(TradingEventDispatcher.PARAM_FIX)).toString
	 * ().getBytes())); } if (!success){ if (evt.isNeedACK()){ HashMap param =
	 * evt.getParam(); param.put(TradingEventDispatcher.PARAM_RESULT, new
	 * Boolean(false)); engine.getEventDispatcher().pushEvent(new
	 * Event(evt.getName()+".nack", param)); } throw new
	 * Exception("send message to server failed, result from server is: "
	 * +success); } if (evt.isNeedACK()){ HashMap param = evt.getParam();
	 * param.put(TradingEventDispatcher.PARAM_RESULT, new Boolean(success));
	 * engine.getEventDispatcher().pushEvent(new Event(evt.getName()+".ack",
	 * param)); } try {Thread.sleep(10);} catch (Exception ex){} } else { int
	 * from = i, to = vdatamsg.size(); for (int k=from; k<to;k++){ Event evt2 =
	 * (Event)vdatamsg.elementAt(k); if (!evt2.getName().equals(CMD_REQUEST)){
	 * if (evt2.isNeedACK()){ HashMap param = evt2.getParam();
	 * param.put(TradingEventDispatcher.PARAM_RESULT, new Boolean(false));
	 * engine.getEventDispatcher().pushEvent(new Event(evt2.getName()+".nack",
	 * param)); } } } break; } } }catch (RemoteException ex){ //kembalikan
	 * dahulu ke status semula int from = i, to = vdatamsg.size(); for (int
	 * k=from; k<to;k++){ Event evt2 = (Event)vdatamsg.elementAt(k); if
	 * (!evt2.getName().equals(CMD_REQUEST)){ if (evt2.isNeedACK() &&
	 * !evt2.getName().endsWith(".callback")){ HashMap param = evt2.getParam();
	 * param.put(TradingEventDispatcher.PARAM_RESULT, new Boolean(false));
	 * engine.getEventDispatcher().pushEvent(new Event(evt2.getName()+".nack",
	 * param)); } } } log.error(Utils.logException(ex));
	 * engine.getConnection().reconnect(); }catch (Exception ex){
	 * ex.printStackTrace(); //kembalikan dahulu ke status semula int from = i,
	 * to = vdatamsg.size(); for (int k=from; k<to;k++){ Event evt2 =
	 * (Event)vdatamsg.elementAt(k); if (!evt2.getName().equals(CMD_REQUEST)){
	 * if (evt2.isNeedACK() && !evt2.getName().endsWith(".callback")){ HashMap
	 * param = evt2.getParam(); param.put(TradingEventDispatcher.PARAM_RESULT,
	 * new Boolean(false)); engine.getEventDispatcher().pushEvent(new
	 * Event(evt2.getName()+".nack", param)); } } }
	 * log.error(Utils.logException(ex)); engine.getConnection().reconnect(); }
	 * }
	 */

	public void processOut(Vector vdatamsg) {
		boolean success = false;
		int i = 0;
		try {
			for (; i < vdatamsg.size(); i++) {
				Event evt = (Event) vdatamsg.elementAt(i);
				if (engine.getConnection().getSessionid() > -1) {
					String event = evt.getName();
					/*log.info("send to server: " + event + " ("
							+ engine.getConnection().getSessionid() + ")  "
							+ evt.getField(TradingEventDispatcher.PARAM_FIX));*/
					if (event.equals(TradingEventDispatcher.EVENT_DELETEORDER)) {
						// Order ord =
						// (Order)evt.getField(TradingEventDispatcher.PARAM_ORDER);
						// success =
						// engine.getConnection().rmi.deleteTemp(engine.getConnection().sesid,
						// Utils.compress(ord.getId().getBytes()));
					} else if (event
							.equals(TradingEventDispatcher.EVENT_REQUESTWITHDRAW)) {
						success = engine
								.getConnection()
								.entryWithdraw(
										Utils.compress(((Message) evt
												.getField(TradingEventDispatcher.PARAM_FIX))
												.toString().getBytes()));
					} else if (event
							.equals(TradingEventDispatcher.EVENT_REQUESTAMEND)) {
						success = engine
								.getConnection()
								.entryAmend(
										Utils.compress(((Message) evt
												.getField(TradingEventDispatcher.PARAM_FIX))
												.toString().getBytes()));
					} else if (event
							.equals(TradingEventDispatcher.EVENT_CREATESCHEDULE)) {
						success = engine
								.getConnection()
								.createAts(
										Utils.compress(((Message) evt
												.getField(TradingEventDispatcher.PARAM_FIX))
												.toString().getBytes()));
					} else if (event
							.equals(TradingEventDispatcher.EVENT_WITHDRAWATS)) {
						success = engine
								.getConnection()
								.atsWithdraw(
										Utils.compress(((Message) evt
												.getField(TradingEventDispatcher.PARAM_FIX))
												.toString().getBytes()));
					} else if (event
							.equals(TradingEventDispatcher.EVENT_CREATEORDER)) {
						success = engine
								.getConnection()
								.createOrder(
										Utils.compress(((Message) evt
												.getField(TradingEventDispatcher.PARAM_FIX))
												.toString().getBytes()));
					} else if (event
							.equals(TradingEventDispatcher.EVENT_NOTIFICATION)) {
						success = engine
								.getConnection()
								.notification(
										Utils.compress(((Message) evt
												.getField(TradingEventDispatcher.PARAM_FIX))
												.toString().getBytes()));
						// success =
						// engine.getConnection().rmi.send_fixmsg(engine.getConnection().sesid,
						// Utils.compress(((Message)evt.getField(TradingEventDispatcher.PARAM_FIX)).toString().getBytes()));
					} else {
						// success =
						// engine.getConnection().rmi.send_fixmsg(engine.getConnection().sesid,
						// Utils.compress(((Message)evt.getField(TradingEventDispatcher.PARAM_FIX)).toString().getBytes()));
					}
					// if (!success){
					// if (evt.isNeedACK()){
					// HashMap param = evt.getParam();
					// param.put(TradingEventDispatcher.PARAM_RESULT, new
					// Boolean(false));
					// engine.getEventDispatcher().pushEvent(new
					// Event(evt.getName()+".nack", param));
					// }
					// throw new
					// Exception("send message to server failed, result from server is: "+success);
					// }
					// if (evt.isNeedACK()){
					// HashMap param = evt.getParam();
					// param.put(TradingEventDispatcher.PARAM_RESULT, new
					// Boolean(success));
					// engine.getEventDispatcher().pushEvent(new
					// Event(evt.getName()+".ack", param));
					// }
					// try {Thread.sleep(10);} catch (Exception ex){}
					/*log.info("send order finished " + event + " ("
							+ engine.getConnection().getSessionid() + ")  ");*/
				} else {
					int from = i, to = vdatamsg.size();
					for (int k = from; k < to; k++) {
						Event evt2 = (Event) vdatamsg.elementAt(k);
						if (!evt2.getName().equals(CMD_REQUEST)) {
							if (evt2.isNeedACK()) {
								HashMap param = evt2.getParam();
								param.put(TradingEventDispatcher.PARAM_RESULT,
										new Boolean(false));
								engine.getEventDispatcher().pushEvent(
										new Event(evt2.getName() + ".nack",
												param));
							}
						}
					}
					break;
				}
			}
		} catch (RemoteException ex) {
			// kembalikan dahulu ke status semula
			int from = i, to = vdatamsg.size();
			for (int k = from; k < to; k++) {
				Event evt2 = (Event) vdatamsg.elementAt(k);
				if (!evt2.getName().equals(CMD_REQUEST)) {
					if (evt2.isNeedACK()
							&& !evt2.getName().endsWith(".callback")) {
						HashMap param = evt2.getParam();
						param.put(TradingEventDispatcher.PARAM_RESULT,
								new Boolean(false));
						engine.getEventDispatcher().pushEvent(
								new Event(evt2.getName() + ".nack", param));
					}
				}
			}
			//log.error(Utils.logException(ex));
			engine.getConnection().reconnect();
		} catch (Exception ex) {
			ex.printStackTrace();
			// kembalikan dahulu ke status semula
			int from = i, to = vdatamsg.size();
			for (int k = from; k < to; k++) {
				Event evt2 = (Event) vdatamsg.elementAt(k);
				if (!evt2.getName().equals(CMD_REQUEST)) {
					if (evt2.isNeedACK()
							&& !evt2.getName().endsWith(".callback")) {
						HashMap param = evt2.getParam();
						param.put(TradingEventDispatcher.PARAM_RESULT,
								new Boolean(false));
						engine.getEventDispatcher().pushEvent(
								new Event(evt2.getName() + ".nack", param));
					}
				}
			}
			//log.error(Utils.logException(ex));
			engine.getConnection().reconnect();
		}
	}

}
