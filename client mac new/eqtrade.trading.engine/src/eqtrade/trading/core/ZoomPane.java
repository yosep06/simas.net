package eqtrade.trading.core;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.KeyStroke;

public class ZoomPane extends JPanel {

    private Image background;
    private Image scaled;
    private float zoom = 1f;

    private Dimension scaledSize;
    private JViewport con;

    public ZoomPane(Image img) {
        background = img;//ImageIO.read(new File("/Users/sinarmassekuritas/Desktop/logon_20110427.png"));
		scaled = background;
		scaledSize = new Dimension(background.getWidth(this), background.getHeight(this));

        InputMap im = getInputMap(WHEN_IN_FOCUSED_WINDOW);
        ActionMap am = getActionMap();

        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, 0), "plus");
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS, InputEvent.SHIFT_DOWN_MASK), "plus");
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, 0), "minus");

        am.put("plus", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setZoom(getZoom() + 0.1f);
            }
        });
        am.put("minus", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setZoom(getZoom() - 0.1f);
            }
        });
        centerInViewport();
        setFocusable(true);
        requestFocusInWindow();

    }
    public ZoomPane(){
    	 InputMap im = getInputMap(WHEN_IN_FOCUSED_WINDOW);
         ActionMap am = getActionMap();

         im.put(KeyStroke.getKeyStroke(KeyEvent.VK_PLUS, 0), "plus");
         im.put(KeyStroke.getKeyStroke(KeyEvent.VK_EQUALS, InputEvent.SHIFT_DOWN_MASK), "plus");
         im.put(KeyStroke.getKeyStroke(KeyEvent.VK_MINUS, 0), "minus");

         am.put("plus", new AbstractAction() {
             @Override
             public void actionPerformed(ActionEvent e) {
                 setZoom(getZoom() + 0.1f);
             }
         });
         am.put("minus", new AbstractAction() {
             @Override
             public void actionPerformed(ActionEvent e) {
                 setZoom(getZoom() - 0.1f);
             }
         });
         centerInViewport();
         setFocusable(true);
         requestFocusInWindow();
    }
    
    public void changeimage(Image img){
    	 background = img;//ImageIO.read(new File("/Users/sinarmassekuritas/Desktop/logon_20110427.png"));
 		scaled = background;
 		scaledSize = new Dimension(background.getWidth(this), background.getHeight(this));
 		repaint();
    }
    @Override
    public void addNotify() {

        super.addNotify();

    }

    public float getZoom() {
        return zoom;
    }
    
    public Image getImage(){
    	return background;
    }

    public void setZoom(float value) {
        if (zoom != value) {
            zoom = value;

            if (zoom < 0) {
                zoom = 0f;
            }

            int width = (int) Math.floor(background.getWidth(this) * zoom);
            int height = (int) Math.floor(background.getHeight(this) * zoom);
            scaled = background.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            scaledSize = new Dimension(width, height);

            if (getParent() instanceof JViewport) {

                int centerX = width / 2;
                int centerY = height / 2;

                JViewport parent = (JViewport) getParent();
                Rectangle viewRect = parent.getViewRect();
                viewRect.x = centerX - (viewRect.width / 2);
                viewRect.y = centerY - (viewRect.height / 2);
                scrollRectToVisible(viewRect);
                x = viewRect.x<0?((viewRect.width / 2)-centerX):0;
                y = viewRect.y<0?((viewRect.height/ 2)-centerY):0;
            }

            invalidate();
            repaint();

        }
    }

    int x,y;
    
    @Override
    public Dimension getPreferredSize() {

        return scaledSize;

    }

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);

        if (scaled != null) {

            g.drawImage(scaled, x, y, this);

        }

    }

    protected void centerInViewport() {

        Container container = getParent();
        if (container instanceof JViewport) {

            JViewport port = (JViewport) container;
            Rectangle viewRect = port.getViewRect();

            int width = getWidth();
            int height = getHeight();

            viewRect.x = (width - viewRect.width) / 2;
            viewRect.y = (height - viewRect.height) / 2;

            scrollRectToVisible(viewRect);

        }

    }
}
