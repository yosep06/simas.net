package eqtrade.trading.core;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Hashtable;
import java.util.Properties;

public final class TradingSetting {
    private static Hashtable hlayout;
    private static Hashtable hcolor;
    public static String C_CURRENT_LAYOUT ;
    public static Hashtable hconfig;
    
    static {
        hlayout = new Hashtable();
        hconfig = new Hashtable();
        defaultColor();
        C_CURRENT_LAYOUT = loadCurrent();
    }
    
    public static final String C_BACKGROUND = "color.background";
    public static final String C_BACKGROUNDEVEN = "color.background.even";
    public static final String C_FOREGROUND = "color.foreground";
    public static final String C_PLUS = "color.plus";
    public static final String C_MINUS = "color.minus";
    public static final String C_ZERO = "color.zero";
    public static final String C_BUY = "color.buy";
    public static final String C_SELL= "color.sell";
    public static final String C_OTHER = "color.other";
    public static final String C_FONT = "default.font";
    public static final Integer C_MAXSPLIT = 100;
    public static final Integer C_MAXSPLITLOT  = 50000;
    
    public static final String C_POPUP = "popup";
    public static final String C_PLAYSOUND = "playsound";
    public static final String C_ANNOUNCEMENT = "announcement";
    
    private static void defaultColor(){
    	hcolor = new Hashtable();
    	hcolor.put(C_BACKGROUND, new Color(11,16,16));
    	hcolor.put(C_BACKGROUNDEVEN, new Color(17,25,25));
    	hcolor.put(C_FOREGROUND, Color.white.darker());
    	hcolor.put(C_PLUS, Color.green);
    	hcolor.put(C_MINUS, Color.red);
    	hcolor.put(C_ZERO, Color.yellow);
    	hcolor.put(C_BUY, Color.red);
    	hcolor.put(C_SELL, Color.green.darker());
    	hcolor.put(C_OTHER, Color.white.darker());
    	hcolor.put(C_FONT, new Font("dialog", Font.BOLD, 11));
    	hcolor.put(C_POPUP, new Boolean(true));
    	hcolor.put(C_PLAYSOUND, new Boolean(true));
    	//hcolor.put(C_ANNOUNCEMENT,new Boolean(true));
    }
    
    private static String loadCurrent() {
        Object obj = Utils.readFile("data/layout/tradingcurrent.dat");
        return (obj!=null) ? obj.toString() : "_tradingdefault";
    }
    
    public static void saveLayout(String name){
    	C_CURRENT_LAYOUT = name;
    	save();
    }
    
    public static  void save(){         
    	hconfig.put("COLOR", hcolor);
    	hconfig.put("LAYOUT", hlayout);
        Utils.writeFile("data/layout/"+C_CURRENT_LAYOUT+".dat",hconfig);               
        Utils.writeFile("data/layout/tradingcurrent.dat", C_CURRENT_LAYOUT);
    }
    
    public static void loadLayout(String name){
    	C_CURRENT_LAYOUT = name;
    	load();
    }
    
    public static void load(){
    	Object obj = Utils.readFile("data/layout/"+C_CURRENT_LAYOUT+".dat");
    	if (obj == null){
    		C_CURRENT_LAYOUT = "_tradingdefault";
    		obj = Utils.readFile("data/layout/_tradingdefault.dat");
    	}
    	
    	if (obj!=null){
    		hconfig = (Hashtable)obj;
    		hcolor = (Hashtable)hconfig.get("COLOR");
    		if (hcolor == null) defaultColor();
    		hlayout = (Hashtable)hconfig.get("LAYOUT");
    		if (hlayout == null) hlayout =new Hashtable();
    	}
    }
    
    public static void putLayout(String key, Object object){
        hlayout.put(key, object);
    }
    
    public static void removeLayout(String key){
    	hlayout.remove(key);
    }
    
    public static Object getLayout(String key){
        return hlayout.get(key);
    }
    
    public static void putColor(String key, Color object){
        hcolor.put(key, object);
    }
    
    public static Object  getSetting(String key){
        return hcolor.get(key);
    }
    
    public static void putSetting(String key, Object object){
        hcolor.put(key, object);
    }
    
    public static Color  getColor(String key){
        return (Color)hcolor.get(key);
    }

    public static void putFont(Font f){
    	hcolor.put(C_FONT, f);    	
    }
    
    public static Font getFont(){
    	return (Font)hcolor.get(C_FONT);
    }

    public static Hashtable getLayout(){
        return hlayout;
    }
    
    public static Hashtable getColor(){
    	return hcolor;
    }
    
    public static void setPlaySound(boolean sound){
    	hcolor.put(C_PLAYSOUND, new Boolean(sound));
    }
    
    public static boolean getPlaySound(){
    	Object o = hcolor.get(C_PLAYSOUND);
    	if (o==null){
    		return true;
    	} else {
    		return ((Boolean)o).booleanValue();
    	}
    }
    
    public static void setPopup(boolean popup){
    	hcolor.put(C_POPUP, new Boolean(popup));
    }
    
    public static boolean getPopup(){
    	Object o = hcolor.get(C_POPUP);
    	if (o==null){
    		return false;
    	} else {
    		return ((Boolean)o).booleanValue();
    	}
    }

	public static boolean getcAnnouncement() {
		Object o = hcolor.get(C_ANNOUNCEMENT);
		if(o==null) {
			return false;
		} else {
			return ((Boolean)o).booleanValue();
		}
		
	}

	public static void setcAnnouncement(boolean cAnnouncement) {
		hcolor.put(C_ANNOUNCEMENT,new Boolean(cAnnouncement));
	}
	private static Properties props;
	private static Properties proplot;
	private static Properties propats;
	public static String C_FIRST_PREOP="first_preop";
	public static String C_END_PREOP="end_preop";
	public static String C_FIRST_SES1="first_ses1";
	public static String C_END_SES1="end_ses1";
	public static String C_END_FRIDAY_SES1="end_ses1_friday";
	public static String C_FIRST_SES2="first_ses2";
	public static String C_FIRST_FRIDAY_SES2="first_ses2_friday";
	public static String C_END_SES2="end_ses2";	
	public static String C_FIRST_POSTTRADING="first_post_trading";
	public static String C_END_POSTTRADING="end_post_trading";
	public static String C_END_PRECLOSING = "end_preclosing";
	
	public static String C_LOTSIZE = "lotsize";
	
	public static String C_OLDVALUE1 = "oldvalue1";
	public static String C_OLDVALUE2 = "oldvalue2";
	public static String C_OLDVALUE3 = "oldvalue3";
	public static String C_OLDVALUE4 = "oldvalue4";
	
	public static String C_VALUEHIGHMAX = "upmax";
	public static String C_VALUEHIGHMID = "upmid";
	public static String C_VALUEHIGHMIN = "upmin";
	public static String C_VALUELOWMAX = "downmax";
	public static String C_VALUELOWMID = "downmid";
	public static String C_VALUELOWMIN = "downmin";
	static {
		hlayout = new Hashtable();
		hconfig = new Hashtable();
		defaultColor();
		C_CURRENT_LAYOUT = loadCurrent();
		//sound.start();
		//System.out.println("trading setting");
		try {
			
			
			File ff = new File("data/config/config-queue.properties");			
			if(!ff.exists())
				ff.createNewFile();
			
			
			props = new Properties();
			props.load(new FileInputStream(ff));
			if(props.isEmpty()) {
				props.put(C_FIRST_PREOP, "08:45:00");
				props.put(C_END_PREOP, "08:55:00");
				
				props.put(C_FIRST_SES1, "09:00:00");
				props.put(C_END_SES1, "12:00:00");
				
				//props.put("first_ses1_friday", "09:00:00");
				props.put(C_END_FRIDAY_SES1, "11:30:00");
				
				props.put(C_FIRST_SES2, "13:30:00");
				props.put(C_END_SES2, "15:49:49");
				
				props.put(C_FIRST_FRIDAY_SES2, "14:00:00");
				
				props.put(C_FIRST_POSTTRADING, "16:05:00");
				props.put(C_END_POSTTRADING, "16:15:00");
				
				props.put(C_END_PRECLOSING, "16:00:00");
				
				props.store(new FileOutputStream(ff),"");				
				
			}
			
			//System.out.println("preop "+props.getProperty(C_FIRST_PREOP));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try { 
			File file = new File("data/config/config-lot.properties");
			if(!file.exists())
				file.createNewFile();				
				proplot = new Properties();
				proplot.load(new FileInputStream(file));
				if(proplot.isEmpty()){
					proplot.put(C_LOTSIZE, "100");
					proplot.put(C_OLDVALUE1, "500");
					proplot.put(C_OLDVALUE2, "500");
					proplot.put(C_OLDVALUE3, "5000");
					proplot.put(C_OLDVALUE4, "50");
					proplot.store(new FileOutputStream(file), "");
				}
			
			//System.out.println("proplot "+C_LOTSIZE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try { 
			File file = new File("data/config/config-ats.properties");
			if(!file.exists())
				file.createNewFile();				
				propats = new Properties();
				propats.load(new FileInputStream(file));
				if(propats.isEmpty()){
					propats.put(C_VALUEHIGHMAX, "35");
					propats.put(C_VALUEHIGHMID, "40");
					propats.put(C_VALUEHIGHMIN, "50");
					propats.put(C_VALUELOWMAX, "25");
					propats.put(C_VALUELOWMID, "25");
					propats.put(C_VALUELOWMIN, "25");					
					propats.store(new FileOutputStream(file), "");
				}
			
			//System.out.println("proplot "+C_LOTSIZE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static String getLot(String key){
		return (String)proplot.get(key);
	}
	public static String getats(String key){
		return (String)propats.get(key);
	}
	
	public static String getDateSession(String key){
		return (String) props.get(key);
	}
}
