package eqtrade.trading.core;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;

public class SelectedBorder extends AbstractBorder {
	private static final long serialVersionUID = -708497579988751572L;

	private static final Insets INSETS = new Insets(1, 1, 1, 1);

	@Override
	public Insets getBorderInsets(Component c) {
		return INSETS;
	}

	@Override
	public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
		Color shadow = UIManager.getColor("Button.focus");
		if (shadow == null) shadow = Color.white;
		g.translate(x, y);
		g.setColor(shadow);
		g.fillRect(x, 0, w, 1);
		g.fillRect(0, h-1 , w, 1);
		g.translate(-x, -y);
	}
}
