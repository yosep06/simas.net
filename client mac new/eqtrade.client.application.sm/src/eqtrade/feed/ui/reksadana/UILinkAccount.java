package eqtrade.feed.ui.reksadana;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.util.Hashtable;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.TradeSummaryDef;

public class UILinkAccount extends UI{

	private  JWebBrowser webBrowser;// = new JWebBrowser();
	private String locationUrl = "";
	static WebEngine webEngine;
	public UILinkAccount( String app) {
		super("Link Account", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_LINKACCOUNT);
	}

	@Override
	protected void build() {		
//		JPanel pnlInfo = new JPanel();
		final JFXPanel fxPanel = new JFXPanel();
	    FormLayout l = new FormLayout("100px,2dlu,100px, 2dlu, pref:grow, pref",
			"pref");
//		CellConstraints c = new CellConstraints();
//		PanelBuilder b = new PanelBuilder(l, pnlInfo);
//		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlContent = new JSkinPnl(new BorderLayout());
//		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
//		pnlContent.add(pnlInfo, BorderLayout.NORTH);
		pnlContent.add(fxPanel, BorderLayout.CENTER);
//		jalan();
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {initFX(fxPanel);}});
		
	}
	
	private static void initFX(final JFXPanel fxPanel){
		Group group = new Group();
		Scene scene = new Scene(group);
		fxPanel.setScene(scene);

		BorderPane root = new BorderPane(); 
		WebView webView = new WebView();
//		root.getChildren().add(webView);
		root.setCenter(webView);
		scene.setRoot(root);
		webEngine = webView.getEngine();
		webEngine.load("http://rol.sinarmas-am.co.id/loginv2.asp?userid=IDEY");
	}
	
	private void jalan() {
		//	UIUtils.setPreferredLookAndFeel();
//		    NativeInterface.open();
		    webBrowser = new JWebBrowser();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
				    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
				    webBrowser.setBarsVisible(false);
				    webBrowser.setStatusBarVisible(false);
				    //locationUrl="simas-rol.dev/loginv2.asp?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    locationUrl="http://rol.sinarmas-am.co.id/loginv2.asp?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
//				    locationUrl="http://122.214.186.133:8580/IndonesiaStockExchange";
				    webBrowser.navigate(locationUrl);
				    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
				    pnlContent.add(webBrowser, BorderLayout.CENTER);
					//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		      }
		    });
		}
	
	@Override
	public void focus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadSetting() {
		// TODO Auto-generated method stub
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 950, 750));
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
			
	}
	
	public void show(){
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {webEngine.reload();}
		});
		/*SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				 NativeInterface.open();
				 locationUrl="rol.sinarmas-am.co.id/loginv2.asp?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				 webBrowser.navigate(locationUrl);	
			}
		});*/
	   	   
		super.show();		
	}

}
