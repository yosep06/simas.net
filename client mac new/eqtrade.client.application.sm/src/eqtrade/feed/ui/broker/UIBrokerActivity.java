package eqtrade.feed.ui.broker;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;
import eqtrade.feed.model.TradeSummary;
import eqtrade.feed.model.TradeSummaryDef;
import eqtrade.feed.ui.stock.FilterTradeSummary;

public class UIBrokerActivity extends UI {
	private JGrid table;
	private JGrid tableTrade;
	private JGrid tableTradesell;
	private JPanel pnlInfo;

	private FilterStockSummary filter;
	private FilterTradeSummary filterTrade;
	private JTextField fieldStock;
	private JLabel fieldName;
	private String oldStock = "";

	public UIBrokerActivity(String app) {
		super("Broker Activity By Stock", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_BROKERACTIVITY);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem mnStockSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println("Mouse Position : "+tableTrade.getMousePosition());
				int i=0;
				if(tableTrade.getMousePosition()==null)
				{
					i = tableTradesell.getMappedRow(tableTradesell.getSelectedRow());
					
				}else
				{
					i = tableTrade.getMappedRow(tableTrade.getSelectedRow());	
				}
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMSB)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("BROKER", order.getBroker());
					apps.getUI().showUI(FeedUI.UI_STOCKSUMMARY, param);
				}
			}
		});
		mnStockSumm.setText("Buy/Sell Stock Summary");
		popupMenu.add(mnStockSumm);

		JMenuItem mnStockActivity = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i =0;
				if(tableTrade.getMousePosition()==null)
				{
					i = tableTradesell.getMappedRow(tableTradesell.getSelectedRow());
					
				}else
				{
					i = tableTrade.getMappedRow(tableTrade.getSelectedRow());	
				}
				if (i > -1) {
					TradeSummary order = (TradeSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TRADESUMSB)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("BROKER", order.getBroker());
					apps.getUI().showUI(FeedUI.UI_STOCKACTIVITY, param);
				}
			}
		});
		mnStockActivity.setText("Stock Activity by Broker");
		popupMenu.add(mnStockActivity);

	}

	@Override
	protected void build() {
		createPopup();
		oldStock = "";
		filter = new FilterStockSummary(pnlContent);
		filterTrade = new FilterTradeSummary(pnlContent);
		((FilterColumn) filterTrade.getFilteredData("stock")).setField("");
		table = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_STOCKSUMMARY), filter,
				(Hashtable) hSetting.get("table"));
		// table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO, StockSummary.CIDX_REMARKS,
				StockSummary.CIDX_BESTBID, StockSummary.CIDX_BESTBIDVOLUME,
				StockSummary.CIDX_BESTOFFER, StockSummary.CIDX_BESTOFFERVOLUME,
				StockSummary.CIDX_BOARD, StockSummary.CIDX_FOREIGNERS,
				StockSummary.CIDX_INDIVIDUALINDEX, StockSummary.CIDX_PREVIOUS,
				StockSummary.CIDX_TRADEDFREQUENCY, StockSummary.CIDX_AVG,
				StockSummary.CIDX_TRADEDVOLUME

		});
		

		tableTrade = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_TRADESUMSB), filterTrade,
				(Hashtable) hSetting.get("tabletrade"));
		tableTradesell = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_TRADESUMSB), filterTrade,
				(Hashtable) hSetting.get("tableTradesell"));
		
		tableTrade.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		tableTradesell.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
	tableTrade.setColumnHide(new int[] {// buat table buy
				Model.CIDX_HEADER,
				Model.CIDX_TYPE, 
				Model.CIDX_SEQNO,
				TradeSummary.CIDX_STOCK,
				TradeSummary.CIDX_BOARD,
				TradeSummary.CIDX_INVESTOR,
				TradeSummary.CIDX_SELLAVG,
				TradeSummary.CIDX_BUYVOL,
				TradeSummary.CIDX_SELLVOL,
				TradeSummary.CIDX_SELLVAL,
				TradeSummary.CIDX_SELLFREQ,
				
				TradeSummary.CIDX_SELLLOT,
				TradeSummary.CIDX_VOLTOT,
				TradeSummary.CIDX_LOTTOT,
				TradeSummary.CIDX_VALTOT,
				TradeSummary.CIDX_FREQTOT,
				TradeSummary.CIDX_VOLNET,
				TradeSummary.CIDX_STOCKNAME,
				});
	
	tableTradesell.setColumnHide(new int[] {// buat table sell
			Model.CIDX_HEADER,
			Model.CIDX_TYPE, 
			Model.CIDX_SEQNO,
			TradeSummary.CIDX_STOCK,
			TradeSummary.CIDX_BOARD,
			TradeSummary.CIDX_INVESTOR,
			TradeSummary.CIDX_BUYAVG,
			TradeSummary.CIDX_BUYVOL,
			TradeSummary.CIDX_SELLVOL,
			TradeSummary.CIDX_BUYVAL,
			TradeSummary.CIDX_BUYFREQ,
			TradeSummary.CIDX_BUYLOT,
			
			TradeSummary.CIDX_VOLTOT,
			TradeSummary.CIDX_LOTTOT,
			TradeSummary.CIDX_VALTOT,
			TradeSummary.CIDX_FREQTOT,
			TradeSummary.CIDX_VOLNET,
			TradeSummary.CIDX_STOCKNAME,
			});
		pnlInfo = new JPanel();
		fieldStock = new JTextField("");
		fieldName = new JLabel("please type a stock code");

		FormLayout l = new FormLayout("50px,2dlu,50px, 2dlu, pref:grow, pref",
				"pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(fieldStock, c.xy(1, 1));
		b.add(fieldName, c.xyw(3, 1, 3));

		JSkinPnl header = new JSkinPnl();
		header.add(b.getPanel(), BorderLayout.NORTH);
		header.add(table, BorderLayout.CENTER);
//		JSkinPnl table_bawah = new JSkinPnl();
//		table_bawah.add(tableTrade, BorderLayout.WEST);
//		table_bawah.add(tableTradesell, BorderLayout.EAST);
		table.setPreferredSize(new Dimension(100, 60));
		tableTrade.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		tableTradesell.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		tableTrade.setPreferredSize(new Dimension((lebar()/2),500));
		tableTradesell.setPreferredSize(new Dimension((lebar()/2),500));
		tableTrade.setMinimumSize(new Dimension((lebar()/2),500));
		tableTradesell.setMinimumSize(new Dimension((lebar()/2),500));
		FormLayout layout2 = new FormLayout("120px:grow,120px:grow",
		"fill:pref:grow"); // , 2dlu, fill:80px:grow");
		PanelBuilder builder2 = new PanelBuilder(layout2);
		builder2.getPanel().setPreferredSize(new Dimension(lebar(), 500));
		builder2.getPanel().setMinimumSize(new Dimension(lebar(),500));
		builder2.add(tableTrade, c.xy(1, 1));
		builder2.add(tableTradesell, c.xy(2, 1));
		JSkinPnl table_bawah = new JSkinPnl();
		table_bawah.add(builder2.getPanel(), BorderLayout.CENTER);
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(header, BorderLayout.NORTH);
		pnlContent.add(table_bawah, BorderLayout.CENTER);
		

		fieldStock.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				fieldStockAction();
			}

			@Override
			public void focusGained(FocusEvent e) {
				fieldStock.selectAll();
			}
		});
		fieldStock.registerKeyboardAction(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				fieldStockAction();
			}
		}, null, KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
				JComponent.WHEN_FOCUSED);
		refresh();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (hSetting.get("stock") != null) {
					oldStock = "";
					fieldStock.setText((String) hSetting.get("stock"));
					fieldStockAction();
				}
			}
		});
	}

	private void fieldStockAction() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				String newStock = fieldStock.getText().trim().toUpperCase();
				if ((newStock.length() > 0 && !oldStock.equals(newStock))) {
					if (oldStock.length() > 0) {
						((IEQTradeApp) apps).getFeedEngine().unsubscribe(
								FeedParser.PARSER_TRADESUMMSB,
								FeedParser.PARSER_TRADESUMMSB + "|" + oldStock);
					}
					fieldStock.setText(newStock);
					oldStock = newStock;
					((FilterColumn) filter.getFilteredData("stock"))
							.setField(newStock);
					filter.fireFilterChanged();
					((FilterColumn) filterTrade.getFilteredData("stock"))
							.setField(newStock);
					filterTrade.fireFilterChanged();
					fieldName.setText(geStockName(newStock));
					form.setTitle("Broker Activity by " + oldStock);
					((IEQTradeApp) apps).getFeedEngine().subscribe(
							FeedParser.PARSER_TRADESUMMSB,
							FeedParser.PARSER_TRADESUMMSB + "|" + oldStock);
				}
				fieldStock.selectAll();
			}
		});
	}

	private String geStockName(String code) {
		String name = "";
		int i = ((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK)
				.getIndexByKey(new Object[] { code });
		if (i > -1) {
			name = ((Stock) ((IEQTradeApp) apps).getFeedEngine()
					.getStore(FeedStore.DATA_STOCK).getDataByIndex(i))
					.getName();
		}
		return name;
	}
public Integer lebar()
{
	GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
	int lebar = (gd.getDisplayMode().getWidth())-20;
	return lebar;
	}
	@Override
	public void loadSetting() {
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", StockSummaryDef.getTableDef());
			hSetting.put("tabletrade", TradeSummaryDef.getTableBrokerAcDef());//
			hSetting.put("tableTradesell", TradeSummaryDef.getTableBrokerAcDefKanan());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, lebar(), 678));
		((Hashtable) hSetting.get("table")).put("header",StockSummaryDef.dataHeader.clone());
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
//		hSetting.put("table", table.getTableProperties());
//		hSetting.put("tabletrade", tableTrade.getTableProperties());
//		hSetting.put("stock", oldStock.toUpperCase());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableTrade.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableTradesell.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		fieldName.setForeground(FeedSetting.getColor(FeedSetting.C_FOREGROUND));
		pnlInfo.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
		tableTrade.setNewFont(FeedSetting.getFont());
		tableTradesell.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
			fieldStock.setText(par.get("STOCK").toString());
			fieldStockAction();
		}
		show();
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public void hide() {
		// if (!oldStock.equals(""))
		// ((IEQTradeApp)apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TRADESUMMSB,
		// FeedParser.PARSER_TRADESUMMSB+"|"+oldStock);
		super.hide();
	}

	@Override
	public void close() {
		if (!oldStock.equals(""))
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(
					FeedParser.PARSER_TRADESUMMSB,
					FeedParser.PARSER_TRADESUMMSB + "|" + oldStock);
		super.close();
	}
}
