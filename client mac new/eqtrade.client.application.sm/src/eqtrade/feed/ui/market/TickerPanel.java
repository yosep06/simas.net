package eqtrade.feed.ui.market;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.IApp;
import com.vollux.model.JTickerItem;
import com.vollux.model.JTickerModel;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JTicker;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Currency;
import eqtrade.feed.model.GlobalIndices;
import eqtrade.feed.model.Indices;

public class TickerPanel extends JSkinPnl {
	private static final long serialVersionUID = 4281954910612355426L;

	private JTicker idxTicker = new JTicker(JTicker.C_HORIZONTAL, Color.black,
			50);
	private JTicker markettTicker = new JTicker(JTicker.C_HORIZONTAL,
			Color.black, 45);
	private JTicker currTicker = new JTicker(JTicker.C_HORIZONTAL, Color.black,
			40);
	private Font font = new Font("ARIAL", 1, 12);
	private Font fontBold = new Font("ARIAL", 1, 12);
	private JTickerModel idxModel;
	private JTickerModel marketModel;
	private JTickerModel currModel;
	private IApp apps;

	public TickerPanel(IApp apps) {
		super();
		this.apps = apps;
		build();
	}

	public void setProperty(String name, Object value) {
	}

	public Object getProperty(String name) {
		return null;
	}

	public void build() {
		idxModel = new JTickerModel();
		TableModelListener idxListener = new IdxProvider();
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_INDICES)
				.addTableModelListener(idxListener);
		idxModel.add(new JTickerItem("0",
				"              Sectoral                ", font, Color.white,
				Color.black));
		idxTicker.setText(idxModel);
		idxTicker.setPreferredSize(new Dimension(10, 15));
		idxListener.tableChanged(null);

		marketModel = new JTickerModel();
		TableModelListener marketListener = new MarketProvider();
		((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_GLOBALINDICES)
				.addTableModelListener(marketListener);
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CURRENCY)
				.addTableModelListener(marketListener);
		markettTicker.setText(marketModel);
		markettTicker.setPreferredSize(new Dimension(10, 15));
		marketListener.tableChanged(null);

		currModel = new JTickerModel();
		TableModelListener currListener = new CurrProvider();
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CURRENCY)
				.addTableModelListener(currListener);
		currModel.add(new JTickerItem("0", " Currency  ", font, Color.white,
				Color.black));
		currTicker.setText(currModel);
		currTicker.setPreferredSize(new Dimension(10, 15));
		currListener.tableChanged(null);

		JSkinPnl pTicker = new JSkinPnl();
		// FormLayout tlayout = new FormLayout("fill:450px:grow, 2dlu, 450px",
		// "pref, 1dlu, pref");
		FormLayout tlayout = new FormLayout("fill:450px:grow, 2dlu, 450px",
				"pref, 1dlu, pref");
		PanelBuilder tbuilder = new PanelBuilder(tlayout, pTicker);
		CellConstraints cc = new CellConstraints();
		tbuilder.add(idxTicker, cc.xyw(1, 1, 3));
		// tbuilder.add(markettTicker, cc.xy(1,3));
		tbuilder.add(markettTicker, cc.xyw(1, 3, 3));
		// tbuilder.add(currTicker, cc.xy(3,3));
		pTicker.setBorder(new EmptyBorder(1, 1, 1, 1));
		pTicker.setOpaque(true);
		pTicker.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		this.setOpaque(true);
		this.setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		add(pTicker, BorderLayout.CENTER);
	}

	private static NumberFormat formatter = new DecimalFormat("#,##0.000");

	private class IdxProvider implements TableModelListener {
		@Override
		public void tableChanged(TableModelEvent e) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					for (int i = 0; i < ((IEQTradeApp) apps).getFeedEngine()
							.getStore(FeedStore.DATA_INDICES).getRowCount(); i++) {
						Indices index = (Indices) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_INDICES)
								.getDataByIndex(i);
						double ncurridx = index.getLast().doubleValue();
						double nprevidx = index.getPrev().doubleValue();
						double change = ncurridx - nprevidx;
						JTickerItem item = (idxModel).getItem(index.getCode());
						if (item == null) {
							item = new JTickerItem(index.getCode(), " "
									+ index.getCode() + ":"
									+ formatter.format(ncurridx) + "(chg:"
									+ formatter.format(change) + ") ", index
									.getCode().equals("COMPOSITE") ? fontBold
									: font, Color.black,
									change > 0 ? Color.green
											: change < 0 ? Color.red
													: Color.white);
							(idxModel).addItem(item);
						} else {
							item.setString(" " + index.getCode() + ":"
									+ formatter.format(ncurridx) + "(chg:"
									+ formatter.format(change) + ") ");
							item.setBackground(Color.black);
							item.setForeground(change > 0 ? Color.green
									: change < 0 ? Color.red : Color.white);
						}
					}
				}
			});
		}
	}

	String codeSpecial = "";

	private class MarketProvider implements TableModelListener {

		public MarketProvider() {

		}

		@Override
		public void tableChanged(TableModelEvent e) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					try {
						synchronized (this) {
							for (int i = 0; i < ((IEQTradeApp) apps)
									.getFeedEngine()
									.getStore(FeedStore.DATA_GLOBALINDICES)
									.getRowCount(); i++) {
								GlobalIndices index = (GlobalIndices) ((IEQTradeApp) apps)
										.getFeedEngine()
										.getStore(FeedStore.DATA_GLOBALINDICES)
										.getDataByIndex(i);
								double ncurridx = index.getLast().doubleValue();
								double nprevidx = index.getPrev().doubleValue();
								double change = ncurridx - nprevidx;
								JTickerItem item = (marketModel).getItem(index
										.getCode());
								if (item == null) {

									for (int j = 0; j < ((IEQTradeApp) apps)
											.getFeedEngine()
											.getStore(FeedStore.DATA_CURRENCY)
											.getRowCount(); j++) {
										Currency curr = (Currency) ((IEQTradeApp) apps)
												.getFeedEngine()
												.getStore(
														FeedStore.DATA_CURRENCY)
												.getDataByIndex(j);
										JTickerItem item3 = (marketModel)
												.getItem(curr.getCode());
										if (item3 != null)
											(marketModel).remove(item3);
										if (j == 0) {
											JTickerItem item2 = (marketModel)
													.getItem(codeSpecial + "1");
											if (item2 != null)
												(marketModel).remove(item2);
										}
									}
									if (i == 0) {
										JTickerItem item2 = (marketModel)
												.getItem(index.getCode() + "1");
										String title = "Curre"
												+ formatter.format(ncurridx)
												+ "(chg:"
												+ formatter.format(index
														.getChange()) + ") ";
										item2 = new JTickerItem(index.getCode()
												+ "1", title, font,
												Color.white, Color.black);
										(marketModel).addItem(item2);
									}
									item = new JTickerItem(index.getCode(), " "
											+ index.getCode() + ":"
											+ formatter.format(ncurridx)
											+ "(chg:"
											+ formatter.format(change) + ") ",
											font, Color.black, Color.black);

									(marketModel).addItem(item);
									if (i == ((IEQTradeApp) apps)
											.getFeedEngine()
											.getStore(
													FeedStore.DATA_GLOBALINDICES)
											.getRowCount() - 1) {
										JTickerItem item2 = (marketModel)
												.getItem(index.getCode() + "1");
										String title = " " + index.getCode()
												+ ":"
												+ formatter.format(ncurridx)
												+ "(chg:"
												+ formatter.format(change)
												+ ") ";
										item2 = new JTickerItem(
												index.getCode() + "1",
												title,
												font,
												Color.black,
												change > 0 ? Color.green
														: change < 0 ? Color.red
																: Color.white);
										(marketModel).addItem(item2);
									}
								} else {
									if (i == 0) {
										JTickerItem item2 = (marketModel)
												.getItem(index.getCode() + "1");
										item2.setString("          World Indices");
									}
									if (i == ((IEQTradeApp) apps)
											.getFeedEngine()
											.getStore(
													FeedStore.DATA_GLOBALINDICES)
											.getRowCount() - 1) {
										JTickerItem item2 = (marketModel)
												.getItem(index.getCode() + "1");
										item2.setString(" " + index.getCode()
												+ ":"
												+ formatter.format(ncurridx)
												+ "(chg:"
												+ formatter.format(change)
												+ ") ");
									}
									item.setString(" " + index.getCode() + ":"
											+ formatter.format(ncurridx)
											+ "(chg:"
											+ formatter.format(change) + ") ");
									item.setBackground(Color.black);
									item.setForeground(change > 0 ? Color.green
											: change < 0 ? Color.red
													: Color.white);
								}
								if (i == ((IEQTradeApp) apps).getFeedEngine()
										.getStore(FeedStore.DATA_GLOBALINDICES)
										.getRowCount() - 1) {

									for (int k = 0; k < ((IEQTradeApp) apps)
											.getFeedEngine()
											.getStore(FeedStore.DATA_CURRENCY)
											.getRowCount(); k++) {
										Currency curr = (Currency) ((IEQTradeApp) apps)
												.getFeedEngine()
												.getStore(
														FeedStore.DATA_CURRENCY)
												.getDataByIndex(k);
										String code = curr.getCode();
										JTickerItem itemcurr = (marketModel)
												.getItem(code);

										if (itemcurr == null) {
											if (k == 0) {
												codeSpecial = code;
												JTickerItem item2 = (marketModel)
														.getItem(code + "1");
												String title = "Curre"
														+ formatter.format(curr
																.getValue())
														+ "(chg:"
														+ formatter.format(curr
																.getChange())
														+ ") ";
												item2 = new JTickerItem(code
														+ "1", title, font,
														Color.white,
														Color.black);
												(marketModel).addItem(item2);

											}
											itemcurr = new JTickerItem(
													code,
													" "
															+ code
															+ ":"
															+ formatter
																	.format(curr
																			.getValue())
															+ "(chg:"
															+ formatter
																	.format(curr
																			.getChange())
															+ ") ",
													font,
													Color.black,
													curr.getChange()
															.doubleValue() > 0 ? Color.green
															: curr.getChange()
																	.doubleValue() < 0 ? Color.red
																	: Color.white);

											(marketModel).addItem(itemcurr);

										} else {
											if (k == 0) {
												JTickerItem item2 = (marketModel)
														.getItem(code + "1");
												item2.setString("               Currency");
											}
											itemcurr.setString(" "
													+ code
													+ ":"
													+ formatter.format(curr
															.getValue())
													+ "(chg:"
													+ formatter.format(curr
															.getChange())
													+ ") ");
											itemcurr.setBackground(Color.black);
											itemcurr.setForeground(curr
													.getChange().doubleValue() > 0 ? Color.green
													: curr.getChange()
															.doubleValue() < 0 ? Color.red
															: Color.white);
										}

									}
								}
							}

							// JTickerItem
							// item1=((JTickerModel)marketModel).getItem("0");
							// if(item1==null){
							// item1=new
							// JTickerItem("0","              World Indices              ",
							// font, Color.white, Color.black);
							// ((JTickerModel)marketModel).addLast(item1);
							// }
						}

					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			});
		}
	}

	private class CurrProvider implements TableModelListener {
		@Override
		public void tableChanged(TableModelEvent e) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					for (int i = 0; i < ((IEQTradeApp) apps).getFeedEngine()
							.getStore(FeedStore.DATA_CURRENCY).getRowCount(); i++) {
						Currency curr = (Currency) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_CURRENCY)
								.getDataByIndex(i);
						JTickerItem item = (currModel).getItem(curr.getCode());
						if (item == null) {
							item = new JTickerItem(
									curr.getCode(),
									" "
											+ curr.getCode()
											+ ":"
											+ formatter.format(curr.getValue())
											+ "(chg:"
											+ formatter.format(curr.getChange())
											+ ") ",
									font,
									Color.black,
									curr.getChange().doubleValue() > 0 ? Color.green
											: curr.getChange().doubleValue() < 0 ? Color.red
													: Color.white);
							(currModel).addItem(item);
						} else {
							item.setString(" " + curr.getCode() + ":"
									+ formatter.format(curr.getValue())
									+ "(chg:"
									+ formatter.format(curr.getChange()) + ") ");
							item.setBackground(Color.black);
							item.setForeground(curr.getChange().doubleValue() > 0 ? Color.green
									: curr.getChange().doubleValue() < 0 ? Color.red
											: Color.white);
						}
					}
				}
			});
		}
	}

}
