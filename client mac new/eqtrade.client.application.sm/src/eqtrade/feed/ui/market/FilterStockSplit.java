package eqtrade.feed.ui.market;

import java.awt.Component;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.CorpAction;

public class FilterStockSplit extends FilterBase{

	public FilterStockSplit(Component parent) {
		super(parent, "filter");
		mapFilter.put("action", new FilterColumn("action", String.class, null, FilterColumn.C_EQUAL));
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			CorpAction dat = (CorpAction) val.getSource();

			 if (((FilterColumn) mapFilter.get("action")).compare(dat.getActiontype())) {
				 avail = true;
			 }
			return avail;
		} catch (Exception ec) {
			return false;
		}
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

}
