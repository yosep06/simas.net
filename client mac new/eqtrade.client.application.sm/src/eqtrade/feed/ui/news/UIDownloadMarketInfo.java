package eqtrade.feed.ui.news;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;


import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinForm;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.DownloadMarketInfo;
import eqtrade.feed.model.DownloadMarketInfoDef;
import eqtrade.feed.model.YearRender;
import eqtrade.trading.core.Utils;

public class UIDownloadMarketInfo extends UI {	
	private GridModel DownloadMarketInfoModel;
	private JGrid tabelDownloadMarketInfo;
	private JPanel panelSearch; 
	private JPanel panelMain;
	private JLabel labelPeriod;
	private JComboBox cmbYearPeriod;
	private JComboBox cmbMonthPeriod;
	private JButton btnView;
	private JButton btnDownloadAll;
	private JButton btnCancel;
	private String fileName;
	private String fileDate;
	private int i = 0;
	private JTable tabel;
	private JSkinForm form;
	private JLabel labelToolTip;
	private JProgressBar downloadprogress;
	private JPanel panelInformation;
	private JFileChooser jfc = null;
	private int isCancel = 0;

	public UIDownloadMarketInfo(String app) {
		super("Download Market Info", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_DOWNLOADMARKETINFO);
		this.title = "Download Market Info";		
	}

	private void saveToTextFile(final String FileDate, final String FileName) {		
		SwingUtilities.invokeLater(new Runnable() {			
			@Override
			public void run() {
				try {
					jfc = new JFileChooser(new File("C:\\"));	
					jfc.addChoosableFileFilter(new TextFilter());									
					jfc.setAcceptAllFileFilterUsed(false);
					jfc.setDialogTitle("Save As");
					jfc.setDialogType(JFileChooser.SAVE_DIALOG);
					jfc.setSelectedFile( new File(FileName+".txt"));
					
					int returnVal = jfc.showSaveDialog(form);
					
					String filename = "";
					
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						File ffile = jfc.getSelectedFile();
						filename = ffile.getAbsolutePath();
						if (!filename.endsWith(".txt")) {
							filename = filename + ".txt";
						}
						
						cmbMonthPeriod.setEnabled(false);
						cmbYearPeriod.setEnabled(false);
						btnDownloadAll.setEnabled(false);
						
						try {
							String result = ((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().getHistory("GDMIBD" + "|" + FileDate);		
							String header = "<date>;<ticker>;<open>;<high>;<low>;<close>;<volume>\r\n";
							String row = "";
							row = header+row;
							String as[] = result.split("\n", -2);
							int aslength = as.length - 1;
							for (int j = 0; j < aslength; j++) {
								String s1 = as[j];
								row = row + s1 + "\r\n";	
							}					
							
							BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename), "utf-8"));
							writer.write(row.replace(";",",").replace(" ", "").trim());
							writer.close();
							
							downloadprogress.setMaximum(1);
							downloadprogress.setValue(1);
							
							Utils.showMessage("Download Successfuly", form);
							
							downloadprogress.setValue(0);
							cmbMonthPeriod.setEnabled(true);
							cmbYearPeriod.setEnabled(true);
							btnDownloadAll.setEnabled(true);
						} catch(Exception e) {
							e.printStackTrace();
						}						
					}					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
	}
	
	private Thread threadsavezip = null;
	
	private void saveToZip() {		
		if(threadsavezip != null) {
			threadsavezip.stop();
			threadsavezip=null;
		}
		
		threadsavezip = new Thread(new Runnable() {
			@Override
			public void run() {				
				try {							
					log.info("run thread savetozip");		
					
					btnDownloadAll.setEnabled(false);
					
					int nconfirm = Utils.showConfirmDialog(form, "Download Market Info","are you sure want to download data " +cmbMonthPeriod.getSelectedItem() + " " + cmbYearPeriod.getSelectedItem() + " ?",Utils.C_YES_DLG);
					
					if (nconfirm == Utils.C_YES_DLG) {
						
						tabel = tabelDownloadMarketInfo.getTable();
						
						int rows = tabelDownloadMarketInfo.getTable().getRowCount();	
						
						if(rows > 0) {
							byte[] buffer = new byte[1024];
							
							String zipname = cmbMonthPeriod.getSelectedItem().toString() + cmbYearPeriod.getSelectedItem().toString() + ".zip";
							String tmpfolder = System.getProperty("java.io.tmpdir");	
							
							jfc = new JFileChooser(new File("C:\\"));	
							jfc.setFileFilter(new ZipFilter());
							//jfc.addChoosableFileFilter(new ZipFilter());
							jfc.setAcceptAllFileFilterUsed(false);
							jfc.setDialogTitle("Save As");
							jfc.setDialogType(JFileChooser.SAVE_DIALOG);
							jfc.setSelectedFile(new File(zipname));
							
							int returnVal = jfc.showSaveDialog(form);
							
							if (returnVal == JFileChooser.APPROVE_OPTION) {
								File ffile = jfc.getSelectedFile();
								zipname = ffile.getAbsolutePath();
								
								if (!zipname.endsWith(".zip")) {
									zipname = zipname + ".zip";
								}
								
								try {
									log.info("download to zip startiong ...");
									
									cmbMonthPeriod.setEnabled(false);
									cmbYearPeriod.setEnabled(false);
									btnView.setEnabled(false);
									btnDownloadAll.setEnabled(false);
									btnCancel.setEnabled(true);
									
									ArrayList<String> fileDate= new ArrayList<String>();
									ArrayList<String> fileName= new ArrayList<String>();
									ArrayList<String> result= new ArrayList<String>();
									ArrayList<String> sourceFile = new ArrayList<String>();	
									
									Vector<BufferedWriter> writer = new Vector<BufferedWriter>();		
									
									FileOutputStream fout = new FileOutputStream(zipname);            
						            ZipOutputStream zout = new ZipOutputStream(fout);						            
						            
						            String FolderName = cmbMonthPeriod.getSelectedItem().toString() + cmbYearPeriod.getSelectedItem().toString();
						            downloadprogress.setMaximum(rows);						            
						            
						            for(int i = 0; i<rows; i++) {					            	
						            	if(isCancel > 0) {
											zout.close();      
							                Utils.showMessage("Download Canceled", form);
							                downloadprogress.setValue(0);				               
							                cmbMonthPeriod.setEnabled(true);
							                cmbYearPeriod.setEnabled(true);
							                btnView.setEnabled(false);
							                btnDownloadAll.setEnabled(true);
							                btnCancel.setEnabled(false);
							                isCancel = 0;
							                return;										
										}
						            	downloadprogress.setValue(i);	
						            	fileDate.add(tabel.getModel().getValueAt(i,0).toString());
										fileName.add(tabel.getModel().getValueAt(i, 1).toString());
										
										try {
											result.add(((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().getHistory("GDMIBD" + "|" + fileDate.get(i)));
											String header = "<date>;<ticker>;<open>;<high>;<low>;<close>;<volume>\r\n";
											String row = "";
											row = header+row;
											String as[] = result.get(i).split("\n", -2);
											int aslength = as.length - 2;
											
											for (int j = 0; j < aslength; j++) {
												String s1 = as[j];
												row = row + s1 + "\r\n";						
											}	
											
											writer.add(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(tmpfolder+"\\"+fileName.get(i)+".txt"), "utf-8")));
											writer.get(i).write(row.replace(";",",").replace(" ", "").trim());
											writer.get(i).close();
											
											sourceFile.add(tmpfolder+"\\"+fileName.get(i)+".txt");			
											
											FileInputStream fin = new FileInputStream(sourceFile.get(i));                 
							                zout.putNextEntry(new ZipEntry(FolderName+"\\"+fileName.get(i)+".txt")); 
											
							                int length;

							                 while((length = fin.read(buffer)) > 0) {
							                    zout.write(buffer, 0, length);
							                 }              
							                  zout.closeEntry();
							                  fin.close();			                  
							                  
							                  File filedelete = new File(tmpfolder);
							                  File[] jing = filedelete.listFiles();
							                  filedelete.listFiles();
							                  for(File f : jing) {
							                	  if(f.getName().equals(fileName.get(i)+".txt")) {
							                		  f.delete();
							                	  }
							                  }
										} catch (Exception e) {
											e.printStackTrace();
										}					            	
						            } // end of try  2nd
						            zout.close();      
					                Utils.showMessage("Download Successfuly", form);
					                downloadprogress.setValue(0);				               
					                cmbMonthPeriod.setEnabled(true);
					                cmbYearPeriod.setEnabled(true);
					                btnView.setEnabled(false);
					                btnDownloadAll.setEnabled(true);
					                btnCancel.setEnabled(false);			            
								} catch(Exception e) {
									e.printStackTrace();
								}							
							} else {
								//  End of if returnval == yes		
								btnDownloadAll.setEnabled(true);
							}
						} else {
							/* else rows == 0 */
							Utils.showMessage("No Data Availabe", form);
						}
					} else {
						/* else nconfirm = NO */
						System.err.println("esle");
						btnDownloadAll.setEnabled(true);
					}
										
				} catch (Exception ex) {
					ex.printStackTrace();
				}										
			}
		});
		threadsavezip.start();
	}
	
	private void getYearPeriodMarketInfo() {
		try {
			final DefaultComboBoxModel model = new DefaultComboBoxModel(YearRender.getYear());			
			cmbYearPeriod = new JComboBox(model);	
			 
			DateFormat dateFormat = new SimpleDateFormat("yyyy");
			String dateNow = dateFormat.format(new Date());			
			cmbYearPeriod.setSelectedItem(dateNow);			
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	private void getAvailableMarketInfo() {		
		SwingUtilities.invokeLater(new Runnable() {			
			@Override
			public void run() {				
				try {
					btnDownloadAll.setEnabled(false);
					
					String transdate = "";
					int temp = cmbMonthPeriod.getSelectedIndex();
					String month = Integer.toString(temp);
					String year = cmbYearPeriod.getSelectedItem().toString();
					
					if(month.equals("0")) {
						month = "";
						transdate = year+"%";
					} else {
						if(month.length() == 1) {
							month = "0"+month;
							transdate = year+"-"+month+"%";
						}
						else {
							transdate = year+"-"+month+"%";
						}
					}
					
					try {						
						String result = ((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().getHistory("GAMI" + "|" + transdate);
						
						if (result.length() > 5) {
							DownloadMarketInfoModel.getDataVector().clear();
							DownloadMarketInfoModel.refresh();
							String as[] = result.split("\n", -2);
							int aslength = as.length - 1;
							Vector vRow = new Vector(100, 5);
							for (int j = 0; j < aslength; j++) {
								String s1 = as[j];
								if (!s1.trim().equals("")) {
									DownloadMarketInfo dmarketinfo = new DownloadMarketInfo(s1);							
									vRow.addElement(DownloadMarketInfoDef.createTableRow(dmarketinfo));
								}
							}
							DownloadMarketInfoModel.addRow(vRow, false, false);
							DownloadMarketInfoModel.refresh();	
							btnDownloadAll.setEnabled(true);
						} else {
							//System.err.println("else gami");
							Utils.showMessage("no data available", form);
							DownloadMarketInfoModel.getDataVector().clear();
							DownloadMarketInfoModel.refresh();
						}
					} catch (Exception ex) {
						ex.printStackTrace();
					}					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
		
	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();		
		JMenuItem downloadtotextfile = new JMenuItem(new AbstractAction() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {		
				
				try {
					if(downloadprogress.getValue() > 0) {
						Utils.showMessage("Wait until other Download Complete", form);
					} else {
						saveToTextFile(fileDate, fileName);	
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		downloadtotextfile.setText("Download To Text File");
		popupMenu.add(downloadtotextfile);
	}
		
	@Override
	protected void build() {		
		createPopup();
		getYearPeriodMarketInfo();
		DateFormat dateFormat = new SimpleDateFormat("MM");
		String dateNow = dateFormat.format(new Date());
		
		
		downloadprogress = new JProgressBar();
		
		panelInformation = new JSkinPnl(new BorderLayout());
		panelInformation.setBorder(new EmptyBorder(0,0,0,0));
		
		panelMain = new JPanel();
		panelMain.setBorder(new EmptyBorder(0,0,0,0));	
		
		panelSearch = new JPanel();
		panelSearch.setBorder(new EmptyBorder(0,0,0,0));	
		
		labelPeriod = new JLabel("Period");		
		cmbMonthPeriod = new JComboBox(new Object[] {"All","January","February","March","April","May","June","July","August","September","October","November","December"});
		btnView = new JButton("View");
		btnDownloadAll = new JButton("Download All");
		btnCancel = new JButton("Cancel");
		cmbMonthPeriod.setSelectedIndex(Integer.parseInt(dateNow));
		btnDownloadAll.setEnabled(false);
		btnCancel.setEnabled(false);
		
		labelToolTip = new JLabel("Double Click To Download File");
		labelToolTip.setFont(new Font("Tahoma", Font.ITALIC,10));
		
		DownloadMarketInfoModel = new GridModel(DownloadMarketInfoDef.getHeader(),false);
		tabelDownloadMarketInfo = createTable(DownloadMarketInfoModel, null, (Hashtable) hSetting.get("tabelDownloadMarketInfo"));
		tabelDownloadMarketInfo.setPreferredSize(new Dimension(0, 600));
		tabelDownloadMarketInfo.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);	
		tabelDownloadMarketInfo.setColumnHide(new int[] {DownloadMarketInfo.CIDX_DOWNLOAD});		
		
		FormLayout layout = new FormLayout("pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref","pref");
		PanelBuilder builder = new PanelBuilder(layout,panelSearch);
		CellConstraints cell = new CellConstraints();
		
		panelInformation.add(labelToolTip, BorderLayout.WEST);
		panelInformation.add(downloadprogress, BorderLayout.EAST);
		
		builder.add(labelPeriod, cell.xy(1, 1));
		builder.add(cmbMonthPeriod, cell.xy(3, 1));
		builder.add(cmbMonthPeriod, cell.xy(5, 1));	
		builder.add(cmbYearPeriod, cell.xy(7, 1));	
		builder.add(btnView, cell.xy(9, 1));	
		builder.add(btnDownloadAll,cell.xy(11, 1));
		builder.add(btnCancel, cell.xy(13, 1));
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(panelSearch, BorderLayout.NORTH);
		pnlContent.add(tabelDownloadMarketInfo, BorderLayout.CENTER);
		pnlContent.add(panelInformation, BorderLayout.SOUTH);
		
		tabelDownloadMarketInfo.getTable().add(popupMenu);
				
		tabelDownloadMarketInfo.getTable().addMouseListener(new MouseAdapter() {			
			@Override
			public void mouseReleased(MouseEvent e) {
								
				if(!e.isPopupTrigger() && e.getClickCount() == 2) {					
					int rowindex = tabelDownloadMarketInfo.getSelectedRow();
					
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
					
					tabel = tabelDownloadMarketInfo.getTable();
					
					fileDate = tabel.getModel().getValueAt(rowindex,0).toString();
					fileName = tabel.getModel().getValueAt(rowindex, 1).toString();	
										
				}				
			}		
		});
		
		btnView.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {	
				if(i == 0) {
					i++;
					getAvailableMarketInfo();
					btnView.setEnabled(false);
				}
			}
		});
		
		btnDownloadAll.addActionListener(new ActionListener() {				
			@Override
			public void actionPerformed(ActionEvent arg0) {	
				try {					
					saveToZip();
				} catch (Exception ex) {
					ex.printStackTrace();
				}											
			}
		});
		
		btnCancel.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {		
				isCancel++;
			}
		});
		
		cmbMonthPeriod.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				i = 0;
				btnView.setEnabled(true);
				btnDownloadAll.setEnabled(false);
				DownloadMarketInfoModel.getDataVector().clear();
				DownloadMarketInfoModel.refresh();
			}
			
		});
		
		cmbYearPeriod.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				i = 0;
				btnView.setEnabled(true);
				btnDownloadAll.setEnabled(false);
				DownloadMarketInfoModel.getDataVector().clear();
				DownloadMarketInfoModel.refresh();
			}
			
		});		
		refresh();		
	}

	@Override
	public void focus() {
		
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
		
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tabelDownloadMarketInfo", DownloadMarketInfoDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 400, 300));
	}

	@Override
	public void refresh() {
		tabelDownloadMarketInfo.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tabelDownloadMarketInfo.setNewFont(FeedSetting.getFont());		
	}

	@Override
	public void saveSetting() {
		hSetting.put("tabelDownloadMarketInfo", tabelDownloadMarketInfo.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}
	
	@Override
	public void close() {
		super.close();		
	}
		
	private class TextFilter extends FileFilter {

		  public boolean accept(File f) {
		    if (f.isDirectory())
		      return true;
		    String s = f.getName();
		    int i = s.lastIndexOf('.');

		    if (i > 0 && i < s.length() - 1)
		      if (s.substring(i + 1).toLowerCase().equals("txt"))
		        return true;

		    return false;
		  }
		
		@Override
		public String getDescription() {
			return "Normal text file (*.txt)";
		}
	}
	
	private class ZipFilter extends FileFilter {

		  public boolean accept(File f) {
		    if (f.isDirectory())
		      return true;
		    String s = f.getName();
		    int i = s.lastIndexOf('.');

		    if (i > 0 && i < s.length() - 1)
		      if (s.substring(i + 1).toLowerCase().equals("zip"))
		        return true;

		    return false;
		  }
		
		@Override
		public String getDescription() {
			return "Compressed file (*.zip)";
		}
	}
}