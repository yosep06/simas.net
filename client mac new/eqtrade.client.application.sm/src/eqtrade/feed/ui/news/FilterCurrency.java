package eqtrade.feed.ui.news;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;

import org.jfree.util.Log;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.Article;
import eqtrade.feed.model.Currency;

public class FilterCurrency extends FilterBase{
	private HashMap mapFilter = new HashMap();

	public FilterCurrency(Component parent) {
		super(parent, "filter");
		mapFilter.put("tipe", new FilterColumn("tipe", String.class, null,
				FilterColumn.C_EQUAL));
		
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public int showDialog() {
		return super.showDialog();
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			Currency dat = (Currency) val.getSource();
			
			//Log.info("test data "+dat.getTipe());
			if (((FilterColumn) mapFilter.get("tipe")).compare(dat.getTipe())
					) {
				avail = true;
			}
			return avail;
		} catch (Exception ec) {
			ec.printStackTrace();
			return false;
		}
	}
}
