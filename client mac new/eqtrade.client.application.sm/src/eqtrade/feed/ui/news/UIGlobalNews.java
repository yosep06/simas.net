package eqtrade.feed.ui.news;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JTextLabel;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Article;
import eqtrade.feed.model.ArticleDef;
import eqtrade.feed.model.CorpAction;
import eqtrade.feed.model.CorpActionDef;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.News;
import eqtrade.feed.model.NewsDef;
import eqtrade.feed.model.RupsDef;
import eqtrade.feed.model.StockNews;
import eqtrade.feed.model.StockNewsDef;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Stock;

public class UIGlobalNews extends UI {//Yang Benar
	private JGrid tableNews;
	private JGrid table;
	private FilterGlobalNews filterNews;
	private JGrid tableStock;
	private JGrid tableDividen;
	private JGrid tableRups;
	private JGrid tableOther; // Untuk Right Issue
	
	/* Start |  Add By Iben RUstandi 18-06-2014 */
	private JGrid tableRight;
	private JGrid tableStockSplit;
	private JGrid tableWarrant;
	
	private FilterRightAction filterRight;
	private FilterStockSplitAction filterStockSplit;
	private FilterWarrantAction filterWarrant;
	
	/* End By Iben RUstandi 18-06-2014 */
	
	private FilterStockNews filterStockNews;
	private FilterStockAction filterDividen;
	private FilterStockAction filterOrther;
	private FilterStockRup filterRups;
	
	
	private JTabbedPane tab;
	private JTabbedPane tabCorpAction;
	private JDropDown comboStock;
	private JPanel pnlStock;
	private JLabel lblInfo;
	private JPanel pnlResult;
	private JPanel pnlCorpAction;
	private JPanel pnlSearch;
	private JButton btnrefresh;
	private int counter = 0;

	private JLabel lblnews;
	private JLabel lblCorp;

	
	public UIGlobalNews(String app) {
		super("News", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_GLOBALNEWS);
		this.title = "News";
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tableNews.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				tableNews.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		
		tab = new JTabbedPane();
		tabCorpAction = new JTabbedPane();
		filterNews = new FilterGlobalNews(pnlContent);
		((FilterColumn) filterNews.getFilteredData("subject")).setField("N");
		filterStockNews  = new  FilterStockNews(pnlContent);
		filterDividen = new FilterStockAction(pnlContent);
		
		filterOrther = new FilterStockAction(pnlContent);
		
		filterRups = new FilterStockRup(pnlContent);
		
		filterRight = new FilterRightAction(pnlContent);
		filterStockSplit = new FilterStockSplitAction(pnlContent);
		filterWarrant = new FilterWarrantAction(pnlContent);
		
		((FilterColumn) filterDividen.getFilteredData("action")).setField("Deviden Tunai");
		((FilterColumn) filterDividen.getFilteredData("action2")).setField("Deviden Stock");
		
		((FilterColumn) filterStockSplit.getFilteredData("action2")).setField("Stock Split");
		((FilterColumn) filterRight.getFilteredData("action")).setField("Right Issue");
		((FilterColumn) filterWarrant.getFilteredData("action3")).setField("Warrant");
		
		
		comboStock = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		comboStock.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(comboStock.getText().equals("")) {
					((FilterColumn) filterStockNews.getFilteredData("newsstock")).setField(null);
					filterStockNews.fireFilterChanged();
					((FilterColumn) filterDividen.getFilteredData("stock")).setField(null);
					filterDividen.fireFilterChanged();
					((FilterColumn) filterOrther.getFilteredData("stock")).setField(null);
					filterOrther.fireFilterChanged();
					((FilterColumn)filterRups.getFilteredData("stock")).setField(null);
					filterRups.fireFilterChanged();
					
					((FilterColumn)filterRight.getFilteredData("stock")).setField(null);
					filterRight.fireFilterChanged();
					
					((FilterColumn)filterStockSplit.getFilteredData("stock")).setField(null);
					filterStockSplit.fireFilterChanged();
					
					((FilterColumn)filterWarrant.getFilteredData("stock")).setField(null);
					filterWarrant.fireFilterChanged();					
				} else {
					((FilterColumn) filterStockNews.getFilteredData("newsstock")).setField(comboStock.getSelectedItem());
					filterStockNews.fireFilterChanged();
					((FilterColumn) filterDividen.getFilteredData("stock")).setField(comboStock.getSelectedItem());
					filterDividen.fireFilterChanged();
					((FilterColumn) filterOrther.getFilteredData("stock")).setField(comboStock.getSelectedItem());
					filterOrther.fireFilterChanged();
					((FilterColumn) filterRups.getFilteredData("stock")).setField(comboStock.getSelectedItem());
					filterRups.fireFilterChanged();
					
					((FilterColumn)filterRight.getFilteredData("stock")).setField(comboStock.getSelectedItem());
					filterRight.fireFilterChanged();
					
					((FilterColumn)filterStockSplit.getFilteredData("stock")).setField(comboStock.getSelectedItem());
					filterStockSplit.fireFilterChanged();
					
					((FilterColumn)filterWarrant.getFilteredData("stock")).setField(comboStock.getSelectedItem());
					filterWarrant.fireFilterChanged();
				}
			}
		});	

		tableNews = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_GLOBALNEWS), filterNews,(Hashtable) hSetting.get("tablenews"));
		tableNews.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableNews.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					int i = tableNews.getMappedRow(tableNews.getSelectedRow());
					if (i > -1) {
						Article order = (Article) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_GLOBALNEWS)
								.getDataByIndex(i);
						HashMap map = new HashMap();
						map.put("content", order.getContent());
						apps.getUI().showUI(FeedUI.UI_DLGNEWS, map);
					}
				}
			}
		});
		
		tableStock = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKNEWS), filterStockNews,(Hashtable) hSetting.get("tabelNewsStock"));
		tableStock.setColumnHide(new int[]{StockNews.CIDX_NEWSID,StockNews.CIDX_NEWSSTOCK});
		tableStock.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableStock.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					int i = tableStock.getMappedRow(tableStock.getSelectedRow());
					if (i > -1) {
						StockNews order = (StockNews) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_STOCKNEWS)
								.getDataByIndex(i);
						HashMap map = new HashMap();
						map.put("content", order.getContent());
						apps.getUI().showUI(FeedUI.UI_DLGSTOCKNEWS, map);
					}
				}
			}
		});
		
		tableDividen = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterDividen,(Hashtable) hSetting.get("tableDividen"));
		tableDividen.setColumnHide(new int[] {CorpAction.CIDX_TRANSACTIONDATE, CorpAction.CIDX_LASTTRANSACTIONDATE});
		tableDividen.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		/*tableOther = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterOrther,(Hashtable) hSetting.get("tableOther"));
		tableOther.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);*/
		
		tableRight = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterRight, (Hashtable) hSetting.get("tableRight"));
		tableRight.setColumnHide(new int[] { CorpAction.CIDX_EXPDATE });
		tableRight.setSortColumn(CorpAction.CIDX_CUMDATE);
		tableRight.setSortAscending(false);
		
		tableStockSplit = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterStockSplit, (Hashtable) hSetting.get("tableStockSplit"));
		tableStockSplit.setColumnHide(new int[] { CorpAction.CIDX_AMOUNT, CorpAction.CIDX_TRANSACTIONDATE, CorpAction.CIDX_LASTTRANSACTIONDATE, CorpAction.CIDX_EXPDATE });
		tableStockSplit.setSortColumn(CorpAction.CIDX_CUMDATE);
		tableStockSplit.setSortAscending(false);
	
		tableWarrant = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_CORPACTION), filterWarrant, (Hashtable) hSetting.get("tableWarrant"));
		tableWarrant.setColumnHide(new int[] { CorpAction.CIDX_RATIO1, CorpAction.CIDX_RATIO2,CorpAction.CIDX_CUMDATE, CorpAction.CIDX_RECORDDATE, CorpAction.CIDX_DISTDATE });
		tableWarrant.setSortColumn( CorpAction.CIDX_TRANSACTIONDATE);
		tableWarrant.setSortAscending(false);
		
		tableRups = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_RUPS), filterRups,(Hashtable) hSetting.get("tableRups"));
		tableRups.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,Model.CIDX_SEQNO });
		tableRups.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	
		tableNews.setColumnHide(new int[] { Model.CIDX_HEADER,Model.CIDX_SEQNO, Model.CIDX_TYPE, Article.CIDX_SUBJECT});
		
		new JPanel();
		
		pnlStock = new JPanel();
		lblInfo = new JLabel("please select a stock code");
		btnrefresh = new JButton("Refresh");
		FormLayout layoutStock = new FormLayout("pref,1dlu,75px,2dlu,180px,170px,80px","pref,2dlu,pref");
		PanelBuilder builderStock = new PanelBuilder(layoutStock,pnlStock);
		pnlStock.setBorder(new EmptyBorder(0, 0, 0, 0));
		CellConstraints cellStock = new CellConstraints();
	
		builderStock.add(new JTextLabel("Stock "),cellStock.xy(1,1));
		builderStock.add(comboStock, cellStock.xy(3,1));
		builderStock.add(lblInfo,cellStock.xy(5,1));
		builderStock.add(btnrefresh,cellStock.xy(7, 1));

		pnlResult = new JPanel();
		FormLayout layoutResult = new FormLayout("pref,360dlu","pref,1dlu,75dlu,pref,2dlu");
		PanelBuilder builderResult = new PanelBuilder(layoutResult,pnlResult);
		pnlResult.setBorder(new EmptyBorder(0,0,0,0));
		CellConstraints cellResult = new  CellConstraints();
		lblnews = new JLabel("News");
		lblnews.setFont(new Font("Tahoma", Font.BOLD,12));
		builderResult.add(lblnews, cellResult.xy(2,1));
		builderResult.add(tableStock , cellResult.xy(2,3));
		
		pnlCorpAction = new JPanel();
		FormLayout layoutCorpAction = new FormLayout("pref,360dlu",
				"pref,1dlu,200dlu,pref");
		PanelBuilder builderCorpAction = new PanelBuilder(layoutCorpAction,pnlCorpAction);
		pnlCorpAction.setBorder(new EmptyBorder(0,0,0,0));
		CellConstraints cellCorpAction = new CellConstraints();
		lblCorp = new JLabel("Corporate Action");
		lblCorp.setFont(new Font("Tahoma", Font.BOLD, 12));
		builderCorpAction.add(lblCorp, cellCorpAction.xy(2, 1));
		
		builderCorpAction.add(tabCorpAction, cellCorpAction.xy(2,3));
		tabCorpAction.addTab("Deviden",tableDividen);
		tabCorpAction.addTab("RUPS & Public Expose",tableRups);
		tabCorpAction.addTab("Right Issue",tableRight);
		tabCorpAction.addTab("Stock Split",tableStockSplit);
		tabCorpAction.addTab("Warrant",tableWarrant);
		 
		pnlSearch= new JSkinPnl();
		pnlSearch.setBorder(new EmptyBorder(2,2,2,2));
		FormLayout layoutSearch = new FormLayout("pref,360dlu","pref,3dlu,pref,3dlu,pref,3dlu");
		PanelBuilder builderSearch = new PanelBuilder(layoutSearch,pnlSearch);
		pnlSearch.setBorder(new EmptyBorder(2,2,2,2));
		CellConstraints cellSearch = new CellConstraints();
		builderSearch.add(pnlStock, cellSearch.xy(2,1));
		builderSearch.add(pnlResult, cellSearch.xy(2,3));
		builderSearch.add(pnlCorpAction, cellSearch.xy(2,5));
		
		comboStock.addItemListener(new ItemListener() {				
			@Override
			public void itemStateChanged(ItemEvent arg0) {					
				String str = (String)comboStock.getSelectedItem();					
				if(comboStock.getText().equals("")){
					lblInfo.setText("please select a stock code");
				} else
					lblInfo.setText(geStockName(str));
			}
		});
		
		comboStock.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {				
				String str = (String)comboStock.getSelectedItem();					
					if(comboStock.getText().equals("")){
						lblInfo.setText("please select a stock code");
					} else
						lblInfo.setText(geStockName(str));
			}
		});
		
		comboStock.addKeyListener(new KeyListener() {			
			@Override
			public void keyTyped(KeyEvent e) {				
				String str = (String)comboStock.getSelectedItem();				
				if(comboStock.getText().equals("")) {
					lblInfo.setText("please select a stock code");
				} else
					lblInfo.setText(geStockName(str));
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				String str = (String)comboStock.getSelectedItem();
				//System.out.print(str);
				if(comboStock.getText().equals("")){
					lblInfo.setText("please select a stock code");
				}else
					lblInfo.setText(geStockName(str));
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				String str = (String)comboStock.getSelectedItem();
				//System.out.print(str);
				if(comboStock.getText().equals("")){
					lblInfo.setText("please select a stock code");
				}else
					lblInfo.setText(geStockName(str));
			}
		});
		btnrefresh.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_CORPACTION, FeedParser.PARSER_CORPACTION);
				((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_IPO, FeedParser.PARSER_IPO);
				((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_RUPS, FeedParser.PARSER_RUPS);			
				((IEQTradeApp)apps).getFeedEngine().subscribe(FeedParser.PARSER_STOCKNEWS, FeedParser.PARSER_STOCKNEWS);				
			}
		});
		
		
		table = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_NEWS), null,(Hashtable) hSetting.get("table"));
		table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,Model.CIDX_SEQNO });
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					int i = table.getMappedRow(table.getSelectedRow());
					if (i > -1) {
						News order = (News) ((IEQTradeApp) apps)
								.getFeedEngine().getStore(FeedStore.DATA_NEWS)
								.getDataByIndex(i);
						HashMap map = new HashMap();
						map.put("content", order.getContent());
						apps.getUI().showUI(FeedUI.UI_DLGNEWS, map);
					}
				}
			}
		});
		
		tab.addTab("News", tableNews);
		tab.addTab("BEI News", table);
		tab.addTab("Search",pnlSearch);
				
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(tab, BorderLayout.CENTER);
		
		refresh();
	}
	
	private String geStockName(String code) {
		String name = "";
		int i = ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK)
				.getIndexByKey(new Object[] { code });
		if (i > -1) {
			name = ((Stock) ((IEQTradeApp) apps).getTradingEngine()
					.getStore(TradingStore.DATA_STOCK).getDataByIndex(i))
					.getName();
		}
		return name;
	}
		
	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			/*hSetting.put("tablenews", ArticleDef.getTableDef());
			hSetting.put("tabelNewsStock", StockNewsDef.getTableDef());
			hSetting.put("tableDividen", CorpActionDef.getTableDef());
			hSetting.put("tableOther", CorpActionDef.getTableDef());
			hSetting.put("tableRups", RupsDef.getTableDef());
			hSetting.put("table", NewsDef.getTableDef());*/
		}
		hSetting.put("tablenews", ArticleDef.getTableDef());
		hSetting.put("tabelNewsStock", StockNewsDef.getTableDef());
		hSetting.put("tableDividen", CorpActionDef.getTableDef());
		hSetting.put("tableRight", CorpActionDef.getTableDef());
		hSetting.put("tableStockSplit", CorpActionDef.getTableDef());
		hSetting.put("tableWarrant", CorpActionDef.getTableDef());
		hSetting.put("tableRups", RupsDef.getTableDef());
		hSetting.put("table", NewsDef.getTableDef());
		
		((Hashtable) hSetting.get("tablenews")).put("header",ArticleDef.dataHeader.clone());
		((Hashtable) hSetting.get("table")).put("header",NewsDef.dataHeader.clone());
		/*((Hashtable) hSetting.get("tableDividen")).put("header",NewsDef.dataHeader.clone());
		((Hashtable) hSetting.get("tableRups")).put("header",NewsDef.dataHeader.clone());
		((Hashtable) hSetting.get("tabelNewsStock")).put("header",NewsDef.dataHeader.clone());*/
		
		hSetting.put("size", new Rectangle(20, 20, 500, 300));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("tablenews", tableNews.getTableProperties());	
		hSetting.put("table", table.getTableProperties());
		hSetting.put("tableDividen", tableDividen.getTableProperties());
		hSetting.put("tableRups", tableRups.getTableProperties());
		hSetting.put("tableRight", tableRight.getTableProperties());
		hSetting.put("tableStockSplit", tableStockSplit.getTableProperties());
		hSetting.put("tableWarrant", tableWarrant.getTableProperties());
		hSetting.put("tabelNewsStock", tableStock.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		tableNews.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableNews.setNewFont(FeedSetting.getFont());
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
		tableStock.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableStock.setNewFont(FeedSetting.getFont());
		
		tableRight.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableRight.setNewFont(FeedSetting.getFont());
		
		tableStockSplit.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableStockSplit.setNewFont(FeedSetting.getFont());
		
		tableWarrant.getViewport().setBackground(FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableWarrant.setNewFont(FeedSetting.getFont());
		
		//tableRumor.setNewFont(FeedSetting.getFont());
		//tableResearch.getViewport().setBackground(
		//		FeedSetting.getColor(FeedSetting.C_BACKGROUND));
	//	tableResearch.setNewFont(FeedSetting.getFont());
	}

	Thread laod = null;
	
	@Override
	public void show() {
		//((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_ARTICLE, FeedParser.PARSER_ARTICLE);
		//((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_STOCKNEWS, FeedParser.PARSER_STOCKNEWS);
		
		if(counter == 0) {	
			counter++;
			if(!form.isVisible()) {
				((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_NEWS, FeedParser.PARSER_NEWS);	
				((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_ARTICLE, FeedParser.PARSER_ARTICLE);
			}
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_CORPACTION, FeedParser.PARSER_CORPACTION);
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_IPO, FeedParser.PARSER_IPO);
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_RUPS, FeedParser.PARSER_RUPS);	
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_STOCKNEWS, FeedParser.PARSER_STOCKNEWS);
		
		}
		super.show();
	}

	@Override
	public void hide() {
		//((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_ARTICLE, FeedParser.PARSER_ARTICLE);
		//((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_NEWS, FeedParser.PARSER_NEWS);
		//((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_STOCKNEWS, FeedParser.PARSER_STOCKNEWS);
		super.hide();
	}
	
	protected void initUI() {
		super.initUI();
		form.pack();
	}

	@Override
	public void close() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_ARTICLE, FeedParser.PARSER_ARTICLE);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_NEWS, FeedParser.PARSER_NEWS);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_CORPACTION, FeedParser.PARSER_CORPACTION);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_RUPS, FeedParser.PARSER_RUPS);
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_STOCKNEWS, FeedParser.PARSER_STOCKNEWS);
		super.close();
	}
}
