package eqtrade.feed.ui.trade;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.RunningTrade;

public class FilterTrade extends FilterBase {
	private HashMap mapFilter = new HashMap();

	public FilterTrade(Component parent) {
		super(parent, "filter");
		mapFilter.put("stock", new FilterColumn("stock", String.class,
				new String(""), FilterColumn.C_EQUAL));
		mapFilter.put("price", new FilterColumn("price", Double.class,
				null,
				FilterColumn.C_EQUAL));
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public int showDialog() {
		return super.showDialog();
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			RunningTrade dat = (RunningTrade) val.getSource();
			if (((FilterColumn) mapFilter.get("stock")).compare(dat.getStock())
					&& ((FilterColumn) mapFilter.get("price")).compare(dat
							.getLast())) {
				avail = true;
			}
			return avail;
		} catch (Exception ec) {
			return false;
		}
	}
}