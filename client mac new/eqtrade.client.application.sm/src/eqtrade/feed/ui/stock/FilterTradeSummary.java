package eqtrade.feed.ui.stock;

import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.TradeSummary;

public class FilterTradeSummary extends FilterBase {
	private HashMap mapFilter = new HashMap();

	public FilterTradeSummary(Component parent) {
		super(parent, "filter");
		mapFilter.put("broker", new FilterColumn("broker", String.class, null,
				FilterColumn.C_EQUAL));
		mapFilter.put("stock", new FilterColumn("stock", String.class, null,
				FilterColumn.C_EQUAL));
		mapFilter.put("board", new FilterColumn("board", String.class, null,
				FilterColumn.C_EQUAL));
		mapFilter.put("investor", new FilterColumn("investor", String.class,
				null, FilterColumn.C_EQUAL));//buat sendiri yg banyak
		mapFilter.put("multistock", new FilterColumn("stock",
				Vector.class, null, FilterColumn.C_MEMBEROF));
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public int showDialog() {
		return super.showDialog();
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			
			TradeSummary dat = (TradeSummary) val.getSource();
			if (((FilterColumn) mapFilter.get("broker")).compare(dat
					.getBroker())
					&& ((FilterColumn) mapFilter.get("stock")).compare(dat
							.getStock())
					&& ((FilterColumn) mapFilter.get("board")).compare(dat
							.getBoard())
					&& ((FilterColumn) mapFilter.get("investor")).compare(dat
							.getInvestor())
					&& ((FilterColumn) mapFilter.get("multistock")).compare(dat
							.getStock()))
					{
				avail = true;
			}

			return avail;
			
		} catch (Exception ec) {
			return false;
			
		}
	}
}