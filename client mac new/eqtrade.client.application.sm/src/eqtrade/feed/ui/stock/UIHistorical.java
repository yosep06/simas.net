package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Hashtable;
import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;

















import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

















import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

















import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

















import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

















import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.TradeSummaryDef;

public class UIHistorical extends UI{
	static Runnable pageloader;
	static Thread pageThread;
	private JWebBrowser webBrowser;
	private JEditorPane contentsArea;
	private String locationUrl = "";
	private JPanel webBrowserPanel;
	private JPanel pnlInfo;
	private JTextField fieldStock;
	private JButton btnMenu;
	private JLabel fieldName;
	private String oldStock = "";
	private WebEngine webEngine;
	protected static final String LS = System.getProperty("line.separator");
	
	public UIHistorical(String app) {
		super("Historical", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_HISTORICAL);
		//super(new BorderLayout());
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			//	fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
	}

	
	@Override
	protected void build() {
		btnMenu = new JButton("Back To Main");
			   
		JPanel pnlInfo = new JPanel();
		final JFXPanel fxpanel = new JFXPanel();
		
		FormLayout l = new FormLayout("5px, pref, 5px",
			"5px, pref, 5px");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		b.add(btnMenu, c.xy(2, 2));
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlInfo, BorderLayout.NORTH);
		pnlContent.add(new JPanel().add(fxpanel), BorderLayout.CENTER);
//		jalan();
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() { initFX(fxpanel);}} );
		
		btnMenu.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Platform.runLater( new Runnable() {
					
					@Override
					public void run() {
				locationUrl="http://www.sinarmassekuritas.co.id/simas.net/research_v2.asp";
			    webEngine.load(locationUrl);
				}
				});
			}
		});	
	}

	Thread threadLoad = null;
	
	private void jalan() {
		//UIUtils.setPreferredLookAndFeel();
	    NativeInterface.open();
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
			    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
			    webBrowser = new JWebBrowser();
			    webBrowser.setBarsVisible(false);
			    webBrowser.setStatusBarVisible(false);
			    locationUrl="http://www.sinarmassekuritas.co.id/simas.net/research_v2.asp";
			    webBrowser.navigate(locationUrl);
			    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
			
	    		pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
	
	  		
	    	  
	      }
	    });
	 //   NativeInterface.runEventPump();
	}
	
	private void initFX(final JFXPanel fxPanel){
		Group group = new Group();
		Scene scene = new Scene(group);
		fxPanel.setScene(scene);
		scene.setFill(Color.RED);
		BorderPane root = new BorderPane(); 
		WebView webView = new WebView();
//		root.getChildren().add(webView);
		root.setCenter(webView);
		scene.setRoot(root);
		webView.getEngine().locationProperty().addListener(new ChangeListener<String>() {
			  @Override public void changed(ObservableValue<? extends String> observableValue, String oldLoc, String newLoc) {
			    // check if the newLoc corresponds to a file you want to be downloadable
			    // and if so trigger some code and dialogs to handle the download.
				  try {
					  if (newLoc.endsWith("pdf")) {
						  Desktop.getDesktop().browse(new URI(newLoc) );
					}	
				    } catch (IOException e) {
				        e.printStackTrace();
				    }catch (URISyntaxException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			  }
			});
		webEngine = webView.getEngine();
		webEngine.load("http://www.sinarmassekuritas.co.id/simas.net/research_v2.asp");
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 1000, 700));
	}

	@Override
	public void saveSetting() {
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
		//	fieldStock.setText(par.get("STOCK").toString());
		//	fieldStockAction();
		}
		show();
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void close() {
		super.close();
	}
}
