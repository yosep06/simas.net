package eqtrade.feed.ui.stock;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.StockComparisonBySector;

public class FilterStockComparisonBySector extends FilterBase{
	
	
	public FilterStockComparisonBySector(Component parent) {
		super(parent, "filter");
		mapFilter.put("sector", new FilterColumn("sector",String.class,
				null,FilterColumn.C_EQUAL));
	}


	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column)model.getValueAt(row, 0);
			StockComparisonBySector dat = (StockComparisonBySector)val.getSource();
			if( ((FilterColumn)mapFilter.get("sector")).compare(dat.getSector().toString())){
				avail = true;
				}//System.out.println(dat.getPer()+" sector "+avail);
			return avail ;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Object getFilteredData(String name) {
		// TODO Auto-generated method stub
		return mapFilter.get(name);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		// TODO Auto-generated method stub
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}
}
