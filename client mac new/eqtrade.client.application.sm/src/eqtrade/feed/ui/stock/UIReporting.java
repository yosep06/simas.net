package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Hashtable;
import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Hashtable;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserNavigationParameters;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.TradeSummaryDef;

public class UIReporting extends UI{
	static Runnable pageloader;
	static Thread pageThread;
	private  JWebBrowser webBrowser;
	private  JEditorPane contentsArea;
	private String locationUrl = "";
	private  JPanel webBrowserPanel;
	private JPanel pnlInfo;
	private JFXPanel pnlPortfolio;
	private JFXPanel pnlTC;
	private JFXPanel pnlTA;
	private JFXPanel pnlStatement;
	//#Valdhy20150507
	private JFXPanel pnlPNL;
	private JTextField fieldStock;
	private JLabel fieldName;
	private String oldStock = "";
	private String entryType = "DEPOSIT";
	private JTabbedPane tab;
	private WebEngine webEngineTC,webEngineTA,webEngineStatement,webEnginePortfolio;
	private JWebBrowser webz;
	private JButton btnPrin;
	
	protected static final String LS = System.getProperty("line.separator");
	
	public UIReporting(String app) {
		super("Reporting", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_REPORTING);
		//super(new BorderLayout());
	    
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			//	fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
	}
	
	
	
	@Override
	protected void build() {
		tab = new JTabbedPane();
		btnPrin = new JButton("PRINT");
		JPanel pnlInfo = new JPanel();
	    FormLayout l = new FormLayout("100px,2dlu,100px, 2dlu, pref:grow, pref",
			"pref");
		CellConstraints c = new CellConstraints();
		//PanelBuilder b = new PanelBuilder(l, pnlInfo);
		//pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		pnlPortfolio = new JFXPanel();
		pnlPortfolio.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlTC = new JFXPanel();
		pnlTC.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlTA = new JFXPanel();
		pnlTA.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlStatement = new JFXPanel();
		pnlStatement.setBorder(new EmptyBorder(5, 5, 5, 5));
		//#Valdhy20150507
		pnlPNL = new JFXPanel();
		pnlPNL.setBorder(new EmptyBorder(5, 5, 5, 5));
		
//		jalan();
//		jalan1();
//		jalan2();
//		jalan3();
		tab.addTab("Portfolio", pnlPortfolio);
		tab.addTab("Trade Confirmation", pnlTC);
		tab.addTab("Trade Activity", pnlTA);
		tab.addTab("Statement", pnlStatement);
		//#Valdhy20150507
		tab.addTab("Profit/Loss", pnlPNL);
		//log.info("entry type "+entryType);
		if(entryType.equals("DEPOSIT")){
			tab.setSelectedIndex(0);
		}else {
			tab.setSelectedIndex(1);
		}
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
//		pnlContent.add(btnPrin, BorderLayout.NORTH);
		pnlContent.add(tab, BorderLayout.CENTER);
		Platform.runLater(new Runnable() {
			
			@Override
			public void run()  {
			initFXPortfolio(pnlPortfolio);
			initFXStatement(pnlStatement);
			initFXTA(pnlTA);
			initFXTC(pnlTC);
			initFXPNPL(pnlPNL);
		}}) ;
		btnPrin.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				webz.print(true);
				
			}
		});
	}

	Thread threadLoad = null;
	
	private void jalan() {
	    NativeInterface.open();
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
			    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
			    webBrowser = new JWebBrowser();
			    webBrowser.setBarsVisible(false);
			    webBrowser.setStatusBarVisible(true);			    
			   webz = new JWebBrowser();
			    //locationUrl="http://trading.simasnet.com/report/nextg.portfolio.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();		
			    locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=1";	
			    webBrowser.navigate(locationUrl);		    
			    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
			    pnlPortfolio.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
	    		
			    
			    
	      }
	    });
	}
	private void jalan1() {
		//	UIUtils.setPreferredLookAndFeel();
		    NativeInterface.open();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
				    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
				    webBrowser = new JWebBrowser();
				    webBrowser.setBarsVisible(false);
				    webBrowser.setStatusBarVisible(true);
				    //locationUrl="http://trading.simasnet.com/report/nextg.tc.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    //locationUrl="http://trading.simasnet.com/rep-dev/nextg.tc.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=2";	
					webBrowser.navigate(locationUrl);
				    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
				    pnlTC.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		    		//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		
		  		
		    	  
		      }
		    });
		}

	private void jalan2() {
		//	UIUtils.setPreferredLookAndFeel();
		    NativeInterface.open();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
				    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
				    webBrowser = new JWebBrowser();
				    webBrowser.setBarsVisible(false);
				    webBrowser.setStatusBarVisible(true);
				    locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=3";	
				    //locationUrl="http://trading.simasnet.com/report/nextg.tradeactivity.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    //locationUrl="http://trading.simasnet.com/rep-dev/nextg.tradeactivity.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    webBrowser.navigate(locationUrl);
				    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
				    pnlTA.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		    		//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		
		  		
		    	  
		      }
		    });
		}

	private void jalan3() {
		//	UIUtils.setPreferredLookAndFeel();
		    NativeInterface.open();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
				    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
				    webBrowser = new JWebBrowser();
				    webBrowser.setBarsVisible(false);
				    webBrowser.setStatusBarVisible(true);
				    locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=4";	
				    //locationUrl="http://trading.simasnet.com/report/nextg.statement.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    //locationUrl="http://trading.simasnet.com/rep-dev/nextg.statement.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    webBrowser.navigate(locationUrl);
				    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
				    pnlStatement.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		    		//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		
		  		
		    	  
		      }
		    });
		}
	
	private void initFXPortfolio(final JFXPanel pnlPortfolio){
		Group group = new Group();
		Scene scene = new Scene(group);
		pnlPortfolio.setScene(scene);
		BorderPane root = new BorderPane(); 
		WebView webView = new WebView();
//		root.getChildren().add(webView);
		root.setCenter(webView);
		scene.setRoot(root);
		webEnginePortfolio = webView.getEngine();
		locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=1";	
		webEnginePortfolio.load(locationUrl);
	}
	
	private void initFXTC(final JFXPanel pnlTC){
		Group group = new Group();
		Scene scene = new Scene(group);
		pnlTC.setScene(scene);
		BorderPane root = new BorderPane(); 
		WebView webView = new WebView();
//		root.getChildren().add(webView);
		root.setCenter(webView);
		scene.setRoot(root);
		webEngineTC = webView.getEngine();
		locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=2";	
		webEngineTC.load(locationUrl);
	}
	
	private void initFXTA(final JFXPanel pnlTA){
		Group group = new Group();
		Scene scene = new Scene(group);
		pnlTA.setScene(scene);

		BorderPane root = new BorderPane(); 
		WebView webView = new WebView();
//		root.getChildren().add(webView);
		root.setCenter(webView);
		scene.setRoot(root);
		webEngineTA = webView.getEngine();
		locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=3";	
		webEngineTA.load(locationUrl);
	}
	private void initFXStatement(final JFXPanel pnlStatement){
		Group group = new Group();
		Scene scene = new Scene(group);
		pnlStatement.setScene(scene);

		BorderPane root = new BorderPane(); 
		WebView webView = new WebView();
//		root.getChildren().add(webView);
		root.setCenter(webView);
		scene.setRoot(root);
		webEngineStatement = webView.getEngine();
		locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=4";	
		webEngineStatement.load(locationUrl);
	}

	private void initFXPNPL(final JFXPanel pnlStatement){
		Group group = new Group();
		Scene scene = new Scene(group);
		pnlStatement.setScene(scene);

		BorderPane root = new BorderPane(); 
		WebView webView = new WebView();
//		root.getChildren().add(webView);
		root.setCenter(webView);
		scene.setRoot(root);
		webEngineStatement = webView.getEngine();
		 locationUrl="http://trading.simasnet.com/report/bypass_reporting.php?userid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId()+"&type=5";	
		webEngineStatement.load(locationUrl);
	}
	
	
	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 950, 650));
	}

	@Override
	public void saveSetting() {
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap p = (HashMap) param;
			entryType = (String) p.get("TYPE");
		//	fieldStock.setText(par.get("STOCK").toString());
		//	fieldStockAction();
		}
		show();
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void close() {
		super.close();
	}
}
