package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Stock;
import eqtrade.feed.model.TradeSummaryDef;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import chrriis.common.UIUtils;
import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

public class UICashManagement extends UI{
	static Runnable pageloader;
	static Thread pageThread;
	private  JWebBrowser webBrowser;
	private  JEditorPane contentsArea;
	private String locationUrl = "";
	private  JPanel webBrowserPanel;
	private JPanel pnlInfo;
	private JFXPanel pnlDeposit;
	private JFXPanel pnlWithdraw; 
	private JTextField fieldStock;
	private JLabel fieldName;
	private String oldStock = "";
	private String entryType = "DEPOSIT";
	private JTabbedPane tab;
	private static WebEngine webEngine;
	private static WebEngine webEngine1;
	
	protected static final String LS = System.getProperty("line.separator");
	
	public UICashManagement(String app) {
		super("Cash Management", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_CASHMANAGEMENT);
		//super(new BorderLayout());
	    
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			//	fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
	}
	
	@Override
	protected void build() {
		tab = new JTabbedPane();
		JPanel pnlInfo = new JPanel();
	    FormLayout l = new FormLayout("100px,2dlu,100px, 2dlu, pref:grow, pref",
			"pref");
		CellConstraints c = new CellConstraints();
		//PanelBuilder b = new PanelBuilder(l, pnlInfo);
		//pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		pnlDeposit = new JFXPanel();
//		pnlDeposit.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlWithdraw = new JFXPanel();
//		pnlWithdraw.setBorder(new EmptyBorder(5, 5, 5, 5));
//		jalan();
//		jalan1();
		tab.addTab("Cash Deposit", pnlDeposit);
		tab.addTab("Cash Withdraw", pnlWithdraw);
		
		/*if(entryType.equals("DEPOSIT")){
			tab.setSelectedIndex(0);
		}else {
			tab.setSelectedIndex(1);
		}*/
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(tab, BorderLayout.CENTER);
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
				initFXDeposit(pnlDeposit);
				initFxWithdraw(pnlWithdraw);};
			
			}
		); 
	}
	Thread threadLoad = null;
	
	private void jalan() {
	//	UIUtils.setPreferredLookAndFeel();
	    NativeInterface.open();
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	    	  	JPanel webBrowserPanel = new JPanel(new BorderLayout());
			    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
			    webBrowser = new JWebBrowser();
			    webBrowser.setBarsVisible(false);
			    webBrowser.setStatusBarVisible(true);
			    locationUrl="http://trading.simasnet.com/cash_management/cash_deposit.php?uid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
			    webBrowser.navigate(locationUrl);
			    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
			    pnlDeposit.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
	    		//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
	      }
	    });
	}
	private void jalan1() {
		//	UIUtils.setPreferredLookAndFeel();
		    NativeInterface.open();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
				    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
				    webBrowser = new JWebBrowser();
				    webBrowser.setBarsVisible(false);
				    webBrowser.setStatusBarVisible(true);
				    locationUrl="http://trading.simasnet.com/cash_management/cash_withdraw.php?uid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId();
				    webBrowser.navigate(locationUrl);
				    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
				    pnlWithdraw.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		    		//pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
		      }
		    });
		}

	private void initFXDeposit(final JFXPanel pnlDeposit){
		Group group = new Group();
		Scene scene = new Scene(group);
		pnlDeposit.setScene(scene);
		scene.setFill(Color.RED);
		BorderPane root = new BorderPane(); 
		WebView webView = new WebView();
//		root.getChildren().add(webView);
		root.setCenter(webView);
		scene.setRoot(root);
		webEngine = webView.getEngine();
		webEngine.load("http://trading.simasnet.com/cash_management/cash_deposit.php?uid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId());
	}
	
	private void initFxWithdraw(final JFXPanel pnlWithdraw){
		Group group = new Group();
		Scene scene = new Scene(group);
		pnlWithdraw.setScene(scene);
		scene.setFill(Color.RED);
		BorderPane root = new BorderPane(); 
		WebView webView = new WebView();
//		root.getChildren().add(webView);
		root.setCenter(webView);
		scene.setRoot(root);
		webEngine1 = webView.getEngine();
		webEngine1.load("http://trading.simasnet.com/cash_management/cash_withdraw.php?uid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId());
		
	}
	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 750, 450));
	}

	@Override
	public void saveSetting() {
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
	}

	@Override
	public void show(Object param) {
		
			HashMap p = (HashMap) param;
			entryType = (String) p.get("TYPE");
		//	fieldStock.setText(par.get("STOCK").toString());
		//	fieldStockAction();
		
		show();
	}

	@Override
	public void show() {
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {
			webEngine.load("http://trading.simasnet.com/cash_management/cash_deposit.php?uid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId());
			webEngine1.load("http://trading.simasnet.com/cash_management/cash_withdraw.php?uid=" + ((IEQTradeApp) apps).getFeedEngine().getUserId());
			}
		}
		);
		
		changeform();
		super.show();
	}
	public void changeform(){
		//log.info("entry type "+entryType);
		if(entryType=="DEPOSIT"){
			tab.setSelectedIndex(0);
		} else {
			tab.setSelectedIndex(1);
		}
	}
	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void close() {
		super.close();
	}

	

}
