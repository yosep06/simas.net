package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.demo.Mapping;
import com.vollux.framework.UI;
import com.vollux.model.FilteredAndSortedTableModel;
import com.vollux.model.TableModelSorterEvent;
import com.vollux.model.TableModelSorterListener;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.IMessage;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Board;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;
import eqtrade.feed.parser.TopGainerParser;
import eqtrade.feed.ui.quote.FilterBoard;
import eqtrade.trading.engine.TradingStore;

public class UITopStock extends UI implements IMessage{
	private JGrid tableAll;
	private JGrid tableReg;
	private JGrid tableNonReg;
	private FilterTopStock filterAll;
	private FilterTopStock filterReg;
	private FilterTopStock filterNonReg;
	private JTabbedPane tab;
	private JPanel pnlboard;
	private JDropDown comboBoard;
	private FilterBoard filterBoard;
	String periode ="";
	public UITopStock(String app) {
		super("Top Stock All Board", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_TOPSTOCK);
		this.title = "Top Stock All Board";
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tableAll.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem mnBrokerSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int j = tab.getSelectedIndex();
				int i = j == 0 ? tableReg.getMappedRow(tableReg
						.getSelectedRow())
						: (j == 1 ? tableNonReg.getMappedRow(tableNonReg
								.getSelectedRow())
								: tableAll.getMappedRow(tableAll
										.getSelectedRow()));
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TOPGAINER)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_BROKERSUMMARY, param);
				}
			}
		});
		mnBrokerSumm.setText("Buy/Sell Broker Summary");
		popupMenu.add(mnBrokerSumm);

		JMenuItem mnBrokerActivity = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int j = tab.getSelectedIndex();
				int i = j == 0 ? tableReg.getMappedRow(tableReg
						.getSelectedRow())
						: (j == 1 ? tableNonReg.getMappedRow(tableNonReg
								.getSelectedRow())
								: tableAll.getMappedRow(tableAll
										.getSelectedRow()));
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TOPGAINER)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_BROKERACTIVITY, param);
				}
			}
		});
		mnBrokerActivity.setText("Broker Activity by Stock");
		popupMenu.add(mnBrokerActivity);

		JMenuItem mnInvStock = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int j = tab.getSelectedIndex();
				int i = j == 0 ? tableReg.getMappedRow(tableReg
						.getSelectedRow())
						: (j == 1 ? tableNonReg.getMappedRow(tableNonReg
								.getSelectedRow())
								: tableAll.getMappedRow(tableAll
										.getSelectedRow()));
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TOPGAINER)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_INVSTOCK, param);
				}
			}
		});
		mnInvStock.setText("Foreign/Domestic by Stock");
		popupMenu.add(mnInvStock);

		JMenuItem mnTimeTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int j = tab.getSelectedIndex();
				int i = j == 0 ? tableReg.getMappedRow(tableReg
						.getSelectedRow())
						: (j == 1 ? tableNonReg.getMappedRow(tableNonReg
								.getSelectedRow())
								: tableAll.getMappedRow(tableAll
										.getSelectedRow()));
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TOPGAINER)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_TIMETRADE, param);
				}
			}
		});
		mnTimeTrade.setText("Time & Trade Summary");
		popupMenu.add(mnTimeTrade);

		JMenuItem mnCompany = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					int j = tab.getSelectedIndex();
					int i = j == 0 ? tableReg.getMappedRow(tableReg
							.getSelectedRow()) : (j == 1 ? tableNonReg
							.getMappedRow(tableNonReg.getSelectedRow())
							: tableAll.getMappedRow(tableAll.getSelectedRow()));
					if (i > -1) {
						StockSummary order = (StockSummary) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_TOPGAINER)
								.getDataByIndex(i);
						HashMap param = new HashMap();
						param.put("STOCK", order.getCode());
						apps.getUI().showUI(FeedUI.UI_COMPANYPROFILE, param);
					}
				} catch (Exception ex) {
					Utils.showMessage("cannot open Browser", null);
				}
			}
		});
		mnCompany.setText("Company Profile");
		popupMenu.add(mnCompany);
		JMenuItem mnIntraday = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int j = tab.getSelectedIndex();
				int i = j == 0 ? tableReg.getMappedRow(tableReg
						.getSelectedRow())
						: (j == 1 ? tableNonReg.getMappedRow(tableNonReg
								.getSelectedRow())
								: tableAll.getMappedRow(tableAll
										.getSelectedRow()));
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TOPGAINER)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_INTRADAY, param);
				}
			}
		});
		mnIntraday.setText("Intraday Chart");
		popupMenu.add(mnIntraday);

		JMenuItem mnTechnical = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int j = tab.getSelectedIndex();
				int i = j == 0 ? tableReg.getMappedRow(tableReg
						.getSelectedRow())
						: (j == 1 ? tableNonReg.getMappedRow(tableNonReg
								.getSelectedRow())
								: tableAll.getMappedRow(tableAll
										.getSelectedRow()));
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TOPGAINER)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_CHART, param);
				}
			}
		});
		mnTechnical.setText("Technical Chart");
		popupMenu.add(mnTechnical);
		JMenuItem mnTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int j = tab.getSelectedIndex();
				int i = j == 0 ? tableReg.getMappedRow(tableReg
						.getSelectedRow())
						: (j == 1 ? tableNonReg.getMappedRow(tableNonReg
								.getSelectedRow())
								: tableAll.getMappedRow(tableAll
										.getSelectedRow()));
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TOPGAINER)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_LIVETRADESTOCK, param);
				}
			}
		});
		mnTrade.setText("Live Trade by Stock");
		popupMenu.add(mnTrade);

		JMenuItem mnDetail = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int j = tab.getSelectedIndex();
				int i = j == 0 ? tableReg.getMappedRow(tableReg
						.getSelectedRow())
						: (j == 1 ? tableNonReg.getMappedRow(tableNonReg
								.getSelectedRow())
								: tableAll.getMappedRow(tableAll
										.getSelectedRow()));
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_TOPGAINER)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_STOCKDETAIL, param);
				}
			}
		});
		mnDetail.setText("Stock Detail");
		popupMenu.add(mnDetail);
		
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		propertiesMenu.setText("Properties");

	}

	private void changeTitle(int sort, boolean asc, String header) {
		String col = StockSummaryDef.dataHeader[sort];
		if (asc) {
			if (form != null)
				form.setTitle("Top Losser (" + col + ") " + header);
		} else {
			if (form != null)
				form.setTitle("Top Gainer (" + col + ") " + header);
		}
	}

	@Override
	protected void build() {
		
		FilteredAndSortedTableModel m = new FilteredAndSortedTableModel(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_PERIODE));
		filterBoard = new FilterBoard(pnlContent);
		m.setFilter(filterBoard);
		comboBoard = new JDropDown(((IEQTradeApp)apps).getFeedEngine().getStore(FeedStore.DATA_PERIODE), Mapping.CIDX_CODE,//17112014 gtc .getStore(TradingStore.DATA_ORDERTYPE), Mapping.CIDX_CODE,
				false);
		comboBoard.setBorder(new EmptyBorder(0, 0, 0, 0));
		comboBoard.setSelectedItem("DAY");
		
		pnlboard = new JPanel();
		FormLayout l = new FormLayout("100px,2dlu,50px","pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlboard);
		pnlboard.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(comboBoard, c.xy(1, 1));
		
		comboBoard.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Board vperiode = (Board) ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_PERIODE).getDataByKey(new Object[]{comboBoard.getText()});
				if (!periode.equals(vperiode.getName())) {
						/*((IEQTradeApp)apps).getFeedEngine().getStore( 
								FeedStore.DATA_TOPGAINER).getDataVector().clear();	
					if (vperiode.getName().equalsIgnoreCase("DAY")) {
						int a = ((IEQTradeApp) apps).getFeedEngine()
								.getStore(FeedStore.DATA_STOCKSUMMARY)
								.getRowCount();

						
						for (int i = 0; i < a; i++) {
							StockSummary st = (StockSummary)((IEQTradeApp) apps).getFeedEngine()
											.getStore(FeedStore.DATA_STOCKSUMMARY).getDataByIndex(i);
					        Vector vrow = new Vector(1);
					        vrow.addElement(StockSummaryDef.createTableRow(st));
					        ((IEQTradeApp)apps).getFeedEngine().getStore( 
									FeedStore.DATA_TOPGAINER).addRow(vrow, false, false);
						}
						
					}else {*/
						((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_TOPGAINER,FeedParser.PARSER_TOPGAINER+ "|" + periode);
						((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_TOPGAINER,FeedParser.PARSER_TOPGAINER+ "|" +vperiode.getName());
//					}
					periode = vperiode.getName();
				}
				
			}
		});
		
		
		createPopup();
		tab = new JTabbedPane();
		tab.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent evt) {
				JTabbedPane pane = (JTabbedPane) evt.getSource();
				int sel = pane.getSelectedIndex();
				int sort = 0;
				boolean asc = false;
				String header = "";
				if (sel == 0) {
					sort = tableReg.getSortColumn();
					asc = tableReg.getSortAscending();
					header = "Regular";
				} else if (sel == 1) {
					sort = tableNonReg.getSortColumn();
					asc = tableNonReg.getSortAscending();
					header = "Non-Regular";
				} else if (sel == 2) {
					sort = tableAll.getSortColumn();
					asc = tableAll.getSortAscending();
					header = "All Board";
				}
				changeTitle(sort, asc, header);
			}
		});
		filterAll = new FilterTopStock(pnlContent);
		((FilterColumn) filterAll.getFilteredData("board")).setField(null);
		filterReg = new FilterTopStock(pnlContent);
		filterNonReg = new FilterTopStock(pnlContent);
		((FilterColumn) filterNonReg.getFilteredData("board")).setField(null);
		Vector vboard = new Vector(2);
		vboard.addElement("NG");
		vboard.addElement("TN");
		((FilterColumn) filterNonReg.getFilteredData("multiboard"))
				.setField(vboard);
		tableAll = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
//						FeedStore.DATA_STOCKSUMMARY), filterAll,
						FeedStore.DATA_TOPGAINER), filterAll,
				(Hashtable) hSetting.get("tableall"));
		// tableAll.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableAll.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		tableAll.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				StockSummary.CIDX_AVG, StockSummary.CIDX_BESTBID,
				StockSummary.CIDX_BESTBIDVOLUME, StockSummary.CIDX_BESTOFFER,
				StockSummary.CIDX_BESTOFFERVOLUME,
				StockSummary.CIDX_FOREIGNERS, StockSummary.CIDX_HIGHEST,
				StockSummary.CIDX_INDIVIDUALINDEX, StockSummary.CIDX_LOWEST,
				StockSummary.CIDX_OPENING, StockSummary.CIDX_PREVIOUS,
				StockSummary.CIDX_REMARKS, StockSummary.CIDX_TRADEDLOT });
		tableAll.setColumnSortedProperties(StockSummaryDef.columnsort2);
		tableAll.getDefaultSorter().addTableModelSorterListener(
				new TableModelSorterListener() {
					@Override
					public void sortOrderChanged(TableModelSorterEvent e) {
						int sort = e.getColumn();
						boolean asc = e.isAscending();
						changeTitle(sort, asc, "All Board");
					}
				});
		tableNonReg = createTable(((IEQTradeApp) apps).getFeedEngine()
//				.getStore(FeedStore.DATA_STOCKSUMMARY), filterNonReg,
				.getStore(FeedStore.DATA_TOPGAINER), filterNonReg,
				(Hashtable) hSetting.get("tablenonreg"));
		// tableNonReg.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableNonReg.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		tableNonReg.setColumnHide(new int[] { Model.CIDX_HEADER,
				Model.CIDX_TYPE, StockSummary.CIDX_AVG,
				StockSummary.CIDX_BESTBID, StockSummary.CIDX_BESTBIDVOLUME,
				StockSummary.CIDX_BESTOFFER, StockSummary.CIDX_BESTOFFERVOLUME,
				StockSummary.CIDX_FOREIGNERS, StockSummary.CIDX_HIGHEST,
				StockSummary.CIDX_INDIVIDUALINDEX, StockSummary.CIDX_LOWEST,
				StockSummary.CIDX_OPENING, StockSummary.CIDX_PREVIOUS,
				StockSummary.CIDX_REMARKS, StockSummary.CIDX_TRADEDLOT });
		tableNonReg.setColumnSortedProperties(StockSummaryDef.columnsort2);
		tableNonReg.getDefaultSorter().addTableModelSorterListener(
				new TableModelSorterListener() {
					@Override
					public void sortOrderChanged(TableModelSorterEvent e) {
						int sort = e.getColumn();
						boolean asc = e.isAscending();
						changeTitle(sort, asc, "Non-Regular");
					}
				});
		tableReg = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
//						FeedStore.DATA_STOCKSUMMARY), filterReg,
						FeedStore.DATA_TOPGAINER), filterReg,
				(Hashtable) hSetting.get("tablereg"));
		// tableReg.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableReg.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		tableReg.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				StockSummary.CIDX_AVG, StockSummary.CIDX_BESTBID,
				StockSummary.CIDX_BESTBIDVOLUME, StockSummary.CIDX_BESTOFFER,
				StockSummary.CIDX_BESTOFFERVOLUME,
				StockSummary.CIDX_FOREIGNERS, StockSummary.CIDX_HIGHEST,
				StockSummary.CIDX_INDIVIDUALINDEX, StockSummary.CIDX_LOWEST,
				StockSummary.CIDX_OPENING, StockSummary.CIDX_PREVIOUS,
				StockSummary.CIDX_REMARKS, StockSummary.CIDX_BOARD,
				StockSummary.CIDX_TRADEDLOT });
		tableReg.setColumnSortedProperties(StockSummaryDef.columnsort2);
		tableReg.getDefaultSorter().addTableModelSorterListener(
				new TableModelSorterListener() {
					@Override
					public void sortOrderChanged(TableModelSorterEvent e) {
						int sort = e.getColumn();
						boolean asc = e.isAscending();
						changeTitle(sort, asc, "Regular");
					}
				});
		tab.addTab("Regular", tableReg);
		tab.addTab("Non-Regular", tableNonReg);
		tab.addTab("All Board", tableAll);
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(true);
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(tab, BorderLayout.CENTER);
		pnlContent.add(pnlboard, BorderLayout.NORTH	);
		tab.setSelectedIndex(1);
		refresh();
	}

	@Override
	public void show() {
		super.show();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				tab.setSelectedIndex(0);
				comboBoard.setSelectedIndex(0);
			}
		});
				((IEQTradeApp) apps).getFeedEngine().getEngine().getParser()
				.get(FeedParser.PARSER_STOCKSUMMARY).addListener(this);
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tableall", StockSummaryDef.getTableDef());
			hSetting.put("tablereg", StockSummaryDef.getTableDef());
			hSetting.put("tablenonreg", StockSummaryDef.getTableDef());
		}		
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 500, 300));
		((Hashtable) hSetting.get("tableall")).put("header",StockSummaryDef.dataHeader.clone());
		((Hashtable) hSetting.get("tablereg")).put("header",StockSummaryDef.dataHeader.clone());
		((Hashtable) hSetting.get("tablenonreg")).put("header",StockSummaryDef.dataHeader.clone());
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("tableall", tableAll.getTableProperties());
		hSetting.put("tablereg", tableReg.getTableProperties());
		hSetting.put("tablenonreg", tableNonReg.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		tableAll.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableAll.setNewFont(FeedSetting.getFont());
		tableReg.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableReg.setNewFont(FeedSetting.getFont());
		tableNonReg.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		tableNonReg.setNewFont(FeedSetting.getFont());
	}

	@Override
	public void newMessage(Model model) {
		// TODO Auto-generated method stub
		StockSummary ss = (StockSummary) model;
		StockSummary tp = (StockSummary)((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_TOPGAINER)
				.getDataByKey(new Object[]{ss.getCode(), ss.getBoard()});
//		 StockSummary ssold = (StockSummary)((IEQTradeApp) apps).getFeedEngine()
//					.getStore(FeedStore.DATA_STOCKSUMMARY)
//					.getDataByKey(new Object[]{ss.getCode(), ss.getBoard()});
        
		 
		if (tp !=null) {
				tp.setClosing(ss.getClosing());
//				tp.setTradedFrequency(tp.getTradedFrequency()-ssold.getTradedFrequency()+ ss.getTradedFrequency());
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_TOPGAINER).refresh();
			}
		if (periode.equalsIgnoreCase("DAY")) {
			Vector vrow = new Vector(1);
	        vrow.addElement(StockSummaryDef.createTableRow(ss));
			((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_TOPGAINER).addRow(vrow, false, false);
		}
	}
}
