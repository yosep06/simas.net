package eqtrade.feed.ui.stock;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PagePanel;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.TradeSummaryDef;
import eqtrade.trading.core.ZoomPane;

public class UIDailyNews extends UI {
	static Runnable pageloader;
	static Thread pageThread;
	private  JWebBrowser webBrowser;
	private  JEditorPane contentsArea;
	private String locationUrl = "";
	private  JPanel webBrowserPanel;
	private JPanel pnlInfo;
	private JTextField fieldStock;
	private JLabel fieldName;
	private String oldStock = "";
	protected static final String LS = System.getProperty("line.separator");
	PagePanel panel;PDFPage page;
	private JProgressBar downloadProgress;
	JButton zoomIn,zoomOut;
	ZoomPane zPane;
	
	public UIDailyNews(String app) {
		super("Daily News", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_PICKOFTHEDAY);
		//super(new BorderLayout());
	    
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			//	fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
	}
	
	@Override
	protected void build() {
		
		JPanel pnlInfo = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		zoomIn = new JButton("Zoom In");
		zoomOut = new JButton("Zoom Out");
		downloadProgress = new JProgressBar();
		pnlInfo.add(downloadProgress);
	    pnlInfo.add(zoomIn);
	    pnlInfo.add(zoomOut);
	    
		panel = new PagePanel();
        panel.useZoomTool(true);
		
	    FormLayout l = new FormLayout("50px,2dlu,50px, 2dlu, pref:grow, pref",
			"pref");
//		CellConstraints c = new CellConstraints();
//		PanelBuilder b = new PanelBuilder(l, pnlInfo);
//		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlInfo, BorderLayout.NORTH);
//		pnlContent.add(new JScrollPane(panel), BorderLayout.CENTER);
//		jalan();
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run()  
			{
				try {
					getAsByteArray(new URL(locationUrl()));
//					downloadPdf(new URL(locationUrl()));
	//				PDFFile pdffile = new PDFFile(bf);
	//
	//		        // show the first page
	//		        PDFPage page = pdffile.getPage(0);
	//		        panel.showPage(page);
			        
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		panel.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					panel.useZoomTool(true);
				}else{	System.out.println("klik kanan");
					panel.setClip(null);
				}
			}
		});
		zoomIn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				zPane.setZoom(zPane.getZoom() + 0.1f);
			}
		});
		zoomOut.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				zPane.setZoom(zPane.getZoom() - 0.1f);
			}
		});
		
	}

	Thread threadLoad = null;
	
	private void jalan() {
		//UIUtils.setPreferredLookAndFeel();
	    NativeInterface.open();
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	    	  java.io.BufferedInputStream bis = null;
	    	
	    	  	JPanel webBrowserPanel = new JPanel(new BorderLayout());
			    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
			    webBrowser = new JWebBrowser();
			    webBrowser.setBarsVisible(false);
			    webBrowser.setStatusBarVisible(true);
			    Date date = new Date(((IEQTradeApp) apps).getFeedEngine()
						.getEngine().getConnection().getTime());
			    Calendar cal=Calendar.getInstance();
	            Calendar cal1=Calendar.getInstance();
	            String nama_url="http://www.sinarmassekuritas.co.id/info/research/";
			    String nama_tahun="Tahun%20" + new SimpleDateFormat("yyyy").format(date);
	            String nama_bulan=new SimpleDateFormat("MM").format(date);
	            Integer nm_bln = Integer.valueOf(nama_bulan);
	            switch (nm_bln) {
				case 1:
					nama_bulan="Jan";
					break;
				case 2:
					nama_bulan="Feb";
					break;
				case 3:
					nama_bulan="Mar";
					break;
				case 4:
					nama_bulan="Apr";
					break;
				case 5:
					nama_bulan="May";
					break;
				case 6:
					nama_bulan="Jun";
					break;
				case 7:
					nama_bulan="Jul";
					break;
				case 8:
					nama_bulan="Aug";
					break;
				case 9:
					nama_bulan="Sep";
					break;
				case 10:
					nama_bulan="Oct";
					break;
				case 11:
					nama_bulan="Nov";
					break;
				case 12:
					nama_bulan="Dec";
					break;
				default:
					break;
				}
	            String nama_folder=new SimpleDateFormat("yyyyMMdd").format(date);
	            String nama_file =new SimpleDateFormat("yyyyMMdd").format(date)+"%20daily%20research.pdf";
	            boolean isexist=exists(nama_url + nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file);
	            int i=-1;     
	            while(!isexist){
	            	//System.out.println("i "+i);
	            	cal1.add(Calendar.DATE, i);
	            	nama_tahun="Tahun%20" + new SimpleDateFormat("yyyy").format(cal1.getTime());
	            	nama_bulan=new SimpleDateFormat("MM").format(cal1.getTime());
	            	  nm_bln = Integer.valueOf(nama_bulan);
			            switch (nm_bln) {
						case 1:
							nama_bulan="Jan";
							break;
						case 2:
							nama_bulan="Feb";
							break;
						case 3:
							nama_bulan="Mar";
							break;
						case 4:
							nama_bulan="Apr";
							break;
						case 5:
							nama_bulan="May";
							break;
						case 6:
							nama_bulan="Jun";
							break;
						case 7:
							nama_bulan="Jul";
							break;
						case 8:
							nama_bulan="Aug";
							break;
						case 9:
							nama_bulan="Sep";
							break;
						case 10:
							nama_bulan="Oct";
							break;
						case 11:
							nama_bulan="Nov";
							break;
						case 12:
							nama_bulan="Dec";
							break;
						default:
							break;
						}
	            	nama_folder=new SimpleDateFormat("yyyyMMdd").format(cal1.getTime());
	            	nama_file=new SimpleDateFormat("yyyyMMdd").format(cal1.getTime())+"%20daily%20research.pdf";
	            	isexist=exists(nama_url+ nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file);
	            	//System.out.println("isi url"+nama_url+ nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file);
	            	//System.out.println("isi flag "+isexist);
	            	
	            }
	            locationUrl="http://www.sinarmassekuritas.co.id/info/research/" + nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file;
			    webBrowser.navigate(locationUrl);
			    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
			
	    		pnlContent.add(new JScrollPane(webBrowser), BorderLayout.CENTER);
	
	  		
	      }  
	      
	    });
	 //   NativeInterface.runEventPump();
	}
	
	public String locationUrl(){
		String locationUrl = new String();
		Date date = new Date(((IEQTradeApp) apps).getFeedEngine()
				.getEngine().getConnection().getTime());
	    Calendar cal=Calendar.getInstance();
        Calendar cal1=Calendar.getInstance();
        String nama_url="http://www.sinarmassekuritas.co.id/info/research/";
	    String nama_tahun="Tahun%20" + new SimpleDateFormat("yyyy").format(date);
	    String nama_bulan=new SimpleDateFormat("MMM").getDateInstance(DateFormat.MEDIUM, Locale.US).format(date).substring(0, 3);
	    System.out.println(nama_bulan+" ddd "+new SimpleDateFormat("MMM").getDateInstance(DateFormat.MEDIUM, Locale.US).format(date).substring(0, 3));
		
        String nama_folder=new SimpleDateFormat("yyyyMMdd").format(date);
        String nama_file=new SimpleDateFormat("yyyyMMdd").format(cal1.getTime())+"%20daily%20research.pdf";
    	boolean isexist=exists(nama_url + nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file);
        int i=-1;     
        while(!isexist){
        	//System.out.println("i "+i);
        	cal1.add(Calendar.DATE, i);
        	nama_tahun="Tahun%20" + new SimpleDateFormat("yyyy").format(cal1.getTime());
        	nama_bulan=new SimpleDateFormat("MMM").getDateInstance(DateFormat.MEDIUM, Locale.US).format(date).substring(0, 3);
          
        	nama_folder=new SimpleDateFormat("yyyyMMdd").format(cal1.getTime());
        	nama_file=new SimpleDateFormat("yyyyMMdd").format(cal1.getTime())+"%20daily%20research.pdf";
        	isexist=exists(nama_url+ nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file);
        	//System.out.println("isi url"+nama_url+ nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file);
        	//System.out.println("isi flag "+isexist);
        
		}	
        
        locationUrl="http://www.sinarmassekuritas.co.id/info/research/" + nama_tahun+"/"+nama_bulan+"/"+nama_folder+"/"+nama_file;
        return locationUrl;
	}
	
	static byte[] array = null;
	public void getAsByteArray(final URL url) throws IOException {
		
		download = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					
					URLConnection connection;
					connection = url.openConnection();
					
				    // Since you get a URLConnection, use it to get the InputStream
				    InputStream in = connection.getInputStream();
				    // Now that the InputStream is open, get the content length
				    int contentLength = connection.getContentLength();
				    // To avoid having to resize the array over and over and over as
				    // bytes are written to the array, provide an accurate estimate of
				    // the ultimate size of the byte array
				    ByteArrayOutputStream tmpOut;
				    if (contentLength != -1) {
				        tmpOut = new ByteArrayOutputStream(contentLength);
				    } else {
				        tmpOut = new ByteArrayOutputStream(16384); // Pick some appropriate size
				    }
				    downloadProgress.setMaximum(contentLength);
				    int ln =0;
				    byte[] buf = new byte[512];
				    while (true) {
				        int len = in.read(buf);
				        if (len == -1) {
				            break;
				        }
				        tmpOut.write(buf, 0, len);
				        ln = ln+len;
				        downloadProgress.setValue(ln);
				    }
				    in.close();
				    tmpOut.close(); // No effect, but good to do anyway to keep the metaphor alive
	
				    array = tmpOut.toByteArray();
	
				    
				    //Lines below used to test if file is corrupt
				    //FileOutputStream fos = new FileOutputStream("C:\\abc.pdf");
				    //fos.write(array);
				    //fos.close();
				    PDFFile pdffile = new PDFFile(ByteBuffer.wrap(array));
					
			        // show the first page
			        PDFPage page = pdffile.getPage(0);
			        panel.setSize(1800, 1600);

//					pnlContent.add(new JScrollPane(panel), BorderLayout.EAST);
			        panel.showPage(page);
			        zPane = new ZoomPane(panel.getImage());
			        pnlContent.add(new JScrollPane(zPane), BorderLayout.CENTER);
			        Thread.sleep(2500);
			        downloadProgress.setValue(0);
			        SwingUtilities.updateComponentTreeUI(pnlContent);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		download.start();
//		return ByteBuffer.wrap(array);
	}
	
	Thread download = null;
	void downloadPdf(final URL url) throws IOException{
		download = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					URLConnection connection = url.openConnection();
				    // Since you get a URLConnection, use it to get the InputStream
				    InputStream in = connection.getInputStream();
				    // Now that the InputStream is open, get the content length
				    int contentLength = connection.getContentLength();

				    // To avoid having to resize the array over and over and over as
				    // bytes are written to the array, provide an accurate estimate of
				    // the ultimate size of the byte array
				    ByteArrayOutputStream tmpOut;
				    if (contentLength != -1) {
				        tmpOut = new ByteArrayOutputStream(contentLength);
				    } else {
				        tmpOut = new ByteArrayOutputStream(16384); // Pick some appropriate size
				    }
				    downloadProgress.setMaximum(contentLength);
				    int ln = 0;
				    byte[] buf = new byte[512];
				    while (true) {
				        int len = in.read(buf);
				        if (len == -1) {
				            break;
				        }
				        tmpOut.write(buf, 0, len);
				        ln =ln +len;
				        downloadProgress.setValue(ln);
				    }
				    in.close();
				    tmpOut.close(); // No effect, but good to do anyway to keep the metaphor alive

				    byte[] array = tmpOut.toByteArray();

				    //Lines below used to test if file is corrupt
				    //FileOutputStream fos = new FileOutputStream("C:\\abc.pdf");
				    //fos.write(array);
				    //fos.close();
				    PDFFile pdffile = new PDFFile(ByteBuffer.wrap(array));

			        // show the first page
			        page = pdffile.getPage(0);
			        panel.showPage(page);
			        downloadProgress.setValue(0);;
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
		download.start();
	}
	
	 public static boolean exists(String URLName){
		    try {
		      HttpURLConnection.setFollowRedirects(false);
		      // note : you may also need
		      //        HttpURLConnection.setInstanceFollowRedirects(false)
		      HttpURLConnection con =
		         (HttpURLConnection) new URL(URLName).openConnection();
		      con.setRequestMethod("HEAD");
		      return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		    }
		    catch (Exception e) {
		       e.printStackTrace();
		       return false;
		    }
		  }

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", TradeSummaryDef.getTableTopBrokerDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 750, 450));
	}

	@Override
	public void saveSetting() {
		hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
		//	fieldStock.setText(par.get("STOCK").toString());
		//	fieldStockAction();
		}
		show();
	}

	@Override
	public void show() {
		super.show();
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void close() {
		super.close();
	}

	class ZoomAction extends AbstractAction {

        double zoomfactor = 1.0;

        public ZoomAction(String name, double factor) {
            super(name);
            zoomfactor = factor;
        }

        public ZoomAction(String name, Icon icon, double factor) {
            super(name, icon);
            zoomfactor = factor;
        }

        public void actionPerformed(ActionEvent evt) {
            doZoom(zoomfactor);
        }
    }
	
	public void doZoom(double factor){
		
	}

}
