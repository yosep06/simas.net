package eqtrade.feed.ui.core;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedStore;

public final class UIDlgBroker extends UI {
	private static final long serialVersionUID = 1720762157008498509L;
	private JButton btnOK = new JButton("OK");
	private JButton btnCancel = new JButton("Cancel");
	private JText txtBroker = new JText(true);

	public final static Integer C_ADD = new Integer(0);
	public final static Integer C_EDIT = new Integer(1);
	public final static int C_OK = 0;
	public final static int C_CANCEL = 1;
	public static int C_RESULT = C_CANCEL;
	private static Integer C_TYPE = C_ADD;
	private JSkinDlg frame;

	public UIDlgBroker(String app) {
		super("Edit/Add Broker", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_DLGBROKER);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				txtBroker.requestFocus();
			}
		});
	}

	@Override
	public void refresh() {
		if (frame != null) {
			frame.updateUI();
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.setResizable(false);
		frame.setContent(pnlContent);
		frame.pack();
		// frame.setLocation(((Rectangle) hSetting.get("size")).x, ((Rectangle)
		// hSetting.get("size")).y);
		frame.setModal(true);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				btnCancel.doClick();
			}
		});
		refresh();
	}

	@Override
	protected void build() {
		initLayout();
		initEvent();
	}

	@Override
	public void setProperty(String id, Object obj) {
		super.setProperty(id, obj);
		if (id.equals("location"))
			frame.setLocation((Point) obj);
		else if (id.equals("type"))
			C_TYPE = (Integer) obj;
		else if (id.equals("broker"))
			txtBroker.setText((String) obj);
	}

	@Override
	public Object getProperty(String id) {
		Object o = super.getProperty(id);
		if (o == null) {
			if (id.equals("broker"))
				return txtBroker.getText().trim().toUpperCase();
			else
				return null;
		} else {
			return o;
		}
	}

	private void initLayout() {
		JSkinPnl pBtn = new JSkinPnl(new FlowLayout(FlowLayout.RIGHT));
		pBtn.add(btnOK);
		pBtn.add(btnCancel);

		pnlContent = new JSkinPnl();
		pnlContent.add(pBtn, BorderLayout.SOUTH);
		FormLayout layout = new FormLayout("150px",
				"pref, 2dlu, pref,2dlu,pref");
		PanelBuilder builder = new PanelBuilder(layout, pnlContent);
		CellConstraints cc = new CellConstraints();
		builder.setDefaultDialogBorder();
		builder.add(new JLabel("Broker:"), cc.xy(1, 1));
		builder.add(txtBroker, cc.xy(1, 3));
		builder.add(pBtn, cc.xy(1, 5));
		registerEvent(txtBroker);
		registerEvent(btnOK);
		registerEvent(btnCancel);
	}

	private void registerEvent(JComponent comp) {
		InputMap inputMap = comp
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		// Add a KeyStroke
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");

		comp.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						btnCancel.doClick();
					}
				});

		comp.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton)
							((JButton) evt.getSource()).doClick();
						else
							btnOK.doClick();
					}
				});
	}

	private void initEvent() {
		btnOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (isValidInput()) {
					C_RESULT = C_OK;
					saveSetting();
					frame.setVisible(false);
				} else {
					JOptionPane.showMessageDialog(frame, "Invalid Broker",
							"alert", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				C_RESULT = C_CANCEL;
				saveSetting();
				frame.setVisible(false);
			}
		});
	}

	private boolean isValidInput() {
		int i = ((IEQTradeApp) apps)
				.getFeedEngine()
				.getStore(FeedStore.DATA_BROKER)
				.getIndexByKey(
						new Object[] { txtBroker.getText().trim().toUpperCase() });
		return i > -1;
	}

	@Override
	public void loadSetting() {
	}

	@Override
	public void saveSetting() {
	}

	@Override
	public void show() {
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}

	@Override
	public void show(Object param) {
		txtBroker.setText("");
		if (((Map) param).get("location") != null)
			setProperty("location", ((Map) param).get("location"));
		if (((Map) param).get("type") != null)
			setProperty("type", ((Map) param).get("type"));
		if (((Map) param).get("broker") != null)
			setProperty("broker", ((Map) param).get("broker"));
		txtBroker.requestFocus();
		frame.setTitle(C_TYPE.equals(C_ADD) ? "Add Broker" : "Edit Broker");
		show();
	}

	@Override
	public void hide() {
		saveSetting();
		frame.setVisible(false);
	}

	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}
}
