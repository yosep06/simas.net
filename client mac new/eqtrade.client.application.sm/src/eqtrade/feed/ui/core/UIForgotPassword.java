package eqtrade.feed.ui.core;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Hashtable;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.model.TradeSummaryDef;

public class UIForgotPassword extends UI {
	static Runnable pageloader;
	static Thread pageThread;
	private  JWebBrowser webBrowser;
	private  JEditorPane contentsArea;
	private String locationUrl = "";
	private  JPanel webBrowserPanel;
	private JPanel pnlInfo;
	private JTextField fieldStock;
	private JLabel fieldName;
	private String oldStock = "";
	protected static final String LS = System.getProperty("line.separator");
	private WebView webView;
	private static WebEngine webEngine;
	
	public UIForgotPassword(String app) {
		super("Forgot Password", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_FORGOTPASSWORD);
		//super(new BorderLayout());
	    
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			//	fieldStock.requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
	}
	
	protected void initUI() {
		super.initUI();
		form.setResizable(false);
		form.getBtnMax().setVisible(false);
		form.getBtnMin().setVisible(false);
		
		
	}
	
	@Override
	protected void build() {
		
		JPanel pnlInfo = new JPanel();
		final JFXPanel fxpanel = new JFXPanel();
	    FormLayout l = new FormLayout("50px,2dlu,50px, 2dlu, pref:grow, pref",
			"pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlInfo, BorderLayout.NORTH);
		pnlContent.add(fxpanel, BorderLayout.CENTER);
		Platform.runLater( new Runnable() {
			
			@Override
			public void run() { initFX(fxpanel);}});
//		jalan();
	}
	private static void initFX(final JFXPanel fxPanel){
		Group group = new Group();
		Scene scene = new Scene(group);
		fxPanel.setScene(scene);
		
		WebView webView = new WebView();
		group.getChildren().add(webView);
		
		webEngine = webView.getEngine();
//		webEngine.load("http://financial.sinarmassekuritas.co.id/forgot_password.asp");
		webEngine.load("http://financial.sinarmassekuritas.co.id/forgot_password_v2.asp");
//		webEngine.load("http://web.financial/forgot_password_v2.asp");
	}
	Thread threadLoad = null;
	
	private void jalan() {
		//UIUtils.setPreferredLookAndFeel();
	    NativeInterface.open();
	    SwingUtilities.invokeLater(new Runnable() {
	      public void run() {
	    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
			    webBrowserPanel.setBorder(BorderFactory.createTitledBorder(""));
			    webBrowser = new JWebBrowser();
			    webBrowser.setBarsVisible(false);
			    webBrowser.setStatusBarVisible(false);
			    locationUrl="http://web.financial/forgot_password_v2.asp";//http://financial.sinarmassekuritas.co.id/forgot_password.asp";
			    webBrowser.navigate(locationUrl);
			    webBrowserPanel.add(webBrowser, BorderLayout.CENTER);
	    		pnlContent.add(webBrowser, BorderLayout.CENTER);
	      }
	    });
	 //   NativeInterface.runEventPump();
	}

	@Override
	public void loadSetting() {
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 665, 370));
	}

	@Override
	public void saveSetting() {
		/*hSetting.put("stock", oldStock.toUpperCase());
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);*/
	}

	@Override
	public void refresh() {
	}

	@Override
	public void show(Object param) {
		if (param != null) {
			HashMap par = (HashMap) param;
		//	fieldStock.setText(par.get("STOCK").toString());
		//	fieldStockAction();
		}
		show();
	}

	@Override
	public void show() {
		Platform.runLater(new Runnable() {
			
			@Override
			public void run() {		
				webEngine.reload();
			}
			});
		super.show();
	}

	@Override
	public void hide() {
		super.close();
	}

	@Override
	public void close() {
		super.close();
	}

	
}
