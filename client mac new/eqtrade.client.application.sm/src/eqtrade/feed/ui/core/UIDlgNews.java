package eqtrade.feed.ui.core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.vollux.framework.UI;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.EQTrade;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.Utils;

public final class UIDlgNews extends UI {
	private static final long serialVersionUID = 1720762157008498509L;
	private JEditorPane txtDisclaimer;
	private JScrollPane scrollDisclaimer;

	private JSkinDlg frame;

	public UIDlgNews(String app) {
		super("Viewer", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_DLGNEWS);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				txtDisclaimer.requestFocus();
			}
		});
	}

	@Override
	public void refresh() {
		if (frame != null) {
			frame.updateUI();
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.setResizable(false);
		frame.setContent(pnlContent);
		frame.pack();
		// frame.setLocation(((Rectangle) hSetting.get("size")).x, ((Rectangle)
		// hSetting.get("size")).y);
		frame.setModal(true);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				saveSetting();
				frame.setVisible(false);
			}
		});
		refresh();
	}

	@Override
	protected void build() {
		initLayout();
		initEvent();
	}

	@Override
	public void setProperty(String id, Object obj) {
		super.setProperty(id, obj);
		if (id.equals("location"))
			frame.setLocation((Point) obj);
		else if (id.equals("content")) {
			String data = (String) obj;
			data = data.replaceAll("//", "<br/>");
			data = data.replaceAll("http:<br/>", "http://");
			StringBuffer strDisclaimer = new StringBuffer();
			strDisclaimer.append("<html><body>");
			strDisclaimer
					.append("<div align='justify' style='font-family: tahoma; font-size:10px'>");
			strDisclaimer.append(data);
			strDisclaimer.append("</div>");
			strDisclaimer.append("</body></html>");
			txtDisclaimer.setText(strDisclaimer.toString());
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					txtDisclaimer.setCaretPosition(0);
				}
			});
		}
	}

	@Override
	public Object getProperty(String id) {
		Object o = super.getProperty(id);
		return o;
	}

	private void initLayout() {
		txtDisclaimer = new JEditorPane();
		scrollDisclaimer = new JScrollPane();
		scrollDisclaimer.setBorder(new EmptyBorder(15, 5, 5, 5));
		txtDisclaimer.setContentType("text/html");
		txtDisclaimer.setEditable(false);
		txtDisclaimer.setAlignmentX(Component.CENTER_ALIGNMENT);
		txtDisclaimer.setAlignmentY(Component.TOP_ALIGNMENT);

		scrollDisclaimer.getViewport().add(txtDisclaimer);
		scrollDisclaimer.setBorder(new LineBorder(Color.gray));
		scrollDisclaimer.setFocusable(false);
		scrollDisclaimer.setPreferredSize(new Dimension(590, 365));
		registerEvent(txtDisclaimer);
		registerEvent(scrollDisclaimer);

		pnlContent = new JSkinPnl();
		pnlContent.setBorder(new EmptyBorder(8, 5, 1, 7));
		pnlContent.add(scrollDisclaimer, BorderLayout.CENTER);
	}

	private void registerEvent(JComponent comp) {
		InputMap inputMap = comp
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");

		comp.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						saveSetting();
						frame.setVisible(false);
					}
				});
	}

	private void initEvent() {
	}

	@Override
	public void loadSetting() {
	}

	@Override
	public void saveSetting() {
	}

	@Override
	public void show() {
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}

	@Override
	public void show(Object param) {
		if (((Map) param).get("content") != null)
			setProperty("content", ((Map) param).get("content"));
		show();
	}

	@Override
	public void hide() {
		saveSetting();
		frame.setVisible(false);
	}

	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}
}
