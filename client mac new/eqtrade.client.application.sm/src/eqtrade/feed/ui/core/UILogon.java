package eqtrade.feed.ui.core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Hashtable;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.web.PopupFeatures;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.IconUIResource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.Event;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;

import chrriis.dj.nativeswing.swtimpl.NativeInterface;
import chrriis.dj.nativeswing.swtimpl.components.JWebBrowser;
import chrriis.dj.nativeswing.swtimpl.components.WebBrowserListener;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.Action;
import com.vollux.framework.UI;
import com.vollux.framework.VolluxApp;
import com.vollux.ui.JSkinPnl;
import com.vollux.framework.VolluxApp;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedApplication;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.IFeedApp;
import eqtrade.feed.core.Utils;

public class UILogon extends UI {
	private JLabel lblUserid;
	private JLabel lblPassword;
	private JLabel lblStatus;
	private JLabel lblforgotPassword;
	private JTextField fieldUserid;
	private JPasswordField fieldPassword;
	private JButton btnOK;
	private JButton btnCancel;
	private JLabel image;
	private JLabel image_iklan;
	private ImageIcon img1;
	public JEditorPane window_pane;
	private JWebBrowser webBrowser;
	private String locationUrl;
	private Rectangle pnlDeposit;
	private static WebEngine webEngine;
	public UILogon(String app) {
		super("Simas.net", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_LOGON);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldUserid.requestFocus();
			}
		});
	}

	@Override
	protected void build() {
//		jalan();
		final JFXPanel fxpanel = new JFXPanel();
		image = new JLabel(new ImageIcon(EQTrade.class.getResource("logon.png")),SwingConstants.LEFT);

		lblUserid = new JLabel("User");
		lblPassword = new JLabel("Password");
		lblStatus = new JLabel("please enter correct user & password");
		lblforgotPassword= new JLabel( "<html><a href='www.detik.com'>Forgot Password</a></html>" , JLabel.CENTER );
		fieldUserid = new JTextField(6);
		fieldPassword = new JPasswordField(10);
		btnOK = new JButton("Login");
		btnCancel = new JButton("Close");

		JPanel panel = new JPanel();
		fieldUserid.requestFocus();
		FormLayout layout = new FormLayout("20px, pref, 10px, 130px, 2dlu, pref, 2dlu, pref","70px, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		
		lblStatus.setForeground(Color.black);
		lblUserid.setForeground(Color.black);
		lblPassword.setForeground(Color.black);
		lblforgotPassword.setForeground(Color.blue);
		JLabel titik1 = new JLabel(":");
		titik1.setForeground(Color.black);
		JLabel titik2 = new JLabel(":");
		titik2.setForeground(Color.black);
		PanelBuilder builder = new PanelBuilder(layout, panel);
		CellConstraints cc = new CellConstraints();
		builder.add(lblStatus, cc.xywh(2, 2, 5, 1));
		builder.add(lblUserid, cc.xy(2, 4));
		builder.add(titik1, cc.xy(3, 4));
		builder.add(fieldUserid, cc.xy(4, 4));
		// builder.add(btnOK, cc.xy(6,4));
		builder.add(lblPassword, cc.xy(2, 6));
		builder.add(titik2, cc.xy(3, 6));
		builder.add(fieldPassword, cc.xy(4, 6));
		builder.add(lblforgotPassword, cc.xy(2, 8));
		// builder.add(btnCancel, cc.xy(6,6));
		
		JPanel p = new JPanel();
		p.setOpaque(false);
		FormLayout lay = new FormLayout("pref", " 87px, pref, 2dlu, pref");
		PanelBuilder b = new PanelBuilder(lay, p);
		b.add(btnOK, cc.xy(1, 2));
		b.add(btnCancel, cc.xy(1, 4));

		panel.setOpaque(false);

		JSkinPnl p2 = new JSkinPnl();
		p2.add(panel, BorderLayout.CENTER);
		p2.add(p, BorderLayout.EAST);

		JPanel panel2 = new JPanel();
		FormLayout layout2 = new FormLayout("369px", "pref");
		PanelBuilder builder2 = new PanelBuilder(layout2, panel2);
		builder2.add(p2, cc.xy(1, 1, "l t"));
		builder2.add(image, cc.xy(1, 1));
		// fieldUserid.setNextFocusableComponent(fieldPassword);
		// fieldPassword.setNextFocusableComponent(btnOK);
		// btnOK.setNextFocusableComponent(btnCancel);
		// btnCancel.setNextFocusableComponent(fieldUserid);
		
		lblforgotPassword.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		lblforgotPassword.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				if(evt.getClickCount() > 0){
					//System.out.println("masuk");
					apps.getUI().showUI(FeedUI.UI_FORGOTPASSWORD);
					//feedApp.getUI().showUI(FeedUI.UI_LOGON);
					//close();
					//((IEQTradeApp) apps).getUI().showUI(FeedUI.UI_FORGOTPASSWORD);
					
				}
			}
		});	

		btnOK.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				doLogin();
			}
		}
		);

		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				((IEQTradeApp) apps).getFeedEngine().logout();
				apps.getAction().setState(Action.C_NOTREADY_STATE);
				apps.getUIMain().setProperty("feed_logout", null);
				apps.getUIMain().setProperty("userid", "");
				close();
				apps.getUIMain().show();
				((IEQTradeApp) apps).getFeedEngine().stop();
				((IEQTradeApp) apps).getFeedEngine().logout();
				((IEQTradeApp) apps).getTradingEngine().stop();
				((IEQTradeApp) apps).getTradingEngine().logout();
			}

			
		});		
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(panel2, BorderLayout.NORTH);
		pnlContent.add(fxpanel, BorderLayout.CENTER);
		
		//pnlContent.add(panel_iklan,BorderLayout.SOUTH);
		registerEvent(fieldUserid);
		registerEvent(fieldPassword);
		registerEvent(btnOK);
		registerEvent(btnCancel);
//		doLogin(); // for auto login

		Platform.runLater( new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				initFX(fxpanel);
			}
		}); 
	}
	
	private void jalan() {
		    NativeInterface.open();
		    SwingUtilities.invokeLater(new Runnable() {
		      public void run() {
		    	  JPanel webBrowserPanel = new JPanel(new BorderLayout());
		    	  JScrollBar _horizontalScroll;
		    	  webBrowser = new JWebBrowser();
		    	  webBrowser.setBarsVisible(false);
		    	  webBrowser.setStatusBarVisible(false);
		    	  webBrowser.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		    	  locationUrl="http://www.sinarmassekuritas.co.id/login_images.asp";
		    	  webBrowser.navigate(locationUrl);
		    	  pnlContent.add(webBrowser, BorderLayout.CENTER);
		      }
		    });
		}
	public static final String EVENT_TYPE_CLICK = "click";
	static String newLoc2 = new String();
	
	private static void initFX(final JFXPanel fxPanel){
		Group group = new Group();
		Scene scene = new Scene(group);
		fxPanel.setScene(scene);
		
		final WebView webView = new WebView();
		
		group.getChildren().add(webView);
		webView.getEngine().locationProperty().addListener(new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> arg0,
					final String oldLoc, final String newLoc) {
				// TODO Auto-generated method stub
				System.out.println(""+newLoc+" old "+oldLoc);
				EventListener listener = new EventListener() {
					
					@Override
					public void handleEvent(Event evt) {
						// TODO Auto-generated method stub
						String domEventType = evt.getType();
						if (domEventType.equals(EVENT_TYPE_CLICK)) {
							String href = ((Element)evt.getTarget()).getAttribute("href");
							System.out.println(href+" -- ");
							 if (!newLoc.endsWith("login_images.asp")||!oldLoc.endsWith("login_images.asp") || newLoc2 == null) {
								  if (newLoc!=null) {
									  newLoc2 = newLoc;
								}
								  System.out.println(newLoc+" - "+newLoc2);
								  try {
									Desktop.getDesktop().browse(new URI(newLoc2) );
								} catch (IOException | URISyntaxException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
									
							}
						}
					}
				};
				Document doc = webView.getEngine().getDocument();
                NodeList nodeList = doc.getElementsByTagName("a");//!=null?doc.getElementsByTagName("href"):doc.getElementsByTagName("pdf");
                for (int i = 0; i < nodeList.getLength(); i++) {
                    ((EventTarget) nodeList.item(i)).addEventListener(EVENT_TYPE_CLICK, listener, false);
                    //((EventTarget) nodeList.item(i)).addEventListener(EVENT_TYPE_MOUSEOVER, listener, false);
                    //((EventTarget) nodeList.item(i)).addEventListener(EVENT_TYPE_MOUSEOVER, listener, false);
                }
				  System.out.println(newLoc+" - "+newLoc2);
				  try {
					  if (!newLoc.endsWith("login_images.asp")||!oldLoc.endsWith("login_images.asp") || newLoc2 == null) {
						  if (newLoc!=null) {
							  newLoc2 = newLoc;
						}
//						  System.out.println(newLoc+" - "+newLoc2);
						  Desktop.getDesktop().browse(new URI(newLoc2) );
							
					}
				} catch (IOException | URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		webEngine = webView.getEngine();
		
		webEngine.load("http://www.sinarmassekuritas.co.id/login_images.asp");
	}
	private void doLogin() {
//		fieldUserid.setText("idey"); // for autologin
//		fieldPassword.setText("password"); // for autologin
		 
		if (fieldUserid.getText().trim().equals("")) {
			lblStatus.setText("please enter valid user");
			fieldUserid.requestFocus();
		} else if ((new String(fieldPassword.getPassword()).trim().equals(""))) {
			lblStatus.setText("please enter valid password");
			fieldPassword.requestFocus();
		}else if(((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().getFeedLogin()// yosep other device
				&& ((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().getUserId().equalsIgnoreCase(fieldUserid.getText()) ){
			((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().setCountFeedLogin(1);
			doLoginTrading();
		} else if(((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().getFeedLogin()
				&& !((IEQTradeApp) apps).getFeedEngine().getEngine().getConnection().getUserId().equalsIgnoreCase(fieldUserid.getText()) ){
			((IEQTradeApp) apps).getFeedEngine().logout();
			apps.getUIMain().setProperty("feed_logout", null);
			apps.getUIMain().setProperty("userid", "");
			apps.getUIMain().show();
			setEnableInfo(false);
			lblStatus.setText("feed connecting.....");
			String strPwd = Utils.getMD5(new String(fieldPassword.getPassword()));			
			((IEQTradeApp) apps).getFeedEngine().logout(fieldUserid.getText().toUpperCase(), strPwd,1);
			//yosep other device end
		}  else {
			setEnableInfo(false);
			lblStatus.setText("connecting.....");
			String strPwd = Utils.getMD5(new String(fieldPassword.getPassword()));			
			((IEQTradeApp) apps).getFeedEngine().login(fieldUserid.getText().toUpperCase(), strPwd);
		}
	}

	public void doLoginTrading() {
		if (fieldUserid.getText().trim().equals("")) {
			lblStatus.setText("please enter valid user");
			fieldUserid.requestFocus();
		} else if ((new String(fieldPassword.getPassword()).trim().equals(""))) {
			lblStatus.setText("please enter valid password");
			fieldPassword.requestFocus();
		} else {
			setEnableInfo(false);
			lblStatus.setText("connecting (trading).....");
			String strPwd = Utils.getMD5(new String(fieldPassword.getPassword()));
			((IEQTradeApp) apps).getTradingEngine().login(fieldUserid.getText().toUpperCase(), strPwd);
		}
	}

	private void setEnableInfo(boolean b) {
		fieldUserid.setEnabled(b);
		fieldPassword.setEnabled(b);
		btnOK.setEnabled(b);
	}

	@Override
	protected void initUI() {
		super.initUI();
		form.setResizable(false);
		form.getBtnClose().setVisible(false);
		form.getBtnMin().setVisible(false);
		
		form.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Runtime.getRuntime().halt(0);
				System.exit(-1);
			}
		});
		
	}

	@Override
	public void show() {	
		Platform.runLater( new Runnable() {
			
			@Override
			public void run() { webEngine.reload();}});
		focus();
		Utils.showToCenter(form);
		super.show();
	}

	@Override
	public void loadSetting() {
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 373, 355));
	}

	@Override
	public void saveSetting() {
		Platform.setImplicitExit(false);
		hSetting.put("size",new Rectangle(form.getX(), form.getY(), form.getWidth(), form.getHeight()));
	}

	@Override
	public void refresh() {
		
	}

	public void failed(String reason) {
		((IEQTradeApp) apps).getFeedEngine().logout();
		setStatus(reason);
		setEnableInfo(true);
		fieldUserid.selectAll();
		fieldUserid.requestFocus();
	}

	public void tradingFailed(String reason) {
		int result = Utils.showConfirmDialog(null,"Confirmation"," login to feed server successfully\n but to trading server failed\n do you want to continue? ",0);
		
		if (result == 0) {
			((FeedApplication) apps).logonFeedOnly();
		} else {
			((IEQTradeApp) apps).getFeedEngine().logout();
			setStatus(reason);
			setEnableInfo(true);
			fieldUserid.selectAll();
			fieldUserid.requestFocus();
		}
	}

	public void setStatus(String status) {
		lblStatus.setText(status);
	}

	public void success() {
		saveSetting();
		close();
		apps.getUIMain().show();
	}
	
	// yosep other device
		public void setStatusFalse(String msg){
			setStatus(msg);
			setEnableInfo(true);
			fieldUserid.selectAll();
			fieldUserid.requestFocus();		
		}

	private void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),"enterAction");
		
		comp.getActionMap().put("escapeAction",new AbstractAction("escapeAction") {
			private static final long serialVersionUID = 1L;		
			
			@Override
			public void actionPerformed(ActionEvent evt) {
				btnCancel.doClick();
			}
		});
		comp.getActionMap().put("enterAction",new AbstractAction("enterAction") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent evt) {
				if (evt.getSource() instanceof JButton)
					((JButton) evt.getSource()).doClick();
				else
					btnOK.doClick();
			}
		});
	}
}
