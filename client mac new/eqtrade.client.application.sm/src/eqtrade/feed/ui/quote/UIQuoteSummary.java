package eqtrade.feed.ui.quote;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Model;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.model.StockSummaryDef;

public class UIQuoteSummary extends UI {
	private JGrid table;
	private FilterQuoteSummary filter;

	public UIQuoteSummary(String app) {
		super("Quote Summary", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_QUOTESUMMARY);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem mnBrokerSumm = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_BROKERSUMMARY, param);
				}
			}
		});
		mnBrokerSumm.setText("Buy/Sell Broker Summary");
		popupMenu.add(mnBrokerSumm);

		JMenuItem mnBrokerActivity = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_BROKERACTIVITY, param);
				}
			}
		});
		mnBrokerActivity.setText("Broker Activity by Stock");
		popupMenu.add(mnBrokerActivity);

		JMenuItem mnInvStock = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_INVSTOCK, param);
				}
			}
		});
		mnInvStock.setText("Foreign/Domestic by Stock");
		popupMenu.add(mnInvStock);

		JMenuItem mnTimeTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_TIMETRADE, param);
				}
			}
		});
		mnTimeTrade.setText("Time & Trade Summary");
		popupMenu.add(mnTimeTrade);

		JMenuItem mnCompany = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					int i = table.getMappedRow(table.getSelectedRow());
					if (i > -1) {
						StockSummary order = (StockSummary) ((IEQTradeApp) apps)
								.getFeedEngine()
								.getStore(FeedStore.DATA_STOCKSUMMARY)
								.getDataByIndex(i);
						HashMap param = new HashMap();
						param.put("STOCK", order.getCode());
						apps.getUI().showUI(FeedUI.UI_COMPANYPROFILE, param);
					}
				} catch (Exception ex) {
					Utils.showMessage("cannot open Browser", null);
				}
			}
		});
		mnCompany.setText("Company Profile");
		popupMenu.add(mnCompany);
		JMenuItem mnIntraday = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_INTRADAY, param);
				}
			}
		});
		mnIntraday.setText("Intraday Chart");
		popupMenu.add(mnIntraday);

		JMenuItem mnTechnical = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_CHART, param);
				}
			}
		});
		mnTechnical.setText("Technical Chart");
		popupMenu.add(mnTechnical);
		JMenuItem mnTrade = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = table.getMappedRow(table.getSelectedRow());
				if (i > -1) {
					StockSummary order = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine()
							.getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByIndex(i);
					HashMap param = new HashMap();
					param.put("STOCK", order.getCode());
					apps.getUI().showUI(FeedUI.UI_LIVETRADESTOCK, param);
				}
			}
		});
		mnTrade.setText("Live Trade By Stock");
		popupMenu.add(mnTrade);

		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
	}

	@Override
	protected void build() {
		createPopup();
		filter = new FilterQuoteSummary(pnlContent);
		((FilterColumn) filter.getFilteredData("freq")).setField(new Double(0));
		table = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY), filter,(Hashtable) hSetting.get("table"));
		table.getTable().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		table.setColumnHide(new int[] { Model.CIDX_HEADER, Model.CIDX_TYPE,
				Model.CIDX_SEQNO,
				StockSummary.CIDX_REMARKS,
				StockSummary.CIDX_BOARD, StockSummary.CIDX_FOREIGNERS,
				StockSummary.CIDX_INDIVIDUALINDEX, StockSummary.CIDX_AVG,
				StockSummary.CIDX_TRADEDLOT });
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		refresh();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", StockSummaryDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 400, 300));

		((Hashtable) hSetting.get("table")).put("header",StockSummaryDef.dataHeader.clone()); // JANGAN DIHAPUS KALO GAK MAU PULANG PAGI !!
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				FeedSetting.getColor(FeedSetting.C_BACKGROUND));
		table.setNewFont(FeedSetting.getFont());
	}
}