package eqtrade.feed.ui.quote;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedEventDispatcher;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Queue;
import eqtrade.feed.model.QueueDef;
import eqtrade.feed.model.Stock;

public class UIQuoteQueueOld extends UI {

	private String keyold = "";
	private JDropDown comboType;
	private JDropDown comboStock;
	private JNumber price;
	private JButton btnRefresh;
	private JButton btnClear;
	private JGrid tableQueue;
	private FilterQueue filter;
	private GridModel model;
	private JNumber fieldvarbid;
	private Double oldPrice= new Double(0);
	private Log log = LogFactory.getLog(getClass());
	protected static NumberFormat format2 = new DecimalFormat("###0");

	public UIQuoteQueueOld(String app) {
		super("Quote Queue", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, FeedUI.UI_QUEUE);
		setProperty(C_PROPERTY_NO, no);
	}

	@Override
	protected void build() {

		comboStock = new JDropDown(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK), Stock.CIDX_CODE, true);
		comboStock.setBorder(new EmptyBorder(0, 0, 0, 0));
		price = new JNumber(Double.class, 0, 0, false, true);
		fieldvarbid = new JNumber(Double.class, 0, 0, false, true);
		
		// comboType = new JDropDown(((IEQTradeApp) apps).getFeedEngine()
		// .getStore(FeedStore.DATA_QUEUE_TYPE), Stock.CIDX_CODE, true);
		comboStock.setBorder(new EmptyBorder(0, 0, 0, 0));
		// model = new GridModel(QueueDef.getHeader(), false);
		model = ((IEQTradeApp) apps).getFeedEngine().getStore(
				FeedStore.DATA_QUOTE_QUEUE);
		btnRefresh = new JButton("Refresh");
		filter = new FilterQueue(pnlContent);
		tableQueue = createTable(model, filter,
				(Hashtable) hSetting.get("tablequeue"));
		btnClear = new JButton("Clear");
		btnRefresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// stockcode|board|type|price

				((IEQTradeApp) apps).getFeedEngine()
						.getStore(FeedStore.DATA_QUOTE_QUEUE).getDataVector()
						.clear();

				if (!keyold.isEmpty()) {
					((IEQTradeApp) apps).getFeedEngine().unsubscribe(
							FeedParser.PARSER_QUEUE, keyold);

				}
				//System.out.println("Price--"+price.getValue());
				Double dd = new Double(price.getValue().toString());

				String stock = comboStock.getText().toUpperCase();
				String key = stock + "#RG" + "#0" + "#" + format2.format(dd);
				// log.info("subscribe:" + key);
				//System.out.println("subscribe:" + key);
				keyold = key;

				((IEQTradeApp) apps).getFeedEngine().subscribe(
						FeedParser.PARSER_QUEUE,
						FeedParser.PARSER_QUEUE + "|" + key);

				filter.getFilteredData("stock").setField(stock);
				filter.getFilteredData("price").setField(dd);
				filter.fireFilterChanged();
				

				/*
				 * Thread th = new Thread(new Runnable() {
				 * 
				 * @Override public void run() {
				 * 
				 * try { String message = "10#" + comboStock.getText() + "#RG";
				 * HashMap param = new HashMap();
				 * param.put(FeedEventDispatcher.PARAM_MESSAGE, message); String
				 * result = ((IEQTradeApp) apps) .getFeedEngine().getEngine()
				 * .getConnection().getQueue(message);
				 * 
				 * log.info("data:" + message + ":" + result); String[] header =
				 * result.split("\\|"); String type = header[1]; String stock =
				 * header[2]; String board = header[3]; // int idx =
				 * result.indexOf("["); int idxEnd = result.indexOf("<]");
				 * String sQueue = result.substring(idx + 1, idxEnd); String[]
				 * array = sQueue.split("!"); model.getDataVector().clear();
				 * model.refresh();
				 * 
				 * for (int i = 0; i < array.length; i++) { log.info("array:" +
				 * array[i]); String[] sp = array[i].split("\\["); Double price
				 * = new Double(sp[0]); String[] queuee = sp[1].split(">");
				 * Vector v = new Vector(); for (int j = 0; j < queuee.length;
				 * j++) { String[] dQueue = queuee[j].split("\\|"); String
				 * orderid = dQueue[0]; Double lot = new Double(dQueue[1]);
				 * Integer id = new Integer(dQueue[3].replace( "]", "")); //
				 * Queue q = new Queue(dQueue[0], price, new //
				 * Double(dQueue[1])); // q.setMatch(new //
				 * Double(dQueue[2].replace("]", // ""))); // v.add(q); Queue
				 * queum = new Queue(); queum.setActiontype(stock);
				 * queum.setBoard(board); queum.setLot(lot);
				 * queum.setPrice(price); queum.setOrderid(orderid);
				 * queum.setTypeData(Integer.parseInt(type.trim()));
				 * queum.setId(id); Vector vd = new Vector();
				 * vd.add(QueueDef.createTableRow(queum)); model.addRow(vd,
				 * false, false);
				 * 
				 * log.info("queue get:" + queuee[j]); } }
				 * 
				 * filter.getFilteredData("stock").setField(
				 * comboStock.getText());
				 * filter.getFilteredData("price").setField(price.getValue());
				 * filter.fireFilterChanged(); } catch (Exception e) {
				 * e.printStackTrace(); }
				 * 
				 * }
				 * 
				 * }); th.start();
				 */
			}
		});
		
		
		  
		  
		  
		  
		 
		btnClear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				filter.getFilteredData("stock").setField(null);
				filter.getFilteredData("price").setField(null);
				filter.fireFilterChanged();
			}
		});

		JPanel pnl = new JPanel();
		FormLayout layout = new FormLayout(
				"pref,2dlu,50px,2dlu,pref,2dlu,80px, 2dlu,pref, 2dlu, 80px , 2dlu, pref,2dlu,pref",
				"pref,2dlu,pref,2dlu");
		CellConstraints cc = new CellConstraints();
		PanelBuilder builder = new PanelBuilder(layout, pnl);

		// builder.add(new JLabel("Type"), cc.xy(1, 3));
		// builder.add(comboType, cc.xy(3, 3));
		builder.add(new JLabel("Stock"), cc.xy(1, 3));
		builder.add(comboStock, cc.xy(3, 3));
		builder.add(new JLabel("Price"), cc.xy(5, 3));
		builder.add(price, cc.xy(7, 3));
		builder.add(btnRefresh, cc.xy(9, 3));
		builder.add(btnClear, cc.xy(11, 3));
		JScrollPane t = new JScrollPane(pnl);
		t.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		t.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.setOpaque(true);
		pnlContent.add(t, BorderLayout.NORTH);
		pnlContent.add(tableQueue, BorderLayout.CENTER);

	}
	private void fieldStockAction(final String info){
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				String newStock = comboStock.getText().trim().toUpperCase();
				Double newPrice = (Double) price.getValue();
				if(newPrice == null)
					newPrice = new Double(0);
				if((newStock.length()>0 && !keyold.equals(newStock) 
						&& newPrice.doubleValue()>0)
						||(newPrice.doubleValue()>0 && newStock.length()>0 
								&& newPrice.doubleValue() !=oldPrice.doubleValue())){
					if(keyold.length() > 0){
						((IEQTradeApp) apps).getFeedEngine().unsubscribe(
								FeedParser.PARSER_QUEUE, keyold);
					}
					Double dd = new Double(price.getValue().toString());

					String stock = comboStock.getText().toUpperCase();
					String key = stock + "#RG" + "#0" + "#" + format2.format(dd);
					// log.info("subscribe:" + key);
					//System.out.println("subscribe:" + key);
					keyold = key;

					((IEQTradeApp) apps).getFeedEngine().subscribe(
							FeedParser.PARSER_QUEUE,
							FeedParser.PARSER_QUEUE + "|" + key);

					filter.getFilteredData("stock").setField(stock);
					filter.getFilteredData("price").setField(dd);
					filter.fireFilterChanged();
				}
				
			}
		});
	}
	

	@Override
	public void focus() {
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME)
						+ getProperty(C_PROPERTY_NO));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tablequeue", QueueDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 250, 420));

	}
	@Override
	public void show(Object param){
		if(param != null){
			HashMap par = (HashMap) param;
			comboStock.setSelectedItem((String)par.get("stock").toString());
			price.setText(par.get("price").toString());
			fieldvarbid.setText(par.get("varbid").toString());
			fieldStockAction(par.get("INFO")== null ? null : par.get("INFO").toString());
			//log.info("show keybid"+ keyold+":"+ par.get("varbid").toString());
			if(!keyold.equals("")){
				((IEQTradeApp)apps).getFeedEngine().unsubscribe(FeedParser.PARSER_QUEUE, keyold);
			}
			keyold="";
		}
		show();
			/*String newStock = comboStock.getText().trim().toUpperCase();
			Double newPrice = (Double) price.getValue();
			if(newPrice == null)
				newPrice = new Double(0);
			if((newStock.length()>0 && !keyold.equals(newStock) 
					&& newPrice.doubleValue()>0)
					||(newPrice.doubleValue()>0 && newStock.length()>0 
							&& newPrice.doubleValue() !=oldPrice.doubleValue())){
				if(keyold.length() > 0){
					((IEQTradeApp) apps).getFeedEngine().unsubscribe(
							FeedParser.PARSER_QUEUE, keyold);
				}
				Double dd = new Double(price.getValue().toString());

				String stock = comboStock.getText().toUpperCase();
				String key = stock + "#RG" + "#0" + "#" + format2.format(dd);
				// log.info("subscribe:" + key);
				keyold = key;

				((IEQTradeApp) apps).getFeedEngine().subscribe(
						FeedParser.PARSER_QUEUE,
						FeedParser.PARSER_QUEUE + "|" + key);

				filter.getFilteredData("stock").setField(stock);
				filter.getFilteredData("price").setField(dd);
				filter.fireFilterChanged();
		}
		
		super.show();*/
	}
	@Override
	public void hide() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_QUEUE, keyold);
		super.hide();
	}
	@Override
	public void close() {
		((IEQTradeApp) apps).getFeedEngine().unsubscribe(
				FeedParser.PARSER_QUEUE, keyold);
		super.close();
	}

	@Override
	public void refresh() {
	}

	@Override
	public void saveSetting() {
	}

}
