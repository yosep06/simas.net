package eqtrade.feed.ui.quote;

import java.util.HashMap;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.feed.model.Board;

public class FilterBoard extends FilterBase {
	private HashMap mapFilter = new HashMap();

	public FilterBoard(JComponent parent) {
		super(parent, "filter");
		mapFilter.put("code", new FilterColumn("code", Vector.class, null,
				FilterColumn.C_MEMBEROF));
		Vector temp = new Vector(2);
		temp.addElement("RG");
		temp.addElement("TN");
		((FilterColumn) getFilteredData("code")).setField(temp);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public int showDialog() {
		return super.showDialog();
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column) model.getValueAt(row, 0);
			Board dat = (Board) val.getSource();
			if (((FilterColumn) mapFilter.get("code")).compare(dat.getName())) {
				avail = true;
			}
			return avail;
		} catch (Exception ec) {
			return false;
		}
	}
}