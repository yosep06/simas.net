package eqtrade.application;

import java.awt.Color;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.server.RMISocketFactory;
import java.util.TimeZone;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

import com.vollux.demo.Settings;
import com.vollux.framework.Boot;
import com.vollux.framework.UI;
import com.vollux.framework.UIMain;
import com.vollux.framework.VolluxApp;
import com.vollux.framework.VolluxShortcut;

import eqtrade.feed.app.FeedApplication;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.Utils;
import eqtrade.trading.app.TradingApplication;

public class EQTrade extends Boot {
	private VolluxApp feedApp;
	private VolluxApp tradingApp;

	@Override
	public void buildApplication(String[] args) {
		checkingVersion();
		UI.setIcon(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		UIMain.setLogo(new ImageIcon(EQTrade.class.getResource("eqtrade.png")));
		feedApp = new FeedApplication();
		feedApp.start(args);
		feedApp.registerUIMain(getUIMain());

		tradingApp = new TradingApplication();
		tradingApp.start(args);
		tradingApp.registerUIMain(getUIMain());

		((FeedApplication) feedApp).setTradingApp(((IEQTradeApp) tradingApp)
				.getTradingEngine());
		((TradingApplication) tradingApp).setFeedApp(((IEQTradeApp) feedApp)
				.getFeedEngine());

		((FeedApplication) feedApp).setTrading((TradingApplication) tradingApp);
		((TradingApplication) tradingApp).setFeed((FeedApplication) feedApp);

		VolluxShortcut.registerApplication(tradingApp);
		VolluxShortcut.registerApplication(feedApp);
		VolluxShortcut.init();

		((UIMaster) getUIMain()).setApps((IEQTradeApp) feedApp);

		getUIMain().init(null);
		// feedApp.getUIMain().show();
		feedApp.getUI().showUI(FeedUI.UI_LOGON);
	}

	protected void checkingVersion() {
		// Check the JVM version ASAP
		try {
			String actual = System.getProperty("java.version");
			String required = "1.6";
			if (versionNumberCompare(actual, required) < 0) {
				//log.error("Wrong Java version required: " + required+ " actual: " + actual);
				JOptionPane optionPane = new JOptionPane(
						"Wrong Java version required: " + required
								+ " actual: " + actual,
						JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION);
				optionPane.createDialog("Error").setVisible(true);
				//
				Runtime.getRuntime().halt(0);
				System.exit(-1);
			}
		} catch (SecurityException x) {
		} catch (NumberFormatException x) {
		}
	}

	@Override
	public void start(String[] args) {
		System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
		Utils.deleteTemp("data/temp", new String[] {});

		String theme = "com.eqtrade.skin.Eqtrade";
		configureUI(Settings.createDefault(theme));
		UIDefaults defaults = UIManager.getDefaults();
		defaults.put("inactiveCaptionText", new Color(255, 255, 255));
		defaults.put("activeCaptionText", new Color(253, 253, 136));

		//log.info("starting vollux framework");
		init();

		uiMain = new UIMaster("Simas.net", VolluxApp.CONS_CORE);
		buildApplication(args);
	}

	public static void main(String[] args) {
			TimeZone.setDefault(TimeZone.getTimeZone("GMT+07:00"));
			try {
				RMISocketFactory.setSocketFactory(new RMISocketFactory() {
				public Socket createSocket(String host, int port)
				throws IOException {
				Socket socket = new Socket();
				socket.setSoTimeout(120000); //ini setting timeoutnya (2 menit)
				socket.setSoLinger(false, 0);
				socket.connect(new InetSocketAddress(host, port), 10000);
				return socket;
			}


			public ServerSocket createServerSocket(int port) throws IOException {
				return new ServerSocket(port);
			}
			});
			}catch (Exception e) {
				e.printStackTrace();
			}
			
			new EQTrade().start(args);
			//System.out.println(Utils.getMD5(new String("password1")));
	}
}
