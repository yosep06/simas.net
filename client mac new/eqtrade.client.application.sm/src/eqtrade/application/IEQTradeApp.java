package eqtrade.application;

import com.vollux.framework.IApp;

import eqtrade.feed.core.IFeedApp;
import eqtrade.trading.core.ITradingApp;

public interface IEQTradeApp extends IApp {
	public IFeedApp getFeedEngine();

	public ITradingApp getTradingEngine();

	//public boolean isSales();
}
