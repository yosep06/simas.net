package eqtrade.application;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.channels.FileLock;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.plaf.ColorUIResource;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.Action;
import com.vollux.framework.Menu;
import com.vollux.framework.UIMain;
import com.vollux.framework.VolluxShortcut;
import com.vollux.ui.JSkinPnl;

import eqtrade.feed.app.FeedApplication;
import eqtrade.feed.app.FeedUI;
import eqtrade.feed.core.Encryption;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Currency;
import eqtrade.feed.model.Indices;
import eqtrade.feed.model.Market;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.UserProfile;
import eqtrade.trading.ui.UIOrder;

public class UIMaster extends UIMain {
	private JLabel lblUserid;
	private JLabel lblFeed;
	private JLabel lblTrading;
	public static JLabel lblTime;
	private JLabel lblTrans;
	private JLabel lblCurr;

	public static JLabel lblBuy;
	public static JLabel lblSell;
	public static JLabel lblAmend;
	public static JLabel lblWithdraw;

	private JLabel lblLast;
	// private JLabel lblClose;
	// private JLabel lblHigh;
	// private JLabel lblLow;
	private JLabel lblChg;
	// private JLabel lblOpen;
	private JLabel lblVersion;
	private JLabel lblTemp;

	private Timer timer = null;
	private Thread timerThread = null; // 20-09-2016
	private static SimpleDateFormat formattime = new SimpleDateFormat(" HH:mm:ss ");
	//public static final String version = new String("version:2.5.5 beta");
	//public static final String version = new String("version:2.6.2");
	private static Properties propversion;
	//public static String C_VERSION = "version";
	private IEQTradeApp apps;

	Thread test;
	boolean timeboolean = false;

	public UIMaster(String title, String app) {
		super(title, app);
		try {
			SwingUtilities.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					UIManager.put("ToolTip.foreground", new ColorUIResource(
							Color.WHITE));
				}
			}); 
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		test = new Thread(new Runnable() {
			public void run() {
				try {
					FileOutputStream fos = new FileOutputStream("system.lock");
					FileLock fl = fos.getChannel().tryLock();
					if (fl != null) {
						
					} else {
						//log.error("aborted, because this application is already running.");
						JOptionPane optionPane = new JOptionPane(
								"Application is already running.",
								JOptionPane.ERROR_MESSAGE,
								JOptionPane.DEFAULT_OPTION);
						optionPane.createDialog("Error").setVisible(true);
						
						//frame.dispose();
						//System.exit(-1);
						
						Runtime.getRuntime().halt(0);
						System.exit(-1);
					}
				} catch (Exception ex) {
					//log.error("aborted, because this application is already running.");
					    JOptionPane optionPane = new JOptionPane(
							"Application is already running.",
							JOptionPane.ERROR_MESSAGE,
							JOptionPane.DEFAULT_OPTION);
					optionPane.createDialog("Error").setVisible(true);
					//frame.dispose();
					
					Runtime.getRuntime().halt(0);
					System.exit(-1);
					//
				}
				while (true) {
					try {
						Thread.sleep(500000);
					} catch (Exception ex) {
					}
				}
			}
		});
		test.start();
	}

	public void setProperty(String id, Object obj) {

		if (id.equals("feed_login")) {
			//Encryption a = 
			lblFeed.setIcon(new ImageIcon(UIMaster.class.getResource("feed_green.png")));
			((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
			changeIcon(lblBuy,new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));
			changeIcon(lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw_hover.jpg")));
			changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
			changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell_hover.jpg")));			
			indicesChanged();
			currChanged();
			marketChanged();
//			timer.start();
			timeboolean = true;//20-09-2016
		} else if (id.equals("feed_logout")) {
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(FeedParser.PARSER_CURRENCY, FeedParser.PARSER_CURRENCY);
			lblFeed.setIcon(new ImageIcon(UIMaster.class.getResource("feed_red.png")));
//			timer.stop();
			timeboolean = false;//20-09-2016
			lblTime.setText(" 00:00:00 ");
			lblTrans.setText("  Vol[0]   Val[0]   Freq[0]  ");
			lblCurr.setText("  USD-IDR : 00,000  ");
			lblLast.setText("0000.0000");
			lblChg.setText("0000.0000");
			lblUserid.setText(" Welcome,   ");
			
			changeIcon(lblBuy,new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));
			changeIcon(lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw_hover.jpg")));
			changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
			changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell_hover.jpg")));			
		} else if (id.equals("feed_disconnect")) {
			lblFeed.setIcon(new ImageIcon(UIMaster.class.getResource("feed_yellow.png")));
			changeIcon(lblBuy,new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));
			changeIcon(lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw_hover.jpg")));
			changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
			changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell_hover.jpg")));			
		} else if (id.equals("trading_login")) {
			lblTrading.setIcon(new ImageIcon(UIMaster.class.getResource("trading_blue.png")));			
			changeIcon(lblBuy,new ImageIcon(UIMaster.class.getResource("buy.jpg")));
			changeIcon(lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw.jpg")));
			changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend.jpg")));
			changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell.jpg")));			
		} else if (id.equals("trading_ready")) {
			lblTrading.setIcon(new ImageIcon(UIMaster.class.getResource("trading_green.png")));
			changeIcon(lblBuy,new ImageIcon(UIMaster.class.getResource("buy.jpg")));
			changeIcon(lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw.jpg")));
			changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend.jpg")));
			changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell.jpg")));
		} else if (id.equals("trading_logout")) {
			lblTrading.setIcon(new ImageIcon(UIMaster.class.getResource("trading_red.png")));
			changeIcon(lblBuy,new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));
			changeIcon(lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw_hover.jpg")));
			changeIcon(lblAmend, new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
			changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell_hover.jpg")));

		} else if (id.equals("trading_disconnect")) {
			lblTrading.setIcon(new ImageIcon(UIMaster.class.getResource("trading_yellow.png")));
			changeIcon(lblBuy,new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));
			changeIcon(lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw_hover.jpg")));
			changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
			changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell_hover.jpg")));
		} else if (id.equals("userid")) {
			lblUserid.setText(" Welcome,  " + obj.toString().toUpperCase()+ " ");
		} else
			super.setProperty(id, obj);
	}

	private void iconLogin(boolean islogin) {
		if (islogin) {
			changeIcon(lblBuy,new ImageIcon(UIMaster.class.getResource("buy.jpg")));
			changeIcon(lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw.jpg")));
			changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend.jpg")));
			changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell.jpg")));
			
		} else {
			changeIcon(lblBuy,new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));
			changeIcon(lblWithdraw,new ImageIcon(UIMaster.class.getResource("withdraw_hover.jpg")));
			changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
			changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell_hover.jpg")));
		}
	}

	public Object getProperty(String id) {
		return super.getProperty(id);
	}

	public void setApps(IEQTradeApp apps) {
		this.apps = apps;
	}

	public void loadSetting() {
		if (hSetting == null) {
			hSetting = new Hashtable();
		}
		if (hSetting.get("size") == null) {
			Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
			hSetting.put("size", new Rectangle(0, 0, screenSize.width, 10));
		}
	}

	protected void initUI() {
		super.initUI();		
		frame.setResizable(false);
		frame.getBtnClose().setVisible(false);
		frame.setMinimumSize(new Dimension(150, 102));
		frame.setSize(new Dimension(((Rectangle) hSetting.get("size")).width,((Rectangle) hSetting.get("size")).height));
		frame.getBtnMin().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				apps.getUI().minimizeAll();
				((FeedApplication) apps).getTrading().getUI().minimizeAll();

			}
		});
	
		frame.addWindowStateListener(new WindowStateListener() {
			@Override
			public void windowStateChanged(WindowEvent e) {
				if (e.getNewState() == 0) {
					apps.getUI().restoreAll();
					((FeedApplication) apps).getTrading().getUI().restoreAll();

				}
			}
		});
		
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Runtime.getRuntime().halt(0);
				System.exit(-1);
			}
		});
	
	}

	protected void build() {
		Font f = new Font("Dialog", Font.PLAIN, 12);

		/*timer = new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {lblTime.setText(formattime.format(new Date(apps.getFeedEngine().getEngine().getConnection().getTime())));
			}
		});
		
		timer.setInitialDelay(1000);
		timer.stop();*/
		
		
		lblUserid = new JLabel(" Welcome,   ");
		lblUserid.setPreferredSize(new Dimension(10, 23));
		lblUserid.setBorder(new LineBorder(Color.gray));
		lblFeed = new JLabel(new ImageIcon(UIMaster.class.getResource("feed_red.png")));
		lblTrading = new JLabel(new ImageIcon(UIMaster.class.getResource("trading_red.png")));
		lblTime = new JLabel("  00:00:00  ");
		lblTime.setFont(f);
		lblTime.setPreferredSize(new Dimension(80, 23));
		lblTime.setHorizontalAlignment(JLabel.CENTER);
		lblTime.setBorder(new LineBorder(Color.gray));
		lblVersion = new JLabel("version:" + Encryption.getVersion());
		lblTrans = new JLabel("  Vol[0]   Val[0]   Freq[0]  ");
		lblTrans.setFont(f);
		lblTrans.setHorizontalAlignment(JLabel.CENTER);
		lblVersion.setHorizontalAlignment(JLabel.CENTER);
		lblTrans.setPreferredSize(new Dimension(300, 23));
		lblVersion.setPreferredSize(new Dimension(150, 23));
		lblTemp = new JLabel("");
		lblTemp.setPreferredSize(new Dimension(150, 50));
		lblTemp.setBorder(new LineBorder(Color.gray));
		lblTrans.setBorder(new LineBorder(Color.gray));
		lblVersion.setBorder(new LineBorder(Color.gray));
		lblCurr = new JLabel("  USD-IDR : 00,000  ");
		lblCurr.setFont(f);
		lblCurr.setBorder(new LineBorder(Color.gray));
		lblCurr.setPreferredSize(new Dimension(150, 23));
		lblCurr.setHorizontalAlignment(JLabel.CENTER);

		lblLast = new JLabel("0000.0000");
		lblLast.setFont(f);
		lblLast.setHorizontalAlignment(JLabel.CENTER);
		lblLast.setPreferredSize(new Dimension(120, 23));
		lblLast.setBorder(new LineBorder(Color.gray));
		lblChg = new JLabel("0000.0000");
		lblChg.setFont(f);
		lblChg.setHorizontalAlignment(JLabel.CENTER);
		lblChg.setPreferredSize(new Dimension(150, 23));
		lblChg.setBorder(new LineBorder(Color.gray));

		lblBuy = new JLabel(new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));
		lblBuy.setName("B");
		lblBuy.setVerticalAlignment(SwingConstants.CENTER);
		lblBuy.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblBuy.setHorizontalTextPosition(SwingConstants.CENTER);
		lblBuy.setFont(new Font(Font.SERIF, Font.BOLD, 11));
		lblBuy.setToolTipText("BUY");

		lblBuy.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {				
				boolean islogin = ((FeedApplication) apps).getTrading().getAction().getState() == Action.C_READY_STATE;
				if (islogin) {
					 changeIcon(lblBuy, new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));					 
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				boolean islogin = ((FeedApplication) apps).getTrading().getAction().getState() == Action.C_READY_STATE;
				if (islogin) {
					boolean isgranted = ((FeedApplication) apps).getTrading().getAction().get(TradingAction.A_SHOWBUYORDER).isGranted();

					if (isgranted) {
						changeIcon(lblBuy, new ImageIcon(UIMaster.class.getResource("buy.jpg")));
						JLabel lbl = (JLabel) e.getComponent();
						executeOrder(lbl.getName());
					} else {
						((FeedApplication) apps).getTrading().getUI().showUI(TradingUI.UI_PIN);
					}
				} else {
					((FeedApplication) apps).getUI().showUI(FeedUI.UI_LOGON);
				}

			}

			@Override
			public void mouseExited(MouseEvent e) {								
				boolean islogin = ((FeedApplication) apps).getTrading().getAction().getState() == Action.C_READY_STATE;
				if (islogin) {
					boolean isgranted = ((FeedApplication) apps).getTrading().getAction().get(TradingAction.A_SHOWBUYORDER).isGranted();
					if (isgranted) {
						changeIcon(lblBuy, new ImageIcon(UIMaster.class.getResource("buy.jpg")));
					}
					else {
						changeIcon(lblBuy, new ImageIcon(UIMaster.class.getResource("buy_hover.jpg")));	
					}	
				}				
			}
		});

		lblAmend = new JLabel(new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
		lblAmend.setName("A");
		lblAmend.setVerticalAlignment(SwingConstants.CENTER);
		lblAmend.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblAmend.setHorizontalTextPosition(SwingConstants.CENTER);
		lblAmend.setFont(new Font(Font.SERIF, Font.BOLD, 11));
		lblAmend.setToolTipText("AMEND");
		lblAmend.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {				
				boolean islogin = ((FeedApplication) apps).getTrading().getAction().getState() == Action.C_READY_STATE;
				if (islogin) {
					changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				boolean islogin = ((FeedApplication) apps).getTrading().getAction().getState() == Action.C_READY_STATE;
				if (islogin) {
					boolean isgranted = ((FeedApplication) apps).getTrading()
							.getAction().get(TradingAction.A_SHOWAMENDORDER)
							.isGranted();

					if (!isgranted) {
						((FeedApplication) apps).getTrading().getUI()
								.showUI(TradingUI.UI_PIN);
					} else {

						changeIcon(
								lblAmend,
								new ImageIcon(UIMaster.class
										.getResource("amend.jpg")));
						JLabel lbl = (JLabel) e.getComponent();
						executeOrder(lbl.getName());
					}
				} else {
					((FeedApplication) apps).getUI().showUI(FeedUI.UI_LOGON);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {				
				boolean islogin = ((FeedApplication) apps).getTrading().getAction().getState() == Action.C_READY_STATE;
				if (islogin) {
					boolean isgranted = ((FeedApplication) apps).getTrading().getAction().get(TradingAction.A_SHOWBUYORDER).isGranted();
					if (isgranted) {
						changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend.jpg")));
					}
					else {
						changeIcon(lblAmend,new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));	
					}						
				}
			}
		});

		lblWithdraw = new JLabel(new ImageIcon(UIMaster.class.getResource("amend_hover.jpg")));
		lblWithdraw.setName("W");
		lblWithdraw.setVerticalAlignment(SwingConstants.CENTER);
		lblWithdraw.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblWithdraw.setHorizontalTextPosition(SwingConstants.CENTER);
		lblWithdraw.setFont(new Font(Font.SERIF, Font.BOLD, 11));
		lblWithdraw.setToolTipText("WITHDRAW");
		lblWithdraw.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {				
				boolean islogin = ((FeedApplication) apps).getTrading().getAction().getState() == Action.C_READY_STATE;
				if (islogin) {
					changeIcon(
							lblWithdraw,
							new ImageIcon(UIMaster.class
									.getResource("withdraw_hover.jpg")));
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				boolean islogin = ((FeedApplication) apps).getTrading()
						.getAction().getState() == Action.C_READY_STATE;

				if (islogin) {
					boolean isgranted = ((FeedApplication) apps).getTrading()
							.getAction().get(TradingAction.A_SHOWWITHDRAWORDER)
							.isGranted();
					if (!isgranted) {
						((FeedApplication) apps).getTrading().getUI()
								.showUI(TradingUI.UI_PIN);
					} else {

						changeIcon(
								lblWithdraw,
								new ImageIcon(UIMaster.class
										.getResource("withdraw.jpg")));
						JLabel lbl = (JLabel) e.getComponent();
						executeOrder(lbl.getName());
					}
				} else {
					((FeedApplication) apps).getUI().showUI(FeedUI.UI_LOGON);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {

				/*
				 * boolean islogin = ((FeedApplication) apps).getTrading()
				 * .getAction().getState() == Action.C_READY_STATE;
				 * 
				 * if (islogin) { boolean isgranted = ((FeedApplication)
				 * apps).getTrading()
				 * .getAction().get(TradingAction.A_SHOWWITHDRAWORDER)
				 * .isGranted(); if (isgranted) { changeIcon( lblWithdraw, new
				 * ImageIcon(UIMaster.class .getResource("withdraw.jpg"))); } }
				 */
				boolean islogin = ((FeedApplication) apps).getTrading()
						.getAction().getState() == Action.C_READY_STATE;
				if (islogin) {
					boolean isgranted = ((FeedApplication) apps).getTrading().getAction().get(TradingAction.A_SHOWBUYORDER).isGranted();
					if (isgranted) {
						changeIcon(lblWithdraw,	new ImageIcon(UIMaster.class.getResource("withdraw.jpg")));
					}
					else
					{
						changeIcon(lblWithdraw,	new ImageIcon(UIMaster.class.getResource("withdraw_hover.jpg")));	
					}
				}	
			}
		});

		lblSell = new JLabel(new ImageIcon(
				UIMaster.class.getResource("amend_hover.jpg")));
		lblSell.setName("S");
		lblSell.setVerticalAlignment(SwingConstants.CENTER);
		lblSell.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblSell.setHorizontalTextPosition(SwingConstants.CENTER);
		lblSell.setFont(new Font(Font.SERIF, Font.BOLD, 11));
		// lblSell.setText("SELL");
		lblSell.setToolTipText("SELL");
		lblSell.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				// boolean islogin = apps.getTradingEngine().getUserId() != null
				// && !apps.getTradingEngine().getUserId().isEmpty();

				/*
				 * boolean islogin = ((FeedApplication) apps).getTrading()
				 * .getAction().getState() == Action.C_READY_STATE;
				 * 
				 * if (islogin) { boolean isgranted = ((FeedApplication)
				 * apps).getTrading()
				 * .getAction().get(TradingAction.A_SHOWSELLORDER) .isGranted();
				 * 
				 * if (isgranted) { changeIcon( lblSell, new
				 * ImageIcon(UIMaster.class .getResource("sell_hover.jpg"))); }
				 * }
				 */
				boolean islogin = ((FeedApplication) apps).getTrading()
						.getAction().getState() == Action.C_READY_STATE;
				if (islogin) {
					changeIcon(
							lblSell,
							new ImageIcon(UIMaster.class
									.getResource("sell_hover.jpg")));
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				boolean islogin = ((FeedApplication) apps).getTrading()
						.getAction().getState() == Action.C_READY_STATE;

				if (islogin) {
					boolean isgranted = ((FeedApplication) apps).getTrading()
							.getAction().get(TradingAction.A_SHOWSELLORDER)
							.isGranted();

					if (!isgranted) {
						((FeedApplication) apps).getTrading().getUI()
								.showUI(TradingUI.UI_PIN);
					} else {
						changeIcon(
								lblSell,
								new ImageIcon(UIMaster.class
										.getResource("sell.jpg")));
						JLabel lbl = (JLabel) e.getComponent();
						executeOrder(lbl.getName());
					}
				} else {
					((FeedApplication) apps).getUI().showUI(FeedUI.UI_LOGON);
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {

				/*
				 * boolean islogin = ((FeedApplication) apps).getTrading()
				 * .getAction().getState() == Action.C_READY_STATE; if (islogin)
				 * {
				 * 
				 * boolean isgranted = ((FeedApplication) apps).getTrading()
				 * .getAction().get(TradingAction.A_SHOWSELLORDER) .isGranted();
				 * log.info("isgranted sell exit " + isgranted); if (isgranted)
				 * { changeIcon( lblSell, new ImageIcon(UIMaster.class
				 * .getResource("sell.jpg"))); } }
				 */
				boolean islogin = ((FeedApplication) apps).getTrading()
						.getAction().getState() == Action.C_READY_STATE;
				if (islogin) {
					boolean isgranted = ((FeedApplication) apps).getTrading().getAction().get(TradingAction.A_SHOWBUYORDER).isGranted();
					if (isgranted) {
						changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell.jpg")));
					}
					else
					{
						changeIcon(lblSell,new ImageIcon(UIMaster.class.getResource("sell_hover.jpg")));	
					}
					
					
				}
			}
		});
		timerThread = new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						try {
							while(true){
		//						if (timeboolean) {
									lblTime.setText(formattime.format(new Date(apps.getFeedEngine().getEngine().getConnection().getTime())));
									Thread.sleep(1000);	
		//						}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});

		timerThread.start(); //20-09-2016

		/*
		 * lblBuy.setPreferredSize(new Dimension(50, 23)); lblBuy.setFont(f);
		 * lblBuy.setBorder(new LineBorder(Color.gray));
		 * lblBuy.setHorizontalAlignment(JLabel.CENTER); lblBuy.setName("B");
		 */

		/*
		 * lblSell = new JLabel("S"); lblSell.setPreferredSize(new Dimension(50,
		 * 23)); lblSell.setFont(f); lblSell.setBorder(new
		 * LineBorder(Color.gray));
		 * lblSell.setHorizontalAlignment(JLabel.CENTER); lblSell.setName("S");
		 * 
		 * lblAmend = new JLabel("A"); lblAmend.setPreferredSize(new
		 * Dimension(50, 23)); lblAmend.setFont(f); lblAmend.setBorder(new
		 * LineBorder(Color.gray));
		 * lblAmend.setHorizontalAlignment(JLabel.CENTER);
		 * lblAmend.setName("A");
		 * 
		 * lblWithdraw = new JLabel("W"); lblWithdraw.setPreferredSize(new
		 * Dimension(50, 23)); lblWithdraw.setFont(f); lblWithdraw.setBorder(new
		 * LineBorder(Color.gray));
		 * lblWithdraw.setHorizontalAlignment(JLabel.CENTER);
		 * lblWithdraw.setName("W");
		 */

		/*
		 * lblOpen = new JLabel("0000.0000");
		 * lblOpen.setHorizontalAlignment(JLabel.CENTER);
		 * lblOpen.setPreferredSize(new Dimension(90,23)); lblOpen.setBorder(new
		 * LineBorder(Color.gray)); lblClose = new JLabel("0000.0000");
		 * lblClose.setHorizontalAlignment(JLabel.CENTER);
		 * lblClose.setPreferredSize(new Dimension(90,23));
		 * lblClose.setBorder(new LineBorder(Color.gray)); lblHigh = new
		 * JLabel("0000.0000"); lblHigh.setHorizontalAlignment(JLabel.CENTER);
		 * lblHigh.setPreferredSize(new Dimension(90,23)); lblHigh.setBorder(new
		 * LineBorder(Color.gray)); lblLow = new JLabel("0000.0000");
		 * lblLow.setHorizontalAlignment(JLabel.CENTER);
		 * lblLow.setPreferredSize(new Dimension(90,23)); lblLow.setBorder(new
		 * LineBorder(Color.gray));
		 */

		/*
		 * MouseAdapter mj = new MouseAdapter() { public void
		 * mouseEntered(MouseEvent e) { frame.setCursor(new
		 * Cursor(Cursor.HAND_CURSOR)); }
		 * 
		 * public void mouseExited(MouseEvent e) {
		 * frame.setCursor(Cursor.getDefaultCursor()); }
		 * 
		 * public void mousePressed(MouseEvent e) { TradingApplication ta =
		 * ((FeedApplication) apps).getTrading(); JLabel lbl = (JLabel)
		 * e.getComponent();
		 * 
		 * if (ta.getAction().getState() == Action.C_READY_STATE) {
		 * 
		 * 
		 * 
		 * } } };
		 */
		// lblBuy.addMouseListener(mj);
		// lblAmend.addMouseListener(mj);
		// lblSell.addMouseListener(mj);
		// lblWithdraw.addMouseListener(mj);

		JLabel imgLogo = new JLabel(logo);
		JLabel imgBack = new JLabel(new ImageIcon(
				UIMain.class.getResource("background.png")));
		MouseAdapter ma = new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {
				frame.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}

			public void mouseExited(MouseEvent e) {
				frame.setCursor(Cursor.getDefaultCursor());
			}

			public void mousePressed(MouseEvent e) {
				X = e.getX();
				Y = e.getY();
			}
		};
		imgLogo.addMouseListener(ma);
		// MouseMotionAdapter mma = new MouseMotionAdapter(){
		// public void mouseDragged(MouseEvent e){
		// frame.setLocation(frame.getLocation().x+(e.getX()-X),frame.getLocation().y+(e.getY()-Y));
		// }
		// };
		imgLogo.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2 && !e.isPopupTrigger()) {
					frame.setExtendedState(JFrame.ICONIFIED);
				}
			}
		});
		// imgLogo.addMouseMotionListener(mma);
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setOpaque(false);
		// pnlContent.addMouseListener(ma);
		// pnlContent.addMouseMotionListener(mma);
		Menu menu = new Menu(appAll);
		menu.build();
		JPanel p = new JPanel();
		FormLayout l = new FormLayout(
				"135, 2dlu, pref, 2dlu, 70px,2dlu, pref:grow, 2dlu, 100px",
				"43px");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, p);
		b.add(imgLogo, c.xy(1, 1));
		b.add(menu, c.xy(3, 1));
		b.add(VolluxShortcut.getEditor(), c.xy(5, 1));
		b.add(lblUserid, c.xy(7, 1));
		b.add(lblVersion, c.xy(9, 1));
		imgBack.setSize(900, 35);

		JPanel info = new JPanel();
		info.setBackground(UIManager.getLookAndFeelDefaults()
				.getColor("Menu.selectionBackground").darker());
		info.setBorder(new EmptyBorder(3, 3, 3, 3));
		FormLayout ll = new FormLayout(
				"pref, 3dlu, pref, 3dlu, pref,3dlu,pref,2dlu,pref:grow,2dlu,pref:grow,2dlu,pref:grow,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref",
				"pref");
		CellConstraints cc = new CellConstraints();
		PanelBuilder bb = new PanelBuilder(ll, info);
		bb.add(lblBuy, cc.xy(1, 1));
		bb.add(lblSell, cc.xy(3, 1));
		bb.add(lblAmend, cc.xy(5, 1));
		bb.add(lblWithdraw, cc.xy(7, 1));

		bb.add(lblTrans, cc.xy(9, 1));
		bb.add(lblLast, cc.xy(11, 1));
		bb.add(lblChg, cc.xy(13, 1));
		bb.add(lblCurr, cc.xy(15, 1));
		bb.add(lblTime, cc.xy(17, 1));
		bb.add(lblFeed, cc.xy(19, 1));
		bb.add(lblTrading, cc.xy(21, 1));

		// bb.add(lblOpen, cc.xy(7,1));
		// bb.add(lblHigh, cc.xy(9,1));
		// bb.add(lblLow, cc.xy(11,1));
		// bb.add(lblClose, cc.xy(13,1));
		// bb.add(lblCurr, cc.xy(15,1));
		// bb.add(lblTime, cc.xy(17,1));
		// bb.add(lblFeed, cc.xy(19,1));
		// bb.add(lblTrading, cc.xy(21,1));
		// bb.add(lblTemp, cc.xywh(19,1,1,3));

		JPanel temp = new JPanel(new BorderLayout());
		temp.add(p, BorderLayout.CENTER);
		temp.add(info, BorderLayout.SOUTH);
		pnlContent.add(temp, BorderLayout.CENTER);

		(apps).getFeedEngine().getStore(FeedStore.DATA_INDICES)
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						indicesChanged();
					}
				});
		(apps).getFeedEngine().getStore(FeedStore.DATA_MARKET)
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						marketChanged();
					}
				});
		(apps).getFeedEngine().getStore(FeedStore.DATA_CURRENCY)
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						currChanged();
					}
				});
	}

	protected void executeOrder(String lbl) {

		TradingApplication ta = ((FeedApplication) apps).getTrading();

		if (ta.getAction().getState() == Action.C_READY_STATE) {

			if ((lbl.equals("B") || lbl.equals("S"))) {
				HashMap param = new HashMap();
				param.put("TYPE", lbl.equals("B") ? "BUY" : "SELL");
				ta.getUI().showUI(TradingUI.UI_ENTRYORDER, param);
			} else if (lbl.equals("W") || lbl.equals("A")) {
				String nameAction = lbl.equals("W") ? "withdrawAction"
						: "amendAction";
				((UIOrder) ta.getUI().getForm(TradingUI.UI_ORDER))
						.executeAction(nameAction);
			}
		}
	}

	public static void changeIcon(JLabel lbl, ImageIcon imageIcon) {
		lbl.setIcon(imageIcon);
	}

	private static NumberFormat formatter = new DecimalFormat("#,##0");
	private static NumberFormat formatter2 = new DecimalFormat("#,##0.000");
	private static NumberFormat formatter3 = new DecimalFormat("#,##0.00");

	private String getFormat(Double val) {
		return (formatter.format(val));
	}

	/*private void getversion() {
		try { 
			File file = new File("data/config/config-lot.properties");
			if(!file.exists())
				file.createNewFile();				
			propversion = new Properties();
			propversion.load(new FileInputStream(file));
				if(propversion.isEmpty()) {					
					propversion.put(C_VERSION, );
					propversion.store(new FileOutputStream(file), "");
				}
			
			//System.out.println("proplot "+C_LOTSIZE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	private void marketChanged() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Market market = (Market) apps.getFeedEngine()
						.getStore(FeedStore.DATA_MARKET)
						.getDataByKey(new Object[] { "TOTAL" });
				Double mrkvol, mrkval, mrkfreq;
				String strShowText = new String("");

				mrkvol = market.getVolume();
				strShowText = "   Vol [ ";
				if ((mrkvol >= 1000000) && (mrkvol < 1000000000)) {
					mrkvol = mrkvol / 1000000;
					strShowText = strShowText + formatter3.format(mrkvol)
							+ " M ]";
				} else if (mrkvol >= 1000000000) {
					mrkvol = mrkvol / 1000000000;
					strShowText = strShowText + formatter3.format(mrkvol)
							+ " B ]";

				} else {
					strShowText = strShowText + getFormat(mrkvol) + " ]";
				}

				mrkval = market.getValue();
				strShowText = strShowText + "   Val [ ";
				if ((mrkval >= 1000000) && (mrkval < 1000000000)) {
					mrkval = mrkval / 1000000;
					strShowText = strShowText + formatter3.format(mrkval)
							+ " M ]";
				} else if (mrkval >= 1000000000) {
					mrkval = mrkval / 1000000000;
					strShowText = strShowText + formatter3.format(mrkval)
							+ " B ]";

				} else {
					strShowText = strShowText + getFormat(mrkval) + " ]";
				}

				mrkfreq = market.getFreq();
				strShowText = strShowText + "   Freq [ ";
				if ((mrkfreq >= 1000000) && (mrkfreq < 1000000000)) {
					mrkfreq = mrkfreq / 1000000;
					strShowText = strShowText + formatter3.format(mrkfreq)
							+ " M ]";
				} else if (mrkfreq >= 1000000000) {
					mrkfreq = mrkfreq / 1000000000;
					strShowText = strShowText + formatter3.format(mrkfreq)
							+ " B ]";
				} else {
					strShowText = strShowText + getFormat(mrkfreq) + " ]   ";
				}

				if (market != null) {
					// lblTrans.setText("   V[ "+formatter2.format(market.getVolume())+
					// " " + prefvol +
					// " ]   A[ "+getFormat(market.getValue())+" ]   F[ "+getFormat(market.getFreq())+" ]   ");
					lblTrans.setText(strShowText);
				}
			}
		});
	}

	private void currChanged() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				//Untuk mengambil kurs mana yang akan di jadikan patokan di menu
				Currency curr = (Currency) apps.getFeedEngine()
						.getStore(FeedStore.DATA_CURRENCY)
						.getDataByKey(new Object[] { "USD-IDR" });
						//.getDataByKey(new Object[] { "USD-RP" });
				if (curr != null) {
					lblCurr.setText(" USD-IDR : "
							+ formatter.format(curr.getValue()));
				}
			}
		});
	}

	private void indicesChanged() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Indices index = (Indices) ((IEQTradeApp) apps).getFeedEngine()
						.getStore(FeedStore.DATA_INDICES)
						.getDataByKey(new Object[] { "COMPOSITE" });
				if (index != null) {
					lblLast.setText("IDX: "
							+ formatter2.format(index.getLast()));
					// lblOpen.setText("Open: "+formatter2.format(index.getOpen()));
					// lblClose.setText("Close: "+formatter2.format(index.getPrev()));
					// lblHigh.setText("High: "+formatter2.format(index.getHigh()));
					// lblLow.setText("Low: "+formatter2.format(index.getLow()));
					lblChg.setText("Change: "
							+ formatter2.format(index.getChange()) + "("
							+ formatter2.format(index.getPercent()) + " %)");
					// lblClose.setForeground(FeedSetting.getColor(FeedSetting.C_ZERO));
					// lblOpen.setForeground(index.getOpen().doubleValue()>
					// index.getPrev().doubleValue() ?
					// FeedSetting.getColor(FeedSetting.C_PLUS) :
					// (index.getOpen().doubleValue()<index.getPrev().doubleValue())
					// ? FeedSetting.getColor(FeedSetting.C_MINUS) :
					// FeedSetting.getColor(FeedSetting.C_ZERO));
					lblLast.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
							.getColor(FeedSetting.C_PLUS) : (index.getChange()
							.doubleValue() < 0) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
					// lblHigh.setForeground(index.getHigh().doubleValue()>index.getPrev().doubleValue()
					// ? FeedSetting.getColor(FeedSetting.C_PLUS) :
					// (index.getHigh().doubleValue()<index.getPrev().doubleValue())
					// ? FeedSetting.getColor(FeedSetting.C_MINUS) :
					// FeedSetting.getColor(FeedSetting.C_ZERO));
					// lblLow.setForeground(index.getLow().doubleValue()>index.getPrev().doubleValue()
					// ? FeedSetting.getColor(FeedSetting.C_PLUS) :
					// (index.getLow().doubleValue()<index.getPrev().doubleValue())
					// ? FeedSetting.getColor(FeedSetting.C_MINUS) :
					// FeedSetting.getColor(FeedSetting.C_ZERO));
					lblChg.setForeground(index.getChange().doubleValue() > 0 ? FeedSetting
							.getColor(FeedSetting.C_PLUS) : (index.getChange()
							.doubleValue() < 0) ? FeedSetting
							.getColor(FeedSetting.C_MINUS) : FeedSetting
							.getColor(FeedSetting.C_ZERO));
				} else {
					lblLast.setText("IDX:  - ");
					// lblClose.setText("Close:  - ");
					// lblHigh.setText("High:  - ");
					// lblLow.setText("Low:  - ");
					lblChg.setText("Change:  -");
					// lblOpen.setText("Open: -");
				}
			}
		});
	}

	public void checkIconLogin() {
		boolean isgrantbuy = ((FeedApplication) apps).getTrading().getAction()
				.get(TradingAction.A_SHOWBUYORDER).isGranted();

		boolean isgrantsell = ((FeedApplication) apps).getTrading().getAction()
				.get(TradingAction.A_SHOWSELLORDER).isGranted();

		boolean isgrantamend = ((FeedApplication) apps).getTrading()
				.getAction().get(TradingAction.A_SHOWAMENDORDER).isGranted();

		boolean isgrantwithdraw = ((FeedApplication) apps).getTrading()
				.getAction().get(TradingAction.A_SHOWWITHDRAWORDER).isGranted();

		if (isgrantbuy)
			changeIcon(lblBuy,
					new ImageIcon(UIMaster.class.getResource("buy.jpg")));

		if (isgrantwithdraw)
			changeIcon(lblWithdraw,
					new ImageIcon(UIMaster.class.getResource("withdraw.jpg")));

		if (isgrantamend)
			changeIcon(lblAmend,
					new ImageIcon(UIMaster.class.getResource("amend.jpg")));

		if (isgrantsell)
			changeIcon(lblSell,
					new ImageIcon(UIMaster.class.getResource("sell.jpg")));
	}
	
	public Point getLocationLbl(){
		return lblTrans.getLocation();
	}

}
