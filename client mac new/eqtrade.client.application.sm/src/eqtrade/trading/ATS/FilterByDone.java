package eqtrade.trading.ATS;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.table.TableModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import quickfix.FieldConvertError;

import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.trading.model.Account;
import eqtrade.trading.model.Schedule;

public class FilterByDone extends FilterBase{
	private HashMap mapFilter = new HashMap();
	private GridModel dataSchedule;
	private Log log = LogFactory.getLog(getClass());
	
	public FilterByDone(Component parent, GridModel _dataSchedule) {
		super(parent, "filter");
		mapFilter.put("stock", new FilterColumn("stock", String.class, 
				new String(""), FilterColumn.C_EQUAL));
		mapFilter.put("orderid", new FilterColumn("order", String.class,
				new String(""), FilterColumn.C_EQUAL));
		mapFilter.put("label", new FilterColumn("label", String.class, 
				new String(""), FilterColumn.C_EQUAL));
		mapFilter.put("buysell", new FilterColumn("buysell",String.class
				, new String(""), FilterColumn.C_EQUAL));
		this.dataSchedule = _dataSchedule;
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try {
			Column val = (Column)model.getValueAt(row, 0);
			Schedule data = (Schedule) val.getSource();

			if(((FilterColumn)mapFilter.get("stock")).compare(data.getcSecid())){
				return avail;				
			}
			else if(((FilterColumn)mapFilter.get("orderid")).compare(data.getcOrderid()) && 
					((FilterColumn)mapFilter.get("buysell")).compare(data.getcBuysell()) ){
				return avail;
			}
			else if(((FilterColumn)mapFilter.get("label")).compare(data.getcLabel())
					){
				return avail;
			}
		} catch (Exception e) {

		}
		return avail;
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	public void setFilter(HashMap map) {
		if (map != null) {
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

	public HashMap getFilter() {
		return mapFilter;
	}

	@Override
	public int showDialog() {
		return super.showDialog();
	}

}
