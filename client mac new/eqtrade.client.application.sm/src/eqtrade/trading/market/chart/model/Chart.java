package eqtrade.trading.market.chart.model;

import java.util.Hashtable;

public class Chart {
	 protected static final long serialVersionUID = 7891111031929287100L;
	  protected Hashtable htData = new Hashtable();

	  public Chart() {
	    this.htData = new Hashtable();
	  }

	  public Chart(Hashtable htData) {
	    this.htData = htData;
	  }

	  public String getStock() {
	    return this.htData.get("stock").toString();
	  }

	  public void setStock(String stock) {
	    this.htData.put("stock", stock);
	  }

	  public String getDate() {
	    String temp = this.htData.get("date").toString();
	    if (temp.length() == 10) {
	      temp = temp + " 16:00";
	    }
	    return temp;
	  }

	  public void setDate(String date) {
	    this.htData.put("date", date);
	  }

	  public String getTime() {
	    return this.htData.get("time").toString();
	  }

	  public void setTime(String time) {
	    this.htData.put("time", time);
	  }

	  public String getHigh() {
	    return this.htData.get("high").toString();
	  }

	  public void setHigh(String high) {
	    this.htData.put("high", high);
	  }

	  public String getLow() {
	    return this.htData.get("low").toString();
	  }

	  public void setLow(String low) {
	    this.htData.put("low", low);
	  }

	  public String getOpen() {
	    return this.htData.get("open").toString();
	  }

	  public void setOpen(String open) {
	    this.htData.put("open", open);
	  }

	  public String getClose() {
	    return this.htData.get("close").toString();
	  }

	  public void setClose(String close) {
	    this.htData.put("close", close);
	  }

	  public String getVolume() {
	    return this.htData.get("volume").toString();
	  }

	  public void setVolume(String volume) {
	    this.htData.put("volume", volume);
	  }
}
