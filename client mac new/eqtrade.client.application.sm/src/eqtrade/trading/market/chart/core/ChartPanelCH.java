package eqtrade.trading.market.chart.core;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseEvent;
//import java.awt.geom.Ellipse2D;
//import java.awt.geom.Ellipse2D.Double;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.plot.CombinedDomainXYPlot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.ui.RectangleEdge;

public class ChartPanelCH extends ChartPanel{
	 protected static final long serialVersionUID = 1613413751594491346L;
	  protected static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
	  protected int m_iX;
	  protected int m_iY;
	  protected int left;
	  protected double m_dX;
	  protected double m_dY;
	  protected double m_dXX;
	  protected double m_dYY;
	  protected Line2D m_l2dCrosshairX;
	  protected Line2D m_l2dCrosshairY;
	  protected Ellipse2D m_ellipse;
	  protected String xStr;
	  protected String yStr;
	  protected String dateStr;
	  protected boolean move = false;

	  public ChartPanelCH(JFreeChart arg0) {
	    super(arg0);
	    setDisplayToolTips(false);
	    setDomainZoomable(true);
	    setRangeZoomable(false);
	    setFillZoomRectangle(true);
	  }

	  public void setMove(boolean move) {
	    this.move = move;
	  }

	  public boolean getMove() {
	    return this.move;
	  }

	  public void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    if (getChart() == null) {
	      return;
	    }
	    this.m_l2dCrosshairX = null;
	    this.m_l2dCrosshairY = null;
	  }

	  public void mouseReleased2(MouseEvent e)
	  {
	    Rectangle2D screenDataArea = getScreenDataArea(this.m_iX, this.m_iY);
	    if (screenDataArea == null) return;
	    Graphics2D g2 = (Graphics2D)getGraphics();
	    int iDAMaxX = (int)screenDataArea.getMaxX();
	    int iDAMinX = (int)screenDataArea.getMinX();

	    int iDAMaxY = getHeight() - 23;
	    int iDAMinY = 7;
	    g2.setXORMode(new Color(16776960));
	    if ((this.m_l2dCrosshairX != null) && (this.m_l2dCrosshairY != null)) {
	      g2.draw(this.m_l2dCrosshairX);
	      g2.draw(this.m_l2dCrosshairY);

	      if (this.dateStr != null) g2.drawString(this.dateStr, iDAMinX + 3, (int)screenDataArea.getMaxY() - 3);
	    }
	    this.left = (iDAMinX + 3);
	    this.m_l2dCrosshairX = null;
	    this.m_l2dCrosshairY = null;
	    g2.dispose();
	  }

	  public void clearCrosshair(MouseEvent e) {
	    Graphics2D g2 = (Graphics2D)getGraphics();
	    int iDAMaxY = getHeight() - 23;
	    int iDAMinY = 7;
	    if (this.m_l2dCrosshairX == null) return;
	    g2.setXORMode(new Color(16776960));
	    if ((this.m_l2dCrosshairX != null) && (this.m_l2dCrosshairY != null)) {
	      g2.draw(this.m_l2dCrosshairX);
	      g2.draw(this.m_l2dCrosshairY);
	      if (this.dateStr != null) g2.drawString(this.dateStr, this.left, (int)this.m_l2dCrosshairY.getY2() - 3);
	    }

	    this.m_l2dCrosshairX = null;
	    this.m_l2dCrosshairY = null;
	    g2.dispose();
	  }

	  public void mouseClicked(MouseEvent e)
	  {
	    super.mouseClicked(e);
	    this.move = (!this.move);
	    mouseMoved(e);
	  }

	  public void mouseMoved(MouseEvent e) {
	    if (!this.move) return;
	    if ((getPopupMenu() != null) && (getPopupMenu().isShowing())) return;
	    int iX = e.getX(); int iY = e.getY();
	    this.m_iX = iX;
	    this.m_iY = iY;
	    this.m_dX = iX;
	    this.m_dY = iY;

	    drawRTCrosshair();
	  }

	  protected void drawRTCrosshair() {
	    Rectangle2D screenDataArea = getScreenDataArea(this.m_iX, this.m_iY);
	    if (screenDataArea == null) {
	      clearCrosshair(null);
	      return;
	    }

	    String result = null;
	    if (getInfo() != null)
	    {
	      EntityCollection entities = getChartRenderingInfo().getEntityCollection();
	      if (entities != null)
	      {
	        Insets insets = getInsets();
	        ChartEntity entity = entities.getEntity((int)((this.m_iX - insets.left) / getScaleX()), (int)((this.m_iY - insets.top) / getScaleY()));
	        if (entity != null) {
	          result = entity.getToolTipText();
	        }
	      }
	    }
	    Graphics2D g2 = (Graphics2D)getGraphics();
	    int iDAMaxX = (int)screenDataArea.getMaxX();
	    int iDAMinX = (int)screenDataArea.getMinX();

	    int iDAMaxY = getHeight() - 23;
	    int iDAMinY = 7;

	    Point2D p2dXY = translateScreenToJava2D(new Point(this.m_iX, this.m_iY));

	    XYPlot plot = (XYPlot)getChart().getPlot();
	    ValueAxis domainAxis = plot.getDomainAxis();
	    ValueAxis rangeAxis = plot.getRangeAxis();
	    RectangleEdge domainAxisEdge = plot.getDomainAxisEdge();
	    RectangleEdge rangeAxisEdge = plot.getRangeAxisEdge();
	    if ((plot instanceof CombinedDomainXYPlot)) {
	      CombinedDomainXYPlot combineddomainxyplot = (CombinedDomainXYPlot)plot;
	      plot = combineddomainxyplot.findSubplot(getChartRenderingInfo().getPlotInfo(), p2dXY);
	      if (plot != null) {
	        domainAxis = plot.getDomainAxis();
	        rangeAxis = plot.getRangeAxis();
	        domainAxisEdge = plot.getDomainAxisEdge();
	        rangeAxisEdge = plot.getRangeAxisEdge();
	      } else {
	        return;
	      }
	    }

	    double dXX = domainAxis.java2DToValue(this.m_dX, screenDataArea, domainAxisEdge);
	    double dYY = rangeAxis.java2DToValue(this.m_dY, screenDataArea, rangeAxisEdge);

	    String dt = sdf.format(new Date((long)dXX));
	    g2.setXORMode(new Color(16776960));
	    if ((this.m_l2dCrosshairX != null) && (this.m_l2dCrosshairY != null))
	    {
	      g2.draw(this.m_l2dCrosshairX);
	      g2.draw(this.m_l2dCrosshairY);

	      if (this.dateStr != null) g2.drawString(this.dateStr, iDAMinX + 3, (int)screenDataArea.getMaxY() - 3);
	    }
	    this.m_dXX = dXX;
	    this.m_dYY = dYY;
	    Line2D l2dX = new Line2D.Double(this.m_dX, iDAMinY, this.m_dX, iDAMaxY);

	    g2.draw(l2dX);
	    this.m_l2dCrosshairX = l2dX;
	    Line2D l2dY = new Line2D.Double(iDAMinX, this.m_dY, iDAMaxX, this.m_dY);

	    g2.draw(l2dY);
	    this.m_l2dCrosshairY = l2dY;
	    Ellipse2D ellipse = new Ellipse2D.Double(this.m_dX - 5.0D, this.m_dY - 5.0D, 10.0D, 10.0D);

	    this.m_ellipse = ellipse;

	    if (result != null) g2.drawString(result, iDAMinX + 3, (int)screenDataArea.getMaxY() - 3);
	    this.dateStr = result;
	    this.left = (iDAMinX + 3);
	    g2.dispose();
	  }

	  protected void drawRTInfo() {
	    Rectangle2D screenDataArea = getScreenDataArea(this.m_iX, this.m_iY);
	    if (screenDataArea == null) return;

	    Insets insets = getInsets();
	    Graphics2D g2 = (Graphics2D)getGraphics();
	    int iDAMaxX = (int)screenDataArea.getMaxX();
	    int iDAMinX = (int)screenDataArea.getMinX();
	    int iDAMaxY = (int)screenDataArea.getMaxY();
	    int iDAMinY = (int)screenDataArea.getMinY();
	    Point2D p2dXY = translateScreenToJava2D(new Point(this.m_iX, this.m_iY));

	    XYPlot plot = (XYPlot)getChart().getPlot();
	    ValueAxis domainAxis = plot.getDomainAxis();
	    ValueAxis rangeAxis = plot.getRangeAxis();
	    RectangleEdge domainAxisEdge = plot.getDomainAxisEdge();
	    RectangleEdge rangeAxisEdge = plot.getRangeAxisEdge();
	    if ((plot instanceof CombinedDomainXYPlot)) {
	      CombinedDomainXYPlot combineddomainxyplot = (CombinedDomainXYPlot)plot;
	      plot = combineddomainxyplot.findSubplot(getChartRenderingInfo().getPlotInfo(), p2dXY);
	      if (plot != null) {
	        domainAxis = plot.getDomainAxis();
	        rangeAxis = plot.getRangeAxis();
	        domainAxisEdge = plot.getDomainAxisEdge();
	        rangeAxisEdge = plot.getRangeAxisEdge();
	      } else {
	        return;
	      }
	    }

	    double dXX = domainAxis.java2DToValue(this.m_dX, screenDataArea, domainAxisEdge);
	    double dYY = rangeAxis.java2DToValue(this.m_dY, screenDataArea, rangeAxisEdge);
	    this.m_dXX = dXX;
	    this.m_dYY = dYY;

	    ArrayList alInfo = getInfo();
	    int iLenInfo = alInfo.size();

	    double dDiv = 10.0D;
	    FontMetrics fontMetrics = g2.getFontMetrics();
	    int iFontHgt = fontMetrics.getHeight();
	    double dDivX = Math.floor(iDAMaxX / dDiv);
	  }

	  protected ArrayList getInfo() {
	    DecimalFormat dfV = new DecimalFormat("#,###,###,##0");
	    String[] asT = { "X", "Y" };
	    int iLenT = asT.length;
	    ArrayList alV = new ArrayList();
	    String sV = "";

	    for (int i = iLenT - 1; i >= 0; i--) {
	      switch (i) {
	      case 0:
	        sV = getHMS();
	        break;
	      case 1:
	        sV = String.valueOf(dfV.format(this.m_dYY));
	      }

	      alV.add(asT[i] + "|" + sV);
	    }
	    return alV;
	  }

	  protected String getHMS() {
	    DecimalFormat dfT = new DecimalFormat("00");
	    GregorianCalendar gc = new GregorianCalendar();
	    long lDte = (long)this.m_dXX;
	    Date dtXX = new Date(lDte);
	    gc.setTime(dtXX);
	    String sHH = dfT.format(Double.valueOf(String.valueOf(gc.get(5))));
	    String sMM = dfT.format(Double.valueOf(String.valueOf(1 + gc.get(2))));
	    String sSS = dfT.format(Double.valueOf(String.valueOf(gc.get(1))));
	    String sV = sHH + ":" + sMM + ":" + sSS;
	    return sV;
	  }
}
