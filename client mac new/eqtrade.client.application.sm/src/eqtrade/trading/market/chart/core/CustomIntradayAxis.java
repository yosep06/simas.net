package eqtrade.trading.market.chart.core;

import org.jfree.chart.axis.DateAxis;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTick;
import org.jfree.chart.axis.SegmentedTimeline;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.TextAnchor;

public class CustomIntradayAxis extends DateAxis{
	 public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	  public static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HHmmss");
	  public static final SimpleDateFormat fridayFormat = new SimpleDateFormat("E");
	  private static final long serialVersionUID = 4239630992518135526L;
	  private Date base;

	  public CustomIntradayAxis(String s, Date dt, SegmentedTimeline tl)
	  {
	    super(s);
	    this.base = dt;
	    try {
	      setTimeline(tl);
	      setAutoRange(true);
	      setRange(timeFormat.parse(dateFormat.format(new Date()) + " 085500"), timeFormat.parse(dateFormat.format(new Date()) + " 161500")); } catch (Exception localException) {
	    }
	  }

	  public List refreshTicks(Graphics2D graphics2d, AxisState axisstate, Rectangle2D rectangle2d, RectangleEdge rectangleedge) {
	    ArrayList arraylist = new ArrayList();
	    try {
	      Date start = timeFormat.parse(dateFormat.format(this.base) + " 085500");
	      arraylist.add(new DateTick(start, "08:55", TextAnchor.TOP_LEFT, TextAnchor.CENTER_LEFT, 0.0D));
	      if (fridayFormat.format(this.base).toUpperCase().equals("FRI")) {
	        Date middle = timeFormat.parse(dateFormat.format(this.base) + " 140000");
	        arraylist.add(new DateTick(middle, "14:00", TextAnchor.TOP_CENTER, TextAnchor.CENTER, 0.0D));
	      } else {
	        Date middle = timeFormat.parse(dateFormat.format(this.base) + " 133000");
	        arraylist.add(new DateTick(middle, "13:30", TextAnchor.TOP_CENTER, TextAnchor.CENTER, 0.0D));
	      }
	      Date end = timeFormat.parse(dateFormat.format(this.base) + " 161500");
	      arraylist.add(new DateTick(end, "16:15", TextAnchor.TOP_RIGHT, TextAnchor.CENTER_RIGHT, 0.0D));
	    } catch (Exception ex) {
	      ex.printStackTrace();
	    }
	    return arraylist;
	  }
}
