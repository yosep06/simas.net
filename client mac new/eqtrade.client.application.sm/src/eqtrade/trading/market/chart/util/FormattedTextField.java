package eqtrade.trading.market.chart.util;


import javax.swing.JFormattedTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DocumentFilter;
import javax.swing.text.DocumentFilter.FilterBypass;

public class FormattedTextField extends JFormattedTextField
{
  protected static final long serialVersionUID = 2300201443162067458L;

  public FormattedTextField()
  {
    super(new FormattedTextComp());
  }

  protected static final class FormattedTextComp extends DefaultFormatter {
    protected static final long serialVersionUID = -1783727128591162531L;

    protected DocumentFilter getDocumentFilter() {
      return new FormattedTextField.UpperCaseFilter();
    }
  }

  protected static final class UpperCaseFilter extends DocumentFilter
  {
    public void insertString(DocumentFilter.FilterBypass fb, int offset, String string, AttributeSet attr)
      throws BadLocationException
    {
      super.insertString(fb, offset, string.toUpperCase(), attr);
    }

    public void replace(DocumentFilter.FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
      throws BadLocationException
    {
      super.replace(fb, offset, length, text.toUpperCase(), attrs);
    }
  }
}