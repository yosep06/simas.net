package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JNumber;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JLabel;

public class ULTOSCPlugin extends ChartIndicatorPlugin{
	  private ULTOSCIndicator ultosc = null;
	  private JText color;
	  private JNumber shortPeriod;
	  private JNumber mediumPeriod;
	  private JNumber longPeriod;

	  public ULTOSCPlugin(IChartController controller)
	  {
	    super(controller, "Ultimate Oscillator - ULTOSC");
	    this.ultosc = new ULTOSCIndicator(this.controller, "ULTOSC", "");
	  }

	  protected void build() {
	    super.build();
	    this.color = ChartUtil.getFieldColor();
	    this.shortPeriod = ChartUtil.getFieldNumber(0, 0);
	    this.mediumPeriod = ChartUtil.getFieldNumber(0, 0);
	    this.longPeriod = ChartUtil.getFieldNumber(0, 0);
	    this.color.setBackground(ULTOSCIndicator.DEFAULT_COLOR);
	    this.shortPeriod.setValue(new Double(7.0D));
	    this.mediumPeriod.setValue(new Double(14.0D));
	    this.longPeriod.setValue(new Double(28.0D));
	    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, this.panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("Color:"), cc.xy(1, 1));
	    builder.add(this.color, cc.xy(3, 1));
	    builder.add(new JLabel("Short Period:"), cc.xy(1, 3));
	    builder.add(this.shortPeriod, cc.xy(3, 3));
	    builder.add(new JLabel("Medium Period:"), cc.xy(1, 5));
	    builder.add(this.mediumPeriod, cc.xy(3, 5));
	    builder.add(new JLabel("Long Period:"), cc.xy(1, 7));
	    builder.add(this.longPeriod, cc.xy(3, 7));
	  }
	  public void show() {
	    this.ultosc.show();
	  }
	  public void hide() {
	    this.ultosc.hide();
	  }
	  public void init() {
	    this.color.setBackground(this.ultosc.getColor());
	    this.shortPeriod.setValue(new Double(this.ultosc.getShortPeriod()));
	    this.mediumPeriod.setValue(new Double(this.ultosc.getMediumPeriod()));
	    this.longPeriod.setValue(new Double(this.ultosc.getLongPeriod()));
	  }
	  public void update() {
	    this.ultosc.setColor(this.color.getBackground());
	    this.ultosc.setShortPeriod(((Double)this.shortPeriod.getValue()).intValue());
	    this.ultosc.setMediumPeriod(((Double)this.mediumPeriod.getValue()).intValue());
	    this.ultosc.setLongPeriod(((Double)this.longPeriod.getValue()).intValue());
	  }
}
