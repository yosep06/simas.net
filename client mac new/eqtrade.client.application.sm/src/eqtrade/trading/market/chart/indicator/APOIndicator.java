package eqtrade.trading.market.chart.indicator;


import com.tictactec.ta.lib.Core;
import com.tictactec.ta.lib.MInteger;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;

import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class APOIndicator extends ChartIndicator{
	public static final Color DEFAULT_COLOR = Color.yellow;
	  public static final int DEFAULT_FAST_PERIOD = 3;
	  public static final int DEFAULT_SLOW_PERIOD = 10;
	  public static final int DEFAULT_MA_TYPE = 0;
	  private Color color = DEFAULT_COLOR;
	  private int fastPeriod = 3;
	  private int slowPeriod = 10;
	  private int maType = 0;

	  public APOIndicator(IChartController controller, String name, String securities)
	  {
	    super(controller, "BOTTOM2", name, "line", securities);
	    build();
	  }

	  protected void build() {
	    render = new XYLineAndShapeRenderer(true, false);
	    render.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    render.setSeriesPaint(0, color);
	    render.setSeriesStroke(0, new BasicStroke(1.65F));
	  }

	  public void calculate() {
	    double[] inReal = controller.getDataset().getCloseArray();
	    int startIdx = 0;
	    int endIdx = inReal.length - 1;
	    MInteger outBegIdx = new MInteger();
	    MInteger outNbElement = new MInteger();

	    double[] outReal = getOutputArray(inReal, controller.getTalibCore().apoLookback(fastPeriod, slowPeriod, getTA_MAType(maType)));
	    controller.getTalibCore().apo(startIdx, endIdx, inReal, fastPeriod, slowPeriod, getTA_MAType(maType), outBegIdx, outNbElement, outReal);

	    if (outReal != null) {
	      TimeSeries timeseries = new TimeSeries(name, "", "", Minute.class);
	      int count2 = controller.getDataset().getDateArray().length;
	      int count = outReal.length;
	      for (int i = count; i > 0; i--) {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(controller.getDataset().getDateArray()[(count2 - 1)])), outReal[(i - 1)]);
	        count2--;
	      }
	      xydataset = new TimeSeriesCollection(timeseries);
	    }
	  }

	  public void show() {
	    render.setSeriesPaint(0, color);
	    calculate();
	    controller.getDataset().addListener(this);
	    controller.getContainer().addPlot(position, name, xydataset, null, render);
	  }

	  public void hide() {
	    controller.getDataset().removeListener(this);
	    controller.getContainer().removePlot(position, name);
	  }

	  public void chartChanged()
	  {
	    ((TimeSeriesCollection)xydataset).removeAllSeries();
	    calculate();
	    controller.getContainer().updateDataset(position, name, xydataset);
	  }

	  public Color getColor() {
	    return color;
	  }

	  public void setColor(Color color) {
	    this.color = color;
	  }

	  public int getFastPeriod() {
	    return fastPeriod;
	  }

	  public void setFastPeriod(int fastPeriod) {
	    this.fastPeriod = fastPeriod;
	  }

	  public int getMaType() {
	    return maType;
	  }

	  public void setMaType(int maType) {
	    this.maType = maType;
	  }

	  public int getSlowPeriod() {
	    return slowPeriod;
	  }

	  public void setSlowPeriod(int slowPeriod) {
	    this.slowPeriod = slowPeriod;
	  }
}
