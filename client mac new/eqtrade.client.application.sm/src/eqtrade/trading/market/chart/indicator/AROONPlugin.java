package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JNumber;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JLabel;



public class AROONPlugin extends ChartIndicatorPlugin{

	  private AROONIndicator aroon = null;
	  private JText color;
	  private JText color2;
	  private JNumber period;

	  public AROONPlugin(IChartController controller)
	  {
	    super(controller, "Aroon - AROON");
	    aroon = new AROONIndicator(controller, "AROON", "");
	  }

	  protected void build() {
	    super.build();
	    color = ChartUtil.getFieldColor();
	    color2 = ChartUtil.getFieldColor();
	    period = ChartUtil.getFieldNumber(0, 0);
	    color.setBackground(AROONIndicator.DEFAULT_COLOR);
	    color2.setBackground(AROONIndicator.DEFAULT_COLOR2);
	    period.setValue(new Double(7.0D));
	    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("Color Up:"), cc.xy(1, 1));
	    builder.add(color, cc.xy(3, 1));
	    builder.add(new JLabel("Color Down:"), cc.xy(1, 3));
	    builder.add(color2, cc.xy(3, 3));
	    builder.add(new JLabel("Medium Period:"), cc.xy(1, 5));
	    builder.add(period, cc.xy(3, 5));
	  }
	  public void show() {
	    aroon.show();
	  }
	  public void hide() {
	    aroon.hide();
	  }
	  public void init() {
	    color.setBackground(aroon.getColor());
	    color2.setBackground(aroon.getColor2());
	    period.setValue(new Double(aroon.getPeriod()));
	  }
	  public void update() {
	    aroon.setColor(color.getBackground());
	    aroon.setColor2(color2.getBackground());
	    aroon.setPeriod(((Double)period.getValue()).intValue());
	  }
}
