package eqtrade.trading.market.chart.indicator;

import com.tictactec.ta.lib.MAType;
import com.tictactec.ta.lib.MInteger;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;

import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class MACDIndicator extends ChartIndicator{
	 public static final Color DEFAULT_COLOR = Color.green.darker();
	  public static final Color DEFAULT_SIGNAL_COLOR = Color.red.darker();
	  public static final int DEFAULT_FAST_PERIOD = 12;
	  public static final int DEFAULT_FAST_MA_TYPE = 1;
	  public static final int DEFAULT_SLOW_PERIOD = 26;
	  public static final int DEFAULT_SLOW_MA_TYPE = 1;
	  public static final int DEFAULT_SIGNAL_PERIOD = 9;
	  public static final int DEFAULT_SIGNAL_MA_TYPE = 1;
	  private Color color = DEFAULT_COLOR;
	  private Color signalColor = DEFAULT_SIGNAL_COLOR;
	  private int fastPeriod = 12;
	  private int fastMaType = 1;
	  private int slowPeriod = 26;
	  private int slowMaType = 1;
	  private int signalPeriod = 9;
	  private int signalMaType = 1;
	  protected XYDataset xydataset2;
	  protected XYItemRenderer render2;
	  protected XYDataset xydataset3;
	  protected XYItemRenderer render3;

	  public MACDIndicator(IChartController controller, String name, String securities)
	  {
	    super(controller, "BOTTOM2", name, "line", securities);
	    build();
	  }

	  protected void build() {
	    render = new XYLineAndShapeRenderer(true, false);
	    render.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    render.setSeriesPaint(0, color);
	    render.setSeriesStroke(0, new BasicStroke(1.65F));

	    render2 = new XYLineAndShapeRenderer(true, false);
	    render2.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    render2.setSeriesPaint(0, signalColor);
	    render2.setSeriesStroke(0, new BasicStroke(1.65F, 1, 1, 1.0F, new float[] { 4.0F, 4.0F }, 0.0F));
	    render3 = new XYBarRenderer();
	    ((XYBarRenderer)render3).setDrawBarOutline(true);
	    render3.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	  }

	  public void calculate()
	  {
	    double[] inReal = controller.getDataset().getCloseArray();
	    int startIdx = 0;
	    int endIdx = inReal.length - 1;
	    MInteger outBegIdx = new MInteger();
	    MInteger outNbElement = new MInteger();
	    MAType fastMA = getTA_MAType(fastMaType);
	    MAType slowMA = getTA_MAType(slowMaType);
	    MAType signalMA = getTA_MAType(signalMaType);

	    double[] outMACD = getOutputArray(inReal, controller.getTalibCore().macdExtLookback(fastPeriod, fastMA, slowPeriod, slowMA, signalPeriod, signalMA));
	    double[] outSignal = getOutputArray(inReal, controller.getTalibCore().macdExtLookback(fastPeriod, fastMA, slowPeriod, slowMA, signalPeriod, signalMA));
	    double[] outHist = getOutputArray(inReal, controller.getTalibCore().macdExtLookback(fastPeriod, fastMA, slowPeriod, slowMA, signalPeriod, signalMA));

	    controller.getTalibCore().macdExt(startIdx, endIdx, inReal, fastPeriod, fastMA, slowPeriod, slowMA, signalPeriod, signalMA, outBegIdx, outNbElement, outMACD, outSignal, outHist);

	    if (outMACD != null) {
	      TimeSeries timeseries = new TimeSeries(name);
	      int count2 = controller.getDataset().getDateArray().length;
	      int count = outMACD.length;
	      for (int i = count; i > 0; i--) {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(controller.getDataset().getDateArray()[(count2 - 1)])), outMACD[(i - 1)]);
	        count2--;
	      }
	      xydataset = new TimeSeriesCollection(timeseries);
	    }

	    if (outSignal != null) {
	      TimeSeries timeseries = new TimeSeries(name + "-Signal");
	      int count2 = controller.getDataset().getDateArray().length;
	      int count = outSignal.length;
	      for (int i = count; i > 0; i--) {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(controller.getDataset().getDateArray()[(count2 - 1)])), outSignal[(i - 1)]);
	        count2--;
	      }
	      xydataset2 = new TimeSeriesCollection(timeseries);
	    }

	    if (outHist != null) {
	      TimeSeries timeseries = new TimeSeries(name + "-Hist");
	      int count2 = controller.getDataset().getDateArray().length;
	      int count = outHist.length;
	      for (int i = count; i > 0; i--) {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(controller.getDataset().getDateArray()[(count2 - 1)])), outHist[(i - 1)]);
	        count2--;
	      }
	      xydataset3 = new TimeSeriesCollection(timeseries);
	    }
	  }

	  public void show() {
	    render.setSeriesPaint(0, color);
	    render2.setSeriesPaint(0, signalColor);
	    calculate();
	    controller.getDataset().addListener(this);
	    controller.getContainer().addPlot(position, name, xydataset, null, render);
	    controller.getContainer().addPlot(position, name + "-Signal", xydataset2, null, render2);
	    controller.getContainer().addPlot(position, name + "-Hist", xydataset3, null, render3);
	  }

	  public void hide() {
	    controller.getDataset().removeListener(this);
	    controller.getContainer().removePlot(position, name);
	    controller.getContainer().removePlot(position, name + "-Signal");
	    controller.getContainer().removePlot(position, name + "-Hist");
	  }

	  public void chartChanged()
	  {
	    ((TimeSeriesCollection)xydataset).removeAllSeries();
	    ((TimeSeriesCollection)xydataset2).removeAllSeries();
	    calculate();
	    controller.getContainer().updateDataset(position, name, xydataset);
	    controller.getContainer().updateDataset(position, name + "-Signal", xydataset2);
	    controller.getContainer().updateDataset(position, name + "-Hist", xydataset3);
	  }

	  public Color getColor() {
	    return color;
	  }

	  public void setColor(Color color) {
	    color = color;
	  }

	  public int getFastMaType() {
	    return fastMaType;
	  }

	  public void setFastMaType(int fastMaType) {
	    fastMaType = fastMaType;
	  }

	  public int getFastPeriod() {
	    return fastPeriod;
	  }

	  public void setFastPeriod(int fastPeriod) {
	    fastPeriod = fastPeriod;
	  }

	  public Color getSignalColor() {
	    return signalColor;
	  }

	  public void setSignalColor(Color signalColor) {
	    signalColor = signalColor;
	  }

	  public int getSignalMaType() {
	    return signalMaType;
	  }

	  public void setSignalMaType(int signalMaType) {
	    signalMaType = signalMaType;
	  }

	  public int getSignalPeriod() {
	    return signalPeriod;
	  }

	  public void setSignalPeriod(int signalPeriod) {
	    signalPeriod = signalPeriod;
	  }

	  public int getSlowMaType() {
	    return slowMaType;
	  }

	  public void setSlowMaType(int slowMaType) {
	    slowMaType = slowMaType;
	  }

	  public int getSlowPeriod() {
	    return slowPeriod;
	  }

	  public void setSlowPeriod(int slowPeriod) {
	    slowPeriod = slowPeriod;
	  }
}
