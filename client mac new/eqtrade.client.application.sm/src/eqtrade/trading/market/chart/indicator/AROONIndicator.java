package eqtrade.trading.market.chart.indicator;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;

import com.tictactec.ta.lib.MInteger;
import java.awt.BasicStroke;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Minute;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class AROONIndicator extends ChartIndicator{
	 public static final Color DEFAULT_COLOR = Color.green;
	  public static final Color DEFAULT_COLOR2 = Color.green.darker().darker();
	  public static final int DEFAULT_PERIOD = 14;
	  private Color color = DEFAULT_COLOR;
	  private Color color2 = DEFAULT_COLOR2;
	  private int period = 14;
	  protected XYDataset xydataset2;
	  protected XYItemRenderer render2;

	  public AROONIndicator(IChartController controller, String name, String securities)
	  {
	    super(controller, "TOP", name, "line", securities);
	    build();
	  }

	  protected void build() {
	    this.render = new XYLineAndShapeRenderer(true, false);
	    this.render.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    this.render.setSeriesPaint(0, this.color);
	    this.render.setSeriesStroke(0, new BasicStroke(1.65F));
	    this.render2 = new XYLineAndShapeRenderer(true, false);
	    this.render2.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	    this.render2.setSeriesPaint(0, this.color2);
	    this.render2.setSeriesStroke(0, new BasicStroke(1.65F));
	  }

	  public void calculate() {
	    double[] inReal = this.controller.getDataset().getCloseArray();
	    int startIdx = 0;
	    int endIdx = inReal.length - 1;
	    MInteger outBegIdx = new MInteger();
	    MInteger outNbElement = new MInteger();
	    double[] inHigh = this.controller.getDataset().getHighArray();
	    double[] inLow = this.controller.getDataset().getLowArray();

	    double[] outDown = getOutputArray(inReal, this.controller.getTalibCore().aroonLookback(this.period));
	    double[] outUp = getOutputArray(inReal, this.controller.getTalibCore().aroonLookback(this.period));
	    this.controller.getTalibCore().aroon(startIdx, endIdx, inHigh, inLow, this.period, outBegIdx, outNbElement, outDown, outUp);

	    if (outUp != null) {
	      TimeSeries timeseries = new TimeSeries(this.name + "UP", "", "", Minute.class);
	      int count2 = this.controller.getDataset().getDateArray().length;
	      int count = outUp.length;
	      for (int i = count; i > 0; i--) {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.controller.getDataset().getDateArray()[(count2 - 1)])), outUp[(i - 1)]);
	        count2--;
	      }
	      this.xydataset = new TimeSeriesCollection(timeseries);
	    }
	    if (outDown != null) {
	      TimeSeries timeseries = new TimeSeries(this.name + "-DOWN", "", "", Minute.class);
	      int count2 = this.controller.getDataset().getDateArray().length;
	      int count = outDown.length;
	      for (int i = count; i > 0; i--) {
	        timeseries.add(Minute.parseMinute(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.controller.getDataset().getDateArray()[(count2 - 1)])), outDown[(i - 1)]);
	        count2--;
	      }
	      this.xydataset2 = new TimeSeriesCollection(timeseries);
	    }
	  }

	  public void show() {
	    this.render.setSeriesPaint(0, this.color);
	    this.render2.setSeriesPaint(0, this.color2);
	    calculate();
	    this.controller.getDataset().addListener(this);
	    this.controller.getContainer().addPlot(this.position, this.name + "-UP", this.xydataset, null, this.render);
	    this.controller.getContainer().addPlot(this.position, this.name + "-DOWN", this.xydataset2, null, this.render2);
	  }

	  public void hide() {
	    this.controller.getDataset().removeListener(this);
	    this.controller.getContainer().removePlot(this.position, this.name + "-UP");
	    this.controller.getContainer().removePlot(this.position, this.name + "-DOWN");
	  }

	  public void chartChanged()
	  {
	    ((TimeSeriesCollection)this.xydataset).removeAllSeries();
	    ((TimeSeriesCollection)this.xydataset2).removeAllSeries();
	    calculate();
	    this.controller.getContainer().updateDataset(this.position + "-UP", this.name, this.xydataset);
	    this.controller.getContainer().updateDataset(this.position + "-DOWN", this.name, this.xydataset);
	  }

	  public Color getColor() {
	    return this.color;
	  }

	  public void setColor(Color color) {
	    this.color = color;
	  }

	  public int getPeriod() {
	    return this.period;
	  }

	  public void setPeriod(int period) {
	    this.period = period;
	  }

	  public Color getColor2() {
	    return this.color2;
	  }

	  public void setColor2(Color color2) {
	    this.color2 = color2;
	  }
}
