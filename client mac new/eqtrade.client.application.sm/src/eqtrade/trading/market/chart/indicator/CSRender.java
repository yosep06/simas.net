package eqtrade.trading.market.chart.indicator;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Line2D;
import java.awt.geom.Line2D.Double;
import java.awt.geom.Rectangle2D;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.EntityCollection;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.urls.XYURLGenerator;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.OHLCDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;
public class CSRender extends CandlestickRenderer{
	private static final long serialVersionUID = -6520756737428416110L;

	  public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea, PlotRenderingInfo info, XYPlot plot, ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset, int series, int item, CrosshairState crosshairState, int pass)
	  {
	    ValueAxis axis = plot.getDomainAxis();
	    double x1 = axis.getLowerBound();
	    double x2 = x1 + getMaxCandleWidthInMilliseconds();
	    RectangleEdge edge2 = plot.getDomainAxisEdge();
	    double xx1 = axis.valueToJava2D(x1, dataArea, edge2);
	    double xx2 = axis.valueToJava2D(x2, dataArea, edge2);
	    double maxCandleWidth = Math.abs(xx2 - xx1);

	    PlotOrientation orientation = plot.getOrientation();
	    boolean horiz;
	    if (orientation == PlotOrientation.HORIZONTAL) {
	      horiz = true;
	    }
	    else
	    {
	      // horiz=false;
	      if (orientation == PlotOrientation.VERTICAL)
	        horiz = false;
	      else
	        return;
	    }
	    // horiz=true;
	    EntityCollection entities = null;
	    if (info != null)
	      entities = info.getOwner().getEntityCollection();
	    OHLCDataset highLowData = (OHLCDataset)dataset;
	    double x = highLowData.getXValue(series, item);
	    double yHigh = highLowData.getHighValue(series, item);
	    double yLow = highLowData.getLowValue(series, item);
	    double yOpen = highLowData.getOpenValue(series, item);
	    double yClose = highLowData.getCloseValue(series, item);
	    RectangleEdge domainEdge = plot.getDomainAxisEdge();
	    double xx = domainAxis.valueToJava2D(x, dataArea, domainEdge);
	    RectangleEdge edge = plot.getRangeAxisEdge();
	    double yyHigh = rangeAxis.valueToJava2D(yHigh, dataArea, edge);
	    double yyLow = rangeAxis.valueToJava2D(yLow, dataArea, edge);
	    double yyOpen = rangeAxis.valueToJava2D(yOpen, dataArea, edge);
	    double yyClose = rangeAxis.valueToJava2D(yClose, dataArea, edge);
	    double stickWidth;
	    double volumeWidth;
	    // stickWidth;
	    if (getCandleWidth() > 0.0D)
	    {
	       volumeWidth = getCandleWidth();
	      stickWidth = getCandleWidth();
	    }
	    else {
	      double xxWidth = 0.0D;
	      int itemCount = 0;
	      double lastPos = 0;
	      int i = 0;
	      switch (getAutoWidthMethod())
	      {
	      default:
	        break;
	      case 0:
	         itemCount = highLowData.getItemCount(series);
	        if (horiz)
	          xxWidth = dataArea.getHeight() / itemCount;
	        else
	          xxWidth = dataArea.getWidth() / itemCount;
	        break;
	      case 1:
	        itemCount = highLowData.getItemCount(series);
	        lastPos = -1.0D;
	        xxWidth = dataArea.getWidth();
	        i = 0;
	      case 2:
	        while (i < itemCount)
	        {
	          double pos = domainAxis.valueToJava2D(highLowData.getXValue(series, i), dataArea, domainEdge);
	          if (lastPos != -1.0D)
	            xxWidth = Math.min(xxWidth, Math.abs(pos - lastPos));
	          lastPos = pos;
	          i++;
	          continue;
	        }
	      }

	      xxWidth = xxWidth - 
	        2.0D * getAutoWidthGap();
	      xxWidth *= getAutoWidthFactor();
	      xxWidth = Math.min(xxWidth, maxCandleWidth);
	      volumeWidth = Math.max(Math.min(1.0D, maxCandleWidth), xxWidth);
	      stickWidth = Math.max(Math.min(3.0D, maxCandleWidth), xxWidth);
	    }
	    Paint p = getItemPaint(series, item);
	    if (yClose > yOpen)
	      p = getUpPaint();
	    else {
	      p = getDownPaint();
	    }
	    Stroke s = getItemStroke(series, item);
	    g2.setStroke(s);

	    double maxVolume2 = 0.0D;
	    OHLCDataset highLowDataset = (OHLCDataset)dataset;
	    maxVolume2 = 0.0D;
	    for (int cc = 0; cc < highLowDataset.getSeriesCount(); cc++) {
	      for (int bb = 0; bb < highLowDataset.getItemCount(cc); bb++) {
	        double volume = highLowDataset.getVolumeValue(cc, bb);
	        if (volume > maxVolume2) {
	          maxVolume2 = volume;
	        }
	      }

	    }

	    int volume = (int)highLowData.getVolumeValue(series, item);
	    double volumeHeight = volume / (maxVolume2 * 2.0D);
	    double max;
	    double min;
	    //double max;
	    if (horiz)
	    {
	       min = dataArea.getMinX();
	      max = dataArea.getMaxX();
	    }
	    else {
	      min = dataArea.getMinY();
	      max = dataArea.getMaxY();
	    }
	    double zzVolume = volumeHeight * (max - min);
	    g2.setPaint(Color.gray);
	    Composite originalComposite = g2.getComposite();
	    g2.setComposite(AlphaComposite.getInstance(3, 0.3F));
	    if (horiz)
	      g2.fill(new Rectangle2D.Double(min, xx - volumeWidth / 2.0D, zzVolume, volumeWidth));
	    else
	      g2.fill(new Rectangle2D.Double(xx - volumeWidth / 2.0D, max - zzVolume, volumeWidth, zzVolume));
	    g2.setComposite(originalComposite);

	    g2.setPaint(p);
	    double yyMaxOpenClose = Math.max(yyOpen, yyClose);
	    double yyMinOpenClose = Math.min(yyOpen, yyClose);
	    double maxOpenClose = Math.max(yOpen, yClose);
	    double minOpenClose = Math.min(yOpen, yClose);
	    if (yHigh > maxOpenClose)
	      if (horiz)
	        g2.draw(new Line2D.Double(yyHigh, xx, yyMaxOpenClose, xx));
	      else
	        g2.draw(new Line2D.Double(xx, yyHigh, xx, yyMaxOpenClose));
	    if (yLow < minOpenClose)
	      if (horiz)
	        g2.draw(new Line2D.Double(yyLow, xx, yyMinOpenClose, xx));
	      else
	        g2.draw(new Line2D.Double(xx, yyLow, xx, yyMinOpenClose));
	    Shape body = null;
	    if (horiz)
	      body = new Rectangle2D.Double(yyMinOpenClose, xx - stickWidth / 2.0D, yyMaxOpenClose - yyMinOpenClose, stickWidth);
	    else
	      body = new Rectangle2D.Double(xx - stickWidth / 2.0D, yyMinOpenClose, stickWidth, yyMaxOpenClose - yyMinOpenClose);
	    if (yClose > yOpen)
	    {
	      if (getUpPaint() != null)
	      {
	        g2.setPaint(getUpPaint());
	        g2.fill(body);
	      }
	    }
	    else {
	      if (getDownPaint() != null)
	        g2.setPaint(getDownPaint());
	      g2.fill(body);
	    }
	    g2.setPaint(p);
	    g2.draw(body);
	    if (entities != null)
	    {
	      String tip = null;
	      XYToolTipGenerator generator = getToolTipGenerator(series, item);
	      if (generator != null)
	        tip = generator.generateToolTip(dataset, series, item);
	      String url = null;
	      if (getURLGenerator() != null)
	        url = getURLGenerator().generateURL(dataset, series, item);
	      XYItemEntity entity = new XYItemEntity(body, dataset, series, item, tip, url);
	      entities.add(entity);
	    }
	  }
}
