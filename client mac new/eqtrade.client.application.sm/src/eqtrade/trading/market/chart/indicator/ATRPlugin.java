package eqtrade.trading.market.chart.indicator;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.ui.JNumber;
import com.vollux.ui.JText;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;
import eqtrade.trading.market.chart.util.ChartUtil;

import javax.swing.JLabel;

public class ATRPlugin extends ChartIndicatorPlugin{
	
	 private ATRIndicator atr = null;
	  private JText color;
	  private JNumber period;

	  public ATRPlugin(IChartController controller)
	  {
	    super(controller, "Average True Range - ATR");
	    this.atr = new ATRIndicator(this.controller, "ATR", "");
	  }

	  protected void build() {
	    super.build();
	    this.color = ChartUtil.getFieldColor();
	    this.period = ChartUtil.getFieldNumber(0, 0);
	    this.color.setBackground(ATRIndicator.DEFAULT_COLOR);
	    this.period.setValue(new Double(14.0D));
	    FormLayout layout = new FormLayout("pref, 2dlu, 100dlu, 2dlu", "pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
	    PanelBuilder builder = new PanelBuilder(layout, this.panel);
	    CellConstraints cc = new CellConstraints();
	    builder.add(new JLabel("Color:"), cc.xy(1, 1));
	    builder.add(this.color, cc.xy(3, 1));
	    builder.add(new JLabel("Medium Period:"), cc.xy(1, 3));
	    builder.add(this.period, cc.xy(3, 3));
	  }
	  public void show() {
	    this.atr.show();
	  }
	  public void hide() {
	    this.atr.hide();
	  }
	  public void init() {
	    this.color.setBackground(this.atr.getColor());
	    this.period.setValue(new Double(this.atr.getPeriod()));
	  }
	  public void update() {
	    this.atr.setColor(this.color.getBackground());
	    this.atr.setPeriod(((Double)this.period.getValue()).intValue());
	  }
}
