package eqtrade.trading.market.chart.indicator;

import java.awt.BasicStroke;
import java.awt.Color;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;

public class CandleIndicator extends ChartIndicator{
	 public static final Color DEFAULT_COLOR_UP = Color.green.darker();
	  public static final Color DEFAULT_COLOR_DOWN = Color.red.darker();

	  private Color colorUp = DEFAULT_COLOR_UP;
	  private Color colorDown = DEFAULT_COLOR_DOWN;

	  public CandleIndicator(IChartController controller, String name, String securities) {
	    super(controller, "MIDDLE", name, "line", securities);
	    build();
	  }

	  protected void build() {
	    this.render = new CSRender();

	    ((CandlestickRenderer)this.render).setDrawVolume(false);
	    ((CandlestickRenderer)this.render).setDownPaint(this.colorDown);
	    ((CandlestickRenderer)this.render).setUpPaint(this.colorUp);
	    this.render.setSeriesStroke(0, new BasicStroke(1.65F));
	  }

	  public void calculate() {
	    this.xydataset = this.controller.getDataset().getOHLCDataset();
	  }

	  public void show() {
	    ((CandlestickRenderer)this.render).setDownPaint(this.colorDown);
	    ((CandlestickRenderer)this.render).setUpPaint(this.colorUp);
	    calculate();
	    this.controller.getDataset().addListener(this);
	    this.controller.getContainer().addPlot(this.position, this.name + "CDL", this.xydataset, null, this.render);
	  }

	  public void hide() {
	    this.controller.getDataset().removeListener(this);
	    this.controller.getContainer().removePlot(this.position, this.name + "CDL");
	  }

	  public void chartChanged()
	  {
	    this.controller.getContainer().removePlot(this.position, this.name + "CDL");
	    calculate();
	    this.controller.getContainer().addPlot(this.position, this.name + "CDL", this.xydataset, null, this.render);
	  }

	  public Color getColorDown() {
	    return this.colorDown;
	  }

	  public void setColorDown(Color colorDown) {
	    this.colorDown = colorDown;
	  }

	  public Color getColorUp() {
	    return this.colorUp;
	  }

	  public void setColorUp(Color colorUp) {
	    this.colorUp = colorUp;
	  }
}
