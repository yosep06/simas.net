package eqtrade.trading.market.chart.indicator;

import eqtrade.trading.market.chart.core.ChartIndicator;
import eqtrade.trading.market.chart.ext.IChartController;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.renderer.xy.XYBarRenderer;

public class VolumeIndicator extends ChartIndicator{
	public VolumeIndicator(IChartController controller, String name, String securities)
	  {
	    super(controller, "BOTTOM", name, "bar", securities);
	    build();
	  }

	  protected void build() {
	    this.render = new XYBarRenderer(0.0D);
	    this.render.setSeriesPaint(0, Color.cyan);
	    ((XYBarRenderer)this.render).setDrawBarOutline(true);
	    ((XYBarRenderer)this.render).setSeriesOutlinePaint(0, Color.cyan.darker());
	    this.render.setBaseToolTipGenerator(new StandardXYToolTipGenerator("{0}: ({1}, {2})", new SimpleDateFormat("d-MMM-yyyy HH:mm"), new DecimalFormat("0.00")));
	  }

	  public void calculate() {
	    this.xydataset = this.controller.getDataset().getVolDataset();
	  }

	  public void show() {
	    calculate();
	    this.controller.getDataset().addListener(this);
	    this.controller.getContainer().addPlot(this.position, this.name, this.xydataset, null, this.render);
	  }

	  public void hide() {
	    this.controller.getDataset().removeListener(this);
	    this.controller.getContainer().removePlot(this.position, this.name);
	  }

	  public void chartChanged()
	  {
	    calculate();
	    this.controller.getContainer().updateDataset(this.position, this.name, this.xydataset);
	  }
}
