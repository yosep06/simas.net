package eqtrade.trading.market.chart.indicator;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBoxMenuItem;

import eqtrade.trading.market.chart.core.ChartIndicatorPlugin;
import eqtrade.trading.market.chart.ext.IChartController;

public class VolumePlugin extends ChartIndicatorPlugin{
	private VolumeIndicator vol = null;
	  private boolean show = false;

	  public VolumePlugin(IChartController controller) {
	    super(controller, "Volume");
	    this.vol = new VolumeIndicator(this.controller, "VOLUME", "");
	    this.menu.doClick();
	  }

	  protected void build() {
	    this.menu = new JCheckBoxMenuItem(this.name);
	    this.menu.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent arg0) {
	        if (!VolumePlugin.this.show)
	          VolumePlugin.this.vol.show();
	        else {
	          VolumePlugin.this.vol.hide();
	        }
	        VolumePlugin.this.show = (!VolumePlugin.this.show);
	        VolumePlugin.this.menu.setSelected(VolumePlugin.this.show);
	      }
	    });
	  }
}
