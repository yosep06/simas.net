package eqtrade.trading.market.chart.ext;

import com.tictactec.ta.lib.Core;
import eqtrade.trading.market.chart.core.ChartContainer;

public abstract interface IChartController {
	  public abstract void setDatasource(IChartDatasource paramIChartDatasource);
	  public abstract IChartDataset getDataset();
	  public abstract ChartContainer getContainer();
	  public abstract Core getTalibCore();
}
