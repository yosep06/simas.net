package eqtrade.trading.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import com.vollux.framework.IApp;
import com.vollux.framework.UI;
import com.vollux.framework.VolluxUI;

import eqtrade.trading.ATS.UIByOrderId;
import eqtrade.trading.ATS.UIByScheduleLabel;
import eqtrade.trading.ATS.UIByStockId;
import eqtrade.trading.ATS.UINewSchedule;
import eqtrade.trading.ATS.UIScheduleList;
import eqtrade.trading.Margin.UIMarginMonitoring;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.ui.GTCLIST;
import eqtrade.trading.ui.UIAccount;
import eqtrade.trading.ui.UIAdvertising;
import eqtrade.trading.ui.UIAmendOrder;
import eqtrade.trading.ui.UIAnnouncement;
import eqtrade.trading.ui.UIAutoTrading;
import eqtrade.trading.ui.UIBuySellDetail;
import eqtrade.trading.ui.UICashColl;
import eqtrade.trading.ui.UIDueDate;
import eqtrade.trading.ui.UIEntryAdvertising;
import eqtrade.trading.ui.UIEntryCrossing;
import eqtrade.trading.ui.UIEntryNegdeal;
import eqtrade.trading.ui.UIEntryOrder;
import eqtrade.trading.ui.UIEntryOrderMarginAndShort;
import eqtrade.trading.ui.UIForceSell;
import eqtrade.trading.ui.UILoadMatrix;
import eqtrade.trading.ui.UIMarginCall;
import eqtrade.trading.ui.UIMarginCallPopUp;
import eqtrade.trading.ui.UINegDealList;
import eqtrade.trading.ui.UINotification;
import eqtrade.trading.ui.UIOrder;
import eqtrade.trading.ui.UIOrderMatrix;
import eqtrade.trading.ui.UIPortfolio;
import eqtrade.trading.ui.UIPortfolioSimple;
import eqtrade.trading.ui.UISplitOrder;
import eqtrade.trading.ui.UITrade;
import eqtrade.trading.ui.core.UIChgPIN;
import eqtrade.trading.ui.core.UIChgPassword;
import eqtrade.trading.ui.core.UIColour;
import eqtrade.trading.ui.core.UIConnection;
import eqtrade.trading.ui.core.UILogon;
import eqtrade.trading.ui.core.UIPIN;
import eqtrade.trading.ui.core.UIPIN_NEGO;
import eqtrade.trading.ui.core.UIReconnect;
import eqtrade.trading.ui.core.UIRefresh;

public class TradingUI extends VolluxUI {
	public final static String UI_LOGON = "ui.trading.logon";
	public final static String UI_CHGPIN = "ui.trading.chgpin";
	public final static String UI_CHGPASS = "ui.trading.chgpass";
	public final static String UI_RECONNECT = "ui.trading.reconnect";
	public final static String UI_CONNECTION = "ui.trading.connection";
	public final static String UI_COLOUR = "ui.trading.colour";
	public final static String UI_REFRESH = "ui.trading.refresh";
	public final static String UI_PIN_NEGO = "ui.trading.pinnego";

	public final static String UI_ORDER = "ui.trading.order";
	public final static String UI_TRADE = "ui.trading.trade";
	public final static String UI_ACCOUNT = "ui.trading.account";
	public final static String UI_PF = "ui.trading.pf";
	public final static String UI_MARGINCALL = "ui.trading.margincall";
	public final static String UI_FORCESELL = "ui.trading.forcesell";

	public final static String UI_GTC = "ui.trading.gtc";
	public final static String UI_PFSIMPLE = "ui.trading.pf.simple";

	public final static String UI_ENTRYORDER = "ui.trading.entryorder";
	public final static String UI_ENTRYORDERMARGINANDSHORT = "ui.trading.entryordermarginandshort";
	public final static String UI_AMENDORDER = "ui.trading.amendorder";
	public final static String UI_ENTRYADVERTISING = "ui.trading.entryadvertising";
	public final static String UI_ENTRYNEGDEAL = "ui.trading.entrynegdeal";
	public final static String UI_ENTRYCROSSING = "ui.trading.entrycrossing";

	public final static String UI_SPLITORDER = "ui.trading.splitorder";
	public final static String UI_NEGDEALLIST = "ui.negdeallist";
	public final static String UI_CASHCOLL = "ui.cash.coll";
	public final static String UI_BUYSELLDETAIL = "ui.buyselldetail";
	public final static String UI_PIN = "ui.pin";
	public final static String UI_ADVERTISING = "ui.advertising";
	public final static String UI_MM = "margin.trading.marginmonitoring";
	public final static String UI_DUEDATE ="ui.duedate";
	public final static String UI_NEWSCHEDULE = "ats.trading.uinewschedule";
	public static final String UI_SL = "ats.trading.schedulelist";
	public static final String UI_BYORDERID="ats.trading.byorderid";
	public static final String UI_BYSTOCKID="ats.trading.bystockid";
	public static final String UI_BYSCHEDULLABEL="ats.trading.byschedulelabel";
	public final static String UI_ANNOUCEMENT ="ui.trading.announcement";
	public final static String UI_NOTIFICATION ="ui.notification";
	public final static String UI_ORDERMATRIX = "ui.trading.ordermatrix";
	public final static String UI_LOADORDERMATRIX = "ui.trading.loadordermatrix";
	public final static String UI_AUTOTRADING = "ui.trading.autotrading";
	public final static String UI_MARGINCALLPOPUP = "ui.trading.margincallpopup";
	private HashMap<String, Vector<String>> cek =new HashMap<String, Vector<String>>();
	private Vector<String> user = new Vector<String>();
	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private Date today = new Date();
	private String todayFormat = formatter.format(today);private UIMarginCall MC;
	public TradingUI(IApp apps) {
		super(apps);
	}

	@SuppressWarnings("unchecked")
	public void registerUI(String key, UI ui) {
		formList.put(key, ui);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void buildUI() {
		formList.put(UI_LOGON, new UILogon(TradingApplication.CONS_TRADING));
		formList.put(UI_CHGPIN, new UIChgPIN(TradingApplication.CONS_TRADING));
		formList.put(UI_CHGPASS, new UIChgPassword(
				TradingApplication.CONS_TRADING));
		formList.put(UI_RECONNECT, new UIReconnect(
				TradingApplication.CONS_TRADING));
		formList.put(UI_CONNECTION, new UIConnection(
				TradingApplication.CONS_TRADING));
		formList.put(UI_COLOUR, new UIColour(TradingApplication.CONS_TRADING));
		formList.put(UI_REFRESH, new UIRefresh(TradingApplication.CONS_TRADING));
		formList.put(UI_MARGINCALL, new UIMarginCall(TradingApplication.CONS_TRADING));
		formList.put(UI_FORCESELL, new UIForceSell(TradingApplication.CONS_TRADING));
		formList.put(UI_ORDER, new UIOrder(TradingApplication.CONS_TRADING));
		formList.put(UI_TRADE, new UITrade(TradingApplication.CONS_TRADING));
		formList.put(UI_ACCOUNT, new UIAccount(TradingApplication.CONS_TRADING));
		formList.put(UI_PF, new UIPortfolio(TradingApplication.CONS_TRADING));
		
		
		formList.put(UI_PFSIMPLE, new UIPortfolioSimple(
				TradingApplication.CONS_TRADING));
		formList.put(UI_ENTRYORDER, new UIEntryOrder(
				TradingApplication.CONS_TRADING));
		formList.put(UI_ENTRYORDERMARGINANDSHORT, new UIEntryOrderMarginAndShort(
				TradingApplication.CONS_TRADING));
		formList.put(UI_AMENDORDER, new UIAmendOrder(
				TradingApplication.CONS_TRADING));
		formList.put(UI_ENTRYADVERTISING, new UIEntryAdvertising(
				TradingApplication.CONS_TRADING));
		formList.put(UI_ENTRYNEGDEAL, new UIEntryNegdeal(
				TradingApplication.CONS_TRADING));
		formList.put(UI_SPLITORDER, new UISplitOrder(
				TradingApplication.CONS_TRADING));
		formList.put(UI_ENTRYCROSSING, new UIEntryCrossing(
				TradingApplication.CONS_TRADING));
		formList.put(UI_NEGDEALLIST, new UINegDealList(
				TradingApplication.CONS_TRADING));
		formList.put(UI_MM, new UIMarginMonitoring(
				TradingApplication.CONS_TRADING));//Buatan sendiri
		formList.put(UI_ADVERTISING, new UIAdvertising(
				TradingApplication.CONS_TRADING));
		formList.put(UI_CASHCOLL, new UICashColl(
				TradingApplication.CONS_TRADING));
		formList.put(UI_PIN, new UIPIN(TradingApplication.CONS_TRADING));
		formList.put(UI_PIN_NEGO, new UIPIN_NEGO(TradingApplication.CONS_TRADING));//PIN NEGO
		formList.put(UI_BUYSELLDETAIL, new UIBuySellDetail(
				TradingApplication.CONS_TRADING));
		formList.put(UI_DUEDATE, new UIDueDate(
				TradingApplication.CONS_TRADING));
		formList.put(UI_NOTIFICATION, new UINotification(
						TradingApplication.CONS_TRADING));				
		formList.put(UI_BYORDERID, new UIByOrderId(
				TradingApplication.CONS_TRADING));
		formList.put(UI_BYSTOCKID, new UIByStockId(
				TradingApplication.CONS_TRADING));
		formList.put(UI_BYSCHEDULLABEL, new UIByScheduleLabel(
				TradingApplication.CONS_TRADING));
		formList.put(UI_SL,  new UIScheduleList(TradingApplication.CONS_TRADING));
		formList.put(UI_NEWSCHEDULE,new UINewSchedule(
				TradingApplication.CONS_TRADING));
		formList.put(UI_ANNOUCEMENT,new UIAnnouncement(TradingApplication.CONS_TRADING));
		
		

		formList.put(UI_ORDERMATRIX, new UIOrderMatrix(TradingApplication.CONS_TRADING));
		formList.put(UI_LOADORDERMATRIX, new UILoadMatrix(TradingApplication.CONS_TRADING));
		formList.put(UI_AUTOTRADING, new UIAutoTrading(TradingApplication.CONS_TRADING));
		
		formList.put(UI_GTC,new GTCLIST (TradingApplication.CONS_TRADING));//2
		formList.put(UI_MARGINCALLPOPUP, new UIMarginCallPopUp(TradingApplication.CONS_TRADING));
		
	}

	@Override
	protected void saveLayout(Vector currView) {
		TradingSetting.putLayout("opened", currView);
		//System.out.println("save trading layout: " + currView.toString());
	}

	@Override
	protected Vector loadLayout() {
		/*System.out.println("load trading layout: "
				+ TradingSetting.getLayout("opened"));*/
		return (Vector) TradingSetting.getLayout("opened");
	}
	
    public void load(){
        try {
            Vector currView = loadLayout();
            if (currView!=null){
                    for (int i=0;i<currView.size();i++){
                        UI ui = (UI)formList.get((String)currView.elementAt(i));
                        if (!((Boolean)ui.getProperty(UI.C_PROPERTY_MULTI)).booleanValue()) {
                            String name = (String)currView.elementAt(i);
                            if (!name.equals(UI_LOGON)&&!name.equals(UI_MARGINCALL)) showUI(name);
                        }
                    }
            }
          /* cek.put("today", "26/10/2011");writeFile("data/logLogin/Login.dat", cek.put("today", "26/10/2011"));
            System.out.println("cek ---- "+cek.get("today"));
            if(readFile("data/logLogin/Login.dat") != null){
            cekLogin();}
            else {writeFile("data/logLogin/Login.dat", todayFormat);}*/
            for(Iterator i = formMultiList.keySet().iterator();i.hasNext();){
                UI v = (UI)formMultiList.get(i.next());
                v.init(apps);
            }        
        } catch (Exception ex){
            ex.printStackTrace();
        }        
    }
    public void cekLogin(String userId){
    	
    	try{   
    		 	
    	Object obj = readFile("data/logs/Login.dat");
        cek = (HashMap) obj;    	
        int a =0; 
      //  System.out.println(formatter.format(today));
      //  System.out.println("cek ---- "+cek.get(formatter.format(today)));
    	if(cek.get(formatter.format(today)) != null){
            user.addAll(cek.get(formatter.format(today)));//System.out.println("user "+userId);	
            for(int i =0;i<user.size();i++){
            //	System.out.println("user get"+i+"-"+user.get(i));	
            	if (user.get(i).equals(userId)) {	
            		a++;
				}
            }//System.out.println("nilai a---"+a);
            if(a==0){user.add(userId);
    		cek.put(formatter.format(today), user);
            showUI("ui.trading.margincall");
            writeFile("data/logs/Login.dat", cek);}
            else {load();/*System.out.println("cekuser ---- "+cek);*/}
    	}else{cek =new HashMap<String, Vector<String>>();showUI("ui.trading.margincall");
			user.add(userId);System.out.println("cekuser ---- "+user);
			cek.put(todayFormat, user);
			writeFile("data/logs/Login.dat", cek);
    		/*System.out.println("cekget ---- "+cek);*/}
    	}catch (Exception e) {
    		if(readFile("data/logs/Login.dat") !=null)	load();
    		else{cek =new HashMap<String, Vector<String>>();showUI("ui.trading.margincall");
    		user.add(userId);//System.out.println("cacth ---- "+userId);
    		cek.put(todayFormat, user);
    		writeFile("data/logs/Login.dat", cek);}
		}
    }
    public static Object readFile(String pname) {
		Object objresult = null;

		try {
			File file = new File(pname);
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream in = new ObjectInputStream(fis);
			objresult = in.readObject();
			in.close();
		} catch (Exception ex) {
            objresult = null;
		}

		return objresult;
	}

	public static boolean writeFile(String pname, Object objin) {
		boolean bresult = false;

		try {
			if (objin != null) {
				FileOutputStream fos = new FileOutputStream(pname);
				ObjectOutput out = new ObjectOutputStream(fos);
				out.writeObject(objin);
				out.close();
				bresult = true;
			}
		} catch (Exception ex) {
			//ex.printStackTrace();
		}

		return bresult;
	}
	
}
