package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumnModel;

import quickfix.field.Price;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.border.LineBorder;
import com.vollux.demo.Mapping;
import com.vollux.framework.Action;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Quote;
import eqtrade.feed.model.QuoteDef;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.ui.quote.FilterQuote;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.OrderIDGenerator;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.NegDealList;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.Portfolio;
import eqtrade.trading.model.PortfolioDef;
import eqtrade.trading.model.Stock;
import eqtrade.trading.ui.core.UIPIN_NEGO;

public class UIEntryNegdeal extends UI {
	JDropDown comboStock = null;
	JText fieldStockName;
	// JDropDown fieldStockName;
	JDropDown comboClient = null;
	JNumber fieldPrice;
	JNumber fieldValue;
	JNumber fieldVolume;
	// JText fieldClientName;
	JDropDown fieldClientName;
	JNumber fieldTradingLimit;
	JNumber fieldStockLimit;
	NegDealList order;
	JDropDown comboBoard = null;
	JDropDown comboType = null;
	JDropDown comboIA = null;
	JDropDown comboIAContra = null;
	JDropDown comboBroker = null;
	JCheckBox checkConfirm;
	JLabel labelTA;
	JLabel labelOverlimit;
	JPanel pnlEntry;
	JText fieldTrader;
	JText fieldOrderid;
	JLabel lblOrderid;
	JLabel lblIAContra;

	JButton btnSend;
	JButton btnClear;
	JButton btnClose;

	private JGrid tableBid;
	private JGrid tableOffer;
	private JLabel fieldPrev;
	private JLabel fieldHigh;
	private JLabel fieldLow;
	private JLabel fieldChange;
	private JLabel fieldOpen;
	private JLabel fieldLast;
	private JPanel pnlInfo;

	private JPanel headerEntry;
	private JLabel titleEntry;

	private String entryType = "BUY";

	private FilterQuote filterBid;
	private FilterQuote filterOffer;
	private static NumberFormat formatter = new DecimalFormat("#,##0 ");
	private static NumberFormat formatter2 = new DecimalFormat("#,##0.00");
	private Action sendAction = null;
	private String soldstock = "";

	private JGrid tablePF;
	private FilterPortfolio filterPF;

	public UIEntryNegdeal(String app) {
		super("Entry Neg Deal", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_ENTRYNEGDEAL);
	}

	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
			}
		});
	}

	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				tablePF.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	private JSkinPnl buildPF() {
		createPopup();
		filterPF = new FilterPortfolio(pnlContent);
		tablePF = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_PORTFOLIO), filterPF,
				(Hashtable) hSetting.get("table"));
		tablePF.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		tablePF.setColumnHide(PortfolioDef.columnhide2);

		JSkinPnl temp = new JSkinPnl(new BorderLayout());
		temp.add(tablePF, BorderLayout.CENTER);
		temp.setPreferredSize(new Dimension(300, 200));
		registerEvent(tablePF);
		registerEvent(tablePF.getTable());
		return temp;
	}

	private JSkinPnl buildQuote() {
		filterBid = new FilterQuote(pnlContent);
		filterOffer = new FilterQuote(pnlContent);
		tableBid = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_QUOTE), filterBid,
				(Hashtable) hSetting.get("tablebid"));
		tableOffer = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_QUOTE), filterOffer,
				(Hashtable) hSetting.get("tableoff"));
		tableBid.setPreferredSize(new Dimension(120, 50));
		tableOffer.setPreferredSize(new Dimension(120, 50));
		tableBid.setMinimumSize(new Dimension(120, 50));
		tableOffer.setMinimumSize(new Dimension(120, 50));
		tableBid.getTable().getTableHeader().setReorderingAllowed(false);
		tableOffer.getTable().getTableHeader().setReorderingAllowed(false);
		tableBid.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableBid.setColumnHide(new int[] { Quote.CIDX_NUMBER, Quote.CIDX_PREV,
				Quote.CIDX_TYPE, Quote.CIDX_STOCK });
		tableOffer.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableOffer.setColumnHide(new int[] { Quote.CIDX_NUMBER,
				Quote.CIDX_PREV, Quote.CIDX_TYPE, Quote.CIDX_STOCK });
		tableBid.getTable().getModel()
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						chgStockSummary();
					}
				});

		tableOffer.getTable().getModel()
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						chgStockSummary();
					}
				});
		((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCKSUMMARY)
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						chgStockSummary();
					}
				});
		((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						changeTL();
					}
				});
		fieldPrev = new JLabel(" ", JLabel.RIGHT);
		fieldHigh = new JLabel(" ", JLabel.RIGHT);
		fieldLow = new JLabel(" ", JLabel.RIGHT);
		fieldChange = new JLabel(" ", JLabel.RIGHT);
		fieldOpen = new JLabel(" ", JLabel.RIGHT);
		fieldLast = new JLabel(" ", JLabel.RIGHT);

		pnlInfo = new JPanel();
		FormLayout l = new FormLayout(
				"50px,2dlu,pref:grow, 2dlu,pref,2dlu,pref:grow",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(new JLabel("Last"), c.xy(1, 3));
		b.add(fieldLast, c.xy(3, 3));
		b.add(new JLabel("Hi"), c.xy(1, 5));
		b.add(fieldHigh, c.xy(3, 5));
		b.add(new JLabel("Lo"), c.xy(1, 7));
		b.add(fieldLow, c.xy(3, 7));
		b.add(new JLabel("Open"), c.xy(5, 3));
		b.add(fieldOpen, c.xy(7, 3));
		b.add(new JLabel("Close"), c.xy(5, 5));
		b.add(fieldPrev, c.xy(7, 5));
		b.add(new JLabel("Chg"), c.xy(5, 7));
		b.add(fieldChange, c.xy(7, 7));

		FormLayout layoutInfo = new FormLayout("pref:grow",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		PanelBuilder builderInfo = new PanelBuilder(layoutInfo);
		builderInfo.setBorder(new CompoundBorder(new LineBorder(),
				new EmptyBorder(4, 4, 4, 4)));
		builderInfo.add(labelTA, c.xy(1, 1));
		builderInfo.add(fieldTradingLimit, c.xy(1, 3));
		builderInfo.add(fieldStockLimit, c.xy(1, 5));
		builderInfo.add(labelOverlimit, c.xy(1, 7));
		builderInfo.getPanel().setOpaque(false);

		FormLayout layout2 = new FormLayout("150px:grow, 1dlu, 150px:grow",
				"pref, 2dlu, pref, 2dlu, fill:200px:grow");
		PanelBuilder builder2 = new PanelBuilder(layout2);
		builder2.add(builderInfo.getPanel(), c.xyw(1, 1, 3));
		builder2.add(pnlInfo, c.xyw(1, 3, 3));
		builder2.add(tableBid, c.xy(1, 5));
		builder2.add(tableOffer, c.xy(3, 5));

		JSkinPnl temp = new JSkinPnl(new BorderLayout());
		temp.setBorder(new EmptyBorder(2, 2, 2, 2));
		temp.add(builder2.getPanel(), BorderLayout.CENTER);
		temp.add(buildPF(), BorderLayout.EAST);
		registerEvent(tableBid);
		registerEvent(tableBid.getTable());
		registerEvent(tableOffer);
		registerEvent(tableOffer.getTable());
		return temp;
	}

	protected void build() {
		sendAction = apps.getAction().get(TradingAction.A_SHOWBUYORDER);
		fieldTradingLimit = new JNumber(Double.class, 0, 0, true, false);
		fieldTradingLimit.setHorizontalAlignment(JLabel.CENTER);
		fieldStockLimit = new JNumber(Double.class, 0, 0, true, false);
		fieldStockLimit.setHorizontalAlignment(JLabel.CENTER);
		comboClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboStock = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		comboBoard = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_BOARD), Mapping.CIDX_CODE, false);
		comboBoard.setEnabled(false);
		comboType = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ORDERTYPE), Mapping.CIDX_CODE,
				false);
		comboIA = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_TRADINGACC), Mapping.CIDX_CODE,
				false);
		comboIAContra = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_TRADINGACC), Mapping.CIDX_CODE,
				false);
		
		comboIA.setEnabled(false);
		// fieldClientName = new JText(false);
		fieldStockName = new JText(false);
		fieldClientName = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT), Account.C_NAME, true);
		// fieldStockName = new JDropDown(((IEQTradeApp)
		// apps).getTradingEngine()
		// .getStore(TradingStore.DATA_STOCK), Stock.C_NAME, true);
		comboBroker = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_BROKER), Mapping.CIDX_CODE, true);
		fieldTrader = new JText(true);
		fieldOrderid = new JText(true);
		lblOrderid = new JLabel("Orderid to Confirm");
		lblIAContra = new JLabel("Contra Acc");
		fieldTrader.setForeground(Color.black);
		fieldOrderid.setForeground(Color.black);
		labelTA = new JLabel("Trading Limit");
		labelTA.setHorizontalAlignment(JLabel.CENTER);
		labelOverlimit = new JLabel("");
		labelOverlimit.setHorizontalAlignment(JLabel.CENTER);
		fieldPrice = new JNumber(Double.class, 0, 0, true);
		fieldValue = new JNumber(Double.class, 0, 0, true, false);
		fieldVolume = new JNumber(Double.class, 0, 0, true);
		btnSend = new JButton("Send");
		btnSend.setMnemonic('S');
		btnClear = new JButton("Clear");
		btnClose = new JButton("Close");
		btnClose.setMnemonic('C');
		checkConfirm = new JCheckBox("Confirm");
		checkConfirm.setOpaque(false);

		registerEvent(comboClient.getTextEditor());
		registerEventCombo(comboClient);
		registerEvent(comboStock.getTextEditor());
		registerEventCombo(comboStock);
		// registerEvent(fieldStockName.getTextEditor());
		// registerEventCombo(fieldStockName);
		registerEvent(fieldClientName.getTextEditor());
		registerEventCombo(fieldClientName);
		registerEvent(comboBoard);
		registerEvent(comboType);
		registerEvent(comboIA);
		registerEvent(comboIAContra);
		registerEvent(fieldPrice);
		registerEvent(fieldVolume);
		registerEvent(btnSend);
		registerEvent(btnClear);
		registerEvent(btnClose);
		registerEvent(checkConfirm);
		registerEvent(comboBroker.getTextEditor());
		registerEventCombo(comboBroker);
		registerEvent(fieldTrader);
		registerEvent(fieldOrderid);

		comboClient.getTextEditor().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				clientfocusLost(arg0);
			}

			public void focusGained(FocusEvent e) {
				try {
					if (((IEQTradeApp) apps).getTradingEngine()
							.getStore(TradingStore.DATA_ACCOUNT).getRowCount() == 1) {
						comboClient.setSelectedIndex(0);
					} else {
						comboClient.showPopup();
					}
				} catch (Exception ex) {
				}
			}
		});

		fieldClientName.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				try {
					fieldClientName.showPopup();
				} catch (Exception ex) {
				}
			}

			@Override
			public void focusLost(FocusEvent e) {
				clientfocusLost(e);
			}

		});

		comboStock.getTextEditor().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				stockfocusLost(arg0);
			}

			public void focusGained(FocusEvent e) {
				try {
					comboStock.showPopup();
				} catch (Exception ex) {
				}
			}
		});

		/*
		 * fieldStockName.getTextEditor().addFocusListener(new FocusAdapter() {
		 * 
		 * @Override public void focusGained(FocusEvent e) { try {
		 * fieldStockName.showPopup(); }catch (Exception ex) { } }
		 * 
		 * @Override public void focusLost(FocusEvent e) { stockfocusLost(e); }
		 * 
		 * });
		 */

		fieldVolume.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				calculate();
			}
		});

		comboBroker.getTextEditor().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				comboBroker.getTextEditor().setText(
						comboBroker.getText().trim().toUpperCase());
			}

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboBroker.showPopup();
				} catch (Exception ex) {
				}
			}

		});

		fieldTrader.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				fieldTrader.setText(fieldTrader.getText().trim().toLowerCase());
			}
		});

		fieldOrderid.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				fieldOrderid.setText(fieldOrderid.getText().trim()
						.toUpperCase());
			}
		});

		fieldPrice.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				calculate();
			}
		});

		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				send();
				//clearForm();
			}
		});

		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clearForm();
			}
		});

		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveSetting();
				clearForm();
				form.setVisible(false);
			}
		});

		headerEntry = new JPanel(new BorderLayout());
		headerEntry.setBackground(entryType.equals("BUY") ? Color.red.darker()
				: Color.green.darker());
		titleEntry = new JLabel(entryType.equals("BUY") ? "Confirm  Neg Deal"
				: "Entry Neg Deal");
		titleEntry.setHorizontalAlignment(JLabel.CENTER);
		titleEntry.setFont(new Font("dialog", Font.BOLD, 20));
		headerEntry.setBorder(new EmptyBorder(10, 10, 10, 10));
		headerEntry.add(titleEntry, BorderLayout.CENTER);
		headerEntry.setPreferredSize(new Dimension(100, 60));

		pnlEntry = new JPanel();
		FormLayout lay = new FormLayout(
				"pref, 2dlu, 75px, 2dlu, pref, 2dlu, 100px,2dlu,pref",
				"fill:pref:grow, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref,2dlu,pref,2dlu,pref");

		JSkinPnl temp2 = new JSkinPnl();
		temp2.setBorder(new CompoundBorder(new LineBorder(), new EmptyBorder(4,
				4, 4, 4)));
		CellConstraints cc = new CellConstraints();

		PanelBuilder builder = new PanelBuilder(lay, pnlEntry);
		builder.setBorder(new EmptyBorder(4, 4, 4, 4));
		builder.add(new JLabel("Client"), cc.xy(1, 3));
		builder.add(comboClient, cc.xy(3, 3));
		builder.add(fieldClientName, cc.xywh(5, 3, 5, 1));
		builder.add(new JLabel("Board"), cc.xy(1, 5));
		builder.add(comboBoard, cc.xy(3, 5));
		builder.add(new JLabel("Stock"), cc.xy(1, 7));
		builder.add(comboStock, cc.xy(3, 7));
		builder.add(fieldStockName, cc.xywh(5, 7, 5, 1));
		builder.add(new JLabel("Contra broker"), cc.xy(1, 9));
		builder.add(comboBroker, cc.xy(3, 9));
		builder.add(new JLabel("Contra trader"), cc.xy(5, 9));
		builder.add(fieldTrader, cc.xy(7, 9));
		builder.add(lblOrderid, cc.xy(1, 11));
		builder.add(fieldOrderid, cc.xy(3, 11));
		builder.add(lblIAContra, cc.xy(5, 11));
		builder.add(comboIAContra, cc.xy(7, 11));
		builder.add(new JLabel("Price"), cc.xy(1, 13));
		builder.add(fieldPrice, cc.xy(3, 13));
		builder.add(new JLabel("Quantity"), cc.xy(1, 15));
		builder.add(fieldVolume, cc.xy(3, 15));
		builder.add(new JLabel("Share(s)"), cc.xy(5, 15));

		builder.add(new JLabel("Trading Acc"), cc.xy(1, 17));
		builder.add(comboIA, cc.xy(3, 17));
		builder.add(new JLabel("Expire"), cc.xy(5, 17));
		builder.add(comboType, cc.xy(7, 17));
		builder.add(new JLabel("Amount"), cc.xy(1, 19));
		builder.add(fieldValue, cc.xyw(3, 19, 7));
		pnlEntry.add(headerEntry, cc.xyw(1, 1, 9));

		/*
		 * builder.add(new JLabel("Board"), cc.xy(1, 3));
		 * builder.add(comboBoard, cc.xy(3, 3)); builder.add(new
		 * JLabel("Stock"), cc.xy(1, 5)); builder.add(comboStock, cc.xy(3, 5));
		 * builder.add(fieldStockName, cc.xywh(5, 5, 5, 1)); builder.add(new
		 * JLabel("Contra broker"), cc.xy(1, 7)); builder.add(comboBroker,
		 * cc.xy(3, 7)); builder.add(new JLabel("Contra trader"), cc.xy(5, 7));
		 * builder.add(fieldTrader, cc.xy(7, 7)); builder.add(lblOrderid,
		 * cc.xy(1, 9)); builder.add(fieldOrderid, cc.xy(3, 9));
		 * builder.add(lblIAContra, cc.xy(5, 9)); builder.add(comboIAContra,
		 * cc.xy(7, 9)); builder.add(new JLabel("Price"), cc.xy(1, 11));
		 * builder.add(fieldPrice, cc.xy(3, 11)); builder.add(new
		 * JLabel("Quantity"), cc.xy(1, 13)); builder.add(fieldVolume, cc.xy(3,
		 * 13)); builder.add(new JLabel("Share(s)"), cc.xy(5, 13));
		 * builder.add(new JLabel("Client"), cc.xy(1, 15));
		 * builder.add(comboClient, cc.xy(3, 15)); builder.add(fieldClientName,
		 * cc.xywh(5, 15, 5, 1)); builder.add(new JLabel("Trading Acc"),
		 * cc.xy(1, 17)); builder.add(comboIA, cc.xy(3, 17)); builder.add(new
		 * JLabel("Expire"), cc.xy(5, 17)); builder.add(comboType, cc.xy(7,
		 * 17)); builder.add(new JLabel("Amount"), cc.xy(1, 19));
		 * builder.add(fieldValue, cc.xyw(3, 19, 7)); pnlEntry.add(headerEntry,
		 * cc.xyw(1, 1, 9));
		 */

		JSkinPnl temp3 = new JSkinPnl();
		FormLayout lay3 = new FormLayout(
				"pref, 2dlu, pref:grow, 2dlu, pref, 2dlu, pref", "pref");

		PanelBuilder builder3 = new PanelBuilder(lay3, temp3);
		builder3.add(checkConfirm, cc.xy(1, 1));
		builder3.add(btnSend, cc.xy(5, 1));
		builder3.add(btnClose, cc.xy(7, 1));
		builder.add(temp3, cc.xywh(1, 21, 9, 1));

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(10, 10, 10, 10));
		pnlContent.add(pnlEntry, BorderLayout.CENTER);
		pnlContent.add(buildQuote(), BorderLayout.EAST);
		clearForm();
		refresh();
	}

	void changeTL() {
		String acc = getAccountId(comboClient.getText().toUpperCase().trim());
		if (!acc.equals("")) {
			Account ac = (Account) ((IEQTradeApp) apps).getTradingEngine()
					.getStore(TradingStore.DATA_ACCOUNT)
					.getDataByKey(new String[] { acc });
			if (ac != null) {
				fieldTradingLimit.setValue(ac.getCurrTL());
			}
		}
	}

	private void registerEventCombo(JDropDown comp) {
		comp.getEditor().getEditorComponent()
				.setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(
				JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap()
				.put("enterAction", new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(comboStock.getTextEditor())) {
							comboStock.setSelectedIndex(comboStock
									.getSelectedIndex());
							comboStock.hidePopup();
							// fieldPrice.requestFocus();
																			
							comboBroker.requestFocus();
							// comboBroker.requestFocus();
							// fieldStockName.requestFocus();
						} else if (e.getSource().equals(
								comboClient.getTextEditor())) {
							comboClient.setSelectedIndex(comboClient
									.getSelectedIndex());
							comboClient.hidePopup();
							// comboStock.requestFocus();
							fieldClientName.requestFocus();
						} else if (e.getSource().equals(
								comboBroker.getTextEditor())) {
							comboBroker.setSelectedIndex(comboBroker
									.getSelectedIndex());
							comboBroker.hidePopup();
							fieldTrader.requestFocus();
						} else if (e.getSource().equals(
								fieldClientName.getTextEditor())) {
							comboClient.setSelectedIndex(fieldClientName
									.getSelectedIndex());
							comboStock.requestFocus();
							fieldClientName.hidePopup();
						}/*
						 * else
						 * if(e.getSource().equals(fieldStockName.getTextEditor
						 * ())){ comboStock.setSelectedIndex(fieldStockName.
						 * getSelectedIndex()); comboBroker.requestFocus();
						 * fieldStockName.hidePopup(); }
						 */

						// comboStock.requestFocus();

					}

				});

		comp.getTextEditor().getActionMap()
				.put("tabAction", new AbstractAction("tabAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(comboStock.getTextEditor())) {
							comboStock.setSelectedIndex(comboStock
									.getSelectedIndex());
							comboStock.hidePopup();
							// fieldPrice.requestFocus();
							comboBroker.requestFocus();
							// fieldStockName.requestFocus();
						} else if (e.getSource().equals(
								comboClient.getTextEditor())) {
							comboClient.setSelectedIndex(comboClient
									.getSelectedIndex());
							comboClient.hidePopup();
							// comboStock.requestFocus();
							fieldClientName.requestFocus();
						} else if (e.getSource().equals(
								comboBroker.getTextEditor())) {
							comboBroker.setSelectedIndex(comboBroker
									.getSelectedIndex());
							comboBroker.hidePopup();
							fieldTrader.requestFocus();
						} else if (e.getSource().equals(
								fieldClientName.getTextEditor())) {
							comboClient.setSelectedIndex(fieldClientName
									.getSelectedIndex());
							comboStock.requestFocus();
							fieldClientName.hidePopup();
						}
					}

				});

	}

	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tablebid", QuoteDef.getTableBidDef());
			hSetting.put("tableoff", QuoteDef.getTableOffDef());
			hSetting.put("table", PortfolioDef.getTableDef());
			hSetting.put("typeEntry", "BUY");
			hSetting.put("confim", new Boolean(true));
		}
		entryType = (String) hSetting.get("typeEntry");
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 650, 500));
	}

	private Hashtable getCustomProperties() {
		Hashtable h = new Hashtable();
		TableColumnModel mod = tableOffer.getTable().getColumnModel();
		int[] width = QuoteDef.defaultColumnWidthOff;
		for (int i = 0; i < mod.getColumnCount(); i++) {
			String str = mod.getColumn(i).getHeaderValue().toString();
			int length = (mod.getColumn(i).getPreferredWidth() == 0) ? 0 : mod
					.getColumn(i).getWidth();
			if (str.equals("Offer")) {
				width[4] = length;
			} else if (str.equals("#O")) {
				width[6] = length;
			} else if (str.equals("Lot")) {
				width[5] = length;
			}
		}
		h.put("width", width);
		h.put("order", QuoteDef.defaultColumnOrderOff);
		h.put("font", tableOffer.getTableFont());
		h.put("header", QuoteDef.dataHeaderOff);
		h.put("alignment", QuoteDef.defaultHeaderAlignment);
		h.put("sorted", QuoteDef.columnsort);
		h.put("sortcolumn", new Integer(Quote.CIDX_NUMBER));
		h.put("sortascending", new Boolean(false));

		return h;
	}

	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("typeEntry", entryType);
		hSetting.put("table", tablePF.getTableProperties());
		hSetting.put("tablebid", tableBid.getTableProperties());
		hSetting.put("tableoff", getCustomProperties()); // tableOffer.getTableProperties());
		hSetting.put("confirm", new Boolean(checkConfirm.isSelected()));
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	public void refresh() {
		tableBid.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tableOffer.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		pnlInfo.setBackground(TradingSetting
				.getColor(TradingSetting.C_BACKGROUND));
		tableBid.setNewFont(TradingSetting.getFont());
		tableOffer.setNewFont(TradingSetting.getFont());
		tablePF.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tablePF.setNewFont(TradingSetting.getFont());
		chgStockSummary();
	}

	protected void initUI() {
		super.initUI();
		form.setResizable(false);
		form.getBtnClose().setVisible(false);
		form.pack();
	}

	public void show(Object param) {
		if (!apps.getAction().get(TradingAction.A_SHOWNEGDEAL).isGranted())
			return;
		HashMap p = (HashMap) param;
		entryType = (String) p.get("TYPE");
		if (p.get("ORDER") != null) {
			order = (NegDealList) p.get("ORDER");
			comboBroker.getTextEditor().setText(order.getContraBrokerID());
			fieldTrader.setText(order.getContraUserID());
			fieldOrderid.setText(order.getMarketOrderID());
			comboStock.getTextEditor().setText(order.getSecID());
			fieldPrice.setValue(order.getPrice());
			fieldVolume.setValue(order.getVolume());
			comboClient.getTextEditor().requestFocus();
		}
		show();
	}

	public void show() {
		if (!apps.getAction().get(TradingAction.A_SHOWNEGDEAL).isGranted())
			return;
		changeForm();
		form.pack();
		super.show();
	}

	void changeForm() {
		form.setTitle("Neg Deal");
		titleEntry.setText(entryType.equals("BUY") ? "Confirm  Neg Deal"
				: "Entry Neg Deal");
		headerEntry.setBackground(entryType.equals("BUY") ? Color.red.darker()
				: Color.green.darker());
		pnlEntry.setBackground(headerEntry.getBackground());
		if (entryType.equals("BUY")) {
			labelTA.setText("Trading Limit");
			fieldTradingLimit.setVisible(true);
			fieldStockLimit.setVisible(false);
			fieldOrderid.setVisible(true);
			lblOrderid.setVisible(true);
			fieldOrderid.setEditable(true);
			fieldOrderid.setForeground(Color.black);
			lblIAContra.setVisible(true);
			comboIAContra.setVisible(true);
		} else {			
			labelTA.setText("Stock Balance");
			fieldTradingLimit.setVisible(false);
			fieldStockLimit.setVisible(true);
			fieldOrderid.setForeground(Color.black);
			if (order == null) {
				fieldOrderid.setText("");
				fieldOrderid.setEditable(false);
				fieldOrderid.setVisible(false);

				lblOrderid.setVisible(false);
				lblIAContra.setVisible(false);
				comboIAContra.setVisible(false);
			}
		}
		calculate();
	}

	void clientfocusLost(FocusEvent e) {
		try {
			comboClient.getTextEditor().setText(
					comboClient.getText().toUpperCase());
			Account acc = null;
			int i = ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ACCOUNT)
					.getIndexByKey(
							new String[] { getAccountId(comboClient.getText()) });
			if (i >= 0) {
				acc = (Account) ((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_ACCOUNT).getDataByIndex(i);
				if (acc != null) {
					fieldClientName.setSelectedItem(acc.getName());
					fieldTradingLimit.setValue(acc.getCurrTL());
					comboIA.setSelectedIndex(acc.getInvType().equals("I") ? 0: 1);
				}
				
			} else {
				fieldClientName.getTextEditor().setText("");
				fieldTradingLimit.setValue(null);
			}
			changeMaxLot();
			changePF(acc == null ? "" : acc.getTradingId());
			calculate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	void stockfocusLost(FocusEvent e) {
		try {
			if (!comboStock.getText().trim().equals("")) {
				comboStock.getTextEditor().setText(
						comboStock.getText().toUpperCase());
				int i = ((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_STOCK)
						.getIndexByKey(new String[] { comboStock.getText() });
				if (i >= 0) {
					Stock stock = (Stock) ((IEQTradeApp) apps)
							.getTradingEngine()
							.getStore(TradingStore.DATA_STOCK)
							.getDataByIndex(i);
					if (stock != null) {
						fieldStockName.setText(stock.getName());
						// fieldStockName.setSelectedItem(stock.getName());
					}
				} else {
					fieldStockName.setText("");
					// fieldStockName.getTextEditor().setText("");
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							comboStock.getTextEditor().requestFocus();
						}
					});
				}
				comboStock.hidePopup();
				calculate();
				changeMaxLot();
				if (comboStock.getText().endsWith("-R")) {
					// comboBoard.setSelectedItem("TN");
					comboType.setSelectedIndex(1);
					comboType.setEnabled(false);
					comboType.setFocusable(false);
				} else {
					// comboBoard.setSelectedIndex(0);
					comboType.setSelectedIndex(0);
					comboType.setEnabled(true);
					comboType.setFocusable(true);
				}
				comboBoard.setSelectedItem("NG");
			}
			changeQuote(comboStock.getText());
		} catch (Exception ex) {
		}
	}

	private void chgStockSummary() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				StockSummary summ = (StockSummary) ((IEQTradeApp) apps)
						.getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY)
						.getDataByKey(new Object[] { soldstock, "RG" });
				if (summ != null) {
					fieldLast.setText(formatter.format(summ.getClosing()
							.doubleValue()));
					fieldChange.setText(getChange(summ.getClosing()
							.doubleValue(), summ.getPrevious().doubleValue()));
					fieldPrev.setText(formatter.format(summ.getPrevious()
							.doubleValue()));
					fieldOpen.setText(formatter.format(summ.getOpening()
							.doubleValue()));
					fieldHigh.setText(formatter.format(summ.getHighest()
							.doubleValue()));
					fieldLow.setText(formatter.format(summ.getLowest()
							.doubleValue()));

					fieldLast
							.setForeground(summ.getClosing().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getClosing().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldChange
							.setForeground(summ.getClosing().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getClosing().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldPrev.setForeground(FeedSetting
							.getColor(FeedSetting.C_ZERO));

					fieldOpen
							.setForeground(summ.getOpening().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getOpening().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldHigh
							.setForeground(summ.getHighest().doubleValue() > summ
									.getClosing().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getHighest().doubleValue() < summ
									.getClosing().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldLow.setForeground(summ.getLowest().doubleValue() > summ
							.getPrevious().doubleValue() ? FeedSetting
							.getColor(FeedSetting.C_PLUS)
							: (summ.getLowest().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
				} else {
					fieldOpen.setText("- ");
					fieldHigh.setText("- ");
					fieldLow.setText("- ");
					fieldLast.setText("- ");
					fieldChange.setText("- ");
					fieldPrev.setText("- ");
					fieldOpen.setText("- ");
					fieldHigh.setText("- ");
					fieldLow.setText("- ");
				}
			}
		});
	}

	private String getChange(double slastprice, double nprev) {
		String schange = " ";
		if (slastprice > 0) {
			double ntemp = slastprice - nprev;
			double ndbl = ((double) (ntemp * 100) / nprev);
			int ntmpdbl = (int) (ndbl * 100);
			double npersen = (double) ntmpdbl / 100;
			if (ntemp > 0) {
				schange = "+" + formatter.format(ntemp) + "("
						+ formatter2.format(npersen) + "%) ";
			} else if (ntemp == 0) {
				schange = "" + formatter.format(ntemp) + "(0.00%) ";
			} else {
				schange = "" + formatter.format(ntemp) + "("
						+ formatter2.format(npersen) + "%) ";
			}
		}
		return schange;
	}

	void changeMaxLot() {
		try {
			int i = ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_PORTFOLIO)
					.getIndexByKey(
							new String[] { getAccountId(comboClient.getText()),
									comboStock.getText() });
			if (i >= 0) {
				Portfolio pf = (Portfolio) ((IEQTradeApp) apps)
						.getTradingEngine()
						.getStore(TradingStore.DATA_PORTFOLIO)
						.getDataByIndex(i);
				if (pf != null) {
					double lotsize = 100;
					int lt = ((IEQTradeApp) apps)
							.getTradingEngine()
							.getStore(TradingStore.DATA_STOCK)
							.getIndexByKey(
									new String[] { comboStock.getText() });
					if (lt >= 0) {
						Stock stock = (Stock) ((IEQTradeApp) apps)
								.getTradingEngine()
								.getStore(TradingStore.DATA_STOCK)
								.getDataByIndex(lt);
						if (stock != null) {
							lotsize = (stock.getLotSize().doubleValue());
						}
					}
					fieldStockLimit.setValue(new Double(pf.getAvailableVolume()
							.doubleValue()));
				}
			} else {
				fieldStockLimit.setValue(new Double(0));
			}
		} catch (Exception ex) {
		}
	}

	void calculate() {
		double price = 0, volume = 0;
		try {
			volume = ((Double) fieldVolume.getValue()).doubleValue();
			price = ((Double) fieldPrice.getValue()).doubleValue();
			fieldValue.setValue(new Double(volume * price));
		} catch (Exception ex) {
			fieldValue.setValue(null);
		}
		try {
			if (entryType.equals("BUY")) {
				double limit = ((Double) fieldTradingLimit.getValue())
						.doubleValue();
				if (volume * price > limit)
					labelOverlimit.setText("OVER LIMIT");
				else
					labelOverlimit.setText("");
			} else {
				double limit = ((Double) fieldStockLimit.getValue())
						.doubleValue();
				if (volume > limit)
					labelOverlimit.setText("SHORT SELL");
				else
					labelOverlimit.setText("");
			}
		} catch (Exception ex) {
			labelOverlimit.setText("");
		}
	}

	private boolean isValidEntry() {
		int i = ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK)
				.getIndexByKey(new String[] { comboStock.getText() });
		if (i < 0) {
			Utils.showMessage("please enter valid stock", form);
			comboStock.getTextEditor().requestFocus();
			return false;
		}
		i = ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getIndexByKey(
						new String[] { getAccountId(comboClient.getText()) });
		if (i < 0) {
			Utils.showMessage("please enter valid Client", form);
			comboClient.getTextEditor().requestFocus();
			return false;
		}
		if (entryType.equals("BUY")) {
			Account acc = (Account) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ACCOUNT)
					.getDataByKey(
							new String[] { getAccountId(comboClient.getText()) });
			i = ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ACCSTOCK)
					.getIndexByKey(
							new String[] { acc.getAccType(),
									comboStock.getText() });
			if (i < 0) {
				Utils.showMessage("invalid stock for this account type", form);
				comboStock.getTextEditor().requestFocus();
				return false;
			}
		}
		try {
			fieldPrice.commitEdit();
			calculate();
		} catch (Exception ex) {
		}
		try {
			fieldVolume.commitEdit();
			calculate();
		} catch (Exception ex) {
		}
		Double lot = (Double) fieldVolume.getValue();
		Double price = (Double) fieldPrice.getValue();
		if (price != null) {
			if (price.doubleValue() <= 0) {
				Utils.showMessage("price must be greater than zero", form);
				fieldPrice.requestFocus();
				return false;
			}
		} else {
			Utils.showMessage("please enter valid price", form);
			fieldPrice.requestFocus();
			return false;
		}
		if (lot != null) {
			if (lot.doubleValue() <= 0) {
				Utils.showMessage("lot must be greater than zero", form);
				fieldVolume.requestFocus();
				return false;
			}
		} else {
			Utils.showMessage("please enter valid lot", form);
			fieldVolume.requestFocus();
			return false;
		}
		if (comboBroker.getText().trim().equals("")
				|| fieldTrader.getText().trim().equals("")) {
			Utils.showMessage(
					"please enter valid contra broker & contra trader", form);
			comboBroker.requestFocus();
			return false;
		}
		if (entryType.equals("BUY") && fieldOrderid.getText().trim().equals("")) {
			Utils.showMessage("please enter valid orderid to confirm", form);
			return false;
		}
		if((Double)fieldVolume.getValue() > 1000000000){
			Utils.showMessage(
					"Nominal share pada order tidak boleh lebih dari 1 milyar!", form);
			return false;
		}
		return true;
	}

	
	void clearQuote() {
		if (!soldstock.equals("")) {
			unsubscribe();
		}
		soldstock = "";
		((FilterColumn) filterBid.getFilteredData("stock")).setField("");
		((FilterColumn) filterBid.getFilteredData("bo")).setField("B");
		((FilterColumn) filterOffer.getFilteredData("stock")).setField("");
		((FilterColumn) filterOffer.getFilteredData("bo")).setField("O");
		filterBid.fireFilterChanged();
		filterOffer.fireFilterChanged();
		chgStockSummary();
	}

	void changeQuote(final String newstock) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				String stock = newstock.trim().toUpperCase();
				if (!soldstock.toUpperCase().trim().equals(stock)) {
					stock = stock.trim().toUpperCase();
					if (!soldstock.equals("")) {
						unsubscribe();
					}
					soldstock = stock;
					((FilterColumn) filterBid.getFilteredData("stock"))
							.setField(stock);
					((FilterColumn) filterBid.getFilteredData("bo"))
							.setField("B");
					((FilterColumn) filterOffer.getFilteredData("stock"))
							.setField(stock);
					((FilterColumn) filterOffer.getFilteredData("bo"))
							.setField("O");
					filterBid.fireFilterChanged();
					filterOffer.fireFilterChanged();
					if (!soldstock.equals(""))
						subscribe();
					chgStockSummary();
				}
			}
		});
	}

	void subscribe() {
		((IEQTradeApp) apps).getFeedEngine().subscribe(FeedParser.PARSER_QUOTE,
				FeedParser.PARSER_QUOTE + "|" + soldstock + "#" + "RG");
	}

	void unsubscribe() {
		if (!soldstock.equals("")) {
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(
					FeedParser.PARSER_QUOTE,
					FeedParser.PARSER_QUOTE + "|" + soldstock + "#" + "RG");
		}
	}

	void changePF(String accountid) {
		((FilterColumn) filterPF.getFilteredData("account"))
				.setField(accountid);
		filterPF.fireFilterChanged();
	}

	void clearForm() {
		try {
			labelTA.setText("");
			labelOverlimit.setText("");
			comboClient.getTextEditor().setText("");
			fieldClientName.getTextEditor().setText("");
			fieldStockName.setText("");
			// fieldStockName.getTextEditor().setText("");
			clientfocusLost(null);
			fieldVolume.setValue(null);
			fieldPrice.setValue(null);
			comboStock.getTextEditor().setText("");
			stockfocusLost(null);
			fieldValue.setValue(null);
			fieldTradingLimit.setValue(null);
			comboBoard.setSelectedItem("NG");
			comboType.setSelectedIndex(0);
			comboIA.setSelectedIndex(0);
			comboIAContra.setSelectedIndex(0);
			checkConfirm
					.setSelected(hSetting.get("confirm") != null ? (((Boolean) hSetting
							.get("confirm")).booleanValue()) : true);
			changePF("");
			clearQuote();
			comboType.setSelectedIndex(0);
			comboType.setEnabled(true);
			comboType.setFocusable(true);
			comboBroker.getTextEditor().setText("");
			fieldTrader.setText("");
			fieldOrderid.setText("");
			// comboStock.getTextEditor().requestFocus();
			comboClient.getTextEditor().requestFocus();
			order = null;
		} catch (Exception ex) {
		}
	}

	private String genConfirm() {
		String result = "";
		result = result.concat(entryType.equals("BUY") ? "Confirm Neg Deal\n"
				: "Entry Neg Deal\n");
		result = result.concat("Client        :  " + comboClient.getText())
				.concat("\n");
		result = result.concat("Stock        :  "
				+ comboStock.getText().concat("\n"));
		result = result.concat(
				"Price         :  "
						+ formatter.format((Double) fieldPrice.getValue()))
				.concat("\n");
		result = result.concat(
				"Shares      :  "
						+ formatter.format((Double) fieldVolume.getValue()))
				.concat("\n");
		return result;
	}

	boolean _send = false;	
	public int nconfirm = Utils.C_YES_DLG;
		
		private void send() {
			 double fieldprev = 0;
			 StockSummary summ = null;
			 /*if(fieldPrev.getText().contains(",")) {
			 String[] parts = null;
			 parts = fieldPrev.getText().split(",");
			 String temp = parts[0]+parts[1];
			 fieldprev = Double.parseDouble(temp); 
			 } else {
			 fieldprev = Double.parseDouble(fieldPrev.getText());
			 }*/
			 
			try {
				if (!_send) {
					_send = true;
					if (sendAction == null || !sendAction.isEnabled()) {
						Utils.showMessage("cannot send order, please reconnect..",
								form);
						_send = false;
					} else if (isValidEntry()) {
						 summ = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByKey(new Object[] { soldstock, "RG" });
						 if(summ == null){
								if (checkConfirm.isSelected()) {
									nconfirm = Utils.showConfirmDialog(form, "Crossing Order",
											"Are you sure want to send this order?\n"
													+ genConfirm(), Utils.C_YES_DLG);
									}
									if (nconfirm == Utils.C_YES_DLG) {
										sendOrder();
									} else {
										comboStock.getTextEditor().requestFocus();
										nconfirm = 0;
									}
							}else {
								fieldprev = summ.getPrevious();
								 log.info("hitung "+((Double)fieldPrice.getValue() > (fieldprev + (fieldprev / (100/50))))+" \n" +
										 fieldprev+" - "+(fieldprev + (fieldprev / (100/50)))
										 );
								 
								//int nconfirm = Utils.C_YES_DLG;
								if ((Double)fieldPrice.getValue() > (fieldprev + (fieldprev / (100/50)))){
									HashMap param = new HashMap();
									param.put(("Result"), (entryType.equals("BUY") ? "Confirm Neg Deal\n": "Entry Neg Deal\n"));
									param.put("Client", "Client        :  " + comboClient.getText());
									param.put("Stock", "Stock        :  "
											+ comboStock.getText().concat("\n"));
									param.put("Price", "Price         :  "
									+ formatter.format((Double) fieldPrice.getValue()));
									param.put("Volume","Shares      :  "
											+ formatter.format((Double) fieldVolume.getValue()));
									param.put("form", TradingAction.A_SHOWNEGDEAL.toString());
									((TradingApplication) apps).getUI().showUI(
											TradingUI.UI_PIN_NEGO, param);
									
									/*if (checkConfirm.isSelected()) {
										 nconfirm = Utils.showConfirmDialog(form, "Neg Deal",
													"are you sure want to send this order?\n"
															+ genConfirm(), Utils.C_YES_DLG);
										} 						
									
									if (nconfirm == Utils.C_YES_DLG){sendOrder();
									}else {	comboStock.getTextEditor().requestFocus();	}*/
								}
								else{
									if (checkConfirm.isSelected()) {
									nconfirm = Utils.showConfirmDialog(form, "Neg Deal",
											"Are you sure want to send this order?\n"
													+ genConfirm(), Utils.C_YES_DLG);
									}
									System.out.println("NCONFIRM ="+nconfirm);
									if (nconfirm == Utils.C_YES_DLG) {
										sendOrder();
									} else {
										comboStock.getTextEditor().requestFocus();
										nconfirm = 0;
									}
								}
							}
						 
					}
					_send = false;
				}
			} catch (Exception ex) {
				log.error(Utils.logException(ex));
				_send = false;
			}
		}
	
	public void confirm(){
		if (checkConfirm.isSelected()){
			nconfirm = Utils.showConfirmDialog(form, "Neg Deal",
					"Are you sure want to send this order?\n"
							+ genConfirm(), Utils.C_YES_DLG);
		}
		System.out.println("CONFIRM"+nconfirm);
		if (nconfirm == Utils.C_YES_DLG) {
			sendOrder();
		} else {
			comboStock.getTextEditor().requestFocus();
			nconfirm = 0;
		}
	}

	public void sendOrder() {
		double lot = ((Double) fieldVolume.getValue()).doubleValue();
		Order order = genOrder(lot, 1, 1);
		((IEQTradeApp) apps).getTradingEngine().createOrder(order);
		clearForm();
		hide();
	}

	private Order genOrder(double volume, double counter, double totalsplit) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByKey(
						new String[] { getAccountId(comboClient.getText()) });
		Stock sec = (Stock) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK)
				.getDataByKey(new String[] { comboStock.getText() });
		Order order = new Order();
		order.setOrderIDContra("");
		order.setId(OrderIDGenerator.gen(acc.getTradingId()));
		order.setOrderDate(OrderIDGenerator.getTime());
		order.setExchange("IDX");
		order.setSplitTo(new Double(totalsplit));
		order.setBoard(comboBoard.getText());
		order.setBroker(OrderIDGenerator.getBrokerCode());
		order.setBOS(entryType.equals("BUY") ? "1" : "2");
		order.setOrdType("E");
		Mapping m = (Mapping) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ORDERTYPE)
				.getDataByField(new Object[] { comboType.getText() },
						new int[] { Mapping.CIDX_CODE });
		order.setOrderType(m.getDesc());
		order.setStock(comboStock.getText());
		order.setIsAdvertising("0");
		order.setTradingId(comboClient.getText());
		Mapping ia = (Mapping) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_TRADINGACC)
				.getDataByField(new Object[] { comboIA.getText() },
						new int[] { Mapping.CIDX_CODE });
		order.setInvType(ia.getDesc());
		order.setCurrency("IDR");
		order.setContraBroker("");
		order.setContraUser("");
		order.setPrice((Double) fieldPrice.getValue());
		order.setVolume(new Double(volume));
		order.setLot(new Double(volume / sec.getLotSize().doubleValue()));
		order.setIsBasketOrder("0");
		order.setBasketTime(null);
		order.setStatus(Order.C_ENTRY);
		order.setMarketOrderId("");
		order.setDoneVol(new Double(0));
		order.setDoneLot(new Double(0));
		order.setCounter(new Double(counter));
		order.setComplianceId(acc.getComlianceId());
		order.setAccId(acc.getAccId());
		order.setBalVol(new Double(order.getVolume().doubleValue()));
		order.setBalLot(new Double(order.getLot().doubleValue()));
		order.setTradeNo("0");
		order.setUser(((IEQTradeApp) apps).getTradingEngine().getUserId()
				.toUpperCase());
		order.setIsFloor("0");
		order.setMarketRef(order.getId());
		order.setContraBroker(comboBroker.getText().toUpperCase());
		order.setContraUser(fieldTrader.getText().toLowerCase());
		order.setNegDealRef(fieldOrderid.getText().equals("") ? "0"
				: fieldOrderid.getText().toUpperCase());
		Mapping iac = (Mapping) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_TRADINGACC)
				.getDataByField(new Object[] { comboIAContra.getText() },
						new int[] { Mapping.CIDX_CODE });
		order.setInvTypeContra(iac.getDesc());
		order.buildAmount();
		return order;
	}

	void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				InputEvent.CTRL_DOWN_MASK), "ctrlSAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_P,
				InputEvent.CTRL_DOWN_MASK), "ctrlPAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_T,
				InputEvent.CTRL_DOWN_MASK), "ctrlTAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "secAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "clientAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0), "priceAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0), "qtyAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.ALT_MASK),
				"altCAction");

		comp.getActionMap().put("altCAction", new AbstractAction("altCAction") {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnClose.doClick();
			}
		});

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.ALT_MASK),
				"altSAction");

		comp.getActionMap().put("altSAction", new AbstractAction("altSAction") {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnSend.doClick();
			}
		});

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, Event.SHIFT_MASK),
				"shiftTabAction");

		comp.getActionMap().put("shiftTabAction",
				new AbstractAction("shiftTabAction") {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (e.getSource().equals(comboStock.getTextEditor())) {
							fieldClientName.requestFocus();
						} else if (e.getSource().equals(
								fieldClientName.getTextEditor())) {
							comboClient.requestFocus();
						} else if (e.getSource().equals(
								comboBroker.getTextEditor())) {
							comboStock.requestFocus();
						}
					}
				});

		comp.getActionMap().put("secAction", new AbstractAction("secAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				comboStock.getTextEditor().requestFocus();
			}
		});
		comp.getActionMap().put("clientAction",
				new AbstractAction("clientAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						comboClient.getTextEditor().requestFocus();
					}
				});
		comp.getActionMap().put("priceAction",
				new AbstractAction("priceAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						fieldPrice.requestFocus();
					}
				});
		comp.getActionMap().put("qtyAction", new AbstractAction("qtyAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				fieldVolume.requestFocus();
			}
		});
		comp.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						btnClose.doClick();
					}
				});

		comp.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton) {
							((JButton) evt.getSource()).doClick();
						} else {
							if (evt.getSource() instanceof JNumber) {
								try {
									((JNumber) evt.getSource()).commitEdit();
								} catch (Exception ex) {
								}
								;
							}
							btnSend.doClick();
						}
					}
				});
	}

	String getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { alias },
						new int[] { Account.C_TRADINGID });
		return (acc != null ? acc.getAccId() : "");
	}

}
