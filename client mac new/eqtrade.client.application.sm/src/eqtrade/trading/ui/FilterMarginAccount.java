package eqtrade.trading.ui;

import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.table.TableModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.trading.model.Account;
import eqtrade.trading.model.Margin;
import eqtrade.trading.model.Portfolio;

public class FilterMarginAccount extends FilterBase {
	@SuppressWarnings("rawtypes")
	private HashMap mapFilter = new HashMap();
	public int rowcount =0;

	public FilterMarginAccount(Component parent, GridModel _dataPortfolio) {
		super(parent,"filter");
		
		mapFilter.put("margin", new FilterColumn("margin",
				Double.class, new Double(65), FilterColumn.C_GREATER));
		mapFilter.put("acctype", new FilterColumn("acctype",String.class,
				new String("MR"),FilterColumn.C_EQUAL));
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try{
			Column val = (Column) model.getValueAt(row, 0);
			Margin dat = (Margin) val.getSource(); 
			if( ((FilterColumn)mapFilter.get("margin") ).compare(dat.getCurrentRatio())
					&& ((FilterColumn)mapFilter.get("acctype")).compare(dat.getAccType()) )
				{avail = true;
				if(dat.getNote().equalsIgnoreCase("FORCE SELL")){
					rowcount++;	
				}
			}
			return avail;
		}catch(Exception e){
			e.printStackTrace();return false;
		}
		
	}
	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if(map!=null){
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

}
