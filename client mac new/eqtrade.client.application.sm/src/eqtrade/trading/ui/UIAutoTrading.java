package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.AWTKeyStroke;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumnModel;
import javax.swing.text.JTextComponent;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.border.LineBorder;
import com.vollux.demo.Mapping;
import com.vollux.framework.Action;
import com.vollux.framework.UI;
import com.vollux.model.FilteredAndSortedTableModel;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.IEQTradeApp;
import eqtrade.application.UIMaster;
import eqtrade.feed.app.FeedAction;
import eqtrade.feed.app.FeedApplication;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Quote;
import eqtrade.feed.model.QuoteDef;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.ui.quote.FilterQuote;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.OrderIDGenerator;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.Portfolio;
import eqtrade.trading.model.PortfolioDef;
import eqtrade.trading.model.Stock;
import eqtrade.trading.model.UserProfile;

public class UIAutoTrading extends UI {
	
	boolean isFilled = false;
	JDropDown comboStock = null;
	JText fieldStockName;
	// JDropDown fieldStockName;

	JDropDown comboClient = null;
	// JNumber fieldPrice;
	JSpinner fieldPrice;
	IDXPriceSpinnerModel priceModel;
	JNumber fieldValue;
	JNumber fieldVolume;
	// JNumber fieldLot;F
	JSpinner fieldLot;
	SpinnerModel lotModel;
	JNumber fieldSplit;
	JCheckBox checkRandom;
	// JText fieldClientName;
	JDropDown fieldClientName;

	JNumber fieldTradingLimit;
	JNumber fieldStockLimit;
	JDropDown comboBoard = null;
	JDropDown comboType = null;
	JDropDown comboIA = null;
	JCheckBox checkBasket;
	JCheckBox checkConfirm;
	JLabel labelTA;
	JLabel labelOverlimit;
	JPanel pnlEntry; // = new JPanel();

	JDropDown comboSend = null;
	JTextField fieldTimer;

	JButton btnSend;
	JButton btnClear;
	JButton btnClose;
	JButton btnAdd;

	JTabbedPane tab;
	JPanel pnlBlank;
	JPanel pnlSlank;

	JButton btnAll;
	JButton btnLimit;

	private JGrid tableBid;
	private JGrid tableOffer;
	private JLabel fieldPrev;
	private JLabel fieldHigh;
	private JLabel fieldLow;
	private JLabel fieldChange;
	private JLabel fieldOpen;
	private JLabel fieldLast;
	private JPanel pnlInfo;

	private JPanel headerEntry;
	private JLabel titleEntry;

	private String entryType = "BUY";
	private Font font = new Font("dialog", Font.BOLD, 14);

	private FilterQuote filterBid;
	private FilterQuote filterOffer;
	private static NumberFormat formatter = new DecimalFormat("#,##0 ");
	private static NumberFormat formatter2 = new DecimalFormat("#,##0.00");
	private static SimpleDateFormat formatdate = new SimpleDateFormat("HH:mm:ss");
	private Action sendAction = null;
	private String soldstock = "";

	private JGrid tablePF;
	private FilterPortfolio filterPF;	

	public UIAutoTrading(String app) {
		super("Auto Trading", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_AUTOTRADING);
	}

	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
			}
		});
	}

	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				tablePF.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	private JSkinPnl buildPF() {
		createPopup();
		filterPF = new FilterPortfolio(pnlContent);
		tablePF = createTable(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO), filterPF,(Hashtable) hSetting.get("table"));
		tablePF.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		tablePF.setColumnHide(PortfolioDef.columnhide2);

		JSkinPnl temp = new JSkinPnl(new BorderLayout());
		temp.add(tablePF, BorderLayout.CENTER);
		temp.setPreferredSize(new Dimension(300, 200));
		registerEvent(tablePF);
		registerEvent(tablePF.getTable());
		return temp;
	}

	private JSkinPnl buildQuote() {
		filterBid = new FilterQuote(pnlContent);
		filterOffer = new FilterQuote(pnlContent);
		tableBid = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_QUOTE), filterBid,(Hashtable) hSetting.get("tablebid"));
		tableOffer = createTable(((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_QUOTE), filterOffer,(Hashtable) hSetting.get("tableoff"));
		tableBid.setPreferredSize(new Dimension(120, 50));
		tableOffer.setPreferredSize(new Dimension(120, 50));
		tableBid.setMinimumSize(new Dimension(120, 50));
		tableOffer.setMinimumSize(new Dimension(120, 50));
		tableBid.getTable().getTableHeader().setReorderingAllowed(false);
		tableOffer.getTable().getTableHeader().setReorderingAllowed(false);
		tableBid.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableBid.setColumnHide(new int[] { Quote.CIDX_NUMBER, Quote.CIDX_PREV,Quote.CIDX_TYPE, Quote.CIDX_STOCK });
		tableOffer.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableOffer.setColumnHide(new int[] { Quote.CIDX_NUMBER,Quote.CIDX_PREV, Quote.CIDX_TYPE, Quote.CIDX_STOCK });
		tableBid.getTable().getModel().addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
				chgStockSummary();
			}
		});

		tableOffer.getTable().getModel().addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
				chgStockSummary();
			}
		});
		((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
				chgStockSummary();
			}
		});
		((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
				changeTL();
			}
		});
		
		fieldPrev = new JLabel(" ", JLabel.RIGHT);
		fieldHigh = new JLabel(" ", JLabel.RIGHT);
		fieldLow = new JLabel(" ", JLabel.RIGHT);
		fieldChange = new JLabel(" ", JLabel.RIGHT);
		fieldOpen = new JLabel(" ", JLabel.RIGHT);
		fieldLast = new JLabel(" ", JLabel.RIGHT);

		pnlInfo = new JPanel();
		FormLayout l = new FormLayout("50px,2dlu,pref:grow, 2dlu,pref,2dlu,pref:grow","pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(new JLabel("Last"), c.xy(1, 3));
		b.add(fieldLast, c.xy(3, 3));
		b.add(new JLabel("Hi"), c.xy(1, 5));
		b.add(fieldHigh, c.xy(3, 5));
		b.add(new JLabel("Lo"), c.xy(1, 7));
		b.add(fieldLow, c.xy(3, 7));
		b.add(new JLabel("Open"), c.xy(5, 3));
		b.add(fieldOpen, c.xy(7, 3));
		b.add(new JLabel("Close"), c.xy(5, 5));
		b.add(fieldPrev, c.xy(7, 5));
		b.add(new JLabel("Chg"), c.xy(5, 7));
		b.add(fieldChange, c.xy(7, 7));

		FormLayout layoutInfo = new FormLayout("pref:grow","pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		PanelBuilder builderInfo = new PanelBuilder(layoutInfo);
		builderInfo.setBorder(new CompoundBorder(new LineBorder(),new EmptyBorder(4, 4, 4, 4)));
		builderInfo.add(labelTA, c.xy(1, 1));
		builderInfo.add(fieldTradingLimit, c.xy(1, 3));
		builderInfo.add(fieldStockLimit, c.xy(1, 5));
		builderInfo.add(labelOverlimit, c.xy(1, 7));
		builderInfo.getPanel().setOpaque(false);

		FormLayout layout2 = new FormLayout("150px:grow, 1dlu, 150px:grow","pref, 2dlu, pref, 2dlu, fill:200px:grow");
		PanelBuilder builder2 = new PanelBuilder(layout2);
		builder2.add(builderInfo.getPanel(), c.xyw(1, 1, 3));
		builder2.add(pnlInfo, c.xyw(1, 3, 3));
		builder2.add(tableBid, c.xy(1, 5));
		builder2.add(tableOffer, c.xy(3, 5));

		JSkinPnl temp = new JSkinPnl(new BorderLayout());
		temp.setBorder(new EmptyBorder(2, 2, 2, 2));
		temp.add(builder2.getPanel(), BorderLayout.CENTER);
		temp.add(buildPF(), BorderLayout.EAST);
		registerEvent(tableBid);
		registerEvent(tableBid.getTable());
		registerEvent(tableOffer);
		registerEvent(tableOffer.getTable());
		return temp;
	}

	protected void build() {
		//System.out.println("TEST TRADING GET LOT :"+TradingSetting.getLot(TradingSetting.C_LOTSIZE).toString());
		sendAction = apps.getAction().get(TradingAction.A_SHOWBUYORDER);
		fieldTradingLimit = new JNumber(Double.class, 0, 0, true, false);
		fieldTradingLimit.setHorizontalAlignment(JLabel.CENTER);
		fieldStockLimit = new JNumber(Double.class, 0, 0, true, false);
		fieldStockLimit.setHorizontalAlignment(JLabel.CENTER);
		comboClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboStock = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		FilteredAndSortedTableModel m2 = new FilteredAndSortedTableModel(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_BOARD));
		FilterBoard filter2 = new FilterBoard(form);
		((FilterColumn) filter2.getFilteredData("code")).setField("NG");
		m2.setFilter(filter2);
		comboBoard = new JDropDown(m2, Mapping.CIDX_CODE);
		comboType = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERTYPE), Mapping.CIDX_CODE,false);
		comboIA = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_TRADINGACC), Mapping.CIDX_CODE,false);
		comboIA.setEnabled(false);
		// fieldClientName = new JText(false);
		fieldClientName = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT), Account.C_NAME, true);

		fieldStockName = new JText(false);
		// fieldStockName = new JDropDown(((IEQTradeApp)
		// apps).getTradingEngine()
		// .getStore(TradingStore.DATA_STOCK), Stock.C_NAME, true);

		labelTA = new JLabel("Trading Limit");
		labelTA.setHorizontalAlignment(JLabel.CENTER);
		labelOverlimit = new JLabel("");
		labelOverlimit.setHorizontalAlignment(JLabel.CENTER);
		labelTA.setFont(font);
		labelOverlimit.setFont(font);
		// IDXPriceSpinnerModel priceModel = new IDXPriceSpinnerModel();
		IDXPriceSpinnerModel priceModel = new IDXPriceSpinnerModel();
		// fieldPrice = new JNumber(Double.class, 0, 0, true);
		fieldPrice = new JSpinner(priceModel); // new JNumber(Double.class, 0,
												// 0, true);
		fieldValue = new JNumber(Double.class, 0, 0, true, false);
		fieldVolume = new JNumber(Double.class, 0, 0, true, false);
		// IDXLotSpinnerModel lotModel = new IDXLotSpinnerModel();
		SpinnerModel lotModel = new SpinnerNumberModel(new Double(0),new Double(0), new Double(1000000), new Double(1));
		// fieldLot = new JNumber(Double.class, 0, 0, true);
		fieldLot = new JSpinner(lotModel); // new JNumber(Double.class, 0, 0,
											// true);
		fieldSplit = new JNumber(Double.class, 0, 0, false);
		checkRandom = new JCheckBox("Random");
		checkRandom.setOpaque(false);
		comboSend = new JDropDown(((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_TIMER), Mapping.CIDX_CODE, false);
		fieldTimer = new JTextField();
		fieldTimer.setEnabled(false);
		btnSend = new JButton("Send");
		btnSend.setMnemonic('S');

		btnAll = new JButton("All Cash");
		btnAll.setMnemonic('L');
		btnAll.setFocusable(false);
		btnLimit = new JButton("All Limit");
		btnLimit.setMnemonic('T');
		btnLimit.setFocusable(false);

		btnClear = new JButton("Clear");
		btnClose = new JButton("Close");
		btnClose.setMnemonic('C');
		btnAdd = new JButton("Add");
		btnAdd.setMnemonic('A');
		checkBasket = new JCheckBox();
		checkBasket.setOpaque(false);
		checkConfirm = new JCheckBox("Confirm");
		checkConfirm.setOpaque(false);

		registerEvent(comboClient.getTextEditor());
		registerEventCombo(comboClient);
		registerEvent(comboStock.getTextEditor());
		registerEventCombo(comboStock);
		registerEvent(fieldClientName.getTextEditor());
		registerEventCombo(fieldClientName);
		registerEvent(comboBoard);
		// registerEventCombo(comboBoard);
		registerEvent(comboType);
		registerEvent(comboIA);
		registerEvent(fieldPrice);
		registerEvent(((JSpinner.DefaultEditor) fieldPrice.getEditor()).getTextField());
		registerEvent(fieldLot);
		registerEvent(((JSpinner.DefaultEditor) fieldLot.getEditor()).getTextField());
		registerEvent(fieldSplit);
		registerEvent(checkRandom);
		registerEvent(comboSend);
		registerEvent(fieldTimer);
		registerEvent(btnAdd);
		registerEvent(btnSend);
		registerEvent(btnClear);
		registerEvent(btnClose);
		registerEvent(checkBasket);
		registerEvent(checkConfirm);
		registerEvent(btnAll);
		registerEvent(btnLimit);

		fieldSplit.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				Double split = (Double) fieldSplit.getValue();
				if (split != null && split > TradingSetting.C_MAXSPLIT) {
					fieldSplit.setValue(TradingSetting.C_MAXSPLIT);
					fieldSplit.requestFocus();
				}
			}
		});

		comboSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Mapping map = (Mapping) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_TIMER).getDataByKey(new String[] { comboSend.getText() });
				if (map.getDesc().equals("0")) {
					fieldTimer.setEnabled(false);
					fieldTimer.setText("");
					fieldTimer.setFocusable(false);
					checkBasket.setSelected(false);
				} else {
					checkBasket.setSelected(true);
					fieldTimer.setFocusable(true);
					fieldTimer.setEnabled(true);
				}
			}
		});

//		comboBoard.addFocusListener(new FocusAdapter() {

//			public void focusGained(FocusEvent e) {
//				// try {
//				comboBoard.showPopup();
//				// } catch (Exception ex) {
//				// }
//			}
//			


//		});
		comboClient.getTextEditor().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				clientfocusLost(arg0);
			}

			public void focusGained(FocusEvent e) {
				try {
					if (((IEQTradeApp) apps).getTradingEngine()
							.getStore(TradingStore.DATA_ACCOUNT).getRowCount() == 1) {
						comboClient.setSelectedIndex(0);
					} else {
						comboClient.showPopup();
					}
				} catch (Exception ex) {
				}
			}
		});

		fieldClientName.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				try {
					fieldClientName.showPopup();
				} catch (Exception ex) {
				}

			}

			@Override
			public void focusLost(FocusEvent e) {
				clientfocusLost(e);
			}

		});

		comboStock.getTextEditor().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				stockfocusLost(arg0);
			}

			public void focusGained(FocusEvent e) {
				try {
					comboStock.showPopup();
				} catch (Exception ex) {
				}
			}
		});

		/*
		 * fieldStockName.getTextEditor().addFocusListener(new FocusAdapter() {
		 * 
		 * @Override public void focusGained(FocusEvent e) { try {
		 * fieldStockName.showPopup(); }catch (Exception ex) { } }
		 * 
		 * @Override public void focusLost(FocusEvent e) { stockfocusLost(e); }
		 * 
		 * });
		 */


		/*
		 * fieldLot.addFocusListener(new FocusAdapter() { public void
		 * focusLost(FocusEvent arg0) { calculate(); } });
		 */
		fieldLot.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				calculate();
			}

			public void focusGained(FocusEvent arg0) {
				((JSpinner) arg0.getSource()).transferFocus();
			}
		});
		fieldLot.setFocusable(false);
		((JSpinner.DefaultEditor) fieldLot.getEditor()).getTextField().addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				// System.out.println("here");
				if (e.getSource() instanceof JTextComponent) {
					final JTextComponent textComponent = ((JTextComponent) e
							.getSource());
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							textComponent.selectAll();
						}
					});
				}
			}

					public void focusLost(FocusEvent arg0) {
						calculate();
					}
				});

		checkBasket.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (checkBasket.isSelected()) {
					comboSend.setSelectedIndex(1);
				} else {
					comboSend.setSelectedIndex(0);
				}
			}
		});

		/*
		 * fieldPrice.addFocusListener(new FocusAdapter() { public void
		 * focusLost(FocusEvent arg0) { calculate(); } });
		 */
		fieldPrice.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				stockfocusLost(arg0);
				calculate();
			}

			public void focusGained(FocusEvent arg0) {
				((JSpinner) arg0.getSource()).transferFocus();
			}
		});
		fieldPrice.setFocusable(false);
		((JSpinner.DefaultEditor) fieldPrice.getEditor()).getTextField()
				.addFocusListener(new FocusAdapter() {
					public void focusGained(FocusEvent e) {
						if (e.getSource() instanceof JTextComponent) {
							final JTextComponent textComponent = ((JTextComponent) e
									.getSource());
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									textComponent.selectAll();
								}
							});
						}
					}

					public void focusLost(FocusEvent arg0) {
						stockfocusLost(arg0);
						calculate();
					}
				});

		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				send(true);
				//jalan();
			}
		});
		comboBoard.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clearQuote();
	
				if(comboBoard.getSelectedItem().equals("TN"))
				{
					comboType.setSelectedIndex(1);
					comboType.setEnabled(true);
					comboBoard.setFocusable(false);
					
					
				}else
				{
					comboType.setSelectedIndex(0);
					comboType.setEnabled(true);
					comboBoard.setFocusable(false);

				}
				BestQuote(comboStock.getText());
				changeQuote(comboStock.getText());
				
			
				
				
			}
		});
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnAdd.setEnabled(false);
				send(false);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				btnAdd.setEnabled(true);
				fieldLot.setValue(new Double(0));
				fieldPrice.setValue(new Double(0));

			}
		});

		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clearForm();
			}
		});

		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveSetting();
				clearForm();
				comboClient.getTextEditor().setText("");
				fieldClientName.getTextEditor().setText("");
				form.setVisible(false);
			}
		});

		btnAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (entryType.equalsIgnoreCase("BUY")) {
					Double clientAC = new Double(0);
					Double stockLotSize = new Double(-1);
					int i = ((IEQTradeApp) apps)
							.getTradingEngine()
							.getStore(TradingStore.DATA_ACCOUNT)
							.getIndexByKey(
									new String[] { getAccountId(comboClient
											.getText()) });
					if (i >= 0) {
						Account acc = (Account) ((IEQTradeApp) apps)
								.getTradingEngine()
								.getStore(TradingStore.DATA_ACCOUNT)
								.getDataByIndex(i);
						if (acc != null) {
							clientAC = acc.getNetAC();
						}
					}
					int j = ((IEQTradeApp) apps)
							.getTradingEngine()
							.getStore(TradingStore.DATA_STOCK)
							.getIndexByKey(
									new String[] { comboStock.getText() });
					if (j > 0) {
						Stock stock = (Stock) ((IEQTradeApp) apps)
								.getTradingEngine()
								.getStore(TradingStore.DATA_STOCK)
								.getDataByIndex(j);
						if (stock != null) {
							stockLotSize = stock.getLotSize();
						}
					}
					// String strPrice =
					// ((JSpinner.DefaultEditor)fieldPrice.getEditor()).getTextField().getText();
					// Double pri = Double.parseDouble(strPrice);
					Double pri = (Double) fieldPrice.getValue();
					if (clientAC >= pri * stockLotSize) {
						fieldLot.setValue(new Double(Math.floor(clientAC / pri
								/ stockLotSize)));
						((JSpinner.DefaultEditor) fieldLot.getEditor())
								.getTextField().setValue(
										new Double(Math.floor(clientAC / pri
												/ stockLotSize)));
					} else {
						fieldLot.setValue(new Double(0));
						((JSpinner.DefaultEditor) fieldLot.getEditor())
								.getTextField().setValue(new Double(0));
					}
					calculate();
				} else {
					fieldLot.setValue(fieldStockLimit.getValue());
					((JSpinner.DefaultEditor) fieldLot.getEditor())
							.getTextField()
							.setValue(fieldStockLimit.getValue());
				}
				calculate();
			}
		});

		btnLimit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// String strPrice =
				// ((JSpinner.DefaultEditor)fieldPrice.getEditor()).getTextField().getText();
				Double pri = (Double) fieldPrice.getValue();
				// Double pri = dblPrice; //Double.parseDouble(strPrice);
				if (pri <= 0) {
					((JSpinner.DefaultEditor) fieldLot.getEditor())
							.getTextField().setValue(new Double(0));
					calculate();
					return;
				}
				int i = ((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_STOCK)
						.getIndexByKey(new String[] { comboStock.getText() });
				if (i >= 0) {
					Stock stock = (Stock) ((IEQTradeApp) apps)
							.getTradingEngine()
							.getStore(TradingStore.DATA_STOCK)
							.getDataByIndex(i);
					if (stock != null) {
						Double tel = ((Double) fieldTradingLimit.getValue())
								.doubleValue();
						fieldLot.setValue(new Double(Math.floor(tel / pri
								/ stock.getLotSize())));
						((JSpinner.DefaultEditor) fieldLot.getEditor())
								.getTextField().setValue(
										new Double(Math.floor(tel / pri
												/ stock.getLotSize())));
					}
				}
				calculate();
			}
		});

		headerEntry = new JPanel(new BorderLayout());
		headerEntry.setBackground(entryType.equals("BUY") ? Color.red.darker()
				: Color.green.darker());
		titleEntry = new JLabel(entryType + " ORDER");
		titleEntry.setHorizontalAlignment(JLabel.CENTER);
		titleEntry.setFont(new Font("dialog", Font.BOLD, 20));
		headerEntry.setBorder(new EmptyBorder(10, 10, 10, 10));
		headerEntry.add(titleEntry, BorderLayout.CENTER);
		headerEntry.setPreferredSize(new Dimension(100, 60));

		pnlEntry = new JPanel();
		FormLayout lay = new FormLayout("pref, 2dlu, 75px, 2dlu, pref, 2dlu, 100px,2dlu,pref","fill:pref:grow, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref,2dlu,pref,2dlu,pref");

		JSkinPnl temp2 = new JSkinPnl();
		FormLayout lay2 = new FormLayout(
				"pref, 2dlu, 30px, 2dlu, pref, 2dlu, 60px, 2dlu, pref:grow, 2dlu, 55px",
				"pref, 2dlu, pref");
		temp2.setBorder(new CompoundBorder(new LineBorder(), new EmptyBorder(4,
				4, 4, 4)));

		PanelBuilder builder2 = new PanelBuilder(lay2, temp2);
		CellConstraints cc = new CellConstraints();

		/*
		 * if (apps.getAction()
		 * .get(TradingAction.A_SHOWSPLITORDER).isGranted()) {
		 * 
		 * builder2.add(new JLabel("Split To"), cc.xy(1, 1));
		 * builder2.add(fieldSplit, cc.xy(3, 1)); builder2.add(checkRandom,
		 * cc.xyw(5, 1, 3)); }
		 */

		UserProfile split = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "36" },
						new int[] { UserProfile.C_MENUID });

		// if (split.isAllowed()) {
		builder2.add(new JLabel("Split To"), cc.xy(1, 1));
		builder2.add(fieldSplit, cc.xy(3, 1));
		builder2.add(checkRandom, cc.xyw(5, 1, 3));
		// }

		builder2.add(new JLabel("Put in basket"), cc.xy(1, 3));
		builder2.add(checkBasket, cc.xy(3, 3));
		builder2.add(new JLabel("Sending"), cc.xy(5, 3));
		builder2.add(comboSend, cc.xyw(7, 3, 3));
		builder2.add(fieldTimer, cc.xy(11, 3));

		PanelBuilder builder = new PanelBuilder(lay, pnlEntry);
		/*
		 * builder.setBorder(new EmptyBorder(4, 4, 4, 4)); builder.add(new
		 * JLabel("Board"), cc.xy(1, 3)); builder.add(comboBoard, cc.xy(3, 3));
		 * builder.add(new JLabel("Stock"), cc.xy(1, 5));
		 * builder.add(comboStock, cc.xy(3, 5)); builder.add(fieldStockName,
		 * cc.xywh(5, 5, 5, 1)); builder.add(new JLabel("Price"), cc.xy(1, 7));
		 * builder.add(fieldPrice, cc.xy(3, 7)); builder.add(new
		 * JLabel("Quantity"), cc.xy(1, 9)); builder.add(fieldLot, cc.xy(3, 9));
		 * builder.add(new JLabel("Lot(s)"), cc.xy(5, 9));
		 * builder.add(fieldVolume, cc.xy(7, 9)); builder.add(new
		 * JLabel("Share(s)"), cc.xy(9, 9)); builder.add(new JLabel("Client"),
		 * cc.xy(1, 11)); builder.add(comboClient, cc.xy(3, 11));
		 * builder.add(fieldClientName, cc.xywh(5, 11, 5, 1)); builder.add(new
		 * JLabel("Trading Acc"), cc.xy(1, 13)); builder.add(comboIA, cc.xy(3,
		 * 13)); builder.add(new JLabel("Expire"), cc.xy(5, 13));
		 * builder.add(comboType, cc.xy(7, 13)); builder.add(new
		 * JLabel("Amount"), cc.xy(1, 15)); builder.add(fieldValue, cc.xyw(3,
		 * 15, 7)); builder.add(builder2.getPanel(), cc.xyw(1, 17, 9));
		 * pnlEntry.add(headerEntry, cc.xyw(1, 1, 9));
		 */
		builder.setBorder(new EmptyBorder(4, 4, 4, 4));
		builder.add(new JLabel("Board"), cc.xy(1, 3));
		builder.add(comboBoard, cc.xy(3, 3));
		builder.add(new JLabel("Client"), cc.xy(1, 5));
		builder.add(comboClient, cc.xy(3, 5));
		builder.add(fieldClientName, cc.xywh(5, 5, 5, 1));
		builder.add(new JLabel("Stock"), cc.xy(1, 7));
		builder.add(comboStock, cc.xy(3, 7));
		builder.add(fieldStockName, cc.xywh(5, 7, 5, 1));
		builder.add(new JLabel("Price"), cc.xy(1, 9));
		builder.add(fieldPrice, cc.xy(3, 9));
		builder.add(btnAll, cc.xy(5, 9));
		builder.add(btnLimit, cc.xy(7, 9));
		builder.add(new JLabel("Quantity"), cc.xy(1, 11));
		builder.add(fieldLot, cc.xy(3, 11));
		builder.add(new JLabel("Lot(s)"), cc.xy(5, 11));
		builder.add(fieldVolume, cc.xy(7, 11));
		builder.add(new JLabel("Share(s)"), cc.xy(9, 11));
		builder.add(new JLabel("Trading Acc"), cc.xy(1, 13));
		builder.add(comboIA, cc.xy(3, 13));
		builder.add(new JLabel("Expire"), cc.xy(5, 13));
		builder.add(comboType, cc.xy(7, 13));
		builder.add(new JLabel("Amount"), cc.xy(1, 15));
		builder.add(fieldValue, cc.xyw(3, 15, 7));
		builder.add(builder2.getPanel(), cc.xyw(1, 17, 9));
		pnlEntry.add(headerEntry, cc.xyw(1, 1, 9));

		JSkinPnl temp3 = new JSkinPnl();
		FormLayout lay3 = new FormLayout("pref, 2dlu, pref:grow, 2dlu, pref, 2dlu, pref, 2dlu, pref","pref");

		PanelBuilder builder3 = new PanelBuilder(lay3, temp3);
		builder3.add(checkConfirm, cc.xy(1, 1));
		builder3.add(btnAdd, cc.xy(5, 1));
		builder3.add(btnSend, cc.xy(7, 1));
		builder3.add(btnClose, cc.xy(9, 1));
		builder.add(temp3, cc.xywh(1, 19, 9, 1));

		pnlBlank = new JPanel();
		pnlSlank = new JPanel();
		tab = new JTabbedPane();
		tab.addTab("                         BUY                        ",pnlBlank);
		tab.addTab("                         SELL                        ",pnlSlank);
		pnlBlank.add(pnlEntry);
		tab.addChangeListener(changeListener);
		setupTabTraversalKeys(tab);

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(10, 10, 10, 10));
		pnlContent.add(tab, BorderLayout.CENTER);
		// pnlContent.add(pnlEntry, BorderLayout.CENTER);
		pnlContent.add(buildQuote(), BorderLayout.EAST);

		clearForm();
		refresh();
	}

	void changeTL() {
		String acc = getAccountId(comboClient.getText().toUpperCase().trim());
		if (!acc.equals("")) {
			Account ac = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByKey(new String[] { acc });
			if (ac != null) {
				fieldTradingLimit.setValue(ac.getCurrTL());
			}
		}
	}

	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tablebid", QuoteDef.getTableBidDef());
			hSetting.put("tableoff", QuoteDef.getTableOffDef());
			hSetting.put("table", PortfolioDef.getTableDef());
			hSetting.put("typeEntry", "BUY");
			hSetting.put("confim", new Boolean(true));
		}
		entryType = (String) hSetting.get("typeEntry");
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 650, 500));
	}

	private Hashtable getCustomProperties() {
		Hashtable h = new Hashtable();
		TableColumnModel mod = tableOffer.getTable().getColumnModel();
		int[] width = QuoteDef.defaultColumnWidthOff;
		for (int i = 0; i < mod.getColumnCount(); i++) {
			String str = mod.getColumn(i).getHeaderValue().toString();
			int length = (mod.getColumn(i).getPreferredWidth() == 0) ? 0 : mod.getColumn(i).getWidth();
			if (str.equals("Offer")) {
				width[4] = length;
			} else if (str.equals("#O")) {
				width[6] = length;
			} else if (str.equals("Lot")) {
				width[5] = length;
			}
		}
		h.put("width", width);
		h.put("order", QuoteDef.defaultColumnOrderOff);
		h.put("font", tableOffer.getTableFont());
		h.put("header", QuoteDef.dataHeaderOff);
		h.put("alignment", QuoteDef.defaultHeaderAlignment);
		h.put("sorted", QuoteDef.columnsort);
		h.put("sortcolumn", new Integer(Quote.CIDX_NUMBER));
		h.put("sortascending", new Boolean(false));

		return h;
	}

	public void saveSetting() {
		hSetting.put("size",new Rectangle(form.getX(), form.getY(), form.getWidth(), form.getHeight()));
		hSetting.put("typeEntry", entryType);
		hSetting.put("table", tablePF.getTableProperties());
		hSetting.put("tablebid", tableBid.getTableProperties());
		hSetting.put("tableoff", getCustomProperties()); // tableOffer.getTableProperties());
		hSetting.put("confirm", new Boolean(checkConfirm.isSelected()));
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),hSetting);
		BQstock = "";
	}

	public void refresh() {
		tableBid.getViewport().setBackground(TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tableOffer.getViewport().setBackground(TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		pnlInfo.setBackground(TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tableBid.setNewFont(TradingSetting.getFont());
		tableOffer.setNewFont(TradingSetting.getFont());
		tablePF.getViewport().setBackground(TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tablePF.setNewFont(TradingSetting.getFont());
		chgStockSummary();
	}

	protected void initUI() {
		super.initUI();
		form.setResizable(false);
		form.getBtnClose().setVisible(false);
		form.pack();
	}

	@Override
	public void close() {
		super.close();
		comboClient.getTextEditor().setText("");
		fieldClientName.getTextEditor().setText("");
	}

	public static String BQstock;

	public void show(Object param) {
		HashMap p = (HashMap) param;
		entryType = (String) p.get("TYPE");
		UserProfile user = (UserProfile) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_USERPROFILE).getDataByField(new Object[] { "40" },new int[] { UserProfile.C_MENUID });

		if (user.getProfileId().equalsIgnoreCase("SALES")|| user.getProfileId().equalsIgnoreCase("SUPERUSER")) {
		} else {
			comboIA.setEnabled(false);
		}

		if (p.get("STOCK") != null) {
			comboStock.setSelectedItem((String) p.get("STOCK"));
			comboStock.getTextEditor().setText((String) p.get("STOCK"));
			fieldPrice.setValue(p.get("PRICE"));
			isFilled = true;
			soldstock = (String) p.get("STOCK");
		} else {
			comboStock.setSelectedItem("");
			comboStock.getTextEditor().setText("");
			fieldPrice.setValue(new Double(0));
			isFilled = false;
			soldstock = "";
		}
		
		try {
			if (!p.get("bestqoute").equals("")) {
				BestQuote(soldstock);
			} else {
				BQstock = "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// comboClient.getTextEditor().requestFocus();
		show();
	}

	public void show() {
		if (!apps.getAction().get(TradingAction.A_SHOWBUYORDER).isGranted())
			return;
		if (entryType.equals("BUY")) {
			tab.setSelectedIndex(0);
		} else {
			tab.setSelectedIndex(1);
		}
		changeForm();
		super.show();
		comboClient.getTextEditor().setText("");
		fieldClientName.getTextEditor().setText("");
		comboClient.requestFocus();
	}

	void changeForm() {
		form.setTitle(entryType + " ORDER");
		titleEntry.setText(entryType + " ORDER");
		headerEntry.setBackground(entryType.equals("BUY") ? Color.red.darker(): Color.green.darker());
		pnlEntry.setBackground(headerEntry.getBackground());
		if (entryType.equals("BUY")) {
			labelTA.setText("Trading Limit");
			fieldTradingLimit.setVisible(true);
			fieldStockLimit.setVisible(false);
			btnAll.setText("All Cash");
			btnLimit.setVisible(true);
		} else {
			labelTA.setText("Stock Balance");
			fieldTradingLimit.setVisible(false);
			fieldStockLimit.setVisible(true);
			btnAll.setText("All Stock");
			btnLimit.setVisible(false);
		}
		resetFields();
		calculate();
	}

	void clientfocusLost(FocusEvent e) {
		try {
			comboClient.getTextEditor().setText(comboClient.getText().toUpperCase());
			Account acc = null;
			int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getIndexByKey(new String[] { getAccountId(comboClient.getText()) });
			if (i >= 0) {
				acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByIndex(i);
				if (acc != null) {
					// fieldClientName.setText(acc.getName());
					// System.out.println("Masuk oke");
					fieldClientName.setSelectedItem(acc.getName());
					fieldTradingLimit.setValue(acc.getCurrTL());
					comboIA.setSelectedIndex(acc.getInvType().equals("I") ? 0: 1);
					// ClientAC = acc.getNetAC();
				}
			} else {
				// fieldClientName.setText("");
				// System.out.println("Masuk false");
				fieldClientName.getTextEditor().setText("");
				fieldTradingLimit.setValue(null);
				// ClientAC = new Double(0);
			}
			changeMaxLot();
			changePF(acc == null ? "" : acc.getTradingId());
			calculate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyyMMdd");
	public final static SimpleDateFormat fridayFormat = new SimpleDateFormat("E");

	void stockfocusLost(FocusEvent e) {
		try {
			if (!comboStock.getText().trim().equals("")) {
				comboStock.getTextEditor().setText(comboStock.getText().toUpperCase());
				int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getIndexByKey(new String[] { comboStock.getText() });
				if (i >= 0) {
					Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getDataByIndex(i);
					if (stock != null) {						
						fieldStockName.setText(stock.getName());
						// set put in basket here
						String brid = comboBoard.getText();
						boolean stockpreopening = stock.isPreopening();
						String dt = dateFormat2.format(OrderIDGenerator.getTime());
						boolean friday = fridayFormat.format(OrderIDGenerator.getTime()).toUpperCase().equals("FRI");
						String dtop = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_FIRST_PREOP); // "09:10:00";
						String dtses1 = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_FIRST_SES1); // "09:30:00";
						String dtses2 = dt
								+ "-"
								+ (friday ? TradingSetting
										.getDateSession(TradingSetting.C_FIRST_FRIDAY_SES2)/* "14:00:00" */
										: TradingSetting
												.getDateSession(TradingSetting.C_FIRST_SES2)/* "13:30:00 */); // (friday
																												// ?
																												// "14:00:00"
																												// :
																												// "13:30:00");
						String dtendses1 = dt
								+ "-"
								+ (friday ? TradingSetting
										.getDateSession(TradingSetting.C_END_FRIDAY_SES1) /* "11:30:00" */
										: TradingSetting
												.getDateSession(TradingSetting.C_END_SES1) /* "12:00:00" */);// (friday
																												// ?
						String dtendses2 = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_END_SES2); // "11:30:00"
																					// :
																					// "12:00:00");
						String dtendpre = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_END_PREOP); // "09:25:00";

						String dtpost_first = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_FIRST_POSTTRADING);

						String dtpost_end = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_END_POSTTRADING);

						String preclosing_end = dt
								+ "-"
								+ TradingSetting
										.getDateSession(TradingSetting.C_END_PRECLOSING);

						Date datepreopening = dateFormatAll.parse(dtop);
						Date dateses1 = dateFormatAll.parse(dtses1);
						Date dateses2 = dateFormatAll.parse(dtses2);
						Date enddateses1 = dateFormatAll.parse(dtendses1);
						Date enddateses2 = dateFormatAll.parse(dtendses2);
						Date endpre = dateFormatAll.parse(dtendpre);
						Date postfirst = dateFormatAll.parse(dtpost_first);
						Date postend = dateFormatAll.parse(dtpost_end);
						Date preclosing = dateFormatAll.parse(preclosing_end);
						Date datecurrent = OrderIDGenerator.getTime();// dateFormatAll.parse(dt+"-"+UIMaster.lblTime.getText().trim());
						// log.info("datecurrent "+datecurrent+" "+enddateses2+" "+postfirst+" "+brid+" "+());

						if (datecurrent.getTime() < datepreopening.getTime()) {
							checkBasket.setSelected(true);
							fieldTimer
									.setText(stockpreopening ? TradingSetting
											.getDateSession(TradingSetting.C_FIRST_PREOP)
											: TradingSetting
													.getDateSession(TradingSetting.C_FIRST_SES1));
							comboSend.setSelectedIndex(1);
						} else if (datecurrent.getTime() < endpre.getTime()) {
							if (!stockpreopening) {
								checkBasket.setSelected(true);
								fieldTimer
										.setText(TradingSetting
												.getDateSession(TradingSetting.C_FIRST_SES1));
								comboSend.setSelectedIndex(1);
							}else{
								checkBasket.setSelected(false);
								fieldTimer.setText("");
								comboSend.setSelectedIndex(0);
								}
						} else if (datecurrent.getTime() > endpre.getTime()
								&& datecurrent.getTime() < dateses1.getTime()) {
							checkBasket.setSelected(true);
							fieldTimer.setText(dtendses1);
							fieldTimer
									.setText(TradingSetting
											.getDateSession(TradingSetting.C_FIRST_SES1));
							comboSend.setSelectedIndex(1);
						} else if (datecurrent.getTime() > enddateses1
								.getTime()
								&& datecurrent.getTime() < dateses2.getTime()) {
							checkBasket.setSelected(true);
							fieldTimer
									.setText(friday ? TradingSetting
											.getDateSession(TradingSetting.C_FIRST_FRIDAY_SES2)
											: TradingSetting
													.getDateSession(TradingSetting.C_FIRST_SES2));
							comboSend.setSelectedIndex(1);
						} else if ((datecurrent.getTime() > preclosing
								.getTime() && datecurrent.getTime() < postfirst
								.getTime())
								&& brid.equals("RG")) {
							checkBasket.setSelected(true);
							fieldTimer
									.setText(TradingSetting
											.getDateSession(TradingSetting.C_FIRST_POSTTRADING));
							comboSend.setSelectedIndex(1);
						} else {
							checkBasket.setSelected(false);
							comboSend.setSelectedIndex(0);
						}

					}
				} else {
					fieldStockName.setText("");
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							comboStock.getTextEditor().requestFocus();
						}
					});
				}
				comboStock.hidePopup();
				calculate();
				changeMaxLot();
				/*if (comboStock.getText().endsWith("-R")) {
					comboBoard.setSelectedItem("TN");
					comboType.setSelectedIndex(1);
					comboType.setEnabled(false);
					comboType.setFocusable(false);
				} else {
					comboBoard.setSelectedItem("RG");
					comboType.setSelectedIndex(0);
					comboType.setEnabled(true);
					comboType.setFocusable(true);
				}*/
			}
			changeQuote(comboStock.getText());
			changePrice(comboStock.getText());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void changePrice(String stock) {
		if (!soldstock.equals(stock)) {
			fieldLot.setValue(0);
			// if (!isFilled) {
			StockSummary summ = (StockSummary) ((IEQTradeApp) apps)
					.getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY)
					.getDataByKey(new Object[] { stock, comboBoard.getText() });
			if (summ != null) {
				Double harga = entryType.equals("SELL") ? summ.getBestBid()
						: summ.getBestOffer();
				if (harga < 1)
					harga = summ.getPrevious();
				fieldPrice.setValue(harga);
			} else {
				// System.out.println("masuk 2 " +stock);
				fieldPrice.setValue(0);
			}
		}
	}

	private void chgStockSummary() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				StockSummary summ = (StockSummary) ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new Object[] { soldstock, comboBoard.getText() });
				if (summ != null) {
					fieldLast.setText(formatter.format(summ.getClosing()
							.doubleValue()));
					fieldChange.setText(getChange(summ.getClosing()
							.doubleValue(), summ.getPrevious().doubleValue()));
					fieldPrev.setText(formatter.format(summ.getPrevious()
							.doubleValue()));

					fieldOpen.setText(formatter.format(summ.getOpening()
							.doubleValue()));
					fieldHigh.setText(formatter.format(summ.getHighest()
							.doubleValue()));
					fieldLow.setText(formatter.format(summ.getLowest()
							.doubleValue()));

					fieldLast
							.setForeground(summ.getClosing().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getClosing().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldChange
							.setForeground(summ.getClosing().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getClosing().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldPrev.setForeground(FeedSetting
							.getColor(FeedSetting.C_ZERO));

					fieldOpen
							.setForeground(summ.getOpening().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getOpening().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldHigh
							.setForeground(summ.getHighest().doubleValue() > summ
									.getClosing().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getHighest().doubleValue() < summ
									.getClosing().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldLow.setForeground(summ.getLowest().doubleValue() > summ
							.getPrevious().doubleValue() ? FeedSetting
							.getColor(FeedSetting.C_PLUS)
							: (summ.getLowest().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
				} else {
					fieldOpen.setText("- ");
					fieldHigh.setText("- ");
					fieldLow.setText("- ");
					fieldLast.setText("- ");
					fieldChange.setText("- ");
					fieldPrev.setText("- ");
					fieldOpen.setText("- ");
					fieldHigh.setText("- ");
					fieldLow.setText("- ");
				}
			}
		});
	}

	private String getChange(double slastprice, double nprev) {
		String schange = " ";
		if (slastprice > 0) {
			double ntemp = slastprice - nprev;
			double ndbl = ((double) (ntemp * 100) / nprev);
			int ntmpdbl = (int) (ndbl * 100);
			double npersen = (double) ntmpdbl / 100;
			if (ntemp > 0) {
				schange = "+" + formatter.format(ntemp) + "("
						+ formatter2.format(npersen) + "%) ";
			} else if (ntemp == 0) {
				schange = "" + formatter.format(ntemp) + "(0.00%) ";
			} else {
				schange = "" + formatter.format(ntemp) + "("
						+ formatter2.format(npersen) + "%) ";
			}
		}
		return schange;
	}

	void changeMaxLot() {
		try {
			int i = ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_PORTFOLIO)
					.getIndexByKey(
							new String[] { getAccountId(comboClient.getText()),
									comboStock.getText() });
			if (i >= 0) {
				Portfolio pf = (Portfolio) ((IEQTradeApp) apps)
						.getTradingEngine()
						.getStore(TradingStore.DATA_PORTFOLIO)
						.getDataByIndex(i);
				if (pf != null) {
					double lotsize = Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));
					int lt = ((IEQTradeApp) apps)
							.getTradingEngine()
							.getStore(TradingStore.DATA_STOCK)
							.getIndexByKey(
									new String[] { comboStock.getText() });
					if (lt >= 0) {
						Stock stock = (Stock) ((IEQTradeApp) apps)
								.getTradingEngine()
								.getStore(TradingStore.DATA_STOCK)
								.getDataByIndex(lt);
						if (stock != null) {
							lotsize = (stock.getLotSize().doubleValue());
						}
					}
					fieldStockLimit.setValue(Math.floor(new Double(pf
							.getAvailableVolume().doubleValue() / lotsize)));
				}
			} else {
				fieldStockLimit.setValue(new Double(0));
			}
		} catch (Exception ex) {
		}
	}

	private Object floor(Double double1) {
		return null;
	}

	void calculate() {
		double lotSize = -1, lot = 0, price = 0, volume = 0;
		try {
			fieldLot.commitEdit();
			fieldPrice.commitEdit();
			lot = ((Double) fieldLot.getValue()).doubleValue(); // Double.parseDouble(strLot);
			price = (Double) fieldPrice.getValue(); // Double.parseDouble(strPrice);
			int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getIndexByKey(new String[] { comboStock.getText() });
			if (i >= 0) {
				Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getDataByIndex(i);
				if (stock != null) {
					lotSize = (stock.getLotSize().doubleValue());
				}
			}
			if (lotSize != -1) {
				volume = lotSize * lot;
				fieldValue.setValue(new Double(volume * price));
				fieldVolume.setValue(new Double(volume));
			} else {
				fieldValue.setValue(null);
				fieldVolume.setValue(null);
			}
		} catch (Exception ex) {
			fieldValue.setValue(null);
			fieldVolume.setValue(null);
			ex.printStackTrace();
		}
		try {
			if (entryType.equals("BUY")) {
				double limit = ((Double) fieldTradingLimit.getValue()).doubleValue();
				if (volume * price > limit)
					labelOverlimit.setText("OVER LIMIT");
				else
					labelOverlimit.setText("");
			} else {
				double limit = ((Double) fieldStockLimit.getValue()).doubleValue();
				if (lot > limit)
					labelOverlimit.setText("SHORT SELL");
				else
					labelOverlimit.setText("");
			}
		} catch (Exception ex) {
			labelOverlimit.setText("");
			ex.printStackTrace();
		}
	}

	private boolean isValidEntry() {
		int i = ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getIndexByKey(new String[] { comboStock.getText() });
		if (i < 0) {
			Utils.showMessage("please enter valid stock", form);
			comboStock.getTextEditor().requestFocus();
			return false;
		}
		i = ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getIndexByKey(
						new String[] { getAccountId(comboClient.getText()) });
		if (i < 0) {
			Utils.showMessage("please enter valid Client", form);
			comboClient.getTextEditor().requestFocus();
			return false;
		}
		if (entryType.equals("BUY")) {
			Account acc = (Account) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ACCOUNT)
					.getDataByKey(
							new String[] { getAccountId(comboClient.getText()) });
			i = ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ACCSTOCK)
					.getIndexByKey(
							new String[] { acc.getAccType(),
									comboStock.getText() });
			if (i < 0) {
				Utils.showMessage("invalid stock for this account type", form);
				comboStock.getTextEditor().requestFocus();
				return false;
			}
		}
		try {
			fieldPrice.commitEdit();
			calculate();
		} catch (Exception ex) {
		}
		try {
			fieldLot.commitEdit();
			calculate();
		} catch (Exception ex) {
		}
		try {
			fieldSplit.commitEdit();
		} catch (Exception ex) {
		}
		Double split = (Double) fieldSplit.getValue();
		// String strLot =
		// ((JSpinner.DefaultEditor)fieldLot.getEditor()).getTextField().getText();
		Double lot = (Double) fieldLot.getValue(); // Double.parseDouble(strLot);
		// Double lot = (Double) fieldLot.getValue();
		if (lot != null) {
			if (lot <= 0) {
				Utils.showMessage("lot must be greater than zero", form);
				fieldLot.requestFocus();
				return false;
			}
		} else {
			Utils.showMessage("please enter valid lot", form);
			fieldLot.requestFocus();
			return false;
		}
		if (split != null) {
			if (split.doubleValue() <= 0) {
				Utils.showMessage("split value must be greater than zero", form);
				fieldSplit.requestFocus();
				return false;
			} else if (split.doubleValue() > TradingSetting.C_MAXSPLIT) {
				Utils.showMessage("split value must be lower than or equal to "
						+ TradingSetting.C_MAXSPLIT, form);
				fieldSplit.requestFocus();
				return false;
			} else if (split.doubleValue() > lot.doubleValue()) {
				Utils.showMessage(
						"split value must be lower than or equal to total lot",
						form);
				fieldSplit.requestFocus();
				return false;
			}
		} else {
			Utils.showMessage("please enter valid split value", form);
			fieldSplit.requestFocus();
			return false;
		}
		Double price = (Double) fieldPrice.getValue();
		if (price != null) {
			if (price.doubleValue() <= 0) {
				Utils.showMessage("price must be greater than zero", form);
				fieldPrice.requestFocus();
				return false;
			}
		} else {
			Utils.showMessage("please enter valid price", form);
			fieldPrice.requestFocus();
			return false;
		}
		if (comboSend.getSelectedIndex() != 0) {
			try {
				Date test = formatdate.parse(fieldTimer.getText());
				String tests = formatdate.format(test);
				if (!tests.equals(fieldTimer.getText()))
					throw new Exception("invalid format");
			} catch (Exception ex) {
				Utils.showMessage(
						"invalid timer format, please use pattern HH:mm:ss\nfor example : 14:25:00",
						form);
				return false;
			}
		}

		if ((((Double) fieldLot.getValue() / (Double) fieldSplit.getValue()) < 5)
				&& ((Double) fieldSplit.getValue() > 1)) {
			Utils.showMessage("Minimum 5 lot per Split ", form);
			return false;
		}
		return true;
	}

	void clearQuote() {
		if (!soldstock.equals("")) {
			unsubscribe();
		}
		soldstock = "";
		((FilterColumn) filterBid.getFilteredData("stock")).setField("");
		((FilterColumn) filterBid.getFilteredData("bo")).setField("B");
		((FilterColumn) filterOffer.getFilteredData("stock")).setField("");
		((FilterColumn) filterOffer.getFilteredData("bo")).setField("O");
		filterBid.fireFilterChanged();
		filterOffer.fireFilterChanged();
		chgStockSummary();
	}

	void changeQuote(final String newstock) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				String stock = newstock.trim().toUpperCase();
				if (!soldstock.toUpperCase().trim().equals(stock)) {
					stock = stock.trim().toUpperCase();
					if (!soldstock.equals("") && !soldstock.equals(BQstock)) {
						unsubscribe();
					}
					soldstock = stock;
					((FilterColumn) filterBid.getFilteredData("stock"))
							.setField(stock);
					((FilterColumn) filterBid.getFilteredData("bo"))
							.setField("B");
					((FilterColumn) filterBid.getFilteredData("board"))
							.setField(comboBoard.getText());
					((FilterColumn) filterOffer.getFilteredData("stock"))
							.setField(stock);
					((FilterColumn) filterOffer.getFilteredData("bo"))
							.setField("O");
					((FilterColumn) filterOffer.getFilteredData("board"))
							.setField(comboBoard.getText());
					filterBid.fireFilterChanged();
					filterOffer.fireFilterChanged();
					if (!soldstock.equals(""))
						subscribe();
					chgStockSummary();
				}
			}
		});
	}

	void BestQuote(final String newstock) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				String stock = newstock.trim().toUpperCase();
				// if (!soldstock.toUpperCase().trim().equals(stock)) {
				// stock = stock.trim().toUpperCase();
				if (!soldstock.equals("")) {
					unsubscribe();
				}
				if (!soldstock.equals(""))
					subscribe();
				// soldstock = stock;
				((FilterColumn) filterBid.getFilteredData("stock"))
						.setField(stock);
				((FilterColumn) filterBid.getFilteredData("bo")).setField("B");
				((FilterColumn) filterOffer.getFilteredData("stock"))
						.setField(stock);
				((FilterColumn) filterOffer.getFilteredData("bo"))
						.setField("O");
				filterBid.fireFilterChanged();
				filterOffer.fireFilterChanged();
				BQstock = soldstock;
				chgStockSummary();
				// }
			}
		});
	}

	void subscribe() {
		((IEQTradeApp) apps).getFeedEngine().subscribe(
				FeedParser.PARSER_QUOTE,
				FeedParser.PARSER_QUOTE + "|" + soldstock + "#"
						+ comboBoard.getText());
	}

	void unsubscribe() {
		if (!soldstock.equals("")) {
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(
					FeedParser.PARSER_QUOTE,
					FeedParser.PARSER_QUOTE + "|" + soldstock + "#"
							+ comboBoard.getText());
		}
	}

	void changePF(String accountid) {
		((FilterColumn) filterPF.getFilteredData("account")).setField(accountid);
		filterPF.fireFilterChanged();
	}

	void clearForm() {
		try {
			// labelTA.setText("");
			labelOverlimit.setText("");
			// comboClient.getTextEditor().setText("");
			fieldStockName.setText("");
			// fieldStockName.getTextEditor().setText("");
			fieldSplit.setValue(new Double(1));
			checkRandom.setSelected(false);
			fieldLot.setValue(null);
			fieldPrice.setValue(null);
			comboStock.getTextEditor().setText("");
			stockfocusLost(null);
			fieldValue.setValue(null);
			fieldVolume.setValue(null);
			fieldTradingLimit.setValue(null);
			comboBoard.setSelectedItem("RG");
			comboType.setSelectedIndex(0);
			comboIA.setSelectedIndex(0);
			checkBasket.setSelected(false);
			checkConfirm.setSelected(hSetting.get("confirm") != null ? (((Boolean) hSetting.get("confirm")).booleanValue()) : true);
			changePF("");
			clearQuote();
			// comboStock.getTextEditor().requestFocus();
			// comboClient.getTextEditor().requestFocus();
			// comboStock.getTextEditor().requestFocus();
			clientfocusLost(null);
			comboSend.setSelectedIndex(0);
			comboType.setEnabled(true);
			comboType.setFocusable(true);
		} catch (Exception ex) {
			
		}
	}

	private String genConfirm() {
		Double lot = (Double) fieldLot.getValue();
		Double price = (Double) fieldPrice.getValue();
		String result = "";
		result = result.concat(entryType + " Order\n");
		result = result.concat("Client        :  " + comboClient.getText()).concat("\n");
		result = result.concat("Stock        :  "+ comboStock.getText().concat("\n"));
		result = result.concat("Price         :  " + formatter.format(price)).concat("\n");
		result = result.concat("Lot            :  " + formatter.format(lot)).concat("lot\n");
		return result;
	}

	boolean _send = false;

	private synchronized void send(boolean closed) {
		try {
			if (!_send) {
				_send = true;
				if (sendAction == null || !sendAction.isEnabled()) {
					Utils.showMessage("cannot send order, please reconnect..",	form);
					_send = false;
				} else if (isValidEntry()) {
					jalan();
				}
				_send = false;
			}
		} catch (Exception ex) {
			//log.error(Utils.logException(ex));
			_send = false;
		}
	}

	private static Random gen = new Random();

	private int randMax(int maxval) {
		return gen.nextInt(maxval) + 5;
	}

	private int[] random(int jmlorder, int jmlsplit, boolean random) {
		int[] hasilAll = new int[jmlsplit];
		int hasil = 0;
		int x = 1;
		int jml = 0;
		int maxnumber;
		int minnumber = 0;
		int randomax = 0;
		while (x <= jmlsplit) {
			if (x == jmlsplit) {
				hasil = jmlorder - jml;
			} else {
				if (x == 1) {
					maxnumber = Math.round(jmlorder / jmlsplit);
					minnumber = ((x * 10000)	- Math.round((jmlsplit * 10000) - (jmlorder)) < 0 ? 1 : (x * 10000)- Math.round((jmlsplit * 10000)- (jmlorder)));

				} else {
					maxnumber = (Math.round((jmlorder - jml) / (jmlsplit - x + 1)) > 10000 ? 10000 : Math.round((jmlorder - jml) / (jmlsplit - x + 1)));
					minnumber = ((x * 10000)- Math.round((jmlsplit * 10000) - (jmlorder - jml)) < 0 ? 1: (x * 10000)	- Math.round((jmlsplit * 10000)	- (jmlorder - jml)));
				}

				randomax = randMax(maxnumber);
				while (randomax < minnumber || randomax > maxnumber) {
					randomax = randMax(maxnumber);
				}

				hasil = (random ? randomax : maxnumber);
				jml = jml + hasil;
			}
			hasilAll[x - 1] = hasil;
			x++;
		}
		return hasilAll;
	}

	private Thread threadTest;
	
	void jalan() {
		final double lot = (Double) fieldLot.getValue(); // Double.parseDouble(strLot);
		double count = ((Double) fieldSplit.getValue()).doubleValue();
		String client = comboClient.getText();
		String stock = comboStock.getText();
		String type = comboType.getText();
		String timer = fieldTimer.getText();
		String board = comboBoard.getText();
		int sendMethod = comboSend.getSelectedIndex();
		String bos = new String(entryType);
		String ia = comboIA.getText();
		Double price = (Double) fieldPrice.getValue(); // Double.parseDouble(strPrice);
		double leftlot = lot;
		double currlot = 0;
		
		try {	
			Order order = genOrder(lot, 1, 1, client, stock, type,board, timer, sendMethod, bos, ia, price);
			((IEQTradeApp) apps).getTradingEngine().createOrder(order);
			Thread.sleep(1000);
			jalan();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	void sendOrder(final boolean closed) {
		if (threadTest != null)
			threadTest = null;

		threadTest = new Thread() {
			public void run() {
				synchronized (UIAutoTrading.this) {
					final double lot = (Double) fieldLot.getValue(); // Double.parseDouble(strLot);
					double count = ((Double) fieldSplit.getValue()).doubleValue();
					String client = comboClient.getText();
					String stock = comboStock.getText();
					String type = comboType.getText();
					String timer = fieldTimer.getText();
					String board = comboBoard.getText();
					int sendMethod = comboSend.getSelectedIndex();
					String bos = new String(entryType);
					String ia = comboIA.getText();
					Double price = (Double) fieldPrice.getValue(); // Double.parseDouble(strPrice);
					double leftlot = lot;
					double currlot = 0;

					if (closed)
						hide();
					
					try {	
						Order order = genOrder(lot, 1, 1, client, stock, type,board, timer, sendMethod, bos, ia, price);
						((IEQTradeApp) apps).getTradingEngine().createOrder(order);
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				
				}
			}
		}; // ).start();
		threadTest.start();
	}

	private static SimpleDateFormat dateFormatAll = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

	private Order genOrder(double lot, double counter, double totalsplit,String client, String stock, String type, String board,String timer, int sendMethod, String bos, String ias, Double price) {
		Account acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByKey(new String[] { getAccountId(client) });
		Stock sec = (Stock) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_STOCK).getDataByKey(new String[] { stock });
		Order order = new Order();
		order.setOrderIDContra("");
		order.setId(OrderIDGenerator.gen(acc.getTradingId()));
		order.setOrderDate(OrderIDGenerator.getTime());
		order.setExchange("IDX");
		order.setBoard(board);
		order.setBroker(OrderIDGenerator.getBrokerCode());
		// Remark For JONEC V.2.0
		order.setBOS(bos.equals("BUY") ? "1" : "2");
		/*
		 * if (acc.getAccType().equals("MR") && bos.equals("BUY")) {
		 * order.setBOS("M"); } else order.setBOS(bos.equals("BUY") ? "1" :
		 * "2");
		 */

		// End Of Remark
		order.setOrdType(OrderIDGenerator.getLimitOrder());
		Mapping m = (Mapping) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERTYPE).getDataByField(new Object[] { type },new int[] { Mapping.CIDX_CODE });
		order.setOrderType(m.getDesc());
		order.setStock(stock);
		order.setIsAdvertising("0");
		order.setTradingId(client);
		Mapping ia = (Mapping) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_TRADINGACC).getDataByField(new Object[] { ias },new int[] { Mapping.CIDX_CODE });
		order.setInvType(ia.getDesc());
		order.setCurrency("IDR");
		order.setContraBroker("");
		order.setContraUser("");
		order.setPrice(price);
		order.setLot(new Double(lot));
		order.setVolume(new Double(sec.getLotSize().doubleValue() * lot));
		order.setIsBasketOrder(sendMethod == 0 ? "0" : "1");
		// Update by Kvn 20110503
		/*
		 * try { String dt = dateFormat.format(new Date()); dt = dt + "-" +
		 * timer; order.setBasketTime(sendMethod == 0 ? null : dateFormatAll
		 * .parse(dt)); } catch (Exception ex) { ex.printStackTrace(); }
		 */

		try {
			String dt = dateFormat.format(OrderIDGenerator.getTime());
			dt = dt + "-" + timer;
			order.setBasketTime(sendMethod == 0 ? null : dateFormatAll.parse(dt));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		order.setStatus(Order.C_ENTRY);
		order.setMarketOrderId("");
		order.setDoneVol(new Double(0));
		order.setDoneLot(new Double(0));
		order.setCounter(new Double(counter));
		order.setSplitTo(new Double(totalsplit));
		order.setComplianceId(acc.getComlianceId());
		order.setAccId(acc.getAccId());
		order.setBalVol(new Double(order.getVolume().doubleValue()));
		order.setBalLot(new Double(order.getLot().doubleValue()));
		order.setTradeNo("0");
		order.setUser(((IEQTradeApp) apps).getTradingEngine().getUserId().toUpperCase());
		order.setIsFloor("0");
		order.setMarketRef(order.getId());
		order.buildAmount();
		return order;
	}

	private void registerEventCombo(JDropDown comp) {
		comp.getEditor().getEditorComponent().setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),"enterAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap()	.put("enterAction", new AbstractAction("enterAction") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {

				if (e.getSource().equals(comboStock.getTextEditor())) {
					comboStock.setSelectedIndex(comboStock.getSelectedIndex());
					comboStock.hidePopup();
					fieldPrice.requestFocus();
					// fieldStockName.requestFocus();
				} else if (e.getSource().equals(
					comboClient.getTextEditor())) {
					comboClient.setSelectedIndex(comboClient.getSelectedIndex());
					comboClient.hidePopup();
					// comboBoard.requestFocus();
					// fieldClientName.requestFocus();
					fieldClientName.setSelectedIndex(comboClient.getSelectedIndex());
					fieldClientName.requestFocus();
				}/*
				 * else
				 * if(e.getSource().equals(fieldStockName.getTextEditor
				 * ())){
				 * .setSelectedIndex(fieldStockName.getSelectedIndex());
				 * fieldStockName
				 * .setSelectedIndex(fieldStockName.getSelectedIndex());
				 * fieldPrice.requestFocus(); }
				 */else if (e.getSource().equals(
					fieldClientName.getTextEditor())) {
					fieldClientName.setSelectedIndex(fieldClientName.getSelectedIndex());
					comboClient.setSelectedIndex(fieldClientName.getSelectedIndex());
					// comboBoard.requestFocus();
					comboStock.requestFocus();
				}

				// comboStock.requestFocus();

			}

		});

		comp.getTextEditor().getActionMap().put("tabAction", new AbstractAction("tabAction") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {

				if (e.getSource().equals(comboStock.getTextEditor())) {
					comboStock.setSelectedIndex(comboStock.getSelectedIndex());
					comboStock.hidePopup();
					// fieldPrice.requestFocus();
					((JSpinner.DefaultEditor) fieldPrice.getEditor()).getTextField().requestFocus();
					// fieldStockName.requestFocus();
				} else if (e.getSource().equals(
					comboClient.getTextEditor())) {
					comboClient.setSelectedIndex(comboClient.getSelectedIndex());
					comboClient.hidePopup();
					// comboBoard.requestFocus();
					fieldClientName.setSelectedIndex(comboClient.getSelectedIndex());
					fieldClientName.requestFocus();
				}/*
				 * else
				 * if(e.getSource().equals(fieldStockName.getTextEditor
				 * ())){ comboStock.setSelectedIndex(fieldStockName.
				 * getSelectedIndex());
				 * //fieldStockName.setSelectedIndex
				 * (fieldStockName.getSelectedIndex());
				 * fieldStockName.hidePopup();
				 * fieldPrice.requestFocus(); }
				 */else if (e.getSource().equals(fieldClientName.getTextEditor())) {
					comboClient.setSelectedIndex(fieldClientName.getSelectedIndex());
					fieldClientName.hidePopup();
					// fieldClientName.setSelectedIndex(fieldClientName.getSelectedIndex());
					// comboBoard.requestFocus();
					comboStock.requestFocus();
				}
			}
		});
	}

	void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		/*
		 * inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB,
		 * InputEvent.CTRL_DOWN_MASK), "controlTabAction");
		 * comp.getActionMap().put("controlTabAction", new
		 * AbstractAction("controlTabAction") { private static final long
		 * serialVersionUID = 1L;
		 * 
		 * public void actionPerformed(ActionEvent evt) { int idx =
		 * tab.getSelectedIndex()==0 ? 1 : 0; tab.setSelectedIndex(idx); } });
		 */
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),"enterAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S,InputEvent.CTRL_DOWN_MASK), "ctrlSAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_P,InputEvent.CTRL_DOWN_MASK), "ctrlPAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_T,InputEvent.CTRL_DOWN_MASK), "ctrlTAction");
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "secAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "clientAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0), "priceAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0), "qtyAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.ALT_MASK),	"altCAction");

		comp.getActionMap().put("altCAction", new AbstractAction("altCAction") {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnClose.doClick();
			}
		});

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.ALT_MASK),"altSAction");

		comp.getActionMap().put("altSAction", new AbstractAction("altSAction") {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnSend.doClick();
			}
		});

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_A, Event.ALT_MASK),"altAAction");

		comp.getActionMap().put("altAAction", new AbstractAction("altAAction") {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnAdd.doClick();
			}
		});

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, Event.SHIFT_MASK),"shiftTabAction");

		comp.getActionMap().put("shiftTabAction",new AbstractAction("shiftTabAction") {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource().equals(comboStock.getTextEditor())) {
					comboClient.requestFocus();
				} else if (e.getSource().equals(
					fieldClientName.getTextEditor())) {
					comboClient.requestFocus();
				} else if (e.getSource().equals(
					comboClient.getTextEditor())) {
					// comboClient.requestFocus();
					comboBoard.requestFocus();
				}
			}
		});

		comp.getActionMap().put("secAction", new AbstractAction("secAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				comboStock.getTextEditor().requestFocus();
			}
		});
		
		comp.getActionMap().put("clientAction",new AbstractAction("clientAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				comboClient.getTextEditor().requestFocus();
			}
		});
		
		comp.getActionMap().put("priceAction",new AbstractAction("priceAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				fieldPrice.requestFocus();
			}
		});
		
		comp.getActionMap().put("qtyAction", new AbstractAction("qtyAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				fieldLot.requestFocus();
			}
		});
		
		comp.getActionMap().put("escapeAction",new AbstractAction("escapeAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				btnClose.doClick();
			}
		});

		comp.getActionMap().put("enterAction",new AbstractAction("enterAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				if (evt.getSource() instanceof JButton) {
					((JButton) evt.getSource()).doClick();
				} else {
					if (evt.getSource() instanceof JNumber) {
						try {
							((JNumber) evt.getSource()).commitEdit();
						} catch (Exception ex) {
							
						}								
					}
					btnSend.doClick();
				}
			}
		});
	}

	String getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).getDataByField(new String[] { alias },new int[] { Account.C_TRADINGID });
		return (acc != null ? acc.getAccId() : "");
	}

	void resetFields() {
		checkConfirm.setSelected(hSetting.get("confirm") != null ? (((Boolean) hSetting.get("confirm")).booleanValue()) : true);

		int brd = comboBoard.getSelectedIndex();
		if (brd < 0)
			comboBoard.setSelectedIndex(0);
		int tac = comboIA.getSelectedIndex();
		if (tac < 0)
			comboIA.setSelectedIndex(0);
		int exp = comboType.getSelectedIndex();
		if (exp < 0)
			comboType.setSelectedIndex(0);
		int sen = comboSend.getSelectedIndex();
		if (sen < 0)
			comboSend.setSelectedIndex(0);

		// String stkId = comboStock.getText();
		String cname = fieldClientName.getText();
		String sname = fieldStockName.getText();
		if (!(cname.isEmpty() && sname.isEmpty())) {
			((JSpinner.DefaultEditor) fieldPrice.getEditor()).getTextField().requestFocus();
		}

		fieldLot.setValue(new Double(0));

		if (!isFilled) {
			if (!comboStock.getText().trim().equals("")) {
				comboStock.getTextEditor().setText(comboStock.getText().toUpperCase());
				StockSummary summ = (StockSummary) ((IEQTradeApp) apps).getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY).getDataByKey(new Object[] {comboStock.getText().toUpperCase(),comboBoard.getText() });
				if (summ != null) {
					Double harga = entryType.equals("SELL") ? summ.getBestBid()	: summ.getBestOffer();
					if (harga < 1)
						harga = summ.getPrevious();
					fieldPrice.setValue(harga);
				}
			}
		}
	}

	ChangeListener changeListener = new ChangeListener() {
		public void stateChanged(ChangeEvent changeEvent) {
			JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
			int idx = sourceTabbedPane.getSelectedIndex();
			entryType = idx == 0 ? "BUY" : "SELL";

			if (idx == 0) {
				pnlBlank.add(pnlEntry);
			} else {
				pnlSlank.add(pnlEntry);
			}
			changeForm();
		}
	};

	private static void setupTabTraversalKeys(JTabbedPane tabbedPane) {
		KeyStroke ctrlTab = KeyStroke.getKeyStroke("ctrl TAB");
		KeyStroke ctrlShiftTab = KeyStroke.getKeyStroke("ctrl shift TAB");

		// Remove ctrl-tab from normal focus traversal
		Set<AWTKeyStroke> forwardKeys = new HashSet<AWTKeyStroke>(tabbedPane.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS));forwardKeys.remove(ctrlTab);tabbedPane.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, forwardKeys);

		// Remove ctrl-shift-tab from normal focus traversal
		Set<AWTKeyStroke> backwardKeys = new HashSet<AWTKeyStroke>(tabbedPane.getFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS));backwardKeys.remove(ctrlShiftTab);tabbedPane.setFocusTraversalKeys(KeyboardFocusManager.BACKWARD_TRAVERSAL_KEYS, backwardKeys);

		// Add keys to the tab's input map
		InputMap inputMap = tabbedPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(ctrlTab, "navigateNext");
		inputMap.put(ctrlShiftTab, "navigatePrevious");
	}

	private class IDXPriceSpinnerModel extends SpinnerNumberModel {
		private static final long serialVersionUID = 1L;

		public IDXPriceSpinnerModel() {
			super(new Double(0), new Double(0), new Double(100000000),new Double(1));
		}

		public Object getNextValue() {
			// New TickPrice
			Double newValue = new Double(50);
			Double oldValue = (Double) this.getValue();
			
			if (oldValue < Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE1)))
				newValue = oldValue + 1;
			if (oldValue >= Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE2)))
				newValue = oldValue + 5;
			if (oldValue >= Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE3)))
				newValue = oldValue + 25;
			if (newValue < Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE4)))
				newValue = new Double(50);
			
			return new Double(newValue);
		}

			public Object getPreviousValue() {
			// New TickPrice
			Double newValue = new Double(50);
			Double oldValue = (Double) this.getValue();
			
			if (oldValue < Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE1)))
				newValue = oldValue - 1;
			if (oldValue > Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE2)))
				newValue = oldValue - 5;
			if (oldValue > Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE3)))
				newValue = oldValue - 25;
			if (newValue < Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE4)))
				newValue = new Double(50);
			
			return new Double(newValue);
		}
		
		/*public Object getNextValue() {
    		Double newValue = new Double(50);
    		Double oldValue = (Double)this.getValue();
    		if (oldValue<200) newValue = oldValue + 1;
    		if (oldValue>=200) newValue = oldValue + 5;
    		if (oldValue>=500) newValue = oldValue + 10;
    		if (oldValue>=2000) newValue = oldValue + 25;
    		if (oldValue>=5000) newValue = oldValue + 50;
    		if (newValue<50) newValue = new Double(50);
    		return new Double(newValue);
    	}
    	
    	public Object getPreviousValue() {
    		Double newValue = new Double(50);
    		Double oldValue = (Double)this.getValue();
    		if (oldValue<200) newValue = oldValue - 1;
    		if (oldValue>200) newValue = oldValue - 5;
    		if (oldValue>500) newValue = oldValue - 10;
    		if (oldValue>2000) newValue = oldValue - 25;
    		if (oldValue>5000) newValue = oldValue - 50;
    		if (newValue<50) newValue = new Double(50);
    		return new Double(newValue);
    	}
    	*/
	}

}
