package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import net.sf.nachocalendar.components.DateField;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.demo.Mapping;
import com.vollux.framework.UI;
import com.vollux.framework.UI.MyCustomMouseAdapter;
import com.vollux.framework.UI.MyMouseAdapter;
import com.vollux.idata.Column;
import com.vollux.ui.CustomMouseAdapter;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;
import com.vollux.ui.JTextLabel;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.ui.quote.FilterQuote;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.OrderIDGenerator;
import eqtrade.trading.engine.TradingEngine;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.GTCDef;
import eqtrade.trading.model.Order;
//import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.GTCRender;
//import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.model.OrderStatus;
import eqtrade.trading.model.Portfolio;
import eqtrade.trading.model.Stock;
import eqtrade.trading.model.UserProfile;
import eqtrade.trading.report.OrderReport;
import eqtrade.trading.ui.OrderPanel.ExcelFileFilter;
import eqtrade.trading.ui.OrderPanel.RowChangeList;
import eqtrade.trading.ui.OrderPanel.RowChanged;
import eqtrade.trading.ui.OrderPanel.RowUpdate;

public class GTCLIST extends UI {
	private JGrid table;
	private JDropDown comboClient;
	private JDropDown comboStock;
	private JDropDown comboBOS;
	private JDropDown comboTaker;
	private JDropDown comboBoard;
	private JDropDown comboFloor;
	JNumber price;
	private JDropDown comboStatus;
	FilterGTC filterOrder;
	static JButton btnNotif;
	boolean isLoadingSuperUser = false;
	String client = "", sfilterClient = "",sfilterStock = "";
	JNumber fieldTradingLimit;
	private String entryType = "BUY";
	JLabel labelOverlimit;
	JCheckBox checkBasket;
	JDropDown comboSend = null;
	JTextField fieldTimer;
	private String soldstock = "";

	JSpinner fieldLot;
	JNumber fieldValue;
	JNumber fieldVolume;
	private JLabel fieldPrev;
	private JLabel fieldHigh;
	private JLabel fieldLow;
	private JLabel fieldChange;
	private JLabel fieldOpen;
	private JLabel fieldLast;
	Vector filterclient = null;

	JNumber tCount;
	JNumber oCount;
	JNumber oValue;
	JNumber dCount;
	JNumber dValue;
	JNumber tLot;
	JNumber tValue;
	static String usernotif = "";
	JButton btnClear;
	JButton btnView;
	// private JButton btnPrint;
	private FilterPortfolio filterPF;
	JDropDown comboIA = null;
	JNumber fieldStockLimit;
	// private FilterQuote filterBid;
	// private FilterQuote filterOffer;
	private static NumberFormat formatter = new DecimalFormat("#,##0 ");
	private static NumberFormat formatter2 = new DecimalFormat("#,##0.00");

	JMenuItem resendMenu;
	DateField DateGTC;
	Action negdealAction;
	CustomSelectionModel customSelection;
//	Action amendAction;
	Action withdrawAction;
	Action splitAction;

	JMenuItem withdrawMenu;
//	JMenuItem amendMenu;
	JMenuItem propertiesMenu;
	JMenuItem splitMenu;
	JMenuItem printExcelMenu;

	public GTCLIST(String app) {
		super("GTCLIST", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_GTC);
		// TODO Auto-generated constructor stub

	}

	protected boolean isAllTemporary(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order o = (Order) v.elementAt(i);
			if (!o.getStatus().equals(Order.C_TEMPORARY))
				return false;
		}
		return result;
	}

	private boolean isAllRejected(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order GTC = (Order) v.elementAt(i);

			if ((GTC.getStatus().equals(Order.C_REJECTED) || GTC.getStatus()
					.equals(Order.C_REJECTED2))
					&& !GTC.getOrderIDContra().isEmpty()) {
				return false;
			} else if (GTC.getStatus().equals(Order.C_REJECTED)
					|| GTC.getStatus().equals(Order.C_REJECTED2)) {

			} else {
				return false;
			}
		}
		return result;
	}

	protected boolean isAllQueue(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order order = (Order) v.elementAt(i);
			// if (!GTC.getIsBasketOrder().equals("1"))
			if (!(order.getStatus().equals(Order.C_OPEN)
					|| order.getStatus().equals(Order.C_PARTIAL_MATCH) || (order
					.getStatus().equals(Order.C_ENTRY) && order
					.getIsBasketOrder().equals("1"))))
				return false;
		}
		return result;
	}

	protected boolean isAllOpen(Vector v) {
		boolean result = true;
		for (int i = 0; i < v.size(); i++) {
			Order GTC = (Order) v.elementAt(i);
			if (!GTC.getStatus().equals(Order.C_OPEN)
					&& !GTC.getStatus().equals(Order.C_PARTIAL_MATCH))
				return false;
		}
		return result;
	}

	protected JFileChooser jfc = null;
	public static String BQstock;
	protected ExcelFileFilter filterFile = null;

	/*
	 * protected void savetoFile() { if (jfc == null) { filterFile = new
	 * ExcelFileFilter(); filterFile.addExtension("xls");
	 * filterFile.setDescription(" Microsoft Excel "); jfc = new
	 * JFileChooser(new File("C:\\")); jfc.setFileFilter(filterFile);
	 * jfc.setAcceptAllFileFilterUsed(false);
	 * jfc.setDialogTitle("Save Order to....");
	 * jfc.setDialogType(JFileChooser.SAVE_DIALOG); }
	 */
	// String filename = "";
	/*
	 * int returnVal = jfc.showSaveDialog(this); if (returnVal ==
	 * JFileChooser.APPROVE_OPTION) { File ffile = jfc.getSelectedFile();
	 * filename = ffile.getAbsolutePath(); if (!filename.endsWith(".xls")) {
	 * filename = filename + ".xls"; } } else { return; } int i =
	 * table.getTable().getRowCount(); try { BufferedWriter out = new
	 * BufferedWriter(new FileWriter(filename)); Hashtable prop =
	 * table.getTableProperties(); int[] order = (int[]) prop.get("order");
	 * int[] width = (int[]) prop.get("width"); String header = ""; for (int k =
	 * 0; k < order.length; k++) { if (width[k] != 0) { header = header +
	 * OrderDef.getHeader().elementAt(order[k]) + "\t"; } } out.write(header +
	 * "\n"); for (int j = 0; j < i; j++) { Column dat = (Column)
	 * table.getTable().getValueAt(j, 0); Order ord = (Order) dat.getSource();
	 * String row = ""; for (int k = 0; k < order.length; k++) { if (width[k] !=
	 * 0) { switch (order[k]) { case Order.C_STATUSID: row = row +
	 * OrderRender.hashStatus.get(ord .getData(order[k])) + "\t"; break; case
	 * Order.C_BUYORSELL: row = row + OrderRender.hashSide.get(ord
	 * .getData(order[k])) + "\t"; break; case Order.C_ORDERTYPE: row = row +
	 * OrderRender.hashType.get(ord .getData(order[k])) + "\t"; break; case
	 * Order.C_ORDTYPE: row = row + "limited order\t"; break; default: row = row
	 * + ord.getData(order[k]) + "\t"; break; } } } out.write(row + "\n"); }
	 * out.close(); } catch (Exception ex) { ex.printStackTrace(); } }
	 */


	public Vector getSelected() {
		Vector v = ((CustomSelectionModel) table.getTable().getSelectionModel())
				.getVSelected();
		Vector result = new Vector(v.size());
		for (int i = 0; i < v.size(); i++) {
			result.addElement(((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_GTC)
					.getDataByIndex(
							table.getMappedRow(((Integer) v.elementAt(i))
									.intValue())));
		}
		return result;
	}

	private String genConfirm(Order GTC) {
		String result = "";
		result = result.concat(GTC.getTradingId()).concat(" ");
		result = result.concat(GTC.getBOS().equals(Order.C_BUY) ? "Buy "
				: "Sell ");
		result = result.concat(GTC.getStock().concat(" "));
		result = result.concat("at ")
				.concat(GTCRender.formatter.format((Double) GTC.getPrice()))
				.concat(" ");
		result = result.concat(
				GTCRender.formatter.format((Double) GTC.getLot())).concat(
				" lot\n");
		return result;
	}

	protected class RowChangeList implements TableModelListener {

		@Override
		public void tableChanged(TableModelEvent e) {
			int rowcount = table.getTable().getRowCount();
			// tableOrder.getTable().
		}

	}

	protected class RowUpdate implements TableModelListener {
		public void tableChanged(final TableModelEvent e) {
			if (e != null) {
				boolean move = true;
				if (move) {
					final int first = e.getFirstRow();
					final int last = e.getLastRow();
					if (first == last) {
						final int n = table.getTableRow((first));
						if (n > -1) {
							table.getTable().clearSelection();
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									synchronized (table.getTable()) {
										try {
											Rectangle rec = table.getTable()
													.getCellRect(n, 2, false);
											table.getTable()
													.scrollRectToVisible(rec);
											// tableOrder.getTable()
											// .setRowSelectionInterval(n,
											// n);
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									}
								}
							});
						}
					}
				}
			}
		}
	}

	private Order genOrderGTC(Order orderCopy) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_GTC)
				.getDataByKey(
						new String[] { getAccountId(orderCopy.getTradingId()) });
		Stock sec = (Stock) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK)
				.getDataByKey(new String[] { orderCopy.getStock() });
		Order GTC = new Order();
		///////GTC.setEntryTime(OrderIDGenerator.getTime());
		//GTC.setOrderIDContra("");
		GTC.setId(OrderIDGenerator.gen(acc.getTradingId()));
		GTC.setMarketOrderId("");
		GTC.setTradingId(orderCopy.getTradingId());
		GTC.setNextOrderId(orderCopy.getId());
		//GTC.setOrderDate(OrderIDGenerator.getTime());
		GTC.setBOS(orderCopy.getBOS());
		GTC.setStock(orderCopy.getStock());
		GTC.setLot(new Double(orderCopy.getLot()));
		GTC.setBoard(orderCopy.getBoard());
		GTC.setPrice(orderCopy.getPrice());
		GTC.setVolume(new Double(sec.getLotSize().doubleValue()* orderCopy.getLot()));
		GTC.setValiduntil((Date) DateGTC.getValue());
		GTC.setAccId(acc.getAccId());
		GTC.setStatus(Order.C_OPEN);
		GTC.setNote("");
		GTC.setOrdType(OrderIDGenerator.getLimitOrder());
		GTC.setOrderType(orderCopy.getOrderType());
		GTC.calculate();

		/*
		 * GTC.setExchange(orderCopy.getExchange());
		 * GTC.setBoard(orderCopy.getBoard());
		 * GTC.setBroker(OrderIDGenerator.getBrokerCode());
		 * GTC.setBOS(orderCopy.getBOS());
		 * GTC.setOrdType(OrderIDGenerator.getLimitOrder());
		 * 
		 * GTC.setOrderType(orderCopy.getOrderType());
		 * GTC.setStock(orderCopy.getStock()); GTC.setIsAdvertising("0");
		 * GTC.setTradingId(orderCopy.getTradingId());
		 * 
		 * GTC.setInvType(orderCopy.getInvType());
		 * GTC.setCurrency(orderCopy.getCurrency()); GTC.setContraBroker("");
		 * GTC.setContraUser(""); GTC.setPrice(orderCopy.getPrice());
		 * GTC.setLot(new Double(orderCopy.getLot())); GTC.setVolume(new
		 * Double(sec.getLotSize().doubleValue() orderCopy.getLot()));
		 */

		Date baskettime = orderCopy.getBasketTime();
		Date currentDate = OrderIDGenerator.getTime();

		if (baskettime != null && baskettime.getTime() > currentDate.getTime()) {
			GTC.setIsBasketOrder(orderCopy.getIsBasketOrder());
			GTC.setBasketTime(orderCopy.getBasketTime());
		} else {
			GTC.setIsBasketOrder("0");
		}

		/*
		 * GTC.setStatus(GTC.C_ENTRY); GTC.setMarketOrderId("");
		 * GTC.setDoneVol(new Double(0)); GTC.setDoneLot(new Double(0));
		 * GTC.setCounter(orderCopy.getCounter());
		 * GTC.setSplitTo(orderCopy.getSplitTo());
		 * GTC.setComplianceId(acc.getComlianceId());
		 * GTC.setAccId(acc.getAccId()); GTC.setBalVol(orderCopy.getBalVol());
		 * GTC.setBalLot(orderCopy.getBalLot()); GTC.setTradeNo("0");
		 * GTC.setUser(((IEQTradeApp) apps).getTradingEngine().getUserId()
		 * .toUpperCase()); GTC.setIsFloor("0"); GTC.setMarketRef(GTC.getId());
		 */
		return GTC;
	}

	/*
	 * private Order genOrder(Order orderCopy) { Account acc = (Account)
	 * ((IEQTradeApp) apps) .getTradingEngine()
	 * .getStore(TradingStore.DATA_ACCOUNT) .getDataByKey( new String[] {
	 * getAccountId(orderCopy.getTradingId()) }); Stock sec = (Stock)
	 * ((IEQTradeApp) apps).getTradingEngine()
	 * .getStore(TradingStore.DATA_STOCK) .getDataByKey(new String[] {
	 * orderCopy.getStock() }); Order order = new Order();
	 * order.setOrderIDContra("");
	 * order.setId(OrderIDGenerator.gen(acc.getTradingId()));
	 * order.setOrderDate(OrderIDGenerator.getTime());
	 * order.setExchange(orderCopy.getExchange());
	 * order.setBoard(orderCopy.getBoard());
	 * order.setBroker(OrderIDGenerator.getBrokerCode());
	 * order.setBOS(orderCopy.getBOS());
	 * order.setOrdType(OrderIDGenerator.getLimitOrder());
	 * 
	 * order.setOrderType(orderCopy.getOrderType());
	 * order.setStock(orderCopy.getStock()); order.setIsAdvertising("0");
	 * order.setTradingId(orderCopy.getTradingId());
	 * 
	 * order.setInvType(orderCopy.getInvType());
	 * order.setCurrency(orderCopy.getCurrency()); order.setContraBroker("");
	 * order.setContraUser(""); order.setPrice(orderCopy.getPrice());
	 * order.setLot(new Double(orderCopy.getLot())); order.setVolume(new
	 * Double(sec.getLotSize().doubleValue() orderCopy.getLot()));
	 * 
	 * Date baskettime = orderCopy.getBasketTime(); Date currentDate =
	 * OrderIDGenerator.getTime();
	 * 
	 * if (baskettime != null && baskettime.getTime() > currentDate.getTime()) {
	 * order.setIsBasketOrder(orderCopy.getIsBasketOrder());
	 * order.setBasketTime(orderCopy.getBasketTime()); } else {
	 * order.setIsBasketOrder("0"); }
	 * 
	 * order.setStatus(Order.C_ENTRY); order.setMarketOrderId("");
	 * order.setDoneVol(new Double(0)); order.setDoneLot(new Double(0));
	 * order.setCounter(orderCopy.getCounter());
	 * order.setSplitTo(orderCopy.getSplitTo());
	 * order.setComplianceId(acc.getComlianceId());
	 * order.setAccId(acc.getAccId()); order.setBalVol(orderCopy.getBalVol());
	 * order.setBalLot(orderCopy.getBalLot()); order.setTradeNo("0");
	 * order.setUser(((IEQTradeApp) apps).getTradingEngine().getUserId()
	 * .toUpperCase()); order.setIsFloor("0");
	 * order.setMarketRef(order.getId()); return order; }
	 */

	String getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { alias },
						new int[] { Account.C_TRADINGID });
		return (acc != null ? acc.getAccId() : "");
	}

	void infoNotif() {
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		String user = sales.getUserId();
		usernotif = user;
		//System.out.println("NOTIFFF**----" + user);
	}

	void initData() {

		infoNotif();

	}

	@Override
	protected void build() {
		// TODO Auto-generated method stub
		createPopup();
//		amendAction = apps.getAction().get(TradingAction.A_SHOWAMENDORDER);
		withdrawAction = apps.getAction()
				.get(TradingAction.A_SHOWWITHDRAWORDER);
		splitAction = apps.getAction().get(TradingAction.A_SHOWSPLITORDER);
		negdealAction = apps.getAction().get(TradingAction.A_SHOWNEGDEAL);

		filterOrder = new FilterGTC(form);
		((FilterColumn) filterOrder.getFilteredData("taker")).setField("0");
		((FilterColumn) filterOrder.getFilteredData("user"))
				.setField(((IEQTradeApp) apps).getTradingEngine().getUserId()
						.toUpperCase());
		comboClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		comboStock = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK), Stock.C_ID, true);
		comboBOS = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_SIDE), Mapping.CIDX_CODE, false);
		comboStatus = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ORDERSTATUS), OrderStatus.C_NAME,
				false);
		comboTaker = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_TAKER), OrderStatus.C_ID, false);
		/*
		 * comboBoard = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
		 * .getStore(TradingStore.DATA_BOARDFILTER), Mapping.CIDX_NAME, false);
		 * comboFloor = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
		 * .getStore(TradingStore.DATA_FLOORFILTER), Mapping.CIDX_NAME, false);
		 */
		price = new JNumber(Double.class, 0, 0, false, true);
		btnClear = new JButton("Clear");
		btnClear.setMnemonic('R');
		btnView = new JButton("View");
		btnView.setMnemonic('V');
		btnNotif = new JButton("Notification Setup");
		btnNotif.setMnemonic('N');
		registerEvent(comboClient.getTextEditor());

		registerEvent(comboStock.getTextEditor());

		registerEvent(comboBOS);
		registerEvent(comboStatus);
		registerEvent(comboTaker);
		/*
		 * registerEvent(comboBoard); registerEvent(comboFloor);
		 */
		registerEvent(price);
		registerEvent(btnClear);
		registerEvent(btnView);
		registerEventCombo(comboClient);
		registerEventCombo(comboStock);

		//perubahan 20 nov
		table = createTable( ((IEQTradeApp)
		 apps).getTradingEngine().getStore( TradingStore.DATA_GTC),
		 filterOrder, (Hashtable) hSetting.get("table"));
		 table.getTable().add(popupMenu);
		 table.getTable().addMouseListener(new MyMouseAdapter());
		 table.addMouseListener(new MyCustomMouseAdapter());
		 table.setColumnHide(GTCDef.columnhide);
		 

		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboClient.getTextEditor().setText("");
				comboStock.getTextEditor().setText("");
				comboBOS.setSelectedIndex(0);
				comboStatus.setSelectedIndex(0);
				comboTaker.setSelectedIndex(0);
				/*
				 * comboBoard.setSelectedIndex(0);
				 * comboFloor.setSelectedIndex(1);
				 */
				price.setValue(null);
				((FilterColumn) filterOrder.getFilteredData("account"))
						.setField(null);
				((FilterColumn) filterOrder.getFilteredData("stock"))
						.setField(null);
				((FilterColumn) filterOrder.getFilteredData("bos"))
						.setField(null);
				((FilterColumn) filterOrder.getFilteredData("status"))
						.setField(null);
				((FilterColumn) filterOrder.getFilteredData("taker"))
						.setField("0");
				/*
				 * ((FilterColumn) filterOrder.getFilteredData("board"))
				 * .setField(null);
				 */
				((FilterColumn) filterOrder.getFilteredData("price"))
						.setField(null);
				/*
				 * ((FilterColumn) filterOrder.getFilteredData("floor"))
				 * .setField("0");
				 */
				filterOrder.fireFilterChanged();
			}
		});
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// if (isSuperUser()) {
				String clientid = comboClient.getText();
				String secid = comboStock.getText();
				//String price = comboPrice.getText();
				//System.out.println("price="+price);
				if (clientid != null && !clientid.isEmpty()) {
					setState(true);//perubahan untuk portfolio
					((IEQTradeApp) apps).getTradingEngine().refreshGTC
					("%", "%");
					/*((FilterColumn) filterOrder.getFilteredData("clientid"))
					.setField(null);*/
				}else if(secid!=null && !secid.isEmpty() && isSuperUser()){
					setState(true);//perubahan untuk portfolio
					((IEQTradeApp) apps).getTradingEngine().refreshGTC(
							"%", "%");
					((FilterColumn) filterOrder.getFilteredData("accountv"))
					.setField(null);
				}else if(clientid == null || clientid.isEmpty()){
					setState(true);
					((IEQTradeApp) apps).getTradingEngine().refreshGTC(
							"%", "%");
					/*((FilterColumn) filterOrder.getFilteredData("price"))
					.setField(null);*/
				}

				// }
			//log.info("is superuser:"+isSuperUser());

				if (isSuperUser()) {

					if (!sfilterClient.isEmpty()
							&& !isFilterClientAdded(sfilterClient)) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByClient(sfilterClient);
					}

					String accid = getAccountId(clientid);

					if (accid != null && !accid.isEmpty()) {

						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByClient(accid);
						sfilterClient = accid;
					}

					if (!sfilterStock.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByStock(sfilterStock);

					}

					String stockid = comboStock.getText();

					if (stockid != null && !stockid.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByStock(stockid.toUpperCase());
						sfilterStock = stockid.toUpperCase();
					}
					/*String pricev = comboPrice.getText();

					if (pricev != null && !pricev.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByStock(pricev.toUpperCase());
						sfilterPrice = pricev.toUpperCase();
					}*/

				}

				filterOrder.fireFilterChanged();
			}

		});

/*				if (isSuperUser()) {

					if (!sfilterClient.isEmpty()
							&& !isFilterClientAdded(sfilterClient)) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByClient(sfilterClient);
					}

					String accid = getAccountId(clientid);

					if (accid != null && !accid.isEmpty()) {

						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByClient(accid);
						sfilterClient = accid;
					}

					if (!sfilterStock.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByStock(sfilterStock);

					}

					String stockid = comboStock.getText();

					if (stockid != null && !stockid.isEmpty()) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.addFilterByStock(stockid.toUpperCase());
						sfilterStock = stockid.toUpperCase();
					}
				}

				filterOrder.fireFilterChanged();
			}

		});*/

		/*btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// if (isSuperUser()) {
				if (!comboClient.getText().trim().equals("")) {

					if (isSuperUser()) {
						((IEQTradeApp) apps).getTradingEngine().refreshData(
								comboClient.getText().trim().toUpperCase());
					} else {
					setState(false);
						((IEQTradeApp) apps).getTradingEngine().refreshAccount(
								comboClient.getText().trim().equals("") ? "%"
										: comboClient.getText().trim()
												.toUpperCase());
						((IEQTradeApp) apps)
								.getTradingEngine()
								.refreshPortfolio(
										comboClient.getText().trim().equals("") ? "%"
												: comboClient.getText().trim()
														.toUpperCase(), "%");
					//}
				}
				String clientid = comboClient.getText();
				if (clientid != null && !clientid.isEmpty()) {
					setState(false);
					((IEQTradeApp) apps).getTradingEngine().refreshGTC(
							clientid.trim().toUpperCase(), "%");

				}

				// }
			log.info("is superuser:"+isSuperUser());

				if (isSuperUser()) {

					if (!sfilterClient.isEmpty()
							&& !isFilterClientAdded(sfilterClient)) {
						((IEQTradeApp) apps).getTradingEngine().getConnection()
								.deleteFilterByClient(sfilterClient);
					}

					String accid = getAccountId(clientid);

					((IEQTradeApp) apps).getTradingEngine().getConnection()
							.addFilterByClient(accid);
					sfilterClient = accid;
				}
				filterOrder.fireFilterChanged();
			}

		});*/
		comboStock.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboStock.showPopup();
				} catch (Exception ex) {
				}
			}

			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("stock"))
						.setField(comboStock.getText().trim().equals("") ? null
								: comboStock.getText().trim().toUpperCase());
				filterOrder.fireFilterChanged();
			}
		});

		comboBOS.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("bos"))
						.setField(comboBOS.getSelectedIndex() == 0 ? null
								: getBOSCode());
				filterOrder.fireFilterChanged();
			}
		});
		comboStatus.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("status"))
						.setField(comboStatus.getSelectedIndex() == 0 ? null
								: getStatusCode());
				filterOrder.fireFilterChanged();
			}
		});
		/*
		 * comboBoard.addFocusListener(new FocusAdapter() { public void
		 * focusLost(FocusEvent e) { ((FilterColumn)
		 * filterOrder.getFilteredData("board"))
		 * .setField(comboBoard.getSelectedIndex() == 0 ? null :
		 * getBoardCode()); filterOrder.fireFilterChanged(); } });
		 * comboFloor.addFocusListener(new FocusAdapter() { public void
		 * focusLost(FocusEvent e) { ((FilterColumn)
		 * filterOrder.getFilteredData("floor"))
		 * .setField(comboFloor.getSelectedIndex() == 0 ? null :
		 * getFloorCode()); filterOrder.fireFilterChanged(); } });
		 */
		price.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				try {
					price.commitEdit();
				} catch (Exception ex) {
				}
				((FilterColumn) filterOrder.getFilteredData("price"))
						.setField(price.getValue());
				filterOrder.fireFilterChanged();
			}
		});
		comboTaker.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("taker"))
						.setField(comboTaker.getSelectedIndex() + "");
				filterOrder.fireFilterChanged();
			}
		});
		comboClient.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				try {
					comboClient.showPopup();
				} catch (Exception ex) {
				}
			}

			public void focusLost(FocusEvent e) {
				((FilterColumn) filterOrder.getFilteredData("account"))
						.setField(comboClient.getText().trim().equals("") ? null
								: comboClient.getText().trim().toUpperCase());
				filterOrder.fireFilterChanged();
			}
		});

		/*
		 * registerEvent(table); registerEvent(table.getTable());
		 * table.getTable().getModel().addTableModelListener(new RowChanged());
		 */
		// table.getTable().getModel().addTableModelListener(new RowChanged());

		/*
		 * comboClient = new JDropDown(((IEQTradeApp)
		 * apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT),
		 * Account.C_TRADINGID, true); comboStock = new JDropDown(((IEQTradeApp)
		 * apps).getTradingEngine().getStore(TradingStore.DATA_STOCK),
		 * Stock.C_ID, true); comboBOS = new JDropDown(((IEQTradeApp)
		 * apps).getTradingEngine().getStore(TradingStore.DATA_SIDE),
		 * Mapping.CIDX_CODE, false); comboStatus = new JDropDown(((IEQTradeApp)
		 * apps).getTradingEngine() .getStore(TradingStore.DATA_ORDERSTATUS),
		 * OrderStatus.C_NAME, false); comboTaker = new JDropDown(((IEQTradeApp)
		 * apps).getTradingEngine().getStore(TradingStore.DATA_TAKER),
		 * Mapping.CIDX_CODE, false); comboBoard = new JDropDown(((IEQTradeApp)
		 * apps).getTradingEngine().getStore(TradingStore.DATA_BOARDFILTER),
		 * Mapping.CIDX_NAME,false); comboFloor = new JDropDown(((IEQTradeApp)
		 * apps).getTradingEngine().getStore(TradingStore.DATA_FLOORFILTER),
		 * Mapping.CIDX_NAME,false);
		 */
		// price = new JNumber(Double.class, 0, 0, false, true);

		/*
		 * btnClear = new JButton("Clear"); btnClear.setMnemonic('R'); btnView =
		 * new JButton("View"); btnView.setMnemonic('V');
		 */
		/*
		 * btnPrint = new JButton("Print"); btnPrint.setMnemonic('P');
		 */

		/*
		 * registerEvent(table); registerEvent(table.getTable());
		 * table.getTable().getModel() .addTableModelListener(new RowChanged());
		 * table.getTable().getModel() .addTableModelListener(new
		 * RowChangeList()); log.info("-=-=-=-="+(new RowChanged())+(new
		 * RowChangeList()));
		 * 
		 * tCount = new JNumber(Double.class, 0, 0, false, false); oCount = new
		 * JNumber(Double.class, 0, 0, false, false); oValue = new
		 * JNumber(Double.class, 0, 0, false, false); dCount = new
		 * JNumber(Double.class, 0, 0, false, false); dValue = new
		 * JNumber(Double.class, 0, 0, false, false); tLot = new
		 * JNumber(Double.class, 0, 0, false, false); tValue = new
		 * JNumber(Double.class, 0, 0, false, false);
		 */

		JPanel editorPanel = new JPanel();
		FormLayout layout = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 80px, 2dlu, 100px, 2dlu, pref, 2dlu, pref",
				"pref");
		PanelBuilder builder = new PanelBuilder(layout, editorPanel);
		builder.setBorder(new EmptyBorder(4, 4, 4, 4));
		CellConstraints cc = new CellConstraints();
		builder.add(new JTextLabel("Client", 'C', comboClient.getTextEditor()),
				cc.xy(1, 1));
		builder.add(comboClient, cc.xy(3, 1));
		builder.add(new JTextLabel("Stock", 'S', comboStock.getTextEditor()),
				cc.xy(5, 1));
		builder.add(comboStock, cc.xy(7, 1));
		builder.add(new JTextLabel("Price", 'P', price), cc.xy(9, 1));
		builder.add(price, cc.xy(11, 1));
		builder.add(new JTextLabel("B/S", 'B', comboBOS), cc.xy(13, 1));
		builder.add(comboBOS, cc.xy(15, 1));
		builder.add(new JTextLabel("Status", 'T', comboStatus), cc.xy(17, 1));
		/*
		 * builder.add(comboBoard, cc.xy(19, 1)); builder.add(new
		 * JTextLabel("Entry", 'R', comboFloor), cc.xy(21, 1));
		 * builder.add(comboFloor, cc.xy(23, 1)); builder.add(new
		 * JTextLabel("Status", 'T', comboStatus), cc.xy(25, 1));
		 */
		builder.add(comboStatus, cc.xy(19, 1));
		builder.add(comboTaker, cc.xy(21, 1));
		builder.add(btnView, cc.xy(23, 1));
		builder.add(btnClear, cc.xy(25, 1));
		// builder.add(btnPrint, cc.xy(35, 1));
	 
		((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_GTC)
				.addTableModelListener(new RowUpdate());
		/*table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_GTC), filterOrder,
				(Hashtable) hSetting.get("table"));*/
		table.getTable().add(popupMenu);

		table.getTable().addMouseListener(new MyMouseAdapter());
		table.addMouseListener(new MyCustomMouseAdapter());
		table.setColumnHide(GTCDef.columnhide);
		customSelection = new CustomSelectionModel();
		customSelection
				.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		table.getTable().setSelectionModel(customSelection);
		table.getTable().setSelectionMode(
				ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		/*
		 * JScrollPane t = new JScrollPane(editorPanel);
		 * t.setHorizontalScrollBarPolicy
		 * (ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		 * t.setVerticalScrollBarPolicy
		 * (ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		 */

		/*
		 * table.getTable().addMouseListener(new MyMouseAdapter());
		 * table.addMouseListener(new MyCustomMouseAdapter());
		 * table.setColumnHide(GTCDef.columnhide);
		 */
		/*
		 * customSelection = new CustomSelectionModel(); customSelection
		 * .setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		 * table.getTable().setSelectionModel(customSelection);
		 * table.getTable().setSelectionMode(
		 * ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		 */
		table.getTable().addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == 1 && e.getClickCount() == 2
						&& isValidRow(e.getX(), e.getY(), false)) {

					JTable tblViewList = table.getTable();
					int nRow = tblViewList.rowAtPoint(new Point(e.getX(), e
							.getY()));

					if (nRow >= 0) {
						tblViewList.setRowSelectionInterval(nRow, nRow);
						tblViewList.getSelectionModel().addSelectionInterval(
								nRow, nRow);
						Order data = (Order) table.getDataByIndex(nRow);

						if (data.getStatus().equals(Order.C_FULL_MATCH)
								|| data.getStatus().equals(
										Order.C_PARTIAL_MATCH)) {
							apps.getUI().showUI(TradingUI.UI_TRADE);
							UITrade tradelist = (UITrade) apps.getUI().getForm(
									TradingUI.UI_TRADE);
							tradelist.setFilteredMarket(data.getMarketOrderId());

						}
					}

				} else if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
		});
		table.addMouseListener(new CustomMouseAdapter() {
			public void mousePressed(MouseEvent e) {

				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {

					if (isValidRow(e.getX(), e.getY(), e.isControlDown())) {
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
		});

		registerEvent(table);
		registerEvent(table.getTable());
		table.getTable().getModel().addTableModelListener(new RowChanged());
		table.getTable().getModel().addTableModelListener(new RowChangeList());

		tCount = new JNumber(Double.class, 0, 0, false, false);
		oCount = new JNumber(Double.class, 0, 0, false, false);
		oValue = new JNumber(Double.class, 0, 0, false, false);
		dCount = new JNumber(Double.class, 0, 0, false, false);
		dValue = new JNumber(Double.class, 0, 0, false, false);
		tLot = new JNumber(Double.class, 0, 0, false, false);
		tValue = new JNumber(Double.class, 0, 0, false, false);

		JPanel infoPanel = new JPanel();
		FormLayout infLayout = new FormLayout(
				"pref,2dlu,80px,2dlu,pref,2dlu,80px, 2dlu, pref, 2dlu, 110px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 110px, 2dlu, pref, 2dlu, 80px, 2dlu, pref, 2dlu, 110px",
				"pref");
		PanelBuilder infBuilder = new PanelBuilder(infLayout, infoPanel);
		infBuilder.setBorder(new EmptyBorder(4, 4, 4, 4));

		infBuilder.add(new JLabel("Count"), cc.xy(1, 1));
		infBuilder.add(tCount, cc.xy(3, 1));
		infBuilder.add(new JLabel("Open Count"), cc.xy(5, 1));
		infBuilder.add(oCount, cc.xy(7, 1));
		infBuilder.add(new JLabel("Open Value"), cc.xy(9, 1));
		infBuilder.add(oValue, cc.xy(11, 1));
		infBuilder.add(new JLabel("Done Count"), cc.xy(13, 1));
		infBuilder.add(dCount, cc.xy(15, 1));
		infBuilder.add(new JLabel("Done Value"), cc.xy(17, 1));
		infBuilder.add(dValue, cc.xy(19, 1));
		infBuilder.add(new JLabel("T.Lot"), cc.xy(21, 1));
		infBuilder.add(tLot, cc.xy(23, 1));
		infBuilder.add(new JLabel("T.Value"), cc.xy(25, 1));
		infBuilder.add(tValue, cc.xy(27, 1));

		this.setBorder(new EmptyBorder(2, 2, 2, 2));
		JScrollPane t1 = new JScrollPane(editorPanel);
		t1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		t1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		this.add(t1, BorderLayout.NORTH);
		this.add(table, BorderLayout.CENTER);

		JScrollPane t2 = new JScrollPane(infoPanel);
		t2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		t2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

		this.add(t2, BorderLayout.PAGE_END);
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		if (!sales.getProfileId().equalsIgnoreCase("CUSTOMER")) {
			btnNotif.setEnabled(false);
		}

		try {
			comboBOS.setSelectedIndex(0);
			comboStatus.setSelectedIndex(0);
			comboTaker.setSelectedIndex(0);
			/*
			 * comboBoard.setSelectedIndex(0); comboFloor.setSelectedIndex(0);
			 */
			price.setValue(null);
		} catch (Exception ex) {
		}
		refresh();
		initData();

		pnlContent = new JSkinPnl();
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		pnlContent.add(t1, BorderLayout.NORTH);
		pnlContent.add(t2, BorderLayout.SOUTH);
	}

	private void add(JScrollPane t1, String north) {
		// TODO Auto-generated method stub

	}

	private void setBorder(EmptyBorder emptyBorder) {
		// TODO Auto-generated method stub

	}

	protected boolean isValidRowSingle(int x, int y) {
		int nRow = -1;
		String status = "";
		Order order = new Order();
		order.setDoneLot(new Double(0));
		try {
			JTable tblViewList = table.getTable();
			nRow = tblViewList.rowAtPoint(new Point(x, y));

			if (nRow >= 0) {
				tblViewList.setRowSelectionInterval(nRow, nRow);
				Order data = (Order) table.getDataByIndex(nRow);
				status = data.getStatus();
				order = data;
			}

			for (int i = 0; i < popupMenu.getComponentCount(); i++) {
				Object tool = popupMenu.getComponent(i);
				if (tool instanceof JMenuItem) {
					if (((JMenuItem) tool).getActionCommand()
							.equals("withdraw")) {
						((JMenuItem) tool)
								.setEnabled((withdrawAction != null
										&& withdrawAction.isEnabled()
										&& nRow >= 0 && (status
										.equals(Order.C_OPEN)
										|| status.equals(Order.C_PARTIAL_MATCH) || (order
										.getStatus().equals(Order.C_ENTRY) && order
										.getIsBasketOrder().equals("1")))));
						((JMenuItem) tool)
								.setVisible((withdrawAction != null
										&& withdrawAction.isEnabled()
										&& nRow >= 0 && (status
										.equals(Order.C_OPEN)
										|| status.equals(Order.C_PARTIAL_MATCH) || (order
										.getStatus().equals(Order.C_ENTRY) && order
										.getIsBasketOrder().equals("1")))));
					/*} else if (((JMenuItem) tool).getActionCommand().equals(
							"amend")) {
						((JMenuItem) tool)
								.setEnabled((amendAction != null
										&& amendAction.isEnabled() && nRow >= 0 && (status
										.equals(Order.C_OPEN) || status
										.equals(Order.C_PARTIAL_MATCH))
										&& !order.getOrderType().equals("G")));
						((JMenuItem) tool)
								.setVisible((amendAction != null
										&& amendAction.isEnabled() && nRow >= 0 && (status
										.equals(Order.C_OPEN) || status
										.equals(Order.C_PARTIAL_MATCH))
										&& !order.getOrderType().equals("G")));*/
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"split")) {
						((JMenuItem) tool)
								.setEnabled((splitAction != null
										&& splitAction.isEnabled()
										&& nRow >= 0
										&& ((status.equals(Order.C_FULL_MATCH) && order
												.getSplitBy().equals(""))
												|| status
														.equals(Order.C_WITHDRAW) || status
												.equals(Order.C_AMEND)) && order
										.getDoneVol().doubleValue() > 0));
						((JMenuItem) tool)
								.setVisible((splitAction != null
										&& splitAction.isEnabled()
										&& nRow >= 0
										&& ((status.equals(Order.C_FULL_MATCH) && order
												.getSplitBy().equals(""))
												|| status
														.equals(Order.C_WITHDRAW) || status
												.equals(Order.C_AMEND)) && order
										.getDoneVol().doubleValue() > 0));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"resend")) {

						boolean isenable = false;
						/*log.info(" cek " + order.getOrderIDContra().isEmpty()
								+ " " + order.getStatus());*/
						if ((order.getStatus().equals(Order.C_REJECTED) || order
								.getStatus().equals(Order.C_REJECTED2))
								&& !order.getOrderIDContra().isEmpty()) {
							//log.info("cek false");
							isenable = false;
						} else if (status.equals(Order.C_REJECTED)
								|| status.equals(Order.C_REJECTED2)) {
							//log.info("cek true");
							isenable = true;
						}
						((JMenuItem) tool).setEnabled(isenable);
					}
				}
			}
			return true;
		} catch (Exception z) {
			return false;
		}
	}
	
	protected boolean _withdrawMenu = false;
	protected boolean _sendMenu = false;
	protected boolean _deleteMenu = false;
	protected boolean _splitMenu = false;

	protected void createPopup() {
		popupMenu = new JPopupMenu();
		resendMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				table.showProperties();
				log.info("user selected popup");
				try {

					Vector v = getSelected();
					log.info("user selected resend from popup menu " + v.size());
					StringBuffer info = new StringBuffer("");
					final Vector vorder = new Vector();
					if (v.size() == 1) {
						Order order = (Order) table.getDataByIndex(table
								.getSelectedRow());
						if (order == null) {
							Utils.showMessage("Please select order to resend",
									form);
							return;
						}
						if (order.getBoard().equals("NG")
								&& !negdealAction.isEnabled()) {
							Utils.showMessage(
									"access denied, please contact your helpdesk",
									form);
							return;
						}
						log.info("open resend dialog for confirmation, orderid: "
								+ order.getId()
								+ " with status "
								+ order.getStatus());
						if (order.getStatus().equals(Order.C_REJECTED)
								|| order.getStatus().equals(Order.C_REJECTED2)) {
							info.append("are you sure want to resend this order?\n");
							info.append(genConfirm(order));
						}
					} else {
						log.info("open resend dialog for confirmation, for multiple resend (more than 1 row)");
						if (v.size() <= 20) {
							info.append("are you sure want to resend these order?\n");
							for (int i = 0; i < v.size(); i++) {
								Order order = (Order) v.elementAt(i);
								if (order.getBoard().equals("NG")
										&& !negdealAction.isEnabled()) {
									Utils.showMessage(
											"access denied, please contact your helpdesk",
											form);
									return;
								}
								if (order.getStatus().equals(Order.C_REJECTED)
										|| order.getStatus().equals(
												Order.C_REJECTED2)) {
									info = info.append(genConfirm(order));
								}
							}
						} else {
							info.append("are you sure want to resend all selected order ("
									+ v.size() + " records) ?\n");
						}
					}

					int nconfirm = Utils.C_YES_DLG;
					nconfirm = Utils.showConfirmDialog(form, "Resend Order",
							info.toString(), Utils.C_YES_DLG);
					if (nconfirm == Utils.C_YES_DLG) {

						/*((IEQTradeApp) apps).getTradingEngine()
								.withdrawOrderMulti(vorder);*/

						try {
							int rows = table.getSelectedRow();
							if (table.getTable().getRowCount() == 1)
								rows = 0;
							if (rows >= 0)
								table.getTable().setRowSelectionInterval(rows,
										rows);
						} catch (Exception ex) {
						}

						if (v.size() == 1) {
							Order orderCopy = (Order) table
									.getDataByIndex(table.getSelectedRow());
							Order order = (orderCopy);
							log.info(order.getStatus());
							if (orderCopy.getStatus().equals(Order.C_REJECTED)
									|| orderCopy.getStatus().equals(
											Order.C_REJECTED2)) {

								((IEQTradeApp) apps).getTradingEngine()
										.createOrder(order);
							}
						} else if (v.size() > 1) {
							final Vector vrow = new Vector((int) v.size());
							for (int i = 0; i < v.size(); i++) {
								Order orderCopy = (Order) v.elementAt(i);
								Order order = genOrderGTC(orderCopy);
								if (orderCopy.getStatus().equals(
										Order.C_REJECTED)
										|| orderCopy.getStatus().equals(
												Order.C_REJECTED2)) {
									// ((IEQTradeApp) apps).getTradingEngine()
									// .createOrder(order);
									vrow.addElement(GTCDef
											.createTableRow(order));
									vorder.addElement(order);
								}
							}

							((IEQTradeApp) apps).getTradingEngine()
									.getStore(TradingStore.DATA_GTC)
									.addRow(vrow, false, false);

							/*((IEQTradeApp) apps).getTradingEngine()
									.createGTCMulti(vorder);*/
							((IEQTradeApp) apps).getTradingEngine()
									.getStore(TradingStore.DATA_GTC).refresh();

						}
						try {
							int rows = table.getSelectedRow();
							if (table.getTable().getRowCount() == 1)
								rows = 0;
							if (rows >= 0)
								table.getTable().setRowSelectionInterval(rows,
										rows);
						} catch (Exception ex) {
						}
					}

				} catch (Exception ex) {
					ex.printStackTrace();
					Utils.logException(ex);
				}
			}
		});

		resendMenu.setText("Resend");
		resendMenu.setActionCommand("resend");

		printExcelMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				// savetoFile();
			}
		});

		printExcelMenu.setText("save to excel");
		printExcelMenu.setActionCommand("printExcel");

		withdrawMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				log.info("user selected popup");
				try {
					if (withdrawAction == null || !withdrawAction.isEnabled())
						return;
					if (!_withdrawMenu) {
						_withdrawMenu = true;

						Vector v = getSelected();
						log.info("user selected withdraw from popup menu "
								+ v.size());
						StringBuffer info = new StringBuffer("");
						if (v.size() == 1) {
							Order order = (Order) table.getDataByIndex(table
									.getSelectedRow());
							if (order == null) {
								Utils.showMessage(
										"Please select order to withdraw", form);
								return;
							}
							if (order.getBoard().equals("NG")
									&& !negdealAction.isEnabled()) {
								Utils.showMessage(
										"access denied, please contact your helpdesk",
										form);
								return;
							}
							log.info("open withdraw dialog for confirmation, orderid: "
									+ order.getId()
									+ " with status "
									+ order.getStatus());
							if (order.getStatus().equals(Order.C_OPEN)
									|| order.getStatus().equals(
											Order.C_PARTIAL_MATCH)
									|| (order.getStatus().equals(Order.C_ENTRY) && order
											.getIsBasketOrder().equals("1"))) {
								info.append("are you sure want to withdraw this order?\n");
								info.append(genConfirm(order));
							}
						} else {
							log.info("open resend dialog for confirmation, for multiple resend (more than 1 row) ");

							if (v.size() <= 20) {
								info.append("are you sure want to withdraw these order?\n");
								for (int i = 0; i < v.size(); i++) {
									Order order = (Order) v.elementAt(i);
									if (order.getBoard().equals("NG")
											&& !negdealAction.isEnabled()) {
										Utils.showMessage(
												"access denied, please contact your helpdesk",
												form);
										return;
									}
									if (order.getStatus().equals(Order.C_OPEN)
											|| order.getStatus().equals(
													Order.C_PARTIAL_MATCH)
											|| (order.getStatus().equals(
													Order.C_ENTRY) && order
													.getIsBasketOrder().equals(
															"1"))) {
										info = info.append(genConfirm(order));
									}
								}
							} else {
								info.append("are you sure want to withdraw all selected order ("
										+ v.size() + " records) ?\n");
							}
						}
						int nconfirm = Utils.C_YES_DLG;
						nconfirm = Utils.showConfirmDialog(form,
								"Withdraw Order", info.toString(),
								Utils.C_YES_DLG);
						if (nconfirm == Utils.C_YES_DLG) {
							final Vector vorder = new Vector(v.size());
							for (int i = 0; i < v.size(); i++) {
								Order order = (Order) v.elementAt(i);
								if (order.getStatus().equals(Order.C_OPEN)
										|| order.getStatus().equals(
												Order.C_PARTIAL_MATCH)
										|| (order.getStatus().equals(
												Order.C_ENTRY) && order
												.getIsBasketOrder().equals("1"))) {
									((IEQTradeApp) apps).getTradingEngine()
											.withdrawGtc(order);
									// vorder.add(order);
								}
							}
							// ((IEQTradeApp) apps).getTradingEngine()
							// .withdrawGtc(vorder);
							try {
								int rows = table.getSelectedRow();
								if (table.getTable().getRowCount() == 1)
									rows = 0;
								if (rows >= 0)
									table.getTable().setRowSelectionInterval(
											rows, rows);
							} catch (Exception ex) {
							}
							Utils.showMessage("GTC Cancellation will not withdraw your Order automatically.\n			        	Please check your order.", form); //yosep message confirm withdraw
						}
						_withdrawMenu = false;
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					Utils.logException(ex);
					_withdrawMenu = false;
				}
			}
		});
		withdrawMenu.setText("Withdraw");
		withdrawMenu.setActionCommand("withdraw");

		splitMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				if (splitAction == null || !splitAction.isEnabled())
					return;
				Order order = (Order) table.getDataByIndex(table
						.getSelectedRow());
				if (order == null) {
					Utils.showMessage("Please select order to split", form);
				} else {
					log.info("user selected split from popup menu, orderid: "
							+ order.getId());
					if ((order.getStatus().equals(Order.C_FULL_MATCH) && order
							.getSplitBy().equals(""))
							|| ((order.getStatus().equals(Order.C_AMEND) || order
									.getStatus().equals(Order.C_WITHDRAW)) && order
									.getDoneVol().doubleValue() > 0)) {
						if (splitAction != null)
							splitAction.actionPerformed(new ActionEvent(order,
									0, ""));
					}
				}
			}
		});
		splitMenu.setText("Split Trade");
		splitMenu.setActionCommand("split");

		propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");

		popupMenu.add(resendMenu);
		popupMenu.add(withdrawMenu);

		popupMenu.addSeparator();
		popupMenu.add(splitMenu);
		popupMenu.addSeparator();
		popupMenu.add(propertiesMenu);
		popupMenu.add(printExcelMenu);
	}

	protected boolean isValidRow(int x, int y, boolean ctrlDown) {
		return isValidRowMulti(x, y, ctrlDown);
	}

	protected boolean isValidRowMulti(int x, int y, boolean ctrlDown) {
		int nRow = -1;
		String status = "";
		Order order = new Order();
		order.setDoneLot(new Double(0));
		try {
			JTable tblViewList = table.getTable();
			nRow = tblViewList.rowAtPoint(new Point(x, y));

			if (nRow >= 0) {
				Vector vselected = getSelected();
				nRow = vselected.size();
				if (isAllTemporary(vselected)) {
					status = Order.C_TEMPORARY;
				} else if (isAllOpen(vselected)) {
					status = Order.C_OPEN;
				} else if (isAllRejected(vselected)) {
					status = Order.C_REJECTED;
				} else if (isAllQueue(vselected)) {
					status = Order.C_ENTRY;
				} else
					status = "";

				if (vselected.size() == 1) {
					Order data = (Order) vselected.get(0);
					status = data.getStatus();
					order = data;
					if (status.equals(Order.C_ENTRY))
						status = "";
				}
				// log.info("status "+status);
				/*
				 * if (ctrlDown) { if (isAllTemporary(getSelected())) { status =
				 * Order.C_TEMPORARY; } else if (isAllOpen(getSelected())) {
				 * status = Order.C_OPEN; } else if
				 * (isAllRejected(getSelected())) { status = Order.C_REJECTED; }
				 * else status = ""; } else {
				 * tblViewList.setRowSelectionInterval(nRow, nRow);
				 * tblViewList.getSelectionModel().addSelectionInterval(nRow,
				 * nRow); Order data = (Order) table.getDataByIndex(nRow);
				 * status = data.getStatus(); order = data; }
				 */}

			for (int i = 0; i < popupMenu.getComponentCount(); i++) {
				Object tool = popupMenu.getComponent(i);
				if (tool instanceof JMenuItem) {
					if (((JMenuItem) tool).getActionCommand()
							.equals("withdraw")) {
						((JMenuItem) tool)
								.setEnabled((withdrawAction != null
										&& withdrawAction.isEnabled()
										&& nRow >= 0 && (status
										.equals(Order.C_OPEN)
										|| status
												.equals(Order.C_SENDING_TEMPORARY)
										|| status.equals(Order.C_PARTIAL_MATCH)
										|| (order.getStatus().equals(
												Order.C_ENTRY) && order
												.getIsBasketOrder().equals("1")) || status
										.equals(Order.C_ENTRY)))
										&&apps.getAction().get(TradingAction.A_SHOWBUYORDER).isGranted()
										);
						// ((JMenuItem)tool).setVisible((withdrawAction!=null &&
						// withdrawAction.isEnabled() && nRow>=0 &&
						// (status.equals(Order.C_OPEN) ||
						// status.equals(Order.C_PARTIAL_MATCH) || (
						// order.getStatus().equals(Order.C_ENTRY) &&
						// order.getIsBasketOrder().equals("1")))));
					/*} else if (((JMenuItem) tool).getActionCommand().equals(
							"amend")) {
						((JMenuItem) tool)
								.setEnabled((!ctrlDown && amendAction != null
										&& amendAction.isEnabled() && nRow == 1 && (status
										.equals(Order.C_OPEN) || status
										.equals(Order.C_PARTIAL_MATCH)))
										&&apps.getAction().get(TradingAction.A_SHOWBUYORDER).isGranted()
										&& !order.getOrderType().equals("G"));*/
						// ((JMenuItem)tool).setVisible((!ctrlDown &&
						// amendAction!=null && amendAction.isEnabled() &&
						// nRow>=0 && (status.equals(Order.C_OPEN) ||
						// status.equals(Order.C_PARTIAL_MATCH))));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"split")) {
						((JMenuItem) tool)
								.setEnabled((!ctrlDown
										&& splitAction != null
										&& splitAction.isEnabled()
										&& nRow == 1
										&& ((status.equals(Order.C_FULL_MATCH) && order
												.getSplitBy().equals(""))
												|| status
														.equals(Order.C_WITHDRAW) || status
												.equals(Order.C_AMEND)) && order
										.getDoneVol().doubleValue() > 0));
						// ((JMenuItem)tool).setVisible((splitAction!=null &&
						// splitAction.isEnabled() && nRow>=0 &&
						// (status.equals(Order.C_FULL_MATCH) ||
						// status.equals(Order.C_WITHDRAW) ||
						// status.equals(Order.C_AMEND)) &&
						// order.getDoneLot().doubleValue()>0 ));
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"resend")) {
						((JMenuItem) tool).setEnabled((status
								.equals(Order.C_REJECTED))
								|| status.equals(Order.C_REJECTED2)
								&& nRow >= 0
								&& order.getOrderIDContra().isEmpty());
					} else if (((JMenuItem) tool).getActionCommand().equals(
							"printExcel")) {
						((JMenuItem) tool).setEnabled(true);
					}
				}
			}
			return true;
		} catch (Exception z) {
			z.printStackTrace();
			return false;
		}
	}

	void registerEvent(JComponent comp) {
		// comp.getActionMap().get("withdrawAction").
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "secAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "clientAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0),
				"withdrawAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0), "amendAction");
		inputMap.put(KeyStroke.getKeyStroke("alt W"), "withdrawAction");
		inputMap.put(KeyStroke.getKeyStroke("alt A"), "amendAction");
		inputMap.put(KeyStroke.getKeyStroke("alt I"), "splitAction");
		comp.getActionMap().put("withdrawAction",
				new AbstractAction("withdrawAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {

						Thread thread = new Thread() {

							@Override
							public void run() {
								Vector v = getSelected();
								log.info("user selected withdraw from popup menu "
										+ v.size());
								StringBuffer info = new StringBuffer("");
								final Vector vorder = new Vector();
								if (v.size() == 1) {
									Order order = (Order) table
											.getDataByIndex(table
													.getSelectedRow());
									if (order == null) {
										Utils.showMessage(
												"Please select order to withdraw",
												form);
										return;
									}
									if (order.getBoard().equals("NG")
											&& !negdealAction.isEnabled()) {
										Utils.showMessage(
												"access denied, please contact your helpdesk",
												form);
										return;
									}
									log.info("open withdraw dialog for confirmation, orderid: "
											+ order.getId()
											+ " with status "
											+ order.getStatus());
									if (order.getStatus().equals(Order.C_OPEN)
											|| order.getStatus().equals(
													Order.C_PARTIAL_MATCH)
											|| (order.getStatus().equals(
													Order.C_ENTRY) && order
													.getIsBasketOrder().equals(
															"1"))) {
										info.append("are you sure want to withdraw this order?\n");
										info.append(genConfirm(order));
										vorder.add(order);
									}
								} else {
									log.info("open withdraw dialog for confirmation, for multiple withdraw (more than 1 row) "
											+ v.size());
									if (v.size() <= 20) {
										info.append("are you sure want to withdraw these order?\n");
										for (int i = 0; i < v.size(); i++) {
											Order order = (Order) v
													.elementAt(i);
											if (order.getBoard().equals("NG")
													&& !negdealAction
															.isEnabled()) {
												Utils.showMessage(
														"access denied, please contact your helpdesk",
														form);
												return;
											}
											if (order.getStatus().equals(
													Order.C_OPEN)
													|| order.getStatus()
															.equals(Order.C_PARTIAL_MATCH)
													|| (order
															.getStatus()
															.equals(Order.C_ENTRY) && order
															.getIsBasketOrder()
															.equals("1"))) {
												info = info
														.append(genConfirm(order));
												vorder.add(order);
											}
										}
									} else {
										vorder.addAll(v);
										info.append("are you sure want to withdraw all selected order ("
												+ v.size() + " records) ?\n");
									}
								}

								if (vorder.size() > 0) {
									int nconfirm = Utils.C_YES_DLG;

									nconfirm = Utils.showConfirmDialog(form,
											"Withdraw Order", info.toString(),
											Utils.C_YES_DLG);
									if (nconfirm == Utils.C_YES_DLG) {

										/*
										 * for (int i = 0; i < vorder.size();
										 * i++) { Order order = (Order)
										 * v.elementAt(i); if
										 * (order.getStatus().equals(
										 * Order.C_OPEN) ||
										 * order.getStatus().equals(
										 * Order.C_PARTIAL_MATCH) ||
										 * (order.getStatus().equals(
										 * Order.C_ENTRY) && order
										 * .getIsBasketOrder() .equals("1"))) {
										 * // ((IEQTradeApp) //
										 * apps).getTradingEngine() //
										 * .withdrawOrder(order);
										 * 
										 * } }
										 */

										/*((IEQTradeApp) apps).getTradingEngine()
												.withdrawOrderMulti(vorder);*/

										try {
											int rows = table.getSelectedRow();
											if (table.getTable().getRowCount() == 1)
												rows = 0;
											if (rows >= 0)
												table.getTable()
														.setRowSelectionInterval(
																rows, rows);
										} catch (Exception ex) {
										}
									}
								}
							}
						};

						thread.start();

						/*
						 * int i = table.getSelectedRow(); if
						 * (table.getTable().getRowCount() == 1) i = 0; if (i >=
						 * 0) { Order order = (Order) table.getDataByIndex(i);
						 * if (withdrawAction != null) { if
						 * (withdrawAction.isEnabled()) { log.info(
						 * "user hits F7, open withdraw confirmation for orderid: "
						 * + order.getId()); if
						 * (order.getStatus().equals(Order.C_OPEN) ||
						 * order.getStatus().equals( Order.C_PARTIAL_MATCH) ||
						 * (order.getStatus().equals( Order.C_ENTRY) && order
						 * .getIsBasketOrder().equals( "1"))) { //
						 * ((IEQTradeApp)
						 * apps).getTradingEngine().withdrawOrder(order);
						 * StringBuffer info = new StringBuffer("");
						 * info.append(
						 * "are you sure want to withdraw this order?\n");
						 * info.append(genConfirm(order)); int nconfirm =
						 * Utils.C_YES_DLG; nconfirm = Utils.showConfirmDialog(
						 * form, "Withdraw Order", info.toString(),
						 * Utils.C_YES_DLG); if (nconfirm == Utils.C_YES_DLG) {
						 * if (order.getStatus().equals( Order.C_OPEN) ||
						 * order.getStatus() .equals(Order.C_PARTIAL_MATCH) ||
						 * (order .getStatus() .equals(Order.C_ENTRY) && order
						 * .getIsBasketOrder() .equals("1"))) { ((IEQTradeApp)
						 * apps) .getTradingEngine() .withdrawOrder(order); }
						 * try { int rows = table .getSelectedRow();
						 * 
						 * if (table.getTable() .getRowCount() == 1) rows = 0;
						 * if (rows >= 0) table .getTable()
						 * .setRowSelectionInterval( rows, rows); } catch
						 * (Exception ex) { } } } } } }else {
						 * Utils.showMessage("Please select order to withdraw",
						 * form); }
						 */
					}
				});

		comp.getActionMap().put("splitAction", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Order order = (Order) table.getDataByIndex(table
						.getSelectedRow());
				if (order == null) {
					Utils.showMessage("Please select order to split", form);
				} else {
					log.info("user selected split from popup menu, orderid: "
							+ order.getId());
					if ((order.getStatus().equals(Order.C_FULL_MATCH) && order
							.getSplitBy().equals(""))
							|| ((order.getStatus().equals(Order.C_AMEND) || order
									.getStatus().equals(Order.C_WITHDRAW)) && order
									.getDoneVol().doubleValue() > 0)) {
						if (splitAction != null)
							splitAction.actionPerformed(new ActionEvent(order,
									0, ""));
					}
				}
			}
		});
		/*comp.getActionMap().put("amendAction",
				new AbstractAction("amendAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						if (amendAction == null || !amendAction.isEnabled())
							return;
						Order order = (Order) table.getDataByIndex(table
								.getSelectedRow());
						if (order == null) {
							Utils.showMessage("Please select order to amend",
									form);
						} else {
							if (order.getBoard().equals("NG")
									&& !negdealAction.isEnabled()) {
								Utils.showMessage(
										"access denied, please contact your helpdesk",
										form);
								return;
							}
							log.info("user hits F8(amend), orderid: "
									+ order.getId());
							if (order.getStatus().equals(Order.C_OPEN)
									|| order.getStatus().equals(
											Order.C_PARTIAL_MATCH)) {
								if (amendAction != null)
									amendAction
											.actionPerformed(new ActionEvent(
													order, 0, ""));
							}
						}
					}
				});*/
		comp.getActionMap().put("secAction", new AbstractAction("secAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				comboStock.getTextEditor().requestFocus();
			}
		});
		comp.getActionMap().put("clientAction",
				new AbstractAction("clientAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						comboClient.getTextEditor().requestFocus();
					}
				});
		comp.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						form.setVisible(false);
					}
				});

		comp.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton) {
							((JButton) evt.getSource()).doClick();
						} else {
							if (evt.getSource() instanceof JNumber) {
								try {
									((JNumber) evt.getSource()).commitEdit();
								} catch (Exception ex) {
								}
								;
							}
						}
					}
				});
	}

	private void registerEventCombo(JDropDown comp) {
		// comp.getEditor().getEditorComponent()
		// .setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(
				JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap()
				.put("enterAction", new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(comboStock.getTextEditor())) {
							comboStock.setSelectedIndex(comboStock
									.getSelectedIndex());
							comboStock.hidePopup();
							price.requestFocus();
						} else if (e.getSource().equals(
								comboClient.getTextEditor())) {
							comboClient.setSelectedIndex(comboClient
									.getSelectedIndex());
							comboClient.hidePopup();
							comboStock.requestFocus();
						}

						// comboStock.requestFocus();

					}

				});

		/*
		 * comp.getTextEditor().getActionMap() .put("tabAction", new
		 * AbstractAction("tabAction") { private static final long
		 * serialVersionUID = 1L;
		 * 
		 * @Override public void actionPerformed(ActionEvent e) {
		 * 
		 * if (e.getSource().equals(comboStock.getTextEditor())) {
		 * comboStock.setSelectedIndex(comboStock .getSelectedIndex());
		 * comboStock.hidePopup(); price.requestFocus();
		 * 
		 * } else if (e.getSource().equals( comboClient.getTextEditor())) {
		 * comboClient.setSelectedIndex(comboClient .getSelectedIndex());
		 * comboClient.hidePopup(); comboStock.requestFocus(); } }
		 * 
		 * });
		 */}

	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
		((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_GTC).refresh();
	}
	
	public class MyMouseAdapter extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			if (e.isPopupTrigger()) {

				if (isValidRow(e.getX(), e.getY())) {
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		}

		public class MyCustomMouseAdapter extends CustomMouseAdapter {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {

					if (isValidRow(e.getX(), e.getY())) {
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					if (isValidRow(e.getX(), e.getY())) {
						popupMenu.show(e.getComponent(), e.getX(), e.getY());
					}
				}
			}
		}

	}

	@Override
	public void focus() {
		// TODO Auto-generated method stub

	}

	
	private boolean isSuperUser() {
		// return true;
		UserProfile sales = (UserProfile) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_USERPROFILE)
				.getDataByField(new Object[] { "40" },
						new int[] { UserProfile.C_MENUID });
		return sales != null ? sales.getProfileId().toUpperCase()
				.equals("SUPERUSER") : false;
		// return false;
	}

	public boolean isFilterClientAdded(String client) {
		try {
			String sf = ((IEQTradeApp) apps).getTradingEngine().getConnection()
					.loadFilterByClient();
			String[] sp = sf.split("#");
			for (int i = 0; i < sp.length; i++)
				if (sp[i].trim().toLowerCase().equals(client))
					return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;

	}

	Vector getStatusCode() {
		Vector v = new Vector();
		if (comboStatus.getSelectedIndex() == 0) {
			return null;
		} else {
			OrderStatus os = (OrderStatus) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ORDERSTATUS)
					.getDataByField(
							new Object[] { comboStatus.getSelectedItem() },
							new int[] { OrderStatus.C_NAME });
			if (os != null) {
				Utils.parser(os.getId(), "|", v);
			}
			return v;
		}
	}

	public void setState(boolean state) {
		/*
		 * btnView.setEnabled(state); btnClear.setEnabled(state);
		 * isLoadingSuperUser = !state;
		 */
	}

	String getBOSCode() {
		Mapping m = (Mapping) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_SIDE)
				.getDataByKey(new Object[] { comboBOS.getSelectedItem() });
		return (m != null) ? m.getName() : null;
	}

	String getFloorCode() {
		Mapping m = (Mapping) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_FLOORFILTER)
				.getDataByField(new Object[] { comboFloor.getSelectedItem() },
						new int[] { Mapping.CIDX_NAME });
		return (m != null) ? m.getCode() : null;
	}

	Vector getBoardCode() {
		Vector v = new Vector();
		if (comboBoard.getSelectedIndex() == 0) {
			return null;
		} else {
			Mapping m = (Mapping) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_BOARDFILTER)
					.getDataByField(
							new Object[] { comboBoard.getSelectedItem() },
							new int[] { Mapping.CIDX_NAME });
			if (m != null) {
				Utils.parser(m.getCode(), "|", v);
			}
			return v;
		}
	}

	private static SimpleDateFormat dateFormat2 = new SimpleDateFormat(
			"yyyyMMdd");
	public final static SimpleDateFormat fridayFormat = new SimpleDateFormat(
			"E");

	/*
	 * void stockfocusLost(FocusEvent e) { try { if
	 * (!comboStock.getText().trim().equals("")) {
	 * comboStock.getTextEditor().setText( comboStock.getText().toUpperCase());
	 * int i = ((IEQTradeApp) apps).getTradingEngine()
	 * .getStore(TradingStore.DATA_STOCK) .getIndexByKey(new String[] {
	 * comboStock.getText() }); if (i >= 0) { Stock stock = (Stock)
	 * ((IEQTradeApp) apps) .getTradingEngine()
	 * .getStore(TradingStore.DATA_STOCK) .getDataByIndex(i); if (stock != null)
	 * {
	 * 
	 * fieldStockName.setText(stock.getName()); //StockLotSize =
	 * stock.getLotSize(); //fieldStockName.setSelectedItem(stock.getName());
	 * 
	 * // set put in basket here boolean stockpreopening = stock.isPreopening();
	 * String dt = dateFormat2.format(OrderIDGenerator .getTime()); boolean
	 * friday = fridayFormat .format(OrderIDGenerator.getTime())
	 * .toUpperCase().equals("FRI"); //Perubahan Jats Next G jam perdagangan
	 * preopening String dtop = dt + "-" + "09:10:00"; //String dtop = dt + "-"
	 * + "08:45:00"; String dtendpre = dt + "-" + "09:25:00";
	 * 
	 * String dtses1 = dt + "-" + "09:30:00"; String dtses2 = dt + "-" + (friday
	 * ? "14:00:00" : "13:30:00"); String dtendses1 = dt + "-" + (friday ?
	 * "11:30:00" : "12:00:00"); Date datepreopening =
	 * dateFormatAll.parse(dtop); Date dateses1 = dateFormatAll.parse(dtses1);
	 * Date dateses2 = dateFormatAll.parse(dtses2); Date enddateses1 =
	 * dateFormatAll.parse(dtendses1); Date endpre =
	 * dateFormatAll.parse(dtendpre); Date datecurrent =
	 * OrderIDGenerator.getTime(); if (datecurrent.getTime() <
	 * datepreopening.getTime()) { checkBasket.setSelected(true);
	 * fieldTimer.setText(stockpreopening ? "09:10:00" : "09:30:00");
	 * comboSend.setSelectedIndex(1); } else if (datecurrent.getTime() <
	 * endpre.getTime()) { if (!stockpreopening) {
	 * checkBasket.setSelected(true); fieldTimer.setText("09:30:00");
	 * comboSend.setSelectedIndex(1); } } else if (datecurrent.getTime() >
	 * endpre.getTime() && datecurrent.getTime() < dateses1.getTime()) {
	 * checkBasket.setSelected(true); fieldTimer.setText("09:30:00");
	 * comboSend.setSelectedIndex(1); } else if (datecurrent.getTime() >
	 * enddateses1 .getTime() && datecurrent.getTime() < dateses2.getTime()) {
	 * checkBasket.setSelected(true); fieldTimer .setText(friday ? "14:00:00" :
	 * "13:30:00"); comboSend.setSelectedIndex(1); } else {
	 * checkBasket.setSelected(false); comboSend.setSelectedIndex(0); }
	 * 
	 * // set put in basket here String brid = comboBoard.getText(); boolean
	 * stockpreopening = stock.isPreopening(); String dt =
	 * dateFormat2.format(OrderIDGenerator .getTime()); boolean friday =
	 * fridayFormat .format(OrderIDGenerator.getTime())
	 * .toUpperCase().equals("FRI"); String dtop = dt + "-" + TradingSetting
	 * .getDateSession(TradingSetting.C_FIRST_PREOP); // "09:10:00"; String
	 * dtses1 = dt + "-" + TradingSetting
	 * .getDateSession(TradingSetting.C_FIRST_SES1); // "09:30:00"; String
	 * dtses2 = dt + "-" + (friday ? TradingSetting
	 * .getDateSession(TradingSetting.C_FIRST_FRIDAY_SES2) "14:00:00" :
	 * TradingSetting .getDateSession(TradingSetting.C_FIRST_SES2) "13:30:00 );
	 * // (friday // ? // "14:00:00" // : // "13:30:00"); String dtendses1 = dt
	 * + "-" + (friday ? TradingSetting
	 * .getDateSession(TradingSetting.C_END_FRIDAY_SES1) "11:30:00" :
	 * TradingSetting .getDateSession(TradingSetting.C_END_SES1) "12:00:00" );//
	 * (friday // ? String dtendses2 = dt + "-" + TradingSetting
	 * .getDateSession(TradingSetting.C_END_SES2); // "11:30:00" // : //
	 * "12:00:00"); String dtendpre = dt + "-" + TradingSetting
	 * .getDateSession(TradingSetting.C_END_PREOP); // "09:25:00";
	 * 
	 * String dtpost_first = dt + "-" + TradingSetting
	 * .getDateSession(TradingSetting.C_FIRST_POSTTRADING);
	 * 
	 * String dtpost_end = dt + "-" + TradingSetting
	 * .getDateSession(TradingSetting.C_END_POSTTRADING);
	 * 
	 * String preclosing_end = dt + "-" + TradingSetting
	 * .getDateSession(TradingSetting.C_END_PRECLOSING);
	 * 
	 * Date datepreopening = dateFormatAll.parse(dtop); Date dateses1 =
	 * dateFormatAll.parse(dtses1); Date dateses2 = dateFormatAll.parse(dtses2);
	 * Date enddateses1 = dateFormatAll.parse(dtendses1); Date enddateses2 =
	 * dateFormatAll.parse(dtendses2); Date endpre =
	 * dateFormatAll.parse(dtendpre); Date postfirst =
	 * dateFormatAll.parse(dtpost_first); Date postend =
	 * dateFormatAll.parse(dtpost_end); Date preclosing =
	 * dateFormatAll.parse(preclosing_end); Date datecurrent =
	 * OrderIDGenerator.getTime();//
	 * dateFormatAll.parse(dt+"-"+UIMaster.lblTime.getText().trim()); //
	 * log.info
	 * ("datecurrent "+datecurrent+" "+enddateses2+" "+postfirst+" "+brid
	 * +" "+());
	 * 
	 * if (datecurrent.getTime() < datepreopening.getTime()) {
	 * checkBasket.setSelected(true); fieldTimer .setText(stockpreopening ?
	 * TradingSetting .getDateSession(TradingSetting.C_FIRST_PREOP) :
	 * TradingSetting .getDateSession(TradingSetting.C_FIRST_SES1));
	 * comboSend.setSelectedIndex(1); } else if (datecurrent.getTime() <
	 * endpre.getTime()) { if (!stockpreopening) {
	 * checkBasket.setSelected(true); fieldTimer .setText(TradingSetting
	 * .getDateSession(TradingSetting.C_FIRST_SES1));
	 * comboSend.setSelectedIndex(1); }else{ checkBasket.setSelected(false);
	 * fieldTimer.setText(""); comboSend.setSelectedIndex(0); } } else if
	 * (datecurrent.getTime() > endpre.getTime() && datecurrent.getTime() <
	 * dateses1.getTime()) { checkBasket.setSelected(true);
	 * fieldTimer.setText(dtendses1); fieldTimer .setText(TradingSetting
	 * .getDateSession(TradingSetting.C_FIRST_SES1));
	 * comboSend.setSelectedIndex(1); } else if (datecurrent.getTime() >
	 * enddateses1 .getTime() && datecurrent.getTime() < dateses2.getTime()) {
	 * checkBasket.setSelected(true); fieldTimer .setText(friday ?
	 * TradingSetting .getDateSession(TradingSetting.C_FIRST_FRIDAY_SES2) :
	 * TradingSetting .getDateSession(TradingSetting.C_FIRST_SES2));
	 * comboSend.setSelectedIndex(1); } else if ((datecurrent.getTime() >
	 * preclosing .getTime() && datecurrent.getTime() < postfirst .getTime()) &&
	 * brid.equals("RG")) { checkBasket.setSelected(true); fieldTimer
	 * .setText(TradingSetting
	 * .getDateSession(TradingSetting.C_FIRST_POSTTRADING));
	 * comboSend.setSelectedIndex(1); } else { checkBasket.setSelected(false);
	 * comboSend.setSelectedIndex(0); }
	 * 
	 * } } else { SwingUtilities.invokeLater(new Runnable() { public void run()
	 * { comboStock.getTextEditor().requestFocus(); } }); }
	 * 
	 * comboStock.hidePopup(); changeMaxLot(); if
	 * (comboStock.getText().endsWith("-R")) { comboBoard.setSelectedItem("TN");
	 * comboType.setSelectedIndex(1); comboType.setEnabled(false);
	 * comboType.setFocusable(false); } else{ comboBoard.setSelectedItem("RG");
	 * comboType.setSelectedIndex(0); comboType.setEnabled(true);
	 * comboType.setFocusable(true); }
	 * 
	 * } changeQuote(comboStock.getText()); } catch (Exception ex) {
	 * ex.printStackTrace(); } }
	 */

	private void chgStockSummary() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				StockSummary summ = (StockSummary) ((IEQTradeApp) apps)
						.getFeedEngine()
						.getStore(FeedStore.DATA_STOCKSUMMARY)
						.getDataByKey(
								new Object[] { soldstock, comboBoard.getText() });
				if (summ != null) {
					fieldLast.setText(formatter.format(summ.getClosing()
							.doubleValue()));
					fieldChange.setText(getChange(summ.getClosing()
							.doubleValue(), summ.getPrevious().doubleValue()));
					fieldPrev.setText(formatter.format(summ.getPrevious()
							.doubleValue()));

					fieldOpen.setText(formatter.format(summ.getOpening()
							.doubleValue()));
					fieldHigh.setText(formatter.format(summ.getHighest()
							.doubleValue()));
					fieldLow.setText(formatter.format(summ.getLowest()
							.doubleValue()));

					fieldLast
							.setForeground(summ.getClosing().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getClosing().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldChange
							.setForeground(summ.getClosing().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getClosing().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldPrev.setForeground(FeedSetting
							.getColor(FeedSetting.C_ZERO));

					fieldOpen
							.setForeground(summ.getOpening().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getOpening().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldHigh
							.setForeground(summ.getHighest().doubleValue() > summ
									.getClosing().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getHighest().doubleValue() < summ
									.getClosing().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldLow.setForeground(summ.getLowest().doubleValue() > summ
							.getPrevious().doubleValue() ? FeedSetting
							.getColor(FeedSetting.C_PLUS)
							: (summ.getLowest().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
				} else {
					fieldOpen.setText("- ");
					fieldHigh.setText("- ");
					fieldLow.setText("- ");
					fieldLast.setText("- ");
					fieldChange.setText("- ");
					fieldPrev.setText("- ");
					fieldOpen.setText("- ");
					fieldHigh.setText("- ");
					fieldLow.setText("- ");
				}
			}
		});
	}

	private String getChange(double slastprice, double nprev) {
		String schange = " ";
		if (slastprice > 0) {
			double ntemp = slastprice - nprev;
			double ndbl = ((double) (ntemp * 100) / nprev);
			int ntmpdbl = (int) (ndbl * 100);
			double npersen = (double) ntmpdbl / 100;
			if (ntemp > 0) {
				schange = "+" + formatter.format(ntemp) + "("
						+ formatter2.format(npersen) + "%) ";
			} else if (ntemp == 0) {
				schange = "" + formatter.format(ntemp) + "(0.00%) ";
			} else {
				schange = "" + formatter.format(ntemp) + "("
						+ formatter2.format(npersen) + "%) ";
			}
		}
		return schange;
	}

	void changeMaxLot() {
		try {
			int i = ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_PORTFOLIO)
					.getIndexByKey(
							new String[] { getAccountId(comboClient.getText()),
									comboStock.getText() });
			if (i >= 0) {
				Portfolio pf = (Portfolio) ((IEQTradeApp) apps)
						.getTradingEngine()
						.getStore(TradingStore.DATA_PORTFOLIO)
						.getDataByIndex(i);
				if (pf != null) {
					double lotsize = 500;
					int lt = ((IEQTradeApp) apps)
							.getTradingEngine()
							.getStore(TradingStore.DATA_STOCK)
							.getIndexByKey(
									new String[] { comboStock.getText() });
					if (lt >= 0) {
						Stock stock = (Stock) ((IEQTradeApp) apps)
								.getTradingEngine()
								.getStore(TradingStore.DATA_STOCK)
								.getDataByIndex(lt);
						if (stock != null) {
							lotsize = (stock.getLotSize().doubleValue());
						}
					}
					fieldStockLimit.setValue(Math.floor(new Double(pf
							.getAvailableVolume().doubleValue() / lotsize)));
				}
			} else {
				fieldStockLimit.setValue(new Double(0));
			}
		} catch (Exception ex) {
		}
	}

	void changePF(String accountid) {
		((FilterColumn) filterPF.getFilteredData("account"))
				.setField(accountid);
		filterPF.fireFilterChanged();
	}

	private static SimpleDateFormat dateFormatAll = new SimpleDateFormat(
			"yyyyMMdd-HH:mm:ss");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat(
			"yyyyMMdd");

	/*
	 * void changeQuote(final String newstock) { SwingUtilities.invokeLater(new
	 * Runnable() { public void run() { String stock =
	 * newstock.trim().toUpperCase(); if
	 * (!soldstock.toUpperCase().trim().equals(stock)) { stock =
	 * stock.trim().toUpperCase(); if (!soldstock.equals("") &&
	 * !soldstock.equals(BQstock)) { unsubscribe(); } soldstock = stock;
	 * ((FilterColumn) filterBid.getFilteredData("stock")) .setField(stock);
	 * ((FilterColumn) filterBid.getFilteredData("bo")) .setField("B");
	 * ((FilterColumn) filterBid.getFilteredData("board"))
	 * .setField(comboBoard.getText()); ((FilterColumn)
	 * filterOffer.getFilteredData("stock")) .setField(stock); ((FilterColumn)
	 * filterOffer.getFilteredData("bo")) .setField("O"); ((FilterColumn)
	 * filterOffer.getFilteredData("board")) .setField(comboBoard.getText());
	 * filterBid.fireFilterChanged(); filterOffer.fireFilterChanged(); if
	 * (!soldstock.equals("")) subscribe(); chgStockSummary(); } } }); }
	 */

	protected class RowChanged implements TableModelListener {

		public void tableChanged(final TableModelEvent e) {
			double count = 0, ocount = 0, ovalue = 0, dcount = 0, dvalue = 0, tlot = 0, tvalue = 0;
			int size = table.getTable().getRowCount();

			for (int i = 0; i < size; i++) {
				Order order = (Order) table.getDataByIndex(i);
				count++;
				tlot = tlot + order.getLot().doubleValue();
				tvalue = tvalue
						+ (order.getVolume().doubleValue() * order.getPrice()
								.doubleValue());
				if (order.getStatus().equals(Order.C_PARTIAL_MATCH)
						|| order.getStatus().equals(Order.C_OPEN)) {
					ocount++;
					ovalue = ovalue
							+ (order.getBalVol().doubleValue() * order
									.getPrice().doubleValue());
				}
				if (order.getStatus().equals(Order.C_PARTIAL_MATCH)
						|| order.getStatus().equals(Order.C_FULL_MATCH)) {
					dcount++;
					dvalue = dvalue
							+ (order.getDoneVol().doubleValue() * order
									.getPrice().doubleValue());
				}
			}
			if (e.getType() == TableModelEvent.INSERT) {
				Rectangle r = table.getTable().getCellRect(size - 1, 0, true);
				table.scrollRectToVisible(r);
			}

			tCount.setValue(new Double(count));
			oCount.setValue(new Double(ocount));
			oValue.setValue(new Double(ovalue));
			dCount.setValue(new Double(dcount));
			dValue.setValue(new Double(dvalue));
			tLot.setValue(new Double(tlot));
			tValue.setValue(new Double(tvalue));

			if (e.getType() == TableModelEvent.INSERT) {
				Rectangle r = table.getTable().getCellRect(size - 1, 0, true);
				table.scrollRectToVisible(r);
			}

		}

		/*
		 * public void tableChanged(final TableModelEvent e) { double count = 0,
		 * ocount = 0, ovalue = 0, dcount = 0, dvalue = 0, tlot = 0, tvalue = 0;
		 * int size = tableOrder.getTable().getRowCount(); for (int i = 0; i <
		 * size; i++) { Order order = (Order) tableOrder.getDataByIndex(i);
		 * count++; tlot = tlot + order.getLot().doubleValue(); tvalue = tvalue
		 * + (order.getVolume().doubleValue() * order.getPrice()
		 * .doubleValue()); if (order.getStatus().equals(Order.C_PARTIAL_MATCH)
		 * || order.getStatus().equals(Order.C_OPEN)) { ocount++; ovalue =
		 * ovalue + (order.getBalVol().doubleValue() * order
		 * .getPrice().doubleValue()); } if
		 * (order.getStatus().equals(Order.C_PARTIAL_MATCH) ||
		 * order.getStatus().equals(Order.C_FULL_MATCH)) { dcount++; dvalue =
		 * dvalue + (order.getDoneVol().doubleValue() * order
		 * .getPrice().doubleValue()); } .setValue(new Double(count));
		 * oCount.setValue(new Double(ocount)); oValue.setValue(new
		 * Double(ovalue)); dCount.setValue(new Double(dcount));
		 * dValue.setValue(new Double(dvalue)); tLot.setValue(new Double(tlot));
		 * tValue.setValue(new Double(tvalue)); } }
		 */
	}

	 void unsubscribe() {
		if (!soldstock.equals("")) {
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(
					FeedParser.PARSER_QUOTE,
					FeedParser.PARSER_QUOTE + "|" + soldstock + "#"
							+ comboBoard.getText());
		}
	}

	void subscribe() {
		((IEQTradeApp) apps).getFeedEngine().subscribe(
				FeedParser.PARSER_QUOTE,
				FeedParser.PARSER_QUOTE + "|" + soldstock + "#"
						+ comboBoard.getText());
	}

	public void success() {

		if (isLoadingSuperUser) {

			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					setState(true);
					((FilterColumn) filterOrder.getFilteredData("account"))
							.setField(comboClient.getText().trim().equals("") ? null
									: comboClient.getText().trim()
											.toUpperCase());
					filterOrder.fireFilterChanged();
				}
			});
		}
	}
	
	@Override
	public void loadSetting() {
		// TODO Auto-generated method stub
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
//		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", GTCDef.getTableDef());
//		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 800, 300));
		if (hSetting.get("tablesumm") == null) {
			hSetting.put("tablesumm", GTCDef.getTableDef());
		}
	}

	@Override
	public void saveSetting() {
		// TODO Auto-generated method stub
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}
}
