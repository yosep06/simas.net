package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.AccountDef;
import eqtrade.trading.model.MarginDef;

import java.io.*;
import java.text.*;
import eqtrade.trading.ui.accId;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.*;

public class UIMarginCall extends UI {
	private JSkinDlg frame ;
	private JGrid table;
	private JButton btnRefresh;
	private JButton btnOk;
	private FilterMarginAccount filterByRatio;
	protected final Log log = LogFactory.getLog("UImarginCall");
	protected final Log logCek = LogFactory.getLog("UImarginCek");
	private HashMap<String, Vector<String>> hashLog = new HashMap<String, Vector<String>>();
	private SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private Date today = new Date();
	private HashMap<String, Vector<String>> copyLog =new HashMap<String, Vector<String>>();
	public String[][] arrCoba =null;
	public Vector<String> coba = new Vector<String>() ;
	Vector<String> data ;
	
	public UIMarginCall(String app) {
		super("Margin Call", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_MARGINCALL);
		
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}
	
	
	@Override
	protected void build() {
		filterByRatio = new FilterMarginAccount(pnlContent, ((IEQTradeApp) apps)
		.getTradingEngine().getStore(TradingStore.DATA_MARGIN));
		
		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_MARGIN), filterByRatio,
				(Hashtable) hSetting.get("table"));
		/*table.setColumnHide(new int[]{Account.C_ACCTYPE,Account.C_ADDRESS,Account.C_BID,Account.C_HANDPHONE,
				Account.C_BRANCHID,Account.C_BUY,Account.C_COMPLIANCEID,Account.C_CREDITLIMIT,
				Account.C_CURRTL,Account.C_CUSTID,Account.C_CUSTTYPE,Account.C_DEPOSIT,Account.C_EMAIL,
				Account.C_FORCESELL,Account.C_INVTYPE,Account.C_ISCORPORATE,Account.C_LQVALUE,Account.C_MARKETVAL,
				Account.C_MYACC,Account.C_NETAC,Account.C_OFFER,Account.C_PHONE,Account.C_SALESID,Account.C_SELL,
				Account.C_STATUS,Account.C_STOCKVAL,Account.C_SUBACCOUNT,Account.C_TOPUP,Account.C_TRADINGID,Account.C_WITHDRAW});
		*/
		//tableHistory = createTable(data, null, (Hashtable) hSetting.get("table"));
		
		btnRefresh = new JButton("Refresh");
		btnRefresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {				
			//	log();	
								
			}
		});
		btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
					close();
					if(filterByRatio.rowcount>0){
						//tampil margin force;
						apps.getUI().showUI(TradingUI.UI_FORCESELL);
					}
			}
		});
		//table.getTable().add(popupMenu);
		table.getTable().addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if(!e.isPopupTrigger() && e.getClickCount() == 1){
					int i = table.getMappedRow(table.getSelectedRow());
					//TradingStore order = (TradingStore)
				}
			}
		});
		registerEvent(btnOk);
		table.addMouseListener(new MyCustomMouseAdapter());
		table.setColumnHide(AccountDef.columhide);

		JPanel pnlTop = new JPanel();

		FormLayout l = new FormLayout(
				"2dlu,50px,2dlu,80px,2dlu,80px,2dlu,80px", "2dlu,pref,2dlu");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlTop);
		//b.add(btnRefresh, c.xy(4, 2));
		b.add(btnOk,c.xy(4, 2));
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlTop, BorderLayout.SOUTH);
		pnlContent.add(table, BorderLayout.CENTER);
		((FilterColumn) filterByRatio.getFilteredData("margin")).setField(new Double(64.5));
		//((FilterColumn) filterByRatio.getFilteredData("acctype")).setField("MARGIN");
		filterByRatio.fireFilterChanged();
	/*	if(readFile("data/layout/MarginLog.dat") !=null)log();
		else writeLog();*/
		refresh();
	}
	
	private void registerEvent(JComponent comm) {
		InputMap inputMap = comm
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		comm.getActionMap().put("escapeAction",
		new AbstractAction("escapeAction") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent evt) {
				btnOk.doClick();
			}
		});
		
	}

	protected void refreshAcc() {
		double ratio = 64.5;//System.out.println("margin call");
		((FilterColumn) filterByRatio.getFilteredData("margin")).setField(ratio);
		filterByRatio.fireFilterChanged();
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", MarginDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 600, 300));
	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(frame.getX(), frame.getY(), frame.getWidth(), frame
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		hSetting.put("arrlog",hashLog);
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}
	@Override
	protected void initUI() {
		/*super.initUI();
		frame.setResizable(false);
		frame.getBtnClose().setVisible(false);
		frame.getBtnMin().setVisible(false);*/
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setResizable(false);
		frame.getBtnClose().setVisible(false);
		frame.getBtnMax().setVisible(false);
		frame.setContent(pnlContent);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});frame.setSize(400,400);
	}
	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}
	@Override
	public void show() {
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
		
	}
	@Override
	public void show(Object param) {
		show();
	}
	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
	}
}
