package eqtrade.trading.ui;

import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.table.TableModel;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.trading.model.Account;
import eqtrade.trading.model.Portfolio;

public class FilterMarginMonitor extends FilterBase {
	private HashMap mapFilter = new HashMap();
	private GridModel dataPortfolio;
	private Log log = LogFactory.getLog(getClass());

	public FilterMarginMonitor(Component parent, GridModel _dataPortfolio) {
		super(parent,"filter");
		mapFilter.put("margin", new FilterColumn("margin",Vector.class,
				null,FilterColumn.C_MEMBEROF));
		this.dataPortfolio = _dataPortfolio;
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try{
			Column val = (Column) model.getValueAt(row, 0);
			Account dat = (Account) val.getSource();
			if(((FilterColumn)mapFilter.get("margin") ).compare(dat.getAccId()))
				{avail = true;}
			return avail;
		}catch(Exception e){
			e.printStackTrace();return false;
		}
		
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	public void setFilter(HashMap map) {
		if(map!=null){
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

}
