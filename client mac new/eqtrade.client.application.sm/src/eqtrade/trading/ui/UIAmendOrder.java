package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumnModel;
import javax.swing.text.JTextComponent;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.border.LineBorder;
import com.vollux.framework.Action;
import com.vollux.framework.UI;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JNumber;
import com.vollux.ui.JSkinPnl;
import com.vollux.ui.JText;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.engine.FeedParser;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Quote;
import eqtrade.feed.model.QuoteDef;
import eqtrade.feed.model.StockSummary;
import eqtrade.feed.ui.quote.FilterQuote;
import eqtrade.trading.app.TradingAction;
import eqtrade.trading.app.TradingApplication;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.core.Utils;
import eqtrade.trading.engine.OrderIDGenerator;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.Account;
import eqtrade.trading.model.Order;
import eqtrade.trading.model.Portfolio;
import eqtrade.trading.model.PortfolioDef;
import eqtrade.trading.model.Stock;

public class UIAmendOrder extends UI {
	JText fieldStock = null;
	JText fieldStockName;
	JText fieldClient = null;
	JSpinner fieldNewPrice;
	IDXPriceSpinnerModel priceModel;
	JSpinner fieldNewLot;
	SpinnerModel lotModel;
	JNumber fieldNewValue;
	JText fieldClientName;
	JNumber fieldTradingLimit;
	JNumber fieldStockLimit;
	JText fieldBoard = null;
	JText fieldType = null;
	JNumber fieldOldPrice;
	JNumber fieldOldLot;
	JNumber fieldOldValue;
	JNumber fieldOldShare;
	JNumber fieldDone;
	JNumber fieldDoneShare;
	JNumber fieldNewShare;
	JDropDown comboNewClient;
	JDropDown fieldNewClientName;

	JCheckBox checkConfirm;
	JText fieldIA;
	JButton btnSend;
	JButton btnCancel;
	JLabel labelTA;
	JLabel labelOverlimit;

	private JGrid tableBid;
	private JGrid tableOffer;
	private JLabel fieldPrev;
	private JLabel fieldHigh;
	private JLabel fieldLow;
	private JLabel fieldChange;
	private JLabel fieldOpen;
	private JLabel fieldLast;
	private JPanel pnlInfo;

	private JPanel headerEntry;
	private JLabel titleEntry;
	private JPanel pnlEntry;

	private String entryType = "BUY";

	private FilterQuote filterBid;
	private FilterQuote filterOffer;
	private static NumberFormat formatter = new DecimalFormat("#,##0  ");
	private static NumberFormat formatter2 = new DecimalFormat("#,##0.00");
	private Action sendAction = null;
	private String soldstock = "";

	private JGrid tablePF;
	private FilterPortfolio filterPF;
	
	double lotsize = Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));

	public UIAmendOrder(String app) {
		super("Amend Order", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_AMENDORDER);
	}

	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
			}
		});
	}

	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				tablePF.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	private JSkinPnl buildPF() {
		createPopup();
		filterPF = new FilterPortfolio(pnlContent);
		tablePF = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_PORTFOLIO), filterPF,
				(Hashtable) hSetting.get("table"));
		tablePF.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		tablePF.setColumnHide(PortfolioDef.columnhide2);

		JSkinPnl temp = new JSkinPnl(new BorderLayout());
		temp.add(tablePF, BorderLayout.CENTER);
		temp.setPreferredSize(new Dimension(300, 200));
		registerEvent(tablePF);
		registerEvent(tablePF.getTable());
		return temp;
	}

	private JSkinPnl buildQuote() {
		filterBid = new FilterQuote(pnlContent);
		filterOffer = new FilterQuote(pnlContent);
		tableOffer = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_QUOTE), filterOffer,
				(Hashtable) hSetting.get("tableoff"));
		tableBid = createTable(
				((IEQTradeApp) apps).getFeedEngine().getStore(
						FeedStore.DATA_QUOTE), filterBid,
				(Hashtable) hSetting.get("tablebid"));
		tableBid.setPreferredSize(new Dimension(120, 50));
		tableOffer.setPreferredSize(new Dimension(120, 50));
		tableBid.setMinimumSize(new Dimension(120, 50));
		tableOffer.setMinimumSize(new Dimension(120, 50));
		tableBid.getTable().getTableHeader().setReorderingAllowed(false);
		tableOffer.getTable().getTableHeader().setReorderingAllowed(false);
		tableBid.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableBid.setColumnHide(new int[] { Quote.CIDX_NUMBER, Quote.CIDX_PREV,
				Quote.CIDX_TYPE, Quote.CIDX_STOCK });
		tableOffer.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		tableOffer.setColumnHide(new int[] { Quote.CIDX_NUMBER,
				Quote.CIDX_PREV, Quote.CIDX_TYPE, Quote.CIDX_STOCK });
		tableBid.getTable().getModel()
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						chgStockSummary();
					}
				});

		tableOffer.getTable().getModel()
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						chgStockSummary();
					}
				});
		((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCKSUMMARY)
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						chgStockSummary();
					}
				});
		((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.addTableModelListener(new TableModelListener() {
					public void tableChanged(TableModelEvent e) {
						changeTL();
					}
				});
		fieldPrev = new JLabel(" ", JLabel.RIGHT);
		fieldHigh = new JLabel(" ", JLabel.RIGHT);
		fieldLow = new JLabel(" ", JLabel.RIGHT);
		fieldChange = new JLabel(" ", JLabel.RIGHT);
		fieldOpen = new JLabel(" ", JLabel.RIGHT);
		fieldLast = new JLabel(" ", JLabel.RIGHT);

		pnlInfo = new JPanel();
		FormLayout l = new FormLayout(
				"50px,2dlu,pref:grow, 2dlu,pref,2dlu,pref:grow",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlInfo);
		pnlInfo.setBorder(new EmptyBorder(5, 5, 5, 5));
		b.add(new JLabel("Last"), c.xy(1, 3));
		b.add(fieldLast, c.xy(3, 3));
		b.add(new JLabel("Hi"), c.xy(1, 5));
		b.add(fieldHigh, c.xy(3, 5));
		b.add(new JLabel("Lo"), c.xy(1, 7));
		b.add(fieldLow, c.xy(3, 7));
		b.add(new JLabel("Open"), c.xy(5, 3));
		b.add(fieldOpen, c.xy(7, 3));
		b.add(new JLabel("Close"), c.xy(5, 5));
		b.add(fieldPrev, c.xy(7, 5));
		b.add(new JLabel("Chg"), c.xy(5, 7));
		b.add(fieldChange, c.xy(7, 7));

		FormLayout layoutInfo = new FormLayout("pref:grow",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		PanelBuilder builderInfo = new PanelBuilder(layoutInfo);
		builderInfo.setBorder(new CompoundBorder(new LineBorder(),
				new EmptyBorder(4, 4, 4, 4)));
		builderInfo.add(labelTA, c.xy(1, 1));
		builderInfo.add(fieldTradingLimit, c.xy(1, 3));
		builderInfo.add(fieldStockLimit, c.xy(1, 5));
		builderInfo.add(labelOverlimit, c.xy(1, 7));
		builderInfo.getPanel().setOpaque(false);

		FormLayout layout2 = new FormLayout("150px:grow, 1dlu, 150px:grow",
				"pref, 2dlu, pref, 2dlu, fill:200px:grow");
		PanelBuilder builder2 = new PanelBuilder(layout2);
		builder2.add(builderInfo.getPanel(), c.xyw(1, 1, 3));
		builder2.add(pnlInfo, c.xyw(1, 3, 3));
		builder2.add(tableBid, c.xy(1, 5));
		builder2.add(tableOffer, c.xy(3, 5));

		JSkinPnl temp = new JSkinPnl(new BorderLayout());
		temp.setBorder(new EmptyBorder(2, 2, 2, 2));
		temp.add(builder2.getPanel(), BorderLayout.CENTER);
		temp.add(buildPF(), BorderLayout.EAST);
		registerEvent(tableBid);
		registerEvent(tableBid.getTable());
		registerEvent(tableOffer);
		registerEvent(tableOffer.getTable());
		return temp;
	}

	protected void build() {
		sendAction = apps.getAction().get(TradingAction.A_SHOWAMENDORDER);
		fieldTradingLimit = new JNumber(Double.class, 0, 0, true, false);
		fieldTradingLimit.setHorizontalAlignment(JLabel.CENTER);
		fieldStockLimit = new JNumber(Double.class, 0, 0, true, false);
		fieldStockLimit.setHorizontalAlignment(JLabel.CENTER);
		fieldClient = new JText(false);
		fieldStock = new JText(false);
		fieldBoard = new JText(false);
		fieldType = new JText(false);
		fieldClientName = new JText(false);
		fieldStockName = new JText(false);
		//fieldNewPrice = new JNumber(Double.class, 0, 0, true);
		//fieldNewLot = new JNumber(Double.class, 0, 0, true);
		IDXPriceSpinnerModel priceModel = new IDXPriceSpinnerModel();
		fieldNewPrice = new JSpinner(priceModel);
		SpinnerModel lotModel = new SpinnerNumberModel(new Double(0), new Double(0), new Double(1000000), new Double(1));
		fieldNewLot = new JSpinner(lotModel);
		fieldNewValue = new JNumber(Double.class, 0, 0, true, false);
		fieldOldPrice = new JNumber(Double.class, 0, 0, true, false);
		fieldOldLot = new JNumber(Double.class, 0, 0, true, false);
		fieldOldValue = new JNumber(Double.class, 0, 0, true, false);
		fieldOldShare = new JNumber(Double.class, 0, 0, true, false);
		fieldDone = new JNumber(Double.class, 0, 0, true, false);
		fieldDoneShare = new JNumber(Double.class, 0, 0, true, false);
		fieldNewShare = new JNumber(Double.class, 0, 0, true);
		comboNewClient = new JDropDown(((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT), Account.C_TRADINGID, true);
		// fieldNewClientName = new JText(false);
		fieldNewClientName = new JDropDown(((IEQTradeApp) apps)
				.getTradingEngine().getStore(TradingStore.DATA_ACCOUNT),
				Account.C_NAME, true);
		checkConfirm = new JCheckBox("Confirm");
		checkConfirm.setOpaque(false);
		fieldIA = new JText(false);
		labelTA = new JLabel("Trading Limit");
		labelTA.setHorizontalAlignment(JLabel.CENTER);
		labelOverlimit = new JLabel("");
		labelOverlimit.setHorizontalAlignment(JLabel.CENTER);

		btnSend = new JButton("Send");
		btnSend.setMnemonic('S');
		btnCancel = new JButton("Cancel");
		btnCancel.setMnemonic('C');

		registerEvent(fieldNewPrice);
		registerEvent(((JSpinner.DefaultEditor)fieldNewPrice.getEditor()).getTextField());
		registerEvent(fieldNewLot);
		registerEvent(((JSpinner.DefaultEditor)fieldNewLot.getEditor()).getTextField());
		registerEvent(fieldNewShare);
		registerEvent(comboNewClient.getTextEditor());
		registerEventCombo(comboNewClient);
		registerEvent(fieldNewClientName.getTextEditor());
		registerEventCombo(fieldNewClientName);
		registerEvent(btnSend);
		registerEvent(btnCancel);
		registerEvent(checkConfirm);

		/*
		fieldNewLot.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				if (fieldNewLot.isEditable())
					calculate();
			}
		});
		*/
		fieldNewLot.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				calculate();
			}
			public void focusGained(FocusEvent arg0) {
				((JSpinner)arg0.getSource()).transferFocus();
			}
		});
		fieldNewLot.setFocusable(false);
		((JSpinner.DefaultEditor)fieldNewLot.getEditor ()).getTextField().addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextComponent) {
					final JTextComponent textComponent=((JTextComponent)e.getSource());
					SwingUtilities.invokeLater(new Runnable(){
						public void run() {
							textComponent.selectAll();
						}
					});
				}
			}
			public void focusLost(FocusEvent arg0) {
				if ((((JSpinner.DefaultEditor)fieldNewLot.getEditor()).getTextField()).isEditable())
					calculate();
			}
		});

		fieldNewShare.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				if (fieldNewShare.isEditable())
					calculateLot();
			}
		});

		/*
		fieldNewPrice.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				calculate();
			}
		});
		*/
		
		fieldNewPrice.addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				calculate();
			}
			public void focusGained(FocusEvent arg0) {
				((JSpinner)arg0.getSource()).transferFocus();
			}
		});
		fieldNewPrice.setFocusable(false);
		((JSpinner.DefaultEditor)fieldNewPrice.getEditor ()).getTextField().addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent e) {
				if (e.getSource() instanceof JTextComponent) {
					final JTextComponent textComponent=((JTextComponent)e.getSource());
					SwingUtilities.invokeLater(new Runnable(){
						public void run() {
							textComponent.selectAll();
						}
					});
				}
			}
			public void focusLost(FocusEvent arg0) {
				if ((((JSpinner.DefaultEditor)fieldNewPrice.getEditor()).getTextField()).isEditable())
					calculate();
			}
		});

		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveSetting();
				send();
			}
		});

		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clearForm();
				hide();
			}
		});

		comboNewClient.getTextEditor().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				clientfocusLost(arg0);
			}

			public void focusGained(FocusEvent e) {
				try {
					if (((IEQTradeApp) apps).getTradingEngine()
							.getStore(TradingStore.DATA_ACCOUNT).getRowCount() == 1) {
						comboNewClient.setSelectedIndex(0);
					} else {
						comboNewClient.showPopup();
					}
				} catch (Exception ex) {
				}
			}
		});

		fieldNewClientName.getTextEditor().addFocusListener(new FocusAdapter() {
			public void focusLost(FocusEvent arg0) {
				clientfocusLost(arg0);
			}

			public void focusGained(FocusEvent e) {
				try {
					if (((IEQTradeApp) apps).getTradingEngine()
							.getStore(TradingStore.DATA_ACCOUNT).getRowCount() == 1) {
						fieldNewClientName.setSelectedIndex(0);
					} else {
						fieldNewClientName.showPopup();
					}
				} catch (Exception ex) {
				}
			}
		});

		headerEntry = new JPanel(new BorderLayout());
		headerEntry.setBackground(entryType.equals("BUY") ? Color.red.darker()
				: Color.green.darker());
		titleEntry = new JLabel("AMEND " + entryType);
		titleEntry.setHorizontalAlignment(JLabel.CENTER);
		titleEntry.setFont(new Font("dialog", Font.BOLD, 20));
		headerEntry.setBorder(new EmptyBorder(10, 10, 10, 10));
		headerEntry.add(titleEntry, BorderLayout.WEST);
		headerEntry.setPreferredSize(new Dimension(100, 60));

		pnlEntry = new JPanel();
		FormLayout lay = new FormLayout(
				"pref, 2dlu, 75px, 2dlu, pref, 2dlu, 75px, 2dlu, pref",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref,2dlu,pref");
		PanelBuilder builder = new PanelBuilder(lay, pnlEntry);
		builder.setBorder(new EmptyBorder(4, 4, 4, 4));
		CellConstraints cc = new CellConstraints();
		builder.add(headerEntry, cc.xyw(1, 1, 9));
		builder.add(new JLabel("Board"), cc.xy(1, 3));
		builder.add(fieldBoard, cc.xy(3, 3));
		builder.add(new JLabel("Stock"), cc.xy(1, 5));
		builder.add(fieldStock, cc.xy(3, 5));
		builder.add(fieldStockName, cc.xyw(5, 5, 5));
		builder.add(new JLabel("Price"), cc.xy(1, 7));
		builder.add(fieldOldPrice, cc.xy(3, 7));
		builder.add(new JLabel("Quantity"), cc.xy(1, 9));
		builder.add(fieldOldLot, cc.xy(3, 9));
		builder.add(new JLabel("Lot(s)"), cc.xy(5, 9));
		builder.add(fieldOldShare, cc.xy(7, 9));
		builder.add(new JLabel("Share(s)"), cc.xy(9, 9));
		builder.add(new JLabel("Done"), cc.xy(1, 11));
		builder.add(fieldDone, cc.xy(3, 11));
		builder.add(new JLabel("Lot(s)"), cc.xy(5, 11));
		builder.add(fieldDoneShare, cc.xy(7, 11));
		builder.add(new JLabel("Share(s)"), cc.xy(9, 11));
		builder.add(new JLabel("Client"), cc.xy(1, 13));
		builder.add(fieldClient, cc.xy(3, 13));
		builder.add(fieldClientName, cc.xyw(5, 13, 5));
		builder.add(new JLabel("Trading Acc"), cc.xy(1, 15));
		builder.add(fieldIA, cc.xy(3, 15));
		builder.add(new JLabel("Expire"), cc.xy(5, 15));
		builder.add(fieldType, cc.xy(7, 15));
		builder.add(new JLabel("Amount"), cc.xy(1, 17));
		builder.add(fieldOldValue, cc.xyw(3, 17, 7));
		builder.addSeparator("New Value", cc.xyw(1, 19, 9));
		builder.add(new JLabel("Price"), cc.xy(1, 21));
		builder.add(fieldNewPrice, cc.xy(3, 21));
		builder.add(new JLabel("Amount"), cc.xy(5, 21));
		builder.add(fieldNewValue, cc.xyw(7, 21, 3));
		builder.add(new JLabel("Quantity"), cc.xy(1, 23));
		builder.add(fieldNewLot, cc.xy(3, 23));
		builder.add(new JLabel("Lot(s)"), cc.xy(5, 23));
		builder.add(fieldNewShare, cc.xy(7, 23));
		builder.add(new JLabel("Share(s)"), cc.xy(9, 23));
		builder.add(new JLabel("Client"), cc.xy(1, 25));
		builder.add(comboNewClient, cc.xy(3, 25));
		builder.add(fieldNewClientName, cc.xyw(5, 25, 5));

		JSkinPnl temp3 = new JSkinPnl();
		FormLayout lay3 = new FormLayout(
				"pref, 2dlu, pref:grow, 2dlu, pref, 2dlu, pref", "pref");
		PanelBuilder builder3 = new PanelBuilder(lay3, temp3);
		builder3.add(checkConfirm, cc.xy(1, 1));
		builder3.add(btnSend, cc.xy(5, 1));
		builder3.add(btnCancel, cc.xy(7, 1));
		builder.add(temp3, cc.xywh(1, 27, 9, 1));

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(10, 10, 10, 10));
		pnlContent.add(pnlEntry, BorderLayout.CENTER);
		pnlContent.add(buildQuote(), BorderLayout.EAST);
		clearForm();
		refresh();
	}

	void changeTL() {
		String acc = getAccountId(fieldClient.getText().toUpperCase().trim());
		if (!acc.equals("")) {
			Account ac = (Account) ((IEQTradeApp) apps).getTradingEngine()
					.getStore(TradingStore.DATA_ACCOUNT)
					.getDataByKey(new String[] { acc });
			if (ac != null) {
				fieldTradingLimit.setValue(ac.getCurrTL());
			}
		}
	}

	private void registerEventCombo(JDropDown comp) {
		comp.getEditor().getEditorComponent()
				.setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(
				JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap()
				.put("enterAction", new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource()
								.equals(comboNewClient.getTextEditor())) {
							comboNewClient.setSelectedIndex(comboNewClient
									.getSelectedIndex());
							comboNewClient.hidePopup();
							fieldNewClientName.requestFocus();
						} else if (e.getSource().equals(
								fieldNewClientName.getTextEditor())) {
							comboNewClient.setSelectedIndex(fieldNewClientName
									.getSelectedIndex());
							fieldNewClientName.hidePopup();
							checkConfirm.requestFocus();
						}

						// comboStock.requestFocus();

					}

				});

		comp.getTextEditor().getActionMap()
				.put("tabAction", new AbstractAction("tabAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource()
								.equals(comboNewClient.getTextEditor())) {
							comboNewClient.setSelectedIndex(comboNewClient
									.getSelectedIndex());
							comboNewClient.hidePopup();
							fieldNewClientName.requestFocus();
						} else if (e.getSource().equals(
								fieldNewClientName.getTextEditor())) {
							comboNewClient.setSelectedIndex(fieldNewClientName
									.getSelectedIndex());
							fieldNewClientName.hidePopup();
							checkConfirm.requestFocus();
						}
					}

				});

	}

	void clientfocusLost(FocusEvent e) {
		try {
			comboNewClient.getTextEditor().setText(
					comboNewClient.getText().toUpperCase());
			Account acc = null;
			int i = ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ACCOUNT)
					.getIndexByKey(
							new String[] { getAccountId(comboNewClient
									.getText()) });
			if (i >= 0) {
				acc = (Account) ((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_ACCOUNT).getDataByIndex(i);
				if (acc != null) {
					// fieldNewClientName.setText(acc.getName());
					fieldNewClientName.setSelectedItem(acc.getName());
					fieldTradingLimit.setValue(acc.getCurrTL());
				}
			} else {
				// fieldNewClientName.setText("");
				fieldNewClientName.getTextEditor().setText("");
				fieldTradingLimit.setValue(null);
			}
			changeMaxLot();
			changePF(acc == null ? "" : acc.getTradingId());
			calculate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("tablebid", QuoteDef.getTableBidDef());
			hSetting.put("tableoff", QuoteDef.getTableOffDef());
			hSetting.put("table", PortfolioDef.getTableDef());
			hSetting.put("typeEntry", "BUY");
			hSetting.put("confirm", new Boolean(true));
		}
		entryType = (String) hSetting.get("typeEntry");
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 650, 500));
	}

	private Hashtable getCustomProperties() {
		Hashtable h = new Hashtable();
		TableColumnModel mod = tableOffer.getTable().getColumnModel();
		int[] width = QuoteDef.defaultColumnWidthOff;
		for (int i = 0; i < mod.getColumnCount(); i++) {
			String str = mod.getColumn(i).getHeaderValue().toString();
			int length = (mod.getColumn(i).getPreferredWidth() == 0) ? 0 : mod
					.getColumn(i).getWidth();
			if (str.equals("Offer")) {
				width[4] = length;
			} else if (str.equals("#O")) {
				width[6] = length;
			} else if (str.equals("Lot")) {
				width[5] = length;
			}
		}
		h.put("width", width);
		h.put("order", QuoteDef.defaultColumnOrderOff);
		h.put("font", tableOffer.getTableFont());
		h.put("header", QuoteDef.dataHeaderOff);
		h.put("alignment", QuoteDef.defaultHeaderAlignment);
		h.put("sorted", QuoteDef.columnsort);
		h.put("sortcolumn", new Integer(Quote.CIDX_NUMBER));
		h.put("sortascending", new Boolean(false));

		return h;
	}

	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("typeEntry", entryType);
		hSetting.put("table", tablePF.getTableProperties());
		hSetting.put("tablebid", tableBid.getTableProperties());
		hSetting.put("tableoff", getCustomProperties()); // tableOffer.getTableProperties());
		hSetting.put("confirm", new Boolean(checkConfirm.isSelected()));
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	public void refresh() {
		tableBid.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tableOffer.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		pnlInfo.setBackground(TradingSetting
				.getColor(TradingSetting.C_BACKGROUND));
		tableBid.setNewFont(TradingSetting.getFont());
		tableOffer.setNewFont(TradingSetting.getFont());
		tablePF.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		tablePF.setNewFont(TradingSetting.getFont());
		chgStockSummary();
	}

	protected void initUI() {
		super.initUI();
		form.setResizable(false);
		form.getBtnClose().setVisible(false);
		form.pack();
		// form.getBtnMin().setVisible(false);
	}

	private Order order;

	public void show(Object param) {
		HashMap p = (HashMap) param;
		order = (Order) p.get("ORDER");
		entryType = order.getBOS().equals(Order.C_BUY) ? "BUY" : "SELL";
		form.setTitle("AMEND " + entryType);
		titleEntry.setText("AMEND " + entryType);
		headerEntry.setBackground(entryType.equals("BUY") ? Color.red.darker()
				: Color.green.darker());
		pnlEntry.setBackground(headerEntry.getBackground());
		fillForm();
		show();
	}

	public void show() {
		if (!apps.getAction().get(TradingAction.A_SHOWAMENDORDER).isGranted())
			return;
		super.show();
		((JSpinner.DefaultEditor)fieldNewPrice.getEditor()).getTextField().requestFocus();
		getTime();
	}

	private void fillForm() {
		checkConfirm
				.setSelected(hSetting.get("confirm") != null ? (((Boolean) hSetting
						.get("confirm")).booleanValue()) : true);
		fieldStock.setText(order.getStock());
		fieldClient.setText(order.getTradingId());
		fieldOldPrice.setValue(order.getPrice());
		fieldOldLot.setValue(order.getLot());
		fieldOldValue.setValue(new Double(order.getVolume().doubleValue()
				* order.getPrice().doubleValue()));
		fieldNewPrice.setValue(order.getPrice());
		fieldNewLot.setValue(order.getLot());
		fieldBoard.setText(order.getBoard());
		fieldType.setText(order.getOrderType());
		fieldOldShare.setValue(order.getVolume());
		fieldDone.setValue(order.getDoneLot());
		fieldDoneShare.setValue(order.getDoneVol());
		fieldIA.setText(order.getInvType());
		fieldNewShare.setValue(order.getVolume());
		comboNewClient.getTextEditor().setText(order.getTradingId());
		if (entryType.equals("BUY")) {
			labelTA.setText("Trading Limit");
			fieldTradingLimit.setVisible(true);
			fieldStockLimit.setVisible(false);
		} else {
			labelTA.setText("Stock Balance");
			fieldTradingLimit.setVisible(false);
			fieldStockLimit.setVisible(true);
		}
		if (order.getBoard().equals("NG")) {
			//fieldNewLot.setEditable(false);
			(((JSpinner.DefaultEditor)fieldNewLot.getEditor()).getTextField()).setEditable(false);
			fieldNewLot.setEnabled(false);
			fieldNewShare.setEditable(true);
			fieldNewShare.setEnabled(true);
		} else {
			//fieldNewLot.setEditable(true);
			(((JSpinner.DefaultEditor)fieldNewLot.getEditor()).getTextField()).setEditable(true);
			fieldNewLot.setEnabled(true);
			fieldNewShare.setEditable(false);
			fieldNewShare.setEnabled(false);
		}
		clientfocusLost(null);
		Account acc = null;
		int i = ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getIndexByKey(
						new String[] { getAccountId(fieldClient.getText()) });
		if (i >= 0) {
			acc = (Account) ((IEQTradeApp) apps).getTradingEngine()
					.getStore(TradingStore.DATA_ACCOUNT).getDataByIndex(i);
			if (acc != null) {
				fieldClientName.setText(acc.getName());
				fieldTradingLimit.setValue(acc.getCurrTL());
			}
		} else {
			fieldClientName.setText("");
			fieldTradingLimit.setValue(null);
		}
		changePF(acc == null ? "" : acc.getTradingId());
		changeMaxLot();
		i = ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_STOCK)
				.getIndexByKey(new String[] { fieldStock.getText() });
		if (i >= 0) {
			Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine()
					.getStore(TradingStore.DATA_STOCK).getDataByIndex(i);
			if (stock != null) {
				fieldStockName.setText(stock.getName());
			}
		} else {
			fieldStockName.setText("");
		}
		calculate();
		soldstock = fieldStock.getText();
		subscribe();
		((FilterColumn) filterBid.getFilteredData("stock")).setField(order
				.getStock());
		((FilterColumn) filterBid.getFilteredData("bo")).setField("B");
		((FilterColumn) filterOffer.getFilteredData("stock")).setField(order
				.getStock());
		((FilterColumn) filterOffer.getFilteredData("bo")).setField("O");
		filterBid.fireFilterChanged();
		filterOffer.fireFilterChanged();
		chgStockSummary();
	}

	void changeMaxLot() {
		try {
			int i = ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_PORTFOLIO)
					.getIndexByKey(
							new String[] {
									getAccountId(comboNewClient.getText()),
									fieldStock.getText() });
			if (i >= 0) {
				Portfolio pf = (Portfolio) ((IEQTradeApp) apps)
						.getTradingEngine()
						.getStore(TradingStore.DATA_PORTFOLIO)
						.getDataByIndex(i);
				if (pf != null) {
					
					int lt = ((IEQTradeApp) apps)
							.getTradingEngine()
							.getStore(TradingStore.DATA_STOCK)
							.getIndexByKey(
									new String[] { fieldStock.getText() });
					if (lt >= 0) {
						Stock stock = (Stock) ((IEQTradeApp) apps)
								.getTradingEngine()
								.getStore(TradingStore.DATA_STOCK)
								.getDataByIndex(lt);
						if (stock != null) {
							lotsize = (stock.getLotSize().doubleValue());
						}
					}
					fieldStockLimit.setValue(new Double(pf.getAvailableVolume()
							.doubleValue() / lotsize));
				}
			} else {
				fieldStockLimit.setValue(new Double(0));
			}
		} catch (Exception ex) {
		}
	}

	private void chgStockSummary() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				StockSummary summ = (StockSummary) ((IEQTradeApp) apps)
						.getFeedEngine()
						.getStore(FeedStore.DATA_STOCKSUMMARY)
						.getDataByKey(
								new Object[] { soldstock, fieldBoard.getText() });
				if (summ != null) {
					fieldLast.setText(formatter.format(summ.getClosing()
							.doubleValue()));
					fieldChange.setText(getChange(summ.getClosing()
							.doubleValue(), summ.getPrevious().doubleValue()));
					fieldPrev.setText(formatter.format(summ.getPrevious()
							.doubleValue()));

					fieldOpen.setText(formatter.format(summ.getOpening()
							.doubleValue()));
					fieldHigh.setText(formatter.format(summ.getHighest()
							.doubleValue()));
					fieldLow.setText(formatter.format(summ.getLowest()
							.doubleValue()));

					fieldLast
							.setForeground(summ.getClosing().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getClosing().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldChange
							.setForeground(summ.getClosing().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getClosing().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldPrev.setForeground(FeedSetting
							.getColor(FeedSetting.C_ZERO));

					fieldOpen
							.setForeground(summ.getOpening().doubleValue() > summ
									.getPrevious().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getOpening().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldHigh
							.setForeground(summ.getHighest().doubleValue() > summ
									.getClosing().doubleValue() ? FeedSetting
									.getColor(FeedSetting.C_PLUS) : (summ
									.getHighest().doubleValue() < summ
									.getClosing().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
					fieldLow.setForeground(summ.getLowest().doubleValue() > summ
							.getPrevious().doubleValue() ? FeedSetting
							.getColor(FeedSetting.C_PLUS)
							: (summ.getLowest().doubleValue() < summ
									.getPrevious().doubleValue()) ? FeedSetting
									.getColor(FeedSetting.C_MINUS)
									: FeedSetting.getColor(FeedSetting.C_ZERO));
				} else {
					fieldOpen.setText("- ");
					fieldHigh.setText("- ");
					fieldLow.setText("- ");
					fieldLast.setText("- ");
					fieldChange.setText("- ");
					fieldPrev.setText("- ");
					fieldOpen.setText("- ");
					fieldHigh.setText("- ");
					fieldLow.setText("- ");
				}
			}
		});
	}

	private String getChange(double slastprice, double nprev) {
		String schange = " ";
		if (slastprice > 0) {
			double ntemp = slastprice - nprev;
			double ndbl = ((double) (ntemp * 100) / nprev);
			int ntmpdbl = (int) (ndbl * 100);
			double npersen = (double) ntmpdbl / 100;
			if (ntemp > 0) {
				schange = "+" + formatter.format(ntemp) + "("
						+ formatter2.format(npersen) + "%) ";
			} else if (ntemp == 0) {
				schange = "" + formatter.format(ntemp) + "(0.00%) ";
			} else {
				schange = "" + formatter.format(ntemp) + "("
						+ formatter2.format(npersen) + "%) ";
			}
		}
		return schange;
	}

	void calculateLot() {
		try {
			double lotSize = Double.parseDouble(TradingSetting.getLot(TradingSetting.C_LOTSIZE));
			double share = ((Double) fieldNewShare.getValue()).doubleValue();
			int i = ((IEQTradeApp) apps).getTradingEngine()
					.getStore(TradingStore.DATA_STOCK)
					.getIndexByKey(new String[] { fieldStock.getText() });
			if (i >= 0) {
				Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_STOCK).getDataByIndex(i);
				if (stock != null) {
					lotSize = (stock.getLotSize().doubleValue());
				}
			}
			fieldNewLot.setValue(new Double(share / lotSize));
		} catch (Exception ex) {
		}
		calculate();
	}

	void calculate() {
		double lotSize = -1, price = 0, volume = 0, lot = 0;
		String client = "", newClient = "";
		try {
			fieldNewLot.commitEdit();
			fieldNewPrice.commitEdit();
			lot = (Double)fieldNewLot.getValue();
			price = (Double)fieldNewPrice.getValue();
			//lot = ((Double) fieldNewLot.getValue()).doubleValue();
			//price = ((Double) fieldNewPrice.getValue()).doubleValue();
			client = fieldClient.getText();
			newClient = comboNewClient.getTextEditor().getText();
			int i = ((IEQTradeApp) apps).getTradingEngine()
					.getStore(TradingStore.DATA_STOCK)
					.getIndexByKey(new String[] { fieldStock.getText() });
			if (i >= 0) {
				Stock stock = (Stock) ((IEQTradeApp) apps).getTradingEngine()
						.getStore(TradingStore.DATA_STOCK).getDataByIndex(i);
				if (stock != null) {
					lotSize = (stock.getLotSize().doubleValue());
				}
			}
			if (lotSize != -1) {
				volume = lotSize * lot;
				fieldNewValue.setValue(new Double(volume * price));
				fieldNewShare.setValue(new Double(volume));
			} else {
				fieldNewValue.setValue(null);
				fieldNewShare.setValue(null);
			}
		} catch (Exception ex) {
			fieldNewValue.setValue(null);
			fieldNewShare.setValue(null);
		}
		try {
			if (entryType.equals("BUY")) {
				double limit = ((Double) fieldTradingLimit.getValue())
						.doubleValue();
				if (client.equals(newClient)) {
					if ((volume * price - ((Double) fieldOldValue.getValue())
							.doubleValue()) > limit)
						labelOverlimit.setText("OVER LIMIT");
					else
						labelOverlimit.setText("");
				} else {
					if (volume * price > limit)
						labelOverlimit.setText("OVER LIMIT");
					else
						labelOverlimit.setText("");
				}
			} else {
				double limit = ((Double) fieldStockLimit.getValue())
						.doubleValue();
				if (!client.equals(newClient)) {
					if (lot > limit)
						labelOverlimit.setText("SHORT SELL");
					else
						labelOverlimit.setText("");
				} else {
					labelOverlimit.setText("");
				}
			}
		} catch (Exception ex) {
			labelOverlimit.setText("");
		}
	}

	private boolean isValidEntry() {
		order = (Order) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ORDER)
				.getDataByKey(new String[] { order.getId(), "0" });
		if (order == null) {
			Utils.showMessage("invalid order, please select order first", form);
			hide();
			return false;
		}
		if (!order.getStatus().equals(Order.C_OPEN)
				&& !order.getStatus().equals(Order.C_PARTIAL_MATCH)) {
			Utils.showMessage(
					"cannot amend this order\nbecause order has been changed",
					form);
			hide();
			return false;
		}
		try {
			fieldNewPrice.commitEdit();
		} catch (Exception ex) {
		}
		Double price = (Double) fieldNewPrice.getValue();
		if (price != null) {
			if (price.doubleValue() <= 0) {
				Utils.showMessage("price must be greater than zero", form);
				//fieldNewPrice.requestFocus();
				((JSpinner.DefaultEditor)fieldNewPrice.getEditor ()).getTextField().requestFocus();
				return false;
			}
		} else {
			Utils.showMessage("please enter valid price", form);
			//fieldNewPrice.requestFocus();
			((JSpinner.DefaultEditor)fieldNewPrice.getEditor ()).getTextField().requestFocus();
			return false;
		}
		if (order.getBoard().equals("NG")) {
			try {
				fieldNewShare.commitEdit();
				calculateLot();
			} catch (Exception ex) {
			}
		} else {
			try {
				fieldNewLot.commitEdit();
				calculate();
			} catch (Exception ex) {
			}
		}
		Double newShare = (Double) fieldNewShare.getValue();
		if (newShare != null) {
			if (newShare.doubleValue() <= 0) {
				Utils.showMessage("new share must be greater then zero", form);
				//fieldNewLot.requestFocus();
				((JSpinner.DefaultEditor)fieldNewLot.getEditor ()).getTextField().requestFocus();
				return false;
			}

		} else {
			Utils.showMessage("please enter valid new share", form);
			//fieldNewLot.requestFocus();
			((JSpinner.DefaultEditor)fieldNewLot.getEditor ()).getTextField().requestFocus();
			return false;
		}
		int i = ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getIndexByKey(
						new String[] { getAccountId(comboNewClient.getText()) });
		if (i < 0) {
			Utils.showMessage("please enter valid Client", form);
			comboNewClient.getTextEditor().requestFocus();
			return false;
		}

		if (order.getPrice().doubleValue() == price.doubleValue()
				&& order.getVolume().doubleValue() == newShare.doubleValue()
				&& order.getTradingId().equals(
						comboNewClient.getTextEditor().getText())) {
			Utils.showMessage("please enter valid new order information", form);
			//fieldNewLot.requestFocus();
			((JSpinner.DefaultEditor)fieldNewLot.getEditor ()).getTextField().requestFocus();
			return false;
		}
		double oldShare = order.getVolume().doubleValue();
		if (newShare.doubleValue() > oldShare) {
			Utils.showMessage(
					"invalid input for new share, must be equal or lower than old share",
					form);
			//fieldNewLot.requestFocus();
			((JSpinner.DefaultEditor)fieldNewLot.getEditor ()).getTextField().requestFocus();
			return false;
		}

		double nDone = order.getDoneVol().doubleValue();
		if (newShare.doubleValue() <= nDone) {
			Utils.showMessage("new share must be greater than done share", form);
			if (order.getBoard().equals("NG")) {
				fieldNewShare.requestFocus();
			} else {
				//fieldNewLot.requestFocus();
				((JSpinner.DefaultEditor)fieldNewLot.getEditor ()).getTextField().requestFocus();
			}
			return false;
		}
		return true;
	}

	void subscribe() {
		((IEQTradeApp) apps).getFeedEngine().subscribe(
				FeedParser.PARSER_QUOTE,
				FeedParser.PARSER_QUOTE + "|" + soldstock + "#"
						+ fieldBoard.getText());
	}

	void unsubscribe() {
		if (!soldstock.equals("")) {
			((IEQTradeApp) apps).getFeedEngine().unsubscribe(
					FeedParser.PARSER_QUOTE,
					FeedParser.PARSER_QUOTE + "|" + soldstock + "#"
							+ fieldBoard.getText());
		}
	}

	void changePF(String accountid) {
		((FilterColumn) filterPF.getFilteredData("account"))
				.setField(accountid);
		filterPF.fireFilterChanged();
	}

	void clearForm() {
		try {
			unsubscribe();
			fieldClient.setText("");
			fieldNewLot.setValue(null);
			fieldNewPrice.setValue(null);
			fieldStock.setText("");
			fieldOldValue.setValue(null);
			fieldTradingLimit.setValue(null);
			fieldBoard.setText("");
			fieldType.setText("");
			fieldIA.setText("");
			fieldOldShare.setValue(null);
			// fieldNewClientName.setText("");
			fieldNewClientName.getTextEditor().setText("");
			comboNewClient.getTextEditor().setText("");
			fieldNewShare.setValue(null);
			changePF("");
			((FilterColumn) filterBid.getFilteredData("stock")).setField("");
			((FilterColumn) filterBid.getFilteredData("bo")).setField("B");
			((FilterColumn) filterOffer.getFilteredData("stock")).setField("");
			((FilterColumn) filterOffer.getFilteredData("bo")).setField("O");
			filterBid.fireFilterChanged();
			filterOffer.fireFilterChanged();
			soldstock = "";
			chgStockSummary();
			//fieldNewPrice.requestFocus();
			((JSpinner.DefaultEditor)fieldNewPrice.getEditor ()).getTextField().requestFocus();
			checkConfirm
					.setSelected(hSetting.get("confirm") != null ? (((Boolean) hSetting
							.get("confirm")).booleanValue()) : true);
		} catch (Exception ex) {
		}
	}

	private String genConfirm() {
		//String strLot = ((JSpinner.DefaultEditor)fieldNewLot.getEditor()).getTextField().getText();
		//String strPrice = ((JSpinner.DefaultEditor)fieldNewPrice.getEditor()).getTextField().getText();
		//lot = Double.parseDouble(strLot);
		//price = Double.parseDouble(strPrice);
		String result = "";
		result = result.concat("client    : " + fieldClient.getText()).concat("\n");
		result = result.concat("stock    : " + fieldStock.getText().concat("\n"));
		result = result.concat("price     : " + formatter.format((Double) fieldOldPrice.getValue())).concat("\n");
		result = result.concat("lot         : " + formatter.format((Double) fieldOldLot.getValue())).concat(" lot\n");
		result = result.concat("\namend to\n\n");
		
		//result = result.concat("client    : " + fieldClient.getText()).concat("\n"); DIRUBAH 04 APRIL 2014
		result = result.concat("client    : " + comboNewClient.getText()).concat("\n");
		result = result.concat("stock    : " + fieldStock.getText().concat("\n"));
		result = result.concat("price     : " + formatter.format((Double)fieldNewPrice.getValue())).concat("\n");
		result = result.concat("lot         : " + formatter.format((Double)fieldNewLot.getValue())).concat(" lot\n");
		return result;
	}

	boolean _send = false;
	public int nconfirm = Utils.C_YES_DLG;

	private void send() {
		double fieldprev = 0;
		System.err.println("advertising"+(order.getIsAdvertising().equals("1") ? "Yes" : "No"));
		try {
			if (!_send) {
				_send = true;
				if (sendAction == null || !sendAction.isEnabled()) {
					Utils.showMessage("cannot amend order, please reconnect..",
							form);
					_send = false;
				} else
					if (isValidEntry()){
						 StockSummary summ = (StockSummary) ((IEQTradeApp) apps)
							.getFeedEngine().getStore(FeedStore.DATA_STOCKSUMMARY)
							.getDataByKey(new Object[] { soldstock, fieldBoard.getText() });// fixing sesuai board yosep
						 fieldprev = summ.getPrevious();
					if (!order.getIsAdvertising().equals("1") && order.getBoard().equals("NG")){
					//Double price = (Double)fieldNewPrice.getValue();
					System.err.println("tes : "+(Double)fieldNewPrice.getValue() +" "+ fieldprev);

						if((Double)fieldNewPrice.getValue() > (fieldprev+(fieldprev/(100/50)))){	
						HashMap param = new HashMap();
						param.put(("Result"), (entryType.equals("BUY") ? "Confirm Neg Deal\n": "Entry Neg Deal\n"));
						param.put("Client", "Client        :  " + comboNewClient.getText());
						param.put("Stock", "Stock        :  "
								+ fieldStock.getText().concat("\n"));
						param.put("Price", "Price         :  "
						+ formatter.format((Double) fieldNewPrice.getValue()));
						param.put("Volume","Shares      :  "
								+ formatter.format((Double) fieldNewShare.getValue()));
						param.put("form", TradingAction.A_SHOWAMENDORDER.toString());
						((TradingApplication) apps).getUI().showUI(
								TradingUI.UI_PIN_NEGO, param);
						System.out.println("pinnego");
						}
						else{
							if (checkConfirm.isSelected()) {
							nconfirm = Utils.showConfirmDialog(form, "Amend Order",
									"Are you sure want to send this order?\n"
											+ genConfirm(), Utils.C_YES_DLG);
							}
							System.out.println("NCONFIRM ="+nconfirm);
							if (nconfirm == Utils.C_YES_DLG) {
								sendOrder();
							} else {
								nconfirm = 0;
							}
						}
					}
					else{
						if (checkConfirm.isSelected()) {
							nconfirm = Utils.showConfirmDialog(form, "Amend Order",
									"Are you sure want to send this order?\n"
											+ genConfirm(), Utils.C_YES_DLG);
							}
							System.out.println("NCONFIRM ="+nconfirm);
							if (nconfirm == Utils.C_YES_DLG) {
								sendOrder();
							} else {
								nconfirm = 0;
							}
					}
				}
				_send = false;
			}
			else{
			System.err.println("send123");
				}
			} catch (Exception ex) {
				log.error(Utils.logException(ex));
				_send = false;
			}
		}
						/*if (nconfirm == 0) {
							sendOrder();
							System.out.println("KIRIM");
						} else {
							// hide();
							System.out.println("KLUAR");
						}*/
						/*}
						else{
							if (checkConfirm.isSelected()) {
								nconfirm = Utils.showConfirmDialog(form, "Amend Order",
										"are you sure want to amend this order?\n"
												+ genConfirm(), Utils.C_YES_DLG);
							}
							if (nconfirm == Utils.C_YES_DLG) {
								sendOrder();
							}
						}
					} else {
						System.out.println("KELUAR NEGO");
					}

				} else {
					if (isValidEntry()) {
				}
					int nconfirm = Utils.C_YES_DLG;
					if (checkConfirm.isSelected()) {
						nconfirm = Utils.showConfirmDialog(form, "Amend Order",
								"are you sure want to amend this order?\n"
										+ genConfirm(), Utils.C_YES_DLG);
					}
					if (nconfirm == Utils.C_YES_DLG) {
						sendOrder();
					}
				}
				_send = false;
			}

		} catch (Exception ex) {
			// log.error(Utils.logException(ex));
			_send = false;*/

	public void confirm(){
		if (checkConfirm.isSelected()) {
			nconfirm = Utils.showConfirmDialog(form, "Amend Order",
					"are you sure want to amend this order?\n"
							+ genConfirm(), Utils.C_YES_DLG);
		}
		if (nconfirm == Utils.C_YES_DLG) {
			sendOrder();
			System.err.println("TANPA CONFIRM"+nconfirm);
		}
		else {
			System.out.println("KELUAR NEGO");
			nconfirm = 0;
			System.err.println("TANPA CONFIRM2"+nconfirm);
		}
	}

	void sendOrder() {
		order = (Order) ((IEQTradeApp) apps).getTradingEngine()
				.getStore(TradingStore.DATA_ORDER)
				.getDataByKey(new String[] { order.getId(), "0" });
		if (order == null) {
			Utils.showMessage("invalid order, please select order first", form);
		} else if (!order.getStatus().equals(Order.C_OPEN)
				&& !order.getStatus().equals(Order.C_PARTIAL_MATCH)) {
			Utils.showMessage(
					"cannot amend this order\nbecause order has been changed",
					form);
		} else {
			//log.info("(amend)user aggree to amend order: " + order.getId());
			// double balance = order.getBalVol().doubleValue();
			// double oldQty = order.getVolume().doubleValue();
			double newQty = (Double)fieldNewLot.getValue() //Double.parseDouble(strLot)
					* (order.getVolume().doubleValue() / order.getLot()
							.doubleValue());
			Double bal = new Double(newQty);
			Order neworder = new Order();
			neworder.fromVector((Vector) order.getData().clone());
			neworder.setPrice((Double)fieldNewPrice.getValue());
			neworder.setPrevOrderId(order.getId());
			neworder.updateVolume(bal.doubleValue());
			neworder.setEntryTime(OrderIDGenerator.getTime());
			neworder.setMarketOrderId("");
			neworder.setEntryBy(((IEQTradeApp) apps).getTradingEngine()
					.getUserId().toUpperCase());
			neworder.setAmendBy("");
			neworder.setAmendTime(null);
			neworder.setWithdrawBy("");
			neworder.setWithdrawTime(null);
			Account acc = (Account) ((IEQTradeApp) apps)
					.getTradingEngine()
					.getStore(TradingStore.DATA_ACCOUNT)
					.getDataByKey(
							new String[] { getAccountId(comboNewClient
									.getText()) });
			neworder.setAccId(acc.getAccId());
			neworder.setTradingId(acc.getTradingId());
			neworder.setComplianceId(acc.getComlianceId());
			neworder.setInvType(acc.getInvType());
			neworder.setId(new String(OrderIDGenerator.gen(acc.getTradingId())));
			((IEQTradeApp) apps).getTradingEngine().amendOrder(order, neworder);
		}
		clearForm();
		hide();
	}

	void registerEvent(JComponent comp) {
		InputMap inputMap = comp.getInputMap(JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0), "priceAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0), "qtyAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.ALT_MASK),
				"altCAction");

		comp.getActionMap().put("altCAction", new AbstractAction("altCAction") {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnCancel.doClick();
			}
		});

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.ALT_MASK),
				"altSAction");

		comp.getActionMap().put("altSAction", new AbstractAction("altSAction") {

			@Override
			public void actionPerformed(ActionEvent e) {
				btnSend.doClick();
			}
		});

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, Event.SHIFT_MASK),
				"shiftTabAction");

		comp.getActionMap().put("shiftTabAction",
				new AbstractAction("shiftTabAction") {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (e.getSource()
								.equals(comboNewClient.getTextEditor())) {
							//fieldNewPrice.requestFocus();
							((JSpinner.DefaultEditor)fieldNewLot.getEditor ()).getTextField().requestFocus();
						} else if (e.getSource().equals(
								fieldNewClientName.getTextEditor())) {
							comboNewClient.requestFocus();
						}
					}
				});

		comp.getActionMap().put("priceAction",
				new AbstractAction("priceAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						//fieldNewPrice.requestFocus();
						((JSpinner.DefaultEditor)fieldNewPrice.getEditor ()).getTextField().requestFocus();
					}
				});
		comp.getActionMap().put("qtyAction", new AbstractAction("qtyAction") {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent evt) {
				//fieldNewLot.requestFocus();
				((JSpinner.DefaultEditor)fieldNewLot.getEditor ()).getTextField().requestFocus();
			}
		});
		comp.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						btnCancel.doClick();
					}
				});

		comp.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton) {
							((JButton) evt.getSource()).doClick();
						} else {
							if (evt.getSource() instanceof JNumber) {
								try {
									((JNumber) evt.getSource()).commitEdit();
								} catch (Exception ex) {
								}
								;
							}
							btnSend.doClick();
						}
					}
				});
	}

	String getAccountId(String alias) {
		Account acc = (Account) ((IEQTradeApp) apps)
				.getTradingEngine()
				.getStore(TradingStore.DATA_ACCOUNT)
				.getDataByField(new String[] { alias },
						new int[] { Account.C_TRADINGID });
		return (acc != null ? acc.getAccId() : "");
	}

    private class IDXPriceSpinnerModel extends SpinnerNumberModel {
		private static final long serialVersionUID = 1L;

		public IDXPriceSpinnerModel() {
			super(new Double(0), new Double(0), new Double(100000000), new Double(1));
		}
		
		public Object getNextValue() {
			// New TickPrice
			Double newValue = new Double(50);
			Double oldValue = (Double) this.getValue();
			
			//System.out.println("old value next"+oldValue);
			
			/*if (oldValue < Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE1)))
				newValue = oldValue + 1;
			if (oldValue >= Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE2)))
				newValue = oldValue + 5;
			if (oldValue >= Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE3)))
				newValue = oldValue + 25;
			if (newValue < Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE4)))
				newValue = new Double(50);*/
			
			if (oldValue < 200)
				 newValue = oldValue + 1;
			 if (oldValue >= 200)
				 newValue = (oldValue - (oldValue % 2 )) + 2;
			 if (oldValue >= 500)
				 newValue = (oldValue - (oldValue % 5 )) + 5;
			 if (oldValue >= 2000)
				 newValue = (oldValue - (oldValue % 10 )) + 10;
			 if (oldValue >= 5000)
				 newValue = (oldValue - (oldValue % 25 )) + 25;
			 if (newValue < 50)
				 newValue = new Double(50);
			
			//System.out.println("new value next "+newValue);
			
			return new Double(newValue);
		}

			public Object getPreviousValue() {
			// New TickPrice
			Double newValue = new Double(50);
			Double oldValue = (Double) this.getValue();
			
			//System.out.println("old value "+oldValue);
			
			/*if (oldValue <= Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE1)))
				newValue = oldValue - 1;
			if (oldValue > Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE2)))
				newValue = oldValue - 5;
			if (oldValue > Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE3)))
				newValue = oldValue - 25;
			if (newValue < Integer.parseInt(TradingSetting.getLot(TradingSetting.C_OLDVALUE4)))
				newValue = new Double(50);
			
*/
			 if (oldValue < 200)
				 newValue = oldValue - 1;
			 if (oldValue > 200)
				 newValue = oldValue - ((oldValue % 2 ) == 0?2:(oldValue % 2 ));
			 if (oldValue > 500)
				 newValue = oldValue - ((oldValue % 5 ) == 0?5:(oldValue % 5 ));
			 if (oldValue > 2000)
				 newValue = oldValue - ((oldValue % 10 ) == 0?10:(oldValue % 10 ));
			 if (oldValue > 5000)
				 newValue = oldValue - ((oldValue % 25 ) == 0?25:(oldValue % 25 ));
			 if (newValue < 50)
				 newValue = new Double(50);

			//System.out.println("new value "+newValue);
			
			return new Double(newValue);
		}

		/*public Object getNextValue() {
			Double newValue = new Double(50);
			Double oldValue = (Double) this.getValue();
			if (oldValue < 500)
			newValue = oldValue + 1;
			if (oldValue >= 500)
			newValue = oldValue + 5;
			if (oldValue >= 5000)
			newValue = oldValue + 25;
			if (newValue < 50)
			newValue = new Double(50);
			return new Double(newValue);
			}

			public Object getPreviousValue() {
			Double newValue = new Double(50);
			Double oldValue = (Double) this.getValue();
			if (oldValue < 500)
			newValue = oldValue - 1;
			if (oldValue > 500)
			newValue = oldValue - 5;
			if (oldValue > 5000)
			newValue = oldValue - 25;
			if (newValue < 50)
			newValue = new Double(50);
			return new Double(newValue);
			}*/
		
		/*public Object getNextValue() {
    		Double newValue = new Double(50);
    		Double oldValue = (Double)this.getValue();
    		if (oldValue<200) newValue = oldValue + 1;
    		if (oldValue>=200) newValue = oldValue + 5;
    		if (oldValue>=500) newValue = oldValue + 10;
    		if (oldValue>=2000) newValue = oldValue + 25;
    		if (oldValue>=5000) newValue = oldValue + 50;
    		if (newValue<50) newValue = new Double(50);
    		return new Double(newValue);
    	}
    	
    	public Object getPreviousValue() {
    		Double newValue = new Double(50);
    		Double oldValue = (Double)this.getValue();
    		if (oldValue<200) newValue = oldValue - 1;
    		if (oldValue>200) newValue = oldValue - 5;
    		if (oldValue>500) newValue = oldValue - 10;
    		if (oldValue>2000) newValue = oldValue - 25;
    		if (oldValue>5000) newValue = oldValue - 50;
    		if (newValue<50) newValue = new Double(50);
    		return new Double(newValue);
    	}*/
    }
    
    void getTime(){
		try {
			((IEQTradeApp)apps).getTradingEngine().getConnection().getServertime();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
