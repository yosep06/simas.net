package eqtrade.trading.ui.core;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.FeedSetting;
import eqtrade.feed.core.Utils;
import eqtrade.trading.app.TradingUI;

public final class UIChgPassword extends UI {
	private JSkinDlg frame;
	private JLabel lblNewPass;
	private JLabel lblConfirmNewPass;
	private JLabel lblPass;
	private JPasswordField fieldNewPass;
	private JPasswordField fieldConfirmPass;
	private JPasswordField fieldPass;
	private JButton btnOK;
	private JButton btnCancel;

	public UIChgPassword(String app) {
		super("Change Password", app);
		type = C_DIALOG;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_CHGPASS);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				fieldPass.requestFocus();
			}
		});
	}

	@Override
	protected void build() {
		fieldPass = new JPasswordField();
		fieldNewPass = new JPasswordField();
		fieldNewPass.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				fieldNewPass.selectAll();
			}
		});
		fieldConfirmPass = new JPasswordField();
		fieldConfirmPass.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				fieldConfirmPass.selectAll();
			}
		});
		btnOK = new JButton("OK");
		btnOK.setMnemonic('O');
		btnOK.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnOKAction();
			}
		});
		btnCancel = new JButton("Cancel");
		btnCancel.setMnemonic('C');
		btnCancel.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		lblPass = new JLabel("Old");
		lblPass.setDisplayedMnemonic('l');
		lblPass.setLabelFor(fieldPass);
		lblNewPass = new JLabel("New");
		lblNewPass.setDisplayedMnemonic('n');
		lblNewPass.setLabelFor(fieldNewPass);
		lblConfirmNewPass = new JLabel("Confirm");
		lblConfirmNewPass.setDisplayedMnemonic('f');
		lblConfirmNewPass.setLabelFor(fieldConfirmPass);
		registerEvent(fieldConfirmPass);
		registerEvent(fieldNewPass);
		registerEvent(fieldPass);
		registerEvent(btnOK);
		registerEvent(btnCancel);

		JSkinPnl pnlEntry = new JSkinPnl();
		FormLayout layoutEntry = new FormLayout("pref, 2dlu, 200px",
				"pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref, 2dlu, pref");
		PanelBuilder pnlBuilder = new PanelBuilder(layoutEntry, pnlEntry);
		pnlBuilder.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		pnlBuilder.add(this.lblPass, cc.xy(1, 3));
		pnlBuilder.add(this.fieldPass, cc.xy(3, 3));
		pnlBuilder.add(lblNewPass, cc.xy(1, 7));
		pnlBuilder.add(this.fieldNewPass, cc.xy(3, 7));
		pnlBuilder.add(this.lblConfirmNewPass, cc.xy(1, 9));
		pnlBuilder.add(this.fieldConfirmPass, cc.xy(3, 9));

		pnlContent = new JSkinPnl();
		pnlContent.add(pnlEntry, BorderLayout.CENTER);
		pnlContent.add(buildButton(), BorderLayout.SOUTH);
	}

	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png"))
				.getImage());
		frame.setResizable(false);
		frame.setContent(pnlContent);
		frame.pack();
		frame.setLocation(((Rectangle) hSetting.get("size")).x,
				((Rectangle) hSetting.get("size")).y);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
	}

	private void btnOKAction() {
		String strPwd = Utils.getMD5(new String(fieldPass.getPassword()));
		String strNewPwd = Utils.getMD5(new String(fieldNewPass.getPassword()));
		String strConfirmPwd = Utils.getMD5(new String(fieldConfirmPass
				.getPassword()));
		if (new String(fieldNewPass.getPassword()).trim().equals("")) {
			JOptionPane.showMessageDialog(frame,
					"Failed, New Password cannot be empty", "alert",
					JOptionPane.ERROR_MESSAGE);
			focus();
			return;
		}
		if (new String(fieldNewPass.getPassword()).trim().length() < 8) {
			JOptionPane.showMessageDialog(frame,
					"Failed, Minimum length for Password is 8", "alert",
					JOptionPane.ERROR_MESSAGE);
			focus();
			return;
		}
		if (strPwd
				.equals(((IEQTradeApp) apps).getTradingEngine().getPassword())) {
			if (strNewPwd.equalsIgnoreCase(strConfirmPwd)
					&& !strNewPwd.equalsIgnoreCase(strPwd)) {
				((IEQTradeApp) apps).getTradingEngine().changePasswd(strPwd,
						strNewPwd);
				setState(false);
			} else {
				JOptionPane.showMessageDialog(frame,
						"New Password is not valid", "alert",
						JOptionPane.ERROR_MESSAGE);
				fieldNewPass.requestFocus();
			}
		} else {
			JOptionPane.showMessageDialog(frame, "Old Password is not valid",
					"alert", JOptionPane.ERROR_MESSAGE);
			focus();
		}
	}

	private JSkinPnl buildButton() {
		JSkinPnl pnlButton = new JSkinPnl();
		FormLayout layoutButton = new FormLayout(
				"pref:grow, 2dlu, pref, 2dlu, pref", "pref");
		PanelBuilder builderBtn = new PanelBuilder(layoutButton, pnlButton);
		builderBtn.setDefaultDialogBorder();
		CellConstraints cc = new CellConstraints();
		builderBtn.add(this.btnOK, cc.xy(3, 1));
		builderBtn.add(this.btnCancel, cc.xy(5, 1));
		return pnlButton;
	}

	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}

	public void failed(String reason) {
		JOptionPane.showMessageDialog(frame, reason, "alert",
				JOptionPane.ERROR_MESSAGE);
		setState(true);
	}

	@Override
	public void hide() {
		saveSetting();
		frame.setVisible(false);
	}

	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) FeedSetting
				.getLayout((String) getProperty(C_PROPERTY_NAME));
		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("size", new Rectangle(20, 20, 600, 400));
		}
	}

	@Override
	public void refresh() {
		if (frame != null) {
			frame.updateUI();
			SwingUtilities.updateComponentTreeUI(frame);
		}
	}

	private void registerEvent(JComponent comm) {
		InputMap inputMap = comm
				.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
				"escapeAction");
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");
		comm.getActionMap().put("escapeAction",
				new AbstractAction("escapeAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						btnCancel.doClick();
					}
				});
		comm.getActionMap().put("enterAction",
				new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent evt) {
						if (evt.getSource() instanceof JButton)
							((JButton) evt.getSource()).doClick();
						else
							btnOK.doClick();
					}
				});
	}

	@Override
	public void saveSetting() {
		hSetting.put("size",
				new Rectangle(frame.getX(), frame.getY(), frame.getWidth(),
						frame.getHeight()));
		FeedSetting.putLayout((String) getProperty(C_PROPERTY_NAME), hSetting);
	}

	public void setState(boolean state) {
		fieldPass.setEnabled(state);
		fieldNewPass.setEnabled(state);
		fieldConfirmPass.setEnabled(state);
		btnOK.setEnabled(state);
		btnCancel.setEnabled(state);
	}

	public void setStatus(String status) {
		Utils.showMessage(status, frame);
	}

	@Override
	public void show() {
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}

	@Override
	public void show(Object param) {
		show();
	}

	public void success() {
		Utils.showMessage(" Change Password successfully ", frame);
		close();
	}

	public void setWait(boolean b) {
		// TODO Auto-generated method stub

	}
}
