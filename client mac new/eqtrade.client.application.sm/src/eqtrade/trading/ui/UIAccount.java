package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterColumn;
import com.vollux.ui.JDropDown;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.IEQTradeApp;
import eqtrade.feed.engine.FeedStore;
import eqtrade.feed.model.Stock;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.AccountDef;

public class UIAccount extends UI {
	private JGrid table;
	private JDropDown comboStock;
	private JButton btnView;
	private JButton btnClear;
	private FilterStockAccount filterByStock;

	public UIAccount(String app) {
		super("Account List", app);
		type = C_FRAME;
		setProperty(C_PROPERTY_NAME, TradingUI.UI_ACCOUNT);
	}

	@Override
	public void focus() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				table.getTable().requestFocus();
			}
		});
	}

	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();
		JMenuItem propertiesMenu = new JMenuItem(new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				table.showProperties();
			}
		});
		propertiesMenu.setText("Properties");
		popupMenu.add(propertiesMenu);
	}

	@Override
	protected void build() {
		createPopup();
		filterByStock = new FilterStockAccount(pnlContent, ((IEQTradeApp) apps)
				.getTradingEngine().getStore(TradingStore.DATA_PORTFOLIO));

		table = createTable(
				((IEQTradeApp) apps).getTradingEngine().getStore(
						TradingStore.DATA_ACCOUNT), filterByStock,
				(Hashtable) hSetting.get("table"));

		btnView = new JButton("View");
		btnView.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				view();
			}
		});
		btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				clear();
			}
		});
		comboStock = new JDropDown(((IEQTradeApp) apps).getFeedEngine()
				.getStore(FeedStore.DATA_STOCK), Stock.CIDX_CODE, true);
		comboStock.setBorder(new EmptyBorder(0, 0, 0, 0));

		comboStock.getTextEditor().addFocusListener(new FocusAdapter() {

			@Override
			public void focusGained(FocusEvent e) {
				comboStock.showPopup();
			}

			@Override
			public void focusLost(FocusEvent e) {
				view();
			}

		});
		registerEventCombo(comboStock);

		table.getTable().add(popupMenu);
		table.getTable().addMouseListener(new MyMouseAdapter());
		table.addMouseListener(new MyCustomMouseAdapter());
		table.setColumnHide(AccountDef.columhide);

		JPanel pnlTop = new JPanel();

		FormLayout l = new FormLayout(
				"2dlu,50px,2dlu,80px,2dlu,80px,2dlu,80px", "2dlu,pref,2dlu");
		CellConstraints c = new CellConstraints();
		PanelBuilder b = new PanelBuilder(l, pnlTop);
		b.add(new JLabel("Stock"), c.xy(2, 2));
		b.add(comboStock, c.xy(4, 2));
		b.add(btnView, c.xy(6, 2));
		b.add(btnClear, c.xy(8, 2));

		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 2, 2));
		pnlContent.add(pnlTop, BorderLayout.NORTH);
		pnlContent.add(table, BorderLayout.CENTER);
		((FilterColumn) filterByStock.getFilteredData("stock")).setField("");
		filterByStock.fireFilterChanged();

		refresh();
	}

	protected void clear() {
		/*comboStock.getTextEditor().setText("");
		((FilterColumn) filterByStock.getFilteredData("stock")).setField("");
		filterByStock.fireFilterChanged();*/
		String stock = "";
				
				if (thAccount != null) {
					thAccount.stop();
					thAccount = null;
				}
				thAccount = new ThreadAccount(stock);
				thAccount.start();
				btnView.setEnabled(false);
				comboStock.setEnabled(false);
	}

	protected void view() {
		/*String stock = comboStock.getTextEditor().getText();
		((FilterColumn) filterByStock.getFilteredData("stock")).setField(stock);
		filterByStock.fireFilterChanged();*/
		String stock = comboStock.getTextEditor().getText();

				if (thAccount != null) {
					thAccount.stop();
					thAccount = null;
				}
				thAccount = new ThreadAccount(stock);
				thAccount.start();
				btnView.setEnabled(false);
				comboStock.setEnabled(false);
	}

	@Override
	public void loadSetting() {
		//hSetting = (Hashtable) TradingSetting
		//		.getLayout((String) getProperty(C_PROPERTY_NAME));


		hSetting = null;


		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", AccountDef.getTableDef());
		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 400, 300));
	}

	private void registerEventCombo(JDropDown comp) {
		comp.getEditor().getEditorComponent()
				.setFocusTraversalKeysEnabled(false);
		InputMap inputMap = comp.getTextEditor().getInputMap(
				JComponent.WHEN_FOCUSED);
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enterAction");

		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "tabAction");

		comp.getTextEditor().getActionMap()
				.put("enterAction", new AbstractAction("enterAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(comboStock.getTextEditor())) {
							comboStock.setSelectedIndex(comboStock
									.getSelectedIndex());
							comboStock.hidePopup();
							btnView.requestFocus();
							// fieldStockName.requestFocus();

						}
					}

					// comboStock.requestFocus();

				});

		comp.getTextEditor().getActionMap()
				.put("tabAction", new AbstractAction("tabAction") {
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {

						if (e.getSource().equals(comboStock.getTextEditor())) {
							comboStock.setSelectedIndex(comboStock
									.getSelectedIndex());
							comboStock.hidePopup();
							btnView.requestFocus();
						}
					}
				});

	}

	@Override
	public void saveSetting() {
		hSetting.put(
				"size",
				new Rectangle(form.getX(), form.getY(), form.getWidth(), form
						.getHeight()));
		hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),
				hSetting);
	}

	@Override
	public void refresh() {
		table.getViewport().setBackground(
				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
	}
	ThreadAccount thAccount;
	class ThreadAccount extends Thread
	{
		String stock = "";
		public ThreadAccount(String stock) {
			this.stock = stock;
		}


		@Override
		public void run() {
//
//			((FilterColumn) filterByStock.getFilteredData("stock")).setField(null);
//			filterByStock.fireFilterChanged();
//
//			((IEQTradeApp)apps).getTradingEngine().isRefreshAccount(false);
			log.info("UIAccount filter "+stock);
			((FilterColumn) filterByStock.getFilteredData("stock")).setField(stock);
			filterByStock.fireFilterChanged();
			log.info("UIAccount filter finish");
			btnView.setEnabled(true);
			comboStock.setEnabled(true);
//			((IEQTradeApp)apps).getTradingEngine().isRefreshAccount(true);
			((IEQTradeApp)apps).getTradingEngine().getStore(TradingStore.DATA_ACCOUNT).refresh();
		}
	}
}
