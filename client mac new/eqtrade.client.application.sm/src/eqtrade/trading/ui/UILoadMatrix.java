package eqtrade.trading.ui;

import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.vollux.framework.UI;
import com.vollux.idata.GridModel;
import com.vollux.ui.JGrid;
import com.vollux.ui.JSkinDlg;
import com.vollux.ui.JSkinPnl;

import eqtrade.application.EQTrade;
import eqtrade.application.IEQTradeApp;
import eqtrade.feed.core.Utils;
import eqtrade.trading.app.TradingUI;
import eqtrade.trading.core.TradingSetting;
import eqtrade.trading.engine.TradingStore;
import eqtrade.trading.model.BrowseOrder;
import eqtrade.trading.model.DueDateDef;
import eqtrade.trading.model.OrderMatrix;
import eqtrade.trading.model.OrderMatrixDef;
import eqtrade.trading.model.UserProfile;

public class UILoadMatrix extends UI{
	private JGrid table;
	private JSkinDlg frame;
	private JButton btnOk;
	private String label = "";
	private JTable tempTable;

	private DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	
	public UILoadMatrix(String app) {
		super("Order Matrix", app);
		setProperty(C_PROPERTY_NAME, TradingUI.UI_ORDERMATRIX);
	}
	
	@Override
	protected void createPopup() {
		popupMenu = new JPopupMenu();		
		JMenuItem delete = new JMenuItem(new AbstractAction() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {		
				try {
					if(!label.isEmpty()) {
						int nconfirm = Utils.C_YES_DLG;
						nconfirm = Utils.showConfirmDialog(form, "Order Matrix","are you sure want to delete this label?\n", Utils.C_YES_DLG);
						if (nconfirm == Utils.C_YES_DLG) {	
//							SwingUtilities.invokeLater(new Runnable() {								
//								@Override
//								public void run() {
									((IEQTradeApp) apps).getTradingEngine().deleteOrderMatrixByLabel(((IEQTradeApp) apps).getFeedEngine().getUserId(), label.trim());	
									Thread.sleep(1000);
									((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).getDataVector().clear();
									((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).refresh();
									((IEQTradeApp) apps).getTradingEngine().refreshOrderMatrix(((IEQTradeApp) apps).getFeedEngine().getUserId());
									
//								}
//							});
							Utils.showMessage("Label "+label+" Has Been Deleted!", form);
						}							
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		delete.setText("Delete");
		popupMenu.add(delete);
	}
	
	@Override
	protected void initUI() {
		frame = new JSkinDlg(title);
		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png")).getImage());
		frame.getInsidePanel().setBorder(new EmptyBorder(0, 1, 1, 1));
		frame.setContent(pnlContent);
		frame.pack();
		frame.setLocation(((Rectangle) hSetting.get("size")).x, ((Rectangle) hSetting.get("size")).y);
		frame.setSize(((Rectangle) hSetting.get("size")).width,
		((Rectangle) hSetting.get("size")).height);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				close();
			}
		});
	}

	@Override
	public void close() {
		saveSetting();
		frame.dispose();
		frame = null;
	}
	
	@Override
	protected void build() {
		createPopup();
		
		table = createTable(((IEQTradeApp)apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST), null,(Hashtable) hSetting.get("table"));
		table.setColumnHide(OrderMatrixDef.columhide);		
		table.getTable().addMouseListener(new MouseAdapter() {			
			@Override
			public void mouseReleased(MouseEvent e) {
				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
					int i = table.getMappedRow(table.getSelectedRow());
					if (i > -1) {
						final OrderMatrix om = (OrderMatrix) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).getDataByIndex(i);
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								HashMap param = new HashMap();
								param.put("ORDERMATRIXID", om.getOrdermatrixid());
								param.put("LABEL", om.getLabel());
								param.put("ISBASKET", om.getIsBasket());
								if(om.getIsBasket().equals("1")) {
									param.put("BASKETTIME", om.getBasketTime());									
								}	
								param.put("ISUPDATETRUE", 1);								
								apps.getUI().showUI(TradingUI.UI_ORDERMATRIX,param);
								
							}
						});
						frame.setVisible(false);
					}
				} else if (e.isPopupTrigger()) {
					int rowindex = table.getSelectedRow();
					
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
					
					tempTable = table.getTable();
					
					label = tempTable.getModel().getValueAt(rowindex,0).toString();					
					popupMenu.show(e.getComponent(), e.getX(), e.getY());
				}
			}
		});
		
		pnlContent = new JSkinPnl(new BorderLayout());
		pnlContent.setBorder(new EmptyBorder(2, 2, 0, 2));
		pnlContent.add(table, BorderLayout.CENTER);
		
		refresh();		
	}

	@Override
	public void focus() {		
		
	}

	@Override
	public void loadSetting() {
		hSetting = (Hashtable) TradingSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
//		if (hSetting == null) {
			hSetting = new Hashtable();
			hSetting.put("table", OrderMatrixDef.getTableDef());
//		}
		if (hSetting.get("size") == null)
			hSetting.put("size", new Rectangle(20, 20, 500, 300));
		
	}
	
	@Override
	public void refresh() {
		table.getViewport().setBackground(TradingSetting.getColor(TradingSetting.C_BACKGROUND));
		table.setNewFont(TradingSetting.getFont());
		
	}
		
	@Override
	public void show() {
		focus();
		Utils.showToCenter(frame);
		frame.setVisible(true);
	}
	
	@Override
	public boolean isReady() {
		return frame != null;
	}

	@Override
	public boolean isShow() {
		return (frame.isShowing());
	}
	
	private Thread thread;
	
	@Override
	public void show(Object param) {
		HashMap p = (HashMap) param;
		query((String) p.get("CLIENT"));
		show();
	}
	
	private void query(final String param) {
		if (thread != null) {
			thread.stop();
			thread = null;
		}
		thread = new Thread(new Runnable() {
			@Override
			public void run() {			
				((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).getDataVector().clear();
				((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).refresh();
				((IEQTradeApp) apps).getTradingEngine().refreshOrderMatrix(param);
			}
		});
		thread.start();
	}
	
	@Override
	public void saveSetting() {
		hSetting.put("size",new Rectangle(frame.getX(), frame.getY(), frame.getWidth(),frame.getHeight()));
		hSetting.put("table", table.getTableProperties());
		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),hSetting);		
	}
}

//package eqtrade.trading.ui;
//
//import java.awt.BorderLayout;
//import java.awt.Rectangle;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.HashMap;
//import java.util.Hashtable;
//
//import javax.swing.ImageIcon;
//import javax.swing.JButton;
//import javax.swing.JPanel;
//import javax.swing.JTable;
//import javax.swing.SwingUtilities;
//import javax.swing.WindowConstants;
//import javax.swing.border.EmptyBorder;
//
//import com.jgoodies.forms.builder.PanelBuilder;
//import com.jgoodies.forms.layout.CellConstraints;
//import com.jgoodies.forms.layout.FormLayout;
//import com.vollux.framework.UI;
//import com.vollux.idata.GridModel;
//import com.vollux.ui.JGrid;
//import com.vollux.ui.JSkinDlg;
//import com.vollux.ui.JSkinPnl;
//
//import eqtrade.application.EQTrade;
//import eqtrade.application.IEQTradeApp;
//import eqtrade.feed.core.Utils;
//import eqtrade.trading.app.TradingUI;
//import eqtrade.trading.core.TradingSetting;
//import eqtrade.trading.engine.TradingStore;
//import eqtrade.trading.model.BrowseOrder;
//import eqtrade.trading.model.DueDateDef;
//import eqtrade.trading.model.OrderMatrix;
//import eqtrade.trading.model.OrderMatrixDef;
//import eqtrade.trading.model.UserProfile;
//
//public class UILoadMatrix extends UI{
//	private JGrid table;
//	private JSkinDlg frame;
//	private JButton btnOk;
//
//	private DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
//	
//	public UILoadMatrix(String app) {
//		super("Order Matrix", app);
//		setProperty(C_PROPERTY_NAME, TradingUI.UI_ORDERMATRIX);
//	}
//	
//	@Override
//	protected void initUI() {
//		frame = new JSkinDlg(title);
//		frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
//		frame.setIconImage(new ImageIcon(EQTrade.class.getResource("icon.png")).getImage());
//		frame.getInsidePanel().setBorder(new EmptyBorder(0, 1, 1, 1));
//		frame.setContent(pnlContent);
//		frame.pack();
//		frame.setLocation(((Rectangle) hSetting.get("size")).x, ((Rectangle) hSetting.get("size")).y);
//		frame.setSize(((Rectangle) hSetting.get("size")).width,
//		((Rectangle) hSetting.get("size")).height);
//		frame.addWindowListener(new WindowAdapter() {
//			@Override
//			public void windowClosing(WindowEvent e) {
//				close();
//			}
//		});
//	}
//
//	@Override
//	public void close() {
//		saveSetting();
//		frame.dispose();
//		frame = null;
//	}
//	
//	@Override
//	protected void build() {
//		table = createTable(((IEQTradeApp)apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST), null,(Hashtable) hSetting.get("table"));
//		table.setColumnHide(OrderMatrixDef.columhide);		
//		table.getTable().addMouseListener(new MouseAdapter() {			
//			@Override
//			public void mouseReleased(MouseEvent e) {
//				if (!e.isPopupTrigger() && e.getClickCount() == 2) {
//					int i = table.getMappedRow(table.getSelectedRow());
//					if (i > -1) {
//						final OrderMatrix om = (OrderMatrix) ((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).getDataByIndex(i);
//						SwingUtilities.invokeLater(new Runnable() {
//							@Override
//							public void run() {
//								HashMap param = new HashMap();
//								param.put("ORDERMATRIXID", om.getOrdermatrixid());
//								param.put("LABEL", om.getLabel());
//								param.put("ISBASKET", om.getIsBasket());
//								//System.out.println("ORDERMATRIXID : "+om.getOrdermatrixid());
//								//System.out.println("ORDERMATRIXLABEL : "+om.getLabel());
//								if(om.getIsBasket().equals("1")) {
//									String time = om.getBasketTime();
//									param.put("BASKETTIME", time);
//								}								
//								//System.out.println("PARAM UILoadMatrix : "+param);
//								apps.getUI().showUI(TradingUI.UI_ORDERMATRIX,param);
//								
//							}
//						});
//						frame.setVisible(false);
//					}
//				}
//			}
//		});
//		
//		pnlContent = new JSkinPnl(new BorderLayout());
//		pnlContent.setBorder(new EmptyBorder(2, 2, 0, 2));
//		pnlContent.add(table, BorderLayout.CENTER);
//		
//		refresh();		
//	}
//
//	@Override
//	public void focus() {		
//		
//	}
//
//	@Override
//	public void loadSetting() {
//		hSetting = (Hashtable) TradingSetting.getLayout((String) getProperty(C_PROPERTY_NAME));
////		if (hSetting == null) {
//			hSetting = new Hashtable();
//			hSetting.put("table", OrderMatrixDef.getTableDef());
////		}
//		if (hSetting.get("size") == null)
//			hSetting.put("size", new Rectangle(20, 20, 500, 300));
//		
//	}
//	
//	/*@Override
//	public void close() {
//		saveSetting();
//		frame.dispose();
//		frame = null;
//	}*/
//	
//	@Override
//	public void refresh() {
//		table.getViewport().setBackground(
//				TradingSetting.getColor(TradingSetting.C_BACKGROUND));
//		table.setNewFont(TradingSetting.getFont());
//		
//	}
//	
//	
//	@Override
//	public void show() {
//		focus();
//		Utils.showToCenter(frame);
//		frame.setVisible(true);
//	}
//	
//	@Override
//	public boolean isReady() {
//		return frame != null;
//	}
//
//	@Override
//	public boolean isShow() {
//		return (frame.isShowing());
//	}
//	
//	private Thread thread;
//	
//	@Override
//	public void show(Object param) {
//		HashMap p = (HashMap) param;
//		query((String) p.get("CLIENT"));
//		show();
//	}
//	
//	private void query(final String param) {
//		if (thread != null) {
//			thread.stop();
//			thread = null;
//		}
//		thread = new Thread(new Runnable() {
//			@Override
//			public void run() {
//				//System.out.println("param query uiloadmatrix : "+param);
//				((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).getDataVector().clear();
//				((IEQTradeApp) apps).getTradingEngine().getStore(TradingStore.DATA_ORDERMATRIXLIST).refresh();
//				((IEQTradeApp) apps).getTradingEngine().refreshOrderMatrix(param);
//			}
//		});
//		thread.start();
//	}
//	
//	@Override
//	public void saveSetting() {
//		hSetting.put("size",new Rectangle(frame.getX(), frame.getY(), frame.getWidth(),frame.getHeight()));
//		hSetting.put("table", table.getTableProperties());
//		TradingSetting.putLayout((String) getProperty(C_PROPERTY_NAME),hSetting);		
//	}
//}
