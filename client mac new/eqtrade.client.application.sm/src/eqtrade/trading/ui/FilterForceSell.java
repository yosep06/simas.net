package eqtrade.trading.ui;

import java.awt.Component;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.table.TableModel;

import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.ui.FilterBase;
import com.vollux.ui.FilterColumn;

import eqtrade.trading.model.Account;
import eqtrade.trading.model.Margin;

public class FilterForceSell extends FilterBase{

	private GridModel dataPortfolio;

	public FilterForceSell(Component parent, GridModel _dataPortfolio) {
		super(parent, "FilterForce");
		mapFilter.put("margin", new FilterColumn("margin",Vector.class,
				null,FilterColumn.C_MEMBEROF));
		mapFilter.put("ratio", new FilterColumn("ratio", Double.class,
				new Double(74.5), FilterColumn.C_GREATER));
		mapFilter.put("daycount", new FilterColumn("daycount", Double.class,
				new Double(2), FilterColumn.C_GREATER));
		this.dataPortfolio = _dataPortfolio;
	}

	@Override
	public boolean filter(TableModel model, int row) {
		boolean avail = false;
		try{
			Column val = (Column) model.getValueAt(row, 0);
			Margin dat = (Margin) val.getSource();
			if(//((FilterColumn)mapFilter.get("margin") ).compare(dat.getAccId()))
				((FilterColumn)mapFilter.get("ratio")).compare(dat.getCurrentRatio()) &&
				((FilterColumn)mapFilter.get("daycount")).compare(dat.getDayCount()))
					
					{avail = true;}
			return avail;
		}catch(Exception e){
			e.printStackTrace();return false;
		}
	}

	@Override
	public Object getFilteredData(String name) {
		return mapFilter.get(name);
	}

	@Override
	public void setFilteredData(Object src, String name) {
		this.mapFilter.put(name, src);
		fireFilterChanged();
		
	}
	public void setFilter(HashMap map) {
		if(map!=null){
			this.mapFilter = (HashMap) map.clone();
			fireFilterChanged();
		}
	}

}
