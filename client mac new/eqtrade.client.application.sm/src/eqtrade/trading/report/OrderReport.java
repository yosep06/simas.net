package eqtrade.trading.report;

import java.awt.Color;
import java.awt.Component;
import java.io.File;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JasperViewer;
import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.CustomExpression;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Page;
import ar.com.fdvs.dj.domain.constants.Transparency;
import ar.com.fdvs.dj.domain.constants.VerticalAlign;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;

import com.vollux.idata.Column;
import com.vollux.idata.GridModel;
import com.vollux.idata.indirection.ImmutableIData;

import eqtrade.trading.model.Order;
import eqtrade.trading.model.OrderDef;
import eqtrade.trading.ui.OrderPanel.ExcelFileFilter;

public class OrderReport {
	public static NumberFormat formatter = new DecimalFormat("#,##0  ");
	public static SimpleDateFormat formattime = new SimpleDateFormat("HH:mm:ss");
	public static SimpleDateFormat formatdate = new SimpleDateFormat(
			"yyyy-MM-dd");
	public static HashMap hashStatus = new HashMap();
	public static HashMap hashType = new HashMap();
	public static HashMap hashSide = new HashMap();
	public static HashMap hashAccType = new HashMap();
	static {
		hashStatus.put("SE", "Sending entry...");
		hashStatus.put("ST", "Sending temp...");
		hashStatus.put("SD", "Sending delete...");
		hashStatus.put("SW", "Sending withdraw...");
		hashStatus.put("SA", "Sending amend...");
		hashStatus.put("SES", "Sending entry (on server)...");
		hashStatus.put("STS", "Sending temp (on server)...");
		hashStatus.put("SDS", "Sending delete (on server)...");
		hashStatus.put("SWS", "Sending withdraw (on server)...");
		hashStatus.put("SAS", "Sending amend (on server)...");
		hashStatus.put("F", "Failed");
		hashStatus.put("O|P", "All Open");
		hashStatus.put("0", "Open");
		hashStatus.put("SE|ST|SD|SW|SA|SES|STS|SDS|SWS|SAS", "All Sending");
		hashStatus.put("E|ET|EW|EA|ED", "All Request");
		hashStatus.put("S", "Split");
		hashType.put("0", "Day");
		hashType.put("S", "Session");
		hashSide.put("1", "Buy");
		hashSide.put("2", "Sell");
	}
	Hashtable hsettings;
	protected JFileChooser jfc = null;
	protected PdfFileFilter filterFile = null;
	Component parent;

	public OrderReport(Component parent, Hashtable hsettings) {
		this.hsettings = hsettings;
		filterFile = new PdfFileFilter();
		filterFile.addExtension("pdf");
		filterFile.setDescription(" PDF Format ");
		jfc = new JFileChooser(new File("C:\\"));
		jfc.setFileFilter(filterFile);
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setDialogTitle("Save Order to....");
		jfc.setDialogType(JFileChooser.SAVE_DIALOG);
		this.parent = parent;
	}

	public void print(Vector<Order> order) {

		String filename = "";
		int returnVal = jfc.showSaveDialog(parent);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File ffile = jfc.getSelectedFile();
			filename = ffile.getAbsolutePath();
			if (!filename.endsWith(".pdf")) {
				filename = filename + ".pdf";
			}
		} else {
			return;
		}

		GridModel orderModel = createModel(order);
		int[] columnwidth = (int[]) hsettings.get("width");
		int[] columnorder = (int[]) hsettings.get("order");
		Object[] columnPrint = createColumnPrint(columnwidth, columnorder);
		String[] columnheader = (String[]) hsettings.get("header");
		try {
			FastReportBuilder drb = new FastReportBuilder();

			for (int i = 0; i < columnPrint.length; i++) {
				int idx = (Integer) columnPrint[i];
				// if (columnwidth[i] > 0) {
				String header = columnheader[idx];

				Style detailStyle = new Style();

				detailStyle.setBorderRight(Border.THIN);
				detailStyle.setBorderLeft(Border.THIN);
				detailStyle.setBorderBottom(Border.THIN);
				detailStyle.setBorderTop(Border.THIN);
				detailStyle
						.setHorizontalAlign(isDoubleField(idx) ? HorizontalAlign.RIGHT
								: HorizontalAlign.LEFT);

				Style headerStyle = new Style();
				headerStyle.setFont(Font.ARIAL_MEDIUM_BOLD);
				headerStyle.setBorderTop(Border.THIN);
				headerStyle.setBorderBottom(Border.THIN);			
				headerStyle.setBorderLeft(Border.THIN);
				detailStyle.setBorderLeft(Border.THIN);
				headerStyle.setBorderRight(Border.THIN);
				detailStyle.setBorderRight(Border.THIN);

				headerStyle.setBackgroundColor(Color.GRAY);
				headerStyle.setTransparency(Transparency.OPAQUE);
				headerStyle.setTextColor(Color.white);
				headerStyle.setHorizontalAlign(HorizontalAlign.CENTER);
				headerStyle.setVerticalAlign(VerticalAlign.MIDDLE);
				int width = getColumnWidth(columnwidth[i], idx);

				AbstractColumn ac = ColumnBuilder.getNew().setTitle(header)
						.setStyle(detailStyle).setHeaderStyle(headerStyle)
						.setColumnProperty(header, String.class.getName())
						.setWidth(width)
						.setCustomExpression(new CustomOrderExpression(idx))
						.build();
				drb.addColumn(ac);
				// }
			}

			JRDataSource ds = new JRTableModelDataSource(orderModel);
			drb.setUseFullPageWidth(true);
			// drb.setPageSizeAndOrientation(new Page(500, 1600, true));
			drb.setPageSizeAndOrientation(Page.Page_A4_Landscape());
			drb.setMargins(20, 20, 5, 5);

			DynamicReport dr = drb.build();
			JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr,
					new ClassicLayoutManager(), ds);

			JasperExportManager.exportReportToPdfFile(jp, filename);
			// JasperViewer.viewReport(jp);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private int getColumnWidth(int defaultwidth, int idx) {
		// TODO Auto-generated method stub
		switch (idx) {
		case Order.C_ORDERID:
			defaultwidth = 130;
			break;
		case Order.C_MARKETORDERID:
			defaultwidth = 90;
			break;
		case Order.C_TRADINGID:
			defaultwidth = 70;
			break;
		case 15:
		case 17:
		case 21:
		case 22:
		case 23:
		case 27:
		case 28:
		case 47:
		case 50:
		case 52:
		case 53:
		case 54:
		case 63:
		case 64:
		case 65:
		case 66:
			defaultwidth = 50;
			break;
		case 1:
		case 19:
		case 36:
		case 37:
		case 38:
		case 39:
		case 42:
		case 43:
		case 46:
		case 57:
			defaultwidth = 50;
			break;
		case Order.C_ORDERAMOUNT:
			defaultwidth = 65;
			break;
		case Order.C_LOT:
			defaultwidth = 30;
			break;
		case Order.C_STATUSID:
			defaultwidth = 70;
			break;
		case Order.C_MATCHTIME:
		case Order.C_ENTRYRTTIME:
			defaultwidth = 60;
			break;
		case Order.C_CONTRAUSERID:
			defaultwidth = 80;
			break;
		default:
			defaultwidth = 50;
			break;
		}
	
		return defaultwidth;
	}

	private boolean isDoubleField(int idx) {
		switch (idx) {
		case 15:
		case 16:
		case 17:
		case 21:
		case 22:
		case 23:
		case 27:
		case 28:
		case 47:
		case 50:
		case 52:
		case 53:
		case 54:
		case 63:
		case 64:
		case 65:
		case 66:
		case 74:
			return true;
		default:
			break;
		}
		return false;
	}

	private GridModel createModel(Vector<Order> orders) {
		GridModel orderModel = new GridModel(OrderDef.getHeader(), false);
		Vector vorder = new Vector();

		for (Order order : orders) {
			vorder.addElement(OrderDef.createTableRow(order));
		}
		orderModel.addRow(vorder, false, false);

		return orderModel;
	}

	public class CustomOrderExpression implements CustomExpression {
		private int col;

		public CustomOrderExpression(int column) {
			col = column;
		}

		@Override
		public Object evaluate(Map fields, Map variable, Map parameters) {
			Column column = null;
			// / System.out.println(col+" columns");
			Object value = fields.get(OrderDef.getHeader().get(col));
			String strFieldName = OrderDef.dataHeader[col];

			String strTemp;
			if (value instanceof ImmutableIData) {
				ImmutableIData args = (ImmutableIData) value;
				Object dat = args.getData();
				Order o = (Order) args.getSource();

				if (dat instanceof Double) {
					double d = Math.floor(((Double) dat).doubleValue());
					double s = (((Double) dat).doubleValue()) - d;
					if (s > 0) {
						formatter.setMaximumFractionDigits(2);
						formatter.setMinimumFractionDigits(2);
					} else {
						formatter.setMaximumFractionDigits(0);
						formatter.setMinimumFractionDigits(0);
					}
					return formatter.format(dat);
				} else if (dat instanceof Date) {
					if (strFieldName
							.equals(OrderDef.dataHeader[Order.C_ORDERDATE])) {
						return formatdate.format((Date) dat);
					} else {
						return formattime.format((Date) dat);
					}
				} else if (dat instanceof Timestamp) {
					return formattime.format(new Date(((Timestamp) dat)
							.getTime()));
				} else if (strFieldName
						.equals(OrderDef.dataHeader[Order.C_STATUSID])) {
					if (dat != null && o != null) {
						if (dat.equals("o") && o.getIsBasketOrder().equals("1")) {
							strTemp = "Queue";
						} else {
							strTemp = (String) hashStatus.get(dat);
						}
						if (strTemp == null)
							strTemp = dat.toString();
						return strTemp;
					} else {
						strTemp = (String) hashStatus.get(dat);
						if (strTemp == null)
							strTemp = dat + " Unknown";
						return strTemp;
					}
				} else if (strFieldName
						.equals(OrderDef.dataHeader[Order.C_ORDERTYPE])) {
					strTemp = (String) hashType.get(dat);
					if (strTemp == null)
						strTemp = dat + " Uknown";
					return strTemp;
				} else if (strFieldName
						.equals(OrderDef.dataHeader[Order.C_ORDTYPE])) {
					if (dat != null) {
						if (dat.equals("7") || dat.equals("E"))
							return "Limited Order";
						else
							return dat + " unknown";
					} else {
						return "";
					}
				} else if (strFieldName
						.equals(OrderDef.dataHeader[Order.C_ISADVERTISING])
						|| strFieldName
								.equals(OrderDef.dataHeader[Order.C_ISBASKETORDER])
						|| strFieldName
								.equals(OrderDef.dataHeader[Order.C_ISOVERLIMIT])
						|| strFieldName
								.equals(OrderDef.dataHeader[Order.C_ISFLOOR])
						|| strFieldName
								.equals(OrderDef.dataHeader[Order.C_ISSHORTSELL])) {
					if (dat == null) {
						return "";
					} else {
						return dat.equals("1") ? "Yes" : "No";
					}
				} else if (strFieldName
						.equals(OrderDef.dataHeader[Order.C_BUYORSELL])) {
					strTemp = (String) hashSide.get(dat);

					if (strTemp == null)
						strTemp = dat + " Unknown";
					return strTemp;
				} else if (dat == null) {
					return "";
				}

				return " " + dat.toString();

			}

			return value;

		}

		@Override
		public String getClassName() {
			return String.class.getName();
		}

	}

	private Object[] createColumnPrint(int[] columnwidth, int[] columnorder) {
		Vector<Integer> vprint = new Vector<Integer>();
		for (int i = 0; i < columnorder.length; i++) {
			int idx = columnorder[i];
			if (columnwidth[i] > 0) {
				vprint.add(idx);
			}
		}
		return vprint.toArray();
	}

	public class PdfFileFilter extends FileFilter {
		private Hashtable filters = null;
		private String description = null;
		private String fullDescription = null;
		private boolean useExtensionsInDescription = true;

		public PdfFileFilter() {
			this.filters = new Hashtable();
		}

		public PdfFileFilter(String extension) {
			this(extension, null);
		}

		public PdfFileFilter(String extension, String description) {
			this();
			if (extension != null)
				addExtension(extension);
			if (description != null)
				setDescription(description);
		}

		public PdfFileFilter(String[] filters) {
			this(filters, null);
		}

		public PdfFileFilter(String[] filters, String description) {
			this();
			for (int i = 0; i < filters.length; i++) {
				// add filters one by one
				addExtension(filters[i]);
			}
			if (description != null)
				setDescription(description);
		}

		public boolean accept(File f) {
			if (f != null) {
				if (f.isDirectory()) {
					return true;
				}
				String extension = getExtension(f);
				if (extension != null && filters.get(getExtension(f)) != null) {
					return true;
				}
				;
			}
			return false;
		}

		public String getExtension(File f) {
			if (f != null) {
				String filename = f.getName();
				int i = filename.lastIndexOf('.');
				if (i > 0 && i < filename.length() - 1) {
					return filename.substring(i + 1).toLowerCase();
				}
				;
			}
			return null;
		}

		public void addExtension(String extension) {
			if (filters == null) {
				filters = new Hashtable(5);
			}
			filters.put(extension.toLowerCase(), this);
			fullDescription = null;
		}

		public String getDescription() {
			if (fullDescription == null) {
				if (description == null || isExtensionListInDescription()) {
					fullDescription = description == null ? "(" : description
							+ " (";
					// build the description from the extension list
					Enumeration extensions = filters.keys();
					if (extensions != null) {
						fullDescription += "."
								+ (String) extensions.nextElement();
						while (extensions.hasMoreElements()) {
							fullDescription += ", ."
									+ (String) extensions.nextElement();
						}
					}
					fullDescription += ")";
				} else {
					fullDescription = description;
				}
			}
			return fullDescription;
		}

		public void setDescription(String description) {
			this.description = description;
			fullDescription = null;
		}

		public void setExtensionListInDescription(boolean b) {
			useExtensionsInDescription = b;
			fullDescription = null;
		}

		public boolean isExtensionListInDescription() {
			return useExtensionsInDescription;
		}
	}

}
